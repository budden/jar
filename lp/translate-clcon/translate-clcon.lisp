
#.(asdf:load-system :editor-budden-tools)

(def-merge-packages::! :TRANSLATE-CLCON (:use :cl :budden-tools)
  (:always t)
  (:export
   "
    translate-clcon::*translations*
    translate-clcon::*files*
    translate-clcon::revert-all
    translate-clcon::do-all
    "))

(in-package :TRANSLATE-CLCON)

;; order translations от частного к общему, т.е. put "8.Help" before "Help"
(defparameter *translations*
  '(
    ("Файл не существует. Для создания подайте команду .правка $FileName"
     "File does not exist. To create, enter at the console: .edit $FileName")

    ("1.Файл\" файл" "1.File\" file")
    ("2.Правка\" правка" "2.Edit\" edit")
    ;("3.Lisp\" lisp" "3.Lisp\" lisp")
    ;("4.Tcl/Md\" tcl" "4.Tcl/Md\" tcl")
    ("7.Окно\" окно" "7.Window\" window")        
    ;("8.Bug-workarounds\" secret" "8.Bug-workarounds\" secret")

    ("1.Файл" "1.File")
    ("2.Правка" "2.Edit")
    ("3.Консоль" "3.REPL")
    ("4.Настройка" "4.Settings")
    ("5.История" "5.History")
    ("6.Избранное" "6.Favorites")
    ("7.Окно" "7.Window")
    ("8.Справка" "8.Help")
    
    ("3.Лисп" "3.Lisp")
    ("7.Окно" "7.Window")     
    ("8.Секрет" "8.Bug-workarounds")

    (".файл" ".file")
    (".правка" ".edit")
    (".консоль" ".repl")
    (".настройка" ".settings")
    (".история" ".history")
    (".избранное" ".favorites")
    (".избранное" ".favorites")
    (".окно" ".window")
    (".справка" ".help")
    ("Создать и редактировать файл" "Edit new file")
    ("Открыть для редактирования" "Open for edit")
    ("Выполнить файл Tcl" "Run Tcl script")
    ("Показать последнюю ошибку Tcl" "Show last Tcl error")
    ("Сохранить поток консоли..." "Save output...")
    ("4.Перезагрузить часть исходных текстов ИСР (IDE)" "4.Reload some of IDE sources")
    ("Очистить кеш asd-систем в quicklisp" "Delete quicklisp's ASDF system index")
    ("5.Открыть недавний..." "5.Open recent...")
    ("Выход из лиспа и ИСР" "Quit lisp and IDE")
    ("6.Выход только из ИСР" "Quit IDE only")
    ("Консоль - $title" "Console - $title")
    ("1.Подключиться к текстовому лисп-серверу SWANK" "1.Attach SWANK server")
    ("2.Отключиться от текстового лисп-сервера SWANK" "2.Detach SWANK server")
    ("3.Принудительно остановить подключенный лисп-сервер SWANK" "3.Quit currently attached SWANK server image")
    ("4.Очистить консоль" "4.Clear console")
    ("\"О программе" "\"About")
    ("Поиск идентификатора в Common Lisp Hyperdoc" "Common Lisp Hyperdoc lookup")
    ("1.Руководство clcon" "clcon manual")
    ("6.Сайт по tcl/tk (англ)" "tcl.tk website")
    ("Лисп: автодополнение" "Lisp: complete symbol")
    ("Лисп: перейти к определению" "Lisp: goto definition")
    ("Tcl: автодополнение" "Tcl: complete symbol")
    ("Tcl: перейти к определению" "Tcl: goto proc definition")
    ("м. Маленький шрифт" "v. Small font")
    ("с. Средний шрифт" "c. Average font")
    ("б. Большой шрифт" "b. Big font")
    ("\"Список окон редактора" "\"Editor Buffer List")
    ("\"Консоль" "\"REPL")
    ("\"Редактор" "\"Editor")
    ("Отладчик лиспа" "Lisp Debugger")
    ("Список ошибок" "Compilation errors")
    ("\"Потоки" "\"Threads")
    ("\"Ходьба" "\"Stepping")
    ("Отладчик, уровень $level" "Debugger level $level")
    ("Не могу сохранить файл истории:\n$fid" "Unable to save history file:\n$fid")
    ("Возможно, clcon в панике:\n$err" "clcon panic:\n$err")

    ("Сохранить как..." "Save As")
    ("1.Перейти к определению пакета" "1.Go to package definition")
    ("2.Установить этот пакет в консоли" "2.Set this package at the REPL")
    ("3.Перейти к определению системы" "3.Goto ASDF system definition")
    ("4.Скомпилировать и загрузить систему" "4.Compile and load ASDF system")
    ("5.Удалить файлы результата сборки системы" "5.Delete ASDF system output files")
    ("Проставить отступ в выденном фрагменте" "Indent selection")
    ("перейти к определению" "Go to definition")
    ("кто вызывает функцию" "Who calls the function")
    ("справка по идентиф-ру" "Help on symbol")
    ("скопировать идентф-р в буфер обмена" "Copy symbol to clipboard")
    ("\"Открыть\"" "\"Open\"")
    ("\"Сохранить\"" "\"Save\"")
    ("Сохранить, компилировать и загрузить этот файл" "Save, compile and load this file")
    ("1.Копир.имя файла в буфер обмена (стиль unix)" "1.Copy buffer pathname to clipboard (Unix style)")
    ("2.Копир.имя файла в буфер обмена (стиль windows)" "2.Copy buffer pathname to clipboard (Windows style)")

    ("3.Редактировать файл, имя которого под курсором" "3.Edit file with whose name is under cursor")
    ("6.Открыть этот файл другим редактором" "6.Open this file with alternative editor")
    ("\"Создать\"" "\"New\"")
    ("\"Закрыть\"" "\"Close\"")
    ("\"Вырезать\"" "\"Cut\"")
    ("\"Копировать\"" "\"Copy\"")
    ("\"Вставить\"" "\"Paste\"")
    ("\"Искать\"" "\"Find\"")
    ("Найти далее" "Continue last search")
    ("Найти и заменить" "Find and replace")
    ("\"перейти назад\"" "\"Pop recorded position stack\"")
    ("Определения в текущем файле (без сохранения файла!)" "List od definitions in this file (file is not saved!)")
    ("Поставить закладку" "Set up a bookmark")
    ("Перейти к закладке..." "Go to bookmark")
    ("1. Отправить текст в подчинённый интерпретатор" "Send text to slave interpreter")
    ("Tcl: вставить строчку с отступом" "Auto-indent new line")
    ("Маркдаун: выровнять таблицу" "Markdown: make all table lines of equal layout")
    ("Вставить как имя файла Linux" "Paste as linux filename")
    
    ("НайтиИсходникИнтерпретируемогоКодаИлиЗначенияЛокальнойПеременной" "FindSourceOfInterpretedCodeOrOfTheValueOfLocalVariable")
    ("1. Редактировать текущий файл ASDF, если он определён" "1. Edit current asdf component if defined")
    ("2. Редактировать текущую систему ASDF, если она определена" "2. Edit current asdf system if defined")
    ("3. Закрыть отладчик и вызвать перезапуск по умолчанию" "3. Close debugger and invoke default restart")
    ("[Ловушка (catch tag)" "[Catch tag")
    ("2.Стек" "2.Stack")
    ("Перейти к определению/инспектор локальной переменной" "Goto definition/Inspect local variable value")
    ("0.Найти исходник интерпретируемого кода или значения локальной переменной" "0.Find source of interpreted code or of the value of local variable")
    ("1.Смотреть исключение в инспекторе" "1.Inspect current condition")
    ("2.Вернуться из кадра..." "2.Return from frame...")
    ("3.Перезапустить кадр" "3.Restart frame")
    ("4.Выполнить в кадре..."  "4.Eval in frame")
    ("5.Выполнить в кадре и лепо вывести..." "5.Eval in frame and pretty print...")
    ("6.Перейти в режим ходьбы" "6.Switch to stepping mode")
    ("7.Напечатать первые 500 кадров стека в консоли" "7.Print first 500 stack frames in the server console")
    ("8.Получить ещё 20 кадров стека" "8.Get next 20 stack frames")

    ("3.Правка" "3.Edit")
    ("1.Копировать выделенную часть описания ошибки" "1.Copy selected part of error description")
    ("Копировать тек.ячейку" "Copy current cell")
    ;("ВставитьВМенюПунктыПроШрифты" "InsertFontSizeMenuItems")
    ("Введите выражение Лиспа (shift-enter=новая строка) и нажмите Enter: cl-user>"
     "Enter Lisp expression (shift-return=new line) and press Return: cl-user>")
    ("Отладчик не активен" "Debugger is not active")
    ("Режим ходьбы не активен" "Stepper mode is not active")
    ("4.Команды перезапуска" "4.Restarts")
    ("\"Продолжить\" <F5>" "\"Continue\" <F5>")
    ("\"Выйти наружу\" <Shift-F11>" "\"Step out\" <Shift-F11>")
    ("\"Шагнуть вдоль\" <F10>" "\"Step next\" <F10>")
    ("\"Шагнуть внутрь\" <F11>" "\"Step next\" <F11>")
    ("Отладчик, уровень $level" "Debugger level $level")
    ("\"Кадры стека\"" "\"Stack frames\"")
    ("; Выход на уровень $level" "; Exit to level $level")
    ("4.Редактировать файл инициализации" "4.Edit initialization file")
    ;; next line is not an error!
    ("Translate UI to English" "Перевести интерфейс на Русский")
    ("Закрыть все окна, остановить сервер SWANK и выйти из клиентской части clcon?"
     "Close all windows, quit lisp image and quit clcon client?")
    ("Закрыть все окна и выйти из клиентской части clcon?"
     "Close all windows and quit clcon client?")
    ))


(defparameter *files*
  '("c:/yar/lp/clcon/clcon.tcl"
    "c:/yar/lp/clcon/window_menu.tcl"
    "c:/yar/lp/clcon/edt_menu.edt.tcl"
    "c:/yar/lp/clcon/editor_buffer.edt.tcl"
    "c:/yar/lp/clcon/error-browser.erbr.tcl"
    "c:/yar/lp/clcon/inspector.insp.tcl"
    "c:/yar/lp/clcon/inspthrd.tcl"
    "c:/yar/lp/clcon/lisp-debugger.ldbg.tcl"
    ))

(defun revert-all ()
  "This is for me only - budden"
  (budden0:cmd-c """cd /d c:\yar\lp\clcon & hg revert --all"""))

(defun do-all (goal)
  (perga
   ;(revert-all)
   (dolist (file *files*)
     (safely-translate-the-file file *translations* goal)
     )))

(defun translate-string (string list-of-from-and-to &key forward-p)
  "Applies all transformations to a string"
  (perga
   (let result (copy-seq string))
   (cond
    (forward-p
     (dolist (pair list-of-from-and-to)
       (:@ destructuring-bind (from to) pair)
       (__f search-and-replace-seq 'string result from to :test 'char= :all t)))
    (t
     (dolist (pair list-of-from-and-to)
       (:@ destructuring-bind (from to) pair)
       (__f search-and-replace-seq 'string result to from :test 'char= :all t))))
   result))
     

(defun safely-translate-the-file (filename list-of-from-and-to goal &key (external-format nil external-format-supplied-p))
  "Translates the filename according to list-of-from-and-to. If translation is not reversible, fails. 

Copy-paste from editor-budden-tools:replace-many-strings-in-file. 

  list-of-from-and-to is: ((\"text1 in lang А\" \"text1 in lang B\")...(\"textN in lang A\" \"textN in lang B\"))"
  (perga
   (let original (apply 'read-file-into-string filename (dispatch-keyargs-full external-format)))

   (let direction
     (eswitch (goal :test 'string=)
              ("Translate UI to English" t)
              ("Перевести интерфейс на Русский" nil)))
   
   ;; translate 
   (let translated
     (translate-string original list-of-from-and-to
                       :forward-p direction))
   
   ;; check that translation is reversible
   (let translated-and-back
     (translate-string translated list-of-from-and-to
                       :forward-p (not direction)))
   
   (unless (string= original translated-and-back)
     (bt:make-thread
      (lambda ()
        (budden0:winmerge-strings
         nil translated-and-back
         :lfile filename
         :pravyjj-zagolovok "Translated and translated back")
        :name "Running file comparison for translate-clcon::SAFELY-TRANSLATE-THE-FILE"))
     (cerror "Continue anyway" "Translation of ~S is irreversible" filename))
   
   (apply 'alexandria:write-string-into-file translated filename :if-exists :supersede (dispatch-keyargs-full external-format))))


#|

   ;; translate Russian version to English
   ;; make sure to run it on Russian version only (clcon repo)
   (progn (revert-all)
     (do-all "Translate UI to English")) 


   ;; translate English version to Russian
   ;; make sure to run it on English version only (clcon_en repo)
   (progn (revert-all)
     (do-all "Перевести интерфейс на Русский"))

   ;; Make sure to keep translation reversible in both repos at any time
   ;; Never run translation with dirty state of repo
   ;; Translation allows versions to exchange patches.
   ;; Never commit translated code to the same repo

|#
