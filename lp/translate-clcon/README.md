# README #

clcon de-localization. Idea is to establish the reversible translation between codes in different languages, where Russian version, stored in clcon repo, is the central point. 
This way we avoid complexities of translating languages using intensive metaprogramming. This way we can translate strings, identifiers, comments, docstrings. 
Reversitility of the translation is enforced. 