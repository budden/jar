 ; -*- system :parse-js ; encoding: utf-8; -*-
;; Copyright (C) Денис Будяк 2015

(named-readtables:in-readtable :buddens-readtable-a)
(in-package :yar-parse-js)

(def-trivial-test::! parse-js.1 1 1)

; Этот тест будет работать, только если включена модификация equalp
(def-trivial-test::! parse-js.3
  (YAR-PARSE-JS:PARSE-JS
   K:{ 
   #S(YAR-LEXEM:LEXEM
      :TEXT "putr"
      :CLASS :IDENTIFIER)
   #S(YAR-LEXEM:LEXEM
      :TEXT "("
      :CLASS #.(read-from-string "YAR-SPECIAL-CHARACTERS:|(|")
      )
   #S(YAR-LEXEM:LEXEM
      :TEXT "1"
      :CLASS :INTEGER
      :CONTENTS 1
      )
   #S(YAR-LEXEM:LEXEM
      :TEXT ")"
      :CLASS #.(read-from-string "YAR-SPECIAL-CHARACTERS:|)|")
      )
   #S(YAR-LEXEM:LEXEM
      :TEXT ""
      :CLASS :EOF
      )
   K:}
   NIL
   )

  K:{
  #S(YAR-LEXEM:LEXEM
     :TEXT "FUNCALL"
     :CLASS FUNCALL)
  #S(YAR-LEXEM:LEXEM
     :TEXT "putr"
     :CLASS :IDENTIFIER)
  #S(YAR-LEXEM:LEXEM
     :TEXT "1"
     :CLASS :INTEGER
     :CONTENTS 1
     )
  K:}

  :test 'k::limited-equalp
  )

(def-trivial-test::! parse-js.4
  (YAR-PARSE-JS:PARSE-JS
   KONS:{
   #S(LEXEM
      :TEXT "Э"
      :CLASS :IDENTIFIER)
   #S(LEXEM
      :TEXT "."
      :CLASS |.|
      :CONTENTS |.|)
   #S(LEXEM
      :TEXT "Д"
      :CLASS :IDENTIFIER)
   #S(LEXEM
      :TEXT "="
      :CLASS =
      :CONTENTS NIL
      :WHITESPACE-BEFORE NIL)
   #S(LEXEM
      :TEXT "5"
      :CLASS :INTEGER
      :CONTENTS 5
      :WHITESPACE-BEFORE NIL)
   #S(YAR-LEXEM:LEXEM
      :TEXT ""
      :CLASS :EOF
      )
   KONS:}
      NIL)

  KONS:{ #S(LEXEM
            :TEXT "="
            :CLASS =
            :POSITION NIL
            :CONTENTS NIL
            :WHITESPACE-BEFORE NIL)

  KONS:{ #S(LEXEM
            :TEXT "&f"
            :CLASS &F
            :POSITION NIL
            :CONTENTS |.|
            :WHITESPACE-BEFORE NIL)

  #S(LEXEM
     :TEXT "Э"
     :CLASS :IDENTIFIER
     :POSITION NIL
     :CONTENTS NIL
     :WHITESPACE-BEFORE NIL
     )
  
  #S(LEXEM
     :TEXT "Д"
     :CLASS :IDENTIFIER
     :POSITION NIL
     :CONTENTS NIL
     :WHITESPACE-BEFORE NIL
     ) 

  KONS:}
  
  #S(LEXEM
     :TEXT "5"
     :CLASS :INTEGER
     :POSITION NIL
     :CONTENTS 5
     :WHITESPACE-BEFORE NIL
     ) 

  KONS:}
  
  :test 'kons::limited-equalp
  )

