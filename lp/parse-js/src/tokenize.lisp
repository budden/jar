; -*- system :yar-parse-js ; encoding: utf-8; -*-
;; Copyright (C) Денис Будяк 2015

(in-package :yar-parse-js)

(define-condition js-parse-error (simple-error)
  ;; Здесь были предупреждения, я убрал ссылки на эти переменные. Наверное, нужно откуда-то взять эту информацию
  ((line :initform "*line*" :reader js-parse-error-line)
   (char :initform "*char*" :reader js-parse-error-char)))
(defmethod print-object ((err js-parse-error) stream)
  (call-next-method)
  (format stream " (line ~a, character ~a)" (js-parse-error-line err) (js-parse-error-char err)))

