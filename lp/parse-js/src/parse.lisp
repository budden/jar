;; -*- coding: utf-8; -*-  
;; Copyright (C) Денис Будяк 2015
(named-readtables:in-readtable :buddens-readtable-a)
(in-package :yar-parse-js)

(deftparameter *unary-prefix* (cons yar:valid-lexem-class t) :initial-value '(? - Не))

(defparameter *precedence*
  (let ((precs (make-hash-table :test 'eq))) 
    (loop :for ops :in '((=) (Или) (Либо) (И) (Не)
                         (== === !== !===)
                         (--) (< > <= >=) (+ -) (* /) (?))
          :for n :from 1
          :do (dolist (op ops)
                (assert (typep op 'yar:valid-lexem-class))
                (setf (gethash op precs) n)))
    precs))

(defparameter *in-function* nil)
(defparameter *label-scope* nil)
(defmacro with-label-scope (type label &body body)
  `(let ((*label-scope* (k:cons (k:cons ,type ,label) *label-scope*))) ,@body))


(defvar *type-expr-p*)

(defun parse-js (input type-expr-p)
  (let* ((*type-expr-p* type-expr-p)
         (r1 (if (stringp input)
                 (with-input-from-string (in input) (parse-js* in))
                 (parse-js* input)))
         (res (parser-yar-expression:process-methods-and-fields r1)))
    res))

(defun lex-js-budden (stream)
  (let* ((l1
          (etypecase stream
            (stream (lexer-yar:Устаревший-лексер-Яра stream :eof-error-p nil))
            (string (lexer-yar:Устаревший-лексер-Яра stream :eof-error-p nil))
            (k:cons stream)))
         ;;(lexems (yar-lexem:build-up-lexems-1
         ;;         l1
         ;;         ; :add-eof-lexem t
         ;;         ))
         (lexems l1))
    (lambda () (k:pop lexems))
    )) ; lexem

(defun tokenpunc (token type)
  (and token
       (eq (lexem-class token) type)))

(defun/defs parse-js* (stream)
  (def input
       (cond
        ((functionp stream) stream)
        (t (lex-js-budden stream))))

  (def token (funcall input))
  (def peeked nil)

  (def next ()
    (if peeked
        (setf token peeked peeked nil)
        (setf token (funcall input)))
    token)

  (def token-error (token control &rest args)
    (budden-tools:ignored token)
    (apply 'yar:parsing-error control args))

  (def error* (control &rest args)
    (apply #'token-error token control args))

  (def unexpected (token)
    (token-error token "Unexpected token '~a'." token))

  (def expect-2 (class)
    (if (eq (lexem-class token) class)  
        (next)
        (error* "Unexpected token '~a', expected '~a'." token class)))

  (def as-1 (arg)
    ;(format nil #|*trace-output*|# "~%as-1 collecting ~A" arg)
    ;(cons type args)
    arg
    )

  (def as-2 (&rest args)
    ;(format nil #|*trace-output*|# "~%as-2 collecting ~A" args)
    ;(cons type args)
    (apply 'k:list args)
    )

  (def as-funcall (fn args)
    ; (format *trace-output* "~%as-funcall collecting ~A" args)
    (k:list* (parser-yar-expression:make-optimization-borned-operator 'funcall) fn args))
    
  ;(def parenthesised ()
  ;  (expect-2 'yarsp:|(|) (prog1 (expression) (expect-2 'yarsp:|)|)))

  (def expr-atom (allow-calls)
    (cond ((eq (lexem-class token) 'yarsp:\( )
           (next)
           (subscripts (prog1 (expression) (expect-2 'yarsp:\) )) allow-calls))
          ((member (lexem-class token) '(:identifier :string :integer :float ))
           (let ((atom (as-1 token)))
             (subscripts (prog1 atom (next)) allow-calls)))
          (t (unexpected token))))

  (def expr-list (closing &optional allow-trailing-comma allow-empty)
    (let ((elts ()))
      (loop :for first := t :then nil :until (tokenpunc token closing) :do
         (unless first (expect-2 'yarsp:|,|))
         (when (and allow-trailing-comma (tokenpunc token closing)) (return))
         (k:push (unless (and allow-empty (tokenpunc token 'yarsp:|,|)) (expression nil)) elts)) 
      (next)
      (k:reverse elts)))

  (def as-name ()
    (case (lexem-class token)
      (:identifier (prog1 token (next)))
      ;((:operator :keyword :atom) (prog1 (string-downcase (symbol-name (token-value token))) (next)))
      (t (unexpected token))))

  (def subscripts (expr allow-calls)
    (cond ((tokenpunc token 'yarsp:|.|)
           (let ((dot token))
             (next)
             (subscripts (as-2 dot expr (as-name)) allow-calls)))
          #|((tokenpunc token 'yarsp:|[|)
           (next)
           (let ((sub (expression)))
             (expect-2 'yarsp:\])
             (subscripts (as :sub expr sub) allow-calls)))|#
          ((and (tokenpunc token 'yarsp:\( ) allow-calls)
           (next)
           (let ((args (expr-list 'yarsp:\) )))
             (subscripts (as-funcall expr args) t)))
          (t expr)))

  (def maybe-unary (allow-calls type-expr-p)
    (if (and (lexer-yar:operation-p token type-expr-p) (member (lexem-class token) *unary-prefix*))
        (as-2 (prog1 (lexem-class token) (next)) (maybe-unary allow-calls type-expr-p))
        (expr-atom allow-calls)))

  (def expr-op (left min-prec type-expr-p)
    (let (op op-class prec)
      (setf op (and (lexer-yar:operation-p token type-expr-p)
                    token))
      (cond
       ((null op) left)
       (t
        (setf op-class (lexem-class op))
        (setf prec (gethash op-class *precedence*))
        (cond        
         ((and prec (> prec min-prec))
          (let ((right (progn (next) (expr-op (maybe-unary t type-expr-p) prec type-expr-p))))
            (expr-op (as-2 op left right) min-prec type-expr-p)))
         (t 
          left))))))

  (def expr-ops (type-expr-p)
    (expr-op (maybe-unary t type-expr-p) 0 type-expr-p))

  (def maybe-assign (type-expr-p)
    (let ((left (expr-ops type-expr-p)))
      (cond
       ((and (lexer-yar:operation-p token type-expr-p) (eq (lexem-class token) '=))
        (as-2 left (progn (next) (maybe-assign type-expr-p))))
       (t left)))
    )

  (def expression (&optional (commas t) (type-expr-p nil))
    (let ((expr (maybe-assign type-expr-p)))
      (if (and commas (tokenpunc token 'yarsp:|,|))
          (as-2 expr (progn (next) (expression)))
          expr)))

  (as-1 (expression nil *type-expr-p*)))

(defun parse-js-string (&rest args)
  (apply 'parse-js args))
