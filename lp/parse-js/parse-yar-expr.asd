;; Copyright (C) Денис Будяк 2015

(asdf:defsystem :parse-yar-expr
  :description "JavaScript parser"
  :author "Marijn Haverbeke <marijnh@gmail.com>"
  :license "BSD"
  :depends-on (:polir-parsers-1)
  :components
  ((:module :src
            :components ((:file "package")
                         (:file "util" :depends-on ("package"))
                         (:file "tokenize" :depends-on ("util"))
                         (:file "parse" :depends-on ("tokenize"))
                         (:file "parse-tests" :depends-on ("parse"))
                         ))))
