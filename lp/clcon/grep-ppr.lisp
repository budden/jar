;; -*- coding: utf-8 ; Encoding: utf-8 ; system :clcon-server ; -*-
;; backend for grep-browser.grbr.tcl
;; and toplevel search functions, e.g. clco::find-in-clcon-sources

(in-package :clco)

(defparameter |*корень-ппр*| #p"c:/nuxt1/")

(defparameter |*поддиректории-ппр-не-содержащие-исходников-ппр*|
  '("node_modules/" ".git/"))

(defun putq-otnositelqno-kornya-ppr (path)
  (cl-user::ubeditqsya-chtq-putq-ne-nachinaetsya-s-razdelitelya-direktorijj path)
  (#-sbcl identity #+sbcl sb-impl::canonicalize-pathname (merge-pathnames path |*корень-ппр*|)))

(defparameter |*типы-файлов-исходников-ппр*| 
  '("json" "html" "vue" "js"))

(defun |сравнить-исходные-файлы-ппр-для-упорядочивания-при-поиске| (p1 p2)
  (perga-implementation:perga
   (flet |ранг-исходного-файла-ппр-для-сортировки| (p)
     (list
      (position (pathname-type p) |*типы-файлов-исходников-ппр*| :test 'string=)
      (pathname-name p)))
   (uiop/utility:lexicographic<
    (lambda (x y)
      (etypecase x
        (null y) ; если x - nil, то будет меньше, если y не nil
        (integer (< x y))
        (string (string< x y))))
    (|ранг-исходного-файла-ппр-для-сортировки| p1)
    (|ранг-исходного-файла-ппр-для-сортировки| p2)
    )))

(defun ppr-sources ()
  "Возвращает примерный список исходников п-п-р"
  (let ((filelist nil)
        (|запретные-директории|
         (mapcar 'putq-otnositelqno-kornya-ppr |*поддиректории-ппр-не-содержащие-исходников-ппр*|)))
    (budden-tools:map-dir
      (lambda (x) (push x filelist))
     |*корень-ппр*|
     :subdirs :recurse
     :dir-test (lambda (p) (not (member p |запретные-директории| :test 'equal)))
     :file-test (lambda (p)
                  (and (member (pathname-type p) |*типы-файлов-исходников-ппр*| :test 'string=)
                       (not (equal p #p"c:/yar/lp/budden-tools/866.lisp")) ; этот путь всегда выдаёт ошибку, нечего ему быть в поиске
                       )))
    ; filelist
    (sort filelist '|сравнить-исходные-файлы-ппр-для-упорядочивания-при-поиске|)
    ))

(defun find-in-ppr-sources (string)
  "Ищем в исходниках ППР"
  (clco::present-text-filtering-results
   (clco::filter-many-files (clco::ppr-sources) string)
   :title
   (format nil ".иия: ~A" string)))
