﻿Пытаемся убрать проблему с without-interrupts, к-рая вызывается из разных потоков, а именно в q-event

Pattern-A =   
  (bt:with-lock-held (*q-event-lock*) 
    (oduvanchik-ext:without-interrupts))

Доступ к *editor-input* - через: 

listen-editor-input - буквально ничего не делает
oduvanchik-interface:clear-editor-input 
 Pattern-A
get-key-event 
  %editor-input-method (editor-input ignore-abort-attempts-p) 
    dq-event
unget-key-event
 un-event
un-event 
  Pattern-A
oduvanchik-internals::q-event
  Pattern-A
dq-event
  Pattern-A
??? Вопрос: что защищает without-interrupts во всех этих ф-ях? Можно ли обойтись одним *q-event-lock*

Другие применения without-interrupts: 

Похоже, что здесь она защищает кеш строк, т.е. можно заменить на 
oduvanchik-internals:close-line - для защиты строки
invoke-modifying-buffer <- modifying-buffer -> open-line - здесь open-line защищена, но надо в этом убедиться. 


ИТОГО по файлам:
htext1 : защищаем open-line - заменяем на *invoke-modifying-buffer-lock*
input.lisp : защищаем очереди сообщений (все одновременно) - заменяем на *q-event-lock*
text2odu-dispatch-to-oduvan  - то же 
tty-input.lisp - то же
