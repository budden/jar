#!/bin/sh -e
set -x

# use this script to set up a binary distribution of oduvanchik.
#
# Since the binary is not relocatable at the moment, you need to
# build it in the place where it would be extracted on the user's system. 
#
# 1. Make a directory /opt/oduvanchik
#
# 2. Check out clbuild to /opt/oduvanchik/clbuild
# 2a. (Optionally) Install Qt libraries into /opt/oduvanchik/lib
#
# 3. Use clbuild to download oduvanchik
#
# 4. Use clbuild to download SBCL, patch it using sbcl.diff and build it
#
# 5. Run this script
#
# 6. Find tarballs in /opt/oduvanchik
#
# - Only the -base- tarballs is required for users.
# - The optional -qt- tarball extracts on top of the -base- tarball
#   and enables use of the qt backend.
# - The optional -src- tarball enabled use of M-.
#

base=/opt/oduvanchik
ver=$(date '+%Y-%m-%d')-$(cd $base/clbuild/source/oduvanchik && git show-ref --hash=8 HEAD)
export PATH=$base/clbuild:$PATH

cd $base/clbuild/source/oduvanchik
./build.sh tty qt clx
cp oduvanchik $base/

cd $base

tar cjf oduvanchik-bin-base-$ver.tar.bz2 \
	--absolute-names \
	--exclude '*/sbcl.core' \
	$base/oduvanchik \
	$base/clbuild/source/iolib/src/syscalls/libiolib-syscalls.so \
	$base/clbuild/source/osicat/posix/libosicat.so \
	$base/clbuild/target/lib/sbcl \
        $base/clbuild/source/oduvanchik/resources/oduvanchik11.cursor

tar cjf oduvanchik-bin-qt-$ver.tar.bz2 \
	--absolute-names \
	$base/background.svg \
	$base/clbuild/source/commonqt/libcommonqt.so* \
	$base/lib

tar cjf oduvanchik-src-$ver.tar.bz2 \
	--absolute-names \
	--exclude '*/sbcl.core' \
	--exclude '*/source/oduvanchik/oduvanchik' \
	--exclude '*/source/sbcl/obj/*' \
	--exclude '*/source/sbcl/output/*' \
	--exclude '*.fasl' \
	--exclude '*/clbuild/target/*' \
	$base/clbuild
