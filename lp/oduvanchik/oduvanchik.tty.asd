;;;; -*- Mode: Lisp; indent-tabs-mode: nil -*-

(defparameter *oduvanchik-base-directory*
  (make-pathname :name nil :type nil :version nil
                 :defaults (parse-namestring *load-truename*)))

(asdf:defsystem :oduvanchik.tty
     :pathname #.(make-pathname
                        :directory
                        (pathname-directory *oduvanchik-base-directory*)
                        :defaults *oduvanchik-base-directory*)
     :depends-on (:oduvanchik.base)
    :components
    ((:module tty-1
              :pathname #.(merge-pathnames
                           (make-pathname
                            :directory '(:relative "src"))
                           *oduvanchik-base-directory*)
              :components
              (#-oduvan-invisible (:file "ioconnections")
               #+oduvan-invisible (:file "ioconnections-invisible")
               (:file "terminfo")
               (:file "termcap" :depends-on ("terminfo"))
               (:file "tty-disp-rt")
               (:file "tty-display" #-oduvan-invisible :depends-on #-oduvan-invisible ("terminfo" "tty-disp-rt"))
               (:file "tty-screen" #-oduvan-invisible :depends-on #-oduvan-invisible ("terminfo" "tty-disp-rt"))
               (:file "tty-stuff")
               (:file "tty-input" #-oduvan-invisible :depends-on #-oduvan-invisible ("terminfo"))
               (:file "linedit" #-oduvan-invisible :depends-on #-oduvan-invisible ("tty-display"))))))
