;;;; -*- Mode: Lisp; indent-tabs-mode: nil; coding :utf-8 ; encoding: utf-8 ;  system :oduvanchik.base ; -*-
;;;
;;; **********************************************************************
;;; This code was written as part of the CMU Common Lisp project at
;;; Carnegie Mellon University, and has been placed in the public domain.
;;;
;;;
;;; **********************************************************************
;;;
;;; This file defines all the definitions of keysyms (see key-event.lisp).
;;; These keysyms match those for X11.
;;;
;;; Written by Bill Chiles
;;; Modified by Blaine Burks.
;;;

(in-package :oduvanchik-internals)
(named-readtables::in-readtable :oduvanchik-ext-readtable)

;;; The IBM RT keyboard has X11 keysyms defined for the following modifier
;;; keys, but we leave them mapped to nil indicating that they are non-events
;;; to be ignored:
;;;    ctrl             65507
;;;    meta (left)      65513
;;;    meta (right)     65514
;;;    shift (left)     65505
;;;    shift (right)    65506
;;;    lock             65509
;;;


;;; Function keys for the RT.
;;;
(oduvanchik-ext:define-keysym 65470 "F1")
(oduvanchik-ext:define-keysym 65471 "F2")
(oduvanchik-ext:define-keysym 65472 "F3")
(oduvanchik-ext:define-keysym 65473 "F4")
(oduvanchik-ext:define-keysym 65474 "F5")
(oduvanchik-ext:define-keysym 65475 "F6")
(oduvanchik-ext:define-keysym 65476 "F7")
(oduvanchik-ext:define-keysym 65477 "F8")
(oduvanchik-ext:define-keysym 65478 "F9")
(oduvanchik-ext:define-keysym 65479 "F10")
(oduvanchik-ext:define-keysym 65480 "F11" "L1")
(oduvanchik-ext:define-keysym 65481 "F12" "L2")

;;; Function keys for the Sun (and other keyboards) -- L1-L10 and R1-R15.
;;;
(oduvanchik-ext:define-keysym 65482 "F13" "L3")
(oduvanchik-ext:define-keysym 65483 "F14" "L4")
(oduvanchik-ext:define-keysym 65484 "F15" "L5")
(oduvanchik-ext:define-keysym 65485 "F16" "L6")
(oduvanchik-ext:define-keysym 65486 "F17" "L7")
(oduvanchik-ext:define-keysym 65487 "F18" "L8")
(oduvanchik-ext:define-keysym 65488 "F19" "L9")
(oduvanchik-ext:define-keysym 65489 "F20" "L10")
(oduvanchik-ext:define-keysym 65490 "F21" "R1")
(oduvanchik-ext:define-keysym 65491 "F22" "R2")
(oduvanchik-ext:define-keysym 65492 "F23" "R3")
(oduvanchik-ext:define-keysym 65493 "F24" "R4")
(oduvanchik-ext:define-keysym 65494 "F25" "R5")
(oduvanchik-ext:define-keysym 65495 "F26" "R6")
(oduvanchik-ext:define-keysym 65496 "F27" "R7")
(oduvanchik-ext:define-keysym 65497 "F28" "R8")
(oduvanchik-ext:define-keysym 65498 "F29" "R9")
(oduvanchik-ext:define-keysym 65499 "F30" "R10")
(oduvanchik-ext:define-keysym 65500 "F31" "R11")
(oduvanchik-ext:define-keysym 65501 "F32" "R12")
(oduvanchik-ext:define-keysym 65502 "F33" "R13")
(oduvanchik-ext:define-keysym 65503 "F34" "R14")
(oduvanchik-ext:define-keysym 65504 "F35" "R15")

;;; Upper right key bank.
;;;
(oduvanchik-ext:define-keysym 65377 "Printscreen")
;; Couldn't type scroll lock.
(oduvanchik-ext:define-keysym 65299 "Pause")

;;; Middle right key bank.
;;;
(oduvanchik-ext:define-keysym 65379 "Insert")
(oduvanchik-ext:define-keysym 65535 "Delete" "Rubout" (string (code-char 127)))
(oduvanchik-ext:define-keysym 65360 "Home")
(oduvanchik-ext:define-keysym 65365 "Pageup")
(oduvanchik-ext:define-keysym 65367 "End")
(oduvanchik-ext:define-keysym 65366 "Pagedown")

;;; Arrows.
;;;
(oduvanchik-ext:define-keysym 65361 "Leftarrow")
(oduvanchik-ext:define-keysym 65362 "Uparrow")
(oduvanchik-ext:define-keysym 65364 "Downarrow")
(oduvanchik-ext:define-keysym 65363 "Rightarrow")

;;; Number pad.
;;;
(oduvanchik-ext:define-keysym 65407 "Numlock")
(oduvanchik-ext:define-keysym 65421 "Numpad\-Return" "Numpad\-Enter")      ;num-pad-enter
(oduvanchik-ext:define-keysym 65455 "Numpad/")                             ;num-pad-/
(oduvanchik-ext:define-keysym 65450 "Numpad*")                             ;num-pad-*
(oduvanchik-ext:define-keysym 65453 "Numpad-")                             ;num-pad--
(oduvanchik-ext:define-keysym 65451 "Numpad+")                             ;num-pad-+
(oduvanchik-ext:define-keysym 65456 "Numpad0")                             ;num-pad-0
(oduvanchik-ext:define-keysym 65457 "Numpad1")                             ;num-pad-1
(oduvanchik-ext:define-keysym 65458 "Numpad2")                             ;num-pad-2
(oduvanchik-ext:define-keysym 65459 "Numpad3")                             ;num-pad-3
(oduvanchik-ext:define-keysym 65460 "Numpad4")                             ;num-pad-4
(oduvanchik-ext:define-keysym 65461 "Numpad5")                             ;num-pad-5
(oduvanchik-ext:define-keysym 65462 "Numpad6")                             ;num-pad-6
(oduvanchik-ext:define-keysym 65463 "Numpad7")                             ;num-pad-7
(oduvanchik-ext:define-keysym 65464 "Numpad8")                             ;num-pad-8
(oduvanchik-ext:define-keysym 65465 "Numpad9")                             ;num-pad-9
(oduvanchik-ext:define-keysym 65454 "Numpad.")                             ;num-pad-.

;;; "Named" keys.
;;;
(oduvanchik-ext:define-keysym 65289 "Tab")
(oduvanchik-ext:define-keysym 65307 "Escape" "Altmode" "Alt")              ;escape
(oduvanchik-ext:define-keysym 65288 "Backspace")                           ;backspace
(oduvanchik-ext:define-keysym 65293 "Return" "Enter")                      ;enter
#+nil
;; 65512 = #xffe8 is Meta_R for me.  As per the comment on IBM RT at the
;; to of this file, it needs to be unmapped.
(oduvanchik-ext:define-keysym 65512 "Linefeed" "Action" "Newline")         ;action
(oduvanchik-ext:define-keysym 10 "Linefeed" "Action" "Newline")            ;action
(oduvanchik-ext:define-keysym 32 "Space" " ")

;;; Letters.
;;;
(oduvanchik-ext:define-keysym 97 "a") (oduvanchik-ext:define-keysym 65 "A")
(oduvanchik-ext:define-keysym 98 "b") (oduvanchik-ext:define-keysym 66 "B")
(oduvanchik-ext:define-keysym 99 "c") (oduvanchik-ext:define-keysym 67 "C")
(oduvanchik-ext:define-keysym 100 "d") (oduvanchik-ext:define-keysym 68 "D")
(oduvanchik-ext:define-keysym 101 "e") (oduvanchik-ext:define-keysym 69 "E")
(oduvanchik-ext:define-keysym 102 "f") (oduvanchik-ext:define-keysym 70 "F")
(oduvanchik-ext:define-keysym 103 "g") (oduvanchik-ext:define-keysym 71 "G")
(oduvanchik-ext:define-keysym 104 "h") (oduvanchik-ext:define-keysym 72 "H")
(oduvanchik-ext:define-keysym 105 "i") (oduvanchik-ext:define-keysym 73 "I")
(oduvanchik-ext:define-keysym 106 "j") (oduvanchik-ext:define-keysym 74 "J")
(oduvanchik-ext:define-keysym 107 "k") (oduvanchik-ext:define-keysym 75 "K")
(oduvanchik-ext:define-keysym 108 "l") (oduvanchik-ext:define-keysym 76 "L")
(oduvanchik-ext:define-keysym 109 "m") (oduvanchik-ext:define-keysym 77 "M")
(oduvanchik-ext:define-keysym 110 "n") (oduvanchik-ext:define-keysym 78 "N")
(oduvanchik-ext:define-keysym 111 "o") (oduvanchik-ext:define-keysym 79 "O")
(oduvanchik-ext:define-keysym 112 "p") (oduvanchik-ext:define-keysym 80 "P")
(oduvanchik-ext:define-keysym 113 "q") (oduvanchik-ext:define-keysym 81 "Q")
(oduvanchik-ext:define-keysym 114 "r") (oduvanchik-ext:define-keysym 82 "R")
(oduvanchik-ext:define-keysym 115 "s") (oduvanchik-ext:define-keysym 83 "S")
(oduvanchik-ext:define-keysym 116 "t") (oduvanchik-ext:define-keysym 84 "T")
(oduvanchik-ext:define-keysym 117 "u") (oduvanchik-ext:define-keysym 85 "U")
(oduvanchik-ext:define-keysym 118 "v") (oduvanchik-ext:define-keysym 86 "V")
(oduvanchik-ext:define-keysym 119 "w") (oduvanchik-ext:define-keysym 87 "W")
(oduvanchik-ext:define-keysym 120 "x") (oduvanchik-ext:define-keysym 88 "X")
(oduvanchik-ext:define-keysym 121 "y") (oduvanchik-ext:define-keysym 89 "Y")
(oduvanchik-ext:define-keysym 122 "z") (oduvanchik-ext:define-keysym 90 "Z")

;;; Cyrillic letters

(defparameter *cyrillic-lower-min-keysym* 1728)
(defparameter *cyrillic-lower-max-keysym* (+ 1728 32 -1)) ; почему-то ё отдельно, поэтому 32.
(defparameter *cyrillic_small_letter_io-keysym* 1699)

; цифры взяты из возврата xlib:keycode->keysym при попытке ввода такой буквы в редактор
(oduvanchik-ext:define-keysym 1729 "а")
(oduvanchik-ext:define-keysym 1730 "б")
(oduvanchik-ext:define-keysym 1751 "в")
(oduvanchik-ext:define-keysym 1735 "г")
(oduvanchik-ext:define-keysym 1732 "д")
(oduvanchik-ext:define-keysym 1733 "е")
(oduvanchik-ext:define-keysym 1699 "ё")
(oduvanchik-ext:define-keysym 1750 "ж")
(oduvanchik-ext:define-keysym 1754 "з")
(oduvanchik-ext:define-keysym 1737 "и")
(oduvanchik-ext:define-keysym 1738 "й")
(oduvanchik-ext:define-keysym 1739 "к")
(oduvanchik-ext:define-keysym 1740 "л")
(oduvanchik-ext:define-keysym 1741 "м")
(oduvanchik-ext:define-keysym 1742 "н")
(oduvanchik-ext:define-keysym 1743 "о")
(oduvanchik-ext:define-keysym 1744 "п")
(oduvanchik-ext:define-keysym 1746 "р")
(oduvanchik-ext:define-keysym 1747 "с")
(oduvanchik-ext:define-keysym 1748 "т")
(oduvanchik-ext:define-keysym 1749 "у")
(oduvanchik-ext:define-keysym 1734 "ф")
(oduvanchik-ext:define-keysym 1736 "х")
(oduvanchik-ext:define-keysym 1731 "ц")
(oduvanchik-ext:define-keysym 1758 "ч")
(oduvanchik-ext:define-keysym 1755 "ш")
(oduvanchik-ext:define-keysym 1757 "щ")
(oduvanchik-ext:define-keysym 1759 "ъ")
(oduvanchik-ext:define-keysym 1753 "ы")
(oduvanchik-ext:define-keysym 1752 "ь")
(oduvanchik-ext:define-keysym 1756 "э")
(oduvanchik-ext:define-keysym 1728 "ю")
(oduvanchik-ext:define-keysym 1745 "я")




;;; Standard number keys.
;;;
(oduvanchik-ext:define-keysym 49 "1") (oduvanchik-ext:define-keysym 33 "!")
(oduvanchik-ext:define-keysym 50 "2") (oduvanchik-ext:define-keysym 64 "@")
(oduvanchik-ext:define-keysym 51 "3") (oduvanchik-ext:define-keysym 35 "#")
(oduvanchik-ext:define-keysym 52 "4") (oduvanchik-ext:define-keysym 36 "$")
(oduvanchik-ext:define-keysym 53 "5") (oduvanchik-ext:define-keysym 37 "%")
(oduvanchik-ext:define-keysym 54 "6") (oduvanchik-ext:define-keysym 94 "^")
(oduvanchik-ext:define-keysym 55 "7") (oduvanchik-ext:define-keysym 38 "&")
(oduvanchik-ext:define-keysym 56 "8") (oduvanchik-ext:define-keysym 42 "*")
(oduvanchik-ext:define-keysym 57 "9") (oduvanchik-ext:define-keysym 40 "(")
(oduvanchik-ext:define-keysym 48 "0") (oduvanchik-ext:define-keysym 41 ")")

;;; "Standard" symbol keys.
;;;
(oduvanchik-ext:define-keysym 96 "`") (oduvanchik-ext:define-keysym 126 "~")
(oduvanchik-ext:define-keysym 45 "-") (oduvanchik-ext:define-keysym 95 "_")
(oduvanchik-ext:define-keysym 61 "=") (oduvanchik-ext:define-keysym 43 "+")
(oduvanchik-ext:define-keysym 91 "[") (oduvanchik-ext:define-keysym 123 "{")
(oduvanchik-ext:define-keysym 93 "]") (oduvanchik-ext:define-keysym 125 "}")
(oduvanchik-ext:define-keysym 92 "\\") (oduvanchik-ext:define-keysym 124 "|")
(oduvanchik-ext:define-keysym 59 ";") (oduvanchik-ext:define-keysym 58 ":")
(oduvanchik-ext:define-keysym 39 "'") (oduvanchik-ext:define-keysym 34 "\"")
(oduvanchik-ext:define-keysym 44 ",") (oduvanchik-ext:define-keysym 60 "<")
(oduvanchik-ext:define-keysym 46 ".") (oduvanchik-ext:define-keysym 62 ">")
(oduvanchik-ext:define-keysym 47 "/") (oduvanchik-ext:define-keysym 63 "?")

;;; Standard Mouse keysyms.
;;;
(oduvanchik-ext::define-mouse-keysym 1 25601 "Leftdown" "Super" :button-press)
(oduvanchik-ext::define-mouse-keysym 1 25602 "Leftup" "Super" :button-release)

(oduvanchik-ext::define-mouse-keysym 2 25603 "Middledown" "Super" :button-press)
(oduvanchik-ext::define-mouse-keysym 2 25604 "Middleup" "Super" :button-release)

(oduvanchik-ext::define-mouse-keysym 3 25605 "Rightdown" "Super" :button-press)
(oduvanchik-ext::define-mouse-keysym 3 25606 "Rightup" "Super" :button-release)

;;; Sun keyboard.
;;;
(oduvanchik-ext:define-keysym 65387 "break")                       ;alternate (Sun).
;(oduvanchik-ext:define-keysym 65290 "linefeed")



;;;; SETFs of KEY-EVANT-CHAR and CHAR-KEY-EVENT.

;;; Converting ASCII control characters to Common Lisp control characters:
;;; ASCII control character codes are separated from the codes of the
;;; "non-controlified" characters by the code of atsign.  The ASCII control
;;; character codes range from ^@ (0) through ^_ (one less than the code of
;;; space).  We iterate over this range adding the ASCII code of atsign to
;;; get the "non-controlified" character code.  With each of these, we turn
;;; the code into a Common Lisp character and set its :control bit.  Certain
;;; ASCII control characters have to be translated to special Common Lisp
;;; characters outside of the loop.
;;;    With the advent of Oduvanchik running under X, and all the key bindings
;;; changing, we also downcase each Common Lisp character (where normally
;;; control characters come in upcased) in an effort to obtain normal command
;;; bindings.  Commands bound to uppercase modified characters will not be
;;; accessible to terminal interaction.
;;;
(let ((@-code (char-code #\@)))
  (dotimes (i (char-code #\space))
    (setf (oduvanchik-ext:char-key-event (code-char i))
          (oduvanchik-ext::make-key-event (string (char-downcase (code-char (+ i @-code))))
                               (oduvanchik-ext:key-event-modifier-mask "control")))))
(setf (oduvanchik-ext:char-key-event (code-char 9)) (oduvanchik-ext::make-key-event #k"Tab"))
(setf (oduvanchik-ext:char-key-event (code-char 10)) (oduvanchik-ext::make-key-event #k"Linefeed"))
(setf (oduvanchik-ext:char-key-event (code-char 13)) (oduvanchik-ext::make-key-event #k"Return"))
(setf (oduvanchik-ext:char-key-event (code-char 27)) (oduvanchik-ext::make-key-event #k"Alt"))
;;;
;;; Other ASCII codes are exactly the same as the Common Lisp codes.
;;;
(do ((i (char-code #\space) (1+ i)))
    ((= i 128)) 
  (setf (oduvanchik-ext:char-key-event (code-char i))
        (oduvanchik-ext::make-key-event (string (code-char i)))))

(defparameter *cyr-lower-chars* "абвгдеёжзийклмнопрстуфхцчшщъыьэюя")
(defparameter *cyr-upper-chars* "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ")

(defun set-char-key-event-cyr-char (c) ; ю
  (setf (oduvanchik-ext:char-key-event c)
        (oduvanchik-ext:make-key-event (string c))))

(map 'list #'set-char-key-event-cyr-char *cyr-lower-chars*)
;(map 'list #'set-char-key-event-cyr-char *cyr-upper-chars*)

;;; This makes KEY-EVENT-CHAR the inverse of CHAR-KEY-EVENT from the start.
;;; It need not be this way, but it is.
;;;
(dotimes (i 128)
  (let ((character (code-char i)))
    (setf (oduvanchik-ext::key-event-char (oduvanchik-ext:char-key-event character)) character)))

(defun set-key-event-char-cyr-char (c) ; ю
  (setf (oduvanchik-ext::key-event-char (oduvanchik-ext:char-key-event c)) c))

(map 'list #'set-key-event-char-cyr-char *cyr-lower-chars*)

;;; Since we treated these characters specially above when setting
;;; ODUVANCHIK-EXT:CHAR-KEY-EVENT above, we must set these ODUVANCHIK-EXT:KEY-EVENT-CHAR's specially
;;; to make quoting characters into Oduvanchik buffers more obvious for users.
;;;
(setf (oduvanchik-ext:key-event-char #k"C-h") #\backspace)
(setf (oduvanchik-ext:key-event-char #k"C-i") #\tab)
(setf (oduvanchik-ext:key-event-char #k"C-j") #\linefeed)
(setf (oduvanchik-ext:key-event-char #k"C-m") #\return)
