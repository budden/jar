;;;; -*- Mode: Lisp; indent-tabs-mode: nil;  system :oduvanchik.base ; -*-
;;; ---------------------------------------------------------------------------
;;;     Title: experimental syntax highlighting
;;;   Created: 2004-07-09
;;;    Author: Gilbert Baumann <gilbert@base-electronic.de>
;;;       $Id: exp-syntax.lisp,v 1.1 2004-07-09 15:16:14 gbaumann Exp $
;;; ---------------------------------------------------------------------------
;;;  (c) copyright 2004 by Gilbert Baumann

(in-package :oi)

(defstate initial ()
  (while t
    (call sexp)))

(defstate comment ()
  loop
  (cond ((char= ch #\newline)
         (consume)
         (return))
        (t
         (consume)
         (go loop))))

(defstate hashed-comment ()
  (consume)
  loop
  (cond ((char= ch #\|)
         (consume)
         (cond ((char= ch #\#)
                (consume)
                (return))
               (t
                (go loop))))
        (t
         (consume)
         (go loop))))

(defstate bq ()
  (consume)                             ;consume `
  (call sexp))

(defstate rq ()
  (consume)                             ;consume '
  (call sexp))

(defstate uq ()
  (consume)                             ;consume `
  (call sexp))

(defstate sexp ()
  loop
  (call skip-white*)                    ;skip possible white space and comments
  (cond ((char= ch #\() (call list))
        ((char= ch #\`) (call bq))
        ((char= ch #\') (call rq))
        ((char= ch #\,) (call uq))
        ((char= ch #\;) (call comment))
        ((char= ch #\") (call string))
        ((char= ch #\#) (call hash))
        ((char= ch #\|) (call atom))
        ((sbcl-reader-budden-tools:constituentp ch *readtable*)
         (call atom))
        (t
         ;; hmm
         (consume)
         (go loop))))

(defstate hash ()
  (consume)
  (cond ((char= ch #\\) (call char-const))
        ((char= ch #\+) (call hash-plus))
        ((char= ch #\-) (call hash-minus))
        ((char= ch #\')
         (consume)
         (call sexp))
        ((char= ch #\|) (call hashed-comment))
        (t
         (call sexp))))

(defstate char-const ()
  (consume)                             ;\\
  (cond ((or (alphanumericp ch) (find ch "-+*/"))
         (call atom))
        (t
         (consume))))

(defstate string ()
  (consume)
  (while t
    (cond ((char= ch #\\)
           (consume)
           (consume))
          ((char= ch #\")
           (consume)
           (return))
          (t
           (consume)))))

(defstate escaped-char-in-atom ()
  (consume)
  (consume))

(defstate rest-of-barred-atom ()
  (while t
         (cond
          ((char= ch #\\)
           (call escaped-char-in-atom))
          ((char= ch #\|)
           (consume)
           (return))
          (t
           (consume))
           )))

(defstate atom ()
  (cond
   ((char= ch #\|)
    (consume)
    (call rest-of-barred-atom))
   (t
    (while t
           (cond
            ((sbcl-reader-budden-tools:constituentp ch *readtable*)
             (consume))
            ((char= ch #\\)
             (call escaped-char-in-atom))
            (t
             (return)))))))

(defstate list ()
  (consume)     ;consume open-paren 
  (call list-inside) 
  (consume)     ;consume closing-paren
  )

(defstate list-inside ()
  (while t
    (call skip-white*)                  ;skip possible white space
    (cond ((char= ch #\))
           (return))
          (t
           (call sexp)))))

(defstate skip-white* ()
  loop
  (while (member ch '(#\space #\tab #\newline #\return #\page))
    (consume))
  (cond ((char= ch #\;)
         (call comment)
         (go loop))
        (t
         (return))))

(defstate hash-plus ()
  (consume)                             ;#\+
  (call sexp)                                ;cond
  (call sexp)                                ;form
  )

(defstate hash-minus ()
  (consume)                             ;#\-
  (call sexp)                                ;cond
  (call sexp)                                ;form
  )


(defmacro cache-scanner (regex &rest more-args)
  ;; help compilers that don't support compiler macros
  `(load-time-value (ppcre:create-scanner ,regex ,@more-args)))

