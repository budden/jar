;;;; -*- Mode: Lisp; indent-tabs-mode: nil;  system :oduvanchik.base ; -*-
;;; (C) Denis Budyak 2015
;;; Tcl mode
;;;
(in-package :oduvanchik)
(named-readtables::in-readtable :oduvanchik-ext-readtable)

(defmode "Tcl" :major-p t)
(defcommand "Tcl Mode" (p)
  "Put the current buffer into \"Tcl\" mode."
  "Put the current buffer into \"Tcl\" mode."
  (declare (ignore p))
  (setf (buffer-major-mode (current-buffer)) "Tcl"))

(defhvar "Indent Function"
  "Indentation function which is invoked by \"Indent\" command.
   It must take one argument that is the prefix argument."
  :value #'generic-indent
  :mode "Tcl")

(defhvar "Auto Fill Space Indent"
  "When non-nil, uses \"Indent New Comment Line\" to break lines instead of
   \"New Line\"."
  :mode "Tcl" :value t)

(defhvar "Comment Start"
  "String that indicates the start of a comment."
  :mode "Tcl" :value "#")

(defhvar "Comment End"
  "String that ends comments.  Nil indicates #\newline termination."
  :mode "Tcl" :value nil)

(defhvar "Comment Begin"
  "String that is inserted to begin a comment."
  :mode "Tcl" :value "#")

; (shadow-attribute :scribe-syntax #\< nil "Tcl")

(defhvar "Compile And Load Buffer File Function" ; budden's compile-and-load-buffer-file-function
  "Function's arglist is (filename load-p &rest options), options are passed to compiler function, return value is discarded"
  :value nil ; Позже будет установлена в 'clco::compile-tcl-file-for-tcl
  :mode "Tcl")

(defhvar "Recompute Syntax Marks Function"
  "См. oi::old-lisp-recompute-syntax-marks, РАСКРАСКА-ЯРА-ЛЕКСЕРОМ:Recompute-syntax-marks--специально-для-Яра" 
  :value 'oi::dummy-recompute-syntax-marks
  :mode "Tcl"
  )
