;;;; -*- Mode: Lisp; indent-tabs-mode: nil;  system :oduvanchik.base ; coding: utf-8; -*-
;;; ---------------------------------------------------------------------------
;;;     Title: Prelimiary Undo
;;;   Created: 2004-12-26
;;;    Author: Gilbert Baumann <gilbert@base-engineering.com>
;;;   License: MIT style (see below)
;;; ---------------------------------------------------------------------------
;;;  (c) copyright 2004 by Gilbert Baumann

;;;  Permission is hereby granted, free of charge, to any person obtaining
;;;  a copy of this software and associated documentation files (the
;;;  "Software"), to deal in the Software without restriction, including
;;;  without limitation the rights to use, copy, modify, merge, publish,
;;;  distribute, sublicense, and/or sell copies of the Software, and to
;;;  permit persons to whom the Software is furnished to do so, subject to
;;;  the following conditions:
;;;
;;;  The above copyright notice and this permission notice shall be
;;;  included in all copies or substantial portions of the Software.
;;;
;;;  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
;;;  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
;;;  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
;;;  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

;;;; TODO

;; - Some form of consolidation
;; - Redo
;; - Ensure that what we indentified as a protocol of functions
;;   modifying a buffer is not violated. Both by runtime and compile
;;   time measures.
;; - Look and Feel:
;;   Find out what different variants of undo are implemented both
;;   with gnu emacs and xemacs, try to simulate those.
;; - Q: Are there any commands that modify more than one buffer?
;;   (Besides the kill ring and stuff)
;; - FILTER-REGION (function region) is missing
;; - (SETF NEXT-CHARACTER) (character mark) is missing
;; - Don't record undo information of functions that don't modify the
;;   buffer at all.

;; There is this MODIFYING-BUFFER which should enable access to
;; LINE-NEXT and LINE-PREVIOUS.

;; Instead of putting :command entires onto an undo list, do
;; undo-chunks directly.

;; Q: Do we need a separate Redo command or should we go the route
;;    that XEmacs follows and make any other command than undo change
;;    the current undo sequence and let subsequent undos really redo
;;    stuff? This is useful but awkward.

;; Also: I really want the self-insert-commands be grouped by words
;; and not just by 20 characters as the [documented] behaviour of
;; XEmacs. This also is the observed behavior.

(in-package :oduvanchik-internals)

;; Unfortunately we need numeric buffer positions. (Hmm, maybe after
;; all RMS has a point?) Anyhow to graft this onto oduvanchik we define
;; two functions MARK-POSITION and POSTION-MARK to convert and back
;; and fro. Further these new kind of buffer positions are passed
;; around as (buffer line-number character-position) triples.

(defun mark-position (mark)
  (let ((line-no 0)
        (line (mark-line mark)))
    (do ()
        ((null (line-previous line)))
      (incf line-no)
      (setf line (line-previous line)))
    (list (line-buffer (mark-line mark))
          line-no (mark-charpos mark))))

(defun position-mark (buffer line-no char-pos)
  (let ((line (mark-line (buffer-start-mark buffer))))
    (assert line)
    (dotimes (i line-no)
      (let ((next (line-next line)))
        (unless next
          (error "Corrupted undo position, line ~D column ~D"
                 line-no char-pos))
        (setf line next)))
    (mark line char-pos)))

;;;; Insertion

(defun update-tag-line-number (line)
  "Говорим о том, что строка, в которой находится данная метка, требует перевычисления (и всё, что ниже по течению, ессно)"
  (let* ((buffer (line-buffer line))
         (|Текущая-граница-раскрашенного| (buffer-poslednaya-str-s-tegom buffer))
         )
    (cond
     ((eq (syntax-highlight-mode buffer)
          :send-highlight-after-recomputing-entire-buffer)
      (recompute-line-tags-light buffer))
     ((null |Текущая-граница-раскрашенного|)
      (ПЕРЕРАСКРАСИТЬ-БУФЕР line))
     ((line<= line |Текущая-граница-раскрашенного|)
      (let ((prev (line-previous line)))
        (cond
         (prev (setf (buffer-poslednaya-str-s-tegom buffer) prev))
         (t (setf (buffer-poslednaya-str-s-tegom buffer) nil))))
      ;(format t "~&Сбрасываю раскраску по ~S~%" line)
      (ПЕРЕРАСКРАСИТЬ-БУФЕР line) ; неважны нюансы, первая это строчка или нет, т.е. ПЕРЕРАСКРАСИТЬ-БУФЕР сама найдёт что надо
      ))))

;;; Insert can call itself and other functions.  We only want to recorder
;;; the outermost call.
(defvar *insert-noted-p* nil)

(defun deal-with-highlight-before-insert-command (mark buffer)
  (when buffer ; мы можем вставлять в *kill-ring*
    (update-tag-line-number (mark-line mark))))

(defun deal-with-highlight-after-insert-command (mark buffer)
  (when buffer
    (check-something-ok mark)
    (update-tag-line-number (mark-line mark))))
  

(defmethod insert-character :around (mark character)
  (modifying-buffer
   nil ; только блокируем мьютекс. внутри будут вызовы с конкретным буфером
   (when *do-editing-on-tcl-side*
     (insert-character-with-clcon_text mark character))
   (let ((buffer (line-buffer (mark-line mark)))
         (*do-editing-on-tcl-side* nil))
     (deal-with-highlight-before-insert-command mark buffer)
     (call-next-method)
     (deal-with-highlight-after-insert-command mark buffer)
     )))

(defmethod insert-string :around (mark string &optional (start 0) (end (length string)))
  (modifying-buffer
   nil ; только блокируем мьютекс. внутри будут вызовы с конкретным буфером
   (when *do-editing-on-tcl-side*
     (insert-string-with-clcon_text mark string start end))
   (check-something-ok mark)
   (let ((*do-editing-on-tcl-side* nil)
         (buffer (line-buffer (mark-line mark))))
     (deal-with-highlight-before-insert-command mark buffer)
     (call-next-method)
     (deal-with-highlight-after-insert-command mark buffer)
     )))

(defmethod insert-region :around (mark region)
  (modifying-buffer
   nil ; только блокируем мьютекс. внутри будут вызовы с конкретным буфером
   (when *do-editing-on-tcl-side*
     (insert-region-with-clcon_text mark region))
   (let ((*do-editing-on-tcl-side* nil)
         (buffer (line-buffer (mark-line mark))))
     (deal-with-highlight-before-insert-command mark buffer)
     (call-next-method)
     (deal-with-highlight-after-insert-command mark buffer)
     )))

(defmethod ninsert-region :around (mark region)
  ;; the "n" refers to the region argument.
  ;(assert (beyond-bordeaux-threads:holding-lock-p *invoke-modifying-buffer-lock*))
  (modifying-buffer
   nil ; только блокируем мьютекс. внутри будут вызовы с конкретным буфером
   (when *do-editing-on-tcl-side*
     (ninsert-region-with-clcon_text mark region))
   (let ((buffer (line-buffer (mark-line mark)))
         (*do-editing-on-tcl-side* nil))
     (deal-with-highlight-before-insert-command mark buffer)
     (call-next-method)
     (deal-with-highlight-after-insert-command mark buffer)
     )))

;;;; Deletion

;; We make delete-characters and delete-region both call off to
;; delete-and-save-region which is the most general method and has the
;; benefit to return the deleted stuff.

(defun СБРОСИТЬ-ТЕГИ-В-СОСЕДНИХ-СТРОКАХ-ВОКРУГ-МАРКИ (mark)
  "Sometimes we lose font marks in clcon when newline at the start of line is deleted. To remedy this, we will clean up syntax info from the NEXT line relative to line where we delete. Returns t if something is done (just for debugging)"
  (let* (line prev next)
    (setf line (mark-line mark))
    (when line
      (setf next (line-next line))
      (when next
        (update-tag-line-number next))
      (setf prev (line-previous line))
      (when prev
        (update-tag-line-number prev)))))


(defun deal-with-highlight-before-deletion (buffer mark)
  "Args are intentionnaly incompatible to insert case"
  (when buffer
    (update-tag-line-number (mark-line mark)) ; мы не сможем удалить таким способом ту строчку, к-рая яявляется poslednaya-str-s-tegom, т.к. пометка перенесётся
    (СБРОСИТЬ-ТЕГИ-В-СОСЕДНИХ-СТРОКАХ-ВОКРУГ-МАРКИ mark)
    (check-something-ok mark)))


(defun deal-with-highlight-after-deletion (buffer mark)
  (when buffer
      (update-tag-line-number (mark-line mark)) 
      (СБРОСИТЬ-ТЕГИ-В-СОСЕДНИХ-СТРОКАХ-ВОКРУГ-МАРКИ mark))
  (check-something-ok mark)
  ;(when buffer
  ;  (let ((start-mark (oi::buffer-start-mark buffer)))
  ;    (when start-mark
  ;      (update-tag-line-number (mark-line start-mark)))))
  )


(defmethod delete-characters :around (mark &optional (n 1))
  ; (assert (beyond-bordeaux-threads:holding-lock-p *invoke-modifying-buffer-lock*))
  (modifying-buffer
   nil ; только блокируем мьютекс. внутри будут вызовы с конкретным буфером
   (when *do-editing-on-tcl-side*
     (delete-characters-with-clcon_text mark n))
   (let ((*do-editing-on-tcl-side* nil)
         (buffer (line-buffer (mark-line mark))))
     (deal-with-highlight-before-deletion buffer mark)
     (call-next-method)
     (deal-with-highlight-after-deletion buffer mark)
     )))
           
(defmethod delete-region :around (region)
  ;(assert (beyond-bordeaux-threads:holding-lock-p *invoke-modifying-buffer-lock*))
  (modifying-buffer
   nil ; только блокируем мьютекс. внутри будут вызовы с конкретным буфером
   (when *do-editing-on-tcl-side*
     (delete-region-with-clcon_text region))
   (let* ((*do-editing-on-tcl-side* nil)
          (mark (region-start region))
          (buffer (line-buffer (mark-line mark))))
     (deal-with-highlight-before-deletion buffer mark)
     (call-next-method)
     (deal-with-highlight-after-deletion buffer mark)
     )))


(defmethod delete-and-save-region :around (region)
  ;(assert (beyond-bordeaux-threads:holding-lock-p *invoke-modifying-buffer-lock*))
  (modifying-buffer
   nil ; только блокируем мьютекс. внутри будут вызовы с конкретным буфером
   (when *do-editing-on-tcl-side*
     (delete-and-save-region-with-clcon_text region))
   (let* ((*do-editing-on-tcl-side* nil)
          (mark (region-start region))
          (buffer (line-buffer (mark-line mark))))
     (deal-with-highlight-before-deletion buffer mark)
     (let ((result (call-next-method)))
       (deal-with-highlight-after-deletion buffer mark)
       result
     ))))

;;;;

(defvar last-was-undo-p nil)
(defvar this-is-undo-p nil)
(defvar undoing-undo-list nil)

(defparameter *invoke-hook* #'(lambda (command p)
                                ; (new-undo-invoke-hook command p)
                                (funcall (command-function command) p)
                                (setf last-was-undo-p this-is-undo-p))
  "This function is called by the command interpreter when it wants to invoke a
  command.  The arguments are the command to invoke and the prefix argument.
  The default value just calls the Command-Function with the prefix argument.")


(defcommand "Undo Mode" (p)
  "Enable the recording of Undo information in the current buffer."
  "Enable the recording of Undo information in the current buffer."
  (let* ((buffer (current-buffer))
         (p (if p (plusp p) (not (buffer-undo-p buffer)))))
    (setf (buffer-undo-p buffer) p)
    (setf (buffer-undo-list buffer) nil)
    (if p
        (message "Undo enabled")
        (message "Undo disabled"))))

