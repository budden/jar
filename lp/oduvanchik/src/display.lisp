;;;; -*- Mode: Lisp; indent-tabs-mode: nil;  system :oduvanchik.base ; -*-
;;;
;;; **********************************************************************
;;; This code was written as part of the CMU Common Lisp project at
;;; Carnegie Mellon University, and has been placed in the public domain.
;;;
;;;
;;; **********************************************************************
;;;
;;; Written by Bill Chiles.
;;;
;;; This is the device independent redisplay entry points for Oduvanchik.
;;;

(in-package :oduvanchik-internals)

(declaim (special *in-the-editor*)) ; defined in main.lisp --amb

;;;; Main redisplay entry points.

(defvar *things-to-do-once* ()
  "This is a list of lists of functions and args to be applied to.  The
  functions are called with args supplied at the top of the command loop.")

(defvar *screen-image-trashed* ()
  "This variable is set to true if the screen has been trashed by some screen
   manager operation, and thus should be totally refreshed.  This is currently
   only used by tty redisplay.")

;;; True if we are in redisplay, and thus don't want to enter it recursively.
;;;
(defvar *in-redisplay* nil)

(declaim (special *window-list*))

;;; REDISPLAY-LOOP -- Internal.
;;;
;;; This executes internal redisplay routines on all windows interleaved with
;;; checking for input, and if any input shows up we punt returning
;;; :editor-input.  Special-fun is for windows that the redisplay interface
;;; wants to recenter to keep the window's buffer's point visible.  General-fun
;;; is for other windows.
;;;
;;; Whenever we invoke one of the internal routines, we keep track of the
;;; non-nil return values, so we can return t when we are done.  Returning t
;;; means redisplay should run again to make sure it converged.  To err on the
;;; safe side, if any window had any changed lines, then let's go through
;;; redisplay again; that is, return t.
;;;
;;; After checking each window, we put the cursor in the appropriate place and
;;; force output.  When we try to position the cursor, it may no longer lie
;;; within the window due to buffer modifications during redisplay.  If it is
;;; out of the window, return t to indicate we need to finish redisplaying.
;;;
;;; Then we check for the after-redisplay method.  Routines such as REDISPLAY
;;; and REDISPLAY-ALL want to invoke the after method to make sure we handle
;;; any events generated from redisplaying.  There wouldn't be a problem with
;;; handling these events if we were going in and out of Oduvanchik's event
;;; handling, but some user may loop over one of these interface functions for
;;; a long time without going through Oduvanchik's input loop; when that happens,
;;; each call to redisplay may not result in a complete redisplay of the
;;; device.  Routines such as INTERNAL-REDISPLAY don't want to worry about this
;;; since Oduvanchik calls them while going in and out of the input/event-handling
;;; loop.
;;;
;;; Around all of this, we establish the 'redisplay-catcher tag.  Some device
;;; redisplay methods throw to this to abort redisplay in addition to this
;;; code.
;;;
(defun redisplay-loop (general-fun special-fun &optional (afterp t))
  ; остатки от отсутствия oduvan-invisible
  (declare (ignore general-fun special-fun afterp))
  nil
  )

;;; REDISPLAY -- Public.
;;;
;;; This function updates the display of all windows which need it.  It assumes
;;; it's internal representation of the screen is accurate and attempts to do
;;; the minimal amount of output to bring the screen into correspondence.
;;; *screen-image-trashed* is only used by terminal redisplay.
;;;
(defun redisplay ()
  "The main entry into redisplay; updates any windows that seem to need it."
  (when *things-to-do-once*
    (dolist (thing *things-to-do-once*) (apply (car thing) (cdr thing)))
    (setf *things-to-do-once* nil))
  (cond (*in-redisplay* t)
        (*screen-image-trashed*
         (when (eq (redisplay-all) t)
           (setf *screen-image-trashed* nil)
           t))
        (t
         (redisplay-loop #'redisplay-window #'redisplay-window-recentering))))


;;; REDISPLAY-ALL -- Public.
;;;
;;; Update the screen making no assumptions about its correctness.  This is
;;; useful if the screen gets trashed, or redisplay gets lost.  Since windows
;;; may be on different devices, we have to go through the list clearing all
;;; possible devices.  Always returns T or :EDITOR-INPUT, never NIL.
;;;
(defun redisplay-all ()
  "An entry into redisplay; causes all windows to be fully refreshed."
  (let ((cleared-devices nil))
    (dolist (w *window-list*)
      (let* ((hunk (window-hunk w))
             (device (device-hunk-device hunk)))
        (unless (member device cleared-devices :test #'eq)
          (device-clear device)
          ;;
          ;; It's cleared whether we did clear it or there was no method.
          (push device cleared-devices)))))
  (redisplay-loop
   #'redisplay-window-all
   #'(lambda (window)
       (setf (window-tick window) (tick))
       (update-window-image window)
       (maybe-recenter-window window)
       (device-dumb-redisplay (device-hunk-device (window-hunk window))
                              window)
       t)))



;;;; Internal redisplay entry points.

(defun internal-redisplay ()
  "The main internal entry into redisplay.  This is just like REDISPLAY, but it
   doesn't call the device's after-redisplay method.
   Budden: it seems that returning t means we need call some redisplaying once more.
   "
  (when *things-to-do-once*
    (dolist (thing *things-to-do-once*) (apply (car thing) (cdr thing)))
    (setf *things-to-do-once* nil))
  (cond (*in-redisplay*
         t)
        (*screen-image-trashed*
         (when (eq (redisplay-all) t)
           (setf *screen-image-trashed* nil)
           t))
        (t
         (redisplay-loop #'redisplay-window #'redisplay-window-recentering))))

;;; REDISPLAY-WINDOWS-FROM-MARK -- Internal Interface.
;;;
;;; oduvanchik-output-stream methods call this to update the screen.  It only
;;; redisplays windows which are displaying the buffer concerned and doesn't
;;; deal with making the cursor track the point.  *screen-image-trashed* is
;;; only used by terminal redisplay.  This must call the device after-redisplay
;;; method since stream output may occur without ever returning to the
;;; Oduvanchik input/event-handling loop.
;;;
(defun redisplay-windows-from-mark (mark)
  (when *things-to-do-once*
    (dolist (thing *things-to-do-once*) (apply (car thing) (cdr thing)))
    (setf *things-to-do-once* nil))
  (cond ((or *in-redisplay* (not *in-the-editor*)) t)
        ((listen-editor-input *editor-input*) :editor-input)
        (*screen-image-trashed*
         (when (eq (redisplay-all) t)
           (setf *screen-image-trashed* nil)
           t))
        (t
         (let ((*in-redisplay* t))
           (catch 'redisplay-catcher
             (let ((buffer (line-buffer (mark-line mark))))
               (when buffer
                 (flet ((frob (win)
                          (let* ((device (device-hunk-device
                                          (window-hunk win))))
                            (device-force-output device)
                            (device-after-redisplay device))))
                   (let ((windows (buffer-windows buffer)))
                     (when (member *current-window* windows :test #'eq)
                       (redisplay-window-recentering *current-window*)
                       (frob *current-window*))
                     (dolist (window windows)
                       (unless (eq window *current-window*)
                         (redisplay-window window)
                         (frob window))))))))))))

;;; REDISPLAY-WINDOW -- Internal.
;;;
;;; Return t if there are any changed lines, nil otherwise.
;;;
(defun redisplay-window (window)
  "Maybe updates the window's image and calls the device's smart redisplay
   method.  NOTE: the smart redisplay method may throw to
   'hi::redisplay-catcher to abort redisplay."
  (maybe-update-window-image window)
  (prog1
      (not (eq (window-first-changed window) the-sentinel))
    (device-smart-redisplay (device-hunk-device (window-hunk window)) window)))

(defun redisplay-window-all (window)
  "Updates the window's image and calls the device's dumb redisplay method."
  (setf (window-tick window) (tick))
  (update-window-image window)
  (device-dumb-redisplay (device-hunk-device (window-hunk window)) window)
  t)

(defun random-typeout-redisplay (window)
  (catch 'redisplay-catcher
    (maybe-update-window-image window)
    (let* ((device (device-hunk-device (window-hunk window))))
      (device-smart-redisplay device window)
      (device-force-output device))))


;;;; Support for redisplay entry points.

;;; REDISPLAY-WINDOW-RECENTERING -- Internal.
;;;
;;; This tries to be clever about updating the window image unnecessarily,
;;; recenters the window if the window's buffer's point moved off the window,
;;; and does a smart redisplay.  We call the redisplay method even if we didn't
;;; update the image or recenter because someone else may have modified the
;;; window's image and already have updated it; if nothing happened, then the
;;; smart method shouldn't do anything anyway.  NOTE: the smart redisplay
;;; method may throw to 'hi::redisplay-catcher to abort redisplay.
;;;
;;; This return t if there are any changed lines, nil otherwise.
;;;
(defun redisplay-window-recentering (window)
  (setup-for-recentering-redisplay window)
  (invoke-hook oduvanchik::redisplay-hook window)
  (setup-for-recentering-redisplay window)
  (prog1
      (not (eq (window-first-changed window) the-sentinel))
    (device-smart-redisplay (device-hunk-device (window-hunk window)) window)))

(defun setup-for-recentering-redisplay (window)
  (let* ((display-start (window-display-start window))
         (old-start (window-old-start window)))
    ;;
    ;; If the start is in the middle of a line and it wasn't before,
    ;; then move the start there.
    (when (and (same-line-p display-start old-start)
               (not (start-line-p display-start))
               (start-line-p old-start))
      (line-start display-start))
    (maybe-update-window-image window)
    (maybe-recenter-window window)))


;;; MAYBE-UPDATE-WINDOW-IMAGE only updates if the text has changed or the
;;; display start.
;;;
(defun maybe-update-window-image (window)
  (when (or (> (buffer-modified-tick (window-buffer window))
               (window-tick window))
            (mark/= (window-display-start window)
                    (window-old-start window)))
    (setf (window-tick window) (tick))
    (update-window-image window)
    t))


;;; prepare-window-for-redisplay  --  Internal
;;;
;;;    Called by make-window to do whatever redisplay wants to set up
;;; a new window.
;;;
(defun prepare-window-for-redisplay (window)
  (setf (window-old-lines window) 0))
