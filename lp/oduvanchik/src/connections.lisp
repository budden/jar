;;;; -*- Mode: Lisp; indent-tabs-mode: nil;  system :oduvanchik.base ; -*-

(in-package :oi)

(defvar *all-connections* nil)

(defun list-all-connections ()
  (copy-seq *all-connections*))

(defvar *event-base*)

(defun dispatch-events ()
  (dispatch-events-with-backend *connection-backend*))

(defun dispatch-events-no-hang ()
  (dispatch-events-no-hang-with-backend *connection-backend*))


;;;;
;;;; CONNECTION
;;;;

(defparameter +input-buffer-size+ #x2000)

(defclass connection ()
  ((name :initarg :name
         :accessor connection-name)
   (buffer :initarg :buffer
           :initform nil
           :accessor connection-buffer)
   (stream :initarg :stream
           :initform nil
           :accessor connection-stream)
   (connection-sentinel :initarg :sentinel
                        :initform nil
                        :accessor connection-sentinel)))

(defmethod print-object ((instance connection) stream)
  (print-unreadable-object (instance stream :identity nil :type t)
    (format stream "~A" (connection-name instance))))

(defun make-buffer-with-unique-name (name &rest keys)
  (or (apply #'make-buffer name keys)
      (iter:iter
       (iter:for i from 2)
       (let ((buffer (apply #'make-buffer
                            (format nil "~A<~D>" name i)
                            keys)))
         (when buffer
           (return buffer))))))

(defmethod initialize-instance :after
    ((instance connection) &key buffer)
  (flet ((delete-hook (buffer)
           (when (eq buffer (connection-buffer instance))
             (setf (connection-buffer instance) nil))))
    (typecase buffer
      ((eql t)
       (setf (connection-buffer instance)
             (make-buffer-with-unique-name
              ;; note the space in the buffer name
              (format nil " *Connection ~A*" (connection-name instance))
              :delete-hook (list #'delete-hook))))
      (hi::buffer
       (push #'delete-hook (buffer-delete-hook buffer)))
      (null)
      (t
       (error "expected NIL, T, or a buffer, but found ~A" buffer))))
  (setf (connection-name instance)
        (unique-connection-name (connection-name instance)))
  (push instance *all-connections*))

(defun unique-connection-name (base)
  (let ((name base))
    (iter:iter (iter:for i from 1)
               (iter:while (find name
                                 *all-connections*
                                 :test #'equal
                                 :key #'connection-name))
               (setf name (format nil "~A<~D>" base i)))
    name))

(defun delete-connection-buffer (connection)
  (when (connection-buffer connection)
    (delete-buffer (connection-buffer connection))
    (setf (connection-buffer connection) nil)))

(defmethod delete-connection ((connection connection))
  (delete-connection-buffer connection)
  (setf *all-connections* (remove connection *all-connections*)))

(defun connection-note-event (connection event)
  (let ((sentinel (connection-sentinel connection)))
    (when sentinel
      (funcall sentinel connection event))))


;;;;
;;;; IO-CONNECTION
;;;;

(defclass io-connection (connection)
  ((connection-filter :initarg :filter
                      :initform nil
                      :accessor connection-filter)
   (input-buffer :initform (make-array +input-buffer-size+
                                       :element-type '(unsigned-byte 8))
                 :accessor connection-input-buffer)
   (encoding :initform :utf-8
             :initarg :encoding
             :accessor connection-encoding)))

(defmethod initialize-instance :after
    ((instance io-connection) &key)
  (let ((enc (connection-encoding instance)))
    (when (symbolp enc)
      (setf (connection-encoding instance)
            (babel-encodings:get-character-encoding enc)))))

(defun filter-connection-output (connection data)
  (etypecase data
    (string
     (babel:string-to-octets data :encoding (connection-encoding connection)))
    ((array (unsigned-byte 8) (*))
     data)))

(defun note-connected (connection)
  (connection-note-event connection :connected))

(defun format-to-connection-buffer-or-stream (connection fmt &rest args)
  (let ((buffer (connection-buffer connection))
        (stream (connection-stream connection)))
    (when buffer
      (with-writable-buffer (buffer)
        (insert-string (buffer-point buffer)
                       (apply #'format nil fmt args))))
    (when stream
      (apply #'format stream fmt args))))

(defun insert-into-connection-buffer-or-stream (connection str)
  (let ((buffer (connection-buffer connection))
        (stream (connection-stream connection)))
    (when buffer
      (with-writable-buffer (buffer)
        (insert-string (buffer-point buffer) str)))
    (when stream
      (write-string str stream)
      (finish-output stream))))

(defun note-disconnected (connection)
  (connection-note-event connection :disconnected)
  (format-to-connection-buffer-or-stream connection
                                        "~&* Connection ~S disconnected."
                                        connection))

(defun note-error (connection)
  (connection-note-event connection :error)
  (format-to-connection-buffer-or-stream
   connection
   "~&* Error on connection ~S."
   connection))

(defun filter-incoming-data (connection bytes)
  (funcall (or (connection-filter connection) #'default-filter)
           connection
           bytes))

(defun default-filter (connection bytes)
  ;; fixme: what about multibyte characters that got split between two
  ;; input events data?
  (babel:octets-to-string bytes :encoding (connection-encoding connection)))

;;;;
;;;; PROCESS-CONNECTION-MIXIN
;;;;

(defclass process-connection-mixin ()
  ((command :initarg :command
            :accessor connection-command)
   (exit-code :initform nil
              :initarg :exit-code
              :accessor connection-exit-code)
   (exit-status :initform nil
                :initarg :exit-status
                :accessor connection-exit-status)
   (slave-pty-name :initform nil
                   :initarg :slave-pty-name
                   :accessor connection-slave-pty-name)
   (directory :initform nil
              :initarg :directory
              :accessor connection-directory)))

(defmethod class-for
    ((backend (eql :iolib)) (type (eql 'process-connection-mixin)))
  'process-connection/iolib)

(defmethod class-for
    ((backend (eql :qt)) (type (eql 'process-connection-mixin)))
  'process-connection/qt)

(defun make-process-connection
       (command
        &rest args
        &key name buffer stream filter sentinel slave-pty-name directory)
  (declare (ignore buffer stream filter sentinel slave-pty-name directory))
  (apply #'make-instance
         (class-for *connection-backend* 'process-connection-mixin)
         :name (or name (princ-to-string command))
         :command command
         args))

;;;;
;;;; PIPELIKE-CONNECTION
;;;;

(defclass pipelike-connection-mixin ()
  ())

(defclass process-with-pty-connection-mixin (pipelike-connection-mixin)
  ((process-connection :initarg :process-connection
                       :accessor connection-process-connection)))

(macrolet ((defproxy (name)
             `(defmethod ,name ((connection process-with-pty-connection-mixin))
                (,name (connection-process-connection connection)))))
  (defproxy connection-command)
  (defproxy connection-exit-code)
  (defproxy connection-exit-status))

(defmethod delete-connection
    :before
    ((connection process-with-pty-connection-mixin))
  (delete-connection (connection-process-connection connection)))

(defmethod class-for
    ((backend (eql :iolib)) (type (eql 'process-with-pty-connection-mixin)))
  'process-with-pty-connection/iolib)

(defmethod class-for
    ((backend (eql :qt)) (type (eql 'process-with-pty-connection-mixin)))
  'process-with-pty-connection/qt)

(defmethod class-for
    ((backend (eql :iolib)) (type (eql 'pipelike-connection-mixin)))
  'pipelike-connection/iolib)

(defmethod class-for
    ((backend (eql :qt)) (type (eql 'pipelike-connection-mixin)))
  'pipelike-connection/qt)

(defun make-pipelike-connection
    (read-fd
     write-fd
     &rest args
     &key name buffer stream filter sentinel process-connection encoding)
  (declare (ignore buffer stream sentinel))
  (apply #'make-instance
         (class-for *connection-backend*
                    (if process-connection
                        'process-with-pty-connection-mixin
                        'pipelike-connection-mixin))
         :read-fd read-fd
         :write-fd write-fd
         :name (or name (format nil "descriptor ~D/~D" read-fd write-fd))
         :filter (or filter
                     (lambda (connection bytes)
                       (default-filter connection bytes)))
         :encoding (or encoding :utf-8)
         args))

(defgeneric stream-fd (stream))
(defmethod stream-fd (stream) stream)

#+sbcl
(defmethod stream-fd ((stream sb-sys:fd-stream))
  (sb-sys:fd-stream-fd stream))

#+cmu
(defmethod stream-fd ((stream system:fd-stream))
  (system:fd-stream-fd stream))

#+scl
(defmethod stream-fd ((stream stream))
  (system:fd-stream-fd stream))

#+openmcl
(defmethod stream-fd ((stream ccl::basic-stream))
  (ccl::ioblock-device (ccl::stream-ioblock stream t)))
#+openmcl
(defmethod stream-fd ((stream ccl::fd-stream))
  (ccl::ioblock-device (ccl::stream-ioblock stream t)))

#+clisp
(defmethod stream-fd ((stream stream))
  ;; sockets appear to be direct instances of STREAM
  (ignore-errors (socket:stream-handles stream)))

#+allegro
(defmethod stream-fd ((stream stream))
  (slot-value stream 'excl::input-handle))

(defmethod stream-fd ((stream integer))
  stream)


;;;;
;;;; LISTENING-CONNECTION
;;;;

(defclass listening-connection (connection)
  ((server :initarg :server
           :initform nil
           :accessor connection-server)
   (acceptor :initarg :acceptor
             :initform nil
             :accessor connection-acceptor)
   (initargs :initarg :initargs
             :initform nil
             :accessor connection-initargs)))

(defgeneric convert-pending-connection (listener))

(defun process-incoming-connection (listener)
  (let ((connection (convert-pending-connection listener)))
    (format-to-connection-buffer-or-stream
     connection "~&* ~A." connection)
    (when (connection-acceptor listener)
      (funcall (connection-acceptor listener) connection))))


