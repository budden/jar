;;;; -*- Mode: Lisp; indent-tabs-mode: nil;  system :oduvanchik.base ; -*-
;;; ---------------------------------------------------------------------------
;;;     Title: experimental syntax highlighting
;;;   Created: 2004-07-09
;;;    Author: Gilbert Baumann <gilbert@base-electronic.de>
;;;       $Id: exp-syntax.lisp,v 1.1 2004-07-09 15:16:14 gbaumann Exp $
;;; ---------------------------------------------------------------------------
;;;  (c) copyright 2004 by Gilbert Baumann

(in-package :oi)

;;;; ------------------------------------------------------------------------------------------
;;;; Syntax Highlighting
;;;;

;; This still is only proof of concept.

;; We define highlighting by parsing the buffer content with a simple
;; recursive descend parser. The font attributes for each character are
;; then derived from the parser state. Each line remembers the start and
;; end parser state for caching. If the start parser state is the same as
;; the end parser state of the previous line no reparsing needs to be done.
;; Lines can change and if a line changes its end parser state is
;; considered to be unknown. So if you change a line syntax highlighting of
;; all following lines is potentially invalid. We avoid reparsing all of
;; the rest of the buffer by three means: First we access syntax markup in
;; a lazy fashion; if a line isn't displayed we don't need its syntax
;; markup. Second when while doing reparsing the newly computed end state
;; is the same as the old end state reparsing stops, because this end state
;; then matches the start state of the next line. Third when seeing an open
;; paren in the very first column, we assume that a new top-level
;; expression starts.

;; These recursive descend parsers are written in a mini language which
;; subsequently is compiled to some "byte" code and interpreted by a
;; virtual machine. For now we don't allow for parameters or return values
;; of procedures and so a state boils down to the current procedure, the
;; instruction pointer and the stack of saved activations.

;; This mini language allows to define procedures. Within a body of a
;; procedure the following syntax applies:

;; stmt -> (IF <cond> <tag>)     If <cond> evaluates to true, goto <tag>.
;;                               <cond> can be any lisp expression and has
;;                               the current look-ahead character available
;;                               in the variable 'ch'.
;;         <tag>                 A symbol serving as the target for GOs.
;;         (GO <tag>)            Continue execution at the indicated label.
;;         (CONSUME)             Consume the current lookahead character and
;;                               read the next one putting it into 'ch'.
;;         (CALL <proc>)         Call another procedure
;;         (RETURN)              Return from the current procedure

;; What the user sees is a little different. The function ME expands its
;; input to the above language. Added features are:

;; (IF <cond> <cons> [<alt>])    IF is modified to take statements instead
;;                               of branch targets
;; (PROGN {<stmt>}*)             Mainly because of IF, PROGN is introduced.
;;                               Note that the body can defined new branch
;;                               targets, which also are available from outside
;;                               of it.
;; (WHILE <cond> {<stmt>}*)
;; (COND {(<cond> {<stmt>}*)}*)

;; This mini-language for now is enough to write interesting recursive
;; descend parsers.

(defun me (form)
  (cond ((atom form)
         (list form))
        (t
         (ecase (car form)
           ((if)
            (destructuring-bind (cond cons &optional alt) (cdr form)
              (let ((l1 (gensym "L."))
                    (l2 (gensym "L.")))
                (append (list `(if (not ,cond) ,l1))
                        (me cons)
                        (list `(go ,l2))
                        (list l1)
                        (and alt (me alt))
                        (list l2)))))
           ((while)
            (destructuring-bind (cond &rest body) (cdr form)
              (let ((exit (gensym "EXIT."))
                    (loop (gensym "LOOP.")))
                (append (list loop)
                        (list `(if (not ,cond) ,exit))
                        (me `(progn ,@body))
                        (list `(go ,loop))
                        (list exit)))))
           ((cond)
            (cond ((null (cdr form)) nil)
                  (t
                   (me
                    `(if ,(caadr form) (progn ,@(cdadr form))
                         (cond ,@(cddr form)))))))
           ((consume return) (list form))
           ((progn) (mapcan #'me (cdr form)))
           ((go) (list form))
           ((call) (list form))))))

(defun ass (stmts)
  (let ((ip 0)
        (labels nil)
        (fixups nil)
        (code (make-array 0 :fill-pointer 0 :adjustable t)))
    (loop for stmt in stmts
      do
      (cond ((atom stmt)
             (push (cons stmt ip) labels))
            ((eq (car stmt) 'go)
             (vector-push-extend :go code) (incf ip)
             (push ip fixups)
             (vector-push-extend (cadr stmt) code) (incf ip))
            ((eq (car stmt) 'if)
             (vector-push-extend :if code) (incf ip)
             (vector-push-extend `(lambda (ch) (declare (ignorable ch)) ,(cadr stmt))
                                 code)
             (incf ip)
             (push ip fixups)
             (vector-push-extend (caddr stmt) code) (incf ip))
            ((eq (car stmt) 'call)
             (vector-push-extend :call code) (incf ip)
             (vector-push-extend `',(cadr stmt) code) (incf ip))
            ((eq (car stmt) 'consume)
             (vector-push-extend :consume code) (incf ip))
            ((eq (car stmt) 'return)
             (vector-push-extend :return code) (incf ip))
            (t
             (incf ip)
             (vector-push-extend stmt code))))
    (loop for fixup in fixups do
      (let ((q (cdr (assoc (aref code fixup) labels))))
        (unless q
          (error "Undefined label ~S." (aref code fixup)))
        (setf (aref code fixup) q)))
    code))


(defvar *parsers* (make-hash-table))


(defmacro defstate (name stuff &rest body)
  stuff
  `(setf (gethash ',name *parsers*)
         (vector ,@(coerce (ass (append (me `(progn ,@body))
                                        (list '(return))))
                           'list))))

