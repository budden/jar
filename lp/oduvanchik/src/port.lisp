;;;; -*- Mode: Lisp; indent-tabs-mode: nil;  system :oduvanchik.base ;-*-

(in-package :oduvanchik.wire)

(defun unix-gethostid ()
  #.(or
     #+CMU '(unix:unix-gethostid)
     398792))

(defun unix-getpid ()
  (swank::getpid))

;; fixme: remove this?
(push (cons '*print-readably* nil)
      bt:*default-special-bindings*)
