; -*- system :oduvanchik.base ; -*- 
;; Billets for functions for doing editing on tcl side, they will be really implemented in :clcon-server system. See also odu::set-mark-to-row-and-col and generic function definitions of methods defined in this file

(in-package :oduvanchik-internals)

(defun gen-unique-mark-name-for-clcon_text ()
  "Creates unique mark name for clcon_text. Be sure to delete mark when it is not needed!"
  (gensym))

(defun mark-row-and-col (mark)
  "Row and column of mark. Some izbytochnost must be removed. See also odu::with-mark-in-row-col" 
  (let* ((right-answer (mark-position mark))
         (line (mark-line mark))
         (row (offset-of-line line))
         (col (mark-charpos mark)))
    (assert (= row (+ 1 (second right-answer))))
    (assert (= col (third right-answer)))
    (values row col)))

(defun tcl-index-of-mark (mark)
  "Returns index in the form Row.Col of the mark given"
  (check-type mark mark)
  (multiple-value-bind (row col) (mark-row-and-col mark)
    (format nil "~D.~D" row col)))

(defun tcl-code-for-send-mark-to-clcon_text (clcon_text mark &key remote-name)
  "For mark, returns tcl code which creates corresponding mark in text with name = remote-name, or moves an existing mark"
  (check-type remote-name (or symbol string))
  (check-type mark mark)
  (with-output-to-string (ou)
    (format ou "~A mark set ~A ~A"
            clcon_text (string remote-name) (tcl-index-of-mark mark))
    (when (string= remote-name "insert")
      (format ou ";~A see insert" clcon_text)))
  )

(defun parse-tcl-text-index (index)
  "Returns row and col in tcl coordinate system as multiple values"
  (let* ((splitted (split-sequence:split-sequence #\. index))
         (row-s (first splitted))
         (col-s (second splitted))
         (row (parse-integer row-s))
         (col (parse-integer col-s)))
    (values row col)))

(assert (equal (multiple-value-list (parse-tcl-text-index "4.5")) (list 4 5)))

(defun send-mark-to-clcon_text (clcon_text mark &key remote-name)
  "See also sync-mark-from-clcon_text"
  (call-tcl-simple (tcl-code-for-send-mark-to-clcon_text clcon_text mark :remote-name remote-name))
  )

(defun tcl-code-for-unset-clcon_text-mark (clcon_text remote-name)
  "Code which removes mark with that name from clcon_text"
  (check-type remote-name (or symbol string))
  (let ((rn (string remote-name)))
    (if (member rn '("insert" "end") :test 'string=)
        ; according to definition of text widget, there are only two predefined marks
        "expr 1" ; this is just nop.
        (format nil "~A mark unset ~A" clcon_text (string remote-name)))))

(defun unset-clcon_text-mark (clcon_text remote-name)
  "Must be called in swank::with-connection dynamic scope"
  (call-tcl-simple (tcl-code-for-unset-clcon_text-mark clcon_text remote-name)))

(defun clcon_text-backend-buffer-p (buffer)
  "Returns t if this is a buffer for clcon_text"
  (and (bufferp buffer)
       (oi::oduvanchik-bound-p 'odu::swank-connection :buffer buffer))
  )

(defun buffer-to-clcon_text (buffer)
  "Coordinated to oduvanchik::eval-construct-backend-buffer. See also clcon_text-to-buffer. Returns nil for non-backend buffers"
  (assert (bufferp buffer))
  (when (clcon_text-backend-buffer-p buffer)
    (buffer-name buffer)
    ))

(defun clcon_text-to-buffer (clcon_text)
  "Get buffer by its clcon_text name. See also buffer-to-clcon_text"
  (let ((result (oduvanchik-ext::find-buffer clcon_text)))
    (unless result
      (error "There is no backend buffer for ~A" clcon_text))
    result))


