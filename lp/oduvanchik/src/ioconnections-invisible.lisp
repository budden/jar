;;;; -*- Mode: Lisp; indent-tabs-mode: nil -*-

(in-package :oi)
; #+sbcl (declaim (optimize (speed 2)))
(declaim (optimize debug)) ; budden

(defmethod invoke-with-existing-event-loop ((backend (eql :invisible-connection-backend)) loop fun)
  (warn "entered invoke-with-existing-event-loop :invisible-connection-backend")
  (let ((*event-base*
         nil ; budden
          ))
    (funcall fun)))


