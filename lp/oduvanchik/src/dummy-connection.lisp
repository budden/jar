;;;; -*- Mode: Lisp; indent-tabs-mode: nil;  system :oduvanchik.base ; -*-

(in-package :oi)

(defclass dummy-connection ()
  (
   (filter :initarg :filter)
   (buffer :initarg :buffer)
   (name :initarg :name)
   (encoding :initarg :encoding)
   (write-fd :initarg :write-fd)
   (read-fd :initarg :read-fd)
   )
  )

(defmethod class-for
    ((backend (eql :invisible-connection-backend)) type)
  'dummy-connection)

(defmethod connection-write (data (connection dummy-connection))
  (declare (ignorable data connection))
  ; this function is blank as it is dummy backend which outputs
  ; nothing on the screen
  ; (warn "connection-write stub ~S ~S" data connection)
  )

(defmethod dispatch-events-no-hang-with-backend ((backend (eql :invisible-connection-backend)))
  (cond
    ((tcl-interface-active 1)
     (bt:with-lock-held (*q-event-lock*)
       (warn "I suppose we need no event dispatching as all events are already in the event queue")
       nil
       ))
    (t
     nil ; no events expected - nothing to dispatch
     )
    ))

(defgeneric oduvan-invisible-peek-input-event (dummy &key hang)
  (:documentation "Waits for event or check if it is present. Method implemented in oduvanchik-key-bindings of :clcon-server"))

#+oduvan-use-sleep-in-dispatch
(defparameter *dispatch-event-sleep-interval* 0.3 "Condition variables seem to fail to work under win32. So we need empty loop for events to work. This is sleep interval for the loop. Greater values = greater latency. Smaller values = greater CPU usage")

(defmethod dispatch-events-with-backend ((backend (eql :invisible-connection-backend)))
  #-oduvan-use-sleep-in-dispatch
  (oduvan-invisible-peek-input-event 1 :hang t)
  #+oduvan-use-sleep-in-dispatch
  (progn
    (oduvan-invisible-peek-input-event 1 :hang nil)
    (sleep *dispatch-event-sleep-interval*))
  #| ; maybe at better times...
  (cond
    ((tcl-interface-active 1)
     (warn "I'll just sleep a bit")
     (sleep 0.3)
     )
    (t
     (sleep 0.3) ; no events expected - nothing to dispatch
     )
  )|#
  )

(defclass fake-event-loop () ())

(defmethod make-event-loop ((backend (eql :invisible-connection-backend)))
  (make-instance 'fake-event-loop))
  
