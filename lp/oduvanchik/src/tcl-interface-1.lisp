; -*- system :oduvanchik.base ; -*- 
;; Billets for functions for doing editing on tcl side, they will be really implemented in :clcon-server system. See also odu::set-mark-to-row-and-col and generic function definitions of methods defined in this file

(in-package :oduvanchik-internals)

(defvar *do-editing-on-tcl-side* nil
  "When true, we reproduce every text modification (listed in new-undo.lisp) to associated clcon_text widget on tcl side. We suppose clcon_text widget is frozen during the dialog")

(defgeneric tcl-interface-active (dummy)
  (:documentation "Returns t if we have active interface. No sane implementation yet. FIXME all functions that check *do-editing-on-tcl-side* or bind it to t, must also call tcl-interface-active. We need function there as we migh want to check the state of the stream"))

(defvar *tcl-interface-active* nil "In a good time, it must be set to t when connecting from tcl and to nil when connection is lost. For now, you can set it manually for debugging purposes")

(defmethod tcl-interface-active (dummy)
  (declare (ignore dummy))
  *tcl-interface-active*)

(defgeneric sync-mark-from-clcon_text (clcon_text mark remote-name)
  (:documentation "Sets mark in oduvanchik to the place where remote-mark is")
  )

(defgeneric call-tcl-editing (code)
  (:documentation "Synchonously do editing primitive in tcl. Must be called with *do-editing-on-tcl-side* = t and in dynamic scope of swank::with-connection. Rebinds *do-editing-on-tcl-side* to nil"))

(defgeneric call-tcl-simple (code)
  (:documentation "Synchonously do some motion or marking command in tcl. code must not contain callback;s to oduvanchik. E.g. it can be 'clcon_text mark set'"))

(defun call-combined-tcl-editing (&rest tcl-codes)
  "Concatenates several tcl commands and executes them at once"
  (call-tcl-editing
   (budden-tools:princ-to-string-delimited-list #\; tcl-codes)
   ))

(defgeneric tcl-code-for-insert-character (clcon_text remote-mark-name character)
  (:documentation "Args: oduvanchik's mark and character to insert. Returns: tcl code to do that. It is callers responsibility to send mark to clcon_text"))

(defgeneric tcl-code-for-insert-string (clcon_text remote-mark-name character)
  (:documentation "Args: oduvanchik's mark and string to insert. Returns: tcl code to do that. It is callers responsibility to send mark to clcon_text"))


(defgeneric tcl-code-for-delete-region (clcon_text remote-beg-mark-name remote-end-mark-name)
  (:documentation "Args: clcon_text id, remote mark names of region bounds. Returns: tcl code to do that. It is callers responsibility to send and delete marks"))

(defgeneric insert-string-with-clcon_text (mark string start end)
  (:documentation "Implements requested insertion in clcon_text. See also insert-string")
  )

(defgeneric insert-character-with-clcon_text (mark character)
  (:documentation "Implements requested insertion in clcon_text. See also insert-character")
  )

(defgeneric insert-region-with-clcon_text (mark region)
  (:documentation "Implements requested insertion in clcon_text. See also insert-region")
  )

(defgeneric ninsert-region-with-clcon_text (mark region)
  (:documentation "Implements requested insertion in clcon_text. See also ninsert-region")
  )

(defgeneric delete-characters-with-clcon_text (mark n)
  (:documentation "See also delete-characters")
  )

(defgeneric delete-region-with-clcon_text (region)
  (:documentation "See also delete-region")
  )

(defgeneric delete-and-save-region-with-clcon_text (region)
  (:documentation "See also delete-and-save-region")
  )

;; selection
(defgeneric transfer-selection-to-clcon_text (dummy-parameter)
  (:documentation "Set selection in the clcon_text to region"))

(defgeneric transfer-selection-from-clcon_text (buffer)
  (:documentation "Gets selection from the clcon_text to current buffer"
                  ))
