;;;; -*- coding: utf-8; Mode: Lisp; indent-tabs-mode: nil;  system :oduvanchik.base ; -*-
;;; ---------------------------------------------------------------------------
;;;     Title: experimental syntax highlighting
;;;   Created: 2004-07-09
;;;    Author: Gilbert Baumann <gilbert@base-electronic.de>
;;;       $Id: exp-syntax.lisp,v 1.1 2004-07-09 15:16:14 gbaumann Exp $
;;; ---------------------------------------------------------------------------
;;;  (c) copyright 2004 by Gilbert Baumann

(in-package :oi)

;; "syntax highlight mode" - переменная режима. Она может быть :send-highlight-from-recompute-line-tag (по умолчанию)
;; и :send-highlight-after-recomputing-entire-buffer
;; :send-highlight-from-recompute-line-tag - это то, что мы реализовали раньше. line-tag, если тег данной строки устарел, вызывает 
;; синхронное вычисление тегов по сию строчку и асинхронное - до конца файла
;; :send-highlight-after-recomputing-entire-buffer - при наличии невалидного тега вычисляются теги для всего буфера, а потом запускается полная перераскраска - это только для Яра
(defun syntax-highlight-mode (buffer)
  (assert buffer)
  (let* ((mode (buffer-major-mode buffer)))
    (if (and mode
             (oduvanchik-interface:oduvanchik-bound-p
              'oduvanchik::syntax-highlight-mode
              :mode
              mode))
        (oduvanchik-interface:variable-value 'oduvanchik::syntax-highlight-mode :mode mode)
        :send-highlight-from-recompute-line-tag)))

(defun step** (state char)
  (let* (fun ip code)
    (labels ((fetch ()
               (prog1 (aref code ip) (incf ip)))
             (sync (fun* ip*)
               (setf fun fun*
                     ip  ip*
                     code (or (gethash fun *parsers*)
                              (error "No such fun: ~S." fun))))
             (exit ()
               (sync (pop state) (pop state)))
             (save ()
               (push ip state)
               (push fun state)))
      (exit)
      (loop
          (ecase (fetch)
            (:if
             (let ((cond (fetch))
                   (target (fetch)))
               (when (funcall cond char)
                 (setf ip target))))
            (:consume
             (save)
             (return-from step** state))
            (:return
             '(print (list :return state) *trace-output*)
             (exit)
             ;;(print (list :dada state))
             )
            (:call
             (let ((new-fun (fetch)))
               '(print (list :call new-fun) *trace-output*)
               (save)
               (sync new-fun 0)))
            (:go
             (setf ip (fetch))))))))

(defun dodo (string)
  (let ((state (list 'initial 0)))
    (loop for c across string do
          (setf state (step** state c))
          (let ((q (member-if (lambda (x) (member x '(string rq bq uq comment))) state)))
            (case (car q)
              (comment (format t "/~A" c))
              ((rq bq) (princ (char-upcase c)))
              (uq (princ c))
              ((nil) (princ c)))))
    state))

;;;;;;;;;;;;;

(defun initial-syntax-state ()
  (list 'initial 0))

(defun empty-syntax-info ()
  (make-syntax-info :frob nil (initial-syntax-state) nil))


(defgeneric maybe-send-line-highlight-to-clcon (line)
  (:documentation "Make this generic to change from clcon. Here we define dummy primary method. Real method will be the around one"))

(defmethod maybe-send-line-highlight-to-clcon (line)
  (declare (ignore line)))

(defparameter +function-name-font+ 5)
(defparameter +default-font+ 0)

(defun recompute-font-marks-fn-for-line (line)
  (let* ((buffer (and line (line-buffer line)))
         (mode (and buffer (buffer-major-mode buffer))))
    ;; "Recompute Syntax Marks Function" упоминается здесь дважды
    (if (and mode
             (oduvanchik-interface:oduvanchik-bound-p
              'oduvanchik::recompute-syntax-marks-function
              :mode
              mode))
        (oduvanchik-interface:variable-value 'oduvanchik::recompute-syntax-marks-function :mode mode)
        'dummy-recompute-syntax-marks)))


(defun delete-syntax-info-font-marks-in-line (line tag)
  "Удалить все font-marks из syntax-info, если она есть в этом tag, к-рый заведомо относится к этой line"
  (let* ((sy (or (tag-syntax-info tag)
                 (empty-syntax-info)))
         (font-marks (sy-font-marks sy)))
    (dolist (fm font-marks)
      ;(check-something-ok fm)
      (hi::delete-font-mark fm))
    ;; budden - I noted there is mark leak. Try to fix
    (oi::delete-line-font-marks line)
    (setf (sy-font-marks sy) nil) 
    (check-something-ok)
    (check-something-ok line)))

(defun smart-insert-font-mark (font-mark font-marks)
  (cons font-mark (remove-if (lambda (x) 
                               (= (oi::mark-charpos x)
                                  (oi::mark-charpos font-mark))) 
                             font-marks)))

(defun smart-insert-font-mark! (font-mark line &key (send-highlight t))
;  (format t "smart-mark: ~a ~a~%" font-mark line)
  (let* ((sy (tag-syntax-info
              (line-tag-no-recalc line)))
         (font-marks (sy-font-marks sy)))
    (setf (sy-font-marks sy)
          (smart-insert-font-mark font-mark font-marks)))
  (when send-highlight
    (maybe-send-line-highlight-to-clcon line)))
    
         
(defun recompute-syntax-marks (line tag)
  "Тег всегда является тегом для данной строки."
  (let ((fn (recompute-font-marks-fn-for-line line)))
    (assert (eq (syntax-highlight-mode (line-buffer line)) 
                :send-highlight-from-recompute-line-tag))
    (assert (eq tag (%line-tag line)))
    (funcall fn line tag)))

(defun dummy-recompute-syntax-marks (line tag)
  "Вырезали почти всё из old-lisp-recompute-syntax-marks, чтобы она ничего не делала, особо не разбираясь. Возможно, мы вовсе не правы"
  (check-something-ok line)
  (let* ((sy (or (tag-syntax-info tag)
                 (empty-syntax-info))))
    (values sy nil)))

(defun old-lisp-recompute-syntax-marks (line tag)
  "См. также dummy-recompute-syntax-marks , РАСКРАСКА-ЯРА-ЛЕКСЕРОМ:Recompute-syntax-marks--специально-для-Яра"
  (check-something-ok line)
  (let* ((sy (or (tag-syntax-info tag)
                 (empty-syntax-info)))
         (prev (line-previous line))
         (prev-to (if prev
                      (sy-to-state (tag-syntax-info (%line-tag prev)))
                      (initial-syntax-state)))
         (font-marks (sy-font-marks sy)))
    (cond ((and (eq (sy-signature sy) (line-signature line))
                (equal (sy-from-state sy) prev-to))
           ;; no work
           (check-something-ok)
           ; (format *trace-output* "~%NO WORK~%")
           (values sy nil))
          (t
           ;; work to do, but first remove old font marks
           (delete-syntax-info-font-marks-in-line line tag)
           (setf font-marks nil)
           ;; now do the highlighting
           (let ((state prev-to)
                 (last-font 0))
             ; (print `(:begin ,state) *trace-output*)
             (loop for p from 0 below (line-length line) do
               (let ((ch (line-character line p)))
                 (setf state (step** state ch))
                 (let ((font (state-font state)))
                   (unless (eql font last-font)
                     (push (hi::font-mark line p font) font-marks)
                     (check-something-ok (car font-marks))
                     (setf last-font font)))))
             (setf state (step** state #\newline))
             ;; hack
             (let ((s (line-string line)) p1 p2)
               (when (and (eql 0 (search "(def" s))
                          (setf p1 (position #\space s))
                          (setf p2 (position #\space s :start (1+ p1))))
                 (push (hi::font-mark line (1+ p1) +function-name-font+) font-marks)
                 (check-something-ok (car font-marks))
                 (push (hi::font-mark line p2 +default-font+) font-marks)
                 (check-something-ok (car font-marks))))
             (check-something-ok)
             (values
              (make-syntax-info (line-signature line)
                                prev-to
                                state
                                font-marks) t))))))


;;;; Tag computation

(defun line-tag-valid-p (line)
  (let* ((buffer (line-buffer line))
        (granitsa (buffer-poslednaya-str-s-tegom buffer)))
    (assert buffer)
    (and granitsa (eq (line-buffer granitsa) buffer) (line<= line granitsa))))

(defun line-tag (line)
  "Returns line tag. If it is invalid, calculates it (it can take a minute in a 4Mb file)"
  (let ((buffer (line-buffer line)))
    (cond
     ((null buffer)
      nil)
     (t
      (bt:with-recursive-lock-held (oi::*INVOKE-MODIFYING-BUFFER-LOCK*)
        (unless (line-tag-valid-p line)
          (ecase (syntax-highlight-mode buffer)
           (:send-highlight-from-recompute-line-tag
            (recompute-tags-up-to line nil))
           (:send-highlight-after-recomputing-entire-buffer
            (recompute-line-tags-light buffer)
            ; (oduvanchik::start-background-repaint-after-recomputing-entire-buffer buffer)
            )))
        (unless (%line-tag line)
          (break "You were lucky to hit the bug which I can not localize. If you can reproduce it, please contact the author. Now continue to apply a workaround and I hope you will able to work futher.")
          (assert (eq (syntax-highlight-mode buffer) :send-highlight-from-recompute-line-tag) () "Так не умеем чинить")
          (setf (buffer-poslednaya-str-s-tegom buffer) nil)
          (recompute-tags-up-to line nil))
        (%line-tag line))))))

(defun line-tag-no-recalc (line)
  "Returns line tag. If it is invalid, err"
  (unless (line-tag-valid-p line)
    (error "line-tag-no-recalc: tag is invalid"))
  (%line-tag line))
  

(defun ПЕРЕРАСКРАСИТЬ-БУФЕР (line)
  "Сюда мы заходим только если знаем, что точка раскраски сдвинулась назад и находится примерно в районе line. Начинаем искать первую нераскрашенную строчку от line назад, далее запускаем фоновый процесс перевычисления тегов и перераскраски, к-рый дойдёт до конца файла, если успеет. См. также oduvanchik::ПЕРЕРАСКРАСИТЬ-БУФЕР-ЯРО"
  (recompute-tags-up-to line t))


(defun reset-background-highlight-process (buffer)
  "Informs that currently ongoing background highlight process must be terminated. Returns buffer-highlight-wave-id with which new higlight process can be started"
#| Всё это неправильно. Должны быть следующие операции (внутри самого одуванчика):

1. Сообщить о том, что строка такая-то устарела. При этом, если данная строка была раскрашена до момента устаревания, начать красить с этого места в фоновом режиме заново.
2. Удалить тег из строки, не сообщая, что она устарела - только для случаев, когда строки покидают буфер (например, для вырезанных строк)
3. Синхронно докрасить до текущей строки - для операций типа "получить пакет в точке".
4. Операция "получить пакет в точке" должна иметь параметр, позволяющий не дожидаться раскраски и вернуть ??UNKNOWN??

Хотя возможно, что у нас и так всё неплохо, ведь сбрасывание процесса фоновой раскраски не означает, что этот процесс начнётся с начала файла - он всё равно начнётся
с первой некорректной строки.

|#
  (assert (not (eq (syntax-highlight-mode buffer) :send-highlight-after-recomputing-entire-buffer)))
  (incf (buffer-highlight-wave-id buffer)))

(defgeneric recompute-tags-up-to (end-line background)
  (:documentation "

Обезпечить наличие правильных line-tag как минимум по строку end-line включительно. 

Method for background = t will be created later in clcon. There are two types of backgrounding: 
  i. we already send highlights to tcl via a queue
  ii. calculation of line tags itself can be done as a background, sending commands to event queue. This is what we deal with in this generic function (we'll define another new method  in clcon-odu-commands-infrastructure.lisp with background (eql t) which implements background recompution. 

  Также нужно иметь в виду, что метод с background = nil синхронно считает только до строки end-line, а метод с background = t считает асинхронно все строчки буфера, начиная поиск нераскрашенной строки от end-line. 

"))


(defun НАЙТИ-ПЕРВУЮ-СТРОЧКУ-С-УСТАРЕВШИМ-ТЕГОМ (end-line)
  (let* ((level (buffer-poslednaya-str-s-tegom (line-buffer end-line))))
    (iter (for line initially end-line then prev)
          (for prev = (line-previous line))
          (unless prev
            (return line))
          (let ((validp
                 (and level
                      (line<= prev level))))
            (when validp
              (return line))))))

(defmethod recompute-tags-up-to (end-line (background (eql nil)))
  "Найти самую раннюю строчку с устаревшим тегом, не позднее end-line, пересчитать все теги до end-line включительно 
и запрограммировать фоновую раскраску дальше"
  (bt:with-recursive-lock-held (*invoke-modifying-buffer-lock*)
   (let* ((start-line (НАЙТИ-ПЕРВУЮ-СТРОЧКУ-С-УСТАРЕВШИМ-ТЕГОМ end-line))
          (buffer (line-buffer start-line)))
     (reset-background-highlight-process buffer)
     (iter (for line initially start-line then (line-next line))
           (while line)
           (recompute-line-tag line)
           (until (eq line end-line)))
     ; Теперь мы запускаем фоновую раскраску, т.к. она нам нужна
     (when (line-next end-line)
       (ПЕРЕРАСКРАСИТЬ-БУФЕР (line-next end-line))))))

(defun find-package-change-in-line (line)
  (oduresy:find-package-change-in-string (line-string line)))

(defun find-readtable-change-in-line (line)
  "If the line contains in-readtable form, return (values t readtable-name), otherwise returns (values nil nil). Readtable name must be a keyword or nil"
  (oduresy:find-readtable-change-in-string (line-string line)))


(defun check-that-previous-line-is-valid (line)
  (let ((prev (line-previous line)))
    (assert 
     (or (null prev)
         (line-tag-valid-p prev)) () "Ожидалось, что предыдущая строчка ~S правильно раскрашена" prev))) 


(defun recompute-line-tag-inner-1 (line tag buffer ptag)
  "ptag can be null if we are in the first line. See also budden-tools::decorated-swank-source-path-parser--guess-reader-state. tag является тегом для данной строки"
  (multiple-value-bind (sy recalculated)
                       (recompute-syntax-marks line tag)
    (when (eq (syntax-highlight-mode buffer) :send-highlight-from-recompute-line-tag)
      (setf (tag-syntax-info tag) sy))
    (let ((package-change (find-package-change-in-line line)))
      (setf (tag-package tag)
            (or package-change
                (if ptag
                    (tag-package ptag)
                    (default-tag-package))
                ))
      (multiple-value-bind (readtable-change-found new-rt-name)
                           (find-readtable-change-in-line line)
        (setf (tag-readtable tag) 
              (cond
               (readtable-change-found
                new-rt-name)
               (package-change
                (or (if ptag (tag-readtable ptag) nil)
                    (let ((rt (cdr (assoc (tag-package tag) swank::*readtable-alist* :test 'string=))))
                      (and rt (named-readtables:readtable-name rt)))))
               (t
                (if ptag (tag-readtable ptag) (default-tag-readtable)) 
                )))))
    (check-that-previous-line-is-valid line)
    (check-something-ok)
    (when (eq (oi::syntax-highlight-mode buffer) :send-highlight-from-recompute-line-tag)
      (setf
        (buffer-poslednaya-str-s-tegom buffer)
         line)
      (when recalculated
        (maybe-send-line-highlight-to-clcon line))))
  ) 

(defun recompute-line-tags-light (buffer)
  "это копия recompute-line-tag, в к-рой мы синхронно создаём теги для всех строк, если тегов ещё нет, но ничего не делаем по поводу синтаксической раскраски. 
Синтаксическая раскраска будет производиться потом. Это - версия специально для синхронной раскраски Яра без интерпретатора"
  (assert (eq (oi::syntax-highlight-mode buffer) :send-highlight-after-recomputing-entire-buffer))
  (assert (beyond-bordeaux-threads:holding-lock-p *invoke-modifying-buffer-lock*))
  (assert buffer) 
  ; делаем вид, что все теги уже обновлены
  (setf (buffer-poslednaya-str-s-tegom buffer) (last-line-of-buffer buffer))
  (do ((line (first-line-of-buffer buffer) (line-next line)))
      ((null line) nil)
    (let* ((prev (line-previous line)))
      (cond
       ((null prev)
        (let ((tag (make-tag :syntax-info (empty-syntax-info))))
          (setf (%line-tag line) tag)))
       ((null (%line-tag prev))
        (break "Это бага из recompute-line-tag. Надеюсь, мы досюда не дошли :)"))
       (t
        (let* ((ptag (%line-tag prev))
               (tag (or (%line-tag line)
                        (setf (%line-tag line) (make-tag :syntax-info (empty-syntax-info))))))
          ; обновим номер строки
          (let ((new-line (1+ (tag-line-number ptag))))
            ;(format t "~&уже почти вычислил tag-line-number ~S ~S~%" new-line line)
            (unless (eql (tag-line-number tag) new-line)
              ; тики скорее всего не нужны вовсе, но вроде бы они нужны для синхронизации с ГПИ в исходном одуванчике.
              (incf (tag-ticks tag)))
            (setf (tag-line-number tag) new-line))))))))

(defun recompute-line-tag (line)
  "Перевычислить тег, включая синтаксические марки, для одной строки. We enter here if it is determined that line needs change. Return value unused by caller. При изменении этой функции нужно также менять recompute-line-tags-light"
  (let* ((buffer (line-buffer line))
         (prev (line-previous line)))
    (assert (beyond-bordeaux-threads:holding-lock-p *invoke-modifying-buffer-lock*))
    (assert buffer) ; we should have captured orphan line situation in some caller
    (cond
      ((null prev)
       (let* ((buffer (line-buffer line))
              (tag (make-tag :syntax-info (empty-syntax-info))))
         (setf (%line-tag line) tag)
         (recompute-line-tag-inner-1 line tag buffer nil)))
      ((null (%line-tag prev))
        (break "You were lucky to hit the another bug which I can not localize. If you can reproduce it, please contact the author. Now continue to apply a workaround and I hope you will able to work futher. WBR, Budden")
        (setf (buffer-poslednaya-str-s-tegom buffer) nil)
        (recompute-tags-up-to line nil)
        (recompute-line-tag line))
      (t
       (let* ((ptag (%line-tag prev))
              (tag (or (%line-tag line)
                       (setf (%line-tag line) (make-tag))))
              (buffer (line-buffer line)))
         ; обновим номер строки
         (let ((new-line (1+ (tag-line-number ptag))))
           ;(format t "~&уже почти вычислил tag-line-number ~S ~S~%" new-line line)
           (unless (eql (tag-line-number tag) new-line)
             ; тики скорее всего не нужны вовсе, но вроде бы они нужны для синхронизации с ГПИ в исходном одуванчике.
             (incf (tag-ticks tag)))
           (setf (tag-line-number tag) new-line))
         (recompute-line-tag-inner-1 line tag buffer ptag)
         )))))


(defun state-font (state)
  "see oi::font-map-size to know how many fonts we have. See also .fics ColorTable (or ../../clcon/highlight.edt.tcl)"
  (cond ((member 'hash-plus state)
         6)
        ; enable this to color atoms another way
        ; ((some (lambda (x) (member x state)) '(atom escaped-char-in-atom rest-of-barred-atom)) 1)
        (t
         (let ((q (member-if (lambda (x) (member x '(string rq bq uq comment hash-plus hash-minus hashed-comment :long-comment :short-comment :identifier :string))) state)))
           (case (car q)
             (comment 2)
             (:long-comment 2) 
             (:short-comment 2) 
             (:identifier 5) 
             (:string 1)
             (hashed-comment 2)
             (rq 8)
             (bq 9)
             (uq 14)
             (string 1)
             (hash-plus 6)
             (hash-minus 6)
             (char-const 13)
             (list 15)
             ; also there is +function-name-font+ (see)
             ; also there is odu::*open-paren-highlight-font*
             ((nil) +default-font+))))))

