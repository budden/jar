;;;; -*- Mode: Lisp; indent-tabs-mode: nil;  system :oduvanchik.base ; -*-
;;;
;;; **********************************************************************
;;; This code was written as part of the CMU Common Lisp project at
;;; Carnegie Mellon University, and has been placed in the public domain.
;;;
;;;
;;; **********************************************************************
;;;
;;;    This file contains definitions of various types of streams used
;;; in Oduvanchik.  They are implementation dependant, but should be
;;; portable to all implementations based on Spice Lisp with little
;;; difficulty.
;;;
;;; Written by Skef Wholey and Rob MacLachlan.
;;;

(in-package :oduvanchik-internals)

;;; Note: although this stream is intended for output only it also supports
;;; input to help if the debugger is called.
(defclass oduvanchik-output-stream (#-scl hi::trivial-gray-stream-mixin
                                 #-scl hi::fundamental-character-output-stream
                                 #-scl hi::fundamental-character-input-stream
                                 #+scl ext:character-output-stream
                                 #+scl ext:character-input-stream)
  ((mark
    :initform nil
    :accessor oduvanchik-output-stream-mark
    :documentation "The mark we insert at.")
   (input-string
    :initform nil)
   (input-pos
    :initform 0)
   (out
    :accessor old-lisp-stream-out)
   (sout
    :accessor old-lisp-stream-sout)))

(defun oduvanchik-output-stream-p (x)
  (typep x 'oduvanchik-output-stream))

(defmethod hi::stream-write-char ((stream oduvanchik-output-stream) char)
  (funcall (old-lisp-stream-out stream) stream char))

(defmethod hi::stream-write-sequence
    ((stream oduvanchik-output-stream) seq start end &key)
  (check-type seq string)
  (oduvanchik-output-buffered-sout stream seq start end))

#+scl
(defmethod ext:stream-write-chars
    ((stream oduvanchik-output-stream) seq start end waitp)
  (declare (ignore waitp))
  (check-type seq string)
  (oduvanchik-output-buffered-sout stream seq start end)
  (- end start))

(defmethod hi::stream-line-column ((stream oduvanchik-output-stream))
  (mark-charpos (oduvanchik-output-stream-mark stream)))

(defmethod hi::stream-line-length ((stream oduvanchik-output-stream))
  (mark-charpos (oduvanchik-output-stream-mark stream))
  (let* ((buffer
          (line-buffer (mark-line (oduvanchik-output-stream-mark stream)))))
    (when buffer
      (do ((w (buffer-windows buffer) (cdr w))
           (min most-positive-fixnum (min (window-width (car w)) min)))
          ((null w)
           (if (/= min most-positive-fixnum) min))))))

(defmethod print-object ((object oduvanchik-output-stream) stream)
  (write-string "#<Oduvanchik output stream>" stream))

(defun make-oduvanchik-output-stream (mark &optional (buffered :line))
  "Returns an output stream whose output will be inserted at the Mark.
  Buffered, which indicates to what extent the stream may be buffered
  is one of the following:
   :None  -- The screen is brought up to date after each stream operation.
   :Line  -- The screen is brought up to date when a newline is written.
   :Full  -- The screen is not updated except explicitly via Force-Output."
  (modify-oduvanchik-output-stream (make-instance 'oduvanchik-output-stream)
                                mark
                                buffered))


;;; Note: this is called when re-using a stream and is expected to
;;; re-initialize the stream.
(defun modify-oduvanchik-output-stream (stream mark buffered)
  (unless (and (markp mark)
               (member (mark-kind mark) '(:right-inserting :left-inserting)))
    (error "~S is not a permanent mark." mark))
  (setf (oduvanchik-output-stream-mark stream) mark)
  ;;
  ;; Free the current stream buffers, resetting the buffer pointers.
  #+scl (lisp::free-stream-buffers stream)
  ;;
  (case buffered
    (:none
     #+scl
     (setf (ext:stream-out-buffer stream) (lisp::make-stream-buffer 'oduvanchik-internals:oduvanchik-base-char 0))
     (setf (old-lisp-stream-out stream) #'oduvanchik-output-unbuffered-out
           (old-lisp-stream-sout stream) #'oduvanchik-output-unbuffered-sout))
    (:line
     #+scl
     (setf (ext:stream-out-buffer stream) (lisp::make-stream-buffer 'oduvanchik-internals:oduvanchik-base-char 0))
     (setf (old-lisp-stream-out stream) #'oduvanchik-output-line-buffered-out
           (old-lisp-stream-sout stream) #'oduvanchik-output-line-buffered-sout))
    (:full
     #+scl
     (setf (ext:stream-out-buffer stream) (lisp::make-stream-buffer 'oduvanchik-internals:oduvanchik-base-char))
     (setf (old-lisp-stream-out stream) #'oduvanchik-output-buffered-out
           (old-lisp-stream-sout stream) #'oduvanchik-output-buffered-sout))
    (t
     (error "~S is a losing value for Buffered." buffered)))
  stream)

(defmacro with-left-inserting-mark ((var form) &body forms)
  (let ((change (gensym)))
    `(let* ((,var ,form)
            (,change (eq (mark-kind ,var) :right-inserting)))
       (unwind-protect
           (progn
             (when ,change
               (setf (mark-kind ,var) :left-inserting))
             ,@forms)
         (when ,change
           (setf (mark-kind ,var) :right-inserting))))))

(defun oduvanchik-output-unbuffered-out (stream character)
  (with-left-inserting-mark (mark (oduvanchik-output-stream-mark stream))
    (insert-character mark character)
    (redisplay-windows-from-mark mark)))

(defun oduvanchik-output-unbuffered-sout (stream string start end)
  (with-left-inserting-mark (mark (oduvanchik-output-stream-mark stream))
    (insert-string mark string start end)
    (redisplay-windows-from-mark mark)))

(defun oduvanchik-output-buffered-out (stream character)
  (with-left-inserting-mark (mark (oduvanchik-output-stream-mark stream))
    (insert-character mark character)))

(defun oduvanchik-output-buffered-sout (stream string start end)
  (with-left-inserting-mark (mark (oduvanchik-output-stream-mark stream))
    (insert-string mark string start end)))

(defun oduvanchik-output-line-buffered-out (stream character)
  (with-left-inserting-mark (mark (oduvanchik-output-stream-mark stream))
    (insert-character mark character)
    (when (char= character #\newline)
      (redisplay-windows-from-mark mark))))

(defun oduvanchik-output-line-buffered-sout (stream string start end)
  (declare (simple-string string))
  (with-left-inserting-mark (mark (oduvanchik-output-stream-mark stream))
    (insert-string mark string start end)
    (when (find #\newline string :start start :end end)
      (redisplay-windows-from-mark mark))))

(defmethod stream-finish-output ((stream oduvanchik-output-stream))
  (redisplay-windows-from-mark (oduvanchik-output-stream-mark stream)))

(defmethod stream-force-output ((stream oduvanchik-output-stream))
  (redisplay-windows-from-mark (oduvanchik-output-stream-mark stream)))

(defmethod close ((stream oduvanchik-output-stream) &key abort)
  (declare (ignore abort))
  #+scl
  (when (ext:stream-open-p stream)
    (call-next-method)
    (setf (oduvanchik-output-stream-mark stream) nil)
    t)
  #-scl
  (setf (oduvanchik-output-stream-mark stream) nil))

(defmethod stream-line-column ((stream oduvanchik-output-stream))
  (mark-charpos (oduvanchik-output-stream-mark stream)))


;;; Input methods: although called ODUVANCHIK-OUTPUT-STREAM, the following
;;; methods allow the stream to used for input, too.  Don't do this
;;; at home, because it enters the command loop recursively in a potentially
;;; bad way, but it can be very useful for debugging purposes;

(defvar hi::*reading-lispbuf-input* nil)

(defun ensure-output-stream-input (stream)
  (with-slots (input-string input-pos mark) stream
    (do ()
        ((and input-string (< input-pos (length input-string))))
      (setf input-string
            (catch 'hi::lispbuf-input
              (let ((hi::*reading-lispbuf-input* t)
                    (buffer (line-buffer (mark-line mark))))
                (move-mark
                 (variable-value 'oduvanchik::buffer-input-mark :buffer buffer)
                 (buffer-point buffer))
                (%command-loop))))
      (check-type input-string string)
      (setf input-pos 0))))

(defmethod stream-read-char ((stream oduvanchik-output-stream))
  (ensure-output-stream-input stream)
  (with-slots (input-string input-pos) stream
    (prog1
        (elt input-string input-pos)
      (incf input-pos))))

(defmethod stream-read-char-no-hang ((stream oduvanchik-output-stream))
  (with-slots (input-string input-pos) stream
    (when (and input-string (< input-pos (length input-string)))
      (prog1
          (elt input-string input-pos)
        (incf input-pos)))))

(defmethod stream-listen ((stream oduvanchik-output-stream))
  (with-slots (input-string input-pos) stream
    (and input-string (< input-pos (length input-string)))))

(defmethod stream-unread-char ((stream oduvanchik-output-stream) char)
  (with-slots (input-pos) stream
    (unless (plusp input-pos)
      (error "nothing to unread"))
    (decf input-pos))
  nil)

(defmethod stream-clear-input ((stream oduvanchik-output-stream))
  (with-slots (input-string input-pos) stream
    (unless (and input-string (< input-pos (length input-string)))
      (setf input-string nil)))
  nil)

;;; end of input methods, back in sane code


(defclass oduvanchik-region-stream (#-scl fundamental-character-input-stream
                                 #+scl ext:character-input-stream)
  ;;
  ;; The region we read from.
  ((region :initarg :region
           :accessor oduvanchik-region-stream-region)
   ;;
   ;; The mark pointing to the next character to read.
   (mark :initarg :mark
         :accessor oduvanchik-region-stream-mark)) )

(defmethod print-object ((object oduvanchik-region-stream) stream)
  (declare (ignorable object))
  (write-string "#<Oduvanchik region stream>" stream))

(defun make-oduvanchik-region-stream (region)
  "Returns an input stream that will return successive characters from the
  given Region when asked for input."
  (make-instance 'oduvanchik-region-stream
                 :region region
                 :mark (copy-mark (region-start region) :right-inserting)))

;;; Note: this is called when re-using a stream and is expected to
;;; re-initialize the stream.
(defun modify-oduvanchik-region-stream (stream region)
  (setf (oduvanchik-region-stream-region stream) region)
  (let* ((mark (oduvanchik-region-stream-mark stream))
         (start (region-start region))
         (start-line (mark-line start)))
    ;; Make sure it's dead.
    (delete-mark mark)
    (setf (mark-line mark) start-line  (mark-charpos mark) (mark-charpos start))
    (push mark (line-marks start-line)))
  ;;
  ;; Reset the buffer pointers.
  #+scl (setf (ext:stream-in-head stream) 0)
  #+scl (setf (ext:stream-in-tail stream) 0)
  ;;
  stream)

(defmethod stream-read-char ((stream oduvanchik-region-stream))
  (let ((mark (oduvanchik-region-stream-mark stream)))
    (cond ((mark< mark
                  (region-end (oduvanchik-region-stream-region stream)))
           (prog1 (next-character mark) (mark-after mark)))
          (t :eof))))

(defmethod stream-listen ((stream oduvanchik-region-stream))
  (mark< (oduvanchik-region-stream-mark stream)
         (region-end (oduvanchik-region-stream-region stream))))

(defmethod stream-unread-char ((stream oduvanchik-region-stream) char)
  (let ((mark (oduvanchik-region-stream-mark stream)))
    (unless (mark> mark
                   (region-start (oduvanchik-region-stream-region stream)))
      (error "Nothing to unread."))
    (unless (char= char (previous-character mark))
      (error "Unreading something not read: ~S" char))
    (mark-before mark))
  nil)

(defmethod stream-clear-input ((stream oduvanchik-region-stream))
  (move-mark
   (oduvanchik-region-stream-mark stream)
   (region-end (oduvanchik-region-stream-region stream)))
  nil)

(defmethod close ((stream oduvanchik-region-stream) &key abort)
  (declare (ignorable abort))
  #+scl
  (when (ext:stream-open-p stream)
    (call-next-method)
    (delete-mark (oduvanchik-region-stream-mark stream))
    (setf (oduvanchik-region-stream-region stream) nil)
    t)
  #-scl
  (delete-mark (oduvanchik-region-stream-mark stream))
  #-scl
  (setf (oduvanchik-region-stream-region stream) nil))

#+excl
(defmethod excl:stream-read-char-no-hang ((stream oduvanchik-region-stream))
  (stream-read-char stream))

#+scl
(defmethod stream-file-position ((stream oduvanchik-region-stream) &optional pos)
  (let ((start (region-start (oduvanchik-region-stream-region stream)))
        (mark (oduvanchik-region-stream-mark stream)))
    (cond (pos
           (move-mark mark start)
           (character-offset mark pos))
          (t
           (count-characters (region start mark))))))

#||
(defmethod excl::stream-file-position ((stream oduvanchik-output-stream) &optional pos)
  (assert (null pos))
  (mark-charpos (oduvanchik-output-stream-mark stream)))

(defun region-misc (stream operation &optional arg1 arg2)
  (declare (ignore arg2))
  (case operation

    (:file-position
     (let ((start (region-start (oduvanchik-region-stream-region stream)))
           (mark (oduvanchik-region-stream-mark stream)))
       (cond (arg1
              (move-mark mark start)
              (character-offset mark arg1))
             (t
              (count-characters (region start mark)))))) ))
||#


;;;; Stuff to support keyboard macros.

(defclass kbdmac-stream (editor-input)
  ((buffer :initarg :buffer
           :initform nil
           :accessor kbdmac-stream-buffer
           :documentation "The simple-vector that holds the characters.")
   (index  :initarg :index
           :initform nil
           :accessor kbdmac-stream-index
           :documentation "Index of the next character.")))

(defun make-kbdmac-stream ()
  (make-instance 'kbdmac-stream))

(defmethod get-key-event ((stream kbdmac-stream) &optional ignore-abort-attempts-p)
  (declare (ignore ignore-abort-attempts-p))
  (let ((index (kbdmac-stream-index stream)))
    (setf (kbdmac-stream-index stream) (1+ index))
    (setq *last-key-event-typed*
          (svref (kbdmac-stream-buffer stream) index))))

(defmethod unget-key-event (ignore (stream kbdmac-stream))
  (declare (ignore ignore))
  (if (plusp (kbdmac-stream-index stream))
      (decf (kbdmac-stream-index stream))
      (error "Nothing to unread.")))

(defmethod listen-editor-input ((stream kbdmac-stream))
  (declare (ignore stream))
  t)

;; clear-editor-input ?? --GB

;;; MODIFY-KBDMAC-STREAM  --  Internal
;;;
;;;    Bash the kbdmac-stream Stream so that it will return the Input.
;;;
(defun modify-kbdmac-stream (stream input)
  (setf (kbdmac-stream-index stream) 0)
  (setf (kbdmac-stream-buffer stream) input)
  stream)
