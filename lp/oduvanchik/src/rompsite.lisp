;;;; -*- Mode: Lisp; indent-tabs-mode: nil;  system :oduvanchik.base ; -*-
;;;
;;; **********************************************************************
;;; This code was written as part of the CMU Common Lisp project at
;;; Carnegie Mellon University, and has been placed in the public domain.
;;;
;;; **********************************************************************
;;;
;;; "Site dependent" stuff for the editor while on the IBM RT PC machine.
;;;

(named-readtables::in-readtable :oduvanchik-ext-readtable)
(in-package :oi)

;;; WITHOUT-ODUVANCHIK -- Public.
;;;
;;; Code:lispinit.lisp uses this for a couple interrupt handlers, and
;;; eval-server.lisp.
;;;
#+(and CMU (not oduvan-invisible))
(defmacro without-oduvanchik (&body body)
  "When in the editor and not in the debugger, call the exit method of Oduvanchik's
   device, so we can type.  Do the same thing on exit but call the init method."
  `(progn
     (when (and *in-the-editor* (not debug::*in-the-debugger*))
       (let ((device (device-hunk-device (window-hunk (current-window)))))
         (device-exit device)))
     ,@body
     (when (and *in-the-editor* (not debug::*in-the-debugger*))
       (let ((device (device-hunk-device (window-hunk (current-window)))))
         (device-init device)))))

#+(and (not CMU) (not oduvan-invisible))
(defmacro without-oduvanchik (&body body)
  "When in the editor and not in the debugger, call the exit method of Oduvanchik's
   device, so we can type.  Do the same thing on exit but call the init method."
  `(progn
    (when (and *in-the-editor* )
      (let ((device (device-hunk-device (window-hunk (current-window)))))
        (device-exit device)))
    ,@body
    (when (and *in-the-editor* )
      (let ((device (device-hunk-device (window-hunk (current-window)))))
        (device-init device)))))



;;;; SITE-INIT.

;;; *key-event-history* is defined in input.lisp, but it needs to be set in
;;; SITE-INIT, since MAKE-RING doesn't exist at load time for this file.
;;;
(declaim (special *key-event-history*))

;;; SITE-INIT  --  Internal
;;;
;;;    This function is called at init time to set up any site stuff.
;;;

(defun site-init ()
  (defhvar "Beep Border Width"
    "Width in pixels of the border area inverted by beep."
    :value 20)
  (defhvar "Default Window Width"
    "This is used to make a window when prompting the user.  The value is in
     characters."
    :value 80)
  (defhvar "Default Window Height"
    "This is used to make a window when prompting the user.  The value is in
     characters."
    :value 24)
  (defhvar "Default Initial Window Width"
    "This is used when Oduvanchik first starts up to make its first window.
     The value is in characters."
    :value 80)
  (defhvar "Default Initial Window Height"
    "This is used when Oduvanchik first starts up to make its first window.
     The value is in characters."
    :value 24)
  (defhvar "Default Initial Window X"
    "This is used when Oduvanchik first starts up to make its first window.
     The value is in pixels."
    :value nil)
  (defhvar "Default Initial Window Y"
    "This is used when Oduvanchik first starts up to make its first window.
     The value is in pixels."
    :value nil)
  (defhvar "Bell Style"
    "This controls what beeps do in Oduvanchik.  Acceptable values are :border-flash
     (which is the default), :feep, :border-flash-and-feep, :flash,
     :flash-and-feep, and NIL (do nothing)."
    :value :border-flash)
  #||
  ;; ###
  (defhvar "Reverse Video"
    "Paints white on black in window bodies, black on white in modelines."
    :value nil
    :hooks '(reverse-video-hook-fun))
  ||#
  (defhvar "Cursor Bitmap File"
    "File to read to setup cursors for Oduvanchik windows.  The mask is found by
     merging this name with \".mask\"."
    :value (make-pathname :name "oduvanchik11" :type "cursor"
                          :defaults (merge-pathnames
                                     "resources/"
                                     oduvanchik-system:*oduvanchik-base-directory*)))
  (defhvar "Enter Window Hook"
    "When the mouse enters an editor window, this hook is invoked.  These
     functions take the Oduvanchik Window as an argument."
    :value nil)
  (defhvar "Exit Window Hook"
    "When the mouse exits an editor window, this hook is invoked.  These
     functions take the Oduvanchik Window as an argument."
    :value nil)
  (defhvar "Set Window Autoraise"
    "When non-nil, setting the current window will automatically raise that
     window via a function on \"Set Window Hook\".  If the value is :echo-only
     (the default), then only the echo area window will be raised
     automatically upon becoming current."
    :value :echo-only)
  (defhvar "Default Font"
    "The string name of the font to be used for Oduvanchik -- buffer text,
     modelines, random typeout, etc.  The font is loaded when initializing
     Oduvanchik."
    :value "-*-fixed-*-*-normal-*-*-*-100-100-*-*-iso10646-*" ; "*-courier-medium-r-normal--*-120-*"
    )
  (defhvar "Active Region Highlighting Font"
    "The string name of the font to be used for highlighting active regions.
     The font is loaded when initializing Oduvanchik."
    :value "-*-fixed-*-*-normal-*-*-*-100-100-*-*-iso10646-*" ; "*-courier-medium-o-normal--*-120-*"
    )
  (defhvar "Open Paren Highlighting Font"
    "The string name of the font to be used for highlighting open parens.
     The font is loaded when initializing Oduvanchik."
    :value "-*-fixed-*-*-normal-*-*-*-100-100-*-*-iso10646-*" ;"*-courier-bold-r-normal--*-120-*"
    )
  (defhvar "Thumb Bar Meter"
    "When non-nil (the default), windows will be created to be displayed with
     a ruler in the bottom border of the window."
    :value t)

  (setf *key-event-history* (make-ring 60))
  nil)


;;;; Some generally useful file-system functions.

;;; MERGE-RELATIVE-PATHNAMES takes a pathname that is either absolute or
;;; relative to default-dir, merging it as appropriate and returning a definite
;;; directory pathname.
;;;
;;; This function isn't really needed anymore now that merge-pathnames does
;;; this, but the semantics are slightly different.  So it's easier to just
;;; keep this around instead of changing all the uses of it.
;;;
(defun merge-relative-pathnames (pathname default-directory)
  "Merges pathname with default-directory.  If pathname is not absolute, it
   is assumed to be relative to default-directory.  The result is always a
   directory."
  (let ((pathname (merge-pathnames pathname default-directory)))
    (if (directoryp pathname)
        pathname
        (pathname (concatenate 'simple-string
                               (namestring pathname)
                               "/")))))

(defun directoryp (pathname)
  "Returns whether pathname names a directory, that is whether it has no
   name and no type components."
  (not (or (pathname-name pathname) (pathname-type pathname))))



;;;; I/O specials and initialization

;;; File descriptor for the terminal.
;;;
(defvar *editor-file-descriptor*)


;;; This is a hack, so screen can tell how to initialize screen management
;;; without re-opening the display.  It is set in INIT-RAW-IO and referenced
;;; in WINDOWED-MONITOR-P.
;;;
(defvar *editor-windowed-input* nil)

;;; These are used for selecting X events.
(declaim (special *editor-input* *real-editor-input*))

(declaim (special *editor-input* *real-editor-input*))

;;; INIT-RAW-IO  --  Internal
;;;
;;;    This function should be called whenever the editor is entered in a new
;;; lisp.  It sets up process specific data structures.
;;;
(defvar *default-backend* nil) ;if not set, use the CAR of available ones
(defvar *available-backends* '())

(defun validate-backend-type (want)
  (find want *available-backends*))

(defun choose-backend-type (&optional display)
  (declare (ignorable display))
  #+oduvan-invisible
  :invisible-tty 
  #-oduvan-invisible
  (let ((want
         (if display
             ;; $DISPLAY is set, can use the preferred display
             *default-backend*
             ;; $DISPLAY unset, revert to TTY
             :tty)))
    (cond
      ((validate-backend-type want))
      ((car *available-backends*))
      (t (error "no Oduvanchik backends loaded, giving up")))))

(defgeneric backend-init-raw-io (backend-type display))

(defmethod backend-init-raw-io ((backend (eql :tty)) display)
  (declare (ignore display))
  ;; The editor's file descriptor is Unix standard input (0).
  ;; We don't need to affect system:*file-input-handlers* here
  ;; because the init and exit methods for tty redisplay devices
  ;; take care of this.
  ;;
  (setf *editor-file-descriptor* 0)
  (setf *editor-input* (make-tty-editor-input :fd 0))
  (setf *real-editor-input* *editor-input*))

(defmethod backend-init-raw-io ((backend (eql :mini)) display)
  (backend-init-raw-io :tty display))

(defmethod backend-init-raw-io ((backend (eql :invisible-tty)) display)
  (backend-init-raw-io :tty display))


(defmethod backend-init-raw-io ((backend (eql :invisible)) display)
  (declare (ignore backend display))
  )

(defun init-raw-io (backend-type display)
  (setf *editor-windowed-input* nil)
  (backend-init-raw-io backend-type display))

;;; Stop flaming from compiler due to CLX macros expanding into illegal
;;; declarations.
;;;
(declaim (special *default-font-family*))

;;; font-map-size should be defined in font.lisp, but SETUP-FONT-FAMILY would
;;; assume it to be special, issuing a nasty warning.
;;;
#+clx
(defconstant font-map-size 16
  "The number of possible fonts in a font-map.")
#-clx
(defconstant font-map-size 19)


;;;; ODUVANCHIK-BEEP.

(defvar *editor-bell* (make-string 1 :initial-element #\bell))

;;; TTY-BEEP is used in Oduvanchik for beeping when running under a terminal.
;;; Send a #\bell to unix standard output.
;;;
#+NIL
(defun tty-beep (&optional device stream)
  (declare (ignore device stream))
  (when (variable-value 'oduvanchik::bell-style)
    (unix:unix-write 1 *editor-bell* 0 1)))

(declaim (special *current-window*))

#+clx
(declaim (special *foreground-background-xor*))

(defun oduvanchik-beep (stream)
  "Using the current window, calls the device's beep function on stream."
  (cond
    ((tcl-interface-active nil)
     (call-tcl-simple "bell"))
    (t
     (let ((device (device-hunk-device (window-hunk (current-window)))))
       (device-beep device stream)))))


;;; *BEEP-FUNCTION* and BEEP are in SYSTEM package in CMUCL.
;;;
(defvar *beep-function*
  #'(lambda (&optional stream)
      (declare (ignorable stream))
      (cond
        ((tcl-interface-active nil)
         (call-tcl-simple "bell"))
        (t
         (print "BEEP!" *trace-output*)
         (finish-output *trace-output*)))))

(defun beep (&optional (stream *terminal-io*))
  (funcall *beep-function* stream))

; test - to do in clcon: (let ((oi::*tcl-interface-active* t)) (oi:beep))


;;;; GC messages.

;;; ODUVANCHIK-GC-NOTIFY-BEFORE and ODUVANCHIK-GC-NOTIFY-AFTER both MESSAGE GC
;;; notifications when Oduvanchik is not running under X11.  It cannot affect
;;; its window's without using its display connection.  Since GC can occur
;;; inside CLX request functions, using the same display confuses CLX.
;;;

(defun oduvanchik-gc-notify-before (bytes-in-use)
  (let ((control "~%[GC threshold exceeded with ~:D bytes in use.  ~
                  Commencing GC.]~%"))
    (cond ((not hi::*editor-windowed-input*)
           (beep)
           #|(message control bytes-in-use)|#)
          (t
           ;; Can't call BEEP since it would use Oduvanchik's display connection.
           #+nil (lisp::default-beep-function *standard-output*)
           (format t control bytes-in-use)
           (finish-output)))))

(defun oduvanchik-gc-notify-after (bytes-retained bytes-freed trigger)
  (let ((control
         "[GC completed with ~:D bytes retained and ~:D bytes freed.]~%~
          [GC will next occur when at least ~:D bytes are in use.]~%"))
    (cond ((not hi::*editor-windowed-input*)
           (beep)
           #|(message control bytes-retained bytes-freed)|#)
          (t
           ;; Can't call BEEP since it would use Oduvanchik's display connection.
           #+nil (lisp::default-beep-function *standard-output*)
           (format t control bytes-retained bytes-freed trigger)
           (finish-output)))))



;;;; Site-Wrapper-Macro and standard device init/exit functions.

(defun in-oduvanchik-standard-input-read (stream &rest ignore)
  (declare (ignore ignore))
  (error "You cannot read off this stream while in Oduvanchik -- ~S"
         stream))

(defvar *illegal-read-stream*
  #+CMU (lisp::make-lisp-stream :in #'in-oduvanchik-standard-input-read)
  #-CMU (make-broadcast-stream))

(declaim (special *gc-notify-before*
                  *gc-notify-after*))

;; fixme: this is neither site-specific nor should it be a macro.
(defmacro site-wrapper-macro (&body body)
  `(unwind-protect
     (progn
       (when *editor-has-been-entered*
         (let ((device (device-hunk-device (window-hunk (current-window)))))
            (device-init device)))
       (let ((*beep-function* #'oduvanchik-beep)
             (*gc-notify-before* #'oduvanchik-gc-notify-before)
             (*gc-notify-after* #'oduvanchik-gc-notify-after))
         (cond ((not *editor-windowed-input*)
                ,@body)
               (t
                (oduvanchik-ext:with-clx-event-handling
                    (*editor-windowed-input* #'oduvanchik-ext:object-set-event-handler)
                  ,@body)))))
     (let ((device (device-hunk-device (window-hunk (current-window)))))
       (device-exit device))))

(declaim (special *echo-area-window*))


;;;; Line Wrap Char.

(defvar *line-wrap-char* #\!
  "The character to be displayed to indicate wrapped lines.")


;;;; Showing a mark.

(defun show-mark (mark window time)
  "Highlights the position of Mark within Window for Time seconds,
   possibly by moving the cursor there.  If Mark is not displayed within
   Window return NIL.  The wait may be aborted if there is pending input."
  #+oduvan-invisible
  (warn "#+oduvan-invisible oi::show-mark ~S ~S ~S" mark window time)
  #-oduvan-invisible
  (let* ((result t))
    (catch 'redisplay-catcher
      (redisplay-window window)
      (setf result
            (multiple-value-bind (x y) (mark-to-cursorpos mark window)
              (device-show-mark (device-hunk-device (window-hunk window))
                                window x y time))))
    result))


;;;; Function description and defined-from.

;;; FUN-DEFINED-FROM-PATHNAME takes a symbol or function object.  It
;;; returns a pathname for the file the function was defined in.  If it was
;;; not defined in some file, then nil is returned.
;;;
#-(or cmu scl)
(defun fun-defined-from-pathname (function)
  "Takes a symbol or function and returns the pathname for the file the
   function was defined in.  If it was not defined in some file, nil is
   returned."
  (break "fix-conium - didn't run this code on swank yet - budden")
  (let ((location (swank::find-source-location
                   (if (functionp function)
                       function
                       (fdefinition function)))))
    (when (alexandria:starts-with :location location)
      (let ((file (second location)))
        (when (alexandria:starts-with :file file)
          (pathname (second file)))))))
#+(or cmu scl)
(defun fun-defined-from-pathname (function)
  "Takes a symbol or function and returns the pathname for the file the
   function was defined in.  If it was not defined in some file, nil is
   returned."
  (flet ((frob (code)
              (let ((info (kernel:%code-debug-info code)))
                     (when info
                              (let ((sources (c::debug-info-source info)))
                                 (when sources
                                      (let ((source (car sources)))
                                             (when (eq (c::debug-source-from source) :file)
                                                      (c::debug-source-name source)))))))))
    (typecase function
      (symbol (fun-defined-from-pathname (fdefinition function)))
      (kernel:byte-closure
       (fun-defined-from-pathname (kernel:byte-closure-function function)))
      (kernel:byte-function
       (frob (c::byte-function-component function)))
      (function
       (frob (kernel:function-code-header (kernel:%function-self function))))
      (t nil))))


(defvar *editor-describe-stream*
  (#+CMU system:make-indenting-stream #-CMU progn *standard-output*))

;;; EDITOR-DESCRIBE-FUNCTION has to mess around to get indenting streams to
;;; work.  These apparently work fine for DESCRIBE, for which they were defined,
;;; but not in general.  It seems they don't indent initial text, only that
;;; following a newline, so inside our use of INDENTING-FURTHER, we need some
;;; form before the WRITE-STRING.  To get this to work, I had to remove the ~%
;;; from the FORMAT string, and use FRESH-LINE; simply using FRESH-LINE with
;;; the ~% caused an extra blank line.  Possibly I should not have glommed onto
;;; this hack whose interface comes from three different packages, but it did
;;; the right thing ....
;;;
;;; Also, we have set INDENTING-STREAM-STREAM to make sure the indenting stream
;;; is based on whatever *standard-output* is when we are called.
;;;
(defun editor-describe-function (fun sym)
  "Calls DESCRIBE on fun.  If fun is compiled, and its original name is not sym,
   then this also outputs any 'function documentation for sym to
   *standard-output*."
  sym
  (describe fun)
  #+GBNIL
  (when (and (compiled-function-p fun)
             (not (eq (kernel:%function-name (kernel:%closure-function fun))
                      sym)))
    (let ((doc (documentation sym 'function)))
      (when doc
        (format t "~&Function documentation for ~S:" sym)
        (setf (lisp::indenting-stream-stream *editor-describe-stream*)
              *standard-output*)
        (ext:indenting-further *editor-describe-stream* 2
          (fresh-line *editor-describe-stream*)
          (write-string doc *editor-describe-stream*))))))




;;;; X Stuff.
;;; Setting window cursors ...
;;;


;;;; Some hacks for supporting Oduvanchik under Mach.

;;; WINDOWED-MONITOR-P is used by the reverse video variable's hook function
;;; to determine if it needs to go around fixing all the windows.
;;;
(defun windowed-monitor-p ()
  "This returns whether the monitor is being used with a window system.  It
   returns the console's CLX display structure."
  *editor-windowed-input*)

(defun process-editor-tty-input (&optional fd)
  (declare (ignore fd))
  ;; no-op
  )

(defvar *tty-translations* (make-hash-table :test #'equal))

(defun register-tty-translation (string keysym &key kludge)
  (when kludge
    ;; FIXME: This is pretty terrible, but for some reason my *terminfo* has
    ;; Esc,O,<foo> for arraw keys, whereas terminal actually sends Esc,[,<foo>
    ;; -- either I don't understand how terminfo stuff is supposed to work,
    ;; Apple ships with a broken terminfo db, or if something is wrong with
    ;; the terminfo code. I'm inclined to blame me...
    (assert (eq #\O (char string 1)))
    (setf string (format nil "~A[~A" (char string 0) (subseq string 2))))
  (setf (gethash (string string) *tty-translations*) keysym))

(defun translate-tty-event (data)
  (let* ((string (coerce data 'string))
         (sym (gethash string *tty-translations*)))
    (if sym
        (etypecase sym
          (oduvanchik-ext:key-event sym)
          (t (oduvanchik-ext:make-key-event sym 0)))
        (when (= 1 (length string))
          (oduvanchik-ext:char-key-event (char string 0))))))

(defun tty-key-event (data)
  (loop with start = 0
        with length = (length data)
        while (< start length)
        do (loop for end from length downto (1+ start)
                 do (let ((event (translate-tty-event (subseq data start end))))
                      (when event
                        (q-event *real-editor-input* event)
                        (setf start end)
                        (return)))))
  #+nil
  (iter:iter (iter:for char in-vector data)
             (let ((sym
                    (cond
                      ((eql char #\newline) ;### hmm
                       (oduvanchik-ext:key-event-keysym #k"Return"))
                      ((eql char #\tab) ;### hmm
                       (oduvanchik-ext:key-event-keysym #k"Tab"))
                      ((eql char #\Backspace)
                       (oduvanchik-ext:key-event-keysym #k"Backspace"))
                      ((eql char #\Escape)
                       (oduvanchik-ext:key-event-keysym #k"Escape"))
                      ((eql char #\rubout)
                       (oduvanchik-ext:key-event-keysym #k"delete")))))
               (q-event *real-editor-input*
                        (if sym
                            (oduvanchik-ext:make-key-event sym 0)
                            (oduvanchik-ext:char-key-event char))))))

#||
;;; GET-EDITOR-TTY-INPUT reads from stream's Unix file descriptor queuing events
;;; in the stream's queue.
;;;
#+NIL
(defun editor-tty-listen (stream)
  (alien:with-alien ((nc c-call:int))
    (and (unix:unix-ioctl (tty-editor-input-fd stream)
                          unix::FIONREAD
                          (alien:alien-sap (alien:addr nc)))
         (> nc 0))))
||#


;;;;
;;;; PREPL-integration
;;;;

#-oduvan-invisible
(defun prepl-oduvanchik-command-integration-hook (cmd override)
  (multiple-value-bind (fun parsing)
                       (find-override-for-prepl cmd (or override t))
    (cond
     (fun (values fun parsing))
     ((not override) (find-command-for-prepl-normally cmd))
     (nil))))

#-oduvan-invisible
(defparameter *prepl-command-overrides*
  ;; List of (prepl-command-name oduvanchik-command-name)
  ;;      or (prepl-command-name oduvanchik-function-name)
  ;;
  ;; Oduvanchik commands (first form) are run in the master.
  ;; Oduvanchik functions (second form) are called directly in the slave (!).
  ;;
  '(("apropos" "Slave Apropos Ignoring Point")
    ("bt" oduvanchik::debug-using-master)
    ("zoom" oduvanchik::debug-using-master)
    ("help" call-command-with-redirection)))

#-oduvan-invisible
(defun call-with-typeout-pop-up-in-master (fun buffer-name)
  (let* ((buffer-name (or buffer-name "Unnamed typescript"))
         (ts-data
          (oduvanchik.wire:remote-value
           oduvanchik.wire::*current-wire*
           (oduvanchik::%make-extra-typescript-buffer buffer-name)))
         (stream
          ;; (oduvanchik::make-ts-stream oduvanchik.wire::*current-wire* ts-data)
          (oduvanchik::connect-stream ts-data)))
    (funcall fun stream)))

#-oduvan-invisible
(defmacro with-typeout-pop-up-in-master
    ((var &optional buffer-name) &body body)
  `(call-with-typeout-pop-up-in-master (lambda (,var) ,@body)
                                       ,buffer-name))

#-oduvan-invisible
(defun call-with-standard-synonym-streams (fun)
  (let ((*standard-input* (make-synonym-stream '*terminal-io*))
        (*standard-output* (make-synonym-stream '*terminal-io*))
        (*error-output* (make-synonym-stream '*terminal-io*))
        (*debug-io* (make-synonym-stream '*terminal-io*))
        (*query-io* (make-synonym-stream '*terminal-io*)))
    (funcall fun)))

#-oduvan-invisible
(defun call-command-with-redirection ()
  (with-typeout-pop-up-in-master (*terminal-io* "Command output")
    (call-with-standard-synonym-streams #'prepl:call-next-command)))

#-oduvan-invisible
(defun find-override-for-prepl (cmd override)
  (let* ((cons (assoc cmd *prepl-command-overrides* :test 'string-equal))
         (mapping (cadr cons)))
    (when cons
      (typecase mapping
        (symbol
         (values (lambda (&rest args) (apply mapping args)) override))
        (t
         (let ((cmd (getstring mapping hi::*command-names*)))
           (when cmd
             (let ((sym (command-function cmd)))
               (check-type sym symbol)
               (values (lambda (&rest args)
                         (oduvanchik::eval-in-master `(,sym nil ,@args)))
                       override)))))))))

#-oduvan-invisible
(defun find-command-for-prepl-normally (cmd)
  ;; Note: Using lisp syntax for command names here (with dashes) rather
  ;; than real commands to make tokenization in the REPL easier.  This
  ;; way, the point where arguments start is never dependent on which
  ;; commmands are defined.
  ;;
  (let* ((sym (find-symbol (concatenate 'string
                                        (canonical-case cmd)
                                        (symbol-name '#:-command))
                           :oduvanchik))
         (fun (and sym (fdefinition sym))))
    (when fun
      (values (lambda (&rest args)
                (oduvanchik::eval-in-master `(funcall ',sym nil ,@args)))
              t))))

#-oduvan-invisible
(push 'prepl-oduvanchik-command-integration-hook prepl:*command-parser-hooks*)
