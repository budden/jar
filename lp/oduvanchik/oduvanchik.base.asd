;;;; -*- Mode: Lisp; indent-tabs-mode: nil -*-

; disabling this because we're debugging stepper
; (proclaim '(optimize (safety 3) (speed 0) (debug 3)))

(defpackage #:oduvanchik-system
  (:use #:cl)
  (:export #:*oduvanchik-base-directory*))

(in-package #:oduvanchik-system)

(defvar *modern-oduvanchik* nil)
#+cmu (let ((packages (remove-if-not #'find-package
                                     '(:oduvanchik :oduvanchik-internals :wire))))
        (when (and packages (not *modern-oduvanchik*))
          (cerror "Continue and delete the old packages"
                  "It looks like you're trying to load a modern version of ~
                 oduvanchik into a CMUCL image that already has an old oduvanchik ~
                 present.  Hit the restart to replace all old packages.")
          (mapc #'delete-package packages)))
(setf *modern-oduvanchik* t)

(pushnew :command-bits *features*)

(defparameter *oduvanchik-base-directory*
  (make-pathname :name nil :type nil :version nil
                 :defaults (parse-namestring *load-truename*)))

(defparameter *binary-pathname*
  (make-pathname :directory
                 (append (pathname-directory *oduvanchik-base-directory*)
                         (list "bin"
                               #+CLISP "clisp"
                               #+CMU   "cmu"
                               #+EXCL  "acl"
                               #+SBCL  "sbcl"
                               #+scl   "scl"
                               #-(or CLISP CMU EXCL SBCL scl)
                               (string-downcase (lisp-implementation-type))))
                 :defaults *oduvanchik-base-directory*))

(asdf:defsystem :oduvanchik.base
    :pathname #.(make-pathname
                        :directory
                        (pathname-directory *oduvanchik-base-directory*)
                        :defaults *oduvanchik-base-directory*)
    :depends-on (#-scl :alexandria
                  :bordeaux-threads
                  ; :conium - conium depends on it already
                  :trivial-gray-streams
                  :iterate
                  #-oduvan-invisible :prepl 
                  #-oduvan-invisible :osicat
                  :babel
                  #-oduvan-invisible :iolib
                  #-oduvan-invisible :iolib/os
                  :cl-ppcre
                  :named-readtables
                  :budden-tools
                  :buddens-reader
                  :split-sequence
                  ; Проблема - обновился asdf, а эта библиотека нет. За ней потянется обновление quicklisp - не хотим. #-scl :command-line-arguments
                  )
    :serial t
    :components
    ((:module wire
              :pathname #.(merge-pathnames
                           (make-pathname
                            :directory '(:relative "src"))
                           *oduvanchik-base-directory*)
              :depends-on ()
              :serial t
              :components
              ((:file "wire-package")
               (:file "port")
               (:file "wire")
               (:file "remote")))
    (:module core-1
              :pathname #.(merge-pathnames
                           (make-pathname
                            :directory '(:relative "src"))
                           *oduvanchik-base-directory*)
              :depends-on (wire)
              :components
              ((:file "package")
               ;; Lisp implementation specific stuff goes into one of the next
               ;; two files.
               (:file "oduvanchik-ext-readtable")
               (:file "tcl-interface-1" :description "early declarations for tcl interface")
               (:file "lispdep" :depends-on ("package"))
               (:file "oduvanchik-ext" :depends-on ("package"))

               (:file "decls" :depends-on ("package")) ; early declarations of functions and stuff
               (:file "struct" :depends-on ("package"))
               (:file "charmacs" :depends-on ("package"))
               (:file "key-event" :depends-on ("package" "charmacs"))
               ))
     (:module bitmap-1
              :pathname #.(merge-pathnames
                           (make-pathname
                            :directory '(:relative "src"))
                           *oduvanchik-base-directory*)
              :depends-on (core-1)
              :components
              ((:file "keysym-defs") ; hmm.
               ))
     (:module core-2
              :pathname #.(merge-pathnames
                           (make-pathname
                            :directory '(:relative "src"))
                           *oduvanchik-base-directory*)
              :depends-on (bitmap-1 core-1)
              :serial t                 ;...
              :components
              ((:file "rompsite")
               (:file "input")
               (:file "macros")
               (:file "line")
               (:file "ring")
               (:file "htext1") ; buffer depends on it --amb
               (:file "buffer")
               (:file "do-editing-on-tcl-side-interface" :description "Billets for functions for doing editing on tcl side, they will be really implemented in :clcon-server system")
               (:file "vars")
               (:file "interp")
               (:file "syntax")
               (:file "htext2")
               (:file "htext3")
               (:file "htext4")
               (:file "files")
               (:file "search1")
               (:file "search2")
               (:file "table")

               (:file "winimage")
               (:file "window")
               (:file "screen")
               (:file "linimage")
               (:file "cursor")
               (:file "display")
               (:file "exp-syntax-vm")
               (:file "exp-syntax-macros")
               (:file "exp-syntax")
               (:file "connections")
               ; #+oduvan-invisible
               (:file "dummy-connection" :description "buddens dummy backend for invisible oduvan")
               ))
     (:module root-1
              :pathname #.(merge-pathnames
                           (make-pathname
                            :directory '(:relative "src"))
                           *oduvanchik-base-directory*)
              :depends-on (core-2 core-1)
              :components
              ((:file "pop-up-stream")))
     (:module root-2
              :pathname #.(merge-pathnames
                           (make-pathname
                            :directory '(:relative "src"))
                           *oduvanchik-base-directory*)
              :depends-on (root-1 core-1 wire)
              :components
              ((:file "font")
               (:file "streams")
               (:file "main")
               (:file "echo")
               (:file "new-undo" :description "Contains not only undo, but also code to update syntax tags. Can't be disabled. But we can disable buffer-undo-p")
               ))
     (:module core-3
              :pathname #.(merge-pathnames
                           (make-pathname
                            :directory '(:relative "src"))
                           *oduvanchik-base-directory*)
              :depends-on (bitmap-1 core-1 core-2)
              :components
              ((:file "typeout")))
     (:module user-1
              :pathname #.(merge-pathnames
                           (make-pathname
                            :directory '(:relative "src"))
                           *oduvanchik-base-directory*)
              :depends-on (root-2 core-1 wire)
              :serial t
              :components
              ((:file "echocoms")

               (:file "command")
               (:file "kbdmac")
               (:file "undo")
               (:file "killcoms")
               (:file "searchcoms")
               (:file "filecoms")

               (:file "indent" :depends-on ("filecoms"))
               (:file "grep" :depends-on ("filecoms"))
               (:file "apropos" :depends-on ("filecoms"))
               (:file "morecoms")
               (:file "doccoms" :description "Oduvanchik Documentation and Help commands.")
               #+full-oduvan (:file "srccom" :description "Source comparison stuff for Oduvanchik.")
               ;(:file "group" :description "This file provides Oduvanchik commands for manipulating groups of files that make up a larger system. A file group is a set of files whose
;;; names are listed in some other file.")
               #+full-oduvan (:file "fill" :description "Auto Fill Mode")
               (:file "text")

               (:file "defsyn")

               (:file "lispmode")
               ;(:file "ts-buf" :description "processing input to and output from slaves using typescript streams")
               ;(:file "ts-stream" :description "A typescript stream is a bidirectional stream which uses remote function calls to interact with a Oduvanchik typescript buffer. That is: the code in this file is executed on the slave side.")
               ;(:file "request" :description "Just a simple handle on the socket and system:serve-event handler that make up a request server.")
               ; #+fix-conium (:file "eval-server" :description "code for connecting to eval servers and some command level stuff too.")
               (:file "lispbuf" :depends-on ("filecoms"))
                                        ;(:file "lispeval" :depends-on ("eval-server"))
               (:file "lispeval")
               #+full-oduvan (:file "spell-rt")
               #+full-oduvan (:file "spell-corr" :depends-on ("spell-rt"))
               #+full-oduvan (:file "spell-aug" :depends-on ("spell-corr"))
               #+full-oduvan (:file "spellcoms" :depends-on ("spell-aug" "filecoms"))
               #+full-oduvan (:file "spell-build" :depends-on ("spell-aug"))

               (:file "comments")
               (:file "overwrite")
               ; (:file "abbrev") ; port to new event model.
               ; (:file "icom" :description "italicized comment")
               (:file "scribe")
               (:file "pascal")
               (:file "tcl-mode")
               #+full-oduvan (:file "dylan" :depends-on ("filecoms"))

               ; (:file "edit-defs" :description "Editing DEFMACRO and DEFUN definitions.")
               #+full-oduvan (:file "register" :description "Registers for holding text and positions.")
               #+full-oduvan (:file "xcoms" :description "X related features")
               #+port-user-unixcoms (:file "unixcoms")
               (:file "highlight")
               (:file "dired")
               (:file "diredcoms" :depends-on ("dired"))
               (:file "bufed")
               ;(:file "coned" :description "(Connection Editing)")
               #+fix-conium (:file "xref")
               #+port-user-lisp-lib (:file "lisp-lib")
               #+full-oduvan (:file "completion" :depends-on ("lispmode"))
               #+full-oduvan (:file "cpc")
               #+fix-conium (:file "fuzzy" :depends-on ("cpc"))
               #+full-oduvan (:file "shell")
               #+fix-conium (:file "debug")
               #+port-user-netnews (:file "netnews")
               #+port-user-rcs (:file "rcs")
               (:file "dabbrev")
               (:file "bindings")
               #+full-oduvan (:file "slave-list")))
     ))
