#!/bin/sh
unset backends

SBCL=${SBCL:-clbuild lisp}

if test $# -eq 0; then
	cat <<eof
Building backends tty and clx.
(Specify backend types at the command line to override this default.)
eof
else
	while test $# -gt 0; do
		case $1 in
			tty|clx|qt)
				backends="$backends :oduvanchik.$1"
				echo backend $1 enabled
				shift
				;;
			*)
				echo invalid backend type $1
				exit 1
				;;
		esac
	done
fi


$SBCL <<EOF
;; NOTE: the order in which clx and tty are given matters.
;; The last backend loaded is the default, and only if no $DISPLAY is
;; specified, main.lisp has special-cased the tty backend as a fallback,
;; so CLX must come last to have a chance of overriding it.
;;
(dolist (system (or '($backends) '(:oduvanchik.tty :oduvanchik.clx)))
  (asdf:operate 'asdf:load-op system))

(defun oduvanchik-toplevel ()
  #+ccl (when (find-package :qt) (funcall (find-symbol "REBIRTH" :qt)))
  (let ((argv0 (car (command-line-arguments:get-command-line-arguments)))) 
    (setf hi::*installation-directory*
	  (concatenate 
	   'string
	   (iolib.pathnames:file-path-directory argv0 :namestring t)
	   "/"))
    (setf oduvanchik::*slave-command* (list argv0 "--slave"))
    (oduvanchik:main))
  (quit))

(sb-ext:save-lisp-and-die "oduvanchik"
                          :save-runtime-options t
			  :toplevel 'oduvanchik-toplevel
			  :executable t)
EOF
