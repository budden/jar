;; topological sort by Ingvar, src.hexapodia.net
;; modified by budden.
;; Added :test to tsort-plain. tsort-hashes might die, but it seem just to ignore :test


(cl:defpackage "NET.HEXAPODIA.TOPOSORT" 
  (:nicknames "TOPOSORT")
  (:export "TSORT-PLAIN")
  (:use "CL"))

(cl:in-package :net.hexapodia.toposort)

(defclass tsort-item ()
  ((name :reader name :initarg :name)
   (used :accessor used :initarg :used :initform nil)
   (pres :accessor pres :initarg :pres :initform nil)
   (posts :accessor posts :initarg :posts :initform nil)
   ))

(defun canonicalize-db (db)
  (loop for dep in db
	append (loop with pre = (car dep)
		     for post in (cdr dep)
		     collect (list pre post))))

(defun tsort-plain (db &key (test #'eql)) "??跥??谳岠????'((a b) (a c) (b c))"
  (tsort-plain-1 (canonicalize-db db) nil test))

(defun tsort-plain-1 (db result test)
  (if (null db)
       (nreverse result)
    (let* ((candidates (find-candidates-plain db nil nil test))
	   (next-one (car candidates)))
      (if (null candidates)
	  'error
	(tsort-plain-1 (remove-from-db db next-one test) (cons next-one result) test)))))

(defun cons-if-new (item list test)
  (if (member item list :test test)
      list
    (cons item list)))
  
(defun find-candidates-plain (db candidates non-candidates test)
  (if (null db)
      (values candidates non-candidates)
    (let ((head (car db))
	  (tail (cdr db)))
      (let ((pre (car head))
	    (post (cadr head)))
	(when (funcall test pre post)
	  (setf post nil))
	(if (member pre non-candidates :test test)
	    (setf candidates (delete pre candidates :test test))
	  (push pre candidates))
	(when (member post candidates :test test)
	  (setf candidates (delete post candidates :test test)))
	(find-candidates-plain tail candidates (cons-if-new post non-candidates test) test)))))

(defun remove-from-db (db pre test)
  (let ((posts nil))
    (flet ((checker (sym list)
	     (let ((pre (car list))
		   (post (cadr list)))
	       (when (and (funcall test sym pre) post)
		 (unless (funcall test pre post)
		   (setf posts (cons-if-new post posts test)))
		 t))))
      (let ((new-db (remove pre db :test #'checker)))
	(loop for post in posts
	      do (if (not (member post new-db :key #'cadr :test test))
		     (push (list post post) new-db)))
	new-db))))

(defun tsort-hashes (db)
  (let ((hashtab (make-hash-table))
	(syms nil)
	(db (canonicalize-db db)))
    (loop for (pre post) in db
	  do (let ((pre-obj (gethash pre hashtab))
		   (post-obj (gethash post hashtab)))
	       (unless pre-obj
		 (setf pre-obj (make-instance 'tsort-item :name pre))
		 (setf (gethash pre hashtab) pre-obj)
		 (push pre syms))
	       (unless post-obj
		 (setf post-obj (make-instance 'tsort-item :name post))
		 (setf (gethash post hashtab) post-obj)
		 (push post syms))
	       (push post (posts pre-obj))
	       (push pre (pres post-obj))))
    (tsort-hashes-1 hashtab syms)))

(defun tsort-hashes-1 (hashtab todo &optional revlist)
  (if (null todo)
      (nreverse revlist)
    (let ((new-todo nil)
	  (emitted nil))
      (loop for sym in todo
	    do
	    (let ((obj (gethash sym hashtab)))
	      (cond ((used obj) (format t ";;; Something odd happened processing ~A, it has a used-flag set!~%" sym))
		    ((pres obj) (push sym new-todo))
		    (t (push sym revlist)
		       (setf (used obj) t)
		       (setf emitted t)
		       (loop for post in (posts obj)
			     do (let ((post-obj (gethash post hashtab)))
				  (setf (pres post-obj)
					(delete sym (pres post-obj)))))))))
      (when emitted
	(tsort-hashes-1 hashtab new-todo revlist)))))

