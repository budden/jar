(in-package #:net.hexapodia.toposort)

(defun random-data (limit)
  (loop for n from 1 to (truncate (expt limit 1.3))
	for a = (random limit)
	for b = (random limit)
	collect (if (< a b) (list a b) (list b a))))

(defvar *test-db-small* '((banan apa)
			  (tree apa)
			  (apa pinne)
			  (banan milkshake)))

(defvar *test-db-medium* '((main parse-options) (main tail-file)
			   (main tail-forever) (tail-file pretty-name)
			   (tail-file write-header) (tail-file tail)
			   (tail-forever recheck) (tail-forever pretty-name)
			   (tail-forever write-header)
			   (tail-forever dump-remainder) (tail tail-lines)
			   (tail tail-bytes) (tail-lines start-lines)
			   (tail-lines dump-remainder) (tail-lines file-lines)
			   (tail-lines pipe-lines) (tail-bytes xlseek)
			   (tail-bytes start-bytes) (tail-bytes dump-remainder)
			   (tail-bytes pipe-bytes) (file-lines dump-remainder)
			   (recheck pretty-name)))

(defvar *test-db-100* (random-data 100))
(defvar *test-db-200* (random-data 200))
(defvar *test-db-400* (random-data 400))
(defvar *test-db-800* (random-data 800))
(defvar *test-db-1600* (random-data 1600))


(defun extract-pres (sym db)
  (mapcan #'(lambda (list) (when (member sym (cdr list))
			     (remove sym (list (car list)))))
	  db))

(defun extract-posts (sym db)
  (mapcan #'(lambda (list) (when (eql sym (car list))
			     (remove sym (cdr list))))
	  db))

(defun verify-correct (fun db)
  (let ((sorted (funcall fun db)))
    (verify-correct-1 sorted db)))

(defun verify-correct-1 (sorted db)
  (if (null sorted)
      'ok
    (let ((sym (car sorted))
	  (tail (cdr sorted)))
      (let ((posts (extract-posts sym db))
	    (pres (extract-pres sym db)))
	(or (loop for datum in tail
		  if (member datum pres)
		  return 'false
		  if (member datum posts)
		  do (setf posts (delete datum posts)))
	    (if (not (null posts)) 'not-ok)
	    (verify-correct-1 tail db))))))
