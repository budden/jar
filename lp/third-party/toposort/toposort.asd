(asdf:defsystem "toposort"
  :version "0.1"
  :description "A package with a couple of tsort functions"
  :components ((:file "packages")
	       (:file "tsort" :depends-on ("packages"))))
