;;; -*- coding: utf-8; -*-

(in-package #:asdf)

(defsystem :def-struct-or-class
  :serial t
  :description "Uniform syntax for common features of defstruct and defclass. See def-struct-or-class:def-struct-or-class for docs"
  :license "MIT"
  :author "Denis Budyak"  
  :depends-on (:alexandria :named-readtables)
  :components
  ((:file "def-struct-or-class-package")
   (:file "def-struct-or-class"))
  :in-order-to ((test-op (test-op :def-struct-or-class-tests))))

(defsystem :def-struct-or-class-tests
  :serial t
  :depends-on (:def-struct-or-class)
  :components
  ((:file "def-struct-or-class-tests"))
  :perform (test-op (o c)
                    (asdf:load-system :def-struct-or-class-tests)
                    (symbol-call :def-struct-or-class '#:do-tests)))

