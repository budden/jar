;; -*- coding: utf-8; system: def-struct-or-class ; -*- 

(named-readtables:in-readtable nil)
(in-package :def-struct-or-class)


(defstruct parsed-slot-definition
  name
  initval
  type
  read-only
  ;; non-standard options
  (inline t))

(defstruct parsed-definition
  "Defaults for conc-name, constructor-name and so on are missing argument markers"
  name
  ;; non-standard options
  (implement-as 'structure-class :type (member structure-class standard-class))
  (make-load-form t)
  (package *package*) 
  ;; standard options
  (conc-name 0)
  (constructor-name t)
  (constructor-arglist t)
  include
  print-function
  (copier t)
  (predicate t)
  ;; docstring and slots
  (docstring nil :type (or null string))
  (slots nil :type list))

(defmacro ! (name-and-options &body docstring-and-slots)
  `(def-struct-or-class ,name-and-options ,@docstring-and-slots))

(defmacro def-struct-or-class (name-and-options &body docstring-and-slots)
  (let* ((parsed-definition
          (parse-def-struct-or-class-form name-and-options docstring-and-slots))
         (result (generate-defstruct-or-defclass-form parsed-definition)))
    result))


(defun parse-def-struct-or-class-form (name-and-options docstring-and-slots)
  (let* 
      ((name
        (etypecase name-and-options
          (symbol name-and-options)
          (list (car name-and-options))))
       (docstring
        (typecase (car docstring-and-slots)
          (string (pop docstring-and-slots))
          (t nil)))
       (unparsed-slots docstring-and-slots)
       (options
        (etypecase name-and-options
          (symbol nil)
          (list (cdr name-and-options))))
       ;; the same as options minus meta-options like :implement-as
       (pd (make-parsed-definition
            :name name
            :docstring docstring
            )))
    (with-slots (implement-as package conc-name constructor-name constructor-arglist include print-function copier predicate make-load-form slots) pd
      (dolist (option options)
        (let* ((o2 (etypecase option (symbol (list option)) (cons option)))
               (option-name (pop o2)))
          (ecase option-name
            ;; non-standard option
            (:implement-as
             (setf implement-as (pop o2)))
            (:package
             (error "Опция :package не работает")
             (let ((package-spec (pop o2)))
               (cond
                ((eql package-spec 1)
                 (setf package (symbol-package name)))
                (package-spec
                 (setf package (find-package package-spec)))
                (t
                 (error "Package designator must be present")))))
            ;; standard defstruct options
            (:conc-name
             (setf conc-name (pop o2)))
            (:constructor
             (setf constructor-name (pop o2))
             (when o2
               (setf constructor-arglist (pop o2))))
            (:copier
             (setf copier (pop o2)))
            (:predicate
             (setf predicate (pop o2)))
            (:include
             (setf include (pop o2)))
            (:print-function
             (setf print-function (pop o2))
             (unless print-function
               (error "def-struct-or-class does not support :print-function w/o a name")))
            (:make-load-form
             (setf make-load-form (pop o2)))
            ((:type :named :initial-offset :print-object)
             (warn "Option ~S is unsupported by (def-struct-or-class ~S ...)"
                   option-name name)))
          (assert (null o2) () "Junk after defstruct option: ~S" option)
          ))
      (setf slots (parse-def-struct-or-class-slots unparsed-slots)))
    pd))

(defun parse-def-struct-or-class-slots (unparsed-slots)
  (let* ((result nil))
    (dolist (unparsed-slot unparsed-slots)
      (let ((parsed-slot-definition (make-parsed-slot-definition))
            (u-p unparsed-slot))
        (with-slots (name initval type read-only inline) parsed-slot-definition
          (etypecase unparsed-slot
            (symbol (setf name unparsed-slot))
            (cons
             (setf name (pop u-p))
             (when u-p
               (setf initval (pop u-p))
               (loop
                 (unless u-p (return))
                 (let* ((maybe-option-name (pop u-p))
                        (option-name
                         (if (member maybe-option-name
                                     '(:type :read-only :inline))
                             maybe-option-name
                             (error "Unknown slot option name ~S in ~S" maybe-option-name unparsed-slot)))
                        (option-value
                         (if (null u-p)
                             (error "Missing value for ~A option in ~S" option-name unparsed-slot)
                             (pop u-p))))
                   (ecase option-name
                     (:type (setf type option-value))
                     (:read-only (setf read-only option-value))
                     (:inline (setf inline option-value)))))))))
        (push parsed-slot-definition result)))
    (nreverse result)))

(defun generate-defstruct-or-defclass-form (parsed-definition)
  (ecase (parsed-definition-implement-as parsed-definition)
    (standard-class
     (generate-defclass-form parsed-definition))
    (structure-class
     (generate-defstruct-form parsed-definition))))


(defun generate-defclass-form (parsed-definition)
  (let ((copy-of-parsed-definition (copy-structure parsed-definition)))
    (with-slots (name package conc-name constructor-name constructor-arglist copier predicate include print-function make-load-form docstring slots) copy-of-parsed-definition
      (when (eql conc-name 0)
        (setf conc-name (format-symbol package "~A-" name)))
      (when (eq constructor-name t)
        (setf constructor-name (format-symbol package "~A~A" 'make- name)))
      (when (eq copier t)
        (setf copier (format-symbol package "~A~A" 'copy- name)))
      (when (eq predicate t)
        (setf predicate (format-symbol package "~A~A" name '-p)))
      (when include
        (setf slots (append (get include 'def-clx-class) slots)))
      (let* ((n-slots (length slots))
             (slot-names (make-list n-slots))
             (slot-initforms (make-list n-slots))
             (slot-types (make-list n-slots))
             (slot-readonlies (make-list n-slots)))
        (dotimes (i n-slots)
          (let* ((parsed-slot-definition (elt slots i)))
            (with-slots (name initval type read-only) parsed-slot-definition
              ; inline is not applicable for GFs
              ; see also https://github.com/guicho271828/inlined-generic-function/ (LLGPL so we don't use)
              (setf (elt slot-names i) name)
              (setf (elt slot-initforms i) initval)
              (setf (elt slot-types i) (or type t))
              (setf (elt slot-readonlies i) read-only))))
        `(progn
           (defclass
               ,name ,(and include `(,include))
             (,@(map 'list
                     #'(lambda (slot-name slot-initform slot-type slot-readonly)
                         `(,slot-name
                           :initform ,slot-initform :type ,slot-type
                           ,(if slot-readonly :reader :accessor) ,(format-symbol package "~A~A" conc-name slot-name)
                           ,@(when (and constructor-name
                                        (or (eq constructor-arglist t)
                                            (member slot-name
                                                    constructor-arglist)))
                               `(:initarg ,(make-keyword slot-name)))
                           ))
                     slot-names slot-initforms slot-types slot-readonlies))
             ,@(when docstring `((:documentation ,docstring)))
             ) ; defclass
           ,(when constructor-name
              (if (eq constructor-arglist t)
                  `(defun ,constructor-name (&rest args)
                     (apply #'make-instance
                            ',name args))
                  `(defun ,constructor-name ,constructor-arglist
                     (make-instance ',name
                                    ,@(mapcan #'(lambda (slot-name)
                                                  (and (member slot-name slot-names)
                                                       `(,(make-keyword slot-name) ,slot-name)))
                                              constructor-arglist)))))
           ,(when predicate
              #+allegro
              `(progn
                 (defmethod ,predicate (object)
                   (declare (ignore object))
                   nil)
                 (defmethod ,predicate ((object ,name))
                   t))
              #-allegro
              `(defun ,predicate (object)
                 (typep object ',name)))
           ,(when copier
              `(defmethod ,copier ((.object. ,name))
                 (with-slots ,slot-names .object.
                   (make-instance ',name
                                  ,@(mapcan #'(lambda (slot-name)
                                                `(,(make-keyword slot-name) ,slot-name))
                                            slot-names)))))
           ,(when print-function
              `(defmethod print-object
                          ((object ,name) stream)
                 (,print-function object stream
                                  ;; 0 is an issue, see print-function option
                                  ;; description in the standard
                                  0)))
           ,@(make-load-form-method-def-list copy-of-parsed-definition)
           ',name)))))
  

(defun generate-defstruct-form (parsed-definition)
  (with-slots (name package conc-name constructor-name constructor-arglist copier predicate include print-function make-load-form docstring slots) parsed-definition
    (let* ((conc-name-option-list
            (cond
             ((eql conc-name 0) nil) ; default 
             ((null conc-name) '((:conc-name))) ; empty conc-name
             (t ; custom conc-name
              `((:conc-name ,conc-name)))))
           (constructor-option-list
            (cond
             ((eq constructor-name t) ; default constructor
              nil)
             ((null constructor-name) ; no constructor
              '((:constructor)))
             (t ; custom constructor
              `((:constructor 
                 ,constructor-name
                 ,@(if (eq constructor-arglist t)
                       nil
                       `(,constructor-arglist)))))))
           (copier-option-list
            (cond
             ((eq copier t) ; default copier
              nil)
             ((null copier) ; no copier
              '((:copier)))
             (t ; 
              `((:copier ,copier)))))
           (predicate-option-list
            (cond
             ((eq predicate t) ; default 
              nil)
             ((null predicate) ; none
              '((:predicate)))
             (t ; 
              `((:predicate ,predicate)))))
           (include-option-list (when include `((:include ,include))))
           (print-function-option-list (when print-function `((:print-function ,print-function))))
           (defstruct-options
               `(,@conc-name-option-list
                 ,@constructor-option-list
                 ,@copier-option-list
                 ,@predicate-option-list
                 ,@include-option-list
                 ,@print-function-option-list
                 )))
      (multiple-value-bind
          (slot-defs declarations)
          (generate-defstruct-slot-defs
           parsed-definition)
        
        `(progn
           ,@declarations
           (defstruct (,name ,@defstruct-options)
             ,@(when docstring (list docstring))
             ,@slot-defs)
           ,@(make-load-form-method-def-list parsed-definition)
           )))))

(defun make-load-form-method-def-list (parsed-definition)
  (with-slots (name make-load-form) parsed-definition
    (when make-load-form
      `((defmethod make-load-form ((self ,name) &optional env)
          (make-load-form-saving-slots self :environment env))))))


(defun generate-defstruct-slot-defs (parsed-definition)
  "Return two values: 
- slot definitions for defstruct, 
- list of global definitions (e.g. proclamations)"
  (let* ((slot-defs nil) (global-forms nil))
    (dolist (parsed-slot-definition (parsed-definition-slots parsed-definition))
      (with-slots (name initval type read-only inline) parsed-slot-definition
        (let* ((inline-word (if inline 'inline 'notinline))
               (generated-slot-def
                (cond
                 ((or initval type read-only)
                  `(,name
                     ,initval
                     ,@(when type `(:type ,type))
                     ,@(when read-only `(:type ,read-only))
                     ))
                 (t
                  name)))
               (inline-declaration
                `(,inline-word ,name)))
          (push generated-slot-def slot-defs)
          (push `(declaim ,inline-declaration) global-forms)
          (push `(proclaim ',inline-declaration) global-forms))))
    (values (reverse slot-defs) (reverse global-forms))))


       
