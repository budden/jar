;; -*- coding: utf-8; system: def-struct-or-class-tests; -*- 
;; 
;; Sorry, I'm completely dissatisfied by the abundance of test frameworks for CL
;; I have my own, but it is not on the quicklisp
;; So I use no test framework and even no special package
;; I use just plain functions and warn. 
;; (C) Denis Budyak 2017
;; #include MIT license

(named-readtables:in-readtable nil)
(in-package :def-struct-or-class)

(defun test-def-struct-or-class-1 ()
  (let* ((expected 
          '(PROGN
             (DEFCLASS C1 NIL
               ((F1 :INITFORM NIL :TYPE T :ACCESSOR C1-F1 :INITARG :F1)
                (F2 :INITFORM 1 :TYPE INTEGER :ACCESSOR C1-F2 :INITARG :F2))
               (:DOCUMENTATION "My struct"))
             (DEFUN MAKE-C1 (&REST ARGS) (APPLY #'MAKE-INSTANCE 'C1 ARGS))
             (DEFUN C1-P (OBJECT) (TYPEP OBJECT 'C1))
             NIL
             NIL
             (DEFMETHOD MAKE-LOAD-FORM ((SELF C1) &OPTIONAL ENV)
               (MAKE-LOAD-FORM-SAVING-SLOTS SELF :ENVIRONMENT ENV))
             'C1))
         (obtained
          (macroexpand '(def-struct-or-class (C1 (:copier nil) (:implement-as standard-class)) "My struct" F1 (F2 1 :type integer)))))
    (values (equal expected obtained) expected obtained)))

(defun test-def-struct-or-class-2 ()
  (let* ((expected
          '(PROGN
             (DECLAIM (INLINE F1))
             (PROCLAIM '(INLINE F1))
             (DECLAIM (NOTINLINE F2))
             (PROCLAIM '(NOTINLINE F2))
             (DEFSTRUCT (C1 (:COPIER)) "My struct" F1 (F2 1 :TYPE INTEGER))
             (DEFMETHOD MAKE-LOAD-FORM ((SELF C1) &OPTIONAL ENV)
               (MAKE-LOAD-FORM-SAVING-SLOTS SELF :ENVIRONMENT ENV))))
         (obtained
          (macroexpand '(def-struct-or-class (C1 (:copier) (:implement-as structure-class)) "My struct" F1 (F2 1 :type integer :inline nil)))))
    (values (equal expected obtained) expected obtained)))

(defun test-def-struct-or-class-3 ()
  (let* ((expected
          '(PROGN
             (DEFCLASS C1 NIL
               ((F1 :INITFORM NIL :TYPE T :ACCESSOR FIELD-OF-C1-NAMED-F1 :INITARG
                    :F1)))
             (DEFUN CREATE-C1 (&REST DEF-STRUCT-OR-CLASS::ARGS)
               (APPLY #'MAKE-INSTANCE 'C1 DEF-STRUCT-OR-CLASS::ARGS))
             (DEFUN C1-P (DEF-STRUCT-OR-CLASS::OBJECT)
               (TYPEP DEF-STRUCT-OR-CLASS::OBJECT 'C1))
             (DEFMETHOD COPY-C1 ((DEF-STRUCT-OR-CLASS::.OBJECT. C1))
               (WITH-SLOTS (F1)
                           DEF-STRUCT-OR-CLASS::.OBJECT.
                 (MAKE-INSTANCE 'C1 :F1 F1)))
             NIL
             (DEFMETHOD MAKE-LOAD-FORM
                        ((DEF-STRUCT-OR-CLASS::SELF C1) &OPTIONAL DEF-STRUCT-OR-CLASS::ENV)
               (MAKE-LOAD-FORM-SAVING-SLOTS DEF-STRUCT-OR-CLASS::SELF :ENVIRONMENT
                                            DEF-STRUCT-OR-CLASS::ENV))
             'C1))
         (obtained
          (macroexpand-1 '(def-struct-or-class::def-struct-or-class (c1 (:implement-as standard-class) (:conc-name field-of-c1-named-) (:constructor create-c1)) f1))))
    (values (equal expected obtained) expected obtained)))

(defun do-tests ()
  (format t "~&Running tests for def-struct-or-class. Test failures cause warnings")
  (unwind-protect
      (let ((*package* (find-package :def-struct-or-class)))
        (dolist (test '(test-def-struct-or-class-1
                        test-def-struct-or-class-2))
          (multiple-value-bind (success expected obtained) (funcall test)
            (unless success
              (warn "~&~A failed.
expected: ~S
obtained: ~S~%" (let ((*package* (find-package :keyword)))
                  (format nil "~S" test)) expected obtained)))))
    (format t "~&Tests for def-struct-or-class finished")))
