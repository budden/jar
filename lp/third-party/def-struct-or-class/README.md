# def-struct-or-class

CLOS classes are more flexible, but slower. 
DEFSTRUCTs have less options, but faster

`def-struct-or-class:def-struct-or-class` (aka `def-struct-or-class::!`) is a macro which unifies structs and class, so that
one can switch between either implementation by changing one option and rebuilding sources. 

Syntax is basically that of [defstruct](http://www.lispworks.com/documentation/lw61/CLHS/Body/m_defstr.htm)

with extras:

- `(:implement-as [ cl:structure-class | cl:standard-class ])` defstruct option controls if the type is created as a structure or a class
- `(:make-load-form <truth-value>)` defstruct option enables/disables the creation of make-load-form method for both defstruct and defclass. Default is T. 
- `(:package <package-designator>)` - a package where to intern all generated symbols. Number 1 here means `take the package of the name`. 
- `:inline <truth-value>` slot option controls whether STRUCT-FIELD function will be proclaimed inline or notinline for defstruct only

and omissions:

- the following defstruct options are ignored with a warning: `:type :named :initial-offset :print-object`
- `read-only` slot option is not completely functional for `cl:standard-class` implementation. One have no STRUCT-FIELD function to be reader (and no writer), but slot-value can still be used to write to read-only fields
- if there is no print-function, standard-class version will print as a normal CLOS object, not as a structure. 
- standard-class is not readable with `#S` syntax

Tests are scant. 
