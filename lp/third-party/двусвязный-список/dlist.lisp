;; -*- system :ДВУСВЯЗНЫЙ-СПИСОК; -*- 
;; Copyright (c) 2011-2012, Krzysztof Drewniak
;; All rights reserved.

;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are met:
;;     * Redistributions of source code must retain the above copyright
;;       notice, this list of conditions and the following disclaimer.
;;     * Redistributions in binary form must reproduce the above copyright
;;       notice, this list of conditions and the following disclaimer in the
;;       documentation and/or other materials provided with the distribution.
;;     * Neither the name of the <organization> nor the
;;       names of its contributors may be used to endorse or promote products
;;       derived from this software without specific prior written permission.

;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
;; DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
;; DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
;; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(named-readtables:in-readtable :buddens-readtable-a)
(in-package :Д-СПИСОК)

(defclass dlist (#| #+generic-sequences sequence |# standard-object)
  ;; отключили generic-sequences, но пусть побудет классом
  ((first :initarg :first :accessor %dlist-first)
   (last :initarg :last :accessor %dlist-last))
  (:documentation  "A class that represents a doubly-linked list"))

(defun dlist-first (dlist)
  "Gets the first `dcons' in a `dlist'. Здесь был полиморфизм"
  (%dlist-first dlist))

(defun dlist-last (dlist)
  "Gets the last `dcons' in a `dlist'. Здесь был полиморфизм"
  (%dlist-last dlist))

(defun (setf dlist-first) (val place)
  (setf (%dlist-first place) val))

(defun (setf dlist-last) (val place)
  (setf (%dlist-last place) val))

(defun dlist-cons-on (object dlist)
  "Returns a dlist whose elements are `object' and the elements of `dlist'. `dlist' is destructively modified. This is intended to have the same use as @code{(cons object list)} for regular lists."
  (let ((new-cons (dcons nil object dlist)))
    (setf (dcons-prev dlist) new-cons)
    new-cons))

(defun dcons-append (object dcons)
  "Creates a dcons whose `data' is `object' and appends it to `dcons', returning `dcons' with a pointer to the new dcons in `dcons''s next."
  (let ((new-dcons (dcons dcons object nil)))
    (setf (dcons-next dcons) new-dcons)
    dcons))

(defun dlist (&rest elements)
  "Returns a doubly-linked list (dlist) with the elements in `elements'"
  (cond
   ((null elements)
    (make-instance 'dlist :first nil :last nil))
   ((null (cdr elements)) ; длина 1
    (let* ((dcons (dcons nil (car elements) nil)) (dlist (make-instance 'dlist :first dcons :last dcons)))
      dlist))
   (t
    (let ((dlist (make-instance 'dlist)) (current-dcons nil))
      (setf (dlist-first dlist) (dcons nil (first elements) nil))
      (setf current-dcons (dlist-first dlist))
      (loop for i on (rest elements) do
        (setf current-dcons (dcons-next (dcons-append (car i) current-dcons)))
        (or (cdr i) (setf (dlist-last dlist) current-dcons)))
      dlist))))

(defun dlist= (dlist-1 dlist-2 &key (test 'eql))
  "Срванивает списки поэлементно, рекурсивно сравнивая элементы, являющиеся d-list-ами. Эл-ты, не являющиеся d-list-ами, сравниваются с помощью test"
  (do ((i (dlist-first dlist-1) (dcons-next i))
       (j (dlist-first dlist-2) (dcons-next j)))
      (nil)
    (cond
     ((and (null i) (null j))
      (return t))
     ((or (null i) (null j))
      (return nil))
     (t
      (let ((di (dcons-data i))
            (dj (dcons-data j)))
        (cond
         ((and (typep di 'dlist) (typep dj 'dlist))
          (unless (dlist= di dj :test test)
            (return nil)))
         (t
          (unless (funcall test di dj)
            (return nil)))))))))

(defun dlistp (object)
  "УБЕЙМЯ - сделать dlist структурой и dlist-p"
  (typep object 'dlist))

(defun dlist->list (dlist)
  "Converts a dlist to a list"
  (loop for i = (dlist-first dlist) then (dcons-next i) 
     while i collect (dcons-data i)))

(defun DLIST->LIST--С-ограничением-длины (dlist &key (predel-dliny 500))
  "Для отладки - мы не хотим, чтобы отладчик зациклился на кривых данных"
  (loop for i = (dlist-first dlist) then (dcons-next i)
     for count from 0
     while (< count predel-dliny)
     while i collect (dcons-data i)))

(defun nthdcons (n dlist &key from-end)
  "Returns the @code{n}th dcons in `dlist' (zero-based). If n is >= the length of the list, errs. If `from-end' is true, returns the @code{n}th dcons from the end."
  (let ((val (funcall (if from-end #'dlist-last #'dlist-first) dlist)))
    (dotimes (i n val)
      (unless val (error "Выход за границы двусвязного списка"))
      (setf val (if from-end (dcons-prev val) (dcons-next val))))))

(defun dlist-nth (n dlist &key from-end)
  "Returns the nth element of `dlist', as the primary value. If n is >= the length of the list, errs. The secondary value will be T if the value was actually found in the list, and NIL otherwise. If `from-end' is true, `dlist-nth' returns the @code{n}th element from the end, subject to the rules above."
  (let ((ret (nthdcons n dlist :from-end from-end)))
    (values (dcons-data ret) (not (not ret)))))

(defun (setf dlist-nth) (val n dlist &key from-end)
  "Sets the data of the nth dcons in `dlist' to `val'. If `from-end' is true, sets the @code{n}th element from the end."
  (setf (dcons-data (nthdcons n dlist :from-end from-end)) val))

(defmethod print-object ((object dlist) stream)
  (print-unreadable-object (object stream :type t)
    (format stream "~s" (dlist->list object))))

(defmethod describe-object ((dlist dlist) stream)
  (let ((*print-circle* t))
    (format stream "~&~S is a doubly-linked list (dlist) which has the elements ~%~S.~% Its first dcons is: ~%~S~%. Its last dcons is ~%~S~%" 
	    dlist (dlist->list dlist) (dlist-first dlist) (dlist-last dlist))))
