;; -*- system :ДВУСВЯЗНЫЙ-СПИСОК; -*- 
;; Copyright (c) 2011-2012, Krzysztof Drewniak
;; All rights reserved.

;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are met:
;;     * Redistributions of source code must retain the above copyright
;;       notice, this list of conditions and the following disclaimer.
;;     * Redistributions in binary form must reproduce the above copyright
;;       notice, this list of conditions and the following disclaimer in the
;;       documentation and/or other materials provided with the distribution.
;;     * Neither the name of the <organization> nor the
;;       names of its contributors may be used to endorse or promote products
;;       derived from this software without specific prior written permission.

;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
;; DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
;; DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
;; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(named-readtables:in-readtable :buddens-readtable-a)

(def-merge-packages::! :Д-СПИСОК
  (:use :cl :budden-tools
        )
  (:export "
   Д-СПИСОК:dcons
   Д-СПИСОК:dcons-next
   Д-СПИСОК:dcons-prev
   Д-СПИСОК:dcons-data
   Д-СПИСОК:dlist
   Д-СПИСОК:dlist-first
   Д-СПИСОК:dlist-last
   Д-СПИСОК:make-dlist
   ;Д-СПИСОК:next
   ;Д-СПИСОК:prev
   ;Д-СПИСОК:data
   Д-СПИСОК:dlist=
   Д-СПИСОК:dcons-p
   Д-СПИСОК:dlistp
   Д-СПИСОК:dlist-length
   Д-СПИСОК:nthdcons
   Д-СПИСОК:dlist-nth
   Д-СПИСОК:dlist->list
   Д-СПИСОК:DLIST->LIST--С-ограничением-длины
   Д-СПИСОК:dlist-append
   Д-СПИСОК:dlist-nconc
   Д-СПИСОК:dlist-push
   Д-СПИСОК:dlist-pop
   Д-СПИСОК:mapdlist
   Д-СПИСОК:mapdcons
   Д-СПИСОК:mapdcon
   Д-СПИСОК:dlist-reverse
   Д-СПИСОК:dodlist
   Д-СПИСОК:dodcons
   Д-СПИСОК:copy-dlist
   Д-СПИСОК:in-dlist
   Д-СПИСОК:on-dlist
   Д-СПИСОК:dlist-delete-dcons
   Д-СПИСОК:dlist-insert-before
   Д-СПИСОК:DLIST--Вставить-с-сохранением-порядка
   "
   ))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (when (find-package :sequence) (pushnew :generic-sequences *features*)))
