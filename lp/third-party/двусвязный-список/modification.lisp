;; -*- system :ДВУСВЯЗНЫЙ-СПИСОК; coding: utf-8; -*- 
;; Copyright (c) 2011-2012, Krzysztof Drewniak
;; All rights reserved.

;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are met:
;;     * Redistributions of source code must retain the above copyright
;;       notice, this list of conditions and the following disclaimer.
;;     * Redistributions in binary form must reproduce the above copyright
;;       notice, this list of conditions and the following disclaimer in the
;;       documentation and/or other materials provided with the distribution.
;;     * Neither the name of the <organization> nor the
;;       names of its contributors may be used to endorse or promote products
;;       derived from this software without specific prior written permission.

;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
;; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
;; DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
;; DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
;; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
;; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(named-readtables:in-readtable :buddens-readtable-a)
(in-package :Д-СПИСОК)

(defun dlist-nconc (&rest dlists)
  "Appends `dlists'. This works like `nconc' for singly-linked lists, except it is destructive and the resuld will share structure with the input dlists. This function should have running time proportional to the number of lists being appended."
  (unless dlists (return-from dlist-nconc dlists))
  (reduce #'(lambda (dlist1 dlist2)
	      (setf (dcons-next (dlist-last dlist1)) (dlist-first dlist2)
		    (dcons-prev (dlist-first dlist2)) (dlist-last dlist1)
		    (dlist-last dlist1) (dlist-last dlist2)) dlist1) dlists))

(defmacro dlist-push (obj dlist &key at-end)
  "Pushes `obj' onto `dlist'. If `at-end' is not-nil, the element is added to the end of dlist, otherwise it is added to the begining. Возвращает добавленный dcons"
  (once-only
   (obj at-end dlist)
   (with-gensyms (dlist-part-var dcons-added-by-dlist-push)
     `(let (,dcons-added-by-dlist-push)
        (cond
         (,at-end
          (let ((,dlist-part-var (dlist-last ,dlist)))
            (cond
             ((null ,dlist-part-var) ; пустой список
              (setf ,dcons-added-by-dlist-push (dcons nil ,obj nil))
              (setf (dlist-last ,dlist)
                    (setf (dlist-first ,dlist) ,dcons-added-by-dlist-push)))
             (t
              (setf ,dcons-added-by-dlist-push (dcons ,dlist-part-var ,obj nil))
              (setf (dcons-next ,dlist-part-var) ,dcons-added-by-dlist-push)
              (setf (dlist-last ,dlist) ,dcons-added-by-dlist-push)))))
         (t ; в начало добавляем
          (let ((,dlist-part-var (dlist-first ,dlist)))
            (cond
             ((null ,dlist-part-var) ; пустой список
              (setf ,dcons-added-by-dlist-push (dcons nil ,obj nil))
              (setf (dlist-last ,dlist)
                    (setf (dlist-first ,dlist) ,dcons-added-by-dlist-push)))
             (t
              (setf ,dcons-added-by-dlist-push (dcons nil ,obj ,dlist-part-var))
              (setf (dcons-prev ,dlist-part-var) ,dcons-added-by-dlist-push)
              (setf (dlist-first ,dlist) ,dcons-added-by-dlist-push))))))))))

(defmacro dlist-pop (dlist &key from-end)
  "Pops an element from dlist and returns it. If `from-end' is non-`nil', the element will be popped from the end of the dlist. Otherwise, it will be popped from the begining."
  (once-only
   (dlist from-end)
   (with-gensyms (dcons-being-popped Возможно-соседний-dcons)
     `(if ,from-end
          (let* ((,dcons-being-popped (dlist-last ,dlist))
                 (,Возможно-соседний-dcons (dcons-prev ,dcons-being-popped)))
            (setf (dlist-last ,dlist) ,Возможно-соседний-dcons)
            (cond
             (,Возможно-соседний-dcons
              (setf (dcons-next ,Возможно-соседний-dcons) nil))
             (t
              (setf (dlist-first ,dlist) nil)))
            (dcons-data ,dcons-being-popped))
          (let* ((,dcons-being-popped (dlist-first ,dlist))
                 (,Возможно-соседний-dcons (dcons-next ,dcons-being-popped)))
            (setf (dlist-first ,dlist) ,Возможно-соседний-dcons)
            (cond
             (,Возможно-соседний-dcons
              (setf (dcons-prev ,Возможно-соседний-dcons) nil))
             (t
              (setf (dlist-last ,dlist) nil)))              
            (dcons-data ,dcons-being-popped))))))

(defun dlist-delete-dcons (dlist dcons)
  "Destructively modifies dlist by deleting dcons given. If dcons is not at extreme positions in the list, does not check that dcons to be deleted belongs to the list. Returns dcons"
  (assert dlist)
  (assert dcons)
  (let ((p (dcons-prev dcons))
        (n (dcons-next dcons)))
    (cond
     ((null p)
      (assert (eq dcons (%dlist-first dlist)))
      (dlist-pop dlist))
     ((null n)
      (assert (eq dcons (%dlist-last dlist)))
      (dlist-pop dlist :from-end t))
     (t ; in the middle of the list
      (setf
       (dcons-next p) n
       (dcons-prev n) p)))
    dcons))

(defun dlist-insert-before (dlist at data)
  "Creates new dcons containing data and inserts into before 'at' dcons in dlist. Тестировано только косвенно, через |DLIST--Вставить-с-сохранением-порядка|"
  (let ((p (dcons-prev at)))
    (cond
     ((null p) ; we are at the beginning of list
      (dlist-push data dlist))
     (t
      (let ((result (dcons p data at)))
        (setf (dcons-next p) result)
        (setf (dcons-prev at) result)
        result)))))


(defun |DLIST--Вставить-с-сохранением-порядка| (|Данные| DLIST &key |Место-рядом| (|Предикат| #'<) (|Ключ| #'identity))
  "Есть DLIST, упорядоченный по возрастанию предиката. Вставить новый элемент. Можно указать место (dcons внутри DLIST), которое находится рядом с местом, куда нужно вставлять - это полезно, если эта функция используется для слияния двух сортированных списков. , когда идёт вставка многих элементов подряд и эти элементы упорядочены. В этом случае возврат предыдущей вставки нужно использовать как Место-рядом для следующей. Мне кажется, что при этом стоимость (число сравнений) при вставке М элементов в список размера Н будет О(Н+М), а если не передавать Место-рядом, то число сравнений будет где-то не более О((М+Н)^2). Хотя это всё домыслы. 
   Возвращает вставленный элемент списка"
  (perga
   (let М (or |Место-рядом| (dlist-first DLIST)))
   (flet Меньше (А Б) (funcall |Предикат| (funcall Ключ А) (funcall Ключ Б)))
   (let Направление 0) ; в какую сторону идём для поиска значения?
   (loop
     (cond
      ((null М)
       (dlist-push |Данные| DLIST :at-end (= Направление 1))
       (return
        (nthdcons 0 DLIST :from-end (= Направление 1))))
      ((Меньше |Данные| (dcons-data М))
       ;; может быть, можно ставить перед М, а может быть, нужно вставить ещё раньше.
       ;; Попробуем пойти налево и посмотрим.
       (ecase Направление
         (1 ; шли вправо, значит, предыдущий элемент меньше нашего, а этот больше - вставляем перед М
          (return (dlist-insert-before DLIST М |Данные|)))
         (0 ; никуда не шли - теперь пойдём налево
          (setf Направление -1)
          (setf М (dcons-prev М)))
         (-1 ; шли налево, значит, пойдём дальше
          (setf М (dcons-prev М)))))
      ((Меньше (dcons-data М) |Данные|)
       ;; может быть, можно вставить после М, а может быть, ещё правее.
       ;; сходим направо и посмотрим
       (ecase Направление
         (-1 ; шли налево - значит, следующий от М больше вставляемого и можно вставлять
          (return (dlist-insert-before DLIST (dcons-next М) |Данные|)))
         (0 ; никуда не шли - так пойдём
          (setf Направление 1)
          (setf М (dcons-next М)))
         (1 ; шли направо - значит пойдём дальше
          (setf М (dcons-next М)))))
      (t ; элементы равны - вставляем где попало
       (return (dlist-insert-before DLIST М |Данные|)))))))
