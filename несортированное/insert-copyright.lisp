(in-package :cl-user)

(defun вставить-мой-копирайт (text &key src-file dst-file)
  (declare (ignore dst-file))
  (perga-implementation:perga
   (let type (pathname-type src-file))
   (let comment-beginning
     (cond
      ((member type '("lisp" "asd") :test 'string-equal) ";; ")
      ((string= type "ярс") "// ")
      (t "")))
   (let copyright "Copyright (C) Денис Будяк 2015-2016")
   (when (search copyright text)
     (return-from вставить-мой-копирайт text))
   (:@ with-input-from-string (in text))
   (:@ with-output-to-string (out))
   (let l (read-line in nil nil))
   (when l
     (princ l out)
     (terpri out)
     (format out "~A~A~%" comment-beginning copyright)
     (loop
       (let ll (read-line in nil nil))
       (unless ll (return))
       (princ ll out)
       (terpri out)))))


;(defun map-dir (fn pathname &key dir-options (file-test #'identity) (dir-test #'identity) subdirs) "Iterates over all files. List of files is acquired with cl-fad:list-directory and dir-options. :file-test and dir-test are pathname filters. Subdirs can be :recurse, :skip, :msp (fn is called for subdirs too) :map-and-recurse (fn is called and recursion occurs). Output format is ((pathname . (fn pathname)) ... (:subdir pathname ((pathname . (fn pathname)) ...))). directory structure should not be modified by fn except by deletion of pathname"

(defun вставить-везде-мой-копирайт ()
  (break "Окстись!")
  (budden-tools:map-dir (lambda (file) (budden::macroexpand-file-with-function #'вставить-мой-копирайт file :dst-file file :no-backup t))
                        "c:/yar"
                        :subdirs :recurse
                        ))

 