;;;; calcu-clcon.asd

(asdf:defsystem #:calcu-clcon
  :description "Бэкенд для примитивного калькулятора в clcon"
  :author "Станислав Кондратьев"
  :license "WTFPL"
  :depends-on ("calcu-core")
  :serial t
  :components ((:file "package")
               (:file "calcu-clcon")))

