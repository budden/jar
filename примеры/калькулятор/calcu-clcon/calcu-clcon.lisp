(in-package #:calcu-clcon)

(defvar *calculator* (make-calculator))
(defvar *calculator-thread* nil)

(defun calculator-thread-function (connection)
  (swank::with-connection (connection)
     (loop 
       (let ((command (second (swank::wait-for-event `(:calculator-command . _)))))
         (if (zerop (length command))
             (return)
             (serve-text-command command))))))

(defun calculator-thread (connection)
  (or *calculator-thread*
      (setf *calculator-thread* (bt:make-thread (lambda () (calculator-thread-function connection))
                                                :name "calcu"))))

(defun update-gui (display-string)
  (clco:eval-in-tcl (format nil "::Calcu::update_display {~A}" display-string)))

(defun serve-text-command (command)
  (check-type command string)
  (when (zerop (length command))
    (setf *calculator-thread* nil))
  (loop for c across command
        for fun = (second (assoc c *text-commands*))
        when fun
        do (funcall fun *calculator*)
        finally (update-gui (calcu-core:display-string *calculator*))))
