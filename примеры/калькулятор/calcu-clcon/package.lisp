(defpackage #:calcu-clcon
  (:use #:cl #:calcu-core)
  (:export #:*calculator*
           #:calculator-thread
           #:serve-text-command))

