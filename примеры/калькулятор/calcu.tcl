package require Tk

namespace eval Calcu {

    proc update_display {str} {
        set ::display $str
    }

    proc hit {keys} {
        ::tkcon::EvalInSwankAsync "(swank::send-event (calcu-clcon:calculator-thread swank::*emacs-connection*) '(:calculator-command \"$keys\"))" {} :post-message-thread
    }


    proc calcu {} {

        if {[winfo exists .calcu]} {
            focus .calcu
            raise .calcu
            return 0
        }


        toplevel .calcu

                    
        grid [ttk::frame .calcu.c -padding "3 3 3 3"] -column 0 -row 0 -sticky nwes
        grid [ttk::entry .calcu.c.display -textvar display -state readonly -just right -font {-family Courier -size 20} -width 14] -columnspan 5 

        # Ряды кнопок: имя кнопки - текст - текстовая команда лиспу.
        # Некоторые клавиши потом подрихтовываются.

        foreach row {
            {{off OFF ""} {ce CE c} {ac AC a}}
            {{m M y} {mr MR p} {m+ M+ u} {m- M- i} {mc MC o}}
            {{7 7 7} {8 8 8} {9 9 9} {% %  %} {sqrt √ s}}
            {{4 4 4} {5 5 5} {6 6 6} {* × *} {/ ÷ /}}
            {{1 1 1} {2 2 2} {3 3 3} {+ + +} {- - -}}
            {{0 0 0} {dot · .} {pm ± !} {= = =}}
        } {
            foreach key_code $row {
                lappend keys [ttk::button .calcu.c.[lindex $key_code 0] -text [lindex $key_code 1] -width 2 -command "Calcu::hit [lindex $key_code 2]"]
           }
           eval grid $keys -sticky snwe -padx 2 -pady 2
           set keys [list]
        }

        # adjust a few buttons
        grid .calcu.c.= -columnspan 2
        grid .calcu.c.ce -column 3
        grid .calcu.c.ac -column 4
        .calcu.c.off configure -command {destroy .calcu}

        # keybindings
         
        bind .calcu <Escape> {.calcu.c.off invoke}
        bind .calcu <Return> {.calcu.c.= invoke}

        # Клавиша - имя кнопки

        # Отключенные биндинги - у меня на клавиатуре не работают - ДБ
        # {KP_Insert 0} {KP_End 1} {KP_Down 2} {KP_Next 3} {KP_Left 4}
        # {KP_Begin 5} {KP_Right 6} {KP_Home 7} {KP_Up 8} {KP_Prior 9}
        foreach keybinding {
            {0 0} {1 1} {2 2} {3 3} {4 4}
            {5 5} {6 6} {7 7} {8 8} {9 9}
            {period dot} {KP_Decimal dot}
            {KP_0 0} {KP_1 1} {KP_2 2} {KP_3 3} {KP_4 4}
            {KP_5 5} {KP_6 6} {KP_7 7} {KP_8 8} {KP_9 9}
            {plus +} {minus -} {slash /} {asterisk *} {equal =} {exclam pm}
            {KP_Add +} {KP_Subtract -} {KP_Multiply *} {KP_Divide /}
            {KP_Enter =} {KP_Separator dot} {KP_Delete dot}
            {percent %}
            {s sqrt}
            {c ce} {a ac}
            {m m} {R mr} {P m+} {M m-} {C mc}

        } {
                eval bind .calcu <KeyPress-[lindex $keybinding 0]> "{event generate .calcu.c.[lindex $keybinding 1] <ButtonPress-1>}"
                eval bind .calcu <KeyRelease-[lindex $keybinding 0]> "{event generate .calcu.c.[lindex $keybinding 1] <ButtonRelease-1>}"
        }

        bind .calcu <Destroy> {
            if {"%W" == ".calcu"} {
                ::Calcu::hit ""
            }
        }
    }

    proc main {} {
        calcu
        # shake before use
        hit ao
    }
}

::tkcon::EvalInSwankAsync "(asdf:load-system :calcu-clcon)" {::Calcu::main} t
