SCRIPT_PATH=$(cd $(dirname $0) && pwd);
# Нормализируем путь, чтобы в нём не было .. 
YAR_ROOT=`readlink -f $SCRIPT_PATH/..`

# Пытаемся избежать указание путей - ведь они уже должны быть в образе
export CCL_DEFAULT_DIRECTORY=$YAR_ROOT/ccl/1.12dev
$SCRIPT_PATH/yar-image.linux.ccl.exe --load $YAR_ROOT/zapusk/start-yar-ide-from-image.lisp --eval "(named-readtables:in-readtable :buddens-readtable-a)" 


