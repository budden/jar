@rem All arguments are passed to clcon.tcl script. See clcon manual for possible params
@rem in the release, this file should be called clcon-client-debug.cmd
@rem this is clcon client with enabled logging
@set yar_root=%~dp0..\..
@set clcon_source=%yar_root%\lp\clcon

@start %yar_root%\tcl-8.6.6\bin\wish86t.exe -encoding utf-8 %CLCON_SOURCE%\clcon.tcl -putd-output-file %yar_root%\log\putd.log -- %*
