SCRIPT_PATH=$(cd $(dirname $0) && pwd);
# Нормализируем путь, чтобы в нём не было .. 
YAR_ROOT=`readlink -f $SCRIPT_PATH/../..`

export CCL_DEFAULT_DIRECTORY=$YAR_ROOT/ccl/1.12dev
# Если возникнут несколько версий SBCL с одинаковой сигнатурой, нужно будет что-то добавить в этот путь
export XDG_CACHE_HOME=$YAR_ROOT/bin/fasl-cache
$CCL_DEFAULT_DIRECTORY/lx86cl64 -K utf-8 -l $CCL_DEFAULT_DIRECTORY/lx86cl64-init.lisp --eval "(defparameter *clcon-swank-port* 4006)" --load $YAR_ROOT/lp/clcon/zapusk/zagruzitq-server--clcon.lisp --load $YAR_ROOT/lp/clcon/zapusk/zapustitq--clcon.lisp
