@rem this is a command to start clcon (server and client)
@setlocal
@set yar_root=%~dp0..\..\
@set CCL_DEFAULT_DIRECTORY=%YAR_ROOT%ccl\tek\
@set XDG_CACHE_HOME=%YAR_ROOT%bin\fasl-cache\
@set clcon_zapusk=%YAR_ROOT%lp\clcon\zapusk\
@REM @cd /d %CLCON_ZAPUSK%
@set CLCON_START_CMD=%CCL_DEFAULT_DIRECTORY%wx86cl64.exe -K utf-8 -l %CCL_DEFAULT_DIRECTORY%wx86cl64-init.lisp --eval "(defparameter *clcon-swank-port* 4006)" --load %CLCON_ZAPUSK%zagruzitq-server--clcon.lisp --load %CLCON_ZAPUSK%zapustitq--clcon.lisp
@%CLCON_START_CMD%
@REM start %YAR_ROOT%\bin\util\ConEmu\ConEmu.exe /cmd %CLCON_START_CMD%
