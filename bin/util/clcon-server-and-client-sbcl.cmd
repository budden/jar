@rem this is a command to start clcon (server and client)
@set yar_root=%~dp0..\..\
@set clcon_zapusk=%YAR_ROOT%lp\clcon\zapusk\
@cd /d %CLCON_ZAPUSK%
@set SBCL_HOME=%YAR_ROOT%sbcl\1.4.2-win\
# Esli vozniknut neskolqko versiyi SBCL s odinakovoyi signaturoyi, nuzhno budet chto-to dobavitq v ehtot putq
@set XDG_CACHE_HOME=%YAR_ROOT%bin\fasl-cache\
@set CLCON_START_CMD=%SBCL_HOME%sbcl.exe --core %SBCL_HOME%sbcl.core --sysinit %YAR_ROOT%sbcl\.sbclrc-1.4.2 --no-userinit --eval "(defparameter *clcon-swank-port* 4004)" --load %CLCON_ZAPUSK%zagruzitq-server--clcon.lisp --load %CLCON_ZAPUSK%zapustitq--clcon.lisp
@rem --eval "(asdf:load-system :load-test-2)" 
@rem start %YAR_ROOT%\bin\util\ConEmu\ConEmu.exe /cmd %CLCON_START_CMD%
%CLCON_START_CMD%
