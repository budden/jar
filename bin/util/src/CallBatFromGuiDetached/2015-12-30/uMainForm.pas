unit uMainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs;

type
  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation uses math;

{$R *.dfm}

procedure MsgBox2(msg:string);
begin
Application.MessageBox(PChar(msg),PChar(Application.Title),MB_OK);
end;

// http://www.sql.ru/forum/242379/zapusk-bat-fayla, ����� - RIM1977 
function  FileExec( const CmdLine: String; bHide, bWait: Boolean; out SubprocessExitCode: cardinal): boolean;
var
  StartupInfo : TStartupInfo;
  ProcessInfo : TProcessInformation;
  CreateProcessSuccess : boolean;
  WaitForSingleObjectSuccess : boolean;
  GetExitCodeProcessSuccess : boolean;
begin
//  MsgBox2(CmdLine);
result:=false;
SubprocessExitCode:=0;

FillChar(StartupInfo, SizeOf(TStartupInfo), 0);
with StartupInfo do
  begin
  cb := SizeOf(TStartupInfo);
  dwFlags := STARTF_USESHOWWINDOW or STARTF_FORCEONFEEDBACK;
  if bHide then
     wShowWindow := SW_HIDE
  else
     wShowWindow := SW_SHOWNORMAL;
  end;
  
CreateProcessSuccess:=CreateProcess(nil, PChar(CmdLine), nil, nil, False,
             NORMAL_PRIORITY_CLASS or CREATE_NEW_CONSOLE, nil, nil, StartupInfo, ProcessInfo);

if CreateProcessSuccess then
   CloseHandle(ProcessInfo.hThread)
else
   begin
   SubprocessExitCode:=GetLastError;
   exit;
   end;

try
  if not bWait then
    begin
    result:=true;
    exit;
    end;

  WaitForInputIdle(ProcessInfo.hProcess, INFINITE);
  WaitForSingleObjectSuccess:= WaitForSingleObject(ProcessInfo.hProcess, INFINITE) = WAIT_OBJECT_0;
  if not WaitForSingleObjectSuccess then
    exit;
  GetExitCodeProcessSuccess:=GetExitCodeProcess(ProcessInfo.hProcess,SubprocessExitCode);
  if not GetExitCodeProcessSuccess then
    exit;

  result:=true;
finally
  CloseHandle(ProcessInfo.hProcess);
  end;
end;


procedure TForm1.FormCreate(Sender: TObject);
var PosOfParamstr0:integer;
var subprocessExitCode:cardinal;
begin // GetCommandLine
subprocessExitCode:=0;
if paramstr(1)='' then
  begin
  MsgBox2('����� ������� ���, ��� ����� ��������� ������ GUI ����������. Usage: '
         +paramstr(0)+' ������ ������ ������� ');
  exit;
  end;
// MsgBox2(CmdLine);
PosOfParamstr0:=pos(paramstr(0),CmdLine);
if PosOfParamstr0=0 then
  begin
  MsgBox2('�� ���� �������� ��� ����� ���� '+paramstr(0)+' � ����������� ��������� ������ '+CmdLine);
  exit;
  end;
FileExec(trim(copy(CmdLine,PosOfParamstr0+length(paramstr(0))+1,10000)),false,true,subprocessExitCode);
exitCode:=min(subprocessExitCode,High(Integer));
end;

procedure TForm1.FormShow(Sender: TObject);
begin
Close;
end;

end.
