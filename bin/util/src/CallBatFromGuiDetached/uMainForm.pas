unit uMainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs;

type
  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation uses math;

{$R *.dfm}

procedure MsgBox2(msg:string);
begin
Application.MessageBox(PChar(msg),PChar(Application.Title),MB_OK);
end;


procedure ReadFromPipeToStringList(hPipeOutputRead:THANDLE;Output:TStringList);
//������ �����
var Stream : TMemoryStream;
    bTest:boolean;
    szBuffer : array[0..256] of Char;
    dwNumberOfBytesRead : DWORD;
begin
Stream := TMemoryStream.Create;
try
  while true do
    begin
    bTest := ReadFile(hPipeOutputRead, szBuffer, 256, dwNumberOfBytesRead, nil);
    if not bTest then
      begin
      break;
      end;
    Stream.Write(szBuffer, dwNumberOfBytesRead);
    end;
  Stream.Position := 0;
  Output.LoadFromStream(Stream);
finally
  Stream.Free;
  end;
end;


{ **** UBPFD *********** by http://kladovka.net.ru/delphibase/ ****
>> CreateProcess ������� ���������� ���������� �����

������� ��������� ������� � �������� ��� �����.

�����������: Windows
�����:       [NIKEL], nikel@pisem.net, Norilsk
Copyright:   some help
����:        15 �������� 2002 �.
***************************************************************** }

function ExecConsoleAppWithRedirectedOutput(CommandLine: AnsiString; Output: TStringList; Errors:
                                                                   TStringList; out subprocessExitCode : cardinal):boolean;
var
 sa : TSECURITYATTRIBUTES;
 si : TSTARTUPINFO;
 pi : TPROCESSINFORMATION;
 hPipeOutputRead : THANDLE;
 hPipeOutputWrite : THANDLE;
 hPipeErrorsRead : THANDLE;
 hPipeErrorsWrite : THANDLE;
 env : array[0..100] of Char;
 CreateProcessSuccess : boolean;
 WaitForSingleObjectSuccess : boolean;
 GetExitCodeProcessSuccess : boolean;
 directory : string;
begin
result:=false;
subprocessExitCode:=0;
directory:=GetCurrentDir;
sa.nLength := sizeof(sa);
sa.bInheritHandle := true;
sa.lpSecurityDescriptor := nil;
CreatePipe(hPipeOutputRead, hPipeOutputWrite, @sa, 0);
CreatePipe(hPipeErrorsRead, hPipeErrorsWrite, @sa, 0);

try // �������� �������� ������������ ����. ������ �������� �� �������� �� �������� �� CreateProcess.

  ZeroMemory(@env, SizeOf(env));
  ZeroMemory(@si, SizeOf(si));
  ZeroMemory(@pi, SizeOf(pi));
  si.cb := SizeOf(si);
  si.dwFlags := STARTF_USESHOWWINDOW or STARTF_USESTDHANDLES;
  si.wShowWindow := SW_HIDE;
  si.hStdInput := 0;
  si.hStdOutput := hPipeOutputWrite;
  si.hStdError := hPipeErrorsWrite;

  { ���� �� ������ ��������� ������� ��� ����������, ��nil`�� ������ ��������
   � ����������� ������
  }
  // CREATE_NEW_CONSOLE �������� �� ����� "detach" ����� ��. �� ����� ���� �������, � ��� ��� �� ���������
  // ����������� - ��� �� ��� �� ����� ��� �����.
  CreateProcessSuccess := CreateProcess(nil, pchar(CommandLine), nil, nil, true,
                CREATE_NEW_CONSOLE or NORMAL_PRIORITY_CLASS, @env, pchar(directory), si, pi);

  CloseHandle(hPipeOutputWrite);
  CloseHandle(hPipeErrorsWrite);

  if CreateProcessSuccess then
     CloseHandle(pi.hThread)
  else
     begin
     SubprocessExitCode:=GetLastError;
     exit;
     end;

  try // �������� handle ��������

    ReadFromPipeToStringList(hPipeOutputRead,Output);
    ReadFromPipeToStringList(hPipeErrorsRead,Errors);

    WaitForInputIdle(pi.hProcess, INFINITE);
    WaitForSingleObjectSuccess:= WaitForSingleObject(pi.hProcess, INFINITE) = WAIT_OBJECT_0;
    if not WaitForSingleObjectSuccess then
      exit;
    GetExitCodeProcessSuccess:=GetExitCodeProcess(pi.hProcess,SubprocessExitCode);
    if not GetExitCodeProcessSuccess then
      exit;

    result:=true;
  finally
    CloseHandle(pi.hProcess);
    end;

finally
  CloseHandle(hPipeOutputRead);
  CloseHandle(hPipeErrorsRead);
  end;
end;

// http://www.sql.ru/forum/242379/zapusk-bat-fayla, ����� - RIM1977
function  FileExec( const CmdLine: String; bHide, bWait: Boolean; out SubprocessExitCode: cardinal): boolean;
var
  StartupInfo : TStartupInfo;
  ProcessInfo : TProcessInformation;
  CreateProcessSuccess : boolean;
  WaitForSingleObjectSuccess : boolean;
  GetExitCodeProcessSuccess : boolean;
  directory:string;
begin
//  MsgBox2(CmdLine);
result:=false;
SubprocessExitCode:=0;

FillChar(StartupInfo, SizeOf(TStartupInfo), 0);
with StartupInfo do
  begin
  cb := SizeOf(TStartupInfo);
  dwFlags := STARTF_USESHOWWINDOW or STARTF_FORCEONFEEDBACK;
  if bHide then
     wShowWindow := SW_HIDE
  else
     wShowWindow := SW_SHOWNORMAL;
  end;

directory:=GetCurrentDir;
CreateProcessSuccess:=CreateProcess(nil, PChar(CmdLine), nil, nil, False,
             NORMAL_PRIORITY_CLASS or CREATE_NEW_CONSOLE, nil, PChar(directory), StartupInfo, ProcessInfo);

if CreateProcessSuccess then
   CloseHandle(ProcessInfo.hThread)
else
   begin
   SubprocessExitCode:=GetLastError;
   exit;
   end;

try
  if not bWait then
    begin
    result:=true;
    exit;
    end;

  WaitForInputIdle(ProcessInfo.hProcess, INFINITE);
  WaitForSingleObjectSuccess:= WaitForSingleObject(ProcessInfo.hProcess, INFINITE) = WAIT_OBJECT_0;
  if not WaitForSingleObjectSuccess then
    exit;
  GetExitCodeProcessSuccess:=GetExitCodeProcess(ProcessInfo.hProcess,SubprocessExitCode);
  if not GetExitCodeProcessSuccess then
    exit;

  result:=true;
finally
  CloseHandle(ProcessInfo.hProcess);
  end;
end;

function ExtractFirstParameter(var CommandLine:string;firstParameter:string):boolean;
var posOfParameter:integer;
begin
result:=false;
PosOfParameter:=pos(firstParameter,CommandLine);
if PosOfParameter=0 then
  exit;
CommandLine:=trim(copy(CommandLine,posOfParameter+length(firstParameter)+1,10000));
result:=true;
end;

function ParseCommandLine(out CommandLineToPass:string;out FileNameToRedirect:string):boolean;
begin
result:=false;
if paramstr(1)='' then
  begin
  MsgBox2('����� ������� ���, ��� ����� ��������� ������ GUI ����������. Usage: '
         +paramstr(0)+' [-s filename_prefix] ������ ������ �������. ���� ����� ���� -s, ����� ������� ��� ����� filename_prefix.* � �������� ������� � ������� ������');
  exit;
  end;
// MsgBox2(CmdLine);
CommandLineToPass:=CmdLine;
if not ExtractFirstParameter(CommandLineToPass,paramstr(0)) then
  begin
  MsgBox2('�� ���� �������� ��� ����� ���� '+paramstr(0)+' � ����������� ��������� ������ - ������� ������ ���� � ���������. '+CmdLine);
  exit;
  end;
if paramstr(1)='-s' then
  begin
  FileNameToRedirect:=paramstr(2);
  if not ExtractFirstParameter(CommandLineToPass,paramstr(1)) then
    begin
    MsgBox2('������ ��� ���������� ��������� -s. ������ � ��������� '+paramstr(0));
    exit;
    end;
  if not ExtractFirstParameter(CommandLineToPass,paramstr(2)) then
    begin
    MsgBox2('������ ��� ���������� ��������� -s. ������ � ��������� '+paramstr(0));
    exit;
    end;
  end;

result:=true;
end;


function ExecWithRedirect(commandLineToPass,FileNameToRedirect:string;out subprocessExitCode:cardinal):boolean;
var stdOutput:TStringList;
var stdError:TStringList;
begin
stdOutput:=TStringList.Create;
stdError:=TStringList.Create;
try
  result:=ExecConsoleAppWithRedirectedOutput(commandLineToPass,stdOutput,stdError,subprocessExitCode);
  stdOutput.SaveToFile(FileNameToRedirect+'.stdOutput.txt');
  stdError.SaveToFile(FileNameToRedirect+'.stdError.txt');
finally
  FreeAndNil(stdOutput);
  FreeAndNil(stdError);
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
var CommandLineToPass:string;
var subprocessExitCode:cardinal;
var FileNameToRedirect:string;
var runProcessSuccess:boolean;
begin // GetCommandLine
exitCode:=1;
if not ParseCommandLine(CommandLineToPass,FileNameToRedirect) then
  exit;
subprocessExitCode:=0;
if FileNameToRedirect<>'' then
  runProcessSuccess:=ExecWithRedirect(commandLineToPass,FileNameToRedirect,subprocessExitCode)
else
  runProcessSuccess:=FileExec(commandLineToPass,false,true,subprocessExitCode);
if runProcessSuccess then
  exitCode:=min(subprocessExitCode,High(Integer))
end;

procedure TForm1.FormShow(Sender: TObject);
begin
Close;
end;

end.
