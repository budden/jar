SCRIPT_PATH=$(cd $(dirname $0) && pwd);
# Нормализируем путь, чтобы в нём не было .. 
YAR_ROOT=`readlink -f $SCRIPT_PATH/..`

# Я не понимаю, насколько нужна следующая строка. Попробуем обойтись без неё, 
# ведь мы указываем имя файла инициализации напрямую. Исходники SBCL, во всяком случае, открываются
# export SBCL_HOME=$YAR_ROOT/sbcl/1.3.18-l
$SCRIPT_PATH/yar-image.linux.sbcl.exe --load $YAR_ROOT/zapusk/start-yar-ide-from-image.lisp --eval "(named-readtables:in-readtable :buddens-readtable-a)" 


