;; -*- coding: utf-8 ; -*-
;; (C) Denis Budyak 2015-2018
;; MIT License

(in-package :cl-user)
;; You should be readtable-agnostic here

(eval-when (:compile-toplevel)
  (error "Your must not compile ~S. Load it as a source instead" *compile-file-truename*))

#+SBCL (setf sb-impl::*default-external-format* :utf-8)

;;;;;;;;;;;;;;;;;; There used to be enabling stepper everywhere ;;;;;;;;;;;;;;;;;;;;;;;;;;;

;(sb-ext:RESTRICT-COMPILER-POLICY 'debug 3)
;(sb-ext:RESTRICT-COMPILER-POLICY 'safety 2 3)

;;;;;;;;;;;;;;;;;; Starting server ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; note this is already second swank server
; first was created in .sbclrc to be able to connect to from EMACS
(swank:create-server :port *clcon-swank-port* :dont-close t)

;;;;;;;;;;;;;;;;;; Starting editor backend ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(clco:start-oduvanchik)

;;;;;;;;;;;;;;;;;; Starting editor frontend ;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(zapustitq-klienta--clcon)




