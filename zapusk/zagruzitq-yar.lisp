;; -*- coding: utf-8; -*-
(in-package :cl-user)
;; Copyright (C) Денис Будяк 2015

; set format for our application
(setf *READ-DEFAULT-FLOAT-FORMAT* 'DOUBLE-FLOAT)

(proclaim '(optimize (debug 3) (safety 3) (speed 0) (compilation-speed 0) (space 0)))

(eval-when (:compile-toplevel)
  (error "Do not try to compile this file. Load it"))

;; Возможно, дублирует то, что уже сделано в .sbclrc
(defparameter *yar-root* #.(pred-dir (cl-fad:pathname-directory-pathname  (or *compile-file-truename* *load-truename*))))

;; Возможно, дублирует то, что уже сделано в .sbclrc
(pushnew (pathname *yar-root*) ql:*local-project-directories* :test 'equal)

;; настройка персонально для Budden и приближённых лиц :) 
(when (probe-file #p"c:/yar.my/док/")
  (pushnew :budden *features*)
  (pushnew #p"c:/yar.my/док/" ql:*local-project-directories* :test 'equal))

; принудительно обновляем индекс систем - это пригодится для обновления 
; у клиентов, где иначе будут проблемы с поиском новых систем
(ql::make-system-index (make-pathname :name "non-existent-file" :defaults *yar-root*))

;; without this, compiler is called toooo frequently
;; and execution of our code slows down ~100 times 
#+SBCL (setf sb-ext:*evaluator-mode* :interpret)

(setf *print-case* :downcase)

;; debugging stepper
;;(declaim (optimize (debug 3) (compilation-speed 2)))
;;(proclaim '(optimize (debug 3) (compilation-speed 2)))

(defun load-budden-system ()
  (asdf:load-system :budden))

(load-budden-system)

(setf *print-pretty* t)
(setf *print-circle* t)

#+make-clcon-image-only
(ql:quickload :БУКВЫ-И-МЕСТА-В-ФАЙЛЕ)

#-make-clcon-image-only
(ql:quickload :xmls)
;(ql:quickload :polir) ; здесь подгружается ещё несколько систем!

;(ql:quickload :ПАРСЕР-ЯРА-ТЕСТЫ)

#-make-clcon-image-only
(ql:quickload :ЯРА-ЛИЦО) ;отдельно загрузим транслятор

#-make-clcon-image-only
(ql:quickload :ЯРА-ИСР) ; отдельно среду разработки

#-make-clcon-image-only
(ql:quickload :asdf-yar-source-file) ; и вот эту сомнительной ценности систему


