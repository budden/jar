#-(or CCL SBCL) (error "This Lisp is not supported")

(defun yar-load-asdf-canonicalize-directory-1 (string)
  (assert (member (elt string (- (length string) 1)) '(#\/ #\\)) () 
    "Make sure to end your dir ~S with slash" string)
   ;; From cl-fad:canonical-pathname
  (let ((pathname (pathname string)))
    (loop
       with full-dir = (or (pathname-directory pathname)
                           (list :relative))
       with canon-dir = (if (member (first full-dir) '(:relative :absolute))
                            (list (pop full-dir))
                            (list :relative))
       while full-dir
       do (cond
            ((string= "." (first full-dir))
             (pop full-dir))
            ((or (eql :back (second full-dir))
                 (eql :up (second full-dir)))
             (pop full-dir)
             (pop full-dir))
            (t (push (pop full-dir) canon-dir)))
       finally (return (make-pathname :defaults pathname :directory (nreverse canon-dir))))))
       
(defun yar-load-asdf-canonicalize-directory (string)
  (let ((old string))
   (loop 
     (let ((new (namestring (yar-load-asdf-canonicalize-directory-1 old))))
       (when (string= old new) 
         (return-from yar-load-asdf-canonicalize-directory new))
       (finish-output)
       (setq old new)))))

(defun yar-load-asdf-getenv (string)
  (#+SBCL sb-posix:getenv #+CCL getenv string))
  
(defun yar-load-asdf-setenv (string new-value-string)
  (#+SBCL sb-posix:setenv #+CCL setenv string new-value-string #+SBCL 1))

(defun yar-load-asdf-check-and-canonicalize-one-environment-variable (name)
  (assert (yar-load-asdf-getenv name) () "Env var ~S is missing" name)
  (yar-load-asdf-setenv name 
    (namestring (yar-load-asdf-canonicalize-directory (yar-load-asdf-getenv name)))))
  
(defun yar-load-asdf-canonicalize-and-write-environment-variables ()
  (let ((home-dir #+SBCL "SBCL_HOME" #+CCL "CCL_DEFAULT_DIRECTORY"))
    (yar-load-asdf-check-and-canonicalize-one-environment-variable home-dir)
    (yar-load-asdf-check-and-canonicalize-one-environment-variable "XDG_CACHE_HOME")))

;; in Linux, we have readlink for that. 
#+(or WIN32 WINDOWS)
(yar-load-asdf-canonicalize-and-write-environment-variables)

;; load-asdf and check-output-translations-ok are similar between initialization files of SBCL and CCL
(defun yar-load-asdf ()
  (assert (null (find-package :uiop)))
  (assert (null (find-package :asdf)))
  (require "uiop" (putq-otnositelqno-kornya-yara "lp/asdf-3.3.1/uiop.lisp"))
  (require "asdf" (putq-otnositelqno-kornya-yara "lp/asdf-3.3.1/asdf.lisp")))

(yar-load-asdf)

(defun check-output-translations-ok ()
  "Call it now and once again at the end of loading to ensure that fasls are placed to a right place"
  #-(or windows win32 linux) (error "ne umeyu rabotatq s putyami na ehtojj platforme")
  (let* ((name-to-probe #+(or windows win32) "c/aaa.bbb" #+linux "home/den73/aaa.bbb")
         (super-dir (pathname (#+SBCL sb-posix:getenv #+CCL getenv "XDG_CACHE_HOME")))
         (translated-name
           ; asdf::apply-output-translations 
           (funcall (find-symbol "APPLY-OUTPUT-TRANSLATIONS" :asdf)
            #+(or windows win32) "c:/aaa.bbb" #+linux "/home/den73/aaa.bbb"))
         (super-dir-string (namestring super-dir))
         (translated-name-string (namestring translated-name))
         (name-to-probe-string (namestring name-to-probe)))
    (assert
     (eql 0 ; start with subseq 
          (search super-dir-string translated-name-string))
           () "Neverno nastroeno XDG_CACHE_HOME - ~S dolzhno nachinatqsya s ~S" translated-name-string super-dir-string)
    (assert
     (search
      name-to-probe-string
      translated-name-string) () "CHto-to ne tak: ~S dolzhno vkhoditq v ~S" name-to-probe-string translated-name-string)))

(check-output-translations-ok)
