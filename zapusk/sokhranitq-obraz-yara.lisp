;; -*- Encoding: utf-8; Coding: utf-8 ; -*-
;; Скрипт сборки образа - загружает и сохраняет Яр. Запускать после zagruzitq-server--clcon.lisp 
;; (C) Denis Budyak 2015
;; MIT License

(in-package :cl-user)

(eval-when (:compile-toplevel)
  (error "Не компилируй ~S, загружай его с помощью LOAD" *compile-file-truename*))

(assert (member :saving-yar-image *features*))

;;;;;;;;;;;;;;;;;; Loading yar ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(load (merge-pathnames "zapusk/zagruzitq-yar.lisp" *yar-root*))

;;;;;;;;;;;;;;;;;; Снимаем ограничения на политики ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#+SBCL (sb-ext:RESTRICT-COMPILER-POLICY 'debug)
#+SBCL (sb-ext:RESTRICT-COMPILER-POLICY 'safety)
#+SBCL (assert (null (sb-ext:RESTRICT-COMPILER-POLICY)))


;;;;;;;;;;;;;;;;;; Сохранить образ ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(let* ((platform-suffix #+os-windows "win.x86_64" #+linux "linux" #-(or os-windows  linux) (error "Не знаю как назвать исполняемый файл Яра для этой платформы"))
       (implementation-suffix #+CCL "ccl" #+SBCL "sbcl" #-(or sbcl ccl) (error "Ne znayu kak nazvatq ispolyaemyyyi fayil yara dlya ehtoyi realizacii lispa"))
       (image-filename (merge-pathnames (format nil "bin/yar-image.~A.~A.exe" platform-suffix implementation-suffix) *yar-root*)))
  (loop
    (restart-case
        #+CCL (save-application image-filename :prepend-kernel t :purify nil)
        #+SBCL (save-lisp-and-die image-filename :executable t)
        #-(OR CCL SBCL) (let "Для этой реализации CL не умею сохранять образ Яра")
      (#:Попробовать-сохранить-образ-ещё-раз ())
      )))

