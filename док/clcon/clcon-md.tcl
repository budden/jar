# Процедуры для помощи в разработке md-документации


# Ссылка на помеченный якорь в данном файле 
proc ::СсылкаНаВыделенныйПунктОглавленияИзДругогоМеста {} {
    set clcon_text [::edt::c_btext]
    set FileNameUnix [[$clcon_text cget -opened_file] cget -filename]
    set text [::edt::GetTextSelectedInCurrentEditor]
    set tt [string trim $text]
    set lt [string tolower $tt]
    set rt [string map {" " "-"} [string tolower $lt]]
    set anchor [string cat $FileNameUnix {#header-id-} $rt]
    return $anchor
}
    
