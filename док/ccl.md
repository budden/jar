Портирование clcon/yar на CCL 
=====================

Чего не хватает?
----------------
- kons 
- reader 
- printer
- code walker (defun-to-file)
- getpid
- defpackage-l2 (был недавно стёрт)
- пошаговый отладчик
- getpid
- run-program
- native-namestring
и т.п. 

Как облегчить переход
---------------------
Начать с clcon. Оно почти не использует buddens-readtable-a - можно попробовать вообще вычистить её из clcon и не загружать систему :buddens-readtable в clcon. Тогда не будет нужен и reader/printer.
code walker лишь улучшает качество кода в defun-to-file, но не принципиален для работоспособности.
пошаговый отладчик можно сделать на базе trace.

