;;; -*- coding: utf-8; Mode: Lisp -*-

;; Для пересоздания док-та выполни (ОЯ-ИНФРАСТРУКТУРА:Собрать-проект-Html-файла :РУК-ВО--РАЗРАБОТЧИКА)

(in-package #:asdf)
(named-readtables::in-readtable :buddens-readtable-a)

(defsystem :РУК-ВО--РАЗРАБОТЧИКА
 :serial t
 :depends-on (:СПРАВОЧНИКИ-HTML)
 :components
  ((:file "пакет-рук-во--разработчика")
   (:file "очистить-БД-и-заполнить-теги")
   (:file "рук-во--разработчика-бд")
   ))

