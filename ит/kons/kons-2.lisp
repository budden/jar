; -*- system :kons; -*-

;;; (C) Denis Budyak 2016
;;; @include MIT License 
;;; This code contains portions of SBCL, see also SBCL license

(named-readtables:in-readtable :buddens-readtable-a)

(cl:in-package :kons-impl)

(defun adjoin (item list &key key (test #'eql testp) (test-not nil notp))
  "Add ITEM to LIST unless it is already a member"
  (when (and testp notp)
    (error ":TEST and :TEST-NOT were both supplied."))
  (let ((key (and key (cl-impl:coerce-callable-to-fun key)))
        (test (and testp (cl-impl:coerce-callable-to-fun test)))
        (test-not (and notp (cl-impl:coerce-callable-to-fun test-not))))
    (cond (test
           (if key
               (%adjoin-key-test item list key test)
               (%adjoin-test item list test)))
          (test-not
           (if key
               (%adjoin-key-test-not item list key test-not)
               (%adjoin-test-not item list test-not)))
          (t
           (if key
               (%adjoin-key item list key)
               (%adjoin item list))))))

(defun budden-tools-collect-duplicates (list &rest key-args &key key test test-not)
  "См. budden-tools:collect-duplicates . Делаем тупо - приводим к списку, а потом - наоборот"
  (declare (ignore key test test-not))
  (let* ((cl-list (k-list-to-cl-list list))
         (cl-list-duplicates (apply 'budden-tools:collect-duplicates cl-list key-args)))
    (cl-list-to-k-list cl-list-duplicates)))
  
(defun Разобрать-на-непрерывные-группы-с-одинаковым-значением-ключа (Список &key (Ключ #'identity) (Равенство 'eql))
  "Аналог collect-duplicates, но идёт по порядку следования групп"
  (perga
   (cond
    ((null Список) nil)
    (t
     (let Группы nil)
     (let Группы-т-р nil)
     (let Тек-группа nil)
     (let Тек-группа-т-р nil)
     (let Первая-итерация t)
     (let Тек-ключ (funcall Ключ (first Список))) ; для типизации!
     (dolist (Э Список)
       (cond
        (Первая-итерация
         (budden-tools-collect-into-tail-of-list Э Тек-группа Тек-группа-т-р)
         (budden-tools-collect-into-tail-of-list Тек-группа Группы Группы-т-р)
         (setq Первая-итерация nil))
        (t
         (let Новый-ключ (funcall Ключ Э))
         (cond
          ((funcall Равенство Тек-ключ Новый-ключ)
           (budden-tools-collect-into-tail-of-list Э Тек-группа Тек-группа-т-р))
          (t
           (setq Тек-группа nil)
           (setq Тек-группа-т-р nil)
           (setq Тек-ключ Новый-ключ)
           (budden-tools-collect-into-tail-of-list Э Тек-группа Тек-группа-т-р)
           (budden-tools-collect-into-tail-of-list Тек-группа Группы Группы-т-р))))))
     Группы))))
