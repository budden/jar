; -*- system :kons; coding: utf-8; -*-
;;; (C) Denis Budyak 2016
;;; @include MIT License 
;;; This code contains portions of SBCL, see also SBCL license

(named-readtables:in-readtable :buddens-readtable-a)

(in-package :kons-impl)

(cl:defstruct cons car cdr)

(defun car-is-of-type (cons type)
  (typep (cons-car cons) type))

(defun cdr-is-of-type (cons type)
  (typep (cons-cdr cons) type))

#|
Неясно, как определить тип cons-of car-type cdr-type.
Единственный выход - это satisfies, однако нужно генерировать типы для каждого
сочетания car-type и cdr-type. Это возможно, для этого нужно напечатать их 
в определённом пакете, превратить это в имя и далее сделать defun-to-file. 

Вроде бы должна быть возможность генерировать эти типы на лету прямо из обращения к типу.
Но не будем сейчас на это отвлекаться - FIXME
|#

(defmacro deftype-cons-of (name &optional (car-type t car-type-supplied-p) (cdr-type t cdr-type-supplied-p))
  "Генерирует тип name, который выполняется для k:cons, у к-рых (typep (car x) car-type) и (typep (cdr x) cdr-type) . См. также https://www.linux.org.ru/forum/development/13435681"
  (let ((ap car-type-supplied-p)
        (dp cdr-type-supplied-p)
        at-pred-name
        dt-pred-name
        at-pred-def
        dt-pred-def)
    (when ap
      (setf at-pred-name
            (let ((cl:*package* (cl:symbol-package name)))
              (budden-tools:symbol+ 'Предикат-для--car--для--cons-of--по-имени-- name)))
      (setf at-pred-def
            `(defun ,at-pred-name (x) (typep (car x) ',car-type))))
    (when dp
      (setf dt-pred-name
            (let ((cl:*package* (cl:symbol-package name)))
              (budden-tools:symbol+ 'Предикат-для--cdr--для--cons-of--по-имени-- name)))
      (setf dt-pred-def
            `(defun ,dt-pred-name (x) (typep (cdr x) ',cdr-type))))
    `(progn
       ,@(when ap `(,at-pred-def))
       ,@(when dp `(,dt-pred-def))
       (deftype ,name () '(and cons
                               ,@(when ap `((satisfies ,at-pred-name)))
                               ,@(when dp `((satisfies ,dt-pred-name))))))))

; (cl:defstruct cons car cdr)

(defmethod cl:make-load-form ((self cons) &optional env)
  (cl:declare (ignore env))
  `(make-cons :car ',(cons-car self) :cdr ',(cons-cdr self)))

(deftype atom () '(not cons))
(deftype list (&optional (type t)) "Пока проверка типа не работает, т.к. не доделан conf-of" `(or (cons-of ,type) null))
(deftype not-a-cl-cons () 'cl:atom)

; используется в макросах
(defun consp (x) (typep x 'cons))

(deftype cons-of (&rest ignore)
  (declare (ignore ignore))
  'cons)
