; -*- system :kons; coding: utf-8; -*-
;;; (C) Denis Budyak 2016
;;; @include MIT License 
;;; This code contains portions of SBCL, see also SBCL license

(named-readtables:in-readtable :buddens-readtable-a)

(cl:in-package :kons-impl)

;;; Этот код подлежит уничтожению по двум причинам:
;;; 1. Переносимость
;;; 2. Низкая производительность
;;; Посему копируем тот код, без к-рого не можем обойтись, но делаем не родовыми функциями последовательностей, а одноимёнными
;;; обычными функциями с префиксом kons.

#|(defmethod sb-sequence:length ((sequence cons))
  (kons-impl::length sequence))

 (defmethod sb-sequence:elt ((sequence cons)(index integer))
  (nth index sequence))

 (defmethod (setf sb-sequence:elt) (new-value (sequence cons) (index integer))
  (setf (nth index sequence) new-value)
  )

 (defmethod sb-sequence:adjust-sequence ((s cons) length &key initial-element (initial-contents nil icp))
  "Return destructively modified sequence or a freshly allocated
   sequence of the same class as sequence of length length. 
   Elements of the returned sequence are initialized to initial-element, 
   if supplied, initialized to initial-contents if supplied, 
   or identical to the elements of sequence if neither is supplied. 
   Signals a protocol-unimplemented error if the sequence protocol is not implemented for the class of sequence."
  (if (eql length 0)
      nil
      (let ((olength (length s)))
        (cond
         ((eql length olength) (if icp (cl:replace s initial-contents) s))
         ((< length olength)
          (rplacd (nthcdr (1- length) s) nil)
          (if icp (cl:replace s initial-contents) s))
         ((null s)
          (let ((return (make-list length :initial-element initial-element)))
            (if icp (cl:replace return initial-contents) return)))
         (t (rplacd (nthcdr (1- olength) s)
                    (make-list (- length olength)
                               :initial-element initial-element))
            (if icp (cl:replace s initial-contents) s))))))

 (defmethod sb-sequence:make-sequence-like ((s cons) length &key (initial-element nil iep) (initial-contents nil icp))
  (cond
   ((and icp iep) (error "bar"))
   (iep (make-list length :initial-element initial-element))
   (icp (unless (= (length initial-contents) length)
          (error "foo"))
        (let ((result (make-list length)))
          (cl:replace result initial-contents)
          result))
   (t (make-list length))))

|#


