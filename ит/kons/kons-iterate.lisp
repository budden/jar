; -*- system :kons; coding: utf-8; -*-
;;; (C) Denis Budyak 2016
;;; @include MIT License 
;;;; Драйверы для iterate, а также для perg-и

(named-readtables:in-readtable nil)

(def-merge-packages::! :kons-iterate-keywords-drivers
  (:always t)
  (:documentation "Драйверы для iterate-keywords")
  (:use :cl :iterate-keywords)
  (:import-from :perga-implementation
	                perga-implementation:def-perga-clause
                perga-implementation::when-perga-transformer)
  (:export "kons-iterate-keywords-drivers:k-collect"))

(cl:in-package :kons-iterate-keywords-drivers)


;;; (FOR IN &optional BY)
;; Копия из iterk, пока не доделана
;;(defclause-driver (for var in list &optional by (step ''cdr))
;;  "Elements of a list"
;;  (top-level-check)
;;  (let* ((on-var (make-var-and-default-binding 'list :type 'list))
;;	 (setqs (do-dsetq var `(car ,on-var)))
;;	 (test `(if (endp ,on-var) (go ,*loop-end*))))
;;    (setq *loop-end-used?* t)
;;    (return-driver-code :initial `((setq ,on-var ,list))
;;			:next (list test
;;				    setqs
;;				    (generate-function-step-code on-var step))
;;			:variable var)))

#|

(defun return-k-collection-code (&key variable expression 
                                      start-operation end-operation
                                      one-element
                                      test
                                      (place 'end) (result-type 'k:list))
  "Копия iterk::return-collection-code"
  (when (iterk::quoted? result-type) (setq result-type (second result-type)))
  (when (iterk::quoted? place) (setq place (second place)))
  (let ((place-string (locally (declare (optimize safety)) (symbol-name place))))
    (cond
     ((string= place-string '#:end)
      (setq place 'end))
     ((or (string= place-string '#:start)
	  (string= place-string '#:beginning))
      (setq place 'start))
     (t 
      (iterk::clause-error "~a is neither 'start', 'beginning' nor 'end'" place))))
  (let* ((collect-var-spec (or variable iterk::*result-var*))
	 (collect-var (iterk::extract-var collect-var-spec))
	 (entry (iterk::make-accum-var-binding collect-var-spec nil :collect
		 :type (if (eq result-type 'k:list) 'k:list
			 `(or k:list ,result-type))))
	 (end-pointer (third entry))
	 (prev-result-type (fourth entry)))
    (cond
     ((null end-pointer)
      (if (eq place 'end)
	  (setq end-pointer
                (iterk::make-var-and-binding
                 'end-pointer nil :type 'k:list)))
      (setf (cddr entry) (list end-pointer result-type)))
     (t
      (if (not (equal result-type prev-result-type))
	  (iterk::clause-error "Result type ~a doesn't match ~a" 
			result-type prev-result-type))))
    (let* ((expr (iterk::walk-expr expression))
	   (op-expr 
	    (if (eq place 'start)
		(if (null start-operation)
		    expr
		    (iterk::make-application start-operation expr collect-var))
		(if (null end-operation)
		    expr
		    (iterk::make-application end-operation collect-var expr)))))
      (if (eq place 'start)
          (iterk::return-code :body `((setq ,collect-var ,op-expr))
                       :final (unless (eq result-type 'k:list)
                                `((setq ,collect-var
                                        (coerce ,collect-var ',result-type)))))
	  (iterk::with-temporary temp-var
	    ;; In the update code, must test if collect-var is null to allow
	    ;; for other clauses to collect into same var.  This code
	    ;; is a tad bummed, but probably more for looks than real
	    ;; efficiency.
	    (let* ((update-code `(if ,collect-var
				     (setf (k:cdr ,end-pointer) ,temp-var)
				     (setq ,collect-var ,temp-var)))
		   (main-code (cond
			       ((not one-element)
				`((if (setq ,temp-var ,op-expr)
				      (setq ,end-pointer 
					    (k:last ,update-code)))))
			       (test
				`((when ,(iterk::make-application test
                                                                 collect-var expr)
				      (setq ,temp-var ,op-expr)
				      (setq ,end-pointer ,update-code))))
			       (t
				`((setq ,temp-var ,op-expr)
				  (setq ,end-pointer ,update-code))))))
				
	      (iterk::return-code 
	       ;; Use a progn so collect-var isn't mistaken for a tag.
	       :body `((progn ,.main-code ,collect-var))
	       :final (if (eq result-type 'k:list)
			  nil
			  `((setq ,collect-var 
				  (coerce ,collect-var ',result-type)))))))))))

(iterk::defclause (k-collect expr &optional into var at (place 'iterk::end) 
		    		   result-type (type 'k:list))
  "Collect into a k-list. (defclause (collect expr &optional into var at (place 'end)"
  (return-k-collection-code
   :variable var
   :expression expr
   :one-element t
   :start-operation 'k:cons 
   :end-operation '(subst (var expr) (k:list expr))
   :place place
   :result-type type))

(iterk::defsynonym :k-for k-for)
(iterk::defsynonym :k-collect k-collect)
(iterk::defsynonym :k-collecting k-collect)

(iterk::defclause (k-append expr &optional into var at (place 'iterk::end))
  "Append into a list"
  (return-k-collection-code
   :variable var
   :expression expr
   :start-operation 'k:append
   :end-operation '(subst (var expr) (k:copy-list expr))
   :place place
   :one-element nil))

(iterk::defsynonym :k-append k-append)
(iterk::defsynonym :k-appending k-append)
|#
;;;; kons+perga (dolist)
(def-perga-clause k:dolist 'when-perga-transformer)
