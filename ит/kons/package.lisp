; -*- system :kons; -*-
;;; cons abstraction (SBCL specific)
#| Хотим сделать структуру такую же, как cons, но которая при этом не будет консом

Пока принимаем, что символы имеют те же имена, но берутся из пакета kons. Это тупиковый путь, 
поскольку в лиспе преобразования исходного кода, вообще говоря, нельзя отделить от работы с данными.

Поэтому, код, использующий "абстракцию конса", всё же должен различать консы (настоящие, для работы
с исходным текстом) и абстрактные (применимые только для данных). 

С этой точки зрения лучше иметь разные имена для обычных и абстрактных консов. 

Но удобно разбить работу на отдельные этапы, один из которых - реализация абстрактного конса, а второй - 
переименование. 

|#
;;; (C) Denis Budyak 2016
;;; @include MIT License 
;;; This code contains portions of SBCL, see also SBCL license

(named-readtables:in-readtable :buddens-readtable-a)

(def-merge-packages::! :kons-impl
  (:always t)
  (:use :cl-impl)
  (:shadowing-import-from :cl
                          cl:defun
                          cl:deftype
                          cl:defmacro
                          cl:defparameter
                          cl:defsetf
                          cl:defmethod
                          cl:not

                          cl:typep
                          cl:coerce

                          cl:&rest
                          cl:declare
                          cl:declaim
                          cl:ignore
                          cl:inline
                          cl:dynamic-extent

                          cl:fixnum
                          cl:bignum
                          cl:string
                          cl:integer
                          cl:stream

                          cl:let
                          cl:let*
                          cl:tagbody
                          cl:go
                          cl:block

                          cl:t
                          cl:nil
                          cl:null
                          
                          cl:progn                          
                          cl:cond
                          cl:loop
                          cl:shiftf
                          cl:return
                          cl:setf
                          cl:setq
                          cl:etypecase
                          cl:typecase
                          cl:case
                          cl:ecase
                          
                          cl:funcall
                          cl:apply
                          
                          cl:prog1
                          cl:copy-symbol
                          cl:make-symbol
                          cl:if
                          cl:when
                          cl:unless

                          cl:symbolp
                          cl:identity 

                          cl:multiple-value-bind
                          cl:destructuring-bind

                          cl:&environment
                          cl:&optional
                          cl:&key
                          cl:&body
                          cl:&rest
                          cl:&aux

                          cl:assert
                          cl:break

                          cl:eq
                          cl:eql
                          cl:equalp
                          cl:print
                          cl:format

                          cl:and
                          cl:or

                          cl:<
                          cl:<=
                          cl:>=
                          cl:=
                          cl:>
                          cl:+
                          cl:-

                          cl:error

                          cl:lambda
                          cl:declare
                          cl:optimize
                          cl:speed
                          cl:ignore

                          cl:eval-when
                          cl:in-package
                          
                          cl:do
                          cl:do*
                          cl:flet
                          cl:labels
                          cl:macrolet

                          cl:type
                          cl:unsigned-byte
                          cl:incf
                          cl:decf
                          cl:zerop
                          cl:plusp
                          cl:mod
                          cl:1-
                          cl:1+

                          cl:gensym
                          cl:intern

                          cl:constantp

                          cl:values

                          cl:deftype
                          cl:satisfies
                          cl:lambda
                          cl:check-type

                          cl:print-object
                          cl:*print-circle*
                          cl:*print-pretty*
                          )


  #|(:import-from :cl-impl
                CL-IMPL:truly-the
                CL-IMPL:while 
                CL-IMPL:singleton-p
                CL-IMPL:symbolicate
                CL-IMPL:store-defsetf-method-simple
                CL-IMPL:fast-&rest-nth)|#

  (:import-from :budden-tools
                budden-tools:perga
                budden-tools:str++
                budden-tools:the*)
  #|(:shadow
    #:consp
    #:cons
    #:car
    #:cdr
    #:caar
    #:cdar
    #:cddr
    #:cadr
    
    #:equal
    #:rplaca
    #:rplacd
    #:atom
    #:list
    #:list*
    #:pop 
    #:push
    #:lst
    #:mapcar
    #:|{|
    #:|}|
    #:equal

    #:length
    )|#

  (:export "
    kons-impl:cons  ; function and a type

    kons-impl:not-a-cl-cons

    kons-impl:car 
    kons-impl:cdr
    kons-impl:consp

    kons-impl:cons-of ; устарело!    
    KONS-IMPL:DEFTYPE-CONS-OF

    kons-impl:caar
    kons-impl:cdar
    kons-impl:cadr
    kons-impl:cddr

    kons-impl:cdddr

    kons-impl:equal
    
    kons-impl:endp
    kons-impl:nthcdr           
    kons-impl:nth
            
    kons-impl:atom ; function and a type
    kons-impl:list
    kons-impl:list*
    kons-impl:listp
    kons-impl:make-list

    kons-impl:rplaca
    kons-impl:rplacd
    
    kons-impl:first
    kons-impl:rest

    kons-impl:second
    kons-impl:third 

    kons-impl:last

    kons-impl:pop
    kons-impl:push
    kons-impl:mapcar
    kons-impl:adjoin
    kons-impl:member
    kons-impl:find
    kons-impl:sort

    kons-impl:length 
    kons-impl:reverse
    kons-impl:every
    kons-impl:remove-if
    kons-impl:subseq
    kons-impl:remove-duplicates

    kons-impl:list-length
    kons-impl:dolist

    kons-impl:append
    
    kons-impl:nconc
    kons-impl:copy-list
    kons-impl:revappend

    kons-impl:budden-tools-maptree
    kons-impl:budden-tools-1-to-list
    kons-impl:budden-tools-collect-duplicates
    kons-impl:BUDDEN-TOOLS-COLLECT-INTO-TAIL-OF-LIST

    ;; преобразовывает верхний уровень в список, с защитой от зацикливания по длине
    kons-impl:k-list-to-cl-list 
    
    ;; преобразовывает список в k-список, без защиты от циклов
    kons-impl:cl-list-to-k-list 

    kons-impl:limited-equalp
    KONS-IMPL::Скопировать-до-хвоста-не-включительно   
    KONS-IMPL::Разобрать-на-непрерывные-группы-с-одинаковым-значением-ключа 
   "))

(def-merge-packages::! :kons
  (:always t)
  (:use :kons-impl)
  (:nicknames :k)  
  (:export "
    kons:cons  ; function and a type

    kons:not-a-cl-cons

    kons:car 
    kons:cdr
    kons:consp
    
    kons:cons-of ; type, устарел!
    kons:deftype-cons-of 

    kons:caar
    kons:cdar
    kons:cadr
    kons:cddr

    kons:cdddr

    kons:equal
    
    kons:endp
    kons:nthcdr           
    kons:nth
           
    kons:atom ; function and a type
    kons:list
    kons:list*
    kons:listp
    kons:make-list

    kons:|{|
    kons:|}|

    kons:rplaca
    kons:rplacd
    
    kons:first
    kons:rest

    kons:second
    kons:third 

    kons:last

    kons:pop
    kons:push
    kons:mapcar
    kons:adjoin
    kons:member
    kons:remove-if
    kons:subseq
    kons:remove-duplicates
    kons:find
    kons:sort

    kons:length 
    kons:reverse 
    kons:every

    kons:list-length
    kons:dolist

    kons:append
    
    kons:nconc
    kons:copy-list
    kons:revappend

    kons:budden-tools-maptree
    kons:budden-tools-1-to-list
    kons:budden-tools-collect-duplicates
    kons:BUDDEN-TOOLS-COLLECT-INTO-TAIL-OF-LIST

    kons:k-list-to-cl-list 
    kons:cl-list-to-k-list
    kons:limited-equalp
    KONS:Скопировать-до-хвоста-не-включительно  
    KONS::Разобрать-на-непрерывные-группы-с-одинаковым-значением-ключа 
   
   "))
