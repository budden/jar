; -*- system :kons; -*-

(named-readtables:in-readtable nil)

(def-merge-packages::! :kons-reader
  (:always t)
  (:use :cl)
  (:import-from :kons #:|{| #:|}|)
  (:import-from :budden-tools
                budden-tools:def-symbol-readmacro
                budden-tools:the*))

(in-package :kons-reader)

(defun {-readmacro-reader (stream symbol)
  (declare (ignore symbol))
  ;(read-char stream)
  (read-list stream nil #\}))

(def-symbol-readmacro |{| #'{-readmacro-reader)

;;; from CCL
(defun %read-list-expression (stream dot-ok &optional (termch #\)))
  (loop
      (let* ((firstch (ccl::%next-non-whitespace-char-and-attr-no-eof stream)))
        (if (eq firstch termch)
            (return (values nil nil nil))
            (multiple-value-bind (val val-p source-info)
                (ccl::%parse-expression stream firstch dot-ok)
              (if val-p
                  (return (values val t source-info))))))))

(defun read-list (stream &optional nodots (termch #\)))
  (let* ((dot-ok (k:cons nil nil))
         (head (k:cons nil nil))
         (tail head)
         (source-note-list nil))
    (declare (dynamic-extent dot-ok head)
             (k:list head tail))
    (if nodots (setq dot-ok nil))
    (multiple-value-bind (firstform firstform-p firstform-source-note)
        (ccl::%read-list-expression stream dot-ok termch)
      (when firstform-source-note
        (push firstform-source-note source-note-list))
      (when firstform-p
        (when (eq firstform '|}|)
          (return-from read-list (values (k:cdr head) source-note-list)))
        (if (and dot-ok (eq firstform dot-ok))       ; just read a dot
            (ccl::signal-reader-error stream "Dot context error."))
        (k:rplacd tail (setq tail (k:cons firstform nil)))
        (loop
          (multiple-value-bind (nextform nextform-p nextform-source-note)
              (ccl::%read-list-expression stream dot-ok termch)
            (when nextform-source-note
              (push nextform-source-note source-note-list))
            (if (not nextform-p) (return))
            (when (eq nextform '|}|)  (return))
            (if (and dot-ok (eq nextform dot-ok))    ; just read a dot
                (if (multiple-value-bind (lastform lastform-p lastform-source-note)
                        (ccl::%read-list-expression stream nil termch)
                      (when lastform-source-note
                        (k:push lastform-source-note source-note-list))
                      (and lastform-p
                           (progn (k:rplacd tail lastform)
                                  (not (nth-value 1 (ccl::%read-list-expression stream nil termch))))))
                    (return)
                    (ccl::signal-reader-error stream "Dot context error."))
              (k:rplacd tail (setq tail (k:cons nextform nil))))))))
    (values (k:cdr head) source-note-list)))
