;; -*- encoding: utf-8; coding: utf-8; -*- 
;; Copyright (C) Денис Будяк 2015

(defsystem :kons
  :depends-on (:budden-tools
               :buddens-reader
               :alexandria
               :iterate-keywords
               )
  :serial t
  :components ((:file "package")
               (:file "kons.h")
               (:file "parts-of-sbcl" :description "Части кода, взятые из SBCL и переписанные на CL")
               (:file "kons") ; FIXME вынести kons-printer-sbcl
               #+SBCL (:file "kons-printer-sbcl")
               #+CCL (:file "kons-printer-ccl")
               (:file "kons-adjoin" :description "Зависимость от ф-ии, определённой в kons + долго выполняется")
               (:file "kons-sequence" :description "Расширяемые последовательности SBCL")
               #+SBCL (:file "kons-reader" :description "Позволяет читать с помощью символов kons:{ и kons:} (не забывайте ставить пробелы")
               #+CCL (:file "kons-reader-ccl")
               (:file "kons-iterate" :description ":k-collect")
               (:file "kons-2" :description "То, что зависит от kons-iterate и от kons-adjoin")
               (:file "kons-test")
               ))

