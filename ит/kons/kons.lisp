; -*- system :kons; coding: utf-8; -*-
;;; cons abstraction (SBCL specific)
#| Хотим сделать структуру такую же, как cons, но которая при этом не будет консом

Пока принимаем, что символы имеют те же имена, но берутся из пакета kons. Это тупиковый путь, 
поскольку в лиспе преобразования исходного кода, вообще говоря, нельзя отделить от работы с данными.

Поэтому, код, использующий "абстракцию конса", всё же должен различать консы (настоящие, для работы
с исходным текстом) и абстрактные (применимые только для данных). 

С этой точки зрения лучше иметь разные имена для обычных и абстрактных консов. 

Но удобно разбить работу на отдельные этапы, один из которых - реализация абстрактного конса, а второй - 
переименование. 

|#
;;; (C) Denis Budyak 2016
;;; @include MIT License 
;;; This code contains portions of SBCL, see also SBCL license

(named-readtables:in-readtable :buddens-readtable-a)

(cl:in-package :kons-impl)

(defmacro |BUDDEN-TOOLS-COLLECT-INTO-TAIL-OF-LIST| (|Значение| |Переменная-результат| |Переменная-точка-роста|)
  "Точный аналог budden-tools:COLLECT-INTO-TAIL-OF-LIST"
  `(cond
    ((null ,|Переменная-результат|)
     (setf ,|Переменная-результат|
           (setf ,|Переменная-точка-роста| (list ,|Значение|))))
    (t
     (setf (cdr ,|Переменная-точка-роста|)
           (setf ,|Переменная-точка-роста| (list ,|Значение|))))))

(defun cons (a d) (make-cons :car a :cdr d))

(defun car (x)
  (etypecase x
    (cons (cons-car x))
    (null nil)
    ))

(defun first (list)
  (car list))

(defun second (list)
  (cadr list))

(defun third (list)
  (car (cdr (cdr list))))

(defun cdr (x)
  (etypecase x
    (cons (cons-cdr x))
    (null nil)
    ))

(defun rest (list)
  (cdr list))

(defun caar (x) (car (car x)))
(defun cadr (x) (car (cdr x)))
(defun cdar (x) (cdr (car x)))
(defun cddr (x) (cdr (cdr x)))
(defun cdddr (x) (cdr (cdr (cdr x))))

(defun equal (a b)
  (typecase a
    (cons
     (typecase b
       (cons 
        (and (equal (car a) (car b))
             (equal (cdr a) (cdr b))))
       (t
        nil)))
    (t
     (cl:equal a b))))

(defun limited-equalp (x y)
  "Ограниченная версия - рекурсивно сравнивает k-списки только внутри k-консов и обычных консов. Остальные структуры данных сравниваются с помощью equalp"
  (cond
   ((eq x y) t)
   ((cl:characterp x) (and (cl:characterp y) (cl:char-equal x y)))
   ((cl:numberp x) (and (cl:numberp y) (= x y)))
   ((consp x)
    (and (consp y)
         (limited-equalp (car x) (car y))
         (limited-equalp (cdr x) (cdr y))))
   ((cl:consp x)
    (and (cl:consp y)
         (limited-equalp (cl:car x) (cl:car y))
         (limited-equalp (cl:cdr x) (cl:cdr y))))
   (t (cl:equalp x y))))

(assert (limited-equalp t t))
(assert (limited-equalp #\a #\a))
(assert (limited-equalp 1 1))
(assert (limited-equalp (cons 1 2) (cons 1 2)))
(assert (limited-equalp (cl:cons 1 2) (cl:cons 1 2)))
(assert (not (limited-equalp (cons 1 2) (cl:cons 1 2))))
  
;; Я было декорировал equalp, но потом не создаётся equalp хеш-таблица и не загружается hunchentoot. Поэтому это я убрал.
;;(cl-advice:portably-without-package-locks
;; (cl-advice:define-advice equalp #'decorated-equalp))

(defun rplaca (cons object)
  (setf (cons-car cons) object)
  cons)

(defun rplacd (cons object)
  (setf (cons-cdr cons) object)
  cons)

(defun %rplaca (x val) (rplaca x val) val)
(defun %rplacd (x val) (rplacd x val) val)

(defsetf car %rplaca)
(defsetf cdr %rplacd)

(defun endp (object)
  "This is the recommended way to test for the end of a proper list. It
   returns true if OBJECT is NIL, false if OBJECT is a CONS, and an error
   for any other type of OBJECT."
  (etypecase object
    (null t)
    (cons nil)))

(defun nthcdr (n list)
  (flet ((fast-nthcdr (n list)
           #+SBCL (declare (type cl-impl:index n))
           (do ((i n (1- i))
                (result list (cdr result)))
               ((not (plusp i)) result)
             #+SBCL (declare (type cl-impl:index i)))))
    (typecase n
      (cl-impl:index (fast-nthcdr n list))
      (t (do ((i 0 (1+ i))
              (r-i list (cdr r-i))
              (r-2i list (cddr r-2i)))
             ((and (eq r-i r-2i) (not (zerop i)))
              (fast-nthcdr (mod n i) r-i))
           #+SBCL (declare (type cl-impl:index i)))))))

(defun nth (n list)
  (car (nthcdr n list)))

(defun %setnth (n list newval)
  (typecase n
    #+SBCL (cl-impl:index
     (do ((count n (1- count))
          (list list (cdr list)))
         ((endp list)
          (error "~S is too large an index for SETF of NTH." n))
       (declare (type fixnum count))
       (when (<= count 0)
         (rplaca list newval)
         (return newval))))
    (t (let ((cons (nthcdr n list)))
         (when (endp cons)
           (error "~S is too large an index for SETF of NTH." n))
         (rplaca cons newval)
         newval))))

(eval-when (:compile-toplevel :load-toplevel :execute)
(defun %cxr-setf-expander (sub-accessor setter)
  (flet ((expand (place-reader original-form)
           (let ((temp (make-symbol "LIST"))
                 (newval (make-symbol "NEW")))
             (values (cl:list temp)
                     `((,@place-reader ,@(cl:cdr original-form)))
                     (cl:list newval)
                     `(,setter ,temp ,newval)
                     `(,(if (eq setter '%rplacd) 'cdr 'car) ,temp)))))
    (if (eq sub-accessor 'nthcdr) ; random N
        (lambda (access-form env)
          (declare (ignore env))
          #+SBCL (declare (sb-c::lambda-list (n list)))
          (destructuring-bind (n list) (cl:cdr access-form) ; for effect
            (declare (ignore n list)))
          (expand '(nthcdr) access-form))
        ;; NTHCDR of fixed N, or CxxxxR composition
        (lambda (access-form env)
          (declare (ignore env))
          #+SBCL (declare (sb-c::lambda-list (list)))
          (destructuring-bind (list) (cl:cdr access-form) ; for effect
            (declare (ignore list)))
          (expand sub-accessor access-form))))))


(macrolet ((def (name &optional alias &aux (string (string name)))
             `(eval-when (:compile-toplevel :load-toplevel :execute)
                (let ((closure
                       (%cxr-setf-expander
                        '(,(symbolicate "C" (cl:subseq string 2)))
                        ',(symbolicate "%RPLAC" (cl:subseq string 1 2)))))
                  (store-defsetf-method-simple ',name closure)
                  ,@(when alias `((store-defsetf-method-simple ',alias closure)))))))
  ;; Rather than expand into a DEFINE-SETF-EXPANDER, install a single closure
  ;; as the expander and capture just enough to distinguish the variations.
  (def caar)
  (def cadr second)
  (def cdar)
  (def cddr)
  #|(def caaar)
  (def cadar)
  (def cdaar)
  (def cddar)
  (def caadr)
  (def caddr third)
  (def cdadr)
  (def cdddr)
  (def caaaar)
  (def cadaar)
  (def cdaaar)
  (def cddaar)
  (def caadar)
  (def caddar)
  (def cdadar)
  (def cdddar)
  (def caaadr)
  (def cadadr)
  (def cdaadr)
  (def cddadr)
  (def caaddr)
  (def cadddr fourth)
  (def cdaddr)
  (def cddddr)|#
  )

(eval-when (:compile-toplevel :load-toplevel :execute)
  (store-defsetf-method-simple 'nth (%cxr-setf-expander 'nthcdr '%rplaca)))

(defun atom (object) (typep object 'atom))

(defun list (&rest args)
  (let ((result nil)
        (rostok nil))
    (loop
      (cond
       ((null args)
        (return result))
       ((null result)
        (setf rostok (cons (cl:car args) nil))
        (setf result rostok)
        (setf args (cl:cdr args)))
       (t
        (setf (cdr rostok) (cons (cl:car args) nil))
        (setf rostok (cdr rostok))
        (setf args (cl:cdr args))
        )))))


(defun list* (arg &rest others)
  (let ((length (cl:length others)))
    (cond ((= length 0) arg)
          ((= length 1)
           (cons arg (fast-&rest-nth 0 others)))
          (t
           (let* ((cons (list arg))
                  (result cons)
                  (index 0)
                  (1-length (1- length)))
             (loop
              (cond
                ((< index 1-length)
                 (setf cons
                       (setf (cdr cons)
                             (list (fast-&rest-nth index others))))
                 (incf index))
                (t (return nil))))
             (setf (cdr cons) (fast-&rest-nth index others))
             result)))))


#| OLD - from SBLC
 (defmacro pop (place &environment env)
  (if (symbolp (setq place (sb-impl::macroexpand-for-setf place env)))
      `(prog1 (car ,place) (setq ,place (cdr ,place)))
      (multiple-value-bind (temps vals stores setter getter)
                           (cl:get-setf-expansion place env)
        (let ((list (copy-symbol 'list))
              (ret (copy-symbol 'car)))
          `(let* (,@(cl:mapcar #'cl:list temps vals)
                    (,list ,getter)
                    (,ret (car ,list))
                    (,(cl:car stores) (cdr ,list))
                    ,@(cl:cdr stores))
             ,setter
             ,ret))))) |#

;; from CCL (Apache license)
(defmacro pop (place &environment env &aux win)
  "The argument is a location holding a list. Pops one item off the front
  of the list and returns it."
  (cl-impl:while (cl:atom place)
    (cl:multiple-value-setq (place win) (cl:macroexpand-1 place env))
    (unless win
      (cl:return-from pop
        `(prog1 (car ,place) (setq ,place (cdr (the* list ,place)))))))
  (let ((value (gensym)))
    (multiple-value-bind (dummies vals store-var setter getter)
                         (cl:get-setf-expansion place env)
      `(let* (,@(cl:mapcar #'cl:list dummies vals)
              (,value ,getter)
              (,(cl:car store-var) (cdr ,value)))
         ,@dummies
         ,(cl:car store-var)
         (prog1
           (car (the* list ,value))
           ,setter)))))

;; from CCL (Apache license)
(defmacro push (value place &environment env)
  "Takes an object and a location holding a list. Conses the object onto
  the list, returning the modified list. OBJ is evaluated before PLACE."
  (if (not (cl:consp place))
    `(setq ,place (cons ,value ,place))
    (multiple-value-bind (dummies vals store-var setter getter)
                         (cl:get-setf-expansion place env)
      (let ((valvar (gensym)))
        `(let* ((,valvar ,value)
                ,@(cl:mapcar #'cl:list dummies vals)
                (,(cl:car store-var) (cons ,valvar ,getter)))
           ,@dummies
           ,(cl:car store-var)
           ,setter)))))


(defun listp (x) (typep x 'list))

;;; Returns a list of members of LIST. Useful for dealing with circular lists.
;;; For a dotted list returns a secondary value of T -- in which case the
;;; primary return value does not include the dotted tail.
;;; If the maximum length is reached, return a secondary value of :MAYBE.
(defun list-members (list &key max-length)
  (when list
    (do ((tail (cdr list) (cdr tail))
         (members (list (car list)) (cons (car tail) members))
         (count 0 (1+ count)))
        ((or (not (consp tail)) (eq tail list)
             (and max-length (>= count max-length)))
         (values members (or (not (listp tail))
                             (and (>= count max-length) :maybe)))))))

(defun every (predicate list)
  (loop
    (unless list
      (return t))
    (let ((car (pop list)))
      (unless (funcall predicate car)
        (return nil))
      )))

(defmacro dolist ((var list &optional (result nil)) &body body &environment env)
  #-SBCL (declare (ignore env))
  (multiple-value-bind (forms decls) (alexandria:parse-body body)
    (let* ((n-list (gensym "N-LIST"))
           (start (gensym "START")))
      (multiple-value-bind (clist members clist-ok)
          (cond #+SBCL
                ((constantp list env)
                 (let ((value (sb-int:constant-form-value list env)))
                   (multiple-value-bind (all dot) (list-members value :max-length 20)
                     (when (eql dot t)
                       ;; Full warning is too much: the user may terminate the loop
                       ;; early enough. Contents are still right, though.
                       (uiop:style-warn "Dotted list ~S in DOLIST." value))
                     (if (eql dot :maybe)
                         (values value nil nil)
                         (values value all t)))))
                #+SBCL ((and (consp list) (eq 'list (car list))
                      (every (lambda (arg) (constantp arg env)) (cdr list)))
                 (let ((values (mapcar (lambda (arg) (sb-int:constant-form-value arg env)) (cdr list))))
                   (values values values t)))
                (t
                 (values nil nil nil)))
        `(block nil
           (let ((,n-list ,(if clist-ok clist list)))
              ; difference from cl:list as we need no quote
             (tagbody
                ,start
                (unless (endp ,n-list)
                  (let ((,var ,(if clist-ok
                                   `(cl-impl:truly-the (cl:member ,@(k-list-to-cl-list members)) (car ,n-list))
                                   `(car ,n-list))))
                    ,@decls
                    (setq ,n-list (cdr ,n-list))
                    (tagbody ,@forms))
                  (go ,start))))
           ,(if result
                `(let ((,var nil))
                   ;; Filter out TYPE declarations (VAR gets bound to NIL,
                   ;; and might have a conflicting type declaration) and
                   ;; IGNORE (VAR might be ignored in the loop body, but
                   ;; it's used in the result form).
                   ,@(cl-impl:filter-dolist-declarations decls)
                   ,var
                   ,result)
                nil))))))

(defun list-length (list)
  (do ((n 0 (+ n 2))
       (y list (cddr y))
       (z list (cdr z)))
      (())
    (declare (type fixnum n)
             (type list y z))
    (when (endp y) (return n))
    (when (endp (cdr y)) (return (+ n 1)))
    (when (and (eq y z) (> n 0)) (return nil))))


(defun %coerce-callable-to-fun (callable)
  (etypecase callable
    (cl:function callable)
    (cl:symbol (coerce callable 'cl:function))))

(defun member (item list &key key (test nil testp) (test-not nil notp))
  "Return the tail of LIST beginning with first element satisfying EQLity,
   :TEST, or :TEST-NOT with the given ITEM."
  (when (and testp notp)
    (error ":TEST and :TEST-NOT were both supplied."))
  (let ((key (and key (%coerce-callable-to-fun key)))
        (test (and testp (%coerce-callable-to-fun test)))
        (test-not (and notp (%coerce-callable-to-fun test-not))))
    (cond (test
           (if key
               (%member-key-test item list key test)
               (%member-test item list test)))
          (test-not
           (if key
               (%member-key-test-not item list key test-not)
               (%member-test-not item list test-not)))
          (t
           (if key
               (%member-key item list key)
               (%member item list))))))

(defun remove-if (predicate list)
  (let ((rez nil)
        (tr nil))
    (k:dolist (x list)
              (unless (funcall predicate x)
                (k:budden-tools-collect-into-tail-of-list x rez tr)))
    rez))

(defun remove-duplicates (k-list &rest keyargs)
  (let* ((cl-list (k:k-list-to-cl-list k-list))
         (cl-list-no-duplicates (apply #'cl:remove-duplicates cl-list keyargs)))
    (k:cl-list-to-k-list cl-list-no-duplicates)))

(defun find (element k-list &rest keyargs)
  "Keyargs Берутся из member"
  (car (apply 'member element k-list keyargs)))

(defun sort (k-list predicate &rest keyargs)
  "В отличие от обычного sort, не разрушает k-список, а копирует его. Вообще надо превращать не в список, а в массив - он более естественен для сортировок"
  (let ((cl-list (k:k-list-to-cl-list k-list)))
    (setf cl-list (apply 'cl:sort cl-list predicate keyargs))
    (k:cl-list-to-k-list cl-list)))

(defun subseq (list start &optional end)
  (cond
   ((null end)
    (copy-list (nthcdr start list)))
   (t
    (let ((tail (nthcdr start list))
          (rez nil)
          (tr nil))
      (cl:dotimes (i (- end start))
        (budden-tools-collect-into-tail-of-list (pop tail) rez tr))
      rez))))

(defun length (list)
  "В SBSL эта ф-я не нужна, т.к. cl:length полиморфна, но мы не хотим зависеть от SBCL"
  (let ((result 0))
    (dolist (x list)
      (declare (ignore x))
      (incf result))
    result))

;; from SBCL
(macrolet ((last0-macro ()
             `(let ((rest list)
                    (list list))
                (loop (unless (consp rest)
                        (return rest))
                  (shiftf list rest (cdr rest)))))
           (last1-macro ()
             `(let ((rest list)
                    (list list))
                (loop (unless (consp rest)
                        (return list))
                  (shiftf list rest (cdr rest)))))
           (lastn-macro (type)
             `(let ((returned-list list)
                    (checked-list list)
                    (n (cl-impl:truly-the ,type n)))
                (declare (,type n))
                (tagbody
                 :scan
                   (pop checked-list)
                   (when (atom checked-list)
                     (go :done))
                   (if (zerop (cl-impl:truly-the ,type (decf n)))
                       (go :pop)
                       (go :scan))
                 :pop
                   (pop returned-list)
                   (pop checked-list)
                   (if (atom checked-list)
                       (go :done)
                       (go :pop))
                 :done)
                returned-list)))

  (defun %last0 (list)
    (declare (optimize speed #+SBCL (sb-c::verify-arg-count 0)))
    (last0-macro))

  (defun %last1 (list)
    (declare (optimize speed #+SBCL (sb-c::verify-arg-count 0)))
    (last1-macro))

  (defun %lastn/fixnum (list n)
    (declare (optimize speed #+SBCL (sb-c::verify-arg-count 0))
             (type (and unsigned-byte fixnum) n))
    (case n
      (1 (last1-macro))
      (0 (last0-macro))
      (t (lastn-macro fixnum))))

  (defun %lastn/bignum (list n)
    (declare (optimize speed #+SBCL (sb-c::verify-arg-count 0))
             (type (and unsigned-byte bignum) n))
    (lastn-macro unsigned-byte))

  (defun last (list &optional (n 1))
    "Return the last N conses (not the last element!) of a list."
    (case n
      (1 (last1-macro))
      (0 (last0-macro))
      (t
       (typecase n
         (fixnum
          (lastn-macro fixnum))
         (bignum
          (lastn-macro unsigned-byte)))))))


(defun map1 (fun-designator arglists accumulate take-car)
  "From sb-kernel"
  (do* ((fun (cl-impl:coerce-callable-to-fun fun-designator))
        (non-acc-result (cl:car arglists))
        (ret-list (list nil))
        (temp ret-list)
        (res nil)
        (args (cl:make-list (cl:length arglists))))
       ((cl:dolist (x arglists) (or x (return t)))
        (if accumulate
            (cdr ret-list)
            non-acc-result))
    (do ((l arglists (cl:cdr l))
         (arg args (cl:cdr arg)))
        ((null l))
      (setf (cl:car arg) (if take-car (car (cl:car l)) (cl:car l)))
      (setf (cl:car l) (cdr (cl:car l))))
    (setq res (cl:apply fun args))
    (case accumulate
      (:nconc
       (when res
         (setf (cdr temp) res)
         ;; KLUDGE: it is said that MAPCON is equivalent to
         ;; (apply #'nconc (maplist ...)) which means (nconc 1) would
         ;; return 1, but (nconc 1 1) should signal an error.
         ;; The transformed MAP code returns the last result, do that
         ;; here as well for consistency and simplicity.
         (when (consp res)
           (setf temp (last res)))))
      (:list (setf (cdr temp) (list res)
                   temp (cdr temp))))))

(defun mapcar (function list &rest more-lists)
  "Apply FUNCTION to successive elements of LIST. Return list of FUNCTION
   return values."
  (cl:check-type list (or cons null))
  (map1 function (cl:cons list more-lists) :list t)) 


(defun make-list (size &key initial-element)
  (%make-list size initial-element))

(defun %make-list (size initial-element)
  #+SBCL (declare (type cl-impl:index size))
  (do ((count size (1- count))
       (result '() (cons initial-element result)))
      ((<= count 0) result)
    #+SBCL (declare (type cl-impl:index count))))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defmacro !copy-list-macro (list &key check-proper-list)
    ;; Unless CHECK-PROPER-LIST is true, the list is copied correctly
    ;; even if the list is not terminated by NIL. The new list is built
    ;; by CDR'ing SPLICE which is always at the tail of the new list.
    `(when ,list
       (let ((copy (list (car ,list))))
         (do ((orig (cdr ,list) (cdr orig))
              (splice copy (cdr (rplacd splice (cons (car orig) nil)))))
             (,@(if check-proper-list
                    '((endp orig))
                    '((atom orig)
                      (unless (null orig)
                        (rplacd splice orig))))
              copy))))))

(defun copy-list (list)
  (!copy-list-macro list))

(defun append (&rest lists)
  #+SBCL (declare (sb-impl::truly-dynamic-extent lists) (optimize cl:speed))
  #-SBCL (declare (cl:dynamic-extent lists) (optimize cl:speed))
  (labels ((fail (object)
             (error 'cl:type-error
                    :datum object
                    :expected-type 'list))
           (append-into (last-cons current rest)
             ;; Set (CDR LAST-CONS) to (APPLY #'APPEND CURRENT REST).
             (declare (cons last-cons))
             (declare (cl:cons rest))
             (if (listp current)
                 (if (consp current)
                     ;; normal case, cdr down the list
                     (append-into (setf (cdr last-cons) (list (car current)))
                                  (cdr current)
                                  rest)
                     ;; empty list
                     (let ((more (cl:cdr rest)))
                       (if (null more)
                           (setf (cdr last-cons) (cl:car rest))
                           (append-into last-cons (cl:car rest) more))))
                 (fail current)))
           (append1 (lists)
             (let ((current (cl:car lists))
                   (rest (cl:cdr lists)))
               (cond ((null rest)
                      current)
                     ((consp current)
                      (let ((result (cl-impl:truly-the cons (list (car current)))))
                        (append-into result
                                     (cdr current)
                                     rest)
                        result))
                     ((null current)
                      (append1 rest))
                     (t
                      (fail current))))))
    (append1 lists)))


(defun revappend (x y)
  "Return (append (reverse x) y)."
  (do ((top x (cdr top))
       (result y (cons (car top) result)))
      ((endp top) result)))

(defun nconc (&rest lists)
  "Concatenates the lists given as arguments (by changing them)"
   (declare (optimize speed))
   (flet ((fail (object)
            (error 'type-error
                   :datum object
                   :expected-type 'list)))
     (cl-impl:do-rest-arg ((result index) lists)
         (typecase result
           (cons
            (let ((splice result))
              (cl-impl:do-rest-arg ((ele index) lists (1+ index))
                  (typecase ele
                    (cons (rplacd (last splice) ele)
                          (setf splice ele))
                    (null (rplacd (last splice) nil))
                    (atom (if (< (1+ index) (length lists))
                              (fail ele)
                              (rplacd (last splice) ele)))))
              (return result)))
           (null)
           (atom
            (if (< (1+ index) (length lists))
                (fail result)
                (return result)))))))

(defun reverse (list)
  (do ((new-list ()))
      ((endp list) new-list)
    (push (pop list) new-list)))


(defun budden-tools-maptree (fun tree) 
   #+russian "Проходит рекурсивно по дереву. К каждому атому дерева применяет функцию fun.
    Получается такое дерево."
   #-russian "Walks tree, applying fun to any atom of it and collects results to the fresh isomorphic tree"
  (etypecase tree
    (null nil)
    (k:cons
     (k:mapcar (lambda (x) (cond
                            ((null x) nil)
                            ((k:consp x) (budden-tools-maptree fun x))
                            ((cl:consp x)
                             (error "Didn't expect cl:cons here"))
                            (t (cl:funcall fun x))))
               tree))))

(defun budden-tools-1-to-list (x) 
  "Makes a list from atom, nil from nil and keeps list intact"
  (cond
   ((cl:consp x)
    (error "Didn't expect cl:cons here"))
   ((k:consp x) x)
   (x (list x))
   (t nil)))


(defun k-list-to-cl-list (k-list &key predel-dliny)
  "Проверка цикличности нужна, в основном, для инспектора"
  (let ((rez nil)
        (tr nil)
        (counter 0))
    (k:dolist (x k-list)
              (budden-tools:collect-into-tail-of-list x rez tr)
              (when (and predel-dliny (> (incf counter) predel-dliny))
                (error "k-список - длиннее, чем ~S. Он циклический?" predel-dliny)))
    rez))

(defun cl-list-to-k-list (cl-list)
  "Без проверки цикличности"
  (let ((rez nil)
        (tr nil))
    (cl:dolist (x cl-list)
      (|BUDDEN-TOOLS-COLLECT-INTO-TAIL-OF-LIST| x rez tr))
    rez))

(defun Скопировать-до-хвоста-не-включительно (Голова Какой-то-хвост)
  "Какой-то хвост - это nil или некий k:cdr от конса Голова. Ф-я копирует часть k-списка от головы до хвоста, не включая хвост. Не защищён от циклических списков. Если Какой-то-хвост не является хвостом головы и не является nil, то порождает ошибку"
  (perga
   (let Рез nil)
   (let Рез-т-р nil)
   (loop
     (cond
      ((eq Голова Какой-то-хвост)
       (return Рез))
      (Голова
       (budden-tools-collect-into-tail-of-list (pop Голова) Рез Рез-т-р))
      (Какой-то-хвост
       (error "Какой-то-хвост должен быть хвостом головы"))
      (t
       (return Рез))))))

#+swank
(defmethod swank/backend:emacs-inspect ((r k:cons))
  (swank/backend::label-value-line*
   ("kons:car" (k:car r))
   ("kons:cdr" (k:cdr r))
   ("Как список"
    (cl:or (cl:ignore-errors (k-list-to-cl-list r :predel-dliny 500))
           "Ошибка при преобразовании в список: циклический, точечный или слишком длинный (> 500) список"))))

