; -*- system :kons; coding: utf-8; -*-

;;; (C) Denis Budyak 2016
;;; @include MIT License 
;;; This code contains portions of SBCL, see also SBCL license

(named-readtables:in-readtable :buddens-readtable-a)

(cl:in-package :kons-impl)

;;; Printing 

(defun k-output-object (o stream)
  (typecase o
    (cons (output-kons o stream))
    (t (cl-impl:output-object o stream))))

(defun output-kons (list stream)
  (let ((original list))
    (sb-kernel::descend-into
     (stream)
     (let ((budden-tools:*escape-symbol-readmacros* nil))                       
       (cl-impl:output-object 'kons:|{| stream))
     (cl:write-char #\space stream)
     (let ((length 0)
           (list list))
       (loop
         (sb-kernel::punt-print-if-too-long length stream)
         (k-output-object (pop list) stream)
         (unless list
           (return))
         (when (or (atom list)
                   (sb-kernel::check-for-circularity list))
           (cl:write-string " . " stream)
           (cl-impl:output-object list stream)
           (return))
         (cl:write-char #\space stream)
         (cl:incf length)))
     (cl:write-char #\space stream)
     (cl-impl:output-object 'kons:|}| stream))
    original))


(cl:defmethod cl:print-object ((o cons) stream)
  (output-kons o stream))

