; -*- system :kons; coding: utf-8; -*-
;;; cons abstraction (SBCL specific) - реализация примитивов для adjoin
;;; (C) Denis Budyak 2016
;;; @include MIT License 
;;; This code contains portions of SBCL, see also SBCL license

(named-readtables:in-readtable :buddens-readtable-a)

(cl:in-package :kons-impl)

(cl:macrolet
    ((def (funs form &optional variant)
       (flet ((%def (name &optional conditional)
                (let* ((body-loop
                        `(do ((list list (cdr list)))
                             ((null list) nil)
                           (declare (list list))
                           (let ((this (car list)))
                             ,(let ((cxx (if (cl:char= #\A (cl:char (string name) 0))
                                             'car    ; assoc, assoc-if, assoc-if-not
                                             'cdr))) ; rassoc, rassoc-if, rassoc-if-not
                                   (ecase name
                                      ((assoc rassoc)
                                       (if funs
                                           `(when this
                                              (let ((target (,cxx this)))
                                                (when ,form
                                                  (return this))))
                                           ;; If there is no TEST/TEST-NOT or
                                           ;; KEY, do the EQ/EQL test first,
                                           ;; before checking for NIL.
                                           `(let ((target (,cxx this)))
                                              (when (and ,form this)
                                                (return this)))))
                                 ((assoc-if assoc-if-not rassoc-if rassoc-if-not)
                                  (cl-impl:aver (equal '(eql x) (cl:subseq form 0 2)))
                                  `(when this
                                     (let ((target (,cxx this)))
                                       (,conditional (funcall ,@(cl:cdr form))
                                                     (return this)))))
                                 (member
                                  `(let ((target this))
                                     (when ,form
                                       (return list))))
                                 ((member-if member-if-not)
                                  (cl-impl:aver (equal '(eql x) (cl:subseq form 0 2)))
                                  `(let ((target this))
                                     (,conditional (funcall ,@(cl:cdr form))
                                                   (return list))))
                                 (adjoin
                                  `(let ((target this))
                                     (when ,form
                                       (return t)))))))))
                       (body (if (eq 'adjoin name)
                                 `(if (let ,(when (cl:member 'key funs)
                                                  `((x (funcall key x))))
                                        ,body-loop)
                                      list
                                      (cons x list))
                                 body-loop)))
                  `(defun ,(intern (format nil "%~A~{-~A~}~@[-~A~]" name funs variant))
                       (x list ,@funs)
                     (declare (optimize speed #+SBCL (sb-c::verify-arg-count 0)))
                     ,@(when funs `((declare (cl:function ,@funs))))
                     ,@(unless (cl:member name '(member assoc adjoin rassoc)) `((declare (cl:function x))))
                     #+SBLC (declare (sb-impl::explicit-check))
                     ,body))))
         `(progn
            ,(%def 'adjoin)
            ,(%def 'assoc)
            ,(%def 'member)
            ,(%def 'rassoc)
            ,@(when (and (not variant) (cl:member funs '(() (key)) :test #'equal))
                    (cl:list (%def 'member-if 'when)
                             (%def 'member-if-not 'unless)
                             (%def 'assoc-if 'when)
                             (%def 'assoc-if-not 'unless)
                             (%def 'rassoc-if 'when)
                             (%def 'rassoc-if-not 'unless)))))))
  (def ()
      (eql x target))
  (def ()
      (eq x target)
    eq)
  (def (key)
      (eql x (funcall key target)))
  (def (key)
      (eq x (funcall key target))
    eq)
  (def (key test)
      (funcall test x (funcall key target)))
  (def (key test-not)
      (not (funcall test-not x (funcall key target))))
  (def (test)
      (funcall test x target))
  (def (test-not)
      (not (funcall test-not x target))))

