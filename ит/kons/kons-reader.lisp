; -*- system :kons; -*-

(def-merge-packages::! :kons-reader
  (:always t)
  (:use :cl)
  (:import-from :kons #:|{| #:|}|)
  (:import-from :budden-tools
                budden-tools:def-symbol-readmacro
                budden-tools:the*)
  (:local-nicknames :sbi :sb-impl #|:sbk :sb-kernel|# :sbrbt :buddens-reader-extensions)
  )

(in-package :kons-reader)
(named-readtables:in-readtable :buddens-readtable-a)


(defun {-readmacro-reader (stream symbol)
  (declare (ignore symbol))
  ;(read-char stream)
  (read-klist stream :none))

(def-symbol-readmacro |{| #'{-readmacro-reader)

(macrolet
    ((with-list-reader ((streamvar delimiter) &body body)
       `(let* ((thelist (kons:list nil))
               (listtail thelist)
               (collectp (if *read-suppress* 0 -1)))
          (loop (let ((firstchar (sb-impl::flush-whitespace ,streamvar)))
                  (when (eq firstchar ,delimiter)
                    (return (kons:cdr thelist)))
                  ,@body))))
     (read-list-item (streamvar)
       `(multiple-value-bind (winp obj)
            (sb-impl::read-maybe-nothing ,streamvar firstchar)
          ;; allow for a character macro return to return nothing
          (when (eq obj '|}|)
            (return (kons:cdr thelist)))
          (unless (zerop (logand winp collectp))
            (setq listtail
                  (kons:cdr (kons:rplacd (cl-impl:truly-the kons:cons listtail) (kons:list obj))))))))

  ;;; The character macro handler for left paren
  (defun read-klist (stream ignore)
    (declare (ignore ignore))
    (with-list-reader (stream #\])
      (when (eq firstchar #\.)
        (let ((nextchar (read-char stream t)))
          (cond ((sb-impl::token-delimiterp nextchar)
                 (cond ((eq listtail thelist)
                        (unless (zerop collectp)
                          (alexandria:simple-reader-error
                           stream "Nothing appears before . in list.")))
                       ((buddens-reader-extensions:whitespace[2]p nextchar)
                        (setq nextchar (sb-impl::flush-whitespace stream))))
                 (kons::rplacd (cl-impl:truly-the kons::cons listtail)
                         (read-after-dot stream nextchar collectp))
                 ;; Check for improper ". ,@" or ". ,." now rather than
                 ;; in the #\` reader. The resulting QUASIQUOTE macro might
                 ;; never be exapanded, but nonetheless could be erroneous.
                 (unless (zerop (logand sb-impl::*backquote-depth* collectp))
                   (let ((lastcdr (kons::cdr (kons::last listtail))))
                     (when (and (sb-impl::comma-p lastcdr) (sb-impl::comma-splicing-p lastcdr))
                       (alexandria:simple-reader-error
                        stream "~S contains a splicing comma after a dot"
                        (kons:cdr thelist)))))
                 (return (kons:cdr thelist)))
                    ;; Put back NEXTCHAR so that we can read it normally.
                (t (unread-char nextchar stream)))))
      ;; Next thing is not an isolated dot.
      (read-list-item stream)))

  ;;; (This is a COMMON-LISP exported symbol.)
  (defun read-delimited-klist (endchar &optional
                                      (input-stream *standard-input*)
                                      recursive-p)
    ;(declare (explicit-check))
    (sb-impl::check-for-recursive-read input-stream recursive-p 'read-delimited-klist)
    (flet ((%read-delimited-list ()
             (with-list-reader (input-stream endchar)
               (read-list-item input-stream))))
      (if recursive-p
          (%read-delimited-list)
          (sb-impl::with-read-buffer () (%read-delimited-list)))))) ; end MACROLET

(defun read-after-object-after-dot (stream lastobj)
    ;; At least one thing appears after the dot.
    ;; Expect closing delimiter
  (do ((char1 (sb-impl::flush-whitespace stream) (sb-impl::flush-whitespace stream)))
      (nil nil)
    (multiple-value-bind (winp1 obj1) (sb-impl::read-maybe-nothing stream char1)
      ;(budden-tools:show-expr (list winp1 obj1))
      (cond
       ((zerop winp1)
        ; do nothing - wait for some other object to read
        )
       ((eq obj1 '|}|)
        (return
         lastobj ;success!
         )
        )
       (t 
        (alexandria:simple-reader-error
         stream "More than one object follows . in list."))))))

  
(defun read-after-dot (stream firstchar collectp)
  ;; FIRSTCHAR is non-whitespace!
  (let ((lastobj ()))
    (do ((char firstchar (sb-impl::flush-whitespace stream)))
        (nil nil)
      ;; See whether there's something there.
      (multiple-value-bind (winp obj) (sb-impl::read-maybe-nothing stream char)
        (when (eq obj '|}|)
          (if (zerop collectp)
              (return-from read-after-dot nil)
              (alexandria:simple-reader-error stream "Nothing appears after . in list.")))
        (unless (zerop winp)
          (setq lastobj obj)
          (return (read-after-object-after-dot stream lastobj))
          )))))
