; -*- system :kons; coding: utf-8; -*-
(named-readtables:in-readtable :buddens-readtable-a)

(cl:in-package :ccl)

(defmethod print-object ((object k:cons) stream)
  ;; copy of (defmethod print-object ((object t) stream), see CCL Copyright
  (let ((level (%current-write-level% stream)))   ; what an abortion.  This should be an ARGUMENT!
    (declare (type fixnum level))
    (flet ((depth (stream v)
             (declare (type fixnum v) (type stream stream))
             (when (%i<= v 0)
               ;; *print-level* truncation
               (stream-write-entire-string stream "#")
               t)))
      (unless (depth stream level)
        (write-a-kons-cons object stream level)))
    nil))


(defun kons-opening-paren-for-printer ()
  (let ((budden-tools:*escape-symbol-readmacros* nil))
    (with-output-to-string (ou) (format ou "~S " 'kons:|{|))))

(defun kons-closing-paren-for-printer ()
  (with-output-to-string (ou) (format ou " ~S" 'kons:|}|)))

(defun write-a-kons-cons (cons stream level)
  ;;; copy of write-a-cons, see CCL Copyright
  (declare (type k:cons cons) (type stream stream) (type fixnum level))
  (let ((print-length (get-*print-frob* '*print-length*))
        (level-1 (%i- level 1))
        (head (kons-impl::cons-car cons))
        (tail (kons-impl::cons-cdr cons))
        (prefix (kons-opening-paren-for-printer))
        (suffix (kons-closing-paren-for-printer)))
    (declare (type fixnum print-length) (type fixnum level-1))
    ;(unless (and *print-abbreviate-quote*
    ;             (write-abbreviate-quote head tail stream level-1))
    (let ()
      (pp-start-block stream prefix)
      (if (= print-length 0)
          (%write-string "..." stream)
          (progn
            (write-internal stream head level-1 nil)
            (write-internal stream tail level-1
                                 (if (k:atom tail) ; FIXME what if kons:cdr is cl:cons? 
                                     print-length
                                     (%i- print-length 1)))))
      (pp-end-block stream suffix))))


(defmethod write-internal-1 ((stream t) object level list-kludge)
  (declare (type fixnum level) (type (or null fixnum) list-kludge))
  ;;>> Anybody passing in list-kludge had better be internal to the lisp printer.
  ;(if list-kludge (error "Internal printer error"))
    (let ((circle *print-circle*)
          (pretty *print-pretty*))
      (cond ((or pretty circle)
             ; what about this level stuff??
             ; most peculiar
             (maybe-initiate-xp-printing
              #'(lambda (s o) (write+ o s)) stream object))
            ((not list-kludge)
             (write-a-frob object stream level list-kludge))
            ((null object))
            (t
             (stream-write-char stream #\space)
             (when (not (or (cl:consp object) (k:consp object)))
               (stream-write-char stream #\.)
               (stream-write-char stream #\space))
             (write-a-frob object stream level list-kludge)))))


;; redefining because tail call is required to print long lists.
(cl-advice:PORTABLY-WITHOUT-PACKAGE-LOCKS
 (defun write-a-frob (object stream level list-kludge)
  (declare (type stream stream) (type fixnum level)
           (type (or null fixnum) list-kludge))
  (cond ((not list-kludge)
         (let ((%current-write-stream% stream)   ;>> SIGH
               (%current-write-level% level))
           (print-object object stream)))
        ((< list-kludge 1)
         ;; *print-length* truncation
         (stream-write-entire-string stream "..."))
        ((not (or (cl:consp object) (k:consp object)))
         (write-a-frob object stream level nil))
        ((k:consp object)
         ;; there was a tail call - but we seem to lose it (does CCL add JMP as SBCL does to make
         ;; several tail calls?)
         (write-internal stream (k:car object) level nil)
         (write-internal-1 stream (k:cdr object) level (if (k:consp (k:cdr object))
                                                          (- list-kludge 1)
                                                          list-kludge)))
        (t
         (write-internal stream (cl:car object) level nil)
         ;;>> must do a tail-call!!
         (write-internal-1 stream (cl:cdr object) level (if (cl:consp (cl:cdr object))
                                                          (- list-kludge 1)
                                                          list-kludge))))))



(eval-when (compile load eval)
 (defmacro pprint-logical-block-for-kons ((stream-symbol list
                                                         &key (prefix "" prefixp)
                                                         (per-line-prefix "" per-line-prefix-p)
                                                         (suffix ""))
                                          &body body)
   (cond ((eq stream-symbol nil) (setq stream-symbol '*standard-output*))
         ((eq stream-symbol T) (setq stream-symbol '*terminal-io*)))
   (when (not (symbolp stream-symbol))
     (warn "STREAM-SYMBOL arg ~S to PPRINT-LOGICAL-BLOCK is not a bindable symbol"
           stream-symbol)
     (setq stream-symbol '*standard-output*))
   (when (and prefixp per-line-prefix-p)
     (warn "prefix ~S and per-line-prefix ~S cannot both be specified ~
            in PPRINT-LOGICAL-BLOCK" prefix per-line-prefix)
     (setq per-line-prefix nil))
   `(let ((*logical-block-p* t))
      (maybe-initiate-xp-printing
       #'(lambda (,stream-symbol)
           (let ((+l ,list)
                 (+p (or (and ,prefixp
                              (require-type ,prefix 'string))
                         (and ,per-line-prefix-p
                              (require-type ,per-line-prefix 'string))))
                 (+s (require-type ,suffix 'string)))
             (pprint-logical-block-for-kons+
              (,stream-symbol +l +p +s ,per-line-prefix-p T nil)
              ,@ body nil)))
       (decode-stream-arg ,stream-symbol))))


  (defmacro pprint-logical-block-for-kons+ ((var args prefix suffix per-line? circle-check? atsign?)
				 &body body)
  "Group some output into a logical block. STREAM-SYMBOL should be either a
   stream, T (for *TERMINAL-IO*), or NIL (for *STANDARD-OUTPUT*). The printer
   control variable *PRINT-LEVEL* is automatically handled."
  (when (and circle-check? atsign?)
    (setq circle-check? 'not-first-p))
  `(let ((*current-level* (1+ *current-level*))
	 (*current-length* -1)
	 ;(*parents* *parents*)
	 ,@(if (and circle-check? atsign?) `((not-first-p (plusp *current-length*)))))
     (unless (check-block-abbreviation-for-kons ,var ,args ,circle-check?)
       (start-block ,var ,prefix ,per-line? ,suffix)
       (when
         (catch 'line-limit-abbreviation-exit
           (block logical-block
             (macrolet ((pprint-pop () `(pprint-pop-for-kons+ ,',args ,',var))
                        (pprint-exit-if-list-exhausted ()
                          `(if (null ,',args) (return-from logical-block nil))))
               ,@ body))
           (end-block ,var ,suffix)
           nil)
         (end-block ,var ,suffix)
         (throw 'line-limit-abbreviation-exit T)))))

 (defmacro pprint-pop-for-kons+ (args xp)
  `(if (pprint-pop-check-for-kons+ ,args ,xp)
       (return-from logical-block nil)
       (kons:pop ,args)))

  (defun check-block-abbreviation-for-kons (xp args circle-check?)
    (cond ((not (kons:listp args)) (write+ args xp) T)
          ((and *print-level* (> *current-level* *print-level*))
           (write-char++ #\# XP) 
           ;(setq *abbreviation-happened* T)
           T)
          ((and *circularity-hash-table* circle-check? (neq args *xp-current-object*)
                (eq (circularity-process xp args nil) :subsequent))
           T)
          (T nil)))

 (defun pprint-pop-check-for-kons+ (args xp)
  (let ((current-length *current-length*))
    (declare (fixnum current-length))
    (setq current-length (setq *current-length* (1+ *current-length*)))
    (cond ((not (kons:listp args))  ;must be first so supersedes length abbrev
	   (write-string++ ". " xp 0 2)
	   (write+ args xp)
	   T)
	  ((and *print-length* ;must supersede circle check
	        (not (< current-length *print-length*)))
	   (write-string++ "..." xp 0 3)
	   ;(setq *abbreviation-happened* T)
	   T)
	  ((and *circularity-hash-table* (not *format-top-level*)
                (not (zerop current-length)))
           (let ((circle (circularity-process xp args T)))
	     (case circle
	       (:first ;; note must inhibit rechecking of circularity for args.
                (write+ args xp T circle)
                T)
	       (:subsequent T)
	       (T nil)))))))

  ) ; eval-when 
  



(defun pprint-fill-kons (s list &optional (colon? T) atsign?)
  "Output LIST to STREAM putting :FILL conditional newlines between each
   element. If COLON? is NIL (defaults to T), then no parens are printed
   around the output. ATSIGN? is ignored (but allowed so that PPRINT-FILL
   can be used with the ~/.../ format directive."
  (declare (ignore atsign?))
  (pprint-logical-block-for-kons (s list :prefix (if colon? (kons-opening-paren-for-printer) "")
			        :suffix (if colon? (kons-closing-paren-for-printer) ""))
    (pprint-exit-if-list-exhausted)
    (loop (write+ (pprint-pop) s)
	  (pprint-exit-if-list-exhausted)
	  (write-char++ #\space s)
	  (pprint-newline+ :fill s))))

(set-pprint-dispatch+ 'kons:cons #'pprint-fill-kons '(-10) *IPD*)

;; Redefining. Original is in ../../ccl/lib/pprint.lisp
(cl-advice:portably-without-package-locks 
 (defun write-not-pretty (stream object level list-kludge circle)
  (declare (type fixnum level) (type (or null fixnum) list-kludge))
  (when (xp-structure-p stream)(setq stream (xp-stream stream)))  
  (cond ((eq circle :subsequent)
         (if  list-kludge (stream-write-char stream #\)))
         (return-from write-not-pretty nil))
        ((not list-kludge))
        ((null object)(return-from write-not-pretty nil))
        ((and (not (or (cl:consp object) (kons:consp object))) (not circle))
         (stream-write-entire-string stream " . "))
        ((eq circle :first)
         (when (cl:consp object) (stream-write-char stream #\())
         (when (kons:consp object)
           (stream-write-string stream (kons-opening-paren-for-printer))
           (stream-write-char stream " "))
         (write-a-frob object stream level list-kludge)
         (when (cl:consp object) (stream-write-char stream #\)))
         (when (kons:consp object)
           (stream-write-string stream (kons-closing-paren-for-printer))
           (stream-write-char stream " "))
         (return-from write-not-pretty nil))                     
        (t (stream-write-char stream #\space)))
  (write-a-frob object stream level list-kludge)))
