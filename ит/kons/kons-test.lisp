; -*- system :kons; coding: utf-8; -*-

(def-merge-packages::! :kons-test
  (:always t)
  (:use :cl :kons-reader)
  (:shadowing-import-from :kons
                          #:{ #:} )
  ; :kons не импортируем, т.к. не хотим предупреждения
  )

(named-readtables:in-readtable :buddens-readtable-a)

(in-package :kons-test)

(def-trivial-test::! test-not-a-cl-cons
  (and (typep :budden-tools 'k:not-a-cl-cons)
       (typep nil 'k:not-a-cl-cons)
       (not (typep '(a b) 'k:not-a-cl-cons)))
  t)

(def-trivial-test::! setf-cdr
  (let ((a (k:cons 1 2)))
    (setf (k:cdr a) 3))
  3)

(defun make-abc () (k:cons 'a (k:cons 'b (k:cons 'c nil))))

(def-trivial-test::! setf-cadr
  (let ((abc (make-abc)))
    (setf (k:cadr abc) 'd)
    abc)
  (k:list 'a 'd 'c)
  :test 'k:equal)

(def-trivial-test::! setf-nth
  (let ((abc (make-abc)))
    (setf (k:nth 0 abc) 'd)
    (setf (k:nth 1 abc) 'e)
    (setf (k:nth 2 abc) 'f)
    abc)
  (k:list 'd 'e 'f)
  :test 'k:equal)

(def-trivial-test::! test-rplaca
  (let ((a (k:cons 1 2)))
    (k:rplaca a 3))
  (k:cons 3 2)
  :test 'k:equal)


(def-trivial-test::! kons-reader
  (cl:read-from-string "kons:{ 1 2 3 #+nil 5 4 kons:}")
  (kons:list 1 2 3 4)
  :test 'k:equal)

; FIXME-CCL исправить этот тест. 
#-CCL 
(def-trivial-test::! dot-and-garbage
  (cl:read-from-string "kons:{ 1 2 3 . 4 #+nil 5 kons:}")
  (kons:list* 1 2 3 4)
  :test 'k:equal)

(defparameter *test-cons* (make-abc))

(defparameter *test-hash* (cl:make-hash-table))

(cl:define-symbol-macro hash-entry (cl:gethash 'a *test-hash*))

(assert (k:equal (k:list 'a 'b 'c) (make-abc)))

(assert (k:equal (k:last (k:list 'a 'b 'c) 3) (make-abc)))
(assert (k:equal (k:last (make-abc)) (k:cons 'c nil)))
(assert (eq (k:pop *test-cons*) 'a))
(assert (k:equal (k:push 'a *test-cons*) (make-abc)))
(assert (k:equal *test-cons* (make-abc)))

(setf hash-entry nil)
(k:push 'c hash-entry)
(k:push 'b hash-entry)
(k:push 'a hash-entry)
(assert (k:equal hash-entry (make-abc)))
(assert (eq (k:pop hash-entry) 'a))
(assert (eq (k:pop hash-entry) 'b))
(assert (eq (k:pop hash-entry) 'c))
(assert (null hash-entry))

(assert (k:equal (k:mapcar 'identity (make-abc)) (make-abc)))

(assert (k:equal (k:cdddr (k:list 'a 'b 'c 'd)) (k:list 'd)))
(assert (k:equal (k:third (k:list 'a 'b 'c 'd)) 'c))


(def-trivial-test::! print-normal-cons
                     "{ 1 . 2 }"
  (WITH-STANDARD-IO-SYNTAX
   (let ((*package* (find-package :kons)))
     (cl:prin1-to-string
      (k:cons 1 2)))))

(def-trivial-test::! print-normal-list
                     "{ 1 2 3 }"
  (cl:prin1-to-string
   (k:list 1 2 3)))

(def-trivial-test::! print-nested-list
                     "{ { a b c } }"
  (cl:prin1-to-string
   (k:list (make-abc))))


(def-trivial-test::! reader-list-is-self-evaluating
                     (eval (cl:read-from-string "{ 1 2 3 }"))
  (read-from-string "{ 1 2 3 }")
  :test 'k:equal)


(defun make-circular-cons-2 ()
  (let ((a (k:cons 1 2)))
    (setf (k:cdr a) a)
    a))

(defun make-circular-list-3 ()
  (let ((a (k:list 1 2 3)))
    (setf (k:cdr (k:last a)) a)
    a))

(def-trivial-test::! print-circular-cons-2
                     (let ((cl::*print-circle* t)
                           (cl::*PRINT-PRETTY* nil))
                       (cl:prin1-to-string (make-circular-cons-2)))
  "#1={ 1 . #1# }")

(def-trivial-test::! print-circular-list-3
                     "#1={ 1 2 3 . #1# }"
  (let ((cl::*print-circle* t)
        (cl::*print-pretty* nil)) ; FIXME-CCL - it does not work in Windows when building yar image
    (cl:prin1-to-string (make-circular-list-3))))

(def-trivial-test::! test-list-length
  (k::list-length { 1 2 3 })
  3)

(def-trivial-test::! test-length
  (k:length { 1 2 3 })
  3)

(def-trivial-test::! test-list-length-circular
  (k::list-length (make-circular-list-3))
  nil)

(def-trivial-test::! dolist-1-list-of-constants
  (let (result) 
    (k:dolist (i (k:list 1 2 3))
              (push i result))
    result
    )
  '(3 2 1)
  )

(def-trivial-test::! dolist-1-list-non-constant
  (let* ((l (k:list 1 2 3))
         (result nil))
    (k:dolist (i l)
              (push i result))
    result
    )
  '(3 2 1)
  )

(def-trivial-test::! dolist-1-constant
  (let (result) 
    (k:dolist (i { 1 2 3 })
              (k:push i result))
    result
    )
  (k:list 3 2 1)
  :test 'k:equal
  )

(def-trivial-test::! test-make-list
                     (k:make-list 3 :initial-element 'a)
  (k:list 'a 'a 'a)
  :test 'k:equal)


(def-trivial-test::! test-every
    (k:every 'cl:zerop { 0 0 })
    t)

(def-trivial-test::! test-subseq-1
                     (let ((data '(0 1 2 3))
                           (rez nil) (tr nil))
                       (dotimes (i 3)
                         (iterk:iter (:for j from i to 3)
                                     (budden-tools:collect-into-tail-of-list 
                                      (subseq data i j) rez tr))
                         (budden-tools:collect-into-tail-of-list
                          (subseq data i) rez tr))
                       rez)
  (let ((data k:{ 0 1 2 3 k:})
        (rez nil) (tr nil))
    (dotimes (i 3)
      (iterk:iter (:for j from i to 3)
                  (budden-tools:collect-into-tail-of-list 
                   (k:k-list-to-cl-list (k:subseq data i j)) rez tr))
      (budden-tools:collect-into-tail-of-list
       (k:k-list-to-cl-list (k:subseq data i)) rez tr))
    rez)
  :test 'k:equal)

;; Тесты закомментарены в связи с sequences
#|
 (def-trivial-test::! test-map-1
  (cl:map 'k:cons 'identity #(a b c) )
  (make-abc)
  :test 'k:equal)

 (def-trivial-test::! test-map-2
  (cl:map 'cons 'identity { a b c } )
  '(a b c)
  )

  (def-trivial-test::! test-coerce-1
    (coerce (k:list 1 2 3) 'cl:list)
  '(1 2 3))

 (def-trivial-test::! test-coerce-2
    (coerce (cl:list #\a #\b #\c) 'string)
  "abc")
|#

; Это не работает. Делать как в test-map-1 
;(def-trivial-test::! test-coerce-3
;    (coerce (cl:list #\a #\b #\c) 'k:list)
;  )

#|(def-trivial-test::! test-iterate-k-collecting
  ; аналог :collect
  (let*
      ((num-of-iterations 0)
       (result
        (iterate-keywords:iter
         (:for i :in-sequence (make-abc))
         (incf num-of-iterations)
         (:k-collect i))))
    (and (= num-of-iterations 3)
         (k:equal result (make-abc))))
  t)

(def-trivial-test::! test-iterate-k-appending
  (let*
      ((num-of-iterations 0)
       (result
        (iterate-keywords:iter
         (:for i :in-sequence (make-abc))
         (incf num-of-iterations)
         (:k-append (k:list i)))))
    (and (= num-of-iterations 3)
         (k:equal result (make-abc))))
  t)
|#
(def-trivial-test::! test-copy-list
  (make-abc)
  (k:copy-list (make-abc))
  :test 'k:equal)

(def-trivial-test::! test-nconc
  (k:nconc (make-abc) (make-abc))
  (k:list 'a 'b 'c 'a 'b 'c)
  :test 'k:equal)

(def-trivial-test::! test-nconc-2
  (let* ((a (k:list 'a))
         (result (k:nconc a (k:list 'b) (k:list 'c))))
    (and (eq a result)
         (k:equal result (make-abc))
         ))
  t)

(def-trivial-test::! test-append
  (k:append (make-abc) (make-abc))
  (k:list 'a 'b 'c 'a 'b 'c)
  :test 'k:equal
  )

(def-trivial-test::! test-revappend
  (k:revappend (make-abc) (make-abc))
  (k:list 'c 'b 'a 'a 'b 'c)
  :test 'k:equal
  )

(def-trivial-test::! test-adjoin
  (k:adjoin 'a (k:adjoin 'd (make-abc)))
  (k:list 'd 'a 'b 'c)
  :test 'k:equal)

(def-trivial-test::! test-collect-duplicates
  (k:budden-tools-collect-duplicates (k:list '1 1 2 3 3 3) :test '=)
  (k:list 1 3)
  :test 'k:equal)

(def-trivial-test::! test-budden-tools-maptree-trivial
  (k:budden-tools-maptree 'identity nil)
  nil)

(def-trivial-test::! test-budden-tools-1-to-list
  (k:list
   (k:budden-tools-1-to-list 123)
   (k:budden-tools-1-to-list (k:list 123)))
  (k:list
   (k:list 123)
   (k:list 123))
  :test 'k:equal)


(def-trivial-test::! test-perga-and-dolist
  (perga-implementation:perga
   (let i 1)
   (k:dolist (j (make-abc))
             (declare (ignore j))
             (incf i))
   i)
  4)

(def-trivial-test::! test-Скопировать-до-хвоста-не-включительно.1
  { { 0 } { 0 1 } { 0 1 2 3 } nil }
  (perga-implementation:perga
   (let Д (k:list 0 1 2 3))
   (k:list
    (k:Скопировать-до-хвоста-не-включительно Д (k:cdr Д))
    (k:Скопировать-до-хвоста-не-включительно Д (k:cddr Д))
    (k:Скопировать-до-хвоста-не-включительно Д nil)
    (k:Скопировать-до-хвоста-не-включительно nil nil)))
  :test 'k:equal)
     
(def-trivial-test::! test-Скопировать-до-хвоста-не-включительно.2.ошибка-если-не-хвост
  t
  (typep (nth-value 1 (ignore-errors
                       (k:Скопировать-до-хвоста-не-включительно { 0 1 } { 1 })))
         'error))

(def-trivial-test::! test-Разобрать-на-непрерывные-группы-с-одинаковым-значением-ключа.1
  {
    { { 0 } { 1 1 } { 0 } } 
    { { 0 } { 1 1 } { 0 } } 
  }
  (perga-implementation:perga
   (let Д (k:list 0 1 1 0))
   (k:list
    (k:Разобрать-на-непрерывные-группы-с-одинаковым-значением-ключа Д)
    (k:Разобрать-на-непрерывные-группы-с-одинаковым-значением-ключа Д :Равенство #'=)))
  :test 'k:equal)


(def-trivial-test::! test-Разобрать-на-непрерывные-группы-с-одинаковым-значением-ключа.2
  {
    { } 
    { { 0 } } 
    { { 0 0 } } 
    { { 0 } { 1 } } 
    { { 0 } { 1 1 } } 
    { { 0 0 } { 1 } } 
    { { 0 0 } { 1 1 } } 
  }
  (k:mapcar
   'k:Разобрать-на-непрерывные-группы-с-одинаковым-значением-ключа 
  {
    { } 
    { 0 } 
    { 0 0 } 
    { 0 1 } 
    { 0 1 1 } 
    { 0 0 1 } 
    { 0 0 1 1 } 
  })
  :test 'k:equal)



(def-trivial-test::! test-Разобрать-на-непрерывные-группы-с-одинаковым-значением-ключа.3
  {
    { { #(3) } { #(4) } { #(5) } { #(5) } }
    { { #(3) } { #(4) } { #(5) #(5) } }
  }
  (perga-implementation:perga
   (let Д { #(3) #(4) #(5) #(5) })
   (k:list
    (k:Разобрать-на-непрерывные-группы-с-одинаковым-значением-ключа Д)
    (k:Разобрать-на-непрерывные-группы-с-одинаковым-значением-ключа Д
                                                                    :Ключ (lambda (x) (elt x 0)))))
  :test 'k:limited-equalp)
