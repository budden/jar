;; -*- encoding: utf-8; coding: utf-8; -*- 
;; Copyright (C) Денис Будяк 2015

(named-readtables:in-readtable :buddens-readtable-a)

(defsystem :ЯР.НАНО-ПАРСЕР
  :depends-on (:anaphora 
               :budden-tools
               :buddens-reader
               :kons
               :budden
               :ЯР.ПОЛНОСТЬЮ-КЕШИРОВАННЫЙ-ПОТОК
               :БУКВЫ-И-МЕСТА-В-ФАЙЛЕ
               :ОТКРЫТЬ-ИСХОДНЫЙ-ТЕКСТ-В-РЕДАКТОРЕ
               ;; по идее эта зависимость нужна только для обработки ошибок
               :ЛЕКСЕМЫ-ЯРА
               ; :ВЕРСИИ
               )
  :description "Библиотека для разбора потоков методом рекурсивного спуска"
  :license "MIT"
  :serial t
  :components ((:file "пакет")
               (:file "ма-пе-ти" :description "макросы,переменные,типы")
               (:file "место-ошибки" :description "полиморфные ф-ии, делающие что-то для разных подтипов типа место-ошибки")
               (:file "нано-парсер")))


