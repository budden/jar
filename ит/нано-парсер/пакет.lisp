;;; -*- Mode:Lisp; system :ЯР.НАНО-ПАРСЕР -*-
;; Copyright (C) Денис Будяк 2017

(named-readtables:in-readtable :buddens-readtable-a)

(def-merge-packages::! :ЯР.НАНО-ПАРСЕР
  (:nicknames "яр.нано-парсер")
  (:always t)
  (:use :cl 
        :russian-budden-tools 
        :budden-tools 
        :anaphora
        ; :lexem-pos ;; ПРАВЬМЯ выкинуть
        ; :yar-lexem ;; ПРАВЬМЯ выкинуть
        :ПОЛНОСТЬЮ-КЕШИРОВАННЫЙ-ПОТОК
        :ОТКРЫТЬ-ИСХОДНЫЙ-ТЕКСТ-В-РЕДАКТОРЕ
        )
  (:shadow "Маркер-КФ")
  (:import-from
   :perga-implementation perga-implementation:perga)
  (:export
   "
    ЯР.НАНО-ПАРСЕР:С-долженствованием ; with-must
    ЯР.НАНО-ПАРСЕР:Долженствует ; must

    ЯР.НАНО-ПАРСЕР:Чит-э ; Прочитать-следующий-элемент
    ЯР.НАНО-ПАРСЕР:Предвидеть-э ; peek
    ЯР.НАНО-ПАРСЕР:Предвиденный-элемент-или-ошибка

    ЯР.НАНО-ПАРСЕР:л-Класс ; класс следующей лексемы
    ЯР.НАНО-ПАРСЕР:Чит-ежли-к ; popIfClass

    ЯР.НАНО-ПАРСЕР:Функция-ошибки-по-умолчанию
    ЯР.НАНО-ПАРСЕР:Ошибка-разбора-в ; parsing-error-at
    ЯР.НАНО-ПАРСЕР:Ошибка-разбора-многоместная ; parsing-error-at
    ЯР.НАНО-ПАРСЕР:Ошибка-разбора ; parsing-error
    ЯР.НАНО-ПАРСЕР:Ошибка-р ; не требует лексемы
    ЯР.НАНО-ПАРСЕР:Неожиданный-Кнф ; то же, что и ошибка, спец. для Кнф

    ЯР.НАНО-ПАРСЕР:Вид-разбора
    ЯР.НАНО-ПАРСЕР:вира-До-первой-ошибки
    ЯР.НАНО-ПАРСЕР:вира-Раскраска

    ЯР.НАНО-ПАРСЕР:сп-Контекст-разбора ; переменная
    
    ;;Контекст-разбора
    ЯР.НАНО-ПАРСЕР:Контекст-разбора
    ЯР.НАНО-ПАРСЕР:MAKE-Контекст-разбора
    ЯР.НАНО-ПАРСЕР:Контекст-разбора-P
    ЯР.НАНО-ПАРСЕР:COPY-Контекст-разбора
    
    ЯР.НАНО-ПАРСЕР:Контекст-разбора-Функция-получения-класса
    ЯР.НАНО-ПАРСЕР:Контекст-разбора-Значение-КФ
    ЯР.НАНО-ПАРСЕР:Контекст-разбора-Данные
    ЯР.НАНО-ПАРСЕР:Контекст-разбора-Последний-считанный-элемент
    ЯР.НАНО-ПАРСЕР:Контекст-разбора-Вид-разбора
    ЯР.НАНО-ПАРСЕР:Контекст-разбора-Режим-допечатывания-ли
    ЯР.НАНО-ПАРСЕР:Контекст-разбора-Функция-получения-позиции
    ЯР.НАНО-ПАРСЕР:Контекст-разбора-Предвиденный-элт
    ЯР.НАНО-ПАРСЕР:Контекст-разбора-Источник
    ЯР.НАНО-ПАРСЕР:Контекст-разбора-Функция-ошибки
    ЯР.НАНО-ПАРСЕР:Контекст-разбора-Ошибка-активна
    ЯР.НАНО-ПАРСЕР:Контекст-разбора-Функция-копирования-состояния-для-закладки
    ЯР.НАНО-ПАРСЕР:Контекст-разбора-Функция-восстановления-закладки
    ЯР.НАНО-ПАРСЕР:Контекст-разбора-Функция-забвения-закладки

    ;; Эти ф-ии должны вызываться пользователем из полей
    ;; Контекст-разбора-Функция-восстановления-закладки
    ;; и Контекст-разбора-Функция-забвения-закладки
    ЯР.НАНО-ПАРСЕР:Вернуться-к-закладке-нано-парсер
    ЯР.НАНО-ПАРСЕР:Забыть-закладку-нано-парсер

    
    ЯР.НАНО-ПАРСЕР:Маркер-КФ
    ЯР.НАНО-ПАРСЕР:Не-определено

    ЯР.НАНО-ПАРСЕР:Заложить-закладку
    ЯР.НАНО-ПАРСЕР:Вернуться-к-закладке
    ЯР.НАНО-ПАРСЕР:Забыть-закладку

    ЯР.НАНО-ПАРСЕР:Сбросить-контекст-разбора-после-ошибки
    ЯР.НАНО-ПАРСЕР:Место-ошибки
    ЯР.НАНО-ПАРСЕР:Место-ошибки-Источник
    ЯР.НАНО-ПАРСЕР:Место-ошибки-Начальная-строка
    ЯР.НАНО-ПАРСЕР:Место-ошибки-Начальная-колонка

    ЯР.НАНО-ПАРСЕР::Есть-предвиденный-элемент-ли
    "
   )
  (:local-nicknames :bu :budden-tools 
                    :al :alexandria.0.dev
                    )
  (:custom-token-parsers (:packages :budden))
  )


