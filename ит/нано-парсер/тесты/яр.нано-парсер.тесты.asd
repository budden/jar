;; -*- encoding: utf-8; coding: utf-8; -*- 
;; Copyright (C) Денис Будяк 2015

(named-readtables:in-readtable :buddens-readtable-a)

(defsystem :ЯР.НАНО-ПАРСЕР.ТЕСТЫ
  :depends-on (:ЯР.НАНО-ПАРСЕР)
  :serial t
  :components ((:file "нано-парсер-тесты-пакет")
               (:file "нано-парсер-тесты-функции")
               (:file "нано-парсер-тесты")
               ))
