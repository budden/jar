;;; -*- Mode:Lisp; system :ЯР.НАНО-ПАРСЕР.ТЕСТЫ -*-
;; Copyright (C) Денис Будяк 2017
;; Приходится выносить ф-ии в отдельный файл, т.к. SBCL
;; не показывает исходник, если ошибка происходит во время 
;; загрузки ф-ии

(named-readtables:in-readtable :buddens-readtable-a)

(def-merge-packages::! :ЯР.НАНО-ПАРСЕР.ТЕСТЫ
  (:always t)
  (:use :cl :ЯР.НАНО-ПАРСЕР :budden-tools)
  (:local-nicknames :ПКП :ПОЛНОСТЬЮ-КЕШИРОВАННЫЙ-ПОТОК)
  )

(in-package :ЯР.НАНО-ПАРСЕР.ТЕСТЫ)

(defparameter сп-Имя-тестового-файла
  (merge-pathnames #.(defpackage-budden:this-source-file-directory)
                   "/тестовый-файл.текст"))

