;;; -*- Mode:Lisp; system :П-Я-ГРАМОТЕЙ; coding: utf-8; -*-
(named-readtables:in-readtable :buddens-readtable-a)


(in-package :П-Я-ЛЕКСЕР)


(defun Прочитать-свернуть-и-сгруппировать (Источник &key Режим-раскраски-ли)
  "Источник - согласно П-Я-ЛЕКСЕР::Вызвать-функцию-над-пкп-созданным-из-указателя-потока-или-генератора-Пб
   Возвращает список групп верхнего уровня. В режиме раскраски вроде бы никогда не используется.
   См. также РАСКРАСКА-ЯРА-ЛЕКСЕРОМ::Функция-раскраски-Яра-для-редактора , П-Я-ЛЕКСЕР:Парсер-Яра-с-вызовом-событий. 
  Режим раскраски может включаться для тестов
    "
  (perga
   (let Свёрнуто (Прочитать-и-свернуть Источник))
   (let Указатель Свёрнуто)
   (flet Генератор () (k:pop Указатель))
   (П-Я-ГРУППИРОВЩИК::Группировщик-лексем-в-список #'Генератор Режим-раскраски-ли)))

