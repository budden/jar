;; -*- encoding: utf-8; coding: utf-8; -*- 
;; Copyright (C) Денис Будяк 2015 - 2017

(named-readtables:in-readtable :buddens-readtable-a)


(defsystem :П-Я-ФУНДАМЕНТ
  :depends-on (:ЛЕКСЕМЫ-ЯРА :БУКВЫ-И-МЕСТА-В-ФАЙЛЕ :ЯР.НАНО-ПАРСЕР :ЯР.ПОЛНОСТЬЮ-КЕШИРОВАННЫЙ-ПОТОК)
  :serial t
  :components ((:file "п-я-лексер-пакет")
               (:file "предикаты-классификации-букв" :description "Например, может ли с буквы начинаться идентификатор")
               (:file "примыкаемость" :description "оя::примыкаемость")
               (:file "п-я-фундамент" :description "Функции для продвижения парсера")))
