;;; -*- Mode:Lisp; system :ЛЕКСЕМЫ-ЯРА; coding: utf-8; -*-
;; Copyright (C) Денис Будяк 2017

(named-readtables:in-readtable :buddens-readtable-a)

(def-merge-packages::! :КЛАССЫ-ЛЕКСЕМ-ЯРА
  (:always t)
  (:use)
  (:export
   ;; При именовании символов заботимся об их сыскуемости.
   ;; Строки нужно разделять пробелами, чтобы их можно было скопировать отсюда и вставить в ../../doc/описание-языка/синтаксис-бд.lisp

   ;; оя::циклические-графы
   "Кля-решётка-м"  ; метка - пока что это не символ
   "Кля-решётка-п" ; повтор - пока что это не символ

   ;; оя::скобки-преодоления-приоритета
   "Кля-открывающая-преодоления-приоритета" ; (:
   "Кля-закрывающая-преодоления-приоритета" ; :)

   ;; оя::блок-дпа
   "Кля-открывающая-блок-дпа" ; [!
   "Кля-закрывающая-блок-дпа" ; !]
   "Кля-блок-дпа" ; уже свёрнутый блок ДПА

   ;; скобки для пользователя, см. оя::виды-скобок
   "Кля-открывающая-квадратная-и-процент" ; [%
   "Кля-закрывающая-квадратная-и-процент" ; %]

   ;; обычные скобки, см. оя::виды-скобок
   "Кля-открывающая-круглая" ; (
   "Кля-закрывающая-круглая" ; )
   "Кля-открывающая-квадратная"
   "Кля-закрывающая-квадратная"
   
   "Кля-запятая" ; запятая (разделитель элементов списка)

   "Кля-кн." ; конец строке

   ;; вне таблицы приоритетов
   "Кля-кн*" ; конец всему остальному
   "Кля-отступ" ; отступ в начале строки
   "Кля-сырое-белое-поле" ; пробелы ПРАВЬМЯ - переименовать, чтобы не было иллюзии, что сюда может входить Кн. или табуляция.
   ;;
   "Кля-короткий-комментарий" ; оя::комментарии
   "Кля-совсем-короткий-комментарий" ; /*  */

   "Кля-символ-ключ" ; оя::операция-ключ . Сам символ содержится в Лексема-Сод .
   "Кля-сегмент-квалификатора-библиотеки"
   "Кля-сегмент-квалификатора-пр-ва-имён"

   ;; простые символы оборачиваются пост-лексером в символы, кроме ЛЕКСЕМЫ-ЯРА::*Коя-ключевых-слов*
   ;; но не все слова с синтаксисом идентификатора являеются символами, см. оя::виды-слов-в-исходном-тексте
   "Кля-простой-символ" ; часть имени идентификатора, например в а;б;в:г - г - это простой символ
   "Кля-символ" ; собственно символ, может включать квалификаторы, например, а.б;в:г

   "Кля-отделитель-библиотеки" ; одиночная точка с запятой
   "Кля-отделитель-лексикона" ; двоеточие. Также служит как знак ключа, оя::операция-ключ . ПРАВЬМЯ переименовать

   "Кля-строковый-литерал" ; " ", """ """, (% %)
   "Кля-целый-литерал"
   "Кля-плавающий-литерал"

   ;; Группы
   "Кля-список-через-запятую"  ; в круглых скобках
   "Кля-элемент-списка-через-запятую" ; кусок списка Кля-список-через-запятую

   "Кля-список-выражений-в-квадратных-скобках" ; выражения в квадратных скобках группируются, но не разделяются на подсписки. 

   ;; Определение - не путать со словом определение - оно пока у нас символ
   "Кля-определение" 
   "Кля-определения-заголовок"
   "Кля-определения-тело"
   "Кля-определения-класса-поля"
   
   ;; Если 
   "Кля-оператор-если"
   "Кля-если-часть-1"
   "Кля-часть-предложения-иначесли"
   "Кля-часть-предложения-иначе"

   "Кля-оператор-цикла" ; оператор цикла в целом
   "Кля-оператора-цикла-заголовок" ; до слова тело
   "Кля-оператора-цикла-тело" ; от слова тело
   ;"Кля-оператора-цикла-след" ; от слова след, если есть - пока не реализуем
   "Кля-оператор-блок" ; оя::оператор-блок
   
   "Кля-выражение-в-скобках-преодоления-приоритета"
   "Кля-оператор-пусть" ;; см. также П-Я-ГРАМОТЕЙ:П-Опрт-Пусть
   "Кля-оператор-зап"   ;; см. также П-Я-ГРАМОТЕЙ:П-Опрт-Зап
   "Кля-опрт-опрт-выраж"

   "Кля-КНФ"  ; лексема "конец файла". 

   ;; Если ЯР.НАНО-ПАРСЕР:Контекст-разбора-Режим-допечатывания-ли, то вместо КНФ появляется
   ;; Кля-место-допечатывания
   "Кля-место-допечатывания" 

   ;; Может появляться, если ЯР.НАНО-ПАРСЕР:Контекст-разбора-Вид-разбора == ЯР.НАНО-ПАРСЕР:вира-Раскраска и служит для изображения места ошибки.
   "Кля-ошибка-разбора" 
   ))

(def-merge-packages::! :КЛАССЫ-ОПЕРАЦИЙ-ЯРА
  (:always t)
  (:documentation "")
  (:use)
  (:export
  ;; оя::таблица-операций . Нужно помнить, что для лексера каждая операция - это 
  ;; символ (см оя::операции-в-других-лексиконах), а классы лексем
  ;; нужны нам лишь потому, что они нарушают
  ;; обычные правила именования символов и имеют приоритет, нужный 
  ;; для грамм.разбора. 
   "Коя-."
   "Коя-знак-градуса" ; °
   "Коя-^"
   "Коя-умножить" ; ×
   "Коя-делить" ; ÷
   "Коя-бинарный--"
   "Коя-бинарный-+"
   "Коя-☼"
   "Коя-&"
   "Коя-♥"
   "Коя-♥♥"
   "Коя-<="
   "Коя-<"
   "Коя->"
   "Коя->="
   "Коя-<>"
   "Коя-=="
   "Коя-не"
   "Коя-и"
   "Коя-или"
   "Коя-?"

  ;; А также символы, не входящие в выражение, но характерные тем, что они содержат спец.значки
   "Коя-=" ; присваивание
   "Коя---" ; отделяет тип от выражения

   ;; А также символы, имющие специальное значение. оя::ЦИТАТЫ-И-ШАБЛОНЫ-КОДА
   ;; они отличаются от операций ф-ей ЛЕКСЕМЫ-ЯРА:Это-Коя-ключевого-слова-ли
   ;; ПРАВЬМЯ - ключевые слова не должны быть подмножеством операций
   "Коя-цит" ; цитата
   "Коя-квц" ; квазицитата
   "Коя-квп" ; подстановка в квазицитату

   "Коя-ключи"
   "Коя-псимв"

   ;; оя::раздеть
   "Коя-раздеть"
   ;; dotted list
   "Коя-прихвостень" 
   ))

(def-merge-packages::! :ФУНКЦИИ-ПАРСЕРА-ПРЭТТА
  (:always t)
  (:use)
  (:documentation "Имена функций парсера - мы определим их в системе :П-Я-ГРАМОТЕЙ, однако их имена нужны нам уже в лексемах для заполнения метаданых операций, поэтому есть такой пакет"
                  )
  (:export
   "ФУНКЦИИ-ПАРСЕРА-ПРЭТТА::Nud-Кля-целый-литерал
    ФУНКЦИИ-ПАРСЕРА-ПРЭТТА::Nud-Кля-плавающий-литерал
    ; ФУНКЦИИ-ПАРСЕРА-ПРЭТТА::Led-Коя-бинарный--
    ФУНКЦИИ-ПАРСЕРА-ПРЭТТА::Led-Коя-бинарная-операция
    ФУНКЦИИ-ПАРСЕРА-ПРЭТТА::Led-Коя-=
    ФУНКЦИИ-ПАРСЕРА-ПРЭТТА::Led-Коя-&
    ФУНКЦИИ-ПАРСЕРА-ПРЭТТА::Led-Унарная-постфиксная
    ФУНКЦИИ-ПАРСЕРА-ПРЭТТА::Led-Кля-список-через-запятую
    ФУНКЦИИ-ПАРСЕРА-ПРЭТТА::Nud-Символ-именующий-какую-то-сущность-в-программе
    ФУНКЦИИ-ПАРСЕРА-ПРЭТТА::Nud-Кля-выражение-в-скобках-преодоления-приоритета
    ФУНКЦИИ-ПАРСЕРА-ПРЭТТА::Led-Кля-список-выражений-в-квадратных-скобках
    ФУНКЦИИ-ПАРСЕРА-ПРЭТТА::Nud-Кля-строковый-литерал
    ФУНКЦИИ-ПАРСЕРА-ПРЭТТА::Nud-Унарная-префиксная
    "))

(def-merge-packages::! :ЛЕКСЕМЫ-ЯРА
                       (:always t)
  (:use :cl :budden-tools :БУКВЫ-И-МЕСТА-В-ФАЙЛЕ :КЛАССЫ-ЛЕКСЕМ-ЯРА :КЛАССЫ-ОПЕРАЦИЙ-ЯРА :ФУНКЦИИ-ПАРСЕРА-ПРЭТТА)
  (:export
   "
    ;;Буква-заготовки-строкового-литерала
    ЛЕКСЕМЫ-ЯРА:Буква-заготовки-строкового-литерала
    ЛЕКСЕМЫ-ЯРА:MAKE-Буква-заготовки-строкового-литерала
    ЛЕКСЕМЫ-ЯРА:Буква-заготовки-строкового-литерала-P
    ЛЕКСЕМЫ-ЯРА:COPY-Буква-заготовки-строкового-литерала
    
    ЛЕКСЕМЫ-ЯРА:Буква-заготовки-строкового-литерала-Прочитано
    ЛЕКСЕМЫ-ЯРА:Буква-заготовки-строкового-литерала-Получилось

    ;; Статус лексемы
    ЛЕКСЕМЫ-ЯРА:Статус-лексемы
    ЛЕКСЕМЫ-ЯРА:Собирается
    ЛЕКСЕМЫ-ЯРА:стале-Символ-готов-для-поиска-в-лексиконе
    ЛЕКСЕМЫ-ЯРА:стале-Недопечатанная

    ;; Этап-разбора
    ЛЕКСЕМЫ-ЯРА:Этап-разбора
    ЛЕКСЕМЫ-ЯРА:Сворачивание
    ЛЕКСЕМЫ-ЯРА:Группировка

    ;; Способ-вхождения-лексемы-в-лексему
    ЛЕКСЕМЫ-ЯРА:Способ-вхождения-лексемы-в-лексему
    ЛЕКСЕМЫ-ЯРА:Открывающее-слово
    ЛЕКСЕМЫ-ЯРА:Закрывающее-слово
    ЛЕКСЕМЫ-ЯРА:Лексемы

    ;;Вхождение-лексемы-в-лексему
    ЛЕКСЕМЫ-ЯРА:Вхождение-лексемы-в-лексему
    ЛЕКСЕМЫ-ЯРА:MAKE-Вхождение-лексемы-в-лексему
    ЛЕКСЕМЫ-ЯРА:Вхождение-лексемы-в-лексему-P
    ЛЕКСЕМЫ-ЯРА:COPY-Вхождение-лексемы-в-лексему
    
    ЛЕКСЕМЫ-ЯРА:Вхождение-лексемы-в-лексему-Входит-как
    ЛЕКСЕМЫ-ЯРА:Вхождение-лексемы-в-лексему-Этап
    ЛЕКСЕМЫ-ЯРА:Вхождение-лексемы-в-лексему-Родитель
    
    ;;Лексема
    ЛЕКСЕМЫ-ЯРА:Лексема
    ЛЕКСЕМЫ-ЯРА:MAKE-Лексема
    ЛЕКСЕМЫ-ЯРА:Лексема-P
    ЛЕКСЕМЫ-ЯРА:COPY-Лексема
    
    ЛЕКСЕМЫ-ЯРА:Лексема-Лексемы
    ЛЕКСЕМЫ-ЯРА:Лексема-$Текст
    ЛЕКСЕМЫ-ЯРА:Лексема-Сод
    ЛЕКСЕМЫ-ЯРА:Лексема-Класс

    ЛЕКСЕМЫ-ЯРА:Лексема-$Начало
    ЛЕКСЕМЫ-ЯРА:Лексема-$Конец

    ЛЕКСЕМЫ-ЯРА:Лексема-Заготовка
    ЛЕКСЕМЫ-ЯРА:Лексема-Белое-поле-до
    ЛЕКСЕМЫ-ЯРА:Лексема-Статус
    ЛЕКСЕМЫ-ЯРА:Лексема-Ошибка
    ЛЕКСЕМЫ-ЯРА:Лексема-Отступ-подлежит-контролю
    ЛЕКСЕМЫ-ЯРА:Лексема-Вышестоящие
   
    ;; ф-ии лексемы, не являющиеся полями
    ЛЕКСЕМЫ-ЯРА:Лексема-Текст
    ЛЕКСЕМЫ-ЯРА:Лексема-Начало
    ЛЕКСЕМЫ-ЯРА:Лексема-Конец
    ЛЕКСЕМЫ-ЯРА:Лексема-Источник

  
    ЛЕКСЕМЫ-ЯРА:Б-или-Кнф
    
    ;;Заготовка-строки
    ЛЕКСЕМЫ-ЯРА:Заготовка-строки
    ЛЕКСЕМЫ-ЯРА:MAKE-Заготовка-строки
    ЛЕКСЕМЫ-ЯРА:Заготовка-строки-P
    ЛЕКСЕМЫ-ЯРА:COPY-Заготовка-строки
    
    ЛЕКСЕМЫ-ЯРА:Заготовка-строки-З
    ЛЕКСЕМЫ-ЯРА:Заготовка-строки-Т-р
    
    ЛЕКСЕМЫ-ЯРА:сп-Краткая-печать-лексем
    
    ЛЕКСЕМЫ-ЯРА:Допустимый-класс-лексемы ; тип
    ЛЕКСЕМЫ-ЯРА:Допустимый-класс-лексемы-ли
    ЛЕКСЕМЫ-ЯРА:Допустимый-класс-операции
    
    ;;; Содержимое лексем
    
    ;;Вид-строкового-литерала
    ЛЕКСЕМЫ-ЯРА:Вид-строкового-литерала  
    ЛЕКСЕМЫ-ЯРА:Всл-простой
    ЛЕКСЕМЫ-ЯРА:Всл-трёхкавычечный
    ЛЕКСЕМЫ-ЯРА:Всл-с-отступом

    ;;Сод-Строковый-литерал
    ЛЕКСЕМЫ-ЯРА:Сод-Строковый-литерал
    ЛЕКСЕМЫ-ЯРА:MAKE-Сод-Строковый-литерал
    ЛЕКСЕМЫ-ЯРА:Сод-Строковый-литерал-P
    ЛЕКСЕМЫ-ЯРА:COPY-Сод-Строковый-литерал
    
    ЛЕКСЕМЫ-ЯРА:Сод-Строковый-литерал-Вид
    ЛЕКСЕМЫ-ЯРА:Сод-Строковый-литерал-Закрывающая-последовательность
    ЛЕКСЕМЫ-ЯРА:Сод-Строковый-литерал-Приставка
    ЛЕКСЕМЫ-ЯРА:Сод-Строковый-литерал-Открывающая-последовательность
    
    ;;Сод-Простой-символ
    ЛЕКСЕМЫ-ЯРА:Сод-Простой-символ
    ЛЕКСЕМЫ-ЯРА:MAKE-Сод-Простой-символ
    ЛЕКСЕМЫ-ЯРА:Сод-Простой-символ-P
    ЛЕКСЕМЫ-ЯРА:COPY-Сод-Простой-символ
    
    ЛЕКСЕМЫ-ЯРА:Сод-Простой-символ-Коя-пс
    ЛЕКСЕМЫ-ЯРА:Сод-Простой-символ-Закавыченный
    ЛЕКСЕМЫ-ЯРА:Сод-Простой-символ-Строка-имени
    ЛЕКСЕМЫ-ЯРА:Сод-Простой-символ-Каноническая-строка-имени

    ;;Сод-Символ
    ЛЕКСЕМЫ-ЯРА:Сод-Символ
    ЛЕКСЕМЫ-ЯРА:MAKE-Сод-Символ
    ЛЕКСЕМЫ-ЯРА:Сод-Символ-P
    ЛЕКСЕМЫ-ЯРА:COPY-Сод-Символ
    
    ЛЕКСЕМЫ-ЯРА:Сод-Символ-Квал-лексикона
    ЛЕКСЕМЫ-ЯРА:Сод-Символ-Отделитель-имени
    ЛЕКСЕМЫ-ЯРА:Сод-Символ-Имя
    ЛЕКСЕМЫ-ЯРА:Сод-Символ-Коя-с
    ЛЕКСЕМЫ-ЯРА:Сод-Символ-Квал-библ
    ЛЕКСЕМЫ-ЯРА:Сод-Символ-Символ-Яра
    ЛЕКСЕМЫ-ЯРА:Сод-Символ-Завёрнут-в-псимв
    
    ;;Сод-иерархическая-группа
    ЛЕКСЕМЫ-ЯРА:Сод-иерархическая-группа
    ЛЕКСЕМЫ-ЯРА:MAKE-Сод-иерархическая-группа
    ЛЕКСЕМЫ-ЯРА:Сод-иерархическая-группа-P
    ЛЕКСЕМЫ-ЯРА:COPY-Сод-иерархическая-группа
    
    ЛЕКСЕМЫ-ЯРА:Сод-иерархическая-группа-Открывающее-слово
    ЛЕКСЕМЫ-ЯРА:Сод-иерархическая-группа-Откр-слово-принадлежит-ли
    ЛЕКСЕМЫ-ЯРА:Сод-иерархическая-группа-Закрывающее-слово
    ЛЕКСЕМЫ-ЯРА:Сод-иерархическая-группа-Закр-слово-принадлежит-ли

    ;;Сод-кн*
    ЛЕКСЕМЫ-ЯРА:Сод-кн*
    ЛЕКСЕМЫ-ЯРА:MAKE-Сод-кн*
    ЛЕКСЕМЫ-ЯРА:Сод-кн*-P
    ЛЕКСЕМЫ-ЯРА:COPY-Сод-кн*
    
    ЛЕКСЕМЫ-ЯРА:Сод-кн*-Буква

    ;;Сод-сегмент-квалификатора-библиотеки
    ЛЕКСЕМЫ-ЯРА:Сод-сегмент-квалификатора-библиотеки
    ЛЕКСЕМЫ-ЯРА:MAKE-Сод-сегмент-квалификатора-библиотеки
    ЛЕКСЕМЫ-ЯРА:Сод-сегмент-квалификатора-библиотеки-P
    ЛЕКСЕМЫ-ЯРА:COPY-Сод-сегмент-квалификатора-библиотеки
    
    ЛЕКСЕМЫ-ЯРА:Сод-сегмент-квалификатора-библиотеки-Отделители
    ЛЕКСЕМЫ-ЯРА:Сод-сегмент-квалификатора-библиотеки-Имя
    
    ;;Сод-сегмент-квалификатора-пр-ва-имён
    ЛЕКСЕМЫ-ЯРА:Сод-сегмент-квалификатора-пр-ва-имён
    ЛЕКСЕМЫ-ЯРА:MAKE-Сод-сегмент-квалификатора-пр-ва-имён
    ЛЕКСЕМЫ-ЯРА:Сод-сегмент-квалификатора-пр-ва-имён-P
    ЛЕКСЕМЫ-ЯРА:COPY-Сод-сегмент-квалификатора-пр-ва-имён
    
    ЛЕКСЕМЫ-ЯРА:Сод-сегмент-квалификатора-пр-ва-имён-Отделители
    ЛЕКСЕМЫ-ЯРА:Сод-сегмент-квалификатора-пр-ва-имён-Имя

    ;; А также проэкспортируем имя поля Отделители
    ЛЕКСЕМЫ-ЯРА:Отделители
    
    ;; Псевдо-классы лексем
    ЛЕКСЕМЫ-ЯРА:Это-слово-если-ли
    ЛЕКСЕМЫ-ЯРА:Это-слово-то-ли
    ЛЕКСЕМЫ-ЯРА:Это-слово-если-ли
    ЛЕКСЕМЫ-ЯРА:Это-слово-иначесли-ли
    ЛЕКСЕМЫ-ЯРА:Это-слово-иначе-ли
    ЛЕКСЕМЫ-ЯРА:Это-слово-кн-ли
    ЛЕКСЕМЫ-ЯРА:Это-слово-опр-ли
    ЛЕКСЕМЫ-ЯРА:Это-слово-пусть-ли
    ЛЕКСЕМЫ-ЯРА:Это-слово-зап-ли
    ЛЕКСЕМЫ-ЯРА:Это-слово-опрт-выраж-ли
    ЛЕКСЕМЫ-ЯРА:Это-слово-тело-ли
    ЛЕКСЕМЫ-ЯРА:Это-слово-функ-ли
    ЛЕКСЕМЫ-ЯРА:Это-такое-слово-ли
    ЛЕКСЕМЫ-ЯРА:Это-кн-точка-ли
    ЛЕКСЕМЫ-ЯРА:Это-слово-цикл-ли
    ЛЕКСЕМЫ-ЯРА:Это-слово-пц-ли
    ЛЕКСЕМЫ-ЯРА:Это-слово-из-ли
    ЛЕКСЕМЫ-ЯРА:Это-слово-сч-ли
    ЛЕКСЕМЫ-ЯРА:Это-слово-блок-ли
    ЛЕКСЕМЫ-ЯРА:Это-слово-класс-ли
    ЛЕКСЕМЫ-ЯРА:Это-слово-поля-ли
    ЛЕКСЕМЫ-ЯРА:Это-слово-открывающее-группу-второго-уровня-в-определении-ли

    ;; разница, как минимум, в случае псимв - простой символ будет именовать ключевое слово, а собранный символ - нет
    ЛЕКСЕМЫ-ЯРА:Простой-символ-именует-ключевое-слово-ли
    ЛЕКСЕМЫ-ЯРА:Символ-именует-ключевое-слово-ли 
    ЛЕКСЕМЫ-ЯРА:Лексема-означает-ключевое-слово-ли

    ЛЕКСЕМЫ-ЯРА:Это-Коя-ключевого-слова-ли
    
    ;; Разное
    ЛЕКСЕМЫ-ЯРА:Составная-лексема-ли
    ЛЕКСЕМЫ-ЯРА:Лексема-Собрана
    ЛЕКСЕМЫ-ЯРА:K-CONS-OF--Лексема
    ЛЕКСЕМЫ-ЯРА::DEF-PRINT-OBJECT--Для-структуры-лексера-и--LOAD-FORM
    ЛЕКСЕМЫ-ЯРА::Собственное-имя-символа-по-лексеме

    ;; ЛЕКСЕМЫ-ЯРА::Определить-ключевое-слово-по-тексту-лексемы
    ЛЕКСЕМЫ-ЯРА::Определить-коя-по-тексту-лексемы

    ;;Глобальные-свойства-класса-лексемы
    ЛЕКСЕМЫ-ЯРА:Глобальные-свойства-класса-лексемы
    ЛЕКСЕМЫ-ЯРА:MAKE-Глобальные-свойства-класса-лексемы
    ЛЕКСЕМЫ-ЯРА:Глобальные-свойства-класса-лексемы-P
    ЛЕКСЕМЫ-ЯРА:COPY-Глобальные-свойства-класса-лексемы
    ЛЕКСЕМЫ-ЯРА:Глобальные-свойства-класса-лексемы-Группа-ли
    ЛЕКСЕМЫ-ЯРА:Глобальные-свойства-класса-лексемы-Составная-ли
    
    ЛЕКСЕМЫ-ЯРА:Глобальные-свойства-операции-Nud
    ЛЕКСЕМЫ-ЯРА:Глобальные-свойства-операции-Lbp
    ЛЕКСЕМЫ-ЯРА:Глобальные-свойства-операции-Led
    
    ;;Глобальные-свойства-операции
    ЛЕКСЕМЫ-ЯРА:Глобальные-свойства-операции
    ЛЕКСЕМЫ-ЯРА:MAKE-Глобальные-свойства-операции
    ЛЕКСЕМЫ-ЯРА:Глобальные-свойства-операции-P
    ЛЕКСЕМЫ-ЯРА:COPY-Глобальные-свойства-операции
    
    ЛЕКСЕМЫ-ЯРА:Глобальные-свойства-операции-Имя
    ЛЕКСЕМЫ-ЯРА:Глобальные-свойства-операции-Текст
    ЛЕКСЕМЫ-ЯРА:Глобальные-свойства-операции-Право-ассоциативная-ли
    
    ЛЕКСЕМЫ-ЯРА::Получить-Глобальные-свойства-класса-лексемы
    ЛЕКСЕМЫ-ЯРА::Получить-Глобальные-свойства-операции-по-лексеме

    ЛЕКСЕМЫ-ЯРА::Лексема-является-символом-и-именует-операцию-ли

    ЛЕКСЕМЫ-ЯРА::Запрещён-этот-коя-внутри-псимв-ли
    "))
