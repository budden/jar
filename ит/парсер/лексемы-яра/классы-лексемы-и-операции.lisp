;; -*- system :ЛЕКСЕМЫ-ЯРА ; coding: utf-8; -*- 
;; Copyright (C) Денис Будяк 2015
(budden-tools::in-readtable :buddens-readtable-a)

(in-package :ЛЕКСЕМЫ-ЯРА)

(defun Список-внешних-символов-пакета (п)
  (let (result)
    (do-external-symbols (s п)
      (push s result))
    result))

(defparameter *Классы-лексем*
  (Список-внешних-символов-пакета :КЛАССЫ-ЛЕКСЕМ-ЯРА))

(defparameter *Классы-операций*
  (Список-внешних-символов-пакета :КЛАССЫ-ОПЕРАЦИЙ-ЯРА))

;; Операции - это разные классы с т.з. грамматики (т.к. приоритеты), но все они - символы. Поэтому для парсера понадобится другой предикат. 
(defun Допустимый-класс-лексемы-ли (class)
  (member class *Классы-лексем*))

(defun Допустимый-класс-операции-ли (Коя)
  (member Коя *Классы-операций*))

(deftype Допустимый-класс-лексемы () '(satisfies Допустимый-класс-лексемы-ли))

(deftype Допустимый-класс-операции () '(satisfies Допустимый-класс-операции-ли))

(deftype Ассоциативность-операции ()
  '(member :Лево-ассоциативна :Право-ассоциативна :Операция-сравнения))

(defstruct Глобальные-свойства-класса-лексемы
  "Свойства Кля"
  Составная-ли ; символ, псимв, группы
  Группа-ли ; группа в смысле группировщика
  )

(defstruct (Глобальные-свойства-операции (:include Глобальные-свойства-класса-лексемы))
  "Свойства значка операции, см. свойства-операций.lisp"
  #| Описание метода Pratt Parsing

  https://habrahabr.ru/post/50349/
  
  |#
  ;; В статье Pratt написано, что одна и та же операция (например, минус или круглые скобки), может быть и префиксной и инфиксной.
  ;; Мы от того ПОКА избавились, но не факт, что навсегда. Нужно ли сразу сделать здесь две под-записи для префиксной и инфиксной формы?
  ;; Или подждём до момента, когда оно грянет?
  Lbp ; приоритет связывания символа слева, 
  Nud ; функция, определяющая результат применения оператора в начале выражения, 
  Led ; функция, определяющая результат применения в середине выражения.
  Имя ; должно совпадать с тем, чему присвоено, например, 'Коя-бинарный-+
  Текст ; как пишется операция
  Право-ассоциативная-ли
  )

(defun Лексема-является-символом-и-именует-операцию-ли (Лексема)
  "Коя у нас назначено чёрт-те чему. Отличаем истинно операции в смысле парсера. Возвращает
   Глобальные-свойства-операции"
  (perga
   (let Коя
     (or
      (and (eql (Лексема-Класс Лексема) 'Кля-символ)
           (Сод-Символ-Коя-с (Лексема-Сод Лексема)))
      ;; этот вариант конкретно для цит
      (and (eql (Лексема-Класс Лексема) 'Кля-простой-символ)
           (Сод-Простой-символ-Коя-пс (Лексема-Сод Лексема)))))
   (when (and Коя (boundp Коя))
     (let Коя-зн (symbol-value Коя))
     (and (typep Коя-зн 'Глобальные-свойства-операции)
          Коя-зн))))

(defun Запрещён-этот-коя-внутри-псимв-ли (Коя)
  "Классы операций, на которых не действует псимв и их тем самым невозможно затенить. Список предварительный, есть сомнения хотя бы насчёт точки. Однако консерватизм говорит о том, что не стоит спешить добавлять возможности, поэтому сейчас мы делаем список исключений как можно большим."
  (member Коя '(Коя-. Коя-псимв)))

(defun Получить-Глобальные-свойства-класса-лексемы (Класс)
  "См.  свойства-операций.lisp"
  (perga
   (let Рез (and (boundp Класс) (symbol-value Класс)))
   (and Рез (the Глобальные-свойства-класса-лексемы Рез))))

(defun Получить-Глобальные-свойства-операции-по-лексеме (Л)
  "Если является операцией, то получает свойства. Если не является, то возвращает nil. См. свойства-операций.lisp"
  (perga
   (or (Лексема-является-символом-и-именует-операцию-ли Л)
       (perga
        (let Рез (Получить-Глобальные-свойства-класса-лексемы (Лексема-Класс Л)))
        (and (typep Рез 'Глобальные-свойства-операции) Рез)))))

;; API ключевых слов. По-хорошему, это должно быть на хеш-таблицах,
;; а семантика должна содержаться в символах, например, в их значениях
;; что ещё тут есть? оя::значение-оператора
(defparameter *Коя-ключевых-слов*
  '(("квц" . Коя-квц)
    ("квп" . Коя-квп)
    ("ключи" . Коя-ключи)
    ("псимв" . Коя-псимв)
    ;; раздеть - для вставки одетого выр, где ожидается голое. 
    ("раздеть" . Коя-раздеть)
    ("°" . Коя-знак-градуса)
    ("прихвостень" . Коя-прихвостень)
    ("цит" . Коя-цит)
    )
  "Эти ключевые слова не сворачиваются в символы, а остаются простыми символами. Соответственно, для понимания текста, который их содержит, не нужно понимания действующего пространства имён. Определение должно быть согласовано с оя::ключевые-слова")

(defun Определить-коя-по-тексту-лексемы (Текст)
  (or
   (Определить-ключевое-слово-по-тексту-лексемы Текст)
   (Определить-коя-операции-по-тексту-лексемы Текст)))

(defun Определить-ключевое-слово-по-тексту-лексемы (Текст)
  "Если текст принадлежит ключевому слову, вернуть ключевое слово, иначе NIL"
  (cdr (assoc Текст *Коя-ключевых-слов* :test 'string=)))

(defun Простой-символ-именует-ключевое-слово-ли (Л)
  "Для того, чтобы по собранной лексеме простого символа проверять, что это ключевое слово. См. также Символ-именует-ключевое-слово-ли . Если именует ключевое слово, то возвращаем коя"
  (perga
   (assert (Лексема-Собрана Л))
   (let Коя (Сод-Простой-символ-Коя-пс (Лексема-Сод Л)))
   (and (Это-Коя-ключевого-слова-ли Коя) Коя)))

(defun Символ-именует-ключевое-слово-ли (Л)
  "См. также Символ-именует-ключевое-слово-ли"
  (assert (Лексема-Собрана Л))
  (Это-Коя-ключевого-слова-ли (Сод-Символ-Коя-с (Лексема-Сод Л))))

(defun Лексема-означает-ключевое-слово-ли (Л &optional Какое-именно)
  "Если да, то возвращает коя, например, Коя-цит. Делает лишний поиск, зато не вернёт истину для класса, к-рый не является классом ключевого слова"
  (and (eql (Лексема-Класс Л) 'Кля-простой-символ)
       (perga
        (let Коя (Простой-символ-именует-ключевое-слово-ли Л))
        (and (implies Какое-именно (eq Коя Какое-именно))
             Коя))))

(defun Это-Коя-ключевого-слова-ли (Коя)
  "Если да, то возвращает Коя"
  (and Коя ; небольшая оптимизация
       (cdr (find Коя *Коя-ключевых-слов* :key 'cdr))))

(defun Определить-коя-операции-по-тексту-лексемы (Текст)
  (alexandria:switch
   (Текст :test 'string=)
   ("не" 'Коя-не)
   ("и" 'Коя-и)
   ("или" 'Коя-или)))
