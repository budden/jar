;;; -*- Mode:Lisp; system :П-Я-ЛЕКСЕР; coding: utf-8; -*-
;; Copyright (C) Денис Будяк 2017

(named-readtables:in-readtable :buddens-readtable-a)
(in-package :П-Я-ЛЕКСЕР)

;;; Для чтения чисел
(defun ctoi (d) (- (char-code d) #.(char-code #\0)))

(defun remove-junk-from-printed-number (s)
  "Оставляет от напечатанного плавающего числа голую мантиссу"
  (perga
   (:lett s string s)
   (setf s 
         (remove-if (lambda (x) (member x '(#\+ #\- #\.)))
                    s))
   (let e-index (position-if (lambda (x) (member x '(#\e #\d) :test 'char-equal)) s))
   (when e-index
     (setf s (subseq s 0 e-index)))
   s
   ))

(defun check-float-precision (token fr-scaled nf rational result)
  "Проверяет, что все считанные цифры остались с нами"
  (declare (ignorable nf))
  (perga
    (let str (concatenate 'string (k:reverse token)))
    ;(let original-str str #+nil (format nil "~D" (* unscaled (expt 10 nf))))
    ;(let original-mantissa (remove-junk-from-printed-number original-str))
    (let original-mantissa (format nil "~D" (* fr-scaled (expt 10 nf))))
    (let result-mantissa
      (remove-junk-from-printed-number (format nil "~E" result))) 
    (assert (every 'eql result-mantissa original-mantissa) () "При чтении числа ~S произошла потеря точности мантиссы ~S=~S" str original-mantissa result-mantissa)
    (assert (<= (abs (* 10000000.0 (- rational result)))
                (max (abs rational) (abs result))) ()
      "При чтении числа ~S произошла потеря точности числа: ~S" str result)
    ))

(defvar *check-float-precision* nil)

(defun make-float (token m sign i f nf ex)
  #+nil (list m sign i f nf ex)
  (declare (ignore m))
  (let* ((fr-scaled  (+ i (/ f (expt 10 nf))))
         (rational (* sign fr-scaled (expt 10 ex)))
         (result (coerce rational 'double-float)))
    (when *check-float-precision* 
      (check-float-precision token fr-scaled nf rational result))
    result
    ))

(defun Прочитать-числовой-литерал (Знак)
  "Если передан знак, то это явно положительное или отрицательное число. Иначе - неявно положительное. Следующий знак - цифра"
 (let (x (i 0) fd (f 0) (nf 0) es ed (e 0) m have-dot Цифра-мантиссы Точка)
  (perga
   (let Первая-итерация t)
   (and
    (loop ;; Собираем цифры мантиссы
      ;; Для первой итерации взять цифру, к-рую нам прислали в параметре. Дальше пытаться получить цифры из потока
      (setf Цифра-мантиссы
        (cond
         (Первая-итерация
          (setf Первая-итерация nil)
          (setf Цифра-мантиссы (Чит-э)))
         ((Цифра-ли (л-Класс))
          (setf Цифра-мантиссы (Чит-э)))
         (t (return t))))
      (setq x nil i (+ (* i 10) (ctoi (Б Цифра-мантиссы))))
      (Бб Цифра-мантиссы))
    (or
     (and  
      (setf Точка (Чит-ежли-к #\.))
      (or ; читаем точку
       fd
       (progn
         (k:push #\. x)
         (setf have-dot t)
         (Бб Точка)))
      (loop ; за точкой читаем цифры дробной части
        (or 
         (and
          (setf fd (and (Цифра-ли (л-Класс)) (Чит-э)))
          (setq x nil nf (1+ nf) f (+ (* f 10) (ctoi (Б fd))))
          (Бб fd))
         (return t))))
     t)
    (or
     (and
      (or Цифра-мантиссы fd)
      (and (expmarker-p (л-Класс)) (setf m (Чит-э)))
      (k:push (Б m) x)
      (Бб m) ; считали маркер показателя степени 
      (or 
       (and
        (and (Знак-плюс-или-минус-ли (л-Класс)) (setf es (Чит-э)))
        (k:push (Б es) x)
        (Бб es))
       t)  ; считали знак показателя степени
      (loop
        (or 
         (and
          (Цифра-ли (л-Класс))
          (setf ed (Чит-э))
          (setq x nil e (+ (* e 10) (ctoi (Б ed))))
          (Бб ed))
         (return t))) ; считали цифры показателя степени
      ) ; считали степень
     t ; или не было степени
     ) ; дочитали цифру - теперь обработка (мы всё ещё внутре and)
    (let* ((sign (cond
                  ((null Знак) 1)
                  ((eql (Б Знак) #\-) -1)
                  (t 1)))
           (ex (if (and es (eql (Б es) #\-)) (- e) e))
           (res (cond
                 ((k:limited-equalp (k:mapcar 'Б (Состояние-читателя-Собранные-буквы-лексемы-которую-мы-строим (Состояние-читателя))) k:{ #\. k:} ) '|.|)
                 ((or fd ed have-dot) (make-float (Состояние-читателя-Собранные-буквы-лексемы-которую-мы-строим (Состояние-читателя)) (if m (Б m) #\e) sign i f nf ex)) ; see [ Clinger90 ]
                 ; (dd (/ (* sign i) d)) ; это убрано, т.к. ветки с dd у нас сейчас нет
                 (Цифра-мантиссы (* sign i))
                 (t nil))))
      ; (print (list x xx (Состояние-читателя-Собранные-буквы-лексемы-которую-мы-строим (Состояние-читателя)) m sign i f nf ex))
      ; (loop for u in xx do (collect-char u))
      (cond
       (res
        (Гл :Сод res :Класс (etypecase res (integer 'Кля-целый-литерал) (float 'Кля-плавающий-литерал))))
       (t
        (Ошибка-р "Некорректный числовой литерал")))
      )))))
