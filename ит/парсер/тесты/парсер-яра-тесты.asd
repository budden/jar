;; -*- encoding: utf-8; coding: utf-8; -*- 
;; Copyright (C) Денис Будяк 2015

(named-readtables:in-readtable :buddens-readtable-a)

(defsystem :ПАРСЕР-ЯРА-ТЕСТЫ
  :depends-on (:ПАРСЕР-ЯРА :ЯР-СРЕДА-ВЫП)
  :serial t
  :components ((:file "парсер-яра-тесты")
               ))
