;; -*- encoding: utf-8; coding: utf-8; -*- 
;; Copyright (C) Денис Будяк 2015 - 2017

(named-readtables:in-readtable :buddens-readtable-a)

(defsystem :ПАРСЕР-ЯРА
  :description "Собирает весь парсер Яра"
  :depends-on (:П-Я-ФУНДАМЕНТ :П-Я-ЛЕКСЕР :П-Я-ПОСТ-ЛЕКСЕР :П-Я-ГРУППИРОВЩИК :П-Я-ГРАМОТЕЙ)
  :serial t
  :components ((:file "парсер-яра" :description "Интерфейс")
               ))

;; См. также: 
;; ( ../yar/раскраска-яра-лексером.lisp ) 
;; РАСКРАСКА-ЯРА-ЛЕКСЕРОМ::Функция-раскраски-Яра-лексером-для-редактора , РАСКРАСКА-ЯРА-ЛЕКСЕРОМ::Dump-token--для-степпера-Яра-3  
;; 
;; ( ../../lp/clcon/clcon-odu-commands-infrastructure.lisp ) 
;; РАСКРАСКА-ЯРА-ЛЕКСЕРОМ:Recompute-syntax-marks--специально-для-Яра 


