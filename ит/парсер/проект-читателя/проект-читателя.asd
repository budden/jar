;;; -*- coding: utf-8; Mode: Lisp -*-

;; Для пересоздания док-та выполни (ОЯ-ИНФРАСТРУКТУРА:Собрать-проект-Html-файла :ПРОЕКТ-ЧИТАТЕЛЯ)

(in-package #:asdf)
(named-readtables::in-readtable :buddens-readtable-a)

(defsystem :ПРОЕКТ-ЧИТАТЕЛЯ
 :serial t
 :depends-on (:СПРАВОЧНИКИ-HTML)
 :components
  ((:file "пакет-проект-читателя")
   (:file "очистить-БД-и-заполнить-теги")
   (:file "проект-читателя-бд")
   ))

