;;; -*- Mode:Lisp; system :П-Я-ГРУППИРОВЩИК; coding: utf-8; -*-
;; Copyright (C) Денис Будяк 2017

(named-readtables:in-readtable :buddens-readtable-a)
(in-package :П-Я-ГРУППИРОВЩИК)

(defun Элементы-Лексемы-типа-Список-через-запятую (Л)
  (perga
   (assert (eq (Лексема-Класс Л)
               'Кля-список-через-запятую))
   (let Рез nil)
   (let Рез-тр nil)
   (k:dolist (ПЛ (Лексема-Лексемы Л))
             (when
                 (eq (Лексема-Класс ПЛ)
                     'Кля-элемент-списка-через-запятую)
               (k:budden-tools-collect-into-tail-of-list
                ПЛ
                Рез
                Рез-тр)))
   Рез))


