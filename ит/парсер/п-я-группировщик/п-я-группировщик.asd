;; -*- encoding: utf-8; coding: utf-8; -*- 
;; Copyright (C) Денис Будяк 2015 - 2017

(named-readtables:in-readtable :buddens-readtable-a)

(defsystem :П-Я-ГРУППИРОВЩИК
  :depends-on (:П-Я-ПОСТ-ЛЕКСЕР)
  :description "Сбор групп, окружённых парами скобок, слов или парой слово - конец-строки. См. п-я-группировщик.md"
  :serial t
  :components ((:file "группировщик-пакет")
               (:file "группировщик-фундамент" )
               (:file "группировщик-стек" :description "Реализация стека групп, отдельно от деталей, свойственных отдельным видам групп")
               (:file "открывающие-и-закрывающие-слова" :description "Вычиcление, какие лексемы являются открывающими, какие закрывающими, и как они между собой соотносятся")
               (:file "пров-отст-и-роль-в-групп" :description "Проверка отступов, а также вычисление, какие группы закрываются данным словом в конкретном тексте, с учётом возможных ошибок разбора")
               (:file "группировщик" :description "Собираем всё воедино")
               (:file "анализ-групп" :description "Анализ результатов работы группировщика")))

