; -*- system :СПРАВОЧНИКИ-HTML; coding: utf-8; -*- 

(named-readtables:in-readtable :buddens-readtable-a)

(in-package :ОЯ-ИНФРАСТРУКТУРА)

(defvar *showdown-загружен* nil)
            
(defun загрузить-showdown-если-надо () 
  (unless *showdown-загружен*
    #+SBCL (cl-user::gc :gen 2)
    (cl-js:run-js-file (cl-user::putq-otnositelqno-kornya-yara "док/js/showdown.js"))
    (setf *showdown-загружен* t)))

(загрузить-showdown-если-надо)

(defun md-here (source)
  (let ((js-markdown (cl-js:run-js "f = function(txt) { a = new showdown.Converter({'tables' : true}); return a.makeHtml(txt) }; f")))
    (or (cl-js:js-call js-markdown nil source)
        (format nil "<pre>~a</pre>" source))))

(defun запись-по-символу (sym)
  (find sym (Проект-файла-Html-Определения †Текущий-проект-Html-файла) :key #'БД-ОПРЕДЕЛЕНИЕ-ЗАПИСЬ-ИМЯ))

(defparameter *стиль-таблицы*    
   "
   TABLE {
    border-collapse: collapse; /* Убираем двойные линии между ячейками */
    width: auto; /* Ширина таблицы */
   }
   TH { 
    background: #fff; /* Цвет фона ячейки */
    text-align: center; /* Выравнивание по левому краю */
   }
   TD {
    background: #fff; /* Цвет фона ячеек */
    text-align: center; /* Выравнивание по центру */
   }
   TH, TD {
    border: 1px solid black; /* Параметры рамки */
    padding: 4px; /* Поля вокруг текста */
   }
   ")


(defun прочитать-символ-ссылки (символ)
  (let ((*readtable* (named-readtables:find-readtable :buddens-readtable-a))
        (*package* (find-package (Проект-файла-Html-Имя-пакета †Текущий-проект-Html-файла))))
    (read-from-string символ)))

(defun ссылка-на-статью-в-строку-ф (sym Известные-ссылки)
  (format nil "<li><a href=\"#a~a\">~a</a></li>"
          (Название-статьи-для-адреса sym Известные-ссылки)
          sym
          ))

(defun Заголовок-статьи-в-строку-для-оглавления (запись Известные-ссылки &key Не-глубже-уровня (Префикс-ссылки "") Префикс-имени-этой-ссылки)
  "Если Не-глубже-уровня - число, то для заголовков более глубоких уровней возвращается пустая строка. Уровни начинаются с 0
   Если Префикс-имени задан, то у этого места в оглавлении будет имя, равное ссылке, на которую она указывает + данный префикс."
  (perga-implementation:perga тело
   (let уровень (БД-ОПРЕДЕЛЕНИЕ-ЗАПИСЬ-УРОВЕНЬ запись))
   (when (and Не-глубже-уровня
              (> уровень Не-глубже-уровня))
     (return-from тело ""))
   (let список-отступов (make-list уровень :initial-element "&emsp;"))
   (let отступ (apply 'str+ список-отступов))
   (let Якорь-статьи-без-префиксов (Название-статьи-для-адреса (БД-ОПРЕДЕЛЕНИЕ-ЗАПИСЬ-ИМЯ запись) Известные-ссылки))
   (let имя (БД-ОПРЕДЕЛЕНИЕ-ЗАПИСЬ-ИМЯ запись))
   (let Имя-этого-места-в-оглавлении
     (if Префикс-имени-этой-ссылки
         ;; fixme здесь надо закавычить более культурно
         (str+ "name=\"" Префикс-имени-этой-ссылки Якорь-статьи-без-префиксов "\"")
         ""))
   (format nil "~a<a ~a href=\"#~a~a\">~a</a><br>"
           отступ
           Имя-этого-места-в-оглавлении
           Префикс-ссылки
           Якорь-статьи-без-префиксов
           имя)))

(defun id-тега (тег)
  (url-encode:url-encode
   (russian-budden-tools:translit-reversibly (russian-budden-tools:string-downcase-cyr (string тег)))))
  
(defun Название-статьи-для-адреса (Символ anchors)
  "Неизвестно, как у нас с поддержкой всех букв, могущих войти в символ"
  (unless (gethash Символ anchors)
    (warn "Описание языка: висячая ссылка ~S" Символ))
  (russian-budden-tools:translit-reversibly (string Символ)))

(defun make-html (&key Без-тел-статей)
  (let ((anchors (make-hash-table))
        (i 0)
        (Имя-пакета-проекта (Проект-файла-Html-Имя-пакета †Текущий-проект-Html-файла)))
    (labels ((save-item (запись)
               (setf (gethash (БД-ОПРЕДЕЛЕНИЕ-ЗАПИСЬ-ИМЯ запись) anchors)
                     (incf i))
               "")
             (ссылка-на-статью-в-строку (симв)
               (ссылка-на-статью-в-строку-ф симв anchors))
             (тег-в-строку-для-оглавления (тег)
               (format nil "<li><a href=\"#t~a\">~a</a></li>"
                       (id-тега тег)
                       тег))
             (тег-в-строку-для-статьи (тег) ; вид - :list или :space
               (format nil "<a href=\"#t~a\">~a</a>&emsp;"
                       (id-тега тег)
                       тег))
             (список-статей-по-тегу (тег)
                (let ((pos (id-тега тег)))
                  (format nil "<li><a name=\"t~a\" id=\"t~a\">Статьи с тегом ~a</a><ul>~a</ul></li>"
                          pos pos
                          тег
                          (apply #'concatenate 'string
                                 (mapcar #'ссылка-на-статью-в-строку (mapcar #'car (Где-есть-каждый-из-тегов тег)))))))
             (теги-статьи-в-строку (запись)
                (let ((tags (БД-ОПРЕДЕЛЕНИЕ-ЗАПИСЬ-ТЕГИ запись)))
                   (cond
                     ((null tags) "")
                     ((listp tags) (format nil "Теги:&emsp;~a"
                                              (apply #'concatenate 'string 
                                                     (mapcar #'тег-в-строку-для-статьи tags)))))))
             (refs (запись)
                (let ((refs (БД-ОПРЕДЕЛЕНИЕ-ЗАПИСЬ-СМ-ТАКЖЕ запись)))
                   (cond
                     ((null refs) "")
                     ((listp refs) (format nil "См. также <ul>~a</ul>"
                                              (apply #'concatenate 'string 
                                                     (mapcar #'ссылка-на-статью-в-строку refs)))))))
             (find-links (body)
               (cl-ppcre:regex-replace-all 
                (CL-PPCRE:CREATE-SCANNER (format nil "~A::(\\S+)" Имя-пакета-проекта) :CASE-INSENSITIVE-MODE T)
                body
                (lambda (match reg1 &rest rest)
                  (declare (ignore match rest))
                  (format nil "<a href=\"#a~a\">~a</a>"
                          (Название-статьи-для-адреса (прочитать-символ-ссылки reg1) anchors) reg1))
                :simple-calls t))
             (тело-статьи-в-строку (запись)
               (perga
                (let Н-д-а (Название-статьи-для-адреса (БД-ОПРЕДЕЛЕНИЕ-ЗАПИСЬ-ИМЯ запись) anchors))
                (let Имя-статьи (БД-ОПРЕДЕЛЕНИЕ-ЗАПИСЬ-ИМЯ запись))
                (let Тело-статьи
                  (cond
                   (Без-тел-статей "<p>...Без-тел-статей...</p>")
                   (t (md-here (find-links (БД-ОПРЕДЕЛЕНИЕ-ЗАПИСЬ-СТРОКА запись))))))
                (format nil "<h2><a name=\"a~a\" id=\"a~a\">~a</a><font size=-5>&nbsp;Ссылка: <a href=\"#a~a\">~a::~a</a></font></h2><strong>~a</strong><p>~a</p>~a~a<hr>" 
                         Н-д-а
                         Н-д-а
                         Имя-статьи                         

                         Н-д-а
                         Имя-пакета-проекта
                         Имя-статьи

                         (БД-ОПРЕДЕЛЕНИЕ-ЗАПИСЬ-АЛЬТ-ЗАГОЛОВОК запись)
                         Тело-статьи
                         (refs запись)
                         (теги-статьи-в-строку запись)))))
      (perga
       (let Все-определения (reverse (Проект-файла-Html-Определения †Текущий-проект-Html-файла)))
       (let Кс (format nil "~%"))
       (concatenate 'string "<!DOCTYPE html lang=\"ru\">
 <html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
 <style>
 " *стиль-таблицы* "
 </style>
 <title>" (Проект-файла-Html-Заголовок †Текущий-проект-Html-файла) "</title>
 </head>
 <body>
"

                  ; здесь ничего не печатается
                  (apply #'concatenate 'string 
                         (mapcar #'save-item Все-определения))

                  "<h1>" (Проект-файла-Html-Заголовок †Текущий-проект-Html-файла) "</h1>" Кс

                  "<p><a href=#statqi-po-tegam>Статьи по тегам</a>&nbsp;&nbsp;&nbsp;<a href=#tegi>Список тегов</a></p>"

                  
                  Кс "<h1>Оглавление</h1>" Кс
                  (with-output-to-string (Огл)
                    (dolist (Статья Все-определения)
                      (princ (Заголовок-статьи-в-строку-для-оглавления Статья anchors :Не-глубже-уровня 0
                                                                       :Префикс-ссылки "ado")
                             Огл)))
                  
                  Кс "<h1>Детальное оглавление</h1>" Кс
                  (with-output-to-string (Огл)
                    (dolist (Статья Все-определения)
                      (princ (Заголовок-статьи-в-строку-для-оглавления Статья anchors
         :Префикс-ссылки "a"
         :Префикс-имени-этой-ссылки "ado")
                             Огл)))
                  ""

                  Кс "<h1>Все статьи</h1>" Кс
                  (apply #'concatenate 'string
                         (mapcar #'тело-статьи-в-строку Все-определения))
                  "<h1><a name=statqi-po-tegam>Статьи по тегам</a></h1>"
                  "<ul>"

                  ; 
                  (apply #'concatenate 'string
                         (mapcar #'список-статей-по-тегу (Проект-файла-Html-Теги †Текущий-проект-Html-файла)))
                  "</ul>"                         

                  "<h1><a name=tegi>Теги</a></h1><ul>"
                  (apply #'concatenate 'string
                         (mapcar #'тег-в-строку-для-оглавления (Проект-файла-Html-Теги †Текущий-проект-Html-файла)))
                  "</ul>"

                  "</body></html>")))))

(defun Сохранить-описание-языка-в-файл (&key (Имя-файла (cl-user::putq-otnositelqno-kornya-yara "../yar.my/док/оя-яр-1/сгенерированное-описание-языка.html")) Без-тел-статей (открыть t))
  (with-open-file (s Имя-файла :direction :output :if-exists :supersede)
    (princ (ОЯ-ИНФРАСТРУКТУРА::make-html :Без-тел-статей Без-тел-статей) s))
  (when открыть
        (clcon-server::open-url Имя-файла)))


(defun Собрать-проект-Html-файла (Имя-системы &key Имя-файла Без-тел-статей)
  (asdf:load-system Имя-системы :force t)
  (ОЯ-ИНФРАСТРУКТУРА::Сохранить-описание-языка-в-файл
   :Имя-файла
   (or Имя-файла
       (cl-user::putq-otnositelqno-kornya-yara
        (format nil "пляж/~A.html" Имя-системы)))
   :Без-тел-статей Без-тел-статей))
   
        