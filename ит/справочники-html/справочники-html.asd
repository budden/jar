;;; -*- Encoding: utf-8; Mode: Lisp -*-

;;; Справочники в формате HTML из набора статей в Markdown с тегами и перекрёстными ссылками. 
;;; Каждый справочник должен быть системой, зависящей от данной. См. примеры использовани

(in-package #:asdf)
(named-readtables::in-readtable :buddens-readtable-a)

(defsystem :СПРАВОЧНИКИ-HTML
 :serial t
 :depends-on (:budden-tools :cl-js)
 :components
  ((:file "инфраструктура-для-создания-справочников")
   (:file "сохранить-как--html")
   ))

