;; -*- encoding: utf-8; coding: utf-8; -*- 
;; Copyright (C) Денис Будяк 2015

(named-readtables:in-readtable :buddens-readtable-a)

(defsystem :ЯР.ПОЛНОСТЬЮ-КЕШИРОВАННЫЙ-ПОТОК.ТЕСТЫ
  :depends-on (:budden-tools
               :ЯР.ПОЛНОСТЬЮ-КЕШИРОВАННЫЙ-ПОТОК
               )
  :serial t
  :components ((:file "полностью-кешированный-поток-тест" )))

