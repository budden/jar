;; -*- encoding: utf-8; coding: utf-8; -*- 
;; Copyright (C) Денис Будяк 2015

(named-readtables:in-readtable :buddens-readtable-a)

(defsystem :ЯР.ПОЛНОСТЬЮ-КЕШИРОВАННЫЙ-ПОТОК
  :depends-on (:anaphora ;for lexer-yar
               :budden-tools
               :meta-parse-firebird ; нужен yar-lexem
               :ВЕРСИИ
               :БУКВЫ-И-МЕСТА-В-ФАЙЛЕ
               )
  :serial t
  :components ((:file "полностью-кешированный-поток-типы" :description "Функциональная обёртка над потоком, в т.ч. Место-в-исходнике")
               (:file "полностью-кешированный-поток-1" :description "Часть ф-й отделяем, чтобы работала 'крышка', но не забывай, что крышка несовместима с my-eval")
               (:file "полностью-кешированный-поток-2")))
