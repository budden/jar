;;;; -*- Mode: lisp; indent-tabs-mode: nil; coding: utf-8;  -*-
;;;
(asdf:defsystem :asdf-yar-source-file
  :description "Компоненты для трансляции yar в lisp силами asdf"
  :author "Denis Budyak"
  :depends-on (:ЯРА-ИСР)
  :licence "MIT"
  :serial t
  :components
  ((:file "asdf-yar-source-file-package")
   (:file "asdf-yar-source-file")
   ))

