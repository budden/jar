;; -*- encoding: utf-8; coding: utf-8; system :раскраска-3; -*- 
;; Copyright (C) Денис Будяк 2015-2017
(budden-tools::in-readtable :buddens-readtable-a)

(in-package :РАСКРАСКА-3)


(defmethod print-object ((М Мазок) П)
  (perga
   (:@ print-unreadable-object (М П :type t :identity t))
   (:@ ignore-errors)
   (let Н (Мазок-Начало М))
   (let К (Мазок-Конец М))
   ;(let Н-строка (Номер-строки-в-марке Н))
   ;(let Н-колонка (oi::mark-charpos Н))
   ;(let К-строка (Номер-строки-в-марке К))
   ;(let К-колонка (oi::mark-charpos К))
   (let Акт (Мазок-Акт-раскраски М))
   ;(let Слой (Акт-раскраски-Слой Акт))
   ;(let Код-слоя (Слой-раскраски-Код-слоя Слой))
   ;(let Б (Слой-раскраски-Buffer Слой))
   (:lett Д integer (Мазок-Данные М))
   (let Статус (Мазок-Статус М))
   (let Tick_count (Акт-раскраски-Tick_count Акт))
   (format П "~A ~D ~D ~D ~D" Tick_count Статус Н К Д)))

(defun Эй-зпт-дайте-новый-акт-раскраски (Buffer Имя-слоя-раскраски)
  "Акт раскраски нужен для ф-ии Акт-зпт-добавь-мазок, и он представляет для лексера то место, куда лексер сдаёт свою работу"
  (perga
   (oduvanchik-ext:assert-we-are-in-oduvanchik-thread)
   (Создать-слои-раскраски-и-отправитель-раскраски-для-буфера-если-надо Buffer)
   (let Слой
     (the Слой-раскраски
          (k:find Имя-слоя-раскраски
                (oduvanchik-internals::buffer-raskraska Buffer)
                :key 'Слой-раскраски-Имя-слоя)))
   (Слой-зпт-удали-все-мазки Слой)
   (let Акт
     (MAKE-Акт-раскраски :Слой Слой :Tick_count (oduvanchik-internals::buffer-tcl_tick_count Buffer)))
   ;; Поскольку затребован новый акт, мы считаем, что старые акты устарели и по ним не нужно 
   ;; дорисовывать те мазки, которые ещё не были отправлены. Стирать старые мазки тоже не будем
   ;; спешить
   (symbol-macrolet С-А (Слой-раскраски-Неустаревшие-акты Слой))
   (loop
     (unless С-А
       (return))
     (Акт-зпт-ты-устарел (car С-А) nil))
   (push Акт С-А)
   Акт))

(defun Создать-слои-раскраски-и-отправитель-раскраски-для-буфера-если-надо (Buffer)
  (perga
   (unless (oi::buffer-raskraska Buffer)
     (let Рез nil)
     (let Т-р nil)
     (dolist (Спец *Спецификации-слоёв-раскраски*)
       (let Имя (Спецификация-слоя-раскраски-Имя Спец))
       (k:budden-tools-collect-into-tail-of-list
        (MAKE-Слой-раскраски
         :Buffer Buffer
         :Имя-слоя Имя
         :Код-слоя (Имя-слоя-раскраски-в-код Имя)
         :Сплошной (Спецификация-слоя-раскраски-Сплошной Спец))
        Рез
        Т-р))
     (setf (oi::buffer-raskraska Buffer) Рез)
     (setf (oi::buffer-otpravitelq-raskraski Buffer)
           (MAKE-Отправитель-раскраски
            :Буфер-редактора Buffer)))))

(defun Эй-зпт-раскраска-по-акту-завершена (Акт)
  "Это - сообщение от лексера к коллективу, отвечающему за раскраску, о том, что вся работа лексера завершена. В текущем алгоритме запускает процесс сравнения и раскраски"
  ;; В идеальном мире следующая ф-я должна запускать цепочку событий, чтобы
  ;; не ждать полной перераскраски. И только после этого удалять стёртые мазки
  ;; У нас, похоже, цепочка событий начнётся уже после отправки всех изменений
  ;; в Отправитель-раскраски, а оттуда они уже не удаляются, даже если Акт устаревает. 
  ;; После завершения цепочки событий должно происходить Слой-зпт-удали-все-мазки,
  ;; либо удаление стёртых мазков должно происходить по ходу цепочки событий, отдельными
  ;; порциями. 
  ;; Видимо, это - ПРАВЬМЯ, поскольку раньше было вроде бы лучше - марки отправлялись фоново
  ;; и этот процесс отменялся при новом редактировании. Хотя я уже запутался и точно не уверен.
  (perga
   (oduvanchik-ext:assert-we-are-in-oduvanchik-thread)
   (let Слой (Акт-раскраски-Слой Акт))
   (let Буфер (Слой-раскраски-Buffer Слой))
   (let О-р (oduvanchik-internals::buffer-otpravitelq-raskraski Буфер))
   (Коллектив-зпт-вычислите-и-отправьте-изменения-раскраски
    Слой 
    Акт
    (named-lambda Завершение-для-Эй-зпт-раскраска-по-акту-завершена ()
                  ;(break "Сейчас должна дойти марка")
                  (Отправитель-раскраски-зпт-отправь-буфер О-р Акт)
   ;; по идее, стирание мазков должно происходить после того, как они стёрты на стороне tcl, и не ранее, чем они
   ;; были использованы для итераций на стороне лиспа, но мы пока это не отслеживаем, 
   ;; поэтому стираем их сразу, как только они были использованы на стороне лиспа и была отправлена команда на стирание.
                  (Слой-зпт-удали-все-мазки Слой) ;; дубль - можно убрать?
                  (setf (Акт-раскраски-Статус Акт) 'сар-Вычислен)))))

(defun Акт-зпт-добавь-мазок (Акт вх-Начало вх-Конец Данные #| например, шрифт |# &key Прошлый-добавленный-мазок)
  "Этой ф-ей лексер оповещает Акт о том, что нужно добавить вот такую раскраску. Ф-я возвращает созданный мазок (хотя он не нужен лексеру). Важно то, что возврат nil означает что акт устарел, и лексеру нет смысла продолжать свою работу - лексер должен выйти"
  (perga
   тело
   ;;; Если Акт устарел, то отказываем.
   (when (eq (Акт-раскраски-Статус Акт) 'сар-Отменён)
     (return-from тело nil))
   ;;; Создаём марки
   (let М-Начало вх-Начало)
   (let М-Конец вх-Конец)
   ;; Создаём Мазок
   (let Мазок
     (MAKE-Мазок :Начало М-Начало :Конец М-Конец :Данные Данные
                 :Акт-раскраски Акт))
   ;; Говорим акту, что появился новый мазок, к-рый должен быть вставлен
   ;; в поле Мазки слоя. 
   (let Слой (Акт-раскраски-Слой Акт))
   (let Вхождение-в-список
     (Слой-раскраски-зпт-учти-новый-мазок Мазок Слой Прошлый-добавленный-мазок))
   Вхождение-в-список
   ))
  
(defun Слой-раскраски-зпт-учти-новый-мазок (Мазок Слой Место-рядом)
  "Вставляет мазок в Слой-раскраски-Мазки"
  (Д-СПИСОК:DLIST--Вставить-с-сохранением-порядка
   Мазок
   (Слой-раскраски-Мазки Слой)
   :Место-рядом Место-рядом
   :Предикат #'ПОЛНОСТЬЮ-КЕШИРОВАННЫЙ-ПОТОК:Место-в-исходнике-<
   :Ключ #'Мазок-Начало))

(defun Акт-зпт-ты-устарел (Акт Где)
  "Произошло новое редактирование в точке Где"
  ;;; На данном этапе устаревание тотально и не зависит от точки Где.
  ;; Прописываем статус и удаляем из списка неустаревших актов слоя.
  ;; Остальное произойдёт в других местах.
  ;; Функция раскраски должна проверять статус и возвращаться досрочно, если он нехорош.
  (perga
   (assert (null Где) () "Не умеем поддержать 'Где'")
   (unless (eq (Акт-раскраски-Статус Акт) 'сар-Отменён)
     (setf (Акт-раскраски-Статус Акт) 'сар-Отменён)
     (let Слой (Акт-раскраски-Слой Акт))
     (let Буфер (Слой-раскраски-Buffer Слой))
     (let О-р (oduvanchik-internals::buffer-otpravitelq-raskraski Буфер))
     (__f delete Акт (Слой-раскраски-Неустаревшие-акты Слой))
     (Слой-зпт-удали-все-мазки Слой)
     (Отправитель-раскраски-зпт-всё-отменяется О-р))))

(defun Слой-зпт-удали-все-мазки (Слой)
  (perga
   (setf (Слой-раскраски-Мазки Слой) (Д-СПИСОК:dlist))
   ))


(defstruct Квиоир-сост
  "Состояние процесса вычисления и отправки раскраски, который выполнен в виде цепочки событий"
  Слой
  Акт
  Buffer
  О-р
  (D--нов nil :type (nullable Д-СПИСОК:dcons)) ; а внутри dcons - мазок
  После-успешного-завершения-функ
  )

(defun След-нов (Сост &key Запуск)
  "Найти следующий нарисованный данным актом мазок"
  (Следующий-мазок-по-фильтру
   (Квиоир-сост-Слой Сост)
   t
   (Квиоир-сост-D--нов Сост)
   :Запуск Запуск))


(defun Коллектив-зпт-вычислите-и-отправьте-изменения-раскраски (Слой Акт После-успешного-завершения-функ)
  "Запускает инкрементное (через цепочку сообщений одуванчика) выявление дельты раскраски и её отправку в tcl. Ф-я После-успешного-завершения-функ - без параметров"
  (perga
   (oduvanchik-ext:assert-we-are-in-oduvanchik-thread)
   (let Buffer (Слой-раскраски-Buffer Слой))
   (let О-р (oduvanchik-internals::buffer-otpravitelq-raskraski Buffer))
   ;; D значит, что это dcons, в к-ром мазок является данными. 
   (let D--нов nil)  ; мазок, который получен от функции разбора буфера, но ещё не отправлен в tcl
   (let С (MAKE-Квиоир-сост
           :Слой Слой
           :Акт Акт
           :Buffer Buffer
           :О-р О-р
           :D--нов D--нов
           :После-успешного-завершения-функ После-успешного-завершения-функ))

   (Квиоир-начало С))
  ;; дальнейшая раскраска будет происходить асинхронно, а нам пора возвращаться
  )

(defun Квиоир-начало (С)
  "Квиоир = Коллектив, вычислите и отправьте изменения раскраски"
  (perga
   (:@ with-slots (Слой Акт Buffer О-р D--стар D--нов) С)
   (След-нов С :Запуск t)
   (assert (eq (Акт-раскраски-Статус Акт) 'сар-Вычисляется))
   ;; это отстой, т.к. идёт без буферизации. Но надо чтобы хоть как-то эта гадость заработала сейчас - 
   ;; и так понятно, что всё нужно переделывать. 
   (unless (Слой-раскраски-Сплошной Слой)
     (Стереть-всю-раскраску-слоя О-р Акт))
   (clco::order-call-oduvanchik-from-itself
    (list 'Квиоир-продолжение С))))

(defun Квиоир-продолжение (С)
  "Квиоир = Коллектив, вычислите и отправьте изменения раскраски"
  (perga
   (:@ with-slots (Слой
                   Акт
                   Buffer О-р
                   D--стар
                   D--нов
                   После-успешного-завершения-функ
                   ) С) ; Квиоир-сост
   (dotimes (n ко-Размер-порции-отправителя-раскраски)
     (when (< (Акт-раскраски-Tick_count Акт) (oduvanchik-internals::buffer-tcl_tick_count Buffer))
       (Акт-зпт-ты-устарел Акт nil))
     (when (eq (Акт-раскраски-Статус Акт) 'сар-Отменён)
       ;; Нас грохнули - не нужно больше ничего делать. В буфере отправителя раскраски тоже ничего нет - оно стёрто в Акт-зпт-ты-устарел
       (return-from Квиоир-продолжение nil))
     ;(let М--D--нов (when D--нов (Д-СПИСОК:dcons-data D--нов)))
     ;(show-expr М--D--нов)
     (cond
      ((null D--нов)
       (funcall После-успешного-завершения-функ)          
       (return-from Квиоир-продолжение nil))
      (t
       (check-type (Д-СПИСОК:dcons-data D--нов) Мазок)
       (Отправитель-раскраски-зпт-отправь-мазок О-р D--нов)
       (След-нов С))))
   (clco::order-call-oduvanchik-from-itself
    (list 'Квиоир-продолжение С))))



