;; -*- encoding: utf-8; coding: utf-8; system :РАСКРАСКА-3; -*- 
;; Copyright (C) Денис Будяк 2015-2017
(budden-tools::in-readtable :buddens-readtable-a)

(def-merge-packages::! :РАСКРАСКА-3
  (:always t)
  (:use :cl :БУКВЫ-И-МЕСТА-В-ФАЙЛЕ :ЛЕКСЕМЫ-ЯРА :budden-tools :oduvanchik)
  (:import-from :oduvanchik-interface oduvanchik-interface:mark< oduvanchik-interface:mark=)
  (:import-from :oduvanchik oduvanchik:Квиоир-продолжение)
  (:export
   "
    ;; интерфейс для лексера.
    РАСКРАСКА-3::Эй-зпт-дайте-новый-акт-раскраски
    РАСКРАСКА-3::Акт-зпт-добавь-мазок
    РАСКРАСКА-3::Эй-зпт-раскраска-по-акту-завершена
    "))

