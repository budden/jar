# Запускается из одноимённого тест-1.lisp

namespace eval ::test_raskraski {
    
    proc run {} {
        set clcon_text [::edt::EditNewFile]
        $clcon_text insert insert {01234567890
АБВГДЕЁЖЗИ}

        after 300 "::test_raskraski::e2 $clcon_text"
    }
    
    proc e2 {clcon_text} {
        ::clcon_text::CallOduvanchikFunction $clcon_text \
                "ODUVANCHIK::VYZVATQ-FUNKCIYU-ZPT-OPREDELYONNUYU-NE-V-PAKETE-ODUVANCHIKA-COMMAND РАСКРАСКА-3::|Тест-раскраски-1-часть-2|" {{
            ::test_raskraski::e3 $clcon_text $EventAsList
            }}
    }
 
    proc e3 {clcon_text EventAsList} {
        $clcon_text Unfreeze
        puts "Tcl:Ok"
    }
}


