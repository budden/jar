(defsystem :ВЕРСИИ :version "0.2"
  :author "Roman Klochkov"
  :licence "MIT"
  :depends-on (:trivial-garbage :budden-tools)
  :serial t
  :components ((:file "версии")))
