(defsystem :ВЕРСИИ-ТЕСТ
  :version "0.2"
  :author "Roman Klochkov"
  :licence "MIT"
  :depends-on (:trivial-garbage :budden-tools :ВЕРСИИ)
  :serial t
  :components ((:file "версии-тест")))
