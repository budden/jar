;; -*- coding: utf-8; system: УПАКОВКА-МЕТОДОВ; -*- 


(def-merge-packages::! :УПАКОВКА-МЕТОДОВ
  (:always t)
  (:use :cl 
        :budden-tools 
        )
  (:import-from :kons
   kons:|{|
   kons:|}|
   )
  (:import-from :ЯРА-РОДОВЫЕ-ФУНКЦИИ)
  
  (:export
   "
    ")
  ;; Чтобы это сработало, мы выполняем этот файл дважды, в начале и в конце загрузки системы
  (:export
   #.(apply 'budden-tools:str++
            (mapcar
             (lambda (и)
               (budden-tools:Написать-экспорт-для-структуры `("УПАКОВКА-МЕТОДОВ" ,и)))
             '()))))
