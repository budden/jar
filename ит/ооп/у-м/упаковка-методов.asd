;; -*- coding: utf-8; -*- 
;; Copyright (C) Денис Будяк 2017
;; :ЯРА-ИСР
(asdf:defsystem :УПАКОВКА-МЕТОДОВ
  :description "Определение порядка вставки методов в родовую ф-ю"
  :serial t
  :depends-on (:budden-tools :ЯРА-ОБЪЕКТЫ)
  :components ((:file "пакет-упаковка-методов")
               (:file "упаковка-методов")
               (:file "пересоздать-пакет")
               ))

