;; -*- coding: utf-8; system: УПАКОВКА-МЕТОДОВ; -*- 

(named-readtables:in-readtable :buddens-readtable-a)
(in-package :УПАКОВКА-МЕТОДОВ)

(proclaim '(optimize (speed 3) (safety 0) (debug 0) (compilation-speed 0) (space 0)))
(declaim (optimize (speed 3) (safety 0) (debug 0) (compilation-speed 0) (space 0)))

(proclaim '(inline positivep))
(defun positivep (x) (declare (optimize (speed 0))) (> x 0))

(deftype positive-rational ()
  '(and rational (satisfies positivep))) 

(defparameter †Типы
  (coerce
   ;;; Некоторые типы из print-object (маловато их - 166 в образе, где я это пишу)
   ;;; Гораздо интереснее было бы провести полный анализ сигнатур, где ещё и объекты бывают разные
   (remove-duplicates
    (mapcar 'car
            (mapcar 'sb-mop:method-lambda-list
                    (sb-mop:generic-function-methods #'print-object))))
   'vector)
  ;; хотим, чтобы выстроилось именно в таком порядке, хотя 0 и 1 пересекаются,
  ;; а 3 - не в тему
  #+nil #(positive-rational integer rational string)
  )

(proclaim '(type simple-vector †Типы))
(declaim (type simple-vector †Типы))

(defparameter †Кого-поставить-раньше
  nil
  #+nil '((positive-rational integer)
          (integer string))
  "Первый элемент в каждом подсписке ставится раньше второго")

(defparameter †Ранг (length †Типы))

(proclaim '(type sb-int:index †Ранг))
(declaim (type sb-int:index †Ранг))

(defparameter †Размерность (list †Ранг †Ранг))

;; эл-т матриц - это набор масок:
;; 1 - Аi может пересекаться с Aj 
;; 2 - Ai является подтипом Aj
;; 4 - Ai стоит прямо перед Aj по правилам
;; 8 - Ai стоит перед Aj по порядку (по транзитивности подтипов или правил)
;; 16 - пометка для поиска

(deftype Элемент-матрицы-отношений () '(integer 0 31))

(defun Создать-нулевую-матрицу ()
  (make-array †Размерность
              :element-type 'Элемент-матрицы-отношений
              :initial-element 0))

(defparameter †М1
  (Создать-нулевую-матрицу))

(defun Заполнить-отношения-типов (А)
  "А - матрица знаний про пары вершин, изначально состоит из нулей"
  (declare (type (simple-array Элемент-матрицы-отношений (* *)) А))
  (perga
   (dotimes (и †Ранг)
     (let Аи (elt †Типы и))
     (dotimes (ж †Ранг)
       (when (< и ж) ; за каждую итерацию заполняем Аиж и Ажи, а диагональ правильно заполнить 0-ми
         (:lett Аиж Элемент-матрицы-отношений 0)
         (:lett Ажи Элемент-матрицы-отношений 0)
         (let Аж (elt †Типы ж))
         (:@ mlvl-bind
             (Аи-и-Аж-могут-пересекаться-ли П-Наверняка-ли)
             (sb-kernel::types-equal-or-intersect
              (sb-kernel::specifier-type Аи)
              (sb-kernel::specifier-type Аж)))
         (let Считаем-что-могут-пересекаться
           (or Аи-и-Аж-могут-пересекаться-ли
               (not П-Наверняка-ли)))
         (when Считаем-что-могут-пересекаться
           (_f logior Аиж 1)
           (_f logior Ажи 1)
           (:@ mlvl-bind (Аи-подтип-Аж-ли Аи-подтип-Аж-наверняка-ли)
               (subtypep Аи Аж))
           (when (and Аи-подтип-Аж-ли Аи-подтип-Аж-наверняка-ли)
             (_f logior Аиж 2))
           ; типы могут быть ещё и идентичными. Что делать???? Видимо, это ошибка,
           ; не может один и тот же тип иметь два разных метода
           (:@ mlvl-bind (Аж-п-Аи-ли Аж-п-Аи-н-ли)
               (subtypep Аж Аи))
           (when (and Аж-п-Аи-ли Аж-п-Аи-н-ли) (_f logior Ажи 2))
           (setf (aref А и ж) Аиж)
           (setf (aref А ж и) Ажи)))))))

(defun Добавить-инструкции (А Инструкции)
  (declare (optimize (speed 0)))
  (perga
   (dolist (Ин Инструкции)
     (:@ destructuring-bind (Ранний Поздний) Ин)
     (let и (position Ранний †Типы :test 'equalp))
     (let ж (position Поздний †Типы :test 'equalp))
     (_f logior (aref А и ж) 4))))

(defun Вычислить-частичный-порядок (А) ; https://ru.wikipedia.org/wiki/Алгоритм_Флойда_—_Уоршелла
  ;(declare (optimize (speed 3) (safety 0) (debug 0) (space 0) (compilation-speed 0)))
  (declare (type (simple-array Элемент-матрицы-отношений (* *)) А))
  (perga
   (dotimes (и †Ранг)
     (dotimes (ж †Ранг)
       (dotimes (к †Ранг)
         (let Аиж (aref А и ж))
         (cond
          ((= 1 (logand Аиж 8)) ; всё уже ок, ничего не делаем
           )
          ((and (/= 0 (logand (aref А и к) (+ 2 4 8)))
                (/= 0 (logand (aref А к ж) (+ 2 4 8))))
           (_f logior (aref А и ж) 8))))))))

(defun Проверить-отсутствие-циклов (А) ; maxx
  (declare (ignorable А))
  )

;(Заполнить-отношения-типов †М1)


(defun Разок ()
  (perga
   (let А (Создать-нулевую-матрицу))
   (Заполнить-отношения-типов А) ; это пока занимает основное время.
   ;(Добавить-инструкции А †Кого-поставить-раньше)
   (Вычислить-частичный-порядок А)
   (Проверить-отсутствие-циклов А)))

(time
 (dotimes (i 1)
   (Разок)))
   
;(print †М1)
;; правьмя отформатировать в TCL или хотя бы в HTML. 
