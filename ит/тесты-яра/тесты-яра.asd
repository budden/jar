;; -*- coding: utf-8; -*- 
;; Copyright (C) Денис Будяк 2015-2017

(defsystem :ТЕСТЫ-ЯРА
  :depends-on (:ЯРА-ЛИЦО 
               :ЯРА-ИСР ; вообще-то не должно зависеть от ИСР
               :ТЕСТЫ-ЯР-ЛЕКСИКОНЫ :ПАРСЕР-ЯРА-ТЕСТЫ)
  :serial t
  :components
  ((:static-file "тест-на-дымок.ярс")
   (:static-file "именованный-параметр.ярс")
   (:static-file "именованный-параметр-2.ярс")
   (:static-file "цикл-зпт-вернуть-из-зпт-переменная-до.ярс")
   (:static-file "псимв-и-цит.ярс")
   (:static-file "блок.ярс")
   (:static-file "ооп-точка-крышка.ярс")
   (:static-file "массивы.ярс")
   (:static-file "булевы-операции.ярс")
   (:file "тесты-транслятора")
   
   (:static-file "ярс-для-поиска-символа-яра-под-курсором.ярс")
   (:file "тесты-символа-яра-под-курсором")))
