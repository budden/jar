;; Copyright (C) Денис Будяк 2017

(named-readtables:in-readtable :buddens-readtable-a)

(defsystem :ОТКРЫТЬ-ИСХОДНЫЙ-ТЕКСТ-В-РЕДАКТОРЕ
  :depends-on (:budden-tools)
  :serial t
  :components ((:file "открыть-исходный-текст-в-редакторе")
               ))
