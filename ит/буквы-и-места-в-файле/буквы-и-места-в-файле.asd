;; Copyright (C) Денис Будяк 2017

(named-readtables:in-readtable :buddens-readtable-a)

(defsystem :БУКВЫ-И-МЕСТА-В-ФАЙЛЕ
  :depends-on (:budden-tools :ОТКРЫТЬ-ИСХОДНЫЙ-ТЕКСТ-В-РЕДАКТОРЕ)
  :description "Пб и Место-в-исходнике"
  :serial t
  :components ((:file "буквы-и-места-в-файле")
               (:file "пересчёт-мест-в-файле")
               ))
