;; -*- coding: utf-8; -*- 
;; Copyright (C) Денис Будяк 2015 - 2017

(named-readtables:in-readtable :buddens-readtable-a)

(defsystem :ТЕСТЫ-ЯР-ЛЕКСИКОНЫ
  :description "То же, что и пространства имён. См. оя::ЛЕКСИКОНЫ"
  :depends-on (:ЯР-ЛЕКСИКОНЫ :П-Я-ПОСТ-ЛЕКСЕР :ПАРСЕР-ЯРА-ТЕСТЫ :ЯР-СРЕДА-ВЫП)
  :serial t
  :components ((:file "тесты-яр-лексиконы")))


#|
Текущая задача: создать API согласно описанию языка и опереть его на пакеты:

- описать способ доступа к унаследованным пакетам лиспа
- описать способ указания лексикона в файле Яра (через -*- -*- )
- реализовать функцию поиска символа по имени символа и имени лексикона

|#
