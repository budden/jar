;; -*- coding: utf-8; system: CLCON--РЕЖИМ--MARKDOWN; -*- 
;; Copyright (C) Денис Будяк 2017
;; см. также:
;; ../../../lp/clcon/clcon-odu-commands-infrastructure.lisp 
(in-package :oduvanchik)
(named-readtables::in-readtable :buddens-readtable-a)

(defmode "Md" :major-p t :setup-function 'Установить-режим--Markdown)

(defun Установить-режим--Markdown (buffer)
  (declare (ignore buffer))
  ;;
  )

(dolist (Тип '("md"))
  (eval
   `(define-file-type-hook (,Тип) (buffer type)
      (declare (ignore type))
      (setf (buffer-major-mode buffer) "Md"))))
   
(defun Преобразовать--Markdown--в--html--и-открыть (Имя-файла load-p &rest options)
  (declare (ignore load-p options))
  (budden-tools:perga
   (let* Выходное-имя-файла (make-pathname :name (pathname-name Имя-файла) :type "html" :defaults (cl-user::putq-otnositelqno-kornya-yara "пляж/какой-то.файл")))
   (let Исходник (budden-tools:read-file-into-string Имя-файла))
   (let Результат
     (concatenate
      'string
      "<!DOCTYPE html lang=\"ru\">
       <html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
       <title>Md</title>
       </head>
       <body>
       "
      (ОЯ-ИНФРАСТРУКТУРА::md-here Исходник)
      "
       </body>
       </html>"))
   (budden-tools:save-string-to-file Результат Выходное-имя-файла)
   (clco::open-url Выходное-имя-файла)))


(defhvar "Compile And Load Buffer File Function" ; budden's compile-and-load-buffer-file-function
  "Function's arglist is (filename load-p &rest options), options are passed to compiler function, return value is discarded"
  :value 'Преобразовать--Markdown--в--html--и-открыть ; 
  :mode "Md")

;(defhvar "Recompute Syntax Marks Function"
;  "Вызывается из ODUVANCHIK-INTERNALS::RECOMPUTE-SYNTAX-MARKS"  :value 
;  'oi::dummy-recompute-syntax-marks
;  :mode "Яр"
;  )


(defun Маркдаун-Посчитать-число-разделителей-колонок (line &key В-строке-под-шапкой)
  "Возвращает число границ колонок (= число колонок -1) или nil. Если В-строке-под-шапкой, то дополнительно проверяет, что строка имеет вид
 `----|---|--...--|---`, и при нарушении этого условия возвращает nil"
  (perga-implementation:perga
   (let Стр (line-string line))
   (and
    (budden-tools:implies
     В-строке-под-шапкой
     (every (lambda (ч) (find ч "|-")) Стр))
    (count-if (lambda (ч) (eql ч #\|)) Стр))))

(defun Маркдаун-Получить-позиции-разделителей (Строка-шапки)
  (perga-implementation:perga
   (let Стр (line-string Строка-шапки))
   (let Рез nil)
   (dotimes (И (length Стр))
     (when (eql (elt Стр И) #\|)
       (push И Рез)))
   (nreverse Рез)))

(defun Маркдаун-Сгенерировать-новую-строку-под-шапкой (Строка-под-шапкой Позиции-разделителей)
  (perga-implementation:perga
   (let Стр 
     (with-output-to-string (По)
       (perga-implementation:perga
        (let Прошлый-П nil)
        (dolist (П Позиции-разделителей)
          (cond
           ((null Прошлый-П)
            (format По "~A|" (make-string П :initial-element #\-))
            (setf Прошлый-П П))
           (t
            (let Дл (- П Прошлый-П 1))
            (setf Прошлый-П П)
            (format По "~A|" (make-string Дл :initial-element #\-)))))
        (format По "-------~%"))))
   (move-mark (current-point) (mark Строка-под-шапкой 0))
   (kill-line-command 1)
   (oi::insert-string (current-point) Стр)
   ))

(defun Маркдаун-Выравнять-текущую-строчку (Позиции-разделителей)
  (perga-implementation:perga
   (let line (mark-line (current-point)))
   (let Стр1 (line-string line))
   (let Стр1-Кусочки (split-sequence:split-sequence #\| Стр1))
   (let Стр1-Кусочки-обкромсанные
     (mapcar (lambda (Кус) (string-trim " " Кус))
             Стр1-Кусочки))
   (let Формат "")
   (loop
     (unless Позиции-разделителей (return))
     (let П (pop Позиции-разделителей))
     (setf Формат (budden-tools:str++ Формат " ~A"))
     (setf Формат (budden-tools:str++ Формат "~" П ",1T|")))
   (setf Формат (budden-tools:str++ Формат " ~A~%"))
   (let Нов (apply #'format nil Формат Стр1-Кусочки-обкромсанные))
   (:@ with-mark ((М (current-point) :right-inserting)))
   (kill-line-command 1)
   (oi::insert-string (current-point) Нов) ; видимо, мы попадаем в конец строки (в начало следующей строки)
       ; но из документации это не следует - неясно, является ли current-point право или лево вставляющей.
       ; поэтому мы сохранили позицию и возвращаемся к ней
   (move-mark (current-point) М)
   ))
  
;; Возможно, это надо было делать с помощью ODUVANCHIK::FILTER-REGION-COMMAND
(defcommand "Indent Markdown Table" (п)
  "Выровнять таблицу маркдаун"
  "Выровнять таблицу маркдаун. Ширины колонок берутся из первой строки и мы должны на ней стоять при вызове команды"
  (perga-implementation:perga Тело
   (:@ oi::modifying-buffer (current-buffer))
   (let point (current-point))
   (let Строка-шапки (mark-line point))
   ;; 1. убедиться, что мы стоим на первой строке
   (let Строка-под-шапкой (line-next Строка-шапки))
   (let Число-разделителей-колонок
     (and Строка-под-шапкой
          (Маркдаун-Посчитать-число-разделителей-колонок Строка-под-шапкой :В-строке-под-шапкой t)))
   (unless Число-разделителей-колонок
     (editor-error "Для выравнивания таблицы Markdown нужно стоять в строке с заголовками. Таблица будет выравнена по этой строке. Строка с заголовками определяется тем, что под ней находится строка - разделитель вида `----|---|--...--|---` с таким же числом колонок"))
   (let Позиции-разделителей (Маркдаун-Получить-позиции-разделителей Строка-шапки))
   (:@ with-mark ((К (current-point))))
   (Маркдаун-Сгенерировать-новую-строку-под-шапкой Строка-под-шапкой Позиции-разделителей)
   (move-mark (current-point) К) ; снова встанем в шапку
   (line-offset (current-point) 1 0) ; встаём в начало строки под шапкой и готовимся шагать по строкам
   (loop
     (unless (line-offset (current-point) 1 0)
       ; больше некуда шагать - значит, наша работа закончена
       (return-from Тело nil))
     (let line (mark-line (current-point)))
     (unless (= Число-разделителей-колонок
                (Маркдаун-Посчитать-число-разделителей-колонок line))
       ; что-то не так - число разделителей не совпадает - считаем, что таблица закончилась
       (return-from Тело nil))
     ; current-point должна снова оказаться в начале этой же строки
     (Маркдаун-Выравнять-текущую-строчку Позиции-разделителей))
   ))
