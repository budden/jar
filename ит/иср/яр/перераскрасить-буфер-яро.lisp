;; -*- coding: utf-8; system :ЯРА-ИСР; -*- 
;; Copyright (C) Денис Будяк 2015-2017
;; см. также clcon--режим-яр.lisp , ../../lp/oduvanchik/src/exp-syntax.lisp , ..\..\lp\clcon\clcon-odu-commands-infrastructure.lisp, раскраска-яра-лексером.lisp
(budden-tools::in-readtable :buddens-readtable-a)

(in-package :oduvanchik) 

(defun ПЕРЕРАСКРАСИТЬ-БУФЕР-ЯРО (buffer)
  "См. также ПЕРЕРАСКРАСИТЬ-БУФЕР. Отличие:
  1. Синхронно вызовем recompute-line-tags-light, чтобы обновить номера строк
  2. Лексер прямо сейчас полностью разберёт весь файл, синхронно. 
  3. Запустим процесс фоновой отправки с помощью РАСКРАСКА-3::Акт-раскраски"
  (perga-implementation:perga all
    (oduvanchik-ext:assert-we-are-in-oduvanchik-thread)
    (assert buffer () "Вызов перераскраски буфера для удалённой строки нужно было предотвратить ниже по стеку")
    (unless (member buffer *buffer-list*) ; буфер удалён
      (return-from all nil))
    (assert (eq (oi::syntax-highlight-mode buffer) :send-highlight-after-recomputing-entire-buffer))
    (:@ bt:with-recursive-lock-held (oi::*INVOKE-MODIFYING-BUFFER-LOCK*))
    (oi::recompute-line-tags-light buffer)
    ;; ищи "Recompute Syntax Marks Function" для режима данного буфера. например,
    (let line (oi:first-line-of-buffer buffer))
    ;; РАСКРАСКА-ЯРА-ЛЕКСЕРОМ::Recompute-syntax-marks--специально-для-Яра
    (let fn (oi::recompute-font-marks-fn-for-line line))
    ;; для режима Яра эта функция берёт на себя дальнейшую работу по 
    ;; отправке марок в tcl с помощью цепочки событий, а также отслеживает 
    ;; своё состояние (умирает, если устарела)
    (when line
      (funcall fn line nil))
    ))
    