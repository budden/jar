;; -*- coding: utf-8; system :ЯРА-ИСР; -*- 
;; Copyright (C) Денис Будяк 2015-2017
;; см. также
;; clcon--режим-яр.lisp , ../../lp/oduvanchik/src/exp-syntax.lisp ,
;; ../../lp/clcon/clcon-odu-commands-infrastructure.lisp , раскраска-яра-лексером.lisp , раскраска-3.lisp 
(in-package :oduvanchik)
(named-readtables::in-readtable :buddens-readtable-a)

(defmode "Яр" :major-p t :setup-function 'установить-режим-Яр)

(defun установить-режим-Яр (buffer)
  (declare (ignore buffer))
  ;;
  )

(dolist (тип '("я" "яаи" "яр" "ярс"))
  (eval
   `(define-file-type-hook (,тип) (buffer type)
      (declare (ignore type))
      (setf (buffer-major-mode buffer) "Яр"))))
   
(defun компилировать-файл-Яр-из--tcl (filename load-p &rest options)
  "Частично родственна CLCO::COMPILE-LISP-FILE-FOR-TCL"
  (declare (ignore load-p options))
  ;; не осиливаем компилировать файл Яра целиком, например, из-за параметр. типов и родовых ф-й. Извините :)
  ;(break)
  (perga-implementation:perga
   (let compilation-result 
     (SWANK::COLLECT-NOTES
      (lambda () 
        (ТРАНСЛЯТОР-ЯРА-В-ЛИСП:Выполни-скрипт-Яра filename :в-ИСР t))))
   (let success (swank::compilation-result-successp compilation-result))
   (let notes (swank::compilation-result-notes compilation-result))
   (let serial 0)
   ;; Не будем выводить, т.к. всегда выглядит как неуспех
   (format t "~&Результат загрузки скрипта Яр: ~S~%" (if success "Успех" "Неуспех"))
   (when notes
     (clco:eval-in-tcl (format nil "::erbr::SwankBrowseErrors1 ~A" (CLCO::CONVERT-OBJECT-TO-TCL compilation-result)))
     (dolist (note notes)
       (clco:eval-in-tcl (clco::calc-details-code note (incf serial)))
       ))))

(defhvar "Compile And Load Buffer File Function" ; budden's compile-and-load-buffer-file-function
  "Function's arglist is (filename load-p &rest options), options are passed to compiler function, return value is discarded"
  :value 'компилировать-файл-Яр-из--tcl ; 
  :mode "Яр")

(defhvar "Recompute Syntax Marks Function"
  "Вызывается из ODUVANCHIK-INTERNALS::RECOMPUTE-SYNTAX-MARKS . См. oi::old-lisp-recompute-syntax-marks"  :value 
  'РАСКРАСКА-ЯРА-ЛЕКСЕРОМ:Recompute-syntax-marks--специально-для-Яра
  :mode "Яр"
  )

(defhvar "Syntax Highlight Mode" "Неинкрементная раскраска" :value :send-highlight-after-recomputing-entire-buffer :mode "Яр")

;; отключаем раскраску скобок, т.к. она завязана на старый алгоритм отправки раскраски
(defhvar "Highlight Open Parens"
  "When non-nil, causes open parens to be displayed in a different font when
   the cursor is directly to the right of the corresponding close paren."
  :value nil
  :mode "Яр")


(defun Перебрать-все-строки ()
  (let* ((b (current-buffer))
         (m (buffer-start-mark b))
         (l (mark-line m))
         (count 0))
    (do ((ll l (line-next ll)))
        ((null ll) count)
      (incf count (length (oi::line-marks ll))))))

(defmethod КАРТЫ-ИСХОДНИКОВ-ЛИЦО:SLO-SOURCE-В-ИМЯ-ФАЙЛА ((slo-source БУКВЫ-И-МЕСТА-В-ФАЙЛЕ:Ссылка-на-источник))
  (and
   (eq (БУКВЫ-И-МЕСТА-В-ФАЙЛЕ:Ссылка-на-источник-Тип slo-source) 'БУКВЫ-И-МЕСТА-В-ФАЙЛЕ:Тсни-файл)
   (БУКВЫ-И-МЕСТА-В-ФАЙЛЕ:Ссылка-на-источник-Данное slo-source)))

(defcommand "Nayiti iskhodnik po karte" (p) "Находит исходники с помощью КАРТЫ-ИСХОДНИКОВ , т.е. скачет от лиспа к Яру, если для данного места это возможно. Команда - для отладки пошагового отладчика . См. также '.апр srcpl', '.иия {My find source}'" ""
  (declare (ignorable p))
  (perga-implementation:perga тело
    (let file (buffer-pathname (current-buffer)))
    (unless file
      (message "В текущем буфере нет файла")
      (return-from тело nil))
    (let pnt (current-point))
    (let offset-1-based
      (+ (editor-budden-tools:real-point-offset-0-based pnt)
         clco::+Нужно-добавить-к-смещению-считаемому-от-0-для-получения-смещения-считаемого-от-1+))
    (|Перейти-к-первому-месту-на-карте-исходников| file offset-1-based)))

(defcommand "Nayiti iskhodnik zpt porozhdyonnyyyi iz ehtogo mesta" (p) "Находит порождённые отсюда исходники, т.е. скачет от Яра к Лиспу" ""
  (declare (ignorable p))
  (perga-implementation:perga тело
    (let file (buffer-pathname (current-buffer)))
    (unless file
      (message "В текущем буфере нет файла")
      (return-from тело nil))
    (let pnt (current-point))
    (let offset-1-based
      (+ (editor-budden-tools:real-point-offset-0-based pnt)
         clco::+Нужно-добавить-к-смещению-считаемому-от-0-для-получения-смещения-считаемого-от-1+))
    (let Ссылка-на-исходник
      (БУКВЫ-И-МЕСТА-В-ФАЙЛЕ:MAKE-Ссылка-на-источник :Тип 'БУКВЫ-И-МЕСТА-В-ФАЙЛЕ:Тсни-файл :Данное file))
    (let Места
      (КАРТЫ-ИСХОДНИКОВ-ТЕЛО::Что-порождено-из-этого-места Ссылка-на-исходник offset-1-based))
    (Создать-код-tcl-для-перехода-к-определениям Места)
    ;(|Перейти-к-первому-месту-на-карте-исходников| file offset-1-based)
    ))


(defun Создать-код-tcl-для-перехода-к-определениям (Места &key Скакнуть-от-Лиспа-к-Яру)
  "Аналог CLCO:Обслужить-команду-поиска-связей-символа 
Места - список БУКВЫ-И-МЕСТА-В-ФАЙЛЕ:Место-в-исходнике . 
Функция возвращает строку - код tcl, к-рый либо печатает в консоль список мест, куда можно перейти, 
   либо переходит на единственное место, куда можно перейти"
  (perga-implementation:perga
   (let l (length Места))
   (:@ with-output-to-string (ou))
   (case l
     (0 (clco:print-just-line ou (format nil "Я не знаю о коде, порождённом из этого места (попробуйте встать на первую букву ярой формы - там должно работать)")))
     (t
      (when (> l 1)
        (clco:write-code-to-show-console ou))
      (let Сч 0)
      (dolist (Место Места)
        (cond
         ((= l 1)
          (clco:write-code-to-pass-to-loc ou Место :Скакнуть-от-Лиспа-к-Яру Скакнуть-от-Лиспа-к-Яру))
         (t
          (let link-text (format nil "Порождённый код ~A" Сч))
          (incf Сч)
          (clco:write-one-dspec-and-location link-text Место ou :Скакнуть-от-Лиспа-к-Яру Скакнуть-от-Лиспа-к-Яру))))))
   (when (> l 1)
     (clco:write-code-to-see-console-end ou))))

(defcommand "YAr nayiti iskhodnik" (p) "Находит исходник в Яре" "Не делает сам, а создаёт код для дальнейшего выполнения"
  (perga-implementation:perga тело
   (let Имя (ЯР-ПАРСЕМА-ПО-МЕСТУ:Символ-яра-под-курсором (oi:current-point)))
   (flet Неудача (format &rest args)
     (return-from тело
                  (with-output-to-string (Код-вывода-неудачи)
                    (clco:print-just-line Код-вывода-неудачи
                                           (apply 'format nil format args))
                    (clco:write-code-to-see-console-end Код-вывода-неудачи)
                    (clco:write-code-to-show-console Код-вывода-неудачи)
                    )))

   (flet Есть-символ-яра-попробовать-найти-символ-лиспа (Что-искали Символ-Яра)
     (cond
      ((eql Символ-Яра 'ЯР-ЛЕКСИКОНЫ:Символ-не-найден)
       (Неудача "Не найден символ Яра, обозначаемый ~S" Что-искали))
      (t
       (let Символ-лиспа
         (ЯР-ЛЕКСИКОНЫ:Символ-Лисповый-символ Символ-Яра))
       (cond
        ((null Символ-лиспа)
         (Неудача "Символу Яра ~S не соответствует символ лиспа (или соответствует символ NIL)" Символ-Яра))
        (t Символ-лиспа)))))
   
   (let Символ-лиспа
     (perga-implementation:perga
      (etypecase Имя 
        (null
         (Неудача "В этом месте нет символа Яра, или файл содержит ошибки грамм. разбора"))
        (ЛЕКСЕМЫ-ЯРА:Лексема
         ;; лексема, т.к. это комментарий или иное место, где символа нет
         (let Символ-Яра
           (ЯР-ЛЕКСИКОНЫ:Найти-символ-по-лексеме Имя))
         (Есть-символ-яра-попробовать-найти-символ-лиспа Имя Символ-Яра))
        (П-Я-ГРАМОТЕЙ:П-Имя
         (let Символ-Яра (П-Я-ГРАМОТЕЙ:П-Имя-Символ-яра Имя))
         (Есть-символ-яра-попробовать-найти-символ-лиспа Имя Символ-Яра)))))
   (when Символ-лиспа 
     (clco:server-lookup-definition Символ-лиспа))))

(defcommand "YAr avtodopolnenie" (p) "Автодополнение" "Автодополнение"
  (declare (ignorable p))
  (perga-implementation:perga тело
   ;; получаем исходный текст, который нужно завершить
   (let Имя (ЯР-ПАРСЕМА-ПО-МЕСТУ:Символ-яра-под-курсором (oi:current-point) :Для-автодополнения t))
   (flet Неудача (format &rest args)
     (apply 'message format args)
     (return-from тело nil))
   (etypecase Имя 
     (null
      (Неудача "В этом месте нет символа Яра, или файл содержит ошибки грамм. разбора"))
     (ЛЕКСЕМЫ-ЯРА:Лексема
      ;; лексема, т.к. это комментарий или иное место, где распарсенного символа нет
      (let Стр-длина (length (ЛЕКСЕМЫ-ЯРА:Лексема-Текст Имя nil)))
      (let Список-дополнений 
        (ЯР-АВТОДОПОЛНЕНИЕ:Вывести-продолжения-по-лексеме-символа Имя))
      (Предложить-пользователю-список-продолжений-и-принять-его-выбор Список-дополнений Стр-длина))
     ; П-Я-ГРАМОТЕЙ:П-Имя сюда не должно было попасть по построению 
     )))
   
(defun |Перейти-к-первому-месту-на-карте-исходников| (file offset-1-based)
  "Находит для данного места карту исходников и открывает это место. Если места нет, то за это отвечает CLCO::Скакнуть-от-Лиспа-к-Яру. offset в буквах, считая от 1 (КАРТЫ-ИСХОДНИКОВ-ТЕЛО::сико-ПБу , но такого определения нет, ищи КАРТЫ-ИСХОДНИКОВ-ТЕЛО::Система-координат )"
  (perga-implementation:perga
   (:@ multiple-value-bind (target-file target-offset-1-based)
       (CLCO::Скакнуть-от-Лиспа-к-Яру file offset-1-based))
   (editor-budden-tools:goto-offset
    target-file
    target-offset-1-based
    :kill-buffer nil)))

