; -*- coding: utf-8; system :ТРАНСЛЯТОР-ЯРА-В-ЛИСП ; -*-
;; Copyright (C) Денис Будяк 2015-2017
;; Преобразование распарсенного кода Яра в компилируемый код лисп.
;; оригинал: c:/yar/ит/yar/yar-to-lisp.lisp
;; вырезано много ф-й, в т.ч вывод свойств

;; В основном мы пытаемся пройти один раз по дереву, собирая несколько назначений. 
;; Каждое назначение может быть потоком для вывода в файл (несколько файлов) или в строку. 
;; Затем мы запускаем "сборку", когда фрагменты склеиваются. Также у нас есть основной поток
;; †Вы, к-рый считается основным. Остальные - вспомогательные. 


(named-readtables:in-readtable :buddens-readtable-a)
(in-package :ТРАНСЛЯТОР-ЯРА-В-ЛИСП)

(defmacro С-отключенным-режимом-ходьбы (&body body)
  "В режиме ходьбы SBCL норовит шагнуть вверх, в системный код. Чтобы это предотвратить, запуск кода Яра должен происходить в области действия данной конструкции"
  #+sbcl
  `(sb-impl::with-stepping-disabled ,@body)
  #-sbcl
  `(progn
     ; (break "С-отключенным-режимом-ходьбы не реализован для этого лиспа")
     ,@body))

(defgeneric ВывестиСвойства (э)
 ; выводит свойства для объектов Процедура или для всех процедур в объекте Модуль. 
 ; заносит декларации о выведенных свойствах внутрь объекта или объектов Процедура
  )


#| Доопределить Парсему-в-лисп для :
  (defstruct СписокПараметров Начало Параметры Конец)
  (defstruct Параметр Знач Имя = ЗначениеПоУмолчанию Конец)
  |#

(defparameter *last-translate-time-function-names* nil "Стаскиваем для последующего расследования")

(defun Генерировать-файл-лисп-из-списка-форм-верхнего-уровня (П-Файл &key output-stream output-file
                        (package-designator :лвг)
                        (create-defpackage-form nil)
                        if-exists
                        (ВывестиСвойства t)
                        Печ-источник-исходника
                        Для-ЦЧВП)
  (declare (ignore ВывестиСвойства))
  (perga
   (:lett package-designator symbol (keywordize package-designator))
   ;(format t "~% Пакет ~S" package-designator)
   (let *lisp-output-package-designator* package-designator)
   
   ;;(when ВывестиСвойства
   ;;  (ВывестиСвойства parsed))
   (let user-wants-string (eq output-stream :string))
   (let my-output-stream 
     (cond (user-wants-string
            (make-string-output-stream))
           (output-stream output-stream)
           (output-file
            (perga
             (let naked-stream
               (apply
                #'open output-file :direction :output
                :external-format :utf-8
                :if-does-not-exist :create
                (dispatch-keyargs-simple if-exists)))
             (make-instance 'ПОТОКИ-ЗПТ-СЧИТАЮЩИЕ-БУКВЫ-СТРОКИ-И-КОЛОНКИ:Считающий-выходной-поток-литер
                            :Поток naked-stream)))
           (t *standard-output*)))
   (let Каноническая-имястрока-файла (namestring (cl-fad:canonical-pathname output-file)))
   (when output-file
     (КАРТЫ-ИСХОДНИКОВ-ЛИЦО:Удалить-карты-для-генерируемого-файла Каноническая-имястрока-файла)
     (КАРТЫ-ИСХОДНИКОВ-ЛИЦО:get-stream-location-map-delegate my-output-stream :if-not-exists :create))
   (unwind-protect
       (perga
        (let †Вы my-output-stream)
        (let *translate-time-function-names* nil)
        (let *translate-time-class-names* nil)
        
        (let *Реестр-ссылок-на-источник* nil)
        (let *Номер-следующего-источника* 0)

        (let *print-pretty* nil)
        (let *print-escape* t)
        (let *readtable* (copy-readtable nil))

        (ФВы "; -*- coding: utf-8; -*- 
 ; сгенерировано транслятором Яр-Лисп из ~S~%" Печ-источник-исходника)

        (let *package* †Никакой-пакет)

        (when package-designator
          (when create-defpackage-form
            (format †Вы "~& ;; Определение пакета создано ~S
~S
;; здесь был вызов ДекларацииС1ОчиститьДляПакета
"
                    'code-for-defpackage-form
                    (code-for-defpackage-form package-designator)
                    ;`(YAR-LISP-RUNTIME:ДекларацииС1ОчиститьДляПакета ,package-designator)
                    ))
          (ФВы "~%~S~%~S~%"
                  `(in-package ,package-designator)
                  `(in-readtable :tch-yar-lisp)))

        ;; запрет отключаем, т.к. он пока что толком не работает
        ;;(Сгенерировать-запрет-неявного-сужения-типа)
        (Сгенерировать-декларации-оптимизации)
        
        (when Для-ЦЧВП
          (ФВы "~&(cl:defun ЯР-С-ВЫП:|Текущая-форма-ЦЧВП| () ~%")
          (Увеличить-отступ-на 1))

        (k:dolist
         (П (П-Файл-П-Предложения-верхнего-уровня П-Файл))
         (Парсему-в-лисп П))

        (when Для-ЦЧВП
          (Увеличить-отступ-на -1)
          (ФВы ")")
          (ФВы "~%")
          (ФВы "(ЯР-С-ВЫП:|Готов-результат-ЦЧВП| (ЯР-С-ВЫП:|Текущая-форма-ЦЧВП|))")
          )
       
        (setf *last-translate-time-function-names* *translate-time-function-names*)
        )
     (when output-file
       (close my-output-stream)
       ; всё это происходит в cleanup форме от unwind-protect. А что, если ошибка? Скоро узнаем... 
       (КАРТЫ-ИСХОДНИКОВ-ЛИЦО:l/pass-from-stream-to-file my-output-stream)
       (КАРТЫ-ИСХОДНИКОВ-ЛИЦО:Сохранить-карту-для-файла-в-файл Каноническая-имястрока-файла)
       ))
   (cond (user-wants-string
          (get-output-stream-string my-output-stream))
         (t (values)))
   ))
   

(defun Распарсить-источник (Источник)
  "ПРАВЬМЯ - перенести в П-Я-ГРАМОТЕЙ, т.к. трансляция тут не при чём. Возможно, нужно её привести в соответствие с более богатой опциями ф-ей П-Я-ЛЕКСЕР:Парсер-Яра-с-вызовом-событий"
  (perga
   ;(let П-Я-ЛЕКСЕР:†Искать-символы-Яра-в-пост-лексере-ли t)
   (:@ П-Я-ГРАМОТЕЙ::С-состоянием-грамотея)
   (Парсить-файл (П-Я-ЛЕКСЕР::Прочитать-свернуть-и-сгруппировать Источник))))

(defun code-for-defpackage-form (package-designator)
  `(def-merge-packages::!
    ,package-designator
    (:always t)
    ;(:use :yar-lisp-runtime :yar-lisp :yar-lisp-library)
    (:use "ИЗ-ЛИСПА-В-ЯР")
    (:import-from :budden "^")
    (:import-from :editor-hints.named-readtables :in-readtable)
    ;(:custom-token-parsers yar-lisp-runtime::convert-!)
    )
  )

#| ПРАВЬМЯ - это отключено, поскольку мы так и не сумели сделать нормальный запрет 
   неявного сужения типа для SBCL. А для CCL его и подавно нету
 (defun Сгенерировать-запрет-неявного-сужения-типа ()
  (perga
   (ФВы "~%;; Сгенерировано ~S~%~S~%~S~%~S~%"
        'Сгенерировать-запрет-неявного-сужения-типа
        '(proclaim '(optimize (safety 2)))
        '(declaim (optimize (safety 2)))
        '(eval-when (:compile-toplevel)
           (setq sb-c::|*Запретить-неявное-сужение-типа*| t))))) |#


(defun Сгенерировать-декларации-оптимизации ()
  (perga
   ;(assert *print-readably*)
   (format †Вы "~%;; Сгенерировано ~S~%~S~%"
           'Сгенерировать-декларации-оптимизации
           (ЯР-СК-ЛСП:Декларации-оптимизации-кода-Яра ЯР-СК-ЛСП:†Цель-оптимизации-кода-Яра))))
         
