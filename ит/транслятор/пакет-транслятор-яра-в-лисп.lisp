; -*- coding: utf-8; system :ТРАНСЛЯТОР-ЯРА-В-ЛИСП ; -*-


(named-readtables:in-readtable :buddens-readtable-a)

;; Кусок для рантайма
(def-merge-packages::! :ИСТ
  (:always t)
  (:use)
  (:export "ССЫЛКА-НА-ИСТОЧНИК-В-РЕЕСТРЕ" "Ъ"))


(def-merge-packages::!
 :ТРАНСЛЯТОР-ЯРА-В-ЛИСП
 (:use :cl ; :budden 
  :russian-budden-tools 
  :budden-tools 
  ;:case1  
  ;:ystok.meta
  :cl-ppcre ; :yar
  :anaphora
  :svfirst
  ;:yar-lexem
  ;:lexer-yar
  ;:yar-special-characters
  ;:yar-keywords
  ;:yar-ast-types
  ;:yar-ast-selected-members
  :П-Я-ГРАМОТЕЙ
  :ЛЕКСЕМЫ-ЯРА
  :КЛАССЫ-ЛЕКСЕМ-ЯРА
  :ИСТ
  :ЯР-С-ВЫП
  :ЯР-СК-ЛСП
  )
 (:import-from :kons kons:|{| kons:|}|)
 ;(:shadowing-import-from :yar-special-characters	",")
 (:import-from :budden budden::iter)
 (:import-from :budden-tools budden-tools:def-symbol-readmacro)
 (:import-from :alexandria.0.dev alexandria.0.dev:when-let)
 (:import-from :КАРТЫ-ИСХОДНИКОВ-ЛИЦО)
 (:export 
  "
   ТРАНСЛЯТОР-ЯРА-В-ЛИСП::Распарсить-источник 
   ТРАНСЛЯТОР-ЯРА-В-ЛИСП::Выполни-скрипт-Яра
   ТРАНСЛЯТОР-ЯРА-В-ЛИСП::Выполнить-строку-Яра-из-ЦЧВН
   ТРАНСЛЯТОР-ЯРА-В-ЛИСП::Ошибка-трансляции ; функция
   ТРАНСЛЯТОР-ЯРА-В-ЛИСП::Поддиректория-я-л
   "
  )
 ; (:shadowing-import-from :meta-parse-firebird "." "(" ")" "[" "]" ":")
 (:local-nicknames :bu :budden-tools 
  :b :budden
  ;:mpf :meta-parse-firebird
  :al :alexandria.0.dev
  )
 (:custom-token-parsers (:packages :budden))
 )
