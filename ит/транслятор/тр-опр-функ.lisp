; -*- coding: utf-8; system :ТРАНСЛЯТОР-ЯРА-В-ЛИСП ; -*-
;; Copyright (C) Денис Будяк 2015-2017
;; Преобразование распарсенного кода Яра в компилируемый код лисп.
;; оригинал: c:/yar/ит/yar/yar-to-lisp.lisp
;; вырезано много ф-й, в т.ч вывод свойств

;; В основном мы пытаемся пройти один раз по дереву, собирая несколько назначений. 
;; Каждое назначение может быть потоком для вывода в файл (несколько файлов) или в строку. 
;; Затем мы запускаем "сборку", когда фрагменты склеиваются. Также у нас есть основной поток
;; †Вы, к-рый считается основным. Остальные - вспомогательные. 


(named-readtables:in-readtable :buddens-readtable-a)
(in-package :ТРАНСЛЯТОР-ЯРА-В-ЛИСП)

;;; потоки для вывода частей определения
;; декларации внутри тела ф-ии до perga
(defvar †Вых-декларации-типов-аргтов)
;; декларации перед определением ф-ии
(defvar †Вых-декларации-функции)
;; определение функции пишется сюда
(defvar †Вых-опр-функ) 

;; сюда накапливаем имена параметров по-уклю (byref)
(defvar †Есть-параметры-по-уклю)
(defvar †Вых-параметры-по-уклю)

(defun ДекларироватьПараметрВЛисп (П-Формальный-параметр-функции П-Опр-функ НомерПараметра ВременноеИмя)
  "См. в старом Яре ДекларироватьПараметрВЛисп - там есть обработка необязательных параметров и передачи по значению/ссылке"
  (declare (ignore П-Опр-функ НомерПараметра ВременноеИмя))
  (perga
   (let Арг П-Формальный-параметр-функции)
   ;; наверняка неправильно - нужно какой-то более умный символ?
   (let Имя (П-Формальный-параметр-функции-Л-Имя Арг))

   ;; значение по умолчанию
   (let П-= (П-Формальный-параметр-функции-П-А-= Арг))

   (cond
    (П-= ; выводим в виде (арг знач-по-умолч)
     (ФВы "(")
     (Лексему-в-лисп Имя)
     (ФВы " ")
     (Парсему-в-лисп (П-Атрибут-и-значение-П-Значение П-=))
     (ФВы ")"))
    (t ; обычный аргумент - выводим просто его имя
     (Лексему-в-лисп Имя)))

   (Вывести-способ-передачи-параметра Арг)

   ;; Декларация ftype ограничивает типы факт. параметров, а declare внутри ф-ии
   ;; ограничивает присваивание параметрам
   ; (Киэвл п^Конец)

   (let П-Тип (П-Формальный-параметр-функции-П-А-Тип Арг))
   (let П-Тип-П-Значение
     (and П-Тип (П-Атрибут-и-значение-П-Значение П-Тип)))
   (when П-Тип
     (let †Вы †Вых-декларации-типов-аргтов)
     (Кнотст)
     (ФВы "(cl:declare (cl:type ")
     (Парсему-в-лисп П-Тип-П-Значение)
     (ФВы " ")
     (Лексему-в-лисп Имя)
     (ФВы "))"))
   (let †Вы †Вых-декларации-функции)
   (ecase (П-Формальный-параметр-функции-Доп-Випарли Арг)
     (Випарли-именованный
      (ФВы "(")
      (Лексему-символ-в-лисп Имя :Как-Keyword t)
      (ФВы " ")
      (cond
       (П-Тип
        (Парсему-в-лисп П-Тип-П-Значение))
       (t
        (ФВы "cl:t ")))
      (ФВы ")"))
     (Випарли-позиционный
      (Парсему-в-лисп П-Тип-П-Значение)))))
      
(defun Вывести-способ-передачи-параметра (Арг)
  "Арг - типа П-Формальный-параметр-функции. См. также Закончить-вывод-способа-передачи-параметров"
  (perga
   (let Асп (П-Формальный-параметр-функции-П-А-Способ-передачи Арг))
   (when Асп
     (let СЯ (П-Имя-Символ-яра (П-Атрибут-и-значение-П-Значение Асп)))
     (let СЛ (ЯР-ЛЕКСИКОНЫ:Символ-Лисповый-символ СЯ))
     (ecase СЛ
       (ЛВГ:по-уклю
        (let †Вы †Вых-параметры-по-уклю)
        (unless †Есть-параметры-по-уклю
          (ФВы "(:@ budden-tools:with-byref-params (" )
          (setf †Есть-параметры-по-уклю t))
        (Лексему-в-лисп (П-Формальный-параметр-функции-Л-Имя Арг)))))))

(defun Закончить-вывод-способа-передачи-параметров ()
  (perga
   (let †Вы †Вых-параметры-по-уклю)
   (when †Есть-параметры-по-уклю
     (ФВы "))"))))

(defun Вывести-один-список-параметров (П-Опр-функ Аксессор-поля Счётчик-параметров Временное-имя)
  "Например, выводит все позиционные или все именованные"
  (perga
   (:@ with-byref-params (Счётчик-параметров))
   (let ПСаф (П-Опр-функ-П-Список-аргументов-функции П-Опр-функ))
   (let Арг-ты (funcall Аксессор-поля ПСаф))
   (k:dolist
    (Арг-т Арг-ты)
    (check-type Арг-т П-Формальный-параметр-функции)
    (ДекларироватьПараметрВЛисп Арг-т П-Опр-функ Счётчик-параметров Временное-имя)
    (incf Счётчик-параметров)
    (ФВы " ")
    )))

(defun Формальные-параметры-функции-в-лисп (П-Опр-функ Временное-имя)
  "Печатает список параметров в лисп. В прошлом также имела побочный эффект - заносит декларации 
   передачи по значению этой пр-ры в *translate-time-function-names*"
  (perga
   ;; ещё есть аргументы - ключи, их пока игнорируем
   (let ПСаф (П-Опр-функ-П-Список-аргументов-функции П-Опр-функ))
   (ФВы "(")
   (let НомерПараметра 0)
   
   (Вывести-один-список-параметров
    П-Опр-функ
    'П-Список-аргументов-функции-ПП-Позиционные-аргументы
    (byref НомерПараметра)
    Временное-имя)

   (when (П-Список-аргументов-функции-ПП-Именованные-аргументы ПСаф)
     (ФВы "cl:&key ")
     (let †Вы †Вых-декларации-функции)
     (ФВы "cl:&key "))
   
   (Вывести-один-список-параметров
    П-Опр-функ
    'П-Список-аргументов-функции-ПП-Именованные-аргументы
    (byref НомерПараметра)
    Временное-имя)
   
   (Закончить-вывод-способа-передачи-параметров)
   (ФВы ")")
    ;(Киэвл СпПараметры^Конец)
   ))

#|(defun ТрансляторЗаметитьИмяПроцедуры (Имя)
  "заносит имя пр-ры в *translate-time-function-names*. Возвращает символ имени. FIXME - игнорируется пакет исходного символа"
  (perga
    (:lett и lexem Имя)
    (assert (eq :identifier и^CLASS))
    (:lett normal-name string и^TEXT)
    (assert (not (k:find normal-name *translate-time-function-names* :test 'string=)) () "Функция ~S уже определена в текущем вызове транслятора!"  normal-name)
    (let symbol (make-symbol normal-name))
    (k:push symbol *translate-time-function-names*)
    (YAR-LISP-RUNTIME:ДекларацииС1Очистить symbol)
    (YAR-LISP-RUNTIME:ДекларацииС1ЗаписатьФункцию symbol)
    symbol))    |#
    
(defun Парсему-П-Опр-функ-в-лисп-в-вых-опр-функ (Э)
  "Функция выделена для вывода в потоки, без заботы о том, что потом нужно перелить все эти потоки в главный поток. Ничего не возвращает"
  (perga
   ;; этот поток мы должны будем вписать перед телом
   (let †Вых-декларации-типов-аргтов (КАРТЫ-ИСХОДНИКОВ-ЛИЦО:l/make-string-output-stream))
   (let †Вы †Вых-опр-функ)
   (let †Есть-параметры-по-уклю nil)
   (let †Вых-параметры-по-уклю (КАРТЫ-ИСХОДНИКОВ-ЛИЦО:l/make-string-output-stream))
   (let *ТрансляторТекущаяПроцедура* Э)
   ;(let Нл-Л (П-Опр-функ-Нл-Л Э))
   (Кнотст)
   (Вставить-ъ Э)
   (ФВы "(cl:defun ")
   (let Имя (П-Опр-функ-П-Имя Э))
   (Парсему-в-лисп Имя)
   (ФВы " ")
   (Формальные-параметры-функции-в-лисп Э Имя)
   #|
   (cond
    ((^ э ВозвращаемыйТип)
     (ФВы " ")
     (Парсему-в-лисп э^ВозвращаемыйТип)
     (ФВы " "))
    (t
     (ФВы " t ")))
   (when э^Докстринг
     (ФВы "~% ~S" (СклеитьСтрочкиЧерезРазделитель э^Докстринг (str++ #\Newline " "))))
   |#
   (let *lisp-print-indent* 1) 
   (Перелить-поток :Из-потока †Вых-декларации-типов-аргтов)
   (terpri †Вы)
   (С-шагом-после-вычисления
    ((Пб-в-конце-элемента-синтаксиса Э))
    (П-Сост-оп-в-лисп
     (П-Опр-функ-П-Сост-оп Э)
     :Имя-блока 'ЛВГ:тело ; а не должен ли это быть символ Яра?
     :Префиксы (k:list †Вых-параметры-по-уклю)
     ))
   (ФВы "~% )~%")
   (values)
   ))

(defun Завершить-декларацию-функционального-типа (П-Опр-функ)
  (perga
   (let †Вы †Вых-декларации-функции)
   ;(ФВы ") (cl:values #|гвоздь 12314|# #|cl:&optional|#)) ")
   ;; Пока что не будем пытаться напечатать тип возвр. знач.
   (ФВы ") cl:t) ")
   (Парсему-в-лисп (П-Опр-функ-П-Имя П-Опр-функ))
   (ФВы "))")
   ))

(defmethod Парсему-в-лисп ((Э П-Опр-функ))
  "Также имеет побочный эффект - заносит имя и декларации передачи по значению этой пр-ры в *translate-time-function-names*"
  (perga 
   (let Вы †Вы)
   (let †Вых-декларации-функции (КАРТЫ-ИСХОДНИКОВ-ЛИЦО:l/make-string-output-stream))
   (format †Вых-декларации-функции "~&~%(ЯР-СК-ЛСП:|Декларировать-тип-сигнатуры-функции| (cl:ftype (cl:function (")

   (let †Вых-опр-функ (КАРТЫ-ИСХОДНИКОВ-ЛИЦО:l/make-string-output-stream))

   (Парсему-П-Опр-функ-в-лисп-в-вых-опр-функ Э)

   (Завершить-декларацию-функционального-типа Э)
   (Перелить-поток :Из-потока †Вых-декларации-функции :В-поток Вы)
   (terpri Вы)
   (Перелить-поток :Из-потока †Вых-опр-функ :В-поток Вы)
   ))

    