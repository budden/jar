; -*- coding: utf-8; system :ТРАНСЛЯТОР-ЯРА-В-ЛИСП ; -*-
;; Copyright (C) Денис Будяк 2015-2017
;; Преобразование распарсенного кода Яра в компилируемый код лисп.
;; оригинал: c:/yar/ит/yar/yar-to-lisp.lisp
;; вырезано много ф-й, в т.ч вывод свойств

;; В основном мы пытаемся пройти один раз по дереву, собирая несколько назначений. 
;; Каждое назначение может быть потоком для вывода в файл (несколько файлов) или в строку. 
;; Затем мы запускаем "сборку", когда фрагменты склеиваются. Также у нас есть основной поток
;; †Вы, к-рый считается основным. Остальные - вспомогательные. 


(named-readtables:in-readtable :buddens-readtable-a)
(in-package :ТРАНСЛЯТОР-ЯРА-В-ЛИСП)

(defun ind () 
  "Генерирует строку отступа - устарел"
  (make-string *lisp-print-indent* :initial-element #\ ))

(defun Кнотст (&optional (Поток †Вы))
  "Перенос строки, если не с новой строки, и отступ"
  (format Поток "~&~A" (make-string *lisp-print-indent* :initial-element #\ )))

(defmacro ФВы (Формат &rest Аргументы)
  "сокращение для (format †Вы ...)"
  `(format †Вы ,Формат ,@Аргументы))

(defun ЛексемыИменуютОдинаковыйИдентификаторЛи (x y)
  "Мусор"
  (assert (eq (lexem-class x) :identifier))
  (assert (eq (lexem-class y) :identifier))
  (string= (lexem-text x) (lexem-text y)))
  
(defmethod Парсему-в-лисп ((П null))
  (perga
   (error "Не должно быть парсемы NIL - вместо неё д.б. П-Законный-nil")
   ;(format †Вы " NIL ")
   ))

(defmethod Парсему-в-лисп ((П П-Законный-nil))
  (perga
   (Вставить-ъ-при-наличии П)
   (format †Вы "cl:NIL ")))

(defun СклеитьСтрочкиЧерезРазделитель (s &optional (Разделитель #\Newline))
  (perga
    (:@ with-output-to-string (ou))
    (let first-time t)
    (k:dolist (l s)
      (if first-time
          (setf first-time nil)
        (format ou "~A" Разделитель))
      (format ou "~A" l)
      ))
  )

(defun normalize-1-yar-symbol-name (s)
  "Приводит имя Яр к некоторой нормальной форме, чтобы, имея два имени, можно было понять, одинаковы они или нет. Но печатаем мы их не в этой форме, см. normalize-yar-symbol-name"
  (let ((str
         (etypecase s
           (lexem
            (assert (eq s^CLASS :identifier))
            (lexem-text s))
           (string s)
           (symbol (symbol-name s)))))
    (identity ; а могло быть string-upcase-cyr
     str)))

(defun Киэвл (э)
  "Комментарии из элемента в лисп"
  (КомментарииВЛисп (ИзвлечьКомментарииИзЭлемента э)))
  
(defmethod Парсему-в-лисп ((Э П-Имя))
  "Для П-Имя"
  (perga
   тело
   (let ЛЛ (П-Имя-Л-Л Э))
   (when (and (k:consp ЛЛ) (= (k:length ЛЛ) 1))
     (setf ЛЛ (k:car ЛЛ))
     )
   (unless (typep ЛЛ 'Лексема)
     (Ош-р-п "Не научились транслировать консовое имя в лисп" Э))
   (Лексему-в-лисп ЛЛ)))

                

(defun get-output-stream-string* (Вы)
  (etypecase Вы
    (string-stream
     (get-output-stream-string Вы))
    (ПОТОКИ-ЗПТ-СЧИТАЮЩИЕ-БУКВЫ-СТРОКИ-И-КОЛОНКИ:Считающий-выходной-поток-литер
     (get-output-stream-string (ПОТОКИ-ЗПТ-СЧИТАЮЩИЕ-БУКВЫ-СТРОКИ-И-КОЛОНКИ:Поток-из Вы)))))

(defun Перелить-поток (&key Из-потока (С-новой-строки t) (В-поток †Вы) (Если-нет-делегата-для-сохранения-исходников :error))
  "Поток вывода в строку выводит в основной поток генерации, после чего поток закрывается"
  (perga
   (when С-новой-строки
     (Кнотст В-поток))
   (let Результат (get-output-stream-string* Из-потока))
   (close Из-потока)
   (let Делегат (КАРТЫ-ИСХОДНИКОВ-ЛИЦО:get-stream-location-map-delegate Из-потока :if-not-exists Если-нет-делегата-для-сохранения-исходников))
   (when Делегат
     (КАРТЫ-ИСХОДНИКОВ-ЛИЦО:l/pass-from-delegate-to-object Результат Делегат)
     (КАРТЫ-ИСХОДНИКОВ-ЛИЦО:l/princ Результат В-поток))
   (values)))

(defun Ошибка-трансляции (Место datum &rest arguments)
  (apply 'П-Я-ГРАМОТЕЙ:Ош-р-п Место datum arguments))
