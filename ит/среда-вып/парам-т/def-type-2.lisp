;; -*- coding: utf-8; system :def-type;  -*- 
;; def-type-2.lisp

(in-package :cl-user)

;; от этой формы можно избавиться, если есть зацепка, позволяющая связать переменную
;; вокруг компиляции, например, https://bitbucket.org/budden/budden-tools/src/default/let-around-compile-file-and-load.lisp?at=default&fileviewer=file-view-default
(eval-factory-for-predicates-of-known-parametric-types #.*compile-file-truename*)

(eval-when (:compile-toplevel)
  (setf *known-parametric-types* nil))

(deftype type-ab (type-a type-b)
  (print "Вот выполняется тело (deftype type-ab)")
  (let* ((predicate-name (intern (prin1-to-string `(type-ab ,type-a ,type-b))))
         (predicate-source
          `(defun ,predicate-name (value) 
             (and (typep (str-a value) ',type-a)
                  (typep (str-b value) ',type-b))))
         (result `(and str (satisfies ,predicate-name))))
    (setf (getf *known-parametric-types* predicate-name) predicate-source)
    (format t "Результат deftype: ~S" result)
    ;; определяем предикат во время макрорасширения
    (compile (eval predicate-source))
    result))

(defun bar () 
  (format t "Проверка типа вернула ~A~%"
          (typep (foo) '(type-ab integer string))))

(bar) 

(eval-when (:compile-toplevel)
  (def-factory-for-predicates-of-known-parametric-types #.*compile-file-truename*))

;; eof