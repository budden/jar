; -*- coding: utf-8; -*-
;; def-type.asd
(defsystem :def-type
  :serial t
  :components ((:file "def-type-1")
               (:file "def-type-2")
               ))

;; eof

;; Это пока прототип, система и файлы оформлены некорректно. Но в связи со значительной ценностью
;; добавляю его в репозиторий
