;; -*- coding: utf-8; system :def-type;  -*-  
;; def-type-1.lisp

(in-package :cl-user)

(defstruct str a b)

(defun foo ()
  (make-str :a 1 :b "1"))

(defun only-values-of-plist (plist)
  (let ((flag nil)
        (result nil))
    (dolist (e plist)
      (if flag (push e result))
      (setf flag (not flag)))
    (nreverse result)))

(defvar *known-parametric-types*)

(defmacro generate-funs-for-known-parametric-types ()
  (let* ((sources (only-values-of-plist *known-parametric-types*)))
    `(progn
       ,@sources)))

(defmacro eval-factory-for-predicates-of-known-parametric-types (filename)
  (declare (ignore filename))
  `(eval-when (:load-toplevel)
     (load (compile-file (defun-to-file:|Полное-имя-файла-для-Defun-to-file| 'my-factory)))
     (my-factory)))

(defmacro def-factory-for-predicates-of-known-parametric-types (filename)
  (declare (ignore filename))
  (let* ((sources (only-values-of-plist *known-parametric-types*)))
    `(defun-to-file::defun-to-file my-factory () 
       ,@sources)))

;; eof