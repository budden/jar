; -*- system :ЯР-СРЕДА-ВЫП; coding: utf-8; -*- 
(named-readtables:in-readtable :buddens-readtable-a)
(in-package :ЯР-СТ-БИБ)

(eval-when (:compile-toplevel)
  (unless (boundp 'asdf::*current-component*)
    (break "Не загружай этот файл отдельно, а делай так: (asdf:load-system :ЯР-СРЕДА-ВЫП :force t)")))

(Очистить-Рег) ; это стоит в начале всех вызовов Рег

(defvar †Пакет-лиспа-в-сгенерированных-файлах "ЛВГ")
(defvar †Цель-оптимизации-кода-Яра "Отладка")

(defmacro Отныне-пакет-лиспа-в-сгенерированных-файлах-есть (Обозначение-пакета-строкой)
  "Действует только внутри текущей трансляции. Хотя если выполнить в лиспе, то будет действовать для всех последующих трансляций"
  (setf †Пакет-лиспа-в-сгенерированных-файлах Обозначение-пакета-строкой)
  nil)

(Рег "Отныне-пакет-лиспа-в-сгенерированных-файлах-есть" :Л-симв 'Отныне-пакет-лиспа-в-сгенерированных-файлах-есть)

(defmacro Лисп (Строка)
  (let ((*readtable* (named-readtables:find-readtable :buddens-readtable-a))
        (*package* (find-package †Пакет-лиспа-в-сгенерированных-файлах)))
    (read-from-string Строка)))

(Рег "Лисп" :Л-симв 'Лисп)

(defmacro Отныне-цель-оптимизации-кода-Яра-есть (Строка)
  ;; Действует только в текущем скрипте, хотя если выполнить глобально в лиспе, то будет действовать в лиспе
  ;; для проверки
  (Декларации-оптимизации-кода-Яра Строка)
  (setf †Цель-оптимизации-кода-Яра Строка))

(defun Декларации-оптимизации-кода-Яра (Строка)
  (alexandria:eswitch
   (Строка :test 'string=)
   ("Скорость"
    `(progn
       (proclaim '(optimize (speed 3) (safety 0) (debug 0) (compilation-speed 0) (space 0)))
       (declaim (optimize (speed 3) (safety 0) (debug 0) (compilation-speed 0) (space 0)))))
   ("Отладка"
    `(progn
       (proclaim '(optimize (speed 0) (safety 3) (debug 3) (compilation-speed 0) (space 0)))
       (declaim (optimize (speed 0) (safety 3) (debug 3) (compilation-speed 0) (space 0)))
       (declaim (notinline ЯР-С-ВЫП:Стоп-выр-1 ЯР-СТ-БИБ::+°))))
   ("Запретить-сужение-типов"
    ;; вроде пока отключено, будет ли работать - я не знаю.
    `(progn
       (proclaim '(optimize (speed 0) (safety 2) (debug 3) (compilation-speed 0) (space 0)))
       (declaim (optimize (speed 0) (safety 2) (debug 3) (compilation-speed 0) (space 0)))))))

(Рег "Отныне-цель-оптимизации-кода-Яра-есть" :Л-симв 'Отныне-цель-оптимизации-кода-Яра-есть)

(defmacro Сохрани-результат-компиляции ()
  nil)
(Рег "Сохрани-результат-компиляции" :Л-симв 'Сохрани-результат-компиляции)

(defmacro Авось (Условие Ругательство)
  "Ой, у нас нет &rest :( - но можно было принять список"
  `(assert ,Условие () ,Ругательство))

(Рег "Авось" :Л-симв 'Авось)
