; -*- coding: utf-8; -*- 

(named-readtables:in-readtable :buddens-readtable-a)
(in-package :budden)

;;; Это всего лишь игрушки. Достаточно дорого обновлять объекты лениво и при этом правильно, 
;;; см. ОЯ::переопределение-классов-в-CLOS-не-хранит-историю

(def-merge-packages::! :ВЕРСИИ-ОБЪЕКТОВ
                       (:always t)
                       (:use)
  (:export "
            ВЕРСИИ-ОБЪЕКТОВ::В0
            ВЕРСИИ-ОБЪЕКТОВ::В1
            ВЕРСИИ-ОБЪЕКТОВ::В2
            ВЕРСИИ-ОБЪЕКТОВ::В3
            ВЕРСИИ-ОБЪЕКТОВ::В4
            ВЕРСИИ-ОБЪЕКТОВ::В5 "))

(defparameter †ВЕРСИИ-ОБЪЕКТОВ (find-package :ВЕРСИИ-ОБЪЕКТОВ))

(defclass ъъъ () ((ВЕРСИИ-ОБЪЕКТОВ::В0 :initform t) а б))

(defun Найти-предыдущую-версию-в-списке-свойств (Сп)   
  (perga
   тело
   (break)
   (loop
     (unless Сп (return))
     (let Э (pop Сп))
     (when (eq (symbol-package Э) †ВЕРСИИ-ОБЪЕКТОВ)
       (return-from тело Э))
     (pop Сп))))

(defun Найти-версию-в-списке-имён-слотов (Сп)
  (find †ВЕРСИИ-ОБЪЕКТОВ Сп :key #'symbol-package))

(defmethod update-instance-for-redefined-class ((Ю ъъъ) Плюс Минус Теряем-значения &rest Инициализаторы)
  (perga
   (ignored Теряем-значения Инициализаторы)
   (let Предыдущая-версия (Найти-версию-в-списке-имён-слотов Минус))
   (let Следующая-версия (Найти-версию-в-списке-имён-слотов Плюс))
   (show-exprt Предыдущая-версия)
   (show-exprt Следующая-версия)
   (call-next-method)))

(defparameter ю (make-instance 'ъъъ))
(defparameter ж (make-instance 'ъъъ))

(defun Смотрим-на-апгрейд ()
  (eval '(defclass ъъъ () ((ВЕРСИИ-ОБЪЕКТОВ::В1 :initform t) а б)))
  (print ю)
  (eval '(defclass ъъъ () ((ВЕРСИИ-ОБЪЕКТОВ::В2 :initform t) а б)))
  (print ж)
  (values)
  )
