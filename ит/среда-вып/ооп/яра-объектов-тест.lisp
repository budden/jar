;; -*- coding: utf-8; system: ЯРА-ОБЪЕКТЫ; -*- 

(named-readtables:in-readtable :buddens-readtable-a)
(in-package :ЯРА-ОБЪЕКТЫ)

;(let ((*print-circle* nil))
;  (print
;   (macroexpand-1 
;    '(Опр-объект ъ1 :Поля (а б в)))))


(let ((i 1))
  (defstruct Г (ГГ (incf i))))

(declaim (optimize (speed 3) (compilation-speed 0) (space 0) (safety 0) (debug 0)))
(proclaim '(optimize (speed 3) (compilation-speed 0) (space 0) (safety 0) (debug 0)))

(defstruct Предок А)
(defstruct (Потомок (:include Предок)) Б)
(defstruct (Внук (:include Потомок)) В)

(defmacro Во-время-макрорасширения (Выражение)
  (eval 
   Выражение))

;; Видимо, CCL не видит структур во время компиляции. Может, это и хорошо... 
#-CCL 
(defun Только-для-потомка (Я)
  (cond
   ((eq (type-of Я) (Во-время-макрорасширения (find-class 'Потомок)))
    'аз)
   (t
    'омега)))

#+CCL 
(eval-when (load-toplevel)
  (defun Только-для-потомка (Я)
    (cond
     ((eq (type-of Я) (Во-время-макрорасширения (find-class 'Потомок)))
      'аз)
     (t
      'омега))))


