;; -*- coding: utf-8; system: ЯРА-ОБЪЕКТЫ; -*- 

(named-readtables:in-readtable :buddens-readtable-a)

(in-package :cl-user)

(def-merge-packages::! :ЯРА-ОБЪЕКТЫ
                       (:always t)
  (:use :cl :budden-tools)
  (:export
   #.(apply 'budden-tools:str++
            (mapcar
             (lambda (и)
               (budden-tools:Написать-экспорт-для-структуры `("ЯРА-ОБЪЕКТЫ" ,и)))
             '("ЯРА-ОБЪЕКТЫ::Прочитанное-определение-поля-класса"
               "ЯРА-ОБЪЕКТЫ::Прочитанное-определение-класса"
               )))))

(def-merge-packages::! :ЯРА-БАЗОВЫЙ-КЛАСС
                       (:always t)
  (:use :cl :budden-tools :ЯРА-ОБЪЕКТЫ)
  (:export
   "ЯРА-БАЗОВЫЙ-КЛАСС::ТВМ
   ЯРА-БАЗОВЫЙ-КЛАСС::ТВМ-Мд
   ЯРА-БАЗОВЫЙ-КЛАСС::Создай-ТВМ 
   ЯРА-БАЗОВЫЙ-КЛАСС::Устрой-место-в-ТВМ
   ЯРА-БАЗОВЫЙ-КЛАСС::ЯБК ; Яра базовый класс
   ЯРА-БАЗОВЫЙ-КЛАСС::Дай-Мд-класса
   ЯРА-БАЗОВЫЙ-КЛАСС::Установи-определение-класса
   "
   )
  (:export
   #.(apply 'budden-tools:str++
            (mapcar
             (lambda (и)
               (budden-tools:Написать-экспорт-для-структуры `("ЯРА-БАЗОВЫЙ-КЛАСС" ,и)))
             '("ЯРА-БАЗОВЫЙ-КЛАСС::Мд-класса"
               "ЯРА-БАЗОВЫЙ-КЛАСС::ТВМ"
               ))))  
  )


;; рабочее название
(def-merge-packages::! :СИМВОЛЫ-РЕАЛИЗАЦИИ-ООП
                        (:always t)
  (:allow-qualified-intern t)
  (:use))

;; И спрашивается, зачем нам ЯРА-ОБЪЕКТЫ и ЯРА-КЛАСС отдельно?
(def-merge-packages::! :ЯРА-КЛАСС
                       (:documentation "Производные объекты")
                       (:always t)
  (:use :cl :budden-tools)
  (:import-from :ЯР-ЛЕКСИКОНЫ)
  (:import-from :ЯРА-БАЗОВЫЙ-КЛАСС)
  (:import-from :СИМВОЛЫ-РЕАЛИЗАЦИИ-ООП)
  (:import-from :alexandria)
  (:import-from :sb-ext)
  (:export
   "ЯРА-КЛАСС::Опр-класс
    ЯРА-КЛАСС::Обнови-класс ; alter class
    "
   ))

