;;; -*- Mode:Lisp; system :ЯРА-ОБЪЕКТЫ; coding: utf-8; -*-
;; Copyright (C) Денис Будяк 2017

;; В этой системе есть ещё одно место, где пересоздаётся пакет!
(named-readtables:in-readtable :buddens-readtable-a)

(load (compile-file
       (merge-pathnames
        "яра-объекты-пакет.lisp"
        (asdf:system-definition-pathname (asdf:find-system :ЯРА-ОБЪЕКТЫ)))))

