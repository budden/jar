; -*- system :ЯР-СРЕДА-ВЫП; coding: utf-8; -*- 
(named-readtables:in-readtable :buddens-readtable-a)
(in-package :ЯР-СТ-БИБ)

(eval-when (:compile-toplevel)
  (unless (boundp 'asdf::*current-component*)
    (break "Не загружай этот файл отдельно, а делай так: (asdf:load-system :ЯР-СРЕДА-ВЫП :force t)")))

(deftype любой-объект () t)
(Рег "любой-объект" :Л-симв 'любой-объект) ; фигово получается - 
(Рег "целое" :Л-симв 'integer)
(Рег "л-список" :Л-симв 'list)
(Рег "л-строка" :Л-симв 'string)
(Рег "л-ничто" :Л-симв nil :Это-лисповый-символ-nil-ли t)
(Рег "л-массив" :Л-симв 'array)
(Рег "л-вектор" :Л-симв 'vector)

