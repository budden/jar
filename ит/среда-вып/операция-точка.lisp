; -*- system :ЯР-СРЕДА-ВЫП; coding: utf-8; -*- 
; (С) Денис Будяк, Роман Клочков, 2017, лицензия MIT
(named-readtables:in-readtable :buddens-readtable-a)
(in-package :ЯР-СТ-БИБ)

#+SBCL
(sb-c::defknown Оп-тчк-без-динамики (t t t t &rest t) t () :overwrite-fndb-silently t)

#+SBCL
(sb-c::defknown Оп-тчк-с-динамикой (t t t t &rest t) t () :overwrite-fndb-silently t)

(defun Оп-тчк-с-динамикой (obj Разделитель fld Арг2-это-функция-или-поле &rest args)
  (declare (ignore Арг2-это-функция-или-поле))
  (apply 'budden-tools::runtime^ obj Разделитель fld args))

; "По аналогии с sb-c::%compile-time-type-error"
(defun Оп-тчк-без-динамики (obj Разделитель fld Арг2-это-функция-или-поле &rest args)
  (declare (ignorable obj fld Арг2-это-функция-или-поле args Разделитель))
  (error "Функция для Что-то . ~A не была определена во время компиляции. Или функции для этого типа не было, или тип не был указан" fld))

#+SBCL
(sb-c::defoptimizer (Оп-тчк-без-динамики sb-c:ir2-convert)
                    ((obj Разделитель fld Арг2-это-функция-или-поле &rest args) node block)
                    (declare (ignore Разделитель Арг2-это-функция-или-поле args))
                    (let ((sb-c::*compiler-error-context* node)
                          (fld-name (if (sb-c::constant-lvar-p fld)
                                        (sb-c::lvar-value fld)
                                        "<Имя поля - не константа, это вообще ошибка в применении точки>")))
                      (_f cdr (sb-c::node-source-path node))
                      (warn "~S точка ~S: тип значения слева от точки недостаточен для определения функции, или не найдена функция доступа для этого типа. Выполнение вызовет ошибку"
                            (sb-c::lvar-derived-type obj)
                            fld-name)) 
                    (sb-c::ir2-convert-full-call node block))

#+SBCL
(defun Начинка-DEFTRANSFORM-для-Оп-тчк (obj Разделитель fld Арг2-это-функция-или-поле args)
  ;(declare (ignore args))
  (perga
   (assert (sb-c::constant-lvar-p Разделитель))
   (cond
    ((SB-C::CONSTANT-LVAR-P fld)
     (let acc (get-accessor
               (SB-C::values-type-types (sb-c::lvar-derived-type obj))
               (sb-c::lvar-value Разделитель)
               (sb-c::lvar-value fld)
               (sb-c::lvar-value Арг2-это-функция-или-поле)
               obj))
     (let Имена-аргументов (SB-INT:MAKE-GENSYM-LIST (length args) "Арг-т-для-а-точка-б-"))
     (cond
      (acc
       ;(when args
       ;  (break))
       (let res 
         `(lambda (obj Разделитель fld Арг2-это-функция-или-поле ,@Имена-аргументов)
            (declare (ignore Разделитель fld Арг2-это-функция-или-поле))
            (,acc obj ,@Имена-аргументов)))
       (show-expr res)
       res)
      (t
       (sb-c::give-up-ir1-transform))))
    (t
     (sb-c::give-up-ir1-transform)))))

;; сначала тут было 4 звёздочки, но потом добавился &rest, а как его озвездить - я не знаю, поэтому все аргументы 
;; превращаются в одну звёздочку. 
#+SBCL
(sb-c:deftransform Оп-тчк-без-динамики ((obj Разделитель fld Арг2-это-функция-или-поле &rest args) * *)
                   (Начинка-DEFTRANSFORM-для-Оп-тчк obj Разделитель fld Арг2-это-функция-или-поле args))


;; дословная копия для такового же без динамики
#+SBCL
(sb-c:deftransform Оп-тчк-с-динамикой ((obj Разделитель fld Арг2-это-функция-или-поле &rest args) * *)
                   (Начинка-DEFTRANSFORM-для-Оп-тчк obj Разделитель fld Арг2-это-функция-или-поле args))

#+SBCL
(defun get-accessor (type Разделитель fld Арг2-это-функция-или-поле obj)
  ;(budden-tools:show-expr type)
  ;(budden-tools:show-expr fld)
   (labels ((gi (ty)
              (perga
               (typecase ty
                 (cons
                  (dolist (type1 ty)
                    (let r (gi type1))
                    (when r (return-from get-accessor r))))
                 (sb-kernel:intersection-type
                  (gi (sb-kernel:compound-type-types ty)))
                 (sb-kernel:structure-classoid
                  ;(show-expr *package*)
                  (Найти-функцию-или-поле-для-структуры (sb-kernel:classoid-name ty) Разделитель fld Арг2-это-функция-или-поле obj))))))
   (gi type)))

(defun Найти-функцию-или-поле-для-структуры (Имя-структуры Разделитель Поле Арг2-это-функция-или-поле obj)
  "Возвращает символ, именующий функцию (возможно, акссессор), или ругается (интересно, а если здесь ругаться, не будет ли это разрушательным для компилятора?"
  (perga
   (let Ожидаемое-имя
     (with-standard-io-syntax
      (str++ Имя-структуры Разделитель Поле))) ; ПРАВЬМЯ а как с кривыми, требующими закавычивания именами?
   (let Такой-символ-если-есть (find-symbol Ожидаемое-имя (symbol-package Имя-структуры)))
   ;(show-expr `(,Ожидаемое-имя ,Такой-символ-если-есть))
   (let Такая-функция-если-есть (when (fboundp Такой-символ-если-есть) Такой-символ-если-есть))
   (let Это-функция-или-поле (BUDDEN-TOOLS::FIELD-OR-FUNCTION-BY-TYPE-OR-CLASS-AND-FIELD-NAME Имя-структуры Поле))
   (when Такая-функция-если-есть
     (unless (eq Это-функция-или-поле Арг2-это-функция-или-поле)
       (let Чем-должно-быть
         (case Это-функция-или-поле
           (:function "вызовом функции")
           (:field "именем поля")
           (t "неизвестно чем (ошибка в обработке ошибки)")))
       (error "В выражении ~S.~S аргумент справа от точки должен быть ~S"
              obj Поле Чем-должно-быть))
     Такая-функция-если-есть)))

