; -*- system :ЯР-СРЕДА-ВЫП; coding: utf-8; -*- 
(named-readtables:in-readtable :buddens-readtable-a)
(in-package :cl-user)

;; аналогии ищем в оя::обзор-среды-времени-выполнения-старого-яра

(def-merge-packages::! :ИЗ-ЛИСПА-В-ЯР
  (:always t)
  (:documentation "куски лиспа, нужные транслированному из Яр коду. Раньше нужно было 
минимизировать количество экспортов, т.к. они могли пересечься с одноимёнными ф-ями из Яр и будет плохо.
В будущей версии Яра это не нужно, т.к. в генерируемом коде печатаем префиксы пакетов")
  (:import-from :cl
   ; нужно 
   #:defparameter
   #:return-from
   #:cond
   #:nil 
   #:t 
   #:in-package
   #:let
   #:funcall
   #:setf
   #:lambda
   #:mapcar
   #:typecase
   #:etypecase
   #:declare
   #:ignore
   #:eql
   #:&optional
   #:&rest
   #:function
   #:go
   )
  (:export
   #:defparameter
   #:return-from
   #:cond
   #:nil 
   #:t 
   #:in-package
   #:let
   #:funcall
   #:setf
   #:lambda
   #:mapcar
   #:typecase
   #:etypecase
   #:declare
   #:ignore
   #:eql
   #:&optional
   #:&rest
   #:function
   #:go
   )
  (:import-from :budden "^" "PERGA")  
  ;(:export #:PERGA #:^)
  (:import-from
   :editor-hints.named-readtables
   :in-readtable)
  (:export #:in-readtable)
  )

(def-merge-packages::! :ЯР-С-ВЫП
  (:always t)
  (:documentation "ЯР-Среда-Выполнения:
                   - инициализация среды выполнения
                   - функции инструментирования кода для отладки.
                   - функции для реализации ЦЧВП 
                   Сюда надо перенести ЯР-ЛЕКСИКОНЫ:†Текущий-лексикон и т.п.")
  (:use :cl :budden-tools :ЯР-ЛЕКСИКОНЫ)
  (:export
   "ЯР-С-ВЫП::†Код-заготовки-лексикона
    ЯР-С-ВЫП::Рег
    ЯР-С-ВЫП::Очистить-Рег
    ЯР-С-ВЫП::Опр-фнзл
    ;; это не инициализация среды выполнения, но часть среды выполнения. Не макрос и не часть библиотеки. Поместим сюда, хотя это отдельная категория сущностей
    ЯР-С-ВЫП::Стоп-выр-1 ; ей инструментируется код при пошаговом исполнении
    ЯР-С-ВЫП::*Результат-ЦЧВП*
    ЯР-С-ВЫП::Готов-результат-ЦЧВП
    ЯР-С-ВЫП::Текущая-форма-ЦЧВП ; функция, к-рая переопределяется при каждом шаге ЦЧВП
    "))

(def-merge-packages::! :ЯР-СТ-БИБ
  (:always t)
  (:documentation "ЯР-Стандартная-Библиотка: реализация операций и инструкций Яра, включаемая в генерируемый транслятором файл лиспа.
Аналог :yar-lisp-library, но макросы, реализующие конструкции, из :yar-lisp-library, тоже идут сюда (наверное), иначе будет + в одном месте, а 'или' - в другом")
  (:use :cl :budden-tools :ЯР-ЛЕКСИКОНЫ :ЯР-СК-ЛСП :ЯР-С-ВЫП)
  (:export
   "
    "))
