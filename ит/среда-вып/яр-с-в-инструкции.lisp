; -*- system :ЯР-СРЕДА-ВЫП; coding: utf-8; -*- 
(named-readtables:in-readtable :buddens-readtable-a)
(in-package :ЯР-СТ-БИБ)

(eval-when (:compile-toplevel)
  (unless (boundp 'asdf::*current-component*)
    (break "Не загружай этот файл отдельно, а делай так: (asdf:load-system :ЯР-СРЕДА-ВЫП :force t)")))

(defmacro s-return-from (block value)
  "Кортежи пока не поддерживаем"
  (perga
   (let Зн
     (cond
      (ЯР-СК-ЛСП:*Подробные-шаги*
       `(prog1 ,value
          (ЯР-С-ВЫП:Стоп-выр-1 ,(format nil "Собираемся вернуться из блока ~S" block))))
      (t
       value)))
   `(return-from
     ,block
     ,Зн)))

(Рег "вернуть-из" :Л-симв 's-return-from)


(defmacro Открыв-файл ((stream filespec &rest options) &body body)
  `(with-open-file (,stream ,filespec ,@options) ,@body))

(Рег "Открыв-файл" :Л-симв 'Открыв-файл :Это-собачник-ли t)
