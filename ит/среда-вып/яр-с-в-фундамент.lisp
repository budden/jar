; -*- system :ЯР-СРЕДА-ВЫП; coding: utf-8; -*- 
(named-readtables:in-readtable :buddens-readtable-a)
(in-package :ЯР-С-ВЫП)

(defvar †Код-заполнения-лексикона nil
  "Эта переменная устанавливается с помощью setf внутри каждого файла исходного текста. Это плохо, но что поделать?")

;; должны быть согласованы с действиями в каждом файле системы
(defparameter †Инициализаторы
  '((*Лексикон-лисповое-ядро* (Начальное-значение-лексикона-лисповое-ядро))
    (*Лексикон-временный-глобальный* (Начальное-значение-лексикона-временного-глобального))
    (†Текущий-лексикон *Лексикон-временный-глобальный*)
    (*Лексикон-яра-ключевые-слова* (Начальное-значение-лексикона-ключевые-слова)))
  "Присваивания или связывания, к-рые нужно осуществить для инициализации")

(defmacro Код-инициализации (Действие &body body)
  (perga
   (let Комментарий `(budden-tools:ignored ,(format nil "Сгенерировано из макроса ~S" 'Код-инициализации)))
   (ecase Действие
     (setf
      (assert (null body))
      `(progn
         ,Комментарий
         ,@(mapcar (lambda (И) `(setf ,@И)) †Инициализаторы)))
     (let*
         `(let* ,†Инициализаторы
            ,Комментарий
            ,@body)))))

(defmacro Очистить-Рег ()
  "Очищает сведения, заполненные с помощью Рег. Три макроса используются совместно:
 в начале файла - Очистить-Рег
 по мере сбора сведений о нужных символах - Рег для каждого символа
 в конце файла Опр-фнзл - создаёт функцию, возввращающую начальное значение лексикона.
 После Опр-фнзл нужно вставить код инициализации лексикона. Также нужно поместить этот код в †Инициализаторы
   "
  (setf †Код-заполнения-лексикона nil))

(defmacro Рег (Строка-имени &rest keys &KEY Л-симв Это-лисповый-символ-NIL-ли Это-собачник-ли)
  "Зарегистрировать символ, к-рый будет добавлен в лексикон. Возвращает (values). См. РУК-ВО--РАЗРАБОТЧИКА::как-добавить-новый-вид-определения-в-SBCL"
  (declare (ignorable Это-лисповый-символ-nil-ли Это-собачник-ли)) ; они используются через keys
  #+CCL (when Л-симв
          (ccl::record-source-file Л-симв 'Регистрация-для-Яра))
  (perga
   (push
    `(perga
      (Добавить-новый-собственный-символ-в-лексикон
       Рез
       ,Строка-имени
       ,@keys))
    †Код-заполнения-лексикона)
   #+SBCL `(progn
      ,@(when Л-симв
          `((setf (get ,Л-симв 'ЯР-ЛЕКСИКОНЫ::yar-source-location) (sb-c:source-location))))
      (values))
   #+CCL '(values)
   ))

;; это моя ф-я, определена в c:/yar/lp/budden-tools/sbcl--find-definition-sources-by-name--patch.lisp
#+SBCL
(sb-introspect::ensure-definition-type :yar-reg 'ЯР-С-ВЫП:Рег)

#+SBCL 
(defun sb-introspect--find-definition-sources-by-name-sb--yar-reg (original-fn name type)
  "Декоратор sb-introspect::find-definition-sources-by-name для навигации к символам, зарегистрированным с помощью Рег"
  (perga
   (case type
     (:yar-reg
      (typecase name
        (symbol
         (let location (get name 'ЯР-ЛЕКСИКОНЫ::yar-source-location))
         (and location (list (sb-introspect::translate-source-location location))))
        (t
         (warn "Здорово, что вы хотите узнать, где определение ~S, но я пока не знаю, как его получить" name)
         nil)))
     (t
      (funcall original-fn name type)))))

#+SBCL
(cl-advice:define-advice sb-introspect::find-definition-sources-by-name #'sb-introspect--find-definition-sources-by-name-sb--yar-reg :advice-name yar-reg)

(defmacro Опр-фнзл (Имя-функции &rest Ключи &key Имя #|лексикона|# Пакет)
  "Определить функцию начального значения лексикона. Определяет ф-ю одного параметра из заготовки"
  (declare (ignorable Имя Пакет)) ; они используются через keys
  ;(setf *break-on-signals* t)
  `(defun-to-file:defun-to-file ,Имя-функции ()
                                (perga
                                 (let Рез (ЯР-ЛЕКСИКОНЫ::MAKE-Ярый-лексикон ,@Ключи))
                                 ,@†Код-заполнения-лексикона
                                 Рез)))
