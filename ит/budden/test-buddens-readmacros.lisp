;; -*- encoding: utf-8; coding: utf-8-*- 
;; Copyright (C) Денис Будяк 2015
(in-package :budden)
(of-system :budden)
(in-readtable :buddens-readtable-a)


(def-trivial-test::! carat-1 
                     (let1 -v- "asdf"
                       (^ (^ -v- UPCASE) DOWNCASE))
                     "asdf")

(def-trivial-test::! carat-2
                     (let1 -v- "asdf"
                       (^ (^ -v- UPCASE) EQUAL (^ (^ -v- UPCASE) DOWNCASE)))
                      t)


(def-trivial-test::! carat-3
                     (eval '(let1 -v- "asdf"
                              (^ (^ -v- UPCASE) EQUAL (^ (^ -v- UPCASE) DOWNCASE))))
                      t)


(def-trivial-test::! typed-carat 
                      (ignore-errors
                        (unwind-protect
                            (proga
                              (fmakunbound 'ёё)
                              (eval '(defun ёё ()
                                       (proga 
                                         (with-the1 v string "asdf")
                                         v^UPCASE)
                                       ; (let1 v "asdf" v^UPCASE) вот это вызовет ошибку
                                       ))
                              
                              (compile 'ёё)
                              #+lispworks (trace (budden-tools::runtime^ :before ((error "runtime^ shouldn't have been called"))))
                              #+sbcl (trace budden-tools::runtime^ :condition (error "runtime^ shouldn't have been called"))
                              (ёё)
                              "ASDF"
                              )
                          #+(or lispworks sbcl) (untrace budden-tools::runtime^)))
                      "ASDF")

(def-trivial-test::! РегистрГенсимов
  (and 
    (equal (string '#:bB) "bB")
    (equal (string '#:bb) "BB")
    (equal (string '#:BB) "BB")
    (equal (string '#:яЯ) "яЯ")
    (equal (string '#:ЯЯ) "ЯЯ")
    (equal (string '#:яя) "яя"))
  t)

(def-trivial-test::! РегистрКейвордов
  (and 
    (equal (string ':bB) "bB")
    (equal (string ':bb) "BB")
    (equal (string ':BB) "BB")
    (equal (string ':яЯ) "яЯ")
    (equal (string ':ЯЯ) "ЯЯ")
    (equal (string ':яя) "яя"))
  t)
