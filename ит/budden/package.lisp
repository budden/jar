; -*- Encoding: utf-8 ; coding: utf-8 ; system :budden ; -*- 
;; Copyright (C) Денис Будяк 2015

(in-package :budden-tools)
(in-readtable :buddens-readtable-a)

(def-merge-packages::! :BUDDEN0 (:nicknames :bdn0) 
  (:documentation "Этот пакет надо расформировать")
  (:always t)
  (:print-defpackage-form t)
  (:use :cl :cl-ppcre :budden-tools :russian-budden-tools :editor-budden-tools)
  (:shadowing-import-from :iterate-keywords #:iter)
  (:shadowing-import-from :editor-budden-tools editor-budden-tools:+whitespace+)
  #+ap5 (:shadowing-import-from :ap5 ap5:ignored)
  (:export 
   "
   ; reexprot
   budden0:iter 
   budden0:+whitespace+

   ; merging
   budden0:*winmerge-program* 
   budden0:winmerge-strings

   ; os interface
   budden0:call-bat
   budden0:cmd-c ; cmd /c
   BUDDEN0::SIMPLIFY-PARAMETER-FOR-CMD-FILE
   budden0:ПУТЬ-ВНУТРИ--TEMP

   ; control structures, debugging
   budden0:destructuring-bind-case

   ; lists
   budden0:build-partial-index

   ; utils
   budden0::grep-docstrings
   "
   )
  )


(defpackage DNS.VERSION-MAGIC (:documentation "частные функции, отношения и переменные для хранения текущей БД 
и текущей версии"))

                            
(eval-when (:compile-toplevel :load-toplevel :execute)
  (unless (find-package :fb) 
    (def-merge-packages::! :FB (:use) ; для символов, именующих хр.пр. и т.п.
                            (:allow-qualified-intern t)
                            )))


(def-merge-packages::! :BUDDEN
    ;(:print-defpackage-form t)
    ;(:always t)
    (:nicknames :bdn :dns :|budden|)
    (:documentation "основной и более подвижный пакет")
    (:local-nicknames
     :mpf :meta-parse-firebird
     :bu :budden-tools 
     :ppcres :ppcre-shortcuts)
    (:use :cl :budden-tools :russian-budden-tools
     :cl-ppcre :budden0 
     :fb
     :alexandria
     :named-readtables
     :split-sequence
     :iterk
     )
    (:import-from :lift #:deftestsuite #:addtest #:ensure-same)
    (:SHADOWING-IMPORT-FROM :ALEXANDRIA #:WITH-GENSYMS #:ONCE-ONLY)    
    (:SHADOWING-IMPORT-FROM :SPLIT-SEQUENCE #:SPLIT-SEQUENCE)
    (:SHADOWING-IMPORT-FROM :BUDDEN-TOOLS #:READ-FILE-INTO-STRING #:NOT-NULL)
    (:SHADOWING-IMPORT-FROM :EDITOR-BUDDEN-TOOLS editor-budden-tools:+whitespace+)
    (:SHADOWING-IMPORT-FROM :META-PARSE-FIREBIRD #:*M_FT_ON* #:*FIREBIRD-LOCAL-MACROS*)
    (:shadowing-import-from :asdf #:of-system)
    (:shadowing-import-from :perga-implementation #:perga #:def-perga-clause)
    (:shadowing-import-from :def-merge-packages #:group-similar-items)
    (:shadowing-import-from :budden0 budden0:winmerge-strings)
    (:shadowing-import-from :cons-to-source-formatters
     CONS-TO-SOURCE-FORMATTERS:*PASCAL*
     CONS-TO-SOURCE-FORMATTERS:*FIREBIRD-SQL-NO-UTF*)
    (:export 
     " 
     ; система
     BUDDEN:CMD-C ; выполнить команду операционной системы с помощью cmd /c

     ; firebird
     BUDDEN:*ODBC-DATABASE* ; последняя база данных, к к-рой мы подключались или собираемся подключиться
     BUDDEN:CONNECT-POLIR-GENERAL ; подключиться к базе данных. Все подключения делать только через эту функциюG
     BUDDEN:*TRACE-FIREBIRD* ; если t, выводит все запросы к SQL на консоль
     BUDDEN:*IGNORE-FIREBIRD-ERRORS* ; если t, игнорирует ошибки firebird
     BUDDEN:ASSERT-CONNECTED-TO-DATABASE ; проверить, что подключены к БД, с возможностью перезапуска

     BUDDEN:CALC-DB-CODE-IF-MAY ; специально для вставки в док-ты кода, зависимого от возможности чтения БД

     BUDDEN:*M-ALIASES* ; экспорт нужен, чтобы в рамках рабочего процесса сервера связать эту перем в 0. 

     BUDDEN:FCMD  ; выполнить команду sql, заданную в виде строки, например (fcmd \"update operation set id=0 where id<0\")
     BUDDEN:FSEL  ; выполнить запрос sql, заданный в виде строки
     BUDDEN:FSEL1 ; то же, но возвращает записи в виде (:имяполя1 значение1 :имяполя2 значениеполя2 ...)
     BUDDEN:FSEL* ; то же, но выводит в таблицу tk, см. примечание к budden:fsg
     BUDDEN:FSEL-TO-EXCEL ; выводит результат запроса в Excel

     ;; ------------------------------ ридмакросы для работы с SQL не в виде строки, а в виде непосредственно заданного текста ---------------------
     ; Можно использовать подстановки, а именно
     ;(fse id from product where short_name=:x;
     ;     ) ; где x - переменная лиспа. 
     ;(fse ~~operation_sql_rec^LIST -- подставляется выражение лисп. 
     ; Например, здесь будут перечислены через запятую все поля таблицы operation
     ;     from operationshow where id=0;
     ;    ) 
     ;  закрывающую скобку ставить после переноса строки. 
     ; в листенере можно делать запрос без скобок, он заканчивается точкой с запятой и Enter-ом: select 1 from dual; 
     
     BUDDEN:FCM ; выполнить команду (аналогично fcmd)
     BUDDEN:FSE ; запрос, возвращающий набор данных, напр, fse count(1) from party;
     BUDDEN:FSE1 ; то же, но возвращает записи в виде (:имяполя1 значение1 :имяполя2 значениеполя2 ...)
     BUDDEN:FSG ; запрос вывести в грид с помощью tk (на большом наборе данных зависнет, в этом случае не закрывать tk, 
                ; а прервать лисп-процесс, который выводит данные. Иначе лисп зависнет тоже. 
     BUDDEN:FSP ; вызова хрпр, напр, fsp do_something;

     BUDDEN:FBODY ; читает тело firebird
     ; не работает, поэтому не экспортируем BUDDEN:FBODY-FT ; читает тело firebird и вставляет пометки для отладки после каждой строки, чтобы знать, где упало
     BUDDEN:FBODY-SQ ; читает тело firebird, параметры с одинарными кавычками
     BUDDEN:PBODY ; читает тело pascal 
     BUDDEN:FFSEL ; подобие for select ... into ... do 
     BUDDEN:FFSELR ; то же, но поля формируются статически в виде биндингов

     BUDDEN:COPY-SINGLE-RECORD-FROM-DATABASE-TO-DATABASE 
     BUDDEN:COPY-TABLE-FROM-DATABASE-TO-DATABASE

     ;;---------------------------------- Метаданные SQL -------------------------
     BUDDEN:GET-TBL-ABBREV ; получить краткое наименование таблицы

     BUDDEN:*M_FT_ON* ; не работает. 
     BUDDEN:*M_PRTKL_ON* ; внутренняя переменная. если истина, то выражения M_PRTKL, M_PRTKL_EXPRS в телах хрпр разворачиваются в вывод prtkl_zapisat, к-рая делает вывод
                         ; во внешн. таблицу сообщений
                         ; иначе - они разворачиваются в nop, т.е. ничего не делают
                         ; не используйте эту переменную напрямую, используйте вместо неё параметр m_prtkl_on в def-stored-procedure
     BUDDEN:MOVE-NAME-TO-FB-PACKAGE ; сделать, чтобы данный символ или имя стали внешним символом пакета :fb

     BUDDEN:ALTER-TABLE 
     BUDDEN:DEF-STORED-PROCEDURE ; определить хрпр.
     BUDDEN:DEF-VIEW
     BUDDEN:DEF-TRIGGER
     BUDDEN:DEF-FB-PATCH ; см. :def-fb-patch-implementation
     BUDDEN:INF ; читатель инфиксных выражений (символ-ридмакрос) inf(if (1==1) then 2+2)
     budden:here-document
     budden:here-documentc
     BUDDEN:S<<<  ; символ-ридмакрос: альтернативный синтаксис для строки S<<< a b c>>> = \"a b c\", обратите внимание на пробел после S<<<
     BUDDEN:*ALL-USER-ROLES*

     ; метаданные firebird
     BUDDEN:MAKE-SQL-REC
     BUDDEN:MAKE-REF-SET-SQL-REC
     BUDDEN:LIST-TBL-FIELDS-2 ; перечисляет поля, как они заданы в словаре
     BUDDEN:LIST-NONDICT-TBL-FIELDS-2 ; перечисляет поля таблицы, читая их из метаданных firebird. Работает и для таблиц, 
                         ; не указанных в словаре
     BUDDEN:NON-DICT-TBL-SQL-REC

     BUDDEN:FQ ; обработать идентификатор firebird. Например, если он кирллический, закавычить его. 

     BUDDEN:FB-DOMAIN-TO-TYPE
     BUDDEN:FIELD-TYPE

     BUDDEN:X-TO-INLINE-SQL ; псевдопараметризация. Взять лисповое значение и подставить его в текст sql-запроса

     ;; взаимодействие с Delphi ---------------------------------------------------------------------------------------------
     BUDDEN:MAKE-ALL-SIMPLE-INTERFACES ; порождает conspr.nod и wrapper.nod. 
     BUDDEN:CLI-MEM-STATS ; используется совместно с функцией Дельфи objectCountStats для поиска утечек экземпляров TObject и наследников

     ; ноды. По идее, эти символы должен экспортировать пакет nod, но до этого далеко
     BUDDEN:NTH-NAME
     BUDDEN:ASSO2    ; временно, пока nod не отвязали. СМ. пакет :nod
     BUDDEN:SETASSO2 ; временно, пока nod не отвязали
     BUDDEN:PRINT-NOD-FOR-PASCAL-READER-2 ; Печатать в лиспообразном виде. Временно, пока nod не отвязали. СМ. пакет :nod
     BUDDEN:PRINT-NOD-FOR-PASCAL-READER-2-NO-KEYARGS ; внутренняя, к ней методы определяем
     BUDDEN:LIST-TO-Q ; Есть список, полученный печатью q... Превращает его обратно в q.... Печатать нод: (pds-pascal (list-to-q '(\"a\" \"a\")))
     BUDDEN:+THE-NOD-MERGE-KILLER+
     BUDDEN:Q1
     BUDDEN:Q1-P
     BUDDEN:Q2
     BUDDEN:Q2-LONG
     BUDDEN:Q2-P
     BUDDEN:Q3
     BUDDEN:Q3-P
     BUDDEN:READNOD2
   ; Печатать нод как исходник Pascal: (princ (pds-pascal (list-to-q '(a 2))))
   ; ИЛИ (princ (pds-pascal '(q2 2)))
     BUDDEN:PDS-PASCAL
     BUDDEN:KEYWORDIZE-NOD



     ; дата и время
     budden:TOMORROW
     budden:date-yyyymmdd
     budden:dd-mm-yyyy-to-date

     ; разное
     budden0::EXCLUSIVE-OR
     budden:deftemppackage
     BUDDEN:CLEAR-LEXEM-POSITION-CACHE ; очистить для данного файла

     budden:iter ; из iterate
     budden:dsetq ; из iterate
     perga-implementation:perga
     budden:Окр

     BUDDEN:CONVERT-TIMESTAMP-FOR-PRINT-NOD-FOR-PASCAL-READER-2

     ")
        
     (:forbid #:@NON-NEGATIVE-DECIMAL #:@MAYBE-EVERYTHING #:LISP-IDENTIFIER #:CONSTITUENT-CHAR #:@SQL-SHORT-COMMENT #:@IDENTIFIER #:@WHITESPACE #:@REG-WHITESPACE #:@REG-IDENTIFIER)
     (:custom-token-parsers budden-tools::convert-carat-to-^)
     )

