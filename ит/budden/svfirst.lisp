(asdf::of-system :budden)
;; Copyright (C) Денис Будяк 2015

(def-merge-packages::!
 :SVFIRST
 (:always t)
 (:use :cl)
 (:export 
  "
   SVFIRST:SVFIRST
   SVFIRST:SVSECOND
   SVFIRST:SVTHIRD
   SVFIRST:SVFOURTH
   SVFIRST:SVFIFTH
   SVFIRST:SVSIXTH
   SVFIRST:SVSEVENTH
 ")
 )

(in-package :svfirst)

(defun svfirst (x) (svref x 0))
(defun svsecond (x) (svref x 1))
(defsetf svsecond (x) (elt) `(setf (svref ,x 1) ,elt))
(defun svthird (x) (svref x 2))

(defun svfourth (x) (svref x 3))
(defsetf svfourth (x) (elt) `(setf (svref ,x 3) ,elt))

(defun svfifth (x) (svref x 4))
(defun svsixth (x) (svref x 5))
(defsetf svsixth (x) (elt) `(setf (svref ,x 5) ,elt))

(defun svseventh (x) (svref x 6))
