;; -*- encoding: utf-8; coding: utf-8; system :budden; -*- 

(named-readtables::in-readtable :buddens-readtable-a)
(in-package :yar-lexem)

(defvar *terminator-char-found* nil)
(defvar *single-quote-lisp-params* nil "Если установлена в истину, fbody читает :f как параметр из лиспа")
(defvar *aux-term-char-found* nil)

(defvar *firebird-local-macros* nil "локальные макросы firebird, заданные в fbody-macrolet")
(defvar *M_FT_ON* nil "Если t, то после каждой ; записывается уникальное в данном fbody значение в rdb$set_context('USER_SESSION,'M_FT'). Не работает, потому что нужен полноценный парсер, без него портится if")


(defstruct lexer-output ; можно использовать, когда нужно явно отделить список лексем от списка чего-то иного
  lexems ; "k-Список лексем"
  )

(defstruct lexem ; устаревает. Вместо неё будет Лексема
  (text nil #|(make-string 0)|# :type (or string null)) ; FIXME потом убрать null
  (class nil :type symbol) ; :short-comment :long-comment :identifier :string :integer :float :whitespace *классыОпераций* 
  (position nil :type (or null lexem-pos:lexem-pos))
  (contents nil :type k:not-a-cl-cons ; не заполняется для комментариев, букв, whitespace. Для Firebird, возможно, пока не заполняется для строк, хотя надо бы.
           ; для символов с пр-вом имён будет заполняться консом из пр-ва имён и собственно имени символа.
            )
  (whitespace-before nil :type k:list) ; Если есть, то печатаем. 
  (Лексема-дочитана nil :type lisp-bool)
  (Белое-поле-упрятано nil :type lisp-bool)
  )



(defmethod cl:make-load-form ((self lexem) &optional env)
  (make-load-form-saving-slots self :environment env))

(defvar *print-lexem-short* nil "если истина, лексемы печатаются кратко")
(defmethod print-object ((object lexem) (stream t))
  (if *print-lexem-short*
      (print-unreadable-object (object stream)
        (princ (lexem-text object) stream))
    (call-next-method)))
  

(deftype lexem-class () 'symbol)


