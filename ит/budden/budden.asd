;; -*- Mode: Lisp; Encoding: utf-8;  -*-
;; Copyright (C) Денис Будяк 2015

(in-package :asdf)


(defsystem :meta-parse-firebird
  :description "Условное название"
  :serial t
  :depends-on (
               :iterate-keywords :cl-fad :cl-utilities :cl-ppcre :budden-tools :russian-budden-tools :perga               
                :buddens-reader
                :editor-budden-tools
                :lift
                :anaphora
                :local-time
                :kons
                :meta-parse
		)
  :components
  (
   (:file "package-meta-parse-firebird")
   (:file "lexer-vars-and-macros")
   (:file "lexem-utils")
   ))

(defsystem :budden
  :description "Первый день творения. Здесь в безпорядке находится множество кода. Нужно выделять отсюда куски" 
  :serial t
  :depends-on (
               :iterate-keywords :cl-fad :cl-utilities :cl-ppcre :budden-tools :russian-budden-tools :perga               
                :buddens-reader
                :editor-budden-tools
                :lift
                :anaphora
                :local-time
                :kons
                :meta-parse
                :meta-parse-firebird
		)
  :components
  (
   #|(:file "port-bq" :description "Backquote implementation"
    :long-description "named-readtables are used; ',b now have sense")|#
   (:file "package" :description "пакеты :budden, :budden0, :dns.version-magic")
   ;(:file "package-dat1")
   ;(:file "infix")
   (:file "lisp-macroexpand-file"
    :description "неиспользуемый модуль для макроподстановок в файле")
   ; (:file "inf-symbol-readmacro")
   (:file "test-buddens-readmacros")
   (:file "weak-hash-table")
   (:file "cl-ppcre-idioms")
   ;(:file "date-input") 
   (:file "macroexpand-file")
   (:file "svfirst") ; svfirst, svsecond, ... svseventh and some setf expanders
   (:file "win-shell-encoding" :description "win-shell-to-lisp и lisp-to-win-shell")
   ))

