; -*- Encoding: utf-8 ; coding: utf-8 ; system :budden ; -*- 
;; Copyright (C) Денис Будяк 2015

(in-package :budden-tools)
(in-readtable :buddens-readtable-a)

(def-merge-packages::! :YAR-LEXEM
  (:always t)
  (:documentation "Класс лексемы")
  (:use :cl :iterk :budden-tools :russian-budden-tools :meta-parse
        :anaphora :meta-parse-special-chars :lexem-pos
        )
  (:import-from :perga-implementation perga-implementation:perga)
  #+lispworks (:import-from :editor editor::face)
  (:import-from :alexandria alexandria:simple-reader-error)
  (:export "  
   yar-lexem:LEXEM
   yar-lexem:LEXEM-P
   yar-lexem:MAKE-LEXEM-POS
   yar-lexem:MAKE-LEXEM
   yar-lexem:LEXEM-TEXT
   yar-lexem:LEXEM-CLASS
   yar-lexem:LEXEM-POSITION 
   yar-lexem:LEXEM-CONTENTS
   yar-lexem:LEXEM-Белое-поле-упрятано
   yar-lexem:LEXEM-Лексема-дочитана

   yar-lexem:LEXEM-WHITESPACE-BEFORE
   yar-lexem:LEXEM-EQUALP
   yar-lexem:BUILD-UP-LEXEMS-1 ; деструктивно упрятать whitespace в не-whitespace и добавить лексему eof
   yar-lexem:LEXEM-LIST-GENERATOR-GENERATE ; генерировать код из списка лексем
   yar-lexem:MAKE-LEXEM-LIST-GENERATOR  
   yar-lexem:LEXEM-TOGGLE-PRINT-DEBUGGING ; если вызвать с t, то будет удобно смотреть лексемы на печати
   yar-lexem:*PRINT-LEXEM-SHORT*
   yar-lexem:lexem-get-start-b-offset
   yar-lexem:*warn-on-missing-lexem-source*
   yar-lexem:lexem-edit-source
"))  

(def-merge-packages::! :YAR-LEXEM--TFI
  (:always t)
  (:documentation "tfi имена для пакета :yar-lexem")
  (:use :cl :iterk :budden-tools :russian-budden-tools :meta-parse
        :anaphora :meta-parse-special-chars :lexem-pos
        )
  (:import-from :perga-implementation perga-implementation:perga)
  (:import-from :alexandria alexandria:simple-reader-error)
  (:export "  
   yar-lexem--tfi:build-up-lexems-1
   "))

(def-merge-packages::! :META-PARSE-FIREBIRD  
  (:documentation "Код, относящийся к рекурсивному разбору")
  (:use :cl :iterk :budden-tools :russian-budden-tools :meta-parse
        :anaphora :meta-parse-special-chars
        :yar-lexem :lexem-pos
        )
  (:import-from :perga-implementation perga-implementation:perga)
  #+lispworks (:import-from :editor editor::face)
  (:import-from :alexandria alexandria:simple-reader-error)
  (:import-from :cons-to-source-formatters cons-to-source-formatters:*pascal*
   cons-to-source-formatters:*firebird-sql-no-utf*)
  (:export "
   META-PARSE-FIREBIRD:EXTRACT-LOCATION-FROM-FIREBIRD-TRACE 
   YSTOK.META:WITH-LIST-META
   YSTOK.META:WITH-STREAM-META
   YSTOK.META:MATCHIT
   META-PARSE-FIREBIRD:GENERATE-PD-CODE-FROM-FIREBIRD-LEXEMS
   META-PARSE-FIREBIRD:GROUP-CONSTANT-LEXEMS
   META-PARSE-FIREBIRD:FIREBIRD-LEXER-ONLY
   META-PARSE-FIREBIRD:DAT1-LEXER 
   META-PARSE-FIREBIRD:*SINGLE-QUOTE-LISP-PARAMS* ; переменная устанавливается во время чтения fse fcm fsp и т.п.
   META-PARSE-FIREBIRD:число
   META-PARSE-FIREBIRD:CTOI 
   META-PARSE-FIREBIRD:*warn-on-missing-lexem-source*
   META-PARSE-FIREBIRD:LEXEM-EDIT-SOURCE
   META-PARSE-FIREBIRD:Б--Not-a-double-quote
   META-PARSE-FIREBIRD:GENERATE-CODE-FROM-FIREBIRD-LEXEMS
   META-PARSE-FIREBIRD:Б--Alpha-or-underscore-char
   META-PARSE-FIREBIRD:MAKE-FLOAT
   META-PARSE-FIREBIRD:Б--Not-a-single-quote
   META-PARSE-FIREBIRD:Б--Alpha-number-or-underscore-char-p
   META-PARSE-FIREBIRD:Б--Alpha-underscore-dollar-or-colon-char-p
   META-PARSE-FIREBIRD:Б--Alpha-number-underscore-dollar-or-colon-char-p
   META-PARSE-FIREBIRD:*FIREBIRD-LOCAL-MACROS* ; установлена во время чтения fbody и т.п.
   META-PARSE-FIREBIRD:*M_FT_ON*

   META-PARSE-FIREBIRD:FIREBIRD-LEXER-AND-PARSER
   META-PARSE-FIREBIRD:FIREBIRD-UNPARSER
   META-PARSE-FIREBIRD:вставитьТрассировкуFirebird
   META-PARSE-FIREBIRD:ИнструментитьБуферОбменаFirebird

   meta-parse-firebird:maybe-paint-lexem
   meta-parse-firebird:*current-lexer-syntax*
   meta-parse-firebird:lexem-get-start-b-offset
   meta-parse-firebird:lexem-get-end-b-offset

   meta-parse-firebird:*классыОпераций* ; это нужно перенести.
    ")
  (:custom-token-parsers budden-tools::convert-carat-to-^)
  )
   

#| (def-merge-packages::! :META-PARSE-FIREBIRD--TFI  
  (:use :cl :iterk :budden-tools :russian-budden-tools :meta-parse
        :anaphora :meta-parse-special-chars
        :yar-lexem :lexem-pos
        )
  (:import-from :perga-implementation perga-implementation:perga)
  (:import-from :alexandria alexandria:simple-reader-error)
  (:import-from :cons-to-source-formatters cons-to-source-formatters:*pascal*
   cons-to-source-formatters:*firebird-sql-no-utf*)
  (:export "
   META-PARSE-FIREBIRD--TFI:BUILD-UP-LEXEMS-1
    ")
  ) |#
  


