;; -*- encoding: utf-8; coding: utf-8; system :budden -*- 
;; Copyright (C) Денис Будяк 2015
;; Различные неотсортированные функции

(in-package :budden0)

(defun build-partial-index (list index &rest key-args &key test test-not)
  "Ключ элемента списка x - это (elt x index). Создаёт a-список, сопоставляющий
уникальным (в смысле keyargs) значениям ключа те элементы list, у которых такое
значение ключа. При этом, из самих элементов ключ недеструктивно вырезается"
  (declare (ignorable test test-not))
  (iter 
    (:for item in list)
    (:for k = (elt item index))
    (:for cut-item = (remove-if (constantly t) item :start index :count 1))
    (:for group = (apply 'assoc k groups key-args))
    (if group 
        (push cut-item (cdr group))
      (:collect `(,k ,cut-item) into groups))
    (:finally (return
               (iter (:for (car . cdr) in groups)
                 (if cdr
                     (:collect `(,car ,.(nreverse cdr)))
                   (:collect `(,car))))))))

;;; Параметр wait потерялся, т.к. его не поддерживает uiop. видимо, придётся
;;; запускать отдельный тред, что очень радостно в свете желания отказаться от тредов
(defun call-bat (bat args &key (wait t))
  (unless wait
    (cerror "Continue with wait = t" "wait = nil is currently unsupported when calling OS command ~S" (cons bat args)))
  (funcall 'uiop:run-program
         (list* (uiop:native-namestring (cl-user::putq-otnositelqno-kornya-yara "bin\\util\\CallBatFromGuiDetached.exe"))
                bat args)
         ;(dispatch-keyargs-simple wait)
         ))

#+os-windows
(defun cmd-c (format &rest args) (asdf/run-program:run-program
                                  (apply #'format nil format args)
                                  :force-shell t
                                  ))

(defparameter *winmerge-program*
  #+os-windows "c:\\Program Files (x86)\\WinMerge\\WinMergeU.exe"
  #-os-windows "meld")

(defun ПУТЬ-ВНУТРИ--TEMP (путь)
  (cl-user::ubeditqsya-chtq-putq-ne-nachinaetsya-s-razdelitelya-direktorijj путь)
  (perga
   (let врем (uiop/stream:temporary-directory))
   (merge-pathnames путь врем)))


(defun simplify-parameter-for-cmd-file (string)
  "removes all suspicious characters from cmd file parameter by substituting them with underscores"
  (substitute-if #\_ (lambda (ch) (member ch '(#\  #\" #\' #\~ #\% #\$) :test 'char=)) string))


#+os-windows
(defun winmerge-strings (l r &key 
                           (lfile (ПУТЬ-ВНУТРИ--TEMP "yar/.winmerge-left") lfile-supplied-p)
                           (rfile (ПУТЬ-ВНУТРИ--TEMP "yar/.winmerge-right") rfile-supplied-p)
                           levyjj-zagolovok
                           pravyjj-zagolovok
                           (read-result t)
                           external-format)
  "Запускает *winmerge-program* для пары строк. Заголовки не должны содержать пробелов и кавычек (т.е. с ними не должно быть проблем независимо от того, квотит их вызыватель командной строки или нет). Если передать какую-либо из строк как nil, а вместо этого передать файл то будет произведено сравнение с файлом, а не со строкой"
  (perga
    (cond
     (l
      (ensure-directories-exist lfile)
      (apply 'save-string-to-file l lfile (dispatch-keyargs-simple external-format)))
     (lfile-supplied-p
      ; ok
      )
     (t
      (error "Для сравнения нужно передать левую строку или левое имя файла")))
    (cond
     (r
      (ensure-directories-exist rfile)
      (apply 'save-string-to-file r rfile (dispatch-keyargs-simple external-format)))
     (rfile-supplied-p
      ; ok
      )
     (t
      (error "Для сравнения нужно передать правую строку или правое имя файла")))
      
    (let ltitle (and levyjj-zagolovok (simplify-parameter-for-cmd-file levyjj-zagolovok)))
    (let rtitle (and pravyjj-zagolovok (simplify-parameter-for-cmd-file pravyjj-zagolovok)))
    #+lispworks (system:call-system-showing-output
                 (format nil "c:\\utils\\winmerge.bat ~A ~A~A~A"
                         lfile rfile
                         (if ltitle (format nil " /dl ~A " ltitle) "")
                         (if rtitle (format nil " /dr ~A " rtitle) "")
                         ltitle ptitle :wait t))
    #+sbcl
    ; cmd /c нужно, чтобы winmerge показался поверх. Просто call-bat показывает его снизу.
    (perga-implementation:perga
     (let arg-list (list "/c" *winmerge-program* (namestring lfile) (namestring rfile)))
     (when ltitle (_f append arg-list (list "/dl" ltitle)))
     (when rtitle (_f append arg-list (list "/dr" rtitle)))
     (call-bat "cmd.exe" arg-list :wait t))
    (if read-result
        (values
         (budden-tools:read-file-into-string lfile)
         (budden-tools:read-file-into-string rfile)))))

#-os-windows
(defun winmerge-strings (l r &key 
                           (lfile "~/.winmerge-left")
                           (rfile "~/.winmerge-right")
                           levyjj-zagolovok
                           pravyjj-zagolovok
                           (read-result t)
                           external-format)
  #+russian "Запускает *winmerge-program* для пары строк"
  ;; умеем обрабатывать их только под os-windows
  (declare (ignore levyjj-zagolovok pravyjj-zagolovok))
  (perga-implementation:perga
    (apply 'save-string-to-file l lfile (dispatch-keyargs-simple external-format))
    (apply 'save-string-to-file r rfile (dispatch-keyargs-simple external-format))
    (let command-line (format nil "~A ~A ~A" *winmerge-program* lfile rfile))
    (let exit-status (nth-value 2 (uiop:run-program command-line :ignore-error-status t)))
    (unless (member exit-status '(0 1))
      (error "When calling OS command ~S, got exit status ~A" command-line exit-status))
    (if read-result
        (values
         (budden-tools:read-file-into-string lfile)
         (budden-tools:read-file-into-string rfile)))))


#-sbcl (defun diff-strings (l r &key 
                           (lfile "c:\\.winmerge-left")
                           (rfile "c:\\.winmerge-right")
                           external-format)
  #+russian "Запускает diff для пары строк"
  (apply 'save-string-to-file l lfile (dispatch-keyargs-simple external-format))
  (apply 'save-string-to-file r rfile (dispatch-keyargs-simple external-format))
  (with-output-to-string (o)
    #+lispworks (system:call-system-showing-output (format nil "cmd /c diff ~A ~A" lfile rfile) :wait t :output-stream o)
    ))

(defmacro destructuring-bind-case (var &rest clauses)
  "паттерн матчинг на базе descturturing bind. Todo: default clause. Порождает сигнал на каждую неудачу. See also swank:dcase."
  (let ((in-binding (gensym "DO-MATCH-CASE-IN-BINDING")) ; заводим временный идентификатор
        (the-var (gensym "VAR"))) ; и ещё один
    `(; это уже начался шаблон того кода, к-рый мы генерим
      let ((,the-var ,var) ; вычисляем var и присваиваем его переменной, чтобы каждый раз не вычислялось
           (,in-binding t) ; по этому признаку мы определим, что у нас error возник в ходе связывания, а не 
                           ; внутри пользовательского кода
           ) 
       (block do-match-case ; именованный блок, чтобы из него можно было выйти с помощью именованного return
         ,@( ; этот код выполняется во время расширения макроса и его результат подставляется в шаблон
            loop for (lambda-list . body) in clauses ; берём все имеющиеся ветви
                 collect ; для каждой строим кусок кода, который эту ветвь обрабатывает
                 `(handler-case ; каждую из них оборачиваем в блок try...catch
                              (return-from do-match-case ; пытаемся сопоставить с нужным шаблоном и выйти
                                ; здесь вызывается примитив destructuring-bind - он либо сопоставляет, либо рушится
                                (destructuring-bind ,lambda-list ,the-var (setf ,in-binding nil) ,@body))
                    ; если произошла ошибка при связывании, ничего не делаем (идём к следующему случаю). 
                    ; если же ошибка произошла в пользовательском коде, то перепорождаем её
                            (error (err) (unless ,in-binding (error err)))))
         ; снова находимся в шаблоне. Раз ничто не подошло, то скажем об этом пользователю.
         (error "~S matches neither of ~S~%" ,the-var ',(mapcar 'car clauses))))))

(defun grep-docstrings (regexp package)
  "Поиск регулярного выражения по докстрингам выбранного пакета. Почему-то страшно тормозит"
  (declare (optimize (speed 3) (debug 0) (safety 0)))
  (proga
    (let symbols 
      (make-hash-table :size 1000 :test 'eq))
    (iter (:for s in-package package)
      (:for doc = (documentation s 'function))
      (when doc (unless (gethash s symbols)
                  (:count 1 into i-am-working)
                  (when (scan regexp doc)
                    (setf (gethash s symbols) doc))))
      (:finally (print i-am-working)))
    (iter (:for (s doc) :in-hashtable symbols)
      (format t "~%~A ~S" s doc))))

