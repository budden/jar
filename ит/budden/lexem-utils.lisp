;; -*- encoding: utf-8; coding: utf-8; system :budden ; -*- 

(named-readtables::in-readtable :buddens-readtable-a)
(in-package :yar-lexem)

(defvar *warn-on-missing-lexem-source* t)

; устаревает, см. БУКВЫ-И-МЕСТА-В-ФАЙЛЕ::Открыть-исходный-текст-в-редакторе
(defgeneric lexem-edit-source (x))

(defmethod lexem-edit-source ((lexem lexem))
  (perga
    (break "Этот код устарел и не должен вызываться")
    (:lett lexem lexem lexem) 
    (let pathname (lexem-pos-file (lexem-position lexem)))  ;  fixme - custom-token-parsers
    (cond
     (pathname
      (editor-budden-tools:goto-offset pathname (lexem-get-start-b-offset lexem)))
     (*warn-on-missing-lexem-source* 
      (warn "Лексема ~S не содержит информации об исходнике - не могу показать его" lexem))
      )))

