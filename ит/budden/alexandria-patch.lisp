(in-package :alexandria)
;; Copyright (C) Денис Будяк 2015

#+lispworks
(defun simple-reader-error (stream message &rest args)
  (error 'conditions:simple-reader-error
         :stream stream
         :format-control message
         :format-arguments args))
