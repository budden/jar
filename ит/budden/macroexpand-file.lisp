;; -*- encoding: utf-8; coding: utf-8; system :budden ; -*- 
;; Copyright (C) Денис Будяк 2015
(in-package :budden)
(in-readtable :buddens-readtable-a) 
;; Макрорасширение файла
;; 

(defun make-backup-file-name (dst-file &key (relative-backup-dir ""))
  (merge-pathnames 
   (str+ (namestring (path-to-a-file dst-file)) (maybe-add-slash relative-backup-dir))
   (make-pathname 
    :name (pathname-name dst-file)
    :type (str+ (local-time:format-timestring 
                 nil (local-time:now)
                 :format '((:month 2) (:day 2) (:hour 2) (:min 2) (:sec 2)))
                #\.
                (pathname-type dst-file)))))
    
(defun macroexpand-file-with-function (fn src-file &key dst-file (relative-backup-dir nil relative-backup-dir-supplied-p)
                                          no-backup safe-loss-ratio)
  "Аккуратно расширяет макросы в файле. Есть следующие варианты работы:
1. Файл содержит тела макросов, а обработанный текст должен быть записан в dst-file. 
Порядок работы такой: над исходным текстом вызывается
функция fn, ей передаётся содержимое файла src в виде текста и имена файлов src и dst. 
fn должна вернуть строку либо значение nil и какие-то ещё значения, являющиеся ошибками.
Если эта строка совпадает с содержимым файла dst, то ничего не происходит. В противном случае,
делается резервная копия файла dst и в dst записывается результат fn. 
macroexpand-file-with-function в качестве первого значения возвращает nil, если 
fn вернула nil, t, если было преобразование и :skip, если не было преобразования, а также, второе значение, возвращённое fn, 
если fn вернула nil. 
2. dst-file = nil. В этом случае, преобразование происходит над src-file и рез-т 
записывается обратно в src-file. Если при этом размер нового содержимого составляет менее
, чем safe-loss-ratio от исходного, то происходит (cerror). "
  (proga function
    (assert dst-file)
        ; FIXME - Вынести в отдельную функцию вариант, когда dst-file=nil. Слишком опасно, если случайно
        ; dst-file окажется nil, мы попортим src-file
    (let backup-file-name
      (unless no-backup
        (apply 'make-backup-file-name (or dst-file src-file) (dispatch-keyargs-full relative-backup-dir))
        ))
    (let source (read-file-into-string src-file))
    (let old-source 
      (cond
       ((and dst-file (probe-file dst-file))
        (read-file-into-string dst-file))
       (dst-file ; hence (not (probe-file dst-file))
        "")
       (t ; hence dst-file not given - work on src-file
        source)))
    (multiple-value-bind (new-source error)
        (funcall fn source :src-file src-file :dst-file dst-file))
    (cond
     ((null new-source) (return-from function (values new-source error)))
     ((string= old-source new-source)
      (return-from function (values :skip error)))
     )
    (unless no-backup 
      (cond
       (dst-file 
        (when (probe-file dst-file)
          (copy-file dst-file backup-file-name)))
       ((and src-file (probe-file src-file)
             (copy-file src-file backup-file-name)))
       (src-file ; hence (not (probe-file src-file))
        (cerror "unable to make backup of non-existent ~S" src-file))
       (t ; neither file given
        (error "nothing to convert"))))
    (when (and safe-loss-ratio (> (length source) 0))
      (let ratio (/ (length new-source) (length source)))
      (when (< ratio safe-loss-ratio)
        (cerror "Всё в порядке" "new source's length is ~A of old's. Maybe something is wrong?" ratio)))
    (save-string-to-file new-source (or dst-file src-file))
    (values t error)
    ))
                                                         


