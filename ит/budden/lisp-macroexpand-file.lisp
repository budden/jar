;; -*- encoding: utf-8; coding: utf-8; system :budden -*- 
;; Copyright (C) Денис Будяк 2015

(def-merge-packages::!
 :lisp-macroexpand-file 
 (:use :cl :budden-tools)
 (:export 
  "
  LISP-MACROEXPAND-FILE:!  ; Заменяет фрагменты begmark...endmark заданным текстом
  LISP-MACROEXPAND-FILE:LISP-MACROEXPAND-FILE ; deprecated alias
  LISP-MACROEXPAND-FILE:WRITE-BETWEEN-MARKS ; не работает
  "

  ; fixme - это должно работать и в нижнем регистре
  )
 (:always t))

(in-package :lisp-macroexpand-file)


(defun write-between-marks (filename start-mark end-mark &key text #+ignore (error-when-not-found t))
  "Открывает файл filename, находит в файле start-mark, после него находит end-mark, между ними
   удаляет все, что там есть и вставляет text. 
   Если from-beginning, то поиск производится от начала буфера. Возвращает то, что было между маркерами раньше"
  (declare (ignore text))
  (let*
      (beg-found
       end-found
       success
       (chars (with-open-file (inp filename :direction :input)
                (loop for c = (read-char inp nil nil) 
                      unless c do (loop-finish)
                      collect c)))
       (new-chars
         (error "Здесь что-то не так написано")
        #+nil (search-and-replace-seq 'string
         chars
         (lambda (seq beg fin)
           (let1 pos (search start-mark seq :start2 beg :end2 fin :test #'equalp) 
             (when pos 
               (setf beg-found t)
               (incf pos (length start-mark))
               (when (search end-mark seq :start2 pos :end2 fin :test #'equalp) ; если это не сделать, то крыша едет
                 pos)
               )))
         (lambda (seq beg fin) 
           (let1 pos (search end-mark seq :start2 beg :end2 fin :test #'equalp)
             (when pos (setf end-found t))
             pos))
         (lambda (seq beg fin type) (declare (ignore type)) (setf success (concatenate 'string (subseq seq beg fin))) text)
         )))
    (declare (ignore chars))
    (unless success
             (unless beg-found (error "start-mark not found: ~S" start-mark))
             (unless end-found (error "end-mark not found: ~S" end-mark))
             (assert nil))
    (with-open-file (out filename :direction :output :if-exists :supersede)
      (loop for c in new-chars do (write-char c out)))
    success))

(defparameter *mode* :simple) 
  ; Управляет макроэкспандером для исходников на других языках. 
  ; В будущем будет иметь одно из значений:
  ; :simple - вставляет результат 
  ; :final - вставляет результат и уничтожает маркера, с сохранением рез. копии. 
  ; :reversible - вставляет замену и исходный текст, из которой он получен, а также отдельно в виде комментария - 
  ; весь исходник
  ; :compress - вставляет только исходный текст

(defmacro lisp-macroexpand-file (filename &key formatter (close-file t) expansions (endmark "{}")) 
 "Для каждого из expansions вида (begmark text) производит замену в файле file 
  и может закрыть файл. endmark - марка конца."
 `(case1:with-formatter-stack ,formatter
    ,@(mapcar (lambda (x) `(write-between-marks ,filename ,(first x) ,endmark
                                                :close-file nil
                                                :text ,(second x))) expansions)
    (when ,close-file 
      (save-and-write-buffer ,filename))))

