;; -*- Mode: Lisp; Encoding: utf-8;  -*-
;; Copyright (C) Денис Будяк 2015-2017

(in-package :asdf)

(defsystem :meta-parse
  :description "DSL для рекурсивного парсера" 
  :serial t
  :depends-on (
               :iterate-keywords :cl-fad :cl-utilities :cl-ppcre :budden-tools :russian-budden-tools :perga               
                :buddens-reader
                :editor-budden-tools
                :lift
                :anaphora
                :local-time
                :kons
                ; :ЯР.ПОЛНОСТЬЮ-КЕШИРОВАННЫЙ-ПОТОК
		)
  :components
  (
   (:file "package")
   (:file "meta-parse-vars-and-macros")
   (:file "meta-parse-ystok" :description "DSL парсера методом рекурсивного спуска. Не рекомендуется, т.к. неудобно отлаживать. Но уже используется") ; не получилось перенести в budden-tools, поскольку see-packages зависит от budden-tools
   ))

