;;; -*- Mode:Lisp; Encoding:utf-8; coding:utf-8; -*-
;;; Этот файл не является частью никакой системы

;;; Тесты для потоков - см. ../lexer-yar-tests.lisp

(def-merge-packages::! :test-meta-parse
                       (:always t)
  (:use :cl :meta-parse :meta-parse-firebird-special-chars))

(in-package :test-meta-parse)
; (budden-tools::in-readtable :meta-parse-readtable)
(budden-tools::in-readtable :buddens-readtable-a)

; префикс пакета нужен из-за неизвестной баги в читалке или неправильном её использовании
; пакетом meta-parse
(def-trivial-test::! compileit-string-expansion
                     (meta-parse::compileit { #\+ [ #\- !(setq s -1)] [ ] } :subject-type 'string :test 'eql)
  '(OR (YSTOK.META::STRING-MATCH #\+ :TEST EQL)
       (AND (YSTOK.META::STRING-MATCH #\- :TEST EQL) (SETQ S -1)) (AND)))


(defun vname (symbol)
  (and (symbolp symbol) (not (member symbol lambda-list-keywords))))

(defun lambda-list-p (ll &aux var ovar initform svar rest) ;(index `(,ll))
  (with-list-meta (ll)
                  (when (matchit
                         ($ @ (vname var)
                            { [ &OPTIONAL $ { @ (vname ovar)
                            (@ (vname ovar) { [ @ (t initform) { @ (vname svar) [] } ] [ ] } ) } ]
                            [ ]  }
                            { [ &REST @(vname rest) ] [ ] }) )
                    (list var ovar initform svar rest))))

(def-trivial-test::! lambda-list-empty
                     (lambda-list-p '())
  '(nil nil nil nil nil))

(def-trivial-test::! lambda-list-non-empty
  (lambda-list-p '(a b &optional (c 3 cc) &rest d))
  '(b c 3 cc d))

(def-trivial-test::! lambda-list-string
                     (lambda-list-p "kkk")
  nil)


(defun dotted-var-pair-p (ll &aux car cdr)
  (with-list-meta (ll)
    (when (matchit (@ (vname car) . @ (vname cdr)))
      (list car cdr))))


(def-trivial-test::! dotted-var-pair-p-true
                     (dotted-var-pair-p '(a . b))
  '(a b))

(def-trivial-test::! dotted-var-pair-p-false
                     (dotted-var-pair-p '(a . 3))
  nil)


(def-trivial-test::! test-vname-1
                     (with-list-meta ('(a . 4))
                                     (matchit (@(vname) . 4)))
  t)

(def-trivial-test::! test-vname-2
                     (with-list-meta ('(a . 4))
                                     (matchit (@(vname) . @(t))))
  t)


#|(with-list-meta ('(a . 4))
  (matchit (@(vname) . @(lw:false))))|#

(def-trivial-test::! test-vname-3
                     (with-list-meta ('a)
                                     (matchit @(vname)))
  t)

(def-trivial-test::! test-vname-4
                     (with-list-meta (4)
                                     (matchit @(vname)))
  nil)


(defun-to-file:defun-to-file-me-no-pe lambda-k-list-p (ll &aux var ovar initform svar rest) ;(index `(,ll))
  (with-k-list-meta (ll)
                  (when (matchit
                         ($ @ (vname var)
                            { [ &OPTIONAL $ { @ (vname ovar)
                            (@ (vname ovar) { [ @ (t initform) { @ (vname svar) [] } ] [ ] } ) } ]
                            [ ]  }
                            { [ &REST @ (vname rest) ] [ ] }) )
                    (k:list var ovar initform svar rest))))

(def-trivial-test::! lambda-k-list-non-empty
  (lambda-k-list-p 'k:{ a b &optional k:{ c 3 cc k:} &rest d k:} )
  'k:{ b c 3 cc d k:}
  :test 'k:equal
  )

