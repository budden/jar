;; -*- encoding: utf-8; coding: utf-8; system :meta-parse; -*- 
;; Copyright (C) Денис Будяк 2015

(in-package :meta-parse)
(in-readtable :buddens-readtable-a)

#|
[] - последовательность
{} - альтернтаива
$ - повтор
@(предикат x) - связать перем, если предикат и fail иначе
!lisp-code - выполнить код лисп
|#
(defun meta-reader-for-symbol-readmacro (stream symbol)
  (assert (= 1 (length (string symbol))))
  #+nil (print budden-tools::*package-stack*) 
  (meta-parse::meta-reader stream (elt (string symbol) 0))
  #+nil (meta-parse::make-meta :char (elt (string symbol) 0) :form (read stream)))


(def-symbol-readmacro |@| #'meta-reader-for-symbol-readmacro)
(def-symbol-readmacro |$| #'meta-reader-for-symbol-readmacro)
(def-symbol-readmacro |!| #'meta-reader-for-symbol-readmacro)

(defun meta-paren-reader-for-symbol-readmacro (stream symbol) 
  (assert (= 1 (length (string symbol))))  
  (let1 char (elt (string symbol) 0)
    (meta-parse::make-meta :char char
                           :form (read-delimited-list (eswitch (char) (#\[ #\]) (#\{ #\})) stream t))))

(def-symbol-readmacro |[| #'meta-paren-reader-for-symbol-readmacro)
(def-symbol-readmacro |{| #'meta-paren-reader-for-symbol-readmacro)

