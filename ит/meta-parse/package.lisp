;; -*- Mode: Lisp; Encoding: utf-8; System :meta-parse;  -*-

(named-readtables:in-readtable :buddens-readtable-a)

(def-merge-packages::! :META-PARSE-SPECIAL-CHARS
  (:always t) 
  (:nicknames :meta-parse-firebird-special-chars) ; deprecated
  (:use)
  (:documentation "Особые буквы. На них назначены ридмакросы. Нужно аккуратно с возможными коллизиями")
  (:export "
   META-PARSE-SPECIAL-CHARS:\[  ; последовательность
   META-PARSE-SPECIAL-CHARS:\{  ; альтернатива
   META-PARSE-SPECIAL-CHARS:$   ; повтор
   META-PARSE-SPECIAL-CHARS:!   ; ! lisp-code - выполнить код лисп, и fail, если он вернёт nil
   META-PARSE-SPECIAL-CHARS:@   ; @(предикат x) - связать переменную, если предикат, fail - иначе
   "
   )  
  )

(def-merge-packages::! :meta-parse
  (:always t)
  (:nicknames :ystok.meta)
  (:use :common-lisp :budden-tools :meta-parse-special-chars)
  (:import-from :perga-implementation :perga)
  (:intern #:meta-reader #:make-meta)
  (:export
   "
   META-PARSE:%end%
   META-PARSE:%index%
   META-PARSE:%source%
   META-PARSE:%matched%
   META-PARSE:matchit
   META-PARSE:matchit-stream
   META-PARSE:with-string-meta
   META-PARSE:with-list-meta
   META-PARSE:with-k-list-meta
   META-PARSE:with-stream-meta
   meta-parse:*read-char-fn*
   meta-parse:*char-key-fn*
   meta-parse:*last-read-elt*
   meta-parse:*stream-parsing-error-fn*
   meta-parse:Б
   meta-parse:default-stream-parsing-error-fn
   "
   ))

(def-merge-packages::! :meta-parse--tfi
  (:always t)
  (:use :common-lisp :budden-tools :meta-parse-special-chars)
  (:import-from :perga-implementation :perga)
  (:export
   "
   META-PARSE--TFI:do-read-char-fn 
   "
   ))


