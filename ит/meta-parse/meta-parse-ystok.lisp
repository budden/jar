;;; -*- Mode:Lisp; Encoding: utf-8; coding: utf-8; system :meta-parse -*-
;; Это - плохой парсер. Переходим на ЯР.НАНО-ПАРСЕР
;; Copyright (C) Денис Будяк 2015
;;; Ystok Library - Meta-parser implementation
;;; Copyright (c) 2003-2008 Dr. Dmitriy Ivanov. All rights reserved.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Based on:
;;; (1) meta.lisp 1.0.1 (c) 2001 by Jochen Schmidt.
;;; (2) "Pragmatic Parsing in Common Lisp" of Henry G. Baker.
;;;     http://citeseer.ist.psu.edu/baker91pragmatic.html or inside http://portal.acm.org/
;;; Differences:
;;; (1) Changed @(type var) semantic to @(predicate [var]); this makes the parser
;;;	even more pragmatic due to eliminationg the typep calls.
;;;	Special checking of occurence T-predicate is made.
;;; (2) Use decorated %source%, %index%, and %end% external symbols for vars. Vars are special
;;; (3) Replaced typecase by <typepredicate>-p whereever possible.
;;; (4) List match was fixed for top-level patterns and augmented by dotted patterns.

;;; Changed by Budden
;;; (1) fixed stream parsing so that peek-char returns nil on eof instead of generating a error
;;; (2) renamed match back to matchit to avoid clashes
;;; (3) changed readtable to budden-tools readtable style
;;; (4) used budden-tools
;;; (5) added kons support

;;Copyrign  (c) 2001 by Jochen Schmidt.
;;Copyright (c) 2002-2013 Dr. Dmitry Ivanov. All rights reserved.

;;Redistribution and use in source and binary forms, with or without
;;modification, are permitted provided that the following conditions are met: 

;;Redistribution of source code must retain the above copyright notice,
;;this list of conditions and the following disclaimer. 

;;Redistribution in binary form must reproduce the above copyright notice,
;;this list of conditions and the following disclaimer in the documentation
;;and/or other materials provided with the distribution. 

;;THIS SOFTWARE IS PROVIDED BY DMITRY IVANOV ``AS IS'' AND ANY EXPRESS
;;OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
;;OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
;;IN NO EVENT SHALL DMITRY IVANOV OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;;INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;;(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
;;LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
;;AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
;;OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
;;USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-package :ystok.meta)

(in-readtable nil)

;;; Reader

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defstruct meta char form)
  
  #+(or mcl sbcl) 
  (defmethod make-load-form ((self meta) &optional env)
    (declare (ignore env))
    `(make-meta :char ,(meta-char self) :form ',(meta-form self)) )
  (defun meta-reader (stream char) #+nil (print budden-tools::*package-stack*) (make-meta :char char :form (read stream)))
  
  ; (budden-tools::defreadtable :meta-parse-readtable (:merge :standard))
  ;(setf (budden-tools::package-readtable :meta-parse) :meta-parse-readtable)

);eval-when

; (defparameter *meta-readtable* (budden-tools::find-readtable :meta-parse-readtable))

;(dolist (char '(#\@ #\$ #\!))
;  (set-macro-character char #'meta-reader nil *meta-readtable*))

#|
(set-macro-character #\{
                     #'(lambda (stream char)
                         (make-meta :char char :form (read-delimited-list #\} stream t)))
                     nil *meta-readtable*)

(set-macro-character #\[
                     #'(lambda (stream char)
                         (make-meta :char char :form (read-delimited-list #\] stream t)))
                     nil *meta-readtable*)

(dolist (char '(#\] #\}))
  (set-macro-character char (get-macro-character #\)) nil *meta-readtable*))
|#

; (budden-tools::in-readtable :meta-parse-readtable)

;;; Variables used in course of parsing
(defvar %source%)
(defvar %index%) ; только для строк
(defvar %end%) ; только для строк
(defvar %eof-error-p%) ; только для потока - неясно, имеет ли смысл вообще
(defvar %eof-value%) ; значение, которое мы получим в конце потока
(defvar *memoized-stream-elt*) ; то, что мы считали из потока в последний раз
(defvar *last-read-elt*)  ; то, что лексер употребил в последнюю очередь

;;; Настройка парсера только для with-stream-meta и умолчания для них
(defvar *read-char-fn* 'read-char)
;; функция, которая для потока из результата чтения (например, из Пб) извлекает букву
(defvar *char-key-fn* 'identity) 


;; Функция с сигнатурой как у error (может не только выдавать ошибку, но и создавать лексему с классом ошибки)
(defparameter *stream-parsing-error-fn* 'default-stream-parsing-error-fn)

(defun default-stream-parsing-error-fn (datum &rest arguments)
  (declare (ignore datum arguments))
  (error "Error parsing ~S" %source%))


(defun Б (Элемент)
       "Короткая запись для получения буквы из прочитанного элемента (например, из Пб)"
  (funcall *char-key-fn* Элемент))

(defmacro match-test (test x y &key set-matched)
 ;;; Tester form compiler
 (let ((get-x (if set-matched `(setf %matched% ,x) x)))
   (if (or (symbolp test) (and (consp test) (eq (first test) 'lambda)))
       `(,test ,get-x ,y)
     `(funcall ,test ,get-x ,y))))

(defmacro string-match (pattern &key (test 'eql))
  (cond ((characterp pattern)
         `(when (and (< %index% %end%)
                     (match-test ,test (char %source% %index%) ,pattern))
                     ;(eql (char %source% %index%) ,pattern))
            (incf %index%)))
        ((stringp pattern)
         (let ((old-index-symbol (gensym "OLD-INDEX-")))
           `(let ((,old-index-symbol %index%))
              (or (and ,@(map 'list
                              #'(lambda (char) `(string-match ,char :test ,test))
                              pattern))
                  (progn (setq %index% ,old-index-symbol) nil)))))))

(defmacro string-match-type (predicate &optional var)
  (let ((char-symbol (gensym)))
    `(when (< %index% %end%)
       ,(if var
            `(let ((,char-symbol (char %source% %index%)))
               (declare (type character ,char-symbol))
               (when (,predicate ,char-symbol)  ;(typep ,char-symbol ',pattern)
                 (setq ,var ,char-symbol)
                 (incf %index%)))
            `(when (,predicate (char %source% %index%))
               (incf %index%))))))

;;; Lists: functions list-match[-type] should eat elements by poping them form %source%

(defun compile-list (pattern &key (test 'eql))
  (if (atom pattern)					; end of the list pattern
      (if (meta-p pattern)				; is it dotted ending with meta?
          (compileit pattern :subject-type :self :test test)
          `(match-test ,test %source% ',pattern :set-matched t))
      `(and ,(compileit (first pattern) :subject-type 'list :test test)
            ,(compile-list (rest pattern) :test test))))

(defmacro list-match (pattern &key (test 'eql) self)
  (if self						; pattern is for the entire subject
      (if (atom pattern)		 		; kept in %source% itself
          `(match-test ,test %source% ',pattern :set-matched t)
          (compile-list pattern :test test))
      `(when (and (consp %source%)	 		; pattern is for the first element
                  ,(if (atom pattern)			
                       `(match-test ,test (first %source%) ',pattern :set-matched t)
                       `(let ((%source% (first %source%)))	; drill down to the first
                          ,(compile-list pattern :test test)))) ; with new lexical %source%
         (setq %source% (rest %source%))
         t)))

(defmacro list-match-type (predicate &optional var self)
  (if self						; test the type of the entire
      `(,@(if (eq predicate t)				; subject kept in %source% itself
              '(progn)
              `(when (,predicate %source%)))
        ,@(when var `((setq ,var %source%)))
        t)
      `(when ,(if (eq predicate t)
                  '(consp %source%)
                  `(and (consp %source%) (,predicate (first %source%))))
         ,(if var
              `(setq ,var (pop %source%))
              '(setq %source% (rest %source%)))
         t)))


;;; K-Lists: functions k-list-match[-type] should eat elements by poping them form %source%

(defun compile-k-list (pattern &key (test 'eql))
  (if (atom pattern)					; end of the list pattern
      (if (meta-p pattern)				; is it dotted ending with meta?
          (compileit pattern :subject-type 'k-list :test test)
          `(match-test ,test %source% ',pattern :set-matched t))
      `(and ,(compileit (first pattern) :subject-type 'k-list :test test)
            ,(compile-k-list (rest pattern) :test test))))

(defmacro k-list-match (pattern &key (test 'eql) self)
  (if self						; pattern is for the entire subject
      (if (atom pattern)		 		; kept in %source% itself
          `(match-test ,test %source% ',pattern :set-matched t)
          (compile-k-list pattern :test test))
      `(when (and (k:consp %source%)	 		; pattern is for the first element
                  ,(if (atom pattern)			
                       `(match-test ,test (k:first %source%) ',pattern :set-matched t)
                       `(let ((%source% (k:first %source%)))	; drill down to the first
                          ,(compile-k-list pattern :test test)))) ; with new lexical %source%
         (setq %source% (k:rest %source%))
         t)))

(defmacro k-list-match-type (predicate &optional var self)
  (if self						; test the type of the entire
      `(,@(if (eq predicate t)				; subject kept in %source% itself
              '(progn)
              `(when (,predicate %source%)))
        ,@(when var `((setq ,var %source%)))
        t)
      `(when ,(if (eq predicate t)
                  '(k:consp %source%)
                  `(and (k:consp %source%) (,predicate (k:first %source%))))
         ,(if var
              `(setq ,var (k:pop %source%))
              '(setq %source% (k:rest %source%)))
         t)))

;;; Есть искушение отслеживать позицию здесь. Но мы не можем это сделать, если строки
;;; являются строками одуванчика. Мы можем это сделать только если они являются числами. 
;;; Поэтому обязанность подсчёта строк возлагается на поток. 
(defun do-read-char-fn-and-memoize (stream eof-value)
  (let* ((result (funcall *read-char-fn* stream nil eof-value nil)))
    (setf *memoized-stream-elt* result)))

(defun stream-look-ahead ()
  (assert (not (eq %eof-value% 'Не-определён)))
  (cond
   ((eq *memoized-stream-elt* 'Не-определён)
    (do-read-char-fn-and-memoize %source% %eof-value%))
   (t
    *memoized-stream-elt*)))

(defun stream-consume ()
  "Забирает объект из потока, записывает в *last-read-elt* и возвращает"
  ;; есть сомнения в нужности stream-look-ahead
  (stream-look-ahead)
  (cond
   ((and %eof-error-p%
             (eq %eof-value% *memoized-stream-elt*))
    (funcall *stream-parsing-error-fn* %source%))
   (t
    (setf *last-read-elt* *memoized-stream-elt*
          *memoized-stream-elt* 'Не-определён)
    *last-read-elt*)))

(defmacro stream-match (pattern &key (test 'eql))
  `(when (match-test ,test (funcall *char-key-fn* (stream-look-ahead)) ,pattern)
     (stream-consume)))

(defmacro stream-match-type (predicate &optional var)
  "Для тестов с предикатами, типа (my-pred my-var) мы не извлекаем букву, чтобы иметь полную информацию в my-var"
  `(when (,predicate (stream-look-ahead))
     ,(if var
          `(setq ,var (stream-consume))
          `(stream-consume))))

(defun compileit (pattern &key subject-type (test 'eql))
  (if (meta-p pattern)
      (ecase (meta-char pattern)
        (#\! (meta-form pattern))
        (#\[ `(and ,@(mapcar (lambda (form)
                               (compileit form :subject-type subject-type :test test))
                             (meta-form pattern))))
        (#\{ `(or ,@(mapcar (lambda (form)
                              (compileit form :subject-type subject-type :test test))
                            (meta-form pattern))))
        (#\$ `(not (do () ((not ,(compileit (meta-form pattern)
                                            :subject-type subject-type
                                            :test test
                                            ))))))
        (#\@ (let ((form (meta-form pattern)))
               (ecase subject-type
                 (:self 
                  `(list-match-type ,(first form) ,(second form) ,t))
                 (k-self 
                  `(k-list-match-type ,(first form) ,(second form) ,t))
                 (string
                  `(string-match-type ,(first form) ,@(when (second form) `(,(second form)))))
                 (list
                  `(list-match-type ,(first form) ,@(when (second form) `(,(second form)))))
                 (k-list
                  `(k-list-match-type ,(first form) ,@(when (second form) `(,(second form)))))
                 (stream
                  `(stream-match-type ,(first form) ,@(when (second form) `(,(second form)))))))))
      (ecase subject-type
        (:self `(list-match ,pattern :test ,test :self ,t))
        (k-self `(k-list-match ,pattern :test ,test :self ,t))
        (string `(string-match ,pattern :test ,test))
        (list `(list-match ,pattern :test ,test))
        (k-list `(k-list-match ,pattern :test ,test))
        (stream `(stream-match ,pattern :test ,test)))))


;;; Args: test - keyword argument
;;; - is evaluated when passed to outer with-...-meta
;;; - is not evaluated when passed to inner matchit!

(defmacro matchit-stream (pattern &key (test 'eql))
  "Для использования matchit вне лексического контекста with-stream-meta (но внутри динамического)"
  (compileit pattern :subject-type 'stream :test test))

(defmacro with-stream-meta ((stream &key (test 'eql) (eof-error-p t) (eof-value #\Null) block-name) &body body)
  "Похоже, что eof изображается как nil"
  `(let ((%source% ,stream)
         (%eof-error-p% ,eof-error-p)
         (%eof-value% ,eof-value)
         (*last-read-elt* 'Не-определён)
         (*memoized-stream-elt* 'Не-определён)
         )
     (perga
      ,block-name
      (macrolet matchit (pattern &key (test ',test))
          (compileit pattern :subject-type 'stream :test test))
      ,@body)))

(defmacro with-string-meta ((string &key (start 0) end (test 'eql)) &body body)
  `(let* ((%source% ,string)
          (%index% ,start)
          (%end% (or ,end (length %source%))))
     (declare (fixnum %index% %end%) (type simple-string %source%))
     (macrolet ((matchit (pattern &key (test ',test))
                  (compileit pattern :subject-type 'string :test test)))
       ,@body)))

(defmacro with-list-meta ((list &key (test 'eql)) &body body)
  "Test is applied to two arguments: 1-object from the list, 2-pattern from the parser code"
  `(let ((%source% ,list)
         (%matched% nil))
     (declare (ignorable %matched%))
     (macrolet ((matchit (pattern &key (test ',test))
                  (compileit pattern :subject-type :self :test test)))
       ,@body)))

(defmacro with-k-list-meta ((k-list &key (test 'eql)) &body body)
  "Test is applied to two arguments: 1-object from the list, 2-pattern from the parser code"
  `(let ((%source% ,k-list)
         (%matched% nil))
     (declare (ignorable %matched%))
     (macrolet ((matchit (pattern &key (test ',test))
                  (compileit pattern :subject-type 'k-self :test test)))
       ,@body)))
