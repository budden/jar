; -*- Encoding: utf-8; system  :piston-drive-kinematics -*- 

(named-readtables:in-readtable :buddens-readtable-a)

(def-merge-packages::! :piston-drive-krivoship
                       (:always t)
                       (:use :cl
                        :buddens-symbolic-derivative)
                       (:import-from :budden-tools
                        BUDDEN-TOOLS:in-readtable)
                       (:export "
  piston-drive-krivoship:KrivoshipPistonLaw
  "))

(in-package :piston-drive-krivoship)
                       
(defun-with-symbolic-derivative KrivoshipPistonLaw (x half-stroke shatun-length initial-phase)
                                (Derivative-Of-KrivoshipPistonLaw :documentation " Закон движения кривошипно-шатунного механизма. 
  x - угол поворота коленвала
  initial-phase - угол поворота при t = 0
  half-stroke - половина хода
  shatun-length - длина шатуна. 
  диапазон возвращаемых значений - от 0 до (* 2 half-stroke)")
                                (+ shatun-length
                                   half-stroke
                                   (- (expt
                                       (- (expt shatun-length 2)
                                          (expt
                                           (* half-stroke
                                              (sin (+ x initial-phase)))
                                           2))
                                       0.5))
                                   (- (* half-stroke
                                         (cos (+ x initial-phase))))
                                   )
                                )
  
   
