; -*- Encoding: utf-8; -*-

(in-package :asdf)

(defsystem :piston-drive-kinematics
  :description "Различные приводы" 
  :around-compile (lambda (thunk) (let ((cl:*read-default-float-format* 'cl:double-float)) (funcall thunk)))
  :serial t
  :depends-on (:budden-tools :perga :buddens-reader :sup-lm
               :budden-adw-plotting 
               :m0 ; buddens-symbolic-derivative
               )
  :components
  ((:file "piston-drive-krivoship" :description "Кривошипно-шатунный механизм"))
  )
   
