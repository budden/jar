; -*- Encoding: utf-8; -*-

(in-package :budden-tools)
(in-readtable :buddens-readtable-a)


(def-merge-packages::! :physical-units (:use)
  (:import-from #:common-lisp cl:in-package cl:defconstant #:* cl:pi cl:/ cl:defun cl:+ cl:- cl:apply cl:&rest)
  (:import-from #:budden-tools budden-tools:in-readtable)
  (:export "
  physical-units::[gramm]
  physical-units::[cm3]
  physical-units::[cm2]
  physical-units::[cm]
  physical-units::[gradusov]
  physical-units::[mm]
  physical-units::[liter]
  physical-units::[MPa]
  physical-units::[inch]
  physical-units::[foot]
  physical-units::[minute]
  physical-units::[hour]
  physical-units::[PSI]
  physical-units::[btu]
  physical-units::[kWh]
  physical-units::[ccal]
  physical-units::[lbs]
  physical-units::[Bar]
  physical-units::[atm]
  physical-units::[kgs]
  physical-units::[mm.hg] 
  physical-units::[Celsius] 
  physical-units::[hp]
  physical-units::[Fahrenheit]

  physical-units::[cSt]
  physical-units::[cP]
  "))
