; -*- Encoding: utf-8; system :m0 -*-

(def-merge-packages::! :warn-once
                       (:always t)
                       (:use :cl :budden-tools)
                       (:export
  "
  warn-once:warn-once
  warn-once:start-warnings-over
  "))

(in-package :warn-once)

(defvar *warnings-shown-once* (make-hash-table :test 'equal)
  "Содержит сообщения, которые уже один раз показывали с помощью warn-once")

(defun start-warnings-over ()
  "Теперь warn-once снова покажет все ворнинги"
  (clrhash *warnings-shown-once*))

(defun warn-once (s)
  (cond
   ((gethash s *warnings-shown-once*)
    )
   (t
    (setf (gethash s *warnings-shown-once*) t)
    (warn s))))
