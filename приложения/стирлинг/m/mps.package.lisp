; -*- Encoding: utf-8; System :m -*-

(named-readtables:in-readtable :buddens-readtable-a)

(def-merge-packages::! :mps
  (:use :cl :physical-units :mjr_mat :mjr_vec :m0
   )
  (:local-nicknames
   :str :fluid-stream :hx :heat-exchanger)
  (:import-from :perga-implementation
   perga-implementation:perga)
  (:import-from :budden-tools
   budden-tools:_f
   budden-tools:show-expr
   budden-tools:the*
   budden-tools:deftvar
   budden-tools:deftparameter
   BUDDEN-TOOLS:def-symbol-readmacro
   budden-tools:in-readtable
   budden-tools:str++
   budden-tools:|^|
   budden-tools:defun-to-file
   budden-tools:mandatory-slot
   budden-tools:byref
   budden-tools:with-byref-params
   BUDDEN-TOOLS:mlvl-list
   BUDDEN-TOOLS:mlvl-bind
   budden-tools:in-readtable
   )
  (:custom-token-parsers budden-tools::convert-carat-to-^) 
  (:import-from :iterate-keywords
   iterate-keywords:iter)
  (:import-from :alexandria.0.dev
   ALEXANDRIA.0.DEV:copy-array
   alexandria.0.dev:eswitch)
  (:import-from :buddens-symbolic-derivative
   buddens-symbolic-derivative:defun-with-symbolic-derivative)
  (:import-from :hx-and-str
   HX-AND-STR:MeanEffectiveTemperature)
  (:import-from :piston-drive-krivoship
   PISTON-DRIVE-KRIVOSHIP:KrivoshipPistonLaw)
  (:export  ; mps-utils.lisp
   "mps::EngineTypes 
    mps::alpha
    mps::beta
    mps::gamma
  
    mps::*Cells* ; Cells of engine with heat input laws
    mps::*Temp* ; temperature. Defined in :g1 system

   "
   )
  (:export ; functions defined in g1
   "mps::NoHeatIn
    mps::MakeSimpleHeatInFn
    mps::RegenOutT
    mps::HxHeatIn
   "
   )
  (:export ; (editor-budden-tools::steal-definitions-from-file-for-export "c:\\stir\\sw\\m\\mps-default.lisp")
   "
 MPS:WorkingFluidStreamType
 MPS::WorkingFluidStreamType-Cv
 MPS::WorkingFluidStreamType-GammaGas
 MPS::WorkingFluidStreamType-RGas
 MPS::WorkingFluidStreamType-Cp
 MPS::WorkingFluidStreamType-LAMBDA
 MPS::WorkingFluidStreamType-RHO
 MPS::WorkingFluidStreamType-NU
 MPS::WorkingFluidStreamType-ThermalExpansionCoefficient
 MPS::WorkingFluidStreamType-Temperature
 MPS::WorkingFluidStreamType-Pressure
 MPS::WorkingFluidStreamType-VolumetricFlow
 MPS::WorkingFluidStreamType-P
 MPS::MAKE-WorkingFluidStreamType
 mps:*th*
 mps:*tk*

 MPS:*InitialPressure*
 mps::*CrankcaseInitialPressureFactor*
 mps:*freq*
 mps:*times*
 mps:*n*
 mps:+e
 mps:+h
 mps:|+R1|
 mps:|+R2|
 mps:+k
 mps:+c
 mps::+b ; картер
 MPS:*HotCapCreep*
 mps:*kwr*
 MPS:*RegenDomeMinWallThickness*
 MPS:*RegenDomeDout*
 MPS:*DispMinWallThickness*
 MPS:*RegenDomeDIn*
 MPS:*RegenDomeLength*
 MPS:*DisplacerShuttleLength*
 MPS:*HeaterOuterDiameter*
 MPS:*hotCapDensity*
 MPS:*genDiam*
 MPS:*Xi1Factor*
 mps:*mvr-limit-factor*
 mps:*dmvr-limit-factor*
 mps:*dmvr-to-mvr-099-time-factor*
 mps:*y-delta-abs-max-factor*
 mps:*y-err-abs-max-factor*
 MPS::*EngineType*
 mps::*SimpleGapRegenerator*
 MPS::*dBore*
 MPS::*dStroke*
 MPS::*dhotClearance*
 MPS::*dhotSideRadialClearance*
 MPS::*dcoldClearance*
 MPS::*dRodBore*
 mps::*required-phase*
 mps::*phase*
 MPS::*wBore*
 MPS::*wStroke*
 MPS::*wTopClearance*
 MPS::*BetaCylinderShift*
 MPS::*HotCollectorVolume*
 MPS::*NoOfRegens*
 MPS::*CoolerDuctDiameter*
 MPS::*CoolerDuctLength*
 MPS::*ColdCollectorVolume*

 MPS::*CrankcaseMaximumVolume*
 mps::*PistonSealHoleDiameter*
 MPS::*LubricatingOilKinematicViscosity*
 MPS::*PistonRingHeight*
 MPS::*PistonRingFrictionCoefficient*
 MPS::*PistonRingInitialPressure*
 MPS::*CrossheadFrictionCoefficient*

 MPS::SetCells 
 mps::SetDoubleActingKrivoshipAlphaVolumeLaws ; will be defined in :g1 system
 mps::SetSinEngineVolumeLaws  ; will be defined in :g1 system
 mps::SetRossYokeEngineVolumeLaws ; will be defined in :g1 system
 MPS::SetEngineVolumeLaws ; edit this function to set volume laws.

 mps::*RollerBearingCr* ; статическая нагрузка, http://www.tehburo.ru/index.php?id=255&Itemid=99999999&option=com_content&task=view
 mps::*RollerBearingCor* ; динамическая, там же. 
 MPS::*RollerBearingInnerDiam* ; внутренний диаметр отверстия подшипника, для расчёта трения
 mps::*RollerBearingFnp* ; условный коэфт трения.

 mps::*RollerBearingAvgDiam* ; средний диаметр тел качения


 mps::*CurrentParameters* ; текущие значения для массового расчёта / current values for batch calculations
 
")
  )
