; -*- Encoding: utf-8; System :m -*-

(named-readtables:in-readtable :buddens-readtable-a)

(def-merge-packages::! :m
  (:use :cl :physical-units :mjr_mat :mjr_vec :m0
   :budden-adw-plotting :mps)
  (:local-nicknames
   :str :fluid-stream :hx :heat-exchanger)
  (:import-from :perga-implementation
   perga-implementation:perga)
  (:import-from :budden-tools
   budden-tools:_f
   budden-tools:show-expr
   budden-tools:the*
   budden-tools:deftvar
   budden-tools:deftparameter
   BUDDEN-TOOLS:def-symbol-readmacro
   budden-tools:in-readtable
   budden-tools:str++
   budden-tools:|^|
   budden-tools:defun-to-file
   budden-tools:mandatory-slot
   budden-tools:byref
   budden-tools:with-byref-params
   BUDDEN-TOOLS:mlvl-list
   BUDDEN-TOOLS:mlvl-bind
   )
  (:custom-token-parsers budden-tools::convert-carat-to-^) 
  (:import-from :iterate-keywords
   iterate-keywords:iter)
  (:import-from :alexandria.0.dev
   ALEXANDRIA.0.DEV:copy-array
   alexandria.0.dev:eswitch)
  (:import-from :buddens-symbolic-derivative
   buddens-symbolic-derivative:defun-with-symbolic-derivative)
  (:import-from :hx-and-str
   HX-AND-STR:MeanEffectiveTemperature)
  (:import-from :piston-drive-krivoship
   PISTON-DRIVE-KRIVOSHIP:KrivoshipPistonLaw)
  (:export
   " ; экспортируем не те символы, к-рые нужны пользователю, 
     ; а те, к-рые наиболее нужны разработчику, чтобы
     ; здесь был путеводитель по ним. Потом надо убрать
     ; и переместить куда-то ещё
    m:*package-documentation*
    m:*tim* ; время, за к-рое коленвал повернётся на мин. угол
    m:*times* ; колво поворотв на мин.угол в вызове m:s
    m:call-ode ; вызов решателя для поворота на минимальный угол 

    m:bvalue  ; устанравливает нач. значения
    m:diffur2 ; вычисляет правую часть ур-я
    m:s ; поворачивает коленвал на минимальный угол
    m:stat
    m:simulate
    m:circles ; запускает машину на несколько обороток
    m:do-plots ; рисует картинки
    m:SetCells ; инициализирует ячейки
    m:+e ; индексы
    m:+h ; ячеек  
    m:+r1 ; регенератор разбит на два
    m:+r2 
    m:+k
    m:+e

    m:HeatIn ; подача тепла в ячейку
    M::*HeaterOuterDiameter*
    M::WorkingFluidStreamType

    m::rc ; функция для запуска моделирования
          ; function to run modeling

    m::time-to-ti 
   "
   ; (extract-col-from-matrix *dQS* +h) - колонка данных по хитеру
   )
  (:export
   "m::OptimumPhaseForBeta
    m::OptimumWorkingStrokeForBeta
    ")
  )






