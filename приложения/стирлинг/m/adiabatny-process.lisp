; -*- Encoding: utf-8;  System :m -*-
;; Адиабатный процесс идеального газа
;; Adiabate process of ideal gas

(in-package :m)
(in-readtable :buddens-readtable-a)

(defun TAd (TInitial PInitial PFinal gamm)
  (cond
   ((or (minusp PInitial) (minusp PFinal))
    TInitial)
   (t
    (* TInitial
       (expt (/ PInitial PFinal)
             (/ (- 1 gamm) gamm))))))

(defun VAd (TInitial PInitial PFinal VInitial gamm)
  (/ (* PInitial VInitial (TAd TInitial PInitial PFinal gamm))
     (* TInitial PFinal)))


(defun WAd (TStart TEnd Mass Cv)
  (* (- TStart TEnd) Mass Cv))


(defun PAd (TInitial PInitial VInitial VFinal gamm)
  "FIXME deriva a forumla for it!"
  (let ((mjr_cmp:*mjr_cmp_eps* least-positive-double-float))
    (mjr_nleq:mjr_nleq_root-bsect
     (lambda (p) (- VFinal (VAd TInitial PInitial p VInitial gamm)))
     (/ PInitial 100)
     (* PInitial 100)
     :xeps (* PInitial 1e-15)
     :yeps (* VInitial 1e-15)
     ; :show-progress t
     )))

