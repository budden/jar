; -*- Encoding: utf-8;  System :g1 -*-

(in-package :m)
(in-readtable :buddens-readtable-a)


(defun CycleStatByColumnMultiplier ()
  (perga
    (:lett res mjr_mat
     (mjr_mat_make-const 1 (length *btS*) 1.0))
    ; (dotimes (i (mjr_mat_cols res))
    (setf [ res 0 0 ] 0.0)
    res))

(defun CellInAvgTemperature (ci left-p)
  "Определяем среднюю т-ру потока, входящего в ячейку регенератора ci. Если left-p, то слева (со стороны нагревателя), иначе - со стороны холодильника. Потом отнимем от неё дельту на конечность КПД регена и получим т-ру левой стороны регена. На самом деле мы должны брать не среднюю т-ру, а тепло, передаваемое самому левому слою сетки регена, оно зависит ещё и от коэфта теплоотдачи. Но пока пусть так"
  (perga
    (let index (if left-p (- ci 1) ci))
    (let filter-fn ; ф-я, к-рая определяет, что поток входит в реген.
      (if left-p
          (lambda (x) (min x 0))
        (lambda (x) (max x 0))))
    (let mvr1 (extract-col-from-matrix *mvrS* index)) ; что выходит из регена в награватель (или что входит, с минусом). 
    (let MinusRegenRightMassIn (map 'vector filter-fn mvr1))
    (setf [ MinusRegenRightMassIn 0 ] 0) ; убираем лишнюю точку
    ; что входит в реген, со знаком 
    (let tr1 (extract-col-from-matrix *movedrTS* index))
    (let VectorOfmt (map 'vector '* MinusRegenRightMassIn tr1))
    (let SumOfmt (sum-of-vector-elements VectorOfmt))
    (let SumOfm (sum-of-vector-elements MinusRegenRightMassIn))
    (if (= 0 SumOfmt)
        0.0
      (/ SumOfmt SumOfm))))


(defun CycleWork ()
  (perga
    (:lett res dbl 0.0)
    (dotimes (i *n*)
      (_f + res
          (IntegrateByCycle
           (lambda (tindex) [ *dWS* tindex i ])))
      )
    res))

(defun CycleWork3 ()
  "Они расходятся. Верим, что это связано с перепадами давления в ячейках, возникающих из-за структуры уравнения, а также из-за того, что давление и dV берутся в разные моменты времени (хотя вторую причину мы должны были победить вроде)"
  (perga
    (flet avgtime (i)
      (* 0.5 (+ [ *btS* i ]
                [ *btS*
                  (mod (+ i 1) (length *btS*))
                  ])))      
    (+ (IntegrateByCycle
        (lambda (tindex) (* [ *pS* tindex +e ]
                            ( dVE (avgtime tindex) ))))
       (IntegrateByCycle
        (lambda (tindex) (* [ *pS* tindex +c ]
                            ( dVC (avgtime tindex) ))))
                            )))



(defun StatGrossEfficiency ()
  (perga
    ; при вычислении статистики не забываем, что в матрицах результатов последняя и первая строчка "дублируются", 
    ; т.е., соответствуют углу поворота коленвала 0.
    
    (:lett CycleWork dbl (CycleWork))

    (:lett HeatInByColumn mjr_vec
     (mjr_mat_m2cv
      (mjr_mat_transpose
       (mjr_mat_*
        *tim*
        (CycleStatByColumnMultiplier)
        *dQS*))))
    
    ;(terpri)
    (DONT-show-expr-t HeatInByColumn)
    
    (:lett HeatIn dbl
     (+
      [ HeatInByColumn +e ]
      [ HeatInByColumn +h ]
      (abs [ HeatInByColumn +r1 ])))

    (setf *InAvgTemperatureLeft* (make-array (list *n*) :element-type 'dbl :initial-element 0.0))
    (setf *InAvgTemperatureRight* (make-array (list *n*) :element-type 'dbl :initial-element 0.0))

    (do-for (ci 1 (- *n* 1))
      (setf [ *InAvgTemperatureLeft* ci ]
            (CellInAvgTemperature ci t)))

    (do-for (ci 0 (- *n* 2))
      (setf [ *InAvgTemperatureRight* ci ]
            (CellInAvgTemperature ci nil)))

    (DONT-show-expr-t *InAvgTemperatureLeft*)
    (DONT-show-expr-t *InAvgTemperatureRight*)

    (setf *GrossEfficiency* (/ CycleWork HeatIn))
    (show-expr-t *GrossEfficiency*)
    *GrossEfficiency*))


(deftparameter *MetalUslovnyHeatCapacity* dbl
               :initial-value 0.3
               :documentation "Когда вычисляем новую т-ру металла, смешиваем металл с этим коэфтом и т-ру, задаваемую газом, с (единица минус коэфт")

(defun grow-temperature (place new-value)
  (with-byref-params (place)
    (setf
     place
     (+ 
      (* *MetalUslovnyHeatCapacity* place)
      (* (- 1 *MetalUslovnyHeatCapacity*) new-value)))))
       

(defun UpdateParamsForNextCircle ()
  "Собераемся совершить оборот (вызвать one-circle). Подправляем параметры по результатам
 предыдущих оборотов. Например, можем поменять т-ру нагрвателя"
  ; подготовим все данные
  (perga
    (with-output-to-string (*standard-output*)
      (AdiabStat))
    ;(let deltaTr1 nil)
    (do-for (ci +r1 +r2)
      (:lett cell CoCell [ *Cells* ci ])
      (:lett deltaT dbl (RegenBoundDeltaT ci))
      ; просто назначить нельзя, т.к. начинаются колебания как минимум на границе регенераторов
      ; поэтому назначаем вмему металлу некую "теплоёмкость"
      ;(grow-temperature (byref cell^LeftOutT) (- [ *InAvgTemperatureLeft* ci ] deltaT))
      ;(grow-temperature (byref cell^RightOutT) (+ [ *InAvgTemperatureRight* ci ] deltaT))

      ;; ещё один вариант. Зафиксируем т-ру между регенераторами
      (grow-temperature
       (byref cell^LeftOutT)
       (eswitch (ci)
         (+r1 (- [ *InAvgTemperatureLeft* ci ] deltaT))
         (+r2 (- (MeanEffectiveTemperature *th* *tk*) deltaT))))
      (grow-temperature
       (byref cell^RightOutT)
       (eswitch (ci)
         (+r1 (+ (MeanEffectiveTemperature *th* *tk*) deltaT))
         (+r2 (+ [ *InAvgTemperatureRight* ci ] deltaT))))
      )))


(defmacro with-duplicated-output-streams ((var filename &key if-exists) &body perga-body)
  `(perga
     (:@ with-open-file (,var ,filename :direction :output :if-exists ,if-exists :if-does-not-exist :create :external-format :utf-8))
     (let *standard-output* (make-broadcast-stream *standard-output* ,var))
     (let *error-output* (make-broadcast-stream *error-output* ,var))
     (let *trace-output* (make-broadcast-stream *trace-output* ,var))
     ,@perga-body
     ))

(defparameter *default-report-filename* (merge-pathnames "g1/current-report.txt" (ql:where-is-system :m)))
(defvar *current-report-filename*)

(defun circles (n &rest keyargs &key continue (do-plots nil))
  (perga
    (:@ with-duplicated-output-streams (report-stream *current-report-filename* :if-exists :append))
    (format t "Запуск ~S" `(circles ,n ,@keyargs))
    (dotimes (i n)
      (when (or (> i 0) continue)
        (UpdateParamsForNextCircle))
      (one-circle 0
         ;:initial_values (if (and (= i 0) (not continue)) nil *running_values*)
         :continue (or (> i 0) continue)
         :do-plots (and do-plots (= i (- n 1))))
      ;(show-expr *running_values*)
      )
    (AdiabStat))
  ; (clco:eval-in-tcl (format nil "::clconcmd::edit ~A" (namestring *current-report-filename*)))
  )

(defun rc (&key filename (no-of-circles 2) (do-plots t))
  "Перезагружает определения и запускает no-of-circles кругов. Если filename<>nil, то компилирует и загружает этот файл - в нём должны быть определены параметры машины по аналогии с ../mps-default.lisp. 
  Reloads definitions and starts no-of-circles crankshaft rotations. When filename<>nil, it must be a name of parameter file, which is similar to ../mps-default.lisp"
  (perga
    #+lispworks
    (CAPI:execute-with-interface
     (EDITOR-BUDDEN-TOOLS:get-some-editor)
      (lambda () (EDITOR:save-all-files-command t)))

    (when filename
      (unless (probe-file filename)
        (error "file ~S not found" filename)))

    (setf *current-report-filename*
      (cond
       (filename
        (merge-pathnames (make-pathname :type "report.txt") filename))
       (t
        *default-report-filename*)))
     
    (with-open-file (ou *current-report-filename* :direction :output :if-exists :supersede :if-does-not-exist :create :external-format :utf-8)
      (format ou "~%Запуск (rc :filename ~S :no-of-circles ~S)" filename no-of-circles)
      )

    (sleep 1)

    (with-output-to-string (*standard-output*)
      (asdf:load-system :m) ; это нужно, чтобы файл ../mps-default.lisp не загрузился после нашего файла параметров
      (when filename
        (load (compile-file filename)))
      (asdf:load-system :g1 :force t)
      )
    (circles no-of-circles :do-plots do-plots)))


(defparameter *x-data-interval* (/ *period* 6))

 
(defun do-pl (key)
  "Рисует графики, вызывается из statstep"
  (perga
    (ecase key
      (:p
       (plot-adw *btS* *pS* :title "Pressures" :x-data-interval *x-data-interval*))
      (:mvr
       (plot-adw *btS* *mvrS* :title "mvr" :x-data-interval *x-data-interval*))
      (:dmvr
       (plot-adw *btS* *dmvrS* :title "dmvr" :x-data-interval *x-data-interval*))
      (:m
       (plot-adw *btS* *mS* :title "m" :x-data-interval *x-data-interval*))
      (:v
       (plot-adw *btS* *vS* :title "vol" :x-data-interval *x-data-interval*))
      (:TotalVolume 
       (plot-adw *btS* (mjr_mat_* *vS* (mjr_vec_make-const (mjr_mat_cols *vS*) 1)) :title "TotalVolume" :x-data-interval *x-data-interval*))
      (:temp
       (plot-adw *btS* *TS* :title "temp" :x-data-interval *x-data-interval*))
      (:movedrT
       (plot-adw *btS* (extract-cols-from-matrix *movedrTS* #(0 1 2 3 4))
                 :title "moved-at-right-temps" :x-data-interval *x-data-interval*))
      (:CellEpsilon
       (plot-adw *btS* (extract-cols-from-matrix *CellEpsilonS* (vector +h +r1 +r2 +k))
                 :title "Instant-efficiences" :x-data-interval *x-data-interval*))
      (:pv
       (let multiplier (mjr_vec_make-const (mjr_mat_cols *pS*) 1.0))
       (setf [ multiplier +b ] 0.0)
       (plot-adw (mjr_mat_m2cv (mjr_mat_* *vS* multiplier)) ; объём общий, кроме картера
                 (mjr_mat_* *pS* multiplier (/ *n*)) ; среднее давление по всем, кроме картера
                 :title "PV"))
    
      ; (plot-adw *btS* (mjr_mat_* dWS (mjr_vec_make-const (mjr_mat_cols dWS) 1)) :title "Power")
    ;(plot-adw *btS* *dQS* :title "Heat input" :x-data-interval *x-data-interval*)
      )
    )
  )


(defun do-plots-inner (args)
  (perga
    (loop
     (let key (pop args))
     (let val (pop args))
     (cond
      ((null key)
       (return))
      (val
       (do-pl key))))))
   

(defun do-plots (&key (p t) mvr dmvr (m t) (v t) movedrT (CellEpsilon t) (pv t) (temp t) TotalVolume)
  (do-plots-inner
   (BUDDEN-TOOLS:dispatch-keyargs-simple p mvr dmvr m v movedrT CellEpsilon pv temp TotalVolume)))

  
(defun plot-one-section (matrix i)
  "Например, plot-one-section *ts* 1 - нарисовать т-ру нагревателя"
  (plot-adw *btS* (mjr_mat_cv2m (extract-col-from-matrix matrix i)) :title "OneSection"))


(defun ClearOneCircleResults ()
  "Вызывается в начале rotation. (В светлом будущем) очищает всё, что нужно очистить"
  (perga
    (mapcar 'makunbound '(*StatRegen* *StatFlowFriction* *InAvgTemperatureLeft* *InAvgTemperatureRight*))
  
    (let time-points (+ *times* 1)) ; на самом деле делаем один лишний шаг. 
    ;(:lett asteps fixnum 10) ; steps to measure values in each iteration

    ; заполняем векторы, в к-рых будем накапливать статистические итоге
    (setf *vS* (mjr_mat_make-const time-points *n* 0.0))
    (setf *mvrS* (mjr_mat_make-const time-points *n* 0.0))
    (setf *dmvrS* (mjr_mat_make-const time-points *n* 0.0))
    (setf *mS* (mjr_mat_make-const time-points *n* 0.0))
    (setf *pS* (mjr_mat_make-const time-points *n* 0.0)) ; каждая строка - это момент времени, каждая колонка - это один теплообменник
    (setf *dWS* (mjr_mat_make-const time-points *n* 0.0))
    (setf *qS* (mjr_mat_make-const time-points *n* 0.0))
    (setf *dQS* (mjr_mat_make-const time-points *n* 0.0))
    (setf *TS* (mjr_mat_make-const time-points *n* 0.0))
    (setf *movedrTS* (mjr_mat_make-const time-points *n* 0.0))
  
    (setf *btS* (mjr_vec_* (integer-range 0 *times*) *tim*)) ; size = (+ *times* 1)

    (setf *CellEpsilonS* (mjr_mat_make-const time-points *n* 0.0))

    (setf *StatAveragePressure* nil)
    (setf *StatPressureRatio* nil)
    (setf *StatMinimumPressure* nil)
    (setf *StatMaximumPressure* nil)
    ))

(defparameter *CircleNumber* 0 "Номер оборотка коленвала от начала, начиная с 0")

(defun try-one-time-interval ()
  "Проходим вперёд один отрезок, но не меняем статистические переменные. Мы ещё посмотрим, годится ли он нам"
  (perga
    (multiple-value-setq (*Time* *Results*)
        (stat *tstart-outer* *running_values*)
        ; здесь решается уравнение на отрезке с *tstart-outer* по (+ *tstart-outer* *tim*)
      )
    (format t "~A" (length *Time*))

      ; определим дисперсию давлений (копипаст из one-time-interval)? yt 
    (simulate (aref *Time* 0) (extract-row-from-matrix *Results* 0) t)

    ;(:lett СуммаКвадратовДавлений mjr_vec (mjr_vec_make-const *n* 0.0))
    ;(:lett СуммаДавлений mjr_vec (mjr_vec_make-const *n* 0.0))

    ;(flet stat-pressure-disp-one-point ()
    ;  (_f mjr_vec_+ СуммаКвадратовДавлений (mjr_vec_ewuo *P* (lambda (x) (expt x 2))))
    ;  (_f mjr_vec_+ СуммаДавлений *P*)
    ;  )

    (:lett ДисперсииДавления (or (cons dbl) null) nil)

    (flet stat-pressure-disp-one-point ()
      (push (alexandria.0.dev:standard-deviation (subseq *P* 0 +b))
            ДисперсииДавления))

    (let TN (length *Time*))
    
    (do-for (i 0 (- TN 1)) 
      (simulate (aref *Time* i) (extract-row-from-matrix *Results* i) t)
      (stat-pressure-disp-one-point)
      )

    ;(:lett Дисперсия mjr_vec
    ; (mjr_vec_*
    ;  (mjr_vec_-
    ;   (mjr_vec_/ СуммаКвадратовДавлений TN)
    ;   (mjr_vec_ewuo СуммаДавлений (lambda (x) (expt (/ x TN) 2)))
    ;   )
    ;  (/ (- TN 1) TN)))

    (:lett AverageDispersion dbl
     (ALEXANDRIA.0.DEV:mean ДисперсииДавления))
     ; (nth-value 1 (min-and-max-of-sequence Дисперсия))
     
    AverageDispersion
    
    ))


(defparameter *GoodControlCriteria* 250.0
  "Если значение критерия качества меньше (т.е. лучше) этой величины, то управление уже можно не менять. Если появляется сообщение 1189, то величину следует уменьшить")


(defun find-best-solution-for-one-time-interval ()
  "Меняя управление, подбираем лучший результат и заносим его в *Time* и *Results*"
  (perga
    (let BestControlCriteria nil)
    (let BestRes nil)
    (let BestArg nil)
    (let k *Xi1*)
    (let do-show nil)
    (dolist (p (list k (/ k 3) (* k 3) (/ k 10) (* k 10)))
      (let ThisControlCriteria
        (let ((*Xi1* p))
          (try-one-time-interval)))
      (when (or (null BestControlCriteria)
                (< ThisControlCriteria BestControlCriteria))
        (setf BestControlCriteria ThisControlCriteria)
        (setf BestRes (list *Time* *Results*))
        (setf BestArg p))

      (when (< BestControlCriteria *GoodControlCriteria*)
        (return))

      (setf do-show t)
      (show-expr ThisControlCriteria)
      )
    
    (setf *Time* (first BestRes)
          *Results* (second BestRes))
    (setf *Xi1* BestArg)
    
    (when do-show
      (show-expr BestControlCriteria)
      (show-expr *Xi1*))
    ))
  

(defun one-time-interval (bt) "Проходим вперёд один отрезок времени. bt (от 0) - номер отрезка. Сам отрезок - (* bt *tim*) .. (* (+ b1 1) *tim*)"
  (perga
    (unless (= bt 0) ; если не первый цикл - подставим нач. условия как данные на конце прошлого цикла
      (multiple-value-setq (*running_values* *tstart-outer*) (get-last-solution-values)))

    (find-best-solution-for-one-time-interval)

    (let TN (length *Time*))

    (format t ".")
    (finish-output *standard-output*)

      ; интегрируем второстепенные значения вдоль уравнения - начальная точка
    (simulate (aref *Time* 0) (extract-row-from-matrix *Results* 0) t)

    (flet add-one-point (dt)
      (add-vector-to-row-of-matrix *vS* bt (mjr_vec_* 0.5 dt *V*))
      (add-vector-to-row-of-matrix *pS* bt (mjr_vec_* 0.5 dt *P*))
      (add-vector-to-row-of-matrix *mvrS* bt (mjr_vec_* 0.5 dt *ymvr_inds*))
      (add-vector-to-row-of-matrix *dmvrS* bt (mjr_vec_* 0.5 dt *dmvr*))
      (add-vector-to-row-of-matrix *mS* bt (mjr_vec_* 0.5 dt *ym_inds*))
      (add-vector-to-row-of-matrix *dWS* bt (mjr_vec_* 0.5 dt *dW*))
      (add-vector-to-row-of-matrix *dQS* bt (mjr_vec_* 0.5 dt *dQin*))
      (add-vector-to-row-of-matrix *qS* bt (mjr_vec_* 0.5 dt *yq_inds*))
      (add-vector-to-row-of-matrix *TS* bt (mjr_vec_* 0.5 dt *Temp*))
      (add-vector-to-row-of-matrix *movedrTS* bt (mjr_vec_* 0.5 dt *movedrT*))
      (add-vector-to-row-of-matrix *CellEpsilonS* bt (mjr_vec_* 0.5 dt *CellEpsilon*))
      )

    (do-for (i 1 (- TN 1)) ; в MATLAB было от 1 до TN
      (let ((dt (- [ *Time* i ] [ *Time* (- i 1) ])))
        ; считаем среднее между соседними точками. Затейливый способ
        ; интегрирования!
        (add-one-point dt)
        (simulate (aref *Time* i) (extract-row-from-matrix *Results* i) t)
        (add-one-point dt)
        ))

    (_f mjr_mat_rowop-mult *dWS* bt (/ *tim*))
    (_f mjr_mat_rowop-mult *dQS* bt (/ *tim*))
    (_f mjr_mat_rowop-mult *vS* bt (/ *tim*))
    (_f mjr_mat_rowop-mult *pS* bt (/ *tim*))
    (_f mjr_mat_rowop-mult *TS* bt (/ *tim*))
    (_f mjr_mat_rowop-mult *movedrTS* bt (/ *tim*))
    (_f mjr_mat_rowop-mult *mvrS* bt (/ *tim*))
    (_f mjr_mat_rowop-mult *dmvrS* bt (/ *tim*))
    (_f mjr_mat_rowop-mult *mS* bt (/ *tim*))
    (_f mjr_mat_rowop-mult *qS* bt (/ *tim*))
    (_f mjr_mat_rowop-mult *CellEpsilonS* bt (/ *tim*))

    ))
                 

(defun one-circle (tstart &key (do-plots t) continue)
  "Один оборот коленвала, возможно, с выводом графиков. Если initial_values = nil, то (bvalue) вернёт нач. условия по умолчанию"
  (perga
    (ClearOneCircleResults)
    (WARN-ONCE:start-warnings-over)
    (cond
     (continue ; продолжаем - надо очистить q
      (dovector (ignore index *q_inds*)
        (declare (ignore ignore))
        (setf [ *running_values* index ] 0.0))
      (incf *CircleNumber*))
     (t ; начинаем сначала - задаём значения для одного прогона диффура
      (setf *CircleNumber* 0)
      (setf *running_values* (copy-array (bvalue)))
      ))
    (setf *tstart-outer* (coerce tstart 'dbl))

    ; мы пройдём по отрезкам длиной *tim* каждый в количестве *times*, на каждом посчитаем уравнение. 
    ; готовим границы интервалов решения диффура
    ; bt (от 0) - номер отрезка. Сам отрезок - (* bt *tim*) .. (* (+ b1 1) *tim*)
    (do-for (bt 0 *times*) ; цикл по отрезкам
      (one-time-interval bt)
      ) ;; конец цикла по отрезкам
    
    (when do-plots
      (do-plots)
      )
    
    (StatGrossEfficiency)

    
    )
  )
                        
      
    



