; -*- Encoding: utf-8;  System :g1 -*-

(in-package :m)
(in-readtable :buddens-readtable-a)
(eval-when (:compile-toplevel)
  (assert (eq *READ-DEFAULT-FLOAT-FORMAT* 'double-float)))


(declaim (special *required-phase* *dStroke*))

(defun OptimumPhaseForBeta ()
  "Сдвиг между мертвыми точками коленвалов. Может понадобиться поменять знаки или отнять от pi."
  (/ 
   (acos (sqrt (/ (+ 1 (cos *required-phase*)) 2)))
   [gradusov]
   ))


(defun OptimumWorkingStrokeForBeta ()
  "Такое значение *wStroke*, чтобы был одинаковый вытесняемый объём в холодной и горячей полости беты"
  (* *dStroke* (sqrt (+ 2 (* 2 (cos *required-phase*))))))

(defun MakeMatrixHx (SetkaWireDiam FrontalArea Length SetkaCellSize SetkaLambda)
  "FIXME перевести в отдельный файл"
  (perga
    (:lett res hx:SetkaRodsKaysLondon
     (hx:MAKE-SetkaRodsKaysLondon
      :Length Length
      :FrontalArea FrontalArea
      :SetkaCellSize SetkaCellSize
      :SetkaWireDiam SetkaWireDiam
      :SetkaLambda SetkaLambda))
    (hx:CalcHeatExchanger res)
    (assert (or (<= 0.72 res^Porosity 0.73)     ; чтобы это работало, SetkaCellSize=2.9*SetkaWireDiam
                (<= 0.81 res^Porosity 0.84)))   ; чтобы это работало, SetkaCellSize=4.5*SetkaWireDiam
    res
    ))


(defun MartiniMinAndMaxHotVolume ()
  "Вычисляет мин. и макс. объём горячей части для сверки с программой Мартини. 
 Запускать после моделирования"
  (mapcar
   (lambda (x) (+ x (^ (Hxi +h) FluidVolume)))
   (mlvl-list (min-and-max-of-sequence (extract-col-from-matrix *vS* +e)))))


(defun MartiniMinAndMaxColdVolume ()
  "Вычисляет мин. и макс. объём холодной части для сверки с программой Мартини. 
 Запускать после моделирования"
  (mapcar
   (lambda (x) (+ x (^ (Hxi +k) FluidVolume)))
   (mlvl-list (min-and-max-of-sequence (extract-col-from-matrix *vS* +c)))))


(defun MinAndMaxPressure ()
  (min-and-max-of-sequence (extract-col-from-matrix *pS* +e)))
