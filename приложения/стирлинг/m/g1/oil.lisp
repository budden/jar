; -*- Encoding: utf-8;  System :g1 -*-


(in-package :m)
(in-readtable :buddens-readtable-a)

(defun ВысотаПодъёмаМасла (r)
  (perga
    (let sigma (* 46e-5 (/ [cm])))
    ;(let r (^ (Hxi +r1) HydrDiam))
    (let rho 700)
    (let g 9.81)
    (* 2 sigma
       (/ 1 r rho g))))


(defparameter *МолярнаяМассаМаслаВМ1*
  450)

(defun ДавлениеПаровМаслаВМ1 (tc)
  (exp (linear-scalar-fun tc 20 (log 5.3e-6) 140 (log 1.33))))


(defparameter *ДавлениеПаровVacuumPumpSR2Help*
  (make-piecewize-linear-scalar-fun
   #2A((25 2)
       (50 3.3)
       (100 9.3)
       (150 22.8)
       (200 45.9)
       (250 83.9)
       (300 136))))
    

(defun ДавлениеПаровVacuumPumpSR2 (tc)
  (* 100
     (funcall *ДавлениеПаровVacuumPumpSR2Help* tc)))

(defparameter *ДавлениеПаровVacuumPumpVOil9930*
  (make-piecewize-linear-scalar-fun
   #2A((80 3.5e-5)
       (100 2.7e-4)
       (140 6e-3)
       (221 1.0))))

(defun ДавлениеПаровVacuumPumpVOil9930 (tc)
  (* 100
     (funcall *ДавлениеПаровVacuumPumpVOil9930* tc)))


(defun ДавлениеПаровC16H34 (tc)
  "NIST webbook"
  (perga
    (let A 4.17312)
    (let B 1845.672)
    (let C -117.054)
    (let tk ([Celsius] tc))
    (assert (<= 463 tk 559))
    ([Bar] 
     (expt 10 (- A (/ B (+ tk C))))
     )))


(defun МассаМаслаПролетающегоВЧасЧерезНачалоРегенератора ()
  (* [cm3] 50 25 ; секундный ометаемый объём
     3600 ; количество секунд
     (/ (ДавлениеПаровМаслаВМ1 80) [Bar]) ; давление паров в атмосферах
     (/ *МолярнаяМассаМаслаВМ1* 28.0) ; плотность по воздуху, считая состав C50
     1.3 ; плотность воздуха при атмосферном давлении
     )) 


(defun СмазкаВодой (h w)
  (perga
    (:lett hh1 hx:FlatChannel
     (hx:CalcHeatExchanger (hx:MAKE-FlatChannel :Length ([cm] 1) :Height h :Width ([cm] 4))))
    (:lett str1 str:FluidStreamWithLiquidProperties
     (str:CalcStream (str:MAKE-WaterAt80CStream :VolumetricFlow (* hh1^FlowArea w) :Temperature ([Celsius] 80))))
    (let PressureDrop (HX-AND-STR:hxPressureDrop hh1 str1))
    (let Power (* PressureDrop str1^VolumetricFlow))
    (show-expr (/ PressureDrop [Bar]))
    (show-expr Power)
    (values)))


(defun СмазкаЧемТо (h nu w) 
  (perga
    (:lett hh1 hx:FlatChannel
     (hx:CalcHeatExchanger (hx:MAKE-FlatChannel :Length ([cm] 1) :Height h :Width ([cm] 4))))
    (:lett str1 str:FluidStreamWithLiquidProperties
     (str::MAKE-FluidStreamWithLiquidProperties
      :VolumetricFlow (* hh1^FlowArea w)
      :Pressure [Bar]
      :Temperature 300.0
      :ThermalExpansionCoefficient 0.0
      :nu nu
      :rho 1000.0
      :lambda 0.2
      :Cp 1000.0))
    (let PressureDrop (HX-AND-STR:hxPressureDrop hh1 str1))
    (let Power (* PressureDrop str1^VolumetricFlow))
    (show-expr (/ PressureDrop [Bar]))
    (show-expr Power)
    (values)))
    



