; -*- Encoding: utf-8;  System :g1 -*-

(in-package :m)
(named-readtables:in-readtable :buddens-readtable-a)

;;; параметры машины, часть 1. 

(deftparameter *period* dbl :initial-value (/ 1 *freq*)
               :documentation "Период обращения коленвала")
(deftparameter *omega* dbl :initial-value (* 2 pi *freq*)
               :documentation "Угловая скорость вращения вала")


;;; свойства газа
(deftvar *Rmetric* double-float :documentation "зависит от WorkingFluidStreamType , устанавливаеся в SetGas ")
(deftvar *cp* number) 
(deftvar *cv* number) 
(deftvar *gamm* number) 


(deftparameter *tim* double-float
               :initial-value (/ *period* *times*)
               :documentation "time length of each iteration")

(defconstant +e+ (exp 1.0))


(defmacro dy-rank () '(* *n* 4))

(deftparameter *inds* mjr_vec
               :documentation "индексы для хранения произведения M*Tбыло 1:n , стало 0: (- *n*) 1)"
               :initial-value (integer-range 0 (- *n* 1)))

(deftparameter *m_inds* mjr_vec
               :documentation "индексы для хранения m. Было (n+1):2*n, стало *n* : 2* *n* - 1"
               :initial-value (integer-range *n* (- (* 2 *n*) 1)))

(deftparameter *mvr_inds* mjr_vec
               :documentation "индексы для хранения mvr. 2 *n* .. 3 *n* -1"
               :initial-value (integer-range (* 2 *n*) (- (* 3 *n*) 1)))

(deftparameter *q_inds* mjr_vec
               :documentation "индексы для хранения прихода тепла в каждую ячейку"
               :initial-value (integer-range (* 3 *n*) (- (* 4 *n*) 1)))


;;; Переменные решения. Устанавливаются ф-ей simulate в ходе вычисления правой части, а также могут устанавливаться при анализе вторичных величин. Если в документации ф-ии написано, что она зависит от состояния решения, то ф-я использует эти переменные.
(deftvar *tt* dbl :documentation "Переменная решения. Текущее время. Пока мало где используется. Но все ф-ии от состояния решения не  должны принимать время")
(deftvar *Temp* mjr_vec :documentation "Переменная решения. Вектор текущих температур. Символ экспортируется из :mps")

(defun GetTemp (ci) [ *Temp* ci ])

(deftvar *P* mjr_vec :documentation "Переменная решения. Текущее значение давление в ходе вычисления правой части диф.ур-я.")
(deftvar *V* mjr_vec :documentation "Переменная решения. vector of current volumes")
(deftvar *dV* mjr_vec :documentation "Переменная решения. current volume derivatives")
(deftvar *yinds* mjr_vec :documentation "Переменная решения. values of mT(i), vector")
(deftvar *ym_inds* mjr_vec :documentation "Переменная решения. values of m(i), vector")
(deftvar *ymvr_inds* mjr_vec :documentation "Переменная решения. Скорость втекания массы со стороны следующей ячейки, vector")
(deftvar *yq_inds* mjr_vec :documentation "Переменная решения. подача тепла в ячейку накопительным итогом")

;(deftvar *mvr* mjr_vec :documentation "Скорость втекания массы со стороны следующей ячейки")
(deftvar *mvl* mjr_vec :documentation "Скорость втекания массы со стороны предыдущей ячейки")
(deftvar *dmvr* mjr_vec :documentation "differential of mvr")

(deftvar *tl* mjr_vec :documentation "Температура воздуха, входящего в данную ячейку слева. Если воздух не входит, а выходит из ячейки влево, то данный параметр не имеет значения. Мы считаем, что при наличии перепада давлений между ячейками воздух адиабатно расширяется между ними")
(deftvar *tr* mjr_vec)
(deftvar *movedrT* mjr_vec :documentation "Измеренная в данной ячейке т-ра воздуха, который действительно перемещается через границу этой ячейки")


;;; Результаты решения на одном отрезке
(deftvar *Time* mjr_vec :documentation "вектор значений времени по сетке при решении, на одном шаге решения")
(deftvar *Results* mjr_mat :documentation "значения правой части во времена из *Time*") 

;;; Результаты анализа решения за оборот
(deftvar *StatRegen* (array (or StatRegenRec null) (*))
         :documentation "Заполняется в StatRegen <- AdiabStat, очищается в one-circle")

(deftvar *StatFlowFriction* (array (or StatFlowFrictionRec null) (*))
         :documentation "Заполняется в StatFlowFriction, очищается в one-circle")

(defparameter *StatAveragePressure* nil "Среднее давление во всех областях, кроме буфера, присваивается в StatPressure")
(defparameter *StatPressureRatio* nil "Отношение давлений по результатам работы машины, присваивается в StatPressure")
(defparameter *StatMinimumPressure* nil "См. *StatMaximumPressure*")
(defparameter *StatMaximumPressure* nil "Максимальное давление в цикле, аналогично *StatPressureRatio*")
(defparameter *StatPowerSimple* nil "Мощность с учётом потерь, присваивается в AdiabStat")

(defun SetGas ()
  "Устанавливает глоб. переменные свойств газа для воздуха (при 90С)"
  (perga
    (:lett str WorkingFluidStreamType (MAKE-WorkingFluidStreamType :Pressure *InitialPressure*
                                                                   :Temperature (MeanEffectiveTemperature *th* *tk*)))
    (str:CalcStream str)
    ; (setf *molarMass* 0.028)
    (setf *Rmetric* str^RGas)
    (setf *cp* str^Cp)
    (setf *gamm* str^GammaGas)
    (setf *cv* str^Cv)
    (values)
    ))

(SetGas) ; нужно, т.к. дальше используется *rmetric*

(defvar *tstart*)  ; используется внутри call-ode
(defvar *tstart-outer*) ; используется вне call-ode
(deftvar *running_values* mjr_vec :documentation "текущие значения y")
(defvar *my_t*)
(deftvar *dW* mjr_vec :documentation "механическая мощность")
(deftvar *dQin* mjr_vec :documentation "мощность теплового потока в данный момент. Иcпользуется и обновляется при вычислении правой части")
(deftvar *Qin* mjr_vec :documentation "вход тепла в ячейку с начала оборота")


(deftvar *btS* mjr_vec :documentation "времена для запуска решения дифура между этими временами. График строится по этим точкам")
(deftvar *vS* mjr_mat :documentation "значения объёмов полостей во времена *btS* . Строки времена, колонки - полости")
(deftvar *TS* mjr_mat :documentation "значения т-р полостей")
(deftvar *movedrTS* mjr_mat :documentation "значения т-р перемещённого по правой границе полости i воздуха, измеренные в полости i")
(deftvar *dWS* mjr_mat :documentation "значения работы, совершённой полостями")
(deftvar *dQS* mjr_mat :documentation "значения мощностей притоков тепла в полости")
(deftvar *qS* mjr_mat :documentation "набегающие значения притоков тепла в полости с начала оборота")

(deftvar *pS* mjr_mat :documentation "значения давлений полостей во времена *btS* . Строки времена, колонки - полости")
(deftvar *mvrS* mjr_mat :documentation "скорость втекания массы со стороны следующей ячейки - во времена *btS* . Строки - времена, колонки - полости")
(deftvar *dmvrS* mjr_mat :documentation "производная скорости втекания массы со стороны следующей ячейки - во времена *btS* . Строки - времена, колонки - полости")
(deftvar *mS* mjr_mat :documentation "масса в ячейка во времена *btS* . Строки - времена, колонки - полости")

(deftvar *CellEpsilon* mjr_vec :documentation "эффективность каждой ячейки, вычисленная по мгновенному потоку")
(deftvar *CellEpsilonS* mjr_mat :documentation "эффективность каждой ячейки, вычисленная по мгновенному потоку, от времени. Строки - времена, колонки - ячейки")


;(deftvar *RegenInAvgTemperatureRight* dbl :documentation "Средняя за оборот т-ра воздуха, входящего в регенератора справа. На самом деле, это понятие имеет мало смысла - нужно учитывать ещё и скорость, от которой зависит полнота теплообмена с первыми встреченными слоями сетки и, как следствие, температура этих слоёв. Но пока хотя бы так")
;(deftvar *RegenInAvgTemperatureLeft* dbl :documentation "См. *RegenInAvgTemperatureRight*")

(deftvar *InAvgTemperatureLeft* (vector (dbl)) :documentation "Средняя за оборот т-ра воздуха, входящего в данную ячейку регенератора справа. На самом деле, это понятие имеет мало смысла - нужно учитывать ещё и скорость, от которой зависит полнота теплообмена с первыми встреченными слоями сетки и, как следствие, температура этих слоёв. Но пока хотя бы так")
(deftvar *InAvgTemperatureRight* (vector (dbl)) :documentation "См. *InAvgTemperatureLeft*")

(defun RegenBoundDeltaT (ci)
  "Настолько выходящий из регенератора воздух холоднее/теплее входящего с той же стороны. Ущербно на базе regEffect"
  (perga
    ;(:lett hx hx:HxBase (Hxi ci))
    ;(:lett cell CoCell [ *Cells* ci ])
    (:lett stat StatRegenRec [ *StatRegen* ci ])
    (:lett ideal-left-t dbl
     (eswitch (ci)
       (+r1 *th*)
       (+r2 (MeanEffectiveTemperature *th* *tk*))))
    (:lett ideal-right-t dbl
     (eswitch (ci)
       (+r1 (MeanEffectiveTemperature *th* *tk*))
       (+r2 *tk*)))
    (* (- ideal-left-t ideal-right-t) 0.5 (- 1 stat^regEffect))))


(defvar *RegenDomeFluidFlowArea*) ; присваивается в SetEngine, к-рая вызывает SetRegenDome

(defun SetRegenDome () 
 "Вызывается из SetEngine."
 (setf *RegenDomeFluidFlowArea* 
   (- (CircleArea *RegenDomeDout*)
      (CircleArea *RegenDomeDIn*)
      )))


(deftparameter *AverageStream* WorkingFluidStreamType
               :documentation "Поток со средними параметрами и нулевым расходом. Например, для зазора вытеснителя"
               :initial-value (MAKE-WorkingFluidStreamType))

(defun SetAverageStream ()
  (setf *AverageStream*
        (MAKE-WorkingFluidStreamType :Pressure *InitialPressure*
                                     :Temperature (MeanEffectiveTemperature *th* *tk*)))
  (str:CalcStream *AverageStream*))
               

(deftvar *GrossEfficiency* dbl :documentation "КПД без учёта челночных потерь, потерь на прокачку газа, перетечек и мех.потерь")
(deftvar *NetEfficiency* dbl :documentation "КПД с учётом всех потерь")



;; Контроль потоков между ячейками
#|
примерный troubleshooting.

если т-ра зашкаливает, а давления одинаковы, это может означать недостаток теплообмена. 


|#


(defun ReferenceVolume ()
  (sum-of-vector-elements (subseq *V* 0 +b)))

(deftparameter *Xi1* double-float
               :initial-value 0.501
               :documentation "См. *Xi1Factor*, SetEquationControls . Коэфт, по к-рому разность давлений превращается в массовый поток"
               )




(deftparameter *mvr-limit* dbl
               :initial-value 5.7               
               :documentation "См. SetEquationControls . Предельное значение mvr - нужно ограничить, чтобы не возникало незатухающих колебаний, но нельзя поставить ниже, чем оно есть по природе задачи")

(deftparameter *dmvr-to-mvr-099-time* dbl
               :initial-value 6.0e-4
               :documentation "См. *dmvr-to-mvr-099-time-factor* , *dmvr-to-mvr-099-low-limit* SetEquationControls . за это время время разница между mvr желаемым и реальным затухнет на 99%")


(deftparameter *dmvr-limit* dbl
               :initial-value 956.8
               :documentation "См. SetEquationControls . Значение по модулю максимального dmvr - в начале dmvr бывает во много раз больше, чем потом - не нужно его 'резать'")


#| (deftparameter *dmvr-switch-on-time* dbl
  :initial-value (* 0.1 *period*)
  :documentation "Время мягкого пуска вычисления dmvr*") |#

(defun ScalingDensityFactor ()
  "Во сколько раз плотность превосходит плотность водорода"
  (/ 4157.2 *Rmetric*))

(defun SetEquationControls ()
  "Устанавливает переменные, контролирующие поток массы. См. также call-ode , make-solver-control-vector-by-y"
  (setf *Xi1* (* *Xi1Factor*                 
                 *freq*
                 *InitialPressure* 
                 (ScalingDensityFactor)
                 (ReferenceVolume)
                 ))
  (setf *mvr-limit* (* *mvr-limit-factor* 
                       *InitialPressure*
                       (ReferenceVolume)
                       (ScalingDensityFactor)
                       *freq*
                       (+ *th* *tk*)))
  (setf *dmvr-to-mvr-099-time* (* *period* *dmvr-to-mvr-099-time-factor*))
  (setf *dmvr-limit* (* *dmvr-limit-factor* 
                      2 pi *mvr-limit* *freq*)) 
  )
