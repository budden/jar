; -*- Encoding: utf-8;  System :g1 -*-
(in-package :m)
(in-readtable :buddens-readtable-a)

(defun nshow-expr (x) x)


(defun get_tri (ci mvri)
  "Температура воздуха, входящего в данную ячейку справа или в следующую ячейку слева. Должна вызывать из simulate, а её рез-ты - записываться в *tr*, *tl*"
  (perga
    (let res
      (cond
       ((> mvri 0) ; воздух входит справа в ячейку i
        (TAd (CellOutputT (+ ci 1) nil)
             [ *P* (+ ci 1) ]
             [ *P* ci ]
             *gamm*))
       (t ; воздух выходит вправо из ячейки i
        (TAd (CellOutputT ci t)
             [ *P* ci ]
             [ *P* (+ ci 1) ]
             *gamm*
             ))))
    (unless (< 150.0 res 1200.0)
      (break "Температура зашкалила в get_tri, tt=~S" res))
    res))

(defun MovedTAtCellBoundary (ci right-p)
  "Вычисляем т-ру газа, к-рый приходит или уходит через левую или правую (right-p) границу ячейки ii. 
  Т-ра замерена в ячейке i. Если давление в ячейке i+1 другое, то и температура этого же газа
  в ячейке i+1 будет другой"
  (perga
    (cond
     (right-p
      (:lett mvri dbl [ *ymvr_inds* ci ])
      (cond
       ((> mvri 0.0) ; газ поступает справа в ячейку i
        [ *tr* ci ])
       (t
        (CellOutputT ci t)
        )))
     (t 
      (:lett mvli dbl [ *mvl* ci ])
      (cond
       ((> mvli 0.0) ; газ поступает слева в ячейку ii
        [ *tl* ci ])
       (t ; газ выходит из ячейки
        (CellOutputT ci nil))))
     )))

  

(defun MassTimesDeltaTimesCp (ci)
  "Функция от состояния решения. Вычисляем sum(cp*tinout*minout)"
  (perga
    (let mvri [ *ymvr_inds* ci ])
    (let mvli [ *mvl* ci ])
    (:lett res dbl 0.0)
    ;(:lett thistemp dbl [ *Temp* i ])
    (_f + res (* mvri (MovedTAtCellBoundary ci t)))
    (_f + res (* mvli (MovedTAtCellBoundary ci nil)))
    (* *cp* res)))
    

(defun diffur2inner (tt y) "Separate evaluation of rhs for redifiniton. See diffur2, call-ode"
  (perga
    (simulate tt y nil) ; calculate something
    (:lett dy mjr_vec (mjr_vec_make-const (dy-rank) 0.0))
    (dotimes (i *n*)
      ;(show-expr i)
      (setf [ dy i ]
            (/ (+ [ *dQin* i ]
                  (MassTimesDeltaTimesCp i)
                  (- [ *dW* i ]))
               *cv*)))
    (dovector (ii i2 *m_inds*)
      (setf [ dy i2 ] (+ [ *ymvr_inds* ii ] [ *mvl* ii ])))
    (dovector (ii i3 *mvr_inds*)
      (setf [ dy i3 ] [ *dmvr* ii ]))
    (dovector (ii i4 *q_inds*)
      (setf [ dy i4 ] [ *dQin* ii ]))
    (vector-check-nan dy)
    ))

(declaim (notinline diffur2inner))

(defun diffur2 (tt y) "Rhs of diff eqn. y is (mT1,mT2,...mTn,m1,m2,...,mn). Called from call-ode "
  (diffur2inner tt y)
  )
    

#|
function dy=diffur2(t,y) % y is (mT1,mT2,...mTn,m1,m2,...,mn) 
global n; % number of cells
global inds; % 1:n
global m_inds;  % (n+1):2*n
global molarMass Rmetric cp cv P Temp V dV yinds ym_inds gamm dW;
global mvr mvl dmvr tr tl Qin;
global tstart my_t;

simulate(t,y); % calculate something 

% dy=zeros(2*n,1)
dy=zeros(2*n,1); 

for i=1:n
 dy(i)=(Qin(i) ...
    +cp*(tr(i)*mvr(i)+tl(i)*mvl(i)) ...
    -dW(i))/cv; % Theater, cv, mm
end
dy(m_inds)=mvr(inds)+mvl(inds);
|#


