; -*- Encoding: utf-8;  System :g1 -*-

(in-package :m)
(in-readtable :buddens-readtable-a)

(defun PistonPhaseShiftRadians () (* *phase* [gradusov]))


(defun-with-symbolic-derivative theta (ti) (Derivative-Of-theta) (* *omega* ti))

(defun CylinderSweptVolume (bore stroke)
  (* (CircleArea bore) stroke))

(defun-with-symbolic-derivative SinCylinder (ti dead swept angle) (Derivative-Of-SinCylinder
                                                                   :documentation
                                                                   "BDC в 0, TDC в (/ *period* 2)")
  (+ dead
     (* 0.5
        swept
        (- 1 (cos (+ (theta ti) angle))))))


(defun UnappropriateForAlphaEngine (ti)
  (error "This function is unappropriate for alpha engine. ti=~A" ti))

(buddens-symbolic-derivative:assign-derivative 'UnappropriateForAlphaEngine 'UnappropriateForAlphaEngine)
  
(defun UnappropriateForBetaEngine (ti)
  (error "This function is unappropriate for Beta engine. ti=~A" ti))

(buddens-symbolic-derivative:assign-derivative 'UnappropriateForBetaEngine 'UnappropriateForBetaEngine)
  

(defparameter *ShatunLength*
  (* 5.874 1/2 *wStroke*)
  "Connecting rod length, {ZZC} in terms of Rio's program")


(defparameter *HotSideDeadVolume*
  (+ *HotCollectorVolume*
     (* *dhotClearance* (CircleArea *dBore*))
     (* *dhotSideRadialClearance* (CircleLength *dBore*) (+ *dStroke* *DisplacerShuttleLength*))))


(defun-with-symbolic-derivative KrivoshipCylinder (ti dead swept angle)
                                (dKrivoshipCylinder
                                 :documentation
                                 "Цилиндр, с кривошипом, к-рый двигается со скоростью вращения коленвала, 0 = BDC, (/ *period 2) = TDC со значением = 1, и ещё поврнут на alphaС")
                                (+ dead (* swept (KrivoshipPistonLaw (theta ti) 0.5 (* 0.5 (/ *ShatunLength* *wStroke*)) angle))))


(defun SetDoubleActingKrivoshipAlphaVolumeLaws ()
  "Символ экспортирован из :mps. Может вызываться из MPS:SetCells"
  (perga
    (assert (eq *EngineType* 'alpha))
    (let lowArea '(- (CircleArea *dBore*)
                     (CircleArea *dRodBore*)))
    (eval 
     `(defun-with-symbolic-derivative
       VC1 (ti) (dVC1 :documentation "Объём холодного пр-ва, образуемого рабочим поршнем. see SetDoubleActingKrivoshipAlphaVolumeLaws")
       (KrivoshipCylinder ti (* *wTopClearance* ,lowArea) (* ,lowArea *wStroke*) 0))
     )
    (eval
     `(defun-with-symbolic-derivative
       VC (ti) (dVC :documentation "Объём холодного пр-ва, образуемого вытеснителем. See SetDoubleActingKrivoshipAlphaVolumeLaws")
       (+ (VC1 ti) *ColdCollectorVolume*)))
    (eval
     `(defun-with-symbolic-derivative
       VE (ti) (dVE :documentation "Объём горячего пр-ва от времени. См. SetDoubleActingKrivoshipAlphaVolumeLaws")
       (KrivoshipCylinder ti *HotSideDeadVolume*
                          (CylinderSweptVolume *dBore* *dStroke*) (PistonPhaseShiftRadians))))
    ))


(defun SetSinEngineVolumeLaws ()
  "Символ экспортируется из :mps. Может вызываться из MPS:SetCells"
  (perga
    (let lowArea (- (CircleArea *dBore*)
                    (CircleArea *dRodBore*)))
    (eval 
     `(defun-with-symbolic-derivative
       VC1 (ti) (dVC1 :documentation "Объём холодного пр-ва, образуемого рабочим поршнем. see SetSinEngineVolumeLaws")
       ,(ecase *EngineType*
          ((beta gamma) 
           `(SinCylinder ti (* *wTopClearance* (CircleArea *wBore*))
                         (CylinderSweptVolume *wBore* *wStroke*) 0))
          (alpha
           `(SinCylinder ti (* *wTopClearance* ,lowArea)
                         (* ,lowArea *wStroke*) 0)
           ))))
    (eval
     `(defun-with-symbolic-derivative
       VC2 (ti) (dVC2 :documentation "Объём холодного пр-ва, образуемого вытеснителем. See SetSinEngineVolumeLaws")
       ,(ecase *EngineType*
          ((beta gamma)
           `(SinCylinder ti (* *dcoldClearance* ,lowArea)
                         (* *dStroke* ,lowArea) (- (PistonPhaseShiftRadians) pi)))
          (alpha
           `(UnappropriateForAlphaEngine ti))
          )))
    (eval
     `(defun-with-symbolic-derivative
       VC (ti) (dVC :documentation "Объём холодного пр-ва, образуемого вытеснителем. See SetSinEngineVolumeLaws")
       (+ (VC1 ti) *ColdCollectorVolume*
          ,@(ecase *EngineType*
              (gamma `((VC2 ti)))
              (beta `((VC2 ti) ,(- (* (CircleArea *dBore*) *BetaCylinderShift*))))
              (alpha nil)))))
    (eval
     `(defun-with-symbolic-derivative
       VdispRod (ti) (dVdispRod :documentation "see SetSinEngineVolumeLaws")
       ,(ecase *EngineType*
          ((gamma beta)
           `(SinCylinder ti 0 (* *dStroke* (CircleArea *dRodBore*)) (PistonPhaseShiftRadians)))
          (alpha
           `(UnappropriateForAlphaEngine ti)))))
    (eval
     `(defun-with-symbolic-derivative
       VE (ti) (dVE :documentation "Объём горячего пр-ва от времени. См. SetSinEngineVolumeLaws")
       (SinCylinder ti *HotSideDeadVolume*
                    (CylinderSweptVolume *dBore* *dStroke*) (PistonPhaseShiftRadians))))
    (eval
     `(defun-with-symbolic-derivative
       VCrankcase (ti) (dVCrankcase :documentation "Объём картера от времени. См. SetSinEngineVolumeLaws")
       (- *CrankcaseMaximumVolume* ; это так только для 1-цилиндровой машины! Для многоцилиндровой он может быть константой
          (VC ti)
          (VE ti))))          
    ))



(defun WorkingPistonConrodAngle (ti)
  "Угол отклонения шатуна рабочего поршня от оси цилиндра от времени/angle of conrod vs cylinder axis"
  (perga 
    (let HalfStroke (/ *wStroke* 2))
    (let deviation
      (* (sin (theta ti)) HalfStroke)) ; линейное отклонение от оси
    (asin (/ deviation *ShatunLength*))))


(defun DisplacerConrodAngle (ti)
  "См./see WorkingPistonConrodAngle"
  (perga 
    (let HalfStroke (/ *wStroke* 2))
    (let Angle (+ (theta ti) (PistonPhaseShiftRadians))) ; compare to VE   
    (let deviation (* (sin Angle) HalfStroke)) ; линейное отклонение от оси
    (asin (/ deviation *ShatunLength*)))
  )
  



(defparameter *RossYokeB1* 3.54e-2 "From Urieli's program")
(defparameter *RossYokeB2* 3.45e-2 "From Urieli's program")
(defparameter *RossYokeCrank* 8.5e-3 "From Urieli's program") 


(defun-with-symbolic-derivative RossYokeBTheta (theta)
                                (dRossYokeBTheta :documentation "See SetRossYokeEngineVolumeLaws")
                                (expt (- (expt *RossYokeB1* 2) (* (expt *RossYokeCrank* 2) (expt (cos theta) 2))) 0.5))

(defparameter *RossYokeYMin*
  (perga
    (let Yoke (sqrt (+ (expt *RossYokeB1* 2) (expt *RossYokeB2* 2))))
    (expt (- (expt (- Yoke *RossYokeCrank*) 2) (expt *RossYokeB2* 2)) 0.5)
    ))  

(defun SetRossYokeEngineVolumeLaws ()
  "FIXME экспортировать символ из mps"
  (perga
    (assert (eq *EngineType* 'alpha))
    (eval 
     `(defun-with-symbolic-derivative
       VC (ti) (dVC :documentation "Объём холодного пр-ва от времени. See SetRossYokeEngineVolumeLaws")
       (+ *ColdCollectorVolume*
          (* *wTopClearance* (CircleArea *wBore*))
          (* (CircleArea *wBore*)
             (- (+ (* *RossYokeCrank* (- (sin (theta ti)) (* (/ *RossYokeB2* *RossYokeB1*) (cos (theta ti))))) (RossYokeBTheta (theta ti))) *RossYokeYMin*)))))
    (eval 
     `(defun-with-symbolic-derivative
       VE (ti) (dVE :documentation "Объём горячего пр-ва от времени. See SetRossYokeEngineVolumeLaws")
       (+ *HotSideDeadVolume*
          (* (CircleArea *dBore*)
             (- (+ (* *RossYokeCrank* (+ (sin (theta ti)) (* (/ *RossYokeB2* *RossYokeB1*) (cos (theta ti))))) (RossYokeBTheta (theta ti))) *RossYokeYMin*)))))                                      
    ))


(defun SetVol (ti)
  "sets law of section volumes change"
  (perga
    #| (:lett angle dbl (* *omega* tt))
    (:lett dAngle dbl *omega*)
    (:lett cadb dbl (CircleArea *DisplacerBore*))
    (:lett dispHot dbl
     (+
      (* cadb (+ 1 (cos angle)) *DisplacerStroke*)
      (* cadb *DisplacerHotClearance*)
      (* (- (CircleArea *DisplacerBore*)
            (CircleArea (- *DisplacerBore* (* 2 *DisplacerHotSideClearance*))))
         *DisplacerHotLength*)))

    (:lett dDispHot dbl
     (* cadb dAngle (- (sin angle)) *DisplacerStroke*))

    (:lett dispColdArea dbl (- cadb (CircleArea *DisplacerRodBore*)))
    (:lett dispCold dbl
     (* dispColdArea (- 1 (cos angle)) *DisplacerStroke*))
    (:lett dDispCold dbl
     (* dispColdArea dAngle (sin angle) *DisplacerStroke*))

    (:lett workCold dbl
     (* (CircleArea *WorkingBore*)
        (+ 1 (sin angle))
        *WorkingStroke*))
    (:lett dWorkCold dbl
     (* (CircleArea *WorkingBore*)
        dAngle (cos angle)
        *WorkingStroke*)) |#
    (:lett VCti dbl (VC ti))
    (assert (> VCti 0) () "Отрицательный объём полости сжатия")
    (setf *V*  (vector
                (VE ti)
                (^ (Hxi +h) FluidVolume)
                (^ (Hxi +r1) FluidVolume)
                (^ (Hxi +r2) FluidVolume)
                (^ (Hxi +k) FluidVolume)
                VCti
                (VCrankcase ti)))
    (setf *dV* (vector
                (dVE ti)
                0.0
                0.0
                0.0
                0.0
                (dVC ti)
                (dVCrankcase ti)))
    ))
  


(defun bvalue ()
  "returns initial_values"
  (perga
    (SetGas)
    (SetEngine)
    (SetAverageStream)
    (SetCells)
    (SetVol 0)
    (:lett initial_values mjr_vec
     (mjr_vec_make-const (dy-rank) 0.0))
    (dotimes (i *n*)
      ; m*T
      (let CellInitialPressure *InitialPressure*)
      (when (= i +b)
        (_f * CellInitialPressure *CrankcaseInitialPressureFactor*))
      (setf [ initial_values i ]
            (* CellInitialPressure [ *V* i ] (/ *Rmetric*)))
      ; m
      (setf [ initial_values (+ *n* i) ]
            (/ [ initial_values i ]
               (CoCell-TWall [ *Cells* i ])))
      ; mvr инициализируется нулями
      ; q   инициализируется нулями
      )
    (SetEquationControls)
    initial_values))
      

(defun SetEngine ()
  "Инициализирует законы изменения объёмов, геометрию"
  (SetRegenDome)
  (mps:SetEngineVolumeLaws)
  )


