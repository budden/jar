; -*- Encoding: utf-8;  System :g1 -*-

(named-readtables:in-readtable :buddens-readtable-a)

(def-merge-packages::! :journal_bearing
  (:always t)
  (:use :cl :m0 :budden-tools :physical-units)
  (:local-nicknames
   :str :fluid-stream :hx :heat-exchanger)
  (:custom-token-parsers budden-tools::convert-carat-to-^) 
  (:import-from :perga-implementation
   perga-implementation:perga)
  (:import-from :alexandria.0.dev
   ALEXANDRIA.0.DEV:copy-array
   alexandria.0.dev:eswitch)
  (:export  ; mps-utils.lisp
   "JOURNAL_BEARING::ПодшипникСкольженияБазовый
    JOURNAL_BEARING::ПодшипникСкольженияБазовый-ОтносительныйДиаметральныйЗазор
    JOURNAL_BEARING::ПодшипникСкольженияБазовый-ДлинаПодшипника
    JOURNAL_BEARING::ПодшипникСкольженияБазовый-ДиаметрЦапфы
    journal_bearing::make-ПодшипникСкольженияБазовый
    JOURNAL_BEARING::ПодшипникСкольженияПростой
    JOURNAL_BEARING::ПодшипникСкольженияПростой-ОтносительныйДиаметральныйЗазор
    JOURNAL_BEARING::ПодшипникСкольженияПростой-ДлинаПодшипника
    JOURNAL_BEARING::ПодшипникСкольженияПростой-ДиаметрЦапфы
    journal_bearing::ПодшипникСкольженияПростой-p
    journal_bearing::make-ПодшипникСкольженияПростой   "
   )
  (:export "
  journal_bearing::ЖидкостноеТрениеЛиЧернавскийСтр53
  JOURNAL_BEARING::[Чернавский]
  journal_bearing::ФормулаМаккиОрлова"
   ))

(in-package :journal_bearing)


(defstruct ПодшипникСкольженияБазовый ДиаметрЦапфы ДлинаПодшипника ОтносительныйДиаметральныйЗазор)

(defstruct (ПодшипникСкольженияПростой (:include ПодшипникСкольженияБазовый)))
  
; функция здесь, потому что она нужна для тестов
(defun tst-СоздатьПотокМаслаСЗаданнойВязкостью (nu)
  (str::MAKE-FluidStreamWithLiquidProperties
   :VolumetricFlow 0.0
   :Pressure [Bar]
   :Temperature 300.0
   :ThermalExpansionCoefficient 0.0
   :nu nu
   :rho 900.0
   :lambda 0.2
   :Cp 1000.0))
    
  
   
                                           