; -*- Encoding: utf-8;  System :g1 -*-

(in-package :m)
(in-readtable :buddens-readtable-a)


(defun y-delta-abs-fn (vi)
  "Должна возвращать типичный масштаб соответствующего значения y по модулю, используется для задания макс. допустимой абс.  ошибки и дельты.
  Также использует *v* . Чтобы проверить, что масштаб похож на правду, см. ПроверитьВерность-y-delta-abs-fn"
  (perga
    (:lett ci fixnum (mod vi *n*))
    (let ReferenceVolume (ReferenceVolume))
    (let ThisCellVolumeFactor (/ [ *V* ci ] ReferenceVolume))
    ;(let MeanEffectiveTemperature (MeanEffectiveTemperature *th* *tk*))
    (:lett ThisCell CoCell [ *Cells* ci ])
    (let PressureFactor
      (* 
       (/ *InitialPressure* (* 20 [Bar]))
       (/ ReferenceVolume 1.63e-4)
       ))
    (let result1
      (cond
       ((< vi  *n*) ; m*T, см. *inds*, simulate, 
        (* *InitialPressure* [ *V* ci ] (/ *Rmetric*))
        )
       ((< vi  (* 2 *n*)) ; m
        (* *InitialPressure* [ *V* ci ] (/ *Rmetric*) (/ ThisCell^TWall))
        )
       ((< vi  (* 3 *n*)) ; mvr
        (* 0.03 *freq* PressureFactor (ScalingDensityFactor) ThisCellVolumeFactor))
       ((< vi  (* 4 *n*)) ; q
        (* 0.1 10 *freq* PressureFactor
           1000.0 ; ?точность по q нам не важна, поэтому увеличиваем масштаб? 
           ))
       ))
    ;(when (= ci +b)
    ;  (_f * result1 100.0 ; точность для картера не важна
    ;      ))
    result1
    )) 


(defun ПроверитьВерность-y-delta-abs-fn ()
  "Чтобы проверить, что y-delta-abs-fn вычисляет масштаб адекватно, нужно прервать в какой-то момент круг расчёта и запустить эту функцию. 
  В разные моменты может быть разный результат - то, что в один момент покажется правильным, в другой будет неправильным"
  (cons '("Есть в реальности" "Оценка решателя")
        (map 'list
             'list
             *running_values*
             (map 'list 'y-delta-abs-fn (integer-range 0 (- (* 4 *n*) 1))))))

(defun make-solver-control-vector-by-y (coeff)
  "Зависит от *v*"
  (mjr_vec_*
   coeff
   (mjr_vec_make-from-func #'y-delta-abs-fn :len (* *n* 4))))


(defun call-ode (running_values)
  "Про точность. 
  Методом #'MJR_ODE:mjr_ode_erk-step-fehlberg-7-8, к-рый вроде бы советовал автор библиотеке, при шаге 0.0001 считаем эталонную точность, т.к.
  при шаге 0.00005 этот метод даёт такое же решение, совпадающие в 6 первых знаках мантиссы (первая цифра мантиссы 1)
  Метод #'mjr_ode:mjr_ode_erk-step-runge-kutta-4 даёт тот же результат до 6 знаков мантиссы при более мелком шаге 0.00005, 
  но mjr_ode_erk-step-runge-kutta-4 при 0.00005 работает в 3 раза быстрее, чем mjr_ode_erk-step-fehlberg-7-8 при шаге 0.0001

  Методом #'mjr_ode:mjr_ode_erk-step-runge-kutta-4 получаем при фикс. шаге 0.0003 погрешность порядка 0.05%

  Автор рекомендовал ставить y-err-abs-max, но не видно, чтобы она как-то влияла на #'mjr_ode:mjr_ode_erk-step-runge-kutta-4
   
  Вышеприведённые рез-ты посчитаны при y-err-abs-max = 1.0e-4


"
  (perga 
    (:lett new-time double-float (+ *tstart* *tim*))
    #|(:lett results mjr_mat
     (mjr_ode:mjr_ode_slv-ivp-erk-interval
      #'diffur2 running_values *tstart* new-time
      :x-delta-init 0.0001
      :x-delta-min 0.0001
      :x-delta-max 0.0009
      :y-err-abs-max (make-solver-control-vector-by-y 0.01)
      :algorithm #'mjr_ode:mjr_ode_erk-step-runge-kutta-4 ; #'MJR_ODE:mjr_ode_erk-step-fehlberg-7-8 ; 
      :return-all-steps t
      ))
    |#
    (SetVol *tstart*) ; *v* нужно в make-solver-control-vector-by-y
    (:lett results mjr_mat
     (mjr_ode:mjr_ode_slv-ivp-erk-interval
      #'diffur2 running_values *tstart* new-time
      :x-delta-min (* 0.000001 *period*)
      :x-delta-init (* 0.0001 *period*)
      :x-delta-max (* 0.009 *period*)
      ;:y-delta-rel-max 0.03
      :y-delta-abs-max (make-solver-control-vector-by-y *y-delta-abs-max-factor*)
      ;:y-err-rel-max 1e-3
      :y-err-abs-max (make-solver-control-vector-by-y *y-err-abs-max-factor*)
      :algorithm #'MJR_ODE:mjr_ode_erk-step-cash-karp-5-4
      :return-all-steps t))

    ;(break)
    (:lett times mjr_vec
     (extract-col-from-matrix results 0))
    (:lett results2 mjr_mat
     (extract-cols-from-matrix results (integer-range 1 (length running_values))))
    (values times results2)))


(defun get-last-solution-values ()
  "Если извлечь их после сокращения Results и Time, то получится неправильно"
  (perga
    (:lett last-time-index fixnum (- (mjr_mat_rows *Time*) 1))
    (values (extract-row-from-matrix *Results* last-time-index)
            (aref *Time* last-time-index))))


(defun stat (tstart running_values)
  "Движемся немного вперёд по времени и возвращаем два значения: новое время и матрицу решения в вычисленных точках. См. также diffur2, call-ode ."
  (perga
    (assert (boundp '*tim*)) ; may set (* 2 pi) by default

    (SetGas)

    ; устанавливаем начальные условия для солвера.
    ; если мы только начинаем, ставим *tstart* = 0 и начальные условия initial_values
    ; в противном случае, берём их равными решению в конце прошлого отрезка
    (let *tstart* tstart)
    ;(makunbound '*Time*)
    ;(makunbound '*Results*)
    (assert (= (dy-rank) (length running_values))) ; только для начальной отладки, killme.
    (call-ode running_values) ; возвращает values new_time new_results
    ))


          
    


