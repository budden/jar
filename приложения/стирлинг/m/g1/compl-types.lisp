; -*- Encoding: utf-8;  System :g1 -*-

(in-package :m)
(in-readtable :buddens-readtable-a)
(eval-when (:compile-toplevel)
  (assert (eq *READ-DEFAULT-FLOAT-FORMAT* 'double-float)))

(defstruct StatRegenRec
  "Статистика регенератора (по подобию программы Уриели). См. *StatRegen*, StatRegen, ClearOneCircleResults"
  ; regenReAvg ; среднее Re.
  ; St         ; соответствующий St
  ; Ntu        ; NTU, определено по странной ф-ле
  regEffect  ; КПД регенератора за цикл заряд-разряд
  regEnthalpyLoss  ; потеря энтальпии за этот цикл. Испольузется только для вывода, но не для расчёта КПД
  regEntropyCreation ; возникновение энтропии
  )

(defstruct StatFlowFrictionRec
  "Мощности трения для ячеек. См. StatFlowFriction, *StatFlowFriction*, ClearOneCircleResults"
  (Power
   (mandatory-slot 'Power)
   :read-only t)
  (AveragePressure ; не взвешено по величине потока
   (mandatory-slot 'AveragePressure)
   :read-only t)  
  )

        