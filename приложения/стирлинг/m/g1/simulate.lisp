; -*- Encoding: utf-8;  System :g1 -*-

(in-package :m)
(in-readtable :buddens-readtable-a)


(defun УменьшитьПотериВдвое (epsilon)
  "Дана эффективность. Считаем эффективность в предположении, что потери вдвое меньше"
  (- 1
     (/ (- 1 epsilon)
        2)))


(defun CalcCellEpsilon (ci)
  "Функция от состояния решения. Вычисляет мгновенную эффективность ячейки по потоку, имеющему место в это время"
  (perga
    (:lett str WorkingFluidStreamType (MakeCellStream ci) ; вообще их надо бы кешировать?
     )
    (:lett cell CoCell [ *Cells* ci ])
    (:lett epsilon
     dbl
     (cond
      ((^ cell RegenCellP)
       (УменьшитьПотериВдвое ; поскольку вызываемая ф-я даёт потери для полного цикла, состоящего из заряда и разряда
        (KAYS-LONDON-PERIODIC-FLOW:EpsilonPeriodicFlow_1_infinity cell^HeatExchanger str)))
      (t
       (EPSILON-N_TU:CounterFlow
        (KAYS-LONDON-NOTATION:N_tu_zero cell^HeatExchanger str)
        0.0))))
    epsilon))


(defun simulate (tt y ForStats) 
  "calculates right side of diff.eqn. Вызывается из diffur2inner , one-circle . Вызывает setVol, get_mvr , get_tri. 
 ForStats = t - значит, вычисляем не для решения, а для вычисления красивых второстепенных величин"
  (perga
    ;(show-expr `(:simulate :in ,tt))
    ; init vars
    (setf *tt* tt)
    (assert (= (length y) (dy-rank)))
    (setf *yinds* (extract-elements-from-vector y *inds*))
    (setf *ym_inds* (extract-elements-from-vector y *m_inds*))
    (setf *ymvr_inds* (extract-elements-from-vector y *mvr_inds*))
    (setf *yq_inds* (extract-elements-from-vector y *q_inds*))

    (dotimes (i *n*)
      (when (> 0 [ *yinds* i ])
        (warn "Отрицательное mT")
        ;(break)
        (return)))

    (dotimes (i *n*)
      (when (> 0 [ *m_inds* i ])
        (warn "Отрицательное m")
        (return)))

    ; calculate V
    (SetVol *tt*)
    ; calculate P ant Temp
    (setf *P*
          (mjr_vec_*
           (mjr_vec_/ *yinds* *V*)
           *Rmetric*
           ))
    ;(show-expr tt)    (show-expr *V*)    (show-expr *P*)
    ;(when (> tt 0) (break))

    (setf *Temp*
          (mjr_vec_/ *yinds* *ym_inds*))
    
    ; set tr, mvr, tl, mvl
    (setf *dmvr* (make-zero-vector *n*))
    (setf *mvl* (make-zero-vector *n*))
    (setf *tl* (make-zero-vector *n*))
    (setf *tr* (make-zero-vector *n*))
    (setf *movedrT* (make-zero-vector *n*))
    (setf *dQin* (make-zero-vector *n*))

    (when ForStats
      (setf *CellEpsilon* (make-zero-vector *n*)))

    ; tl(1)=0; mvl(1)=0;  % hot piston - они и так все нули
    ; tr(n)=0; mvr(n)=0;  % cold piston - они и так все нули

    (dotimes (i (- *n* 1))
      (:lett mvri dbl [ *ymvr_inds* i ])
      (setf [ *mvl* (+ i 1) ] (- mvri))
      (:lett dmvri number (get_dmvr mvri i)) ; Pnext Mnext Pthis Mthis))
      (assert (not (nan-p mvri)))
      (setf [ *dmvr* i ] dmvri)
      ; (show-expr *mvr*)
      (:lett tri double-float (get_tri i mvri)) 
      (setf [ *tr* i ] tri)
      (setf [ *tl* (+ i 1) ] tri )
      (setf [ *movedrT* i ] (MovedTAtCellBoundary i t))
      ; [ *tl* 0 ] и [ *tr* (- n 1) ] как были нулём, так их никто и не трогает
      )
    (vector-check-nan *dmvr*)

    (setf *dW* (mjr_vec_* *dV* *P*))

    (dotimes (i *n*)
      (setf [ *dQin* i ]
            (HeatIn i))
      )

    (when ForStats
      (dolist (ci `(,+h ,+r1 ,+r2 ,+k))
        ;(show-expr `(,ci ,(HX-AND-STR:hxRey cell^HeatExchanger str)))
        (setf [ *CellEpsilon* ci ] (CalcCellEpsilon ci))
        ))


    ;(show-expr *Qin*)
    ;(show-expr *P*)
    ;(show-expr *Temp*)

    (unless (every (lambda (x) (< (- *tk* 100) x (+ *th* 400.0))) *Temp*)
      (break "Температура зашкалила, tt=~S" *tt*))

    (values)
    ))

(defun mytanh (x)
  (cond
   ((> x 1000.0) 1.0)
   ((< x -1000.0) -1.0)
   (t (tanh x))))


(defun myscale (x amplification limit)
  (* limit (mytanh (* x amplification (/ limit)))))


(defun GetPistonSealLossMvr (Pnext Pthis)
  "Массовый расход утечек через уплотнения на холодной стороне. Плюс - если входит из картера в рабочий контур. Считаем, что через некий диаметр газ дросселируется
   с коэфтом трения 1, т.е. rho*v^2/2=DeltaP, при этом
   плотность берём по входу, хотя она меняется по ходу прохождения отверстия. Э
   то неверно, но в нулевом приближении так можно"
  (perga
    (let rhoNext (/ [ *ym_inds* +b ] [ *V* +b ]))
    (let rhoThis (/ [ *ym_inds* +c ] [ *V* +c ]))
    (let rhoIn (if (> Pnext Pthis) rhoNext rhoThis))
    (let rhoOut (if (> Pnext Pthis) rhoThis rhoNext))
    (let velocity
      (* 
       (signum (- Pnext Pthis))
       (sqrt (* (abs (- Pnext Pthis)) 2 (/ rhoIn)))))
    ;(show-expr velocity)
    (let massflow (* velocity (CircleArea *PistonSealHoleDiameter*) rhoOut))
    massflow
    ))
       



(defun get_dmvr (mvr i)
  "Функция от состояния решения. Получить производную массовой скорости. Вызывается из simulate"
  (perga
    (:lett Pnext number [ *P* (+ i 1) ])
    (:lett Mnext number [ *ym_inds* (+ i 1)])
    (:lett Pthis number [ *P* i ])
    (:lett Mthis number [ *ym_inds* i ])

    #|(:lett Pnext number 0.0)

    (do-for (j (+ i 1) (- *n* 1)) (_f + Pnext [ *P* j ]))
    (_f / Pnext (- *n* i 1))

    (:lett Mnext number 0.0) 
    (do-for (j (+ i 1) (- *n* 1)) (_f + Mnext [ *ym_inds* j ]))

    (:lett Pthis number 0.0)
    (do-for (j 0 i) (_f + Pthis [ *P* j ]))
    (_f / Pthis (+ i 1))

    (:lett Mthis number 0.0)
    (do-for (j 0 i) (_f + Mthis [ *ym_inds* j ]))|#
    

    (:lett ampl-diff double-float (- Pnext Pthis))
    (:lett time-factor dbl 
     (cond
      ;((= *CircleNumber* 0) ; плавный пуск
      ; (myscale *tt* (/ 6.5 *dmvr-switch-on-time*) 1.0))
      (t
       1.0))
     )
    (:lett supposed-mvr dbl
     (cond
      ((= (+ i 1) +b) ; утечки через уплотнения
       (GetPistonSealLossMvr Pnext Pthis))
      (t
       (myscale (* ampl-diff
                   (/ (min (abs Mnext) (abs Mthis))
                      (max (abs Mnext) (abs Mthis))))
                *Xi1* ; так переводится перепад давления в перепад массы
                *mvr-limit*  ; предельное значение mvr
                ))))
   
    (myscale (* time-factor (- supposed-mvr mvr))
             (/ 6.5 *dmvr-to-mvr-099-time*)
             *dmvr-limit* ; предел значения dmvr
             )))





