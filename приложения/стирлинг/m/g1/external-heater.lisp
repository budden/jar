; -*- Encoding: utf-8;  System :g1 -*-

(named-readtables:in-readtable :buddens-readtable-a)

(def-merge-packages::! :g1-external-heater
  (:always t)
  (:use :cl :physical-units :mjr_mat :mjr_vec :m0 :m
   :budden-adw-plotting)
  (:local-nicknames
   :str :fluid-stream :hx :heat-exchanger)
  (:import-from :perga-implementation
   perga-implementation:perga)
  (:import-from :budden-tools
   budden-tools:_f
   budden-tools:show-expr
   budden-tools:the*
   budden-tools:deftvar
   budden-tools:deftparameter
   BUDDEN-TOOLS:def-symbol-readmacro
   budden-tools:in-readtable
   budden-tools:str++
   budden-tools:|^|
   budden-tools:defun-to-file
   budden-tools:mandatory-slot
   budden-tools:byref
   budden-tools:with-byref-params
   BUDDEN-TOOLS:mlvl-list
   )
  (:custom-token-parsers budden-tools::convert-carat-to-^) 
  (:import-from :iterate-keywords
   iterate-keywords:iter)
  (:import-from :alexandria.0.dev
   ALEXANDRIA.0.DEV:copy-array
   alexandria.0.dev:eswitch)
  (:export
   "G1-EXTERNAL-HEATER::CalcExternalHeater
    G1-EXTERNAL-HEATER::ПодводВнутреннегоНагревателя
    G1-EXTERNAL-HEATER::КоллекторВнутреннегоНагревателя
    G1-EXTERNAL-HEATER::ЯдроВнутреннегоНагревателя
   ")
  )

(in-package :g1-external-heater)


(deftparameter *ExternalStream* str:AirStream1
               :documentation "Поток воздуха, омывающего наружный нагреватель"
               :initial-value
               (str:CalcStream
                (str:MAKE-AirStream1
                 :Temperature ([Celsius] 340)
                 :Pressure [Bar] :VolumetricFlow 0.07)))


#|(deftparameter *ExternalHeater* hx:HxBase
               :documentation "Наружный нагреватель"
               :initial-value
               (hx:CalcHeatExchanger
                (hx:MAKE-КоридорныйПучокТруб
                :ЧислоРядов 2
                :PipeDiameter ([mm] 7)
                :x1 (* [mm] 7 1.5)
                :x2 (* [mm] 7 2.3)
                :ДлинаТрубы ([cm] 10)
                :ЧислоТрубВРяду 240)))|#

; трубка 60х14, внутр. диам. 32
(defparameter *ВнутреннийДиаметрТрубки* ([mm] 30)) ; самый внутренний (внутренность подвода)
(defparameter *ВнешнийДиаметрТрубки* ([mm] 33)) ; самый внешний (наружность пазов)
(defparameter *ВнутреннийДиаметрРёбер* ([mm] 38))
(defparameter *ДлинаТрубки* ([mm] 225))
(defparameter *ЧислоТрубок* 12)
(defparameter *ВнешТолщинаРебра* ([mm] 1))
(defparameter *ВнешШагРебра* ([mm] 3.5))
(defparameter *ВнешДлинаРебраПоТечению* ([mm] 36))
(defparameter *ЗазорВнутреннихПазов* ([mm] 0.9))
(defparameter *ВнешнДиаметрИсходнойТрубки* ([mm] 55)) ; делаем меньше, т.к. нужно сделать сектора

(defun ОбщаяДлинаТрубок () (* *ДлинаТрубки* *ЧислоТрубок*))

(show-expr (ОбщаяДлинаТрубок))


(deftparameter *ExternalHeater* hx:HxBase
               :initial-value
               (hx:CalcHeatExchanger
                ; итого 
                (perga
                  (let ВысотаРёберСДвухСторон ; суммарная высота по двум сторонам
                    ([mm] 5.5 2)
                    #|(- (min
                        (/ (CircleLength M::*RegenDomeDout*) ; периметр размещения
                           *ЧислоТрубок* ; число труб
                           ) ; место на одну трубу
                        *ВнешнДиаметрИсходнойТрубки*
                        )                        
                       *ВнутреннийДиаметрРёбер* 
                       )|#
                    )
                  (show-expr ВысотаРёберСДвухСторон)
                  (hx:MAKE-FlatChannelsMatrix
                   :Length *ВнешДлинаРебраПоТечению*
                   :GapThickness (- *ВнешШагРебра* *ВнешТолщинаРебра*)
                   :RebroThickness *ВнешТолщинаРебра*
                   :FrontalArea
                   (* ВысотаРёберСДвухСторон
                      (ОбщаяДлинаТрубок) ; общая длина трубок
                      )
                   :XiEntryExit 1
                               ; оребрённые трубки стоймя
                   ))))
               


(deftparameter *ТрубаКТеплоаккумулятору* hx:HxBase
               :initial-value
               (hx:CalcHeatExchanger
                (hx:MAKE-Pipe
                 :Length 3.0
                 :PipeDiameter ([cm] 20)
                 :LengthNuFactorMM90 1)))


(deftparameter *КоллекторВнешнегоТеплообменника* hx:HxBase
               :initial-value
               (hx:CalcHeatExchanger
                (hx:MAKE-ParallelPipes
                 :NumPipes 12
                 :Length ([mm] 810)
                 :PipeDiameter ([mm] 150) ; на самом деле там квадрат 23х10
                 :LengthNuFactorMM90 1)))


(defparameter *НужнаяТепловаяМощность* 592.0
  ; берётся из расчёта машины
  )

(defun CalcExternalHeater ()
  (perga
    (WARN-ONCE:start-warnings-over)
    (format t "~%---------- Внешний нагреватель ------------~%")
    (let ПадениеДавленияВТрубеКТеплоаккумулятору
      (HX-AND-STR:hxPressureDrop *ТрубаКТеплоаккумулятору* *ExternalStream*))
    (show-expr ПадениеДавленияВТрубеКТеплоаккумулятору)
    (let ПадениеДавленияВКоллекторе
      (HX-AND-STR:hxPressureDrop *КоллекторВнешнегоТеплообменника* *ExternalStream*))
    (show-expr ПадениеДавленияВКоллекторе)
    (let ПадениеДавлениеВоВнешнемНагревателе
      (HX-AND-STR:hxPressureDrop *ExternalHeater* *ExternalStream*))
    (show-expr ПадениеДавлениеВоВнешнемНагревателе)
    (let МощностьПрокачки
      (* (+ ПадениеДавлениеВоВнешнемНагревателе ПадениеДавленияВТрубеКТеплоаккумулятору ПадениеДавленияВКоллекторе)
         *ExternalStream*^VolumetricFlow))
    (show-expr МощностьПрокачки)
    (let Capacity
      (KAYS-LONDON-NOTATION:c *ExternalStream*))
    (let КПД
      (EPSILON-N_TU:CounterFlow
       (KAYS-LONDON-NOTATION:N_tu_zero *ExternalHeater* *ExternalStream*)
       0.0))
    (show-expr КПД)
    (let ДельтаТ
      (/ *НужнаяТепловаяМощность* ; мощность из расчёта машины
         Capacity
         КПД))
    (show-expr ДельтаТ)
    (values)
    ))
               


(CalcExternalHeater)


;;; Оптимизируем внутренний и внешний нагреватель без прогона машины


;;; Потоки позаимствованы из расчитанной машины


(defparameter *AvgHeaterStream*
  ; (MakeAvgStreamAtHx +h)
  #S(WorkingFluidStreamType :VolumetricFlow 0.0377135439471171
                          :Pressure 84957.40356019524
                          :Temperature 603.5672324425382
                          :ThermalExpansionCoefficient 0.0016568162521897736
                          :nu 6.139344038035749E-5
                          :rho 0.49206297740758676
                          :lambda 0.04786460117899483
                          :Cp 1053.419118812933
                          :RGas 287.0
                          :GammaGas 1.4
                          :Cv 752.4422277235236))

(defparameter *MaxHeaterStream*
  ; макс. поток внутри нагревателя
  ; (MakeTempStreamAtTimeAndHx 0 +h) - после расчёта машины нужно сделать
  #S(WorkingFluidStreamType :VolumetricFlow 0.035127972930336135
                          :Pressure 110474.56152350984
                          :Temperature 621.0956646293241
                          :ThermalExpansionCoefficient 0.00161005792979864
                          :nu 4.950314819544235E-5
                          :rho 0.6217973998830066
                          :lambda 0.048933835542388775
                          :Cp 1057.1000895721582
                          :RGas 287.0
                          :GammaGas 1.4
                          :Cv 755.0714925515416))


(defun КоллекторВнутреннегоНагревателя ()
  "Деталь, к-рая выводит в регенератор и обратно. Интересует вредное пр-во"
  (hx:MAKE-Annulus
   :Length (* 0.5 *ВнешнийДиаметрТрубки*)
   :OuterDiam M::*RegenDomeDout*
   :InnerDiam M::*RegenDomeDIn*
   :XiEntryExit 1.0 
   ))


(defun ДлинаЗазораПоТечению ()
   (- (* (CircleLength (- *ВнешнийДиаметрТрубки* *ЗазорВнутреннихПазов*) ; по среднему радиусу
                       )
         0.5)
      *ЗазорВнутреннихПазов*))

(defun ПлощадьСеченияТрубки ()
  "Для подвода"
  (CircleArea *ВнутреннийДиаметрТрубки*))


; также могут быть полезны    ; http://www.kkz.ru/orebrennie_trubi/401?type=object

(defun ЯдроВнутреннегоНагревателя ()
  (hx:MAKE-FlatChannelsMatrix
   :Length (ДлинаЗазораПоТечению)
   :GapThickness *ЗазорВнутреннихПазов*
   :RebroThickness ([mm] 1)
   :FlowArea
   (*
    *ЗазорВнутреннихПазов* ; зазор
    (ОбщаяДлинаТрубок)
    2 ; вставка обходится с двух сторон
    )
   :XiEntryExit 1.0
   :OneWallP t
                               ; трубка 32x2 с н-образной вставкой - проход через вставку. Здесь нам важен теплообмен и чтобы давление было больше, чем на вход-выход.
   ))

(defun ПодводВнутреннегоНагревателя ()
  "Трубки, в которые вставлены элементы теплообменника"
  (hx:MAKE-ParallelPipes
       ; Интересует только сопротивление
   :Length *ДлинаТрубки*
                  ; в среднем воздух доходит только до
                  ; середины, но потом он попадает во вторую трубку, поэтому пусть так.                
   :XiEntryExit (+ 1 0.3)
   :PipeDiameter ; по площади прохода берём трубку с такой
                                        ; же площадью
   (* 2 (sqrt (/
               (-
                (* 0.5 (ПлощадьСеченияТрубки)) ; полкруга
                (*
                 ([mm] 0.5) ;толщина материала половины Z
                 (+ *ВнутреннийДиаметрТрубки* (ДлинаЗазораПоТечению)))
                (* *ЗазорВнутреннихПазов* ; зазор тоже не участвует
                   2
                   (ДлинаЗазораПоТечению)) 
                ) 
               pi)))
   :NumPipes *ЧислоТрубок*
   :LengthNuFactorMM90 1.0
   ; львиная доля падения приходится на скорость
   ))
      
   #|(hx:MAKE-FlatChannelsMatrix :Length ([mm] 75)
                               :GapThickness ([mm] 41 1/5 0.99)
                               :RebroThickness ([mm] 1)
                               :FlowArea 0.02
                               :XiEntryExit 210
                               ) ; 0.25, 500, 1500 - нарочито плохой - получили 115Вт, 18.7% (37.6 от Карно) после 34 оборотов
   |#

   #|(hx:MAKE-FlatChannelsMatrix :Length ([mm] 40)
                               :GapThickness ([mm] 4)
                               :RebroThickness ([mm] 1)
                               :FlowArea 0.02
                               :XiEntryExit 85
                               ) ; 0.407, 204, 800 - чуть лучше, но тоже нарочито плохой - 
                                 ; получили 147вт, 22.67 (=0.456 от Карно) после 34 оборотов
   |#


   #|(hx:MAKE-ParallelPipes :Length ([mm] 14)
                          :XiEntryExit 1.0
                          :PipeDiameter ([mm] 1.7)
                          :NumPipes 2400) ; 0.54, 39, 76 . Сверлёная деталь из http://www.chipmaker.ru/topic/150921/page__view__findpost__p__2641149 
                                          ; площадь эрозируемой поверхности = 0.17м2 (должно получиться 9.37тыр за эрозию)
   |# 



   #|(hx:MAKE-ParallelPipes :Length ([mm] 2 45)
                          :XiEntryExit 2.0
                          :PipeDiameter ([mm] 4.5)
                          :LengthNuFactorMM90 1.0
                          :NumPipes 360) ; 0.57, 73, 515
                       |#
   #|(hx:MAKE-ParallelPipes :Length ([mm] 4 20)
                          :XiEntryExit 4.0
                          :PipeDiameter ([mm] 2.5)
                          :NumPipes (* 120 11)) ; 0.79, 109, 518 - подходит, но ещё теснее стало
    |#

   #|(hx:MAKE-FlatChannelsMatrix :Length (* 0.5 (CircleLength ([mm] 32)))
                               :GapThickness ([mm] 1)
                               :RebroThickness ([mm] 1)
                               :FlowArea (* [mm] 1 ; зазор 
                                            [mm] 8 ; высота - от R=12 до R=20
                                            25 ; штук в одном (удлинняем)
                                            12 ; штук по кругу поместится
                                            )
                               :XiEntryExit 1
                               ; 0.7, 602, 120, 0.24
                               ; за 34 оборота - 22.6%, 147 Вт. Не хватает 
                               ; учёта коллекторов нагревателя.
                               )|# 

   #|(hx:MAKE-FlatChannelsMatrix :Length ([mm] 80)
                               :GapThickness ([mm] 2.2)
                               :RebroThickness ([mm] 1.3)
                               :FlowArea (* [mm] 2.2 [mm] 20 180)
                               :XiEntryExit 1 
                               ) ; 0.75, 58, 633. Первая деталь с рёбрами из темы http://www.chipmaker.ru/topic/150921/. 32 тыр за эрозию за всё
                                 ; Площадь теплообмена 0.58 м2
   |#                          

   #|(hx:MAKE-ParallelPipes :Length ([cm] 12)
                          :XiEntryExit 1.0
                          :PipeDiameter ([mm] 4)
                          :LengthNuFactorMM90 1.0
                          :NumPipes 360) ; 0.6, 83, 542 - лучше, чем было. http://www.kkz.ru/orebrennie_trubi/401?type=object труба 6x1, наружный диам = 20.
    |#

   #| (hx:MAKE-ParallelPipes :Length ([cm] 20)
                          :XiEntryExit 2.0
                          :PipeDiameter ([mm] 8)
                          :LengthNuFactorMM90 1.0
                          :NumPipes 60
                          ; 0.53, 392, 402, 0.3 - оребрённые трубки 10мм с шагом 2мм
                          ) 
   |# 

   #|(hx:MAKE-ParallelPipes :Length ([cm] 20)
                          :XiEntryExit 2.0
                          :PipeDiameter ([mm] 11)
                          :LengthNuFactorMM90 1.0
                          :NumPipes 15
                          ; - оребрённые трубки 16x1.0 с шагом 2.8мм
                          )|#


; (* *ЯдроВнутреннегоНагревателя*^NumPipes 2 (CircleArea ([mm] 20))) ; это круг диаметром 55см.

; 0.59, 121, 535 - это приемлемо


(defparameter *ВнутреннийНагревательИтого*
  (hx:CalcHeatExchanger 
   (hx:MAKE-SequentialHeatExchangers
    :HydrDiam 1.0 ; не имеет значения, но нужна при вычислении чего-то там
    :FlowArea 1.0 ; нужна только для расчёта Re, Re не имеет смысла здесь
    :Members
    (list 
     (ПодводВнутреннегоНагревателя)
     (КоллекторВнутреннегоНагревателя)
     (ЯдроВнутреннегоНагревателя)
     ))))


(defun CalcInternalHeater ()
  (perga
    ;(let *ЯдроВнутреннегоНагревателя* (Hxi +h))
    (WARN-ONCE:start-warnings-over)
    (format t "~%-------- Внутренний нагреватель ----------------")
    (let fmt1 "~%~A~18t~A~28t~A~38t~A~48t~A")
    (let fmt2 "~%~A~18t~9,4G~28t~9,4G~38t~9,4G~48t~9,4G")
    (format t fmt1 "Показатель" "Подвод" "Коллектор" "Ядро" "Итого")
    (let hxs (append *ВнутреннийНагревательИтого*^Members (list *ВнутреннийНагревательИтого*)))
    (flet LineOfTable (Показатель fn)
      (let Results (mapcar fn hxs))
      ;(let Итого (apply #'+ Results))
      ;(apply 'format t fmt2 Показатель `(,@Results ,Итого))
      (apply 'format t fmt2 Показатель Results)
      )
    (LineOfTable 'Re (lambda (hx) (hx-and-str:hxRey hx *MaxHeaterStream*)))
    (LineOfTable 'Effy (lambda (hx)
                       (EPSILON-N_TU:CounterFlow
                        (KAYS-LONDON-NOTATION:N_tu_zero hx *MaxHeaterStream*)
                        0.0)))
    (LineOfTable 'PressureDrop
                 (lambda (hx)
                   (HX-AND-STR:hxPressureDrop hx *MaxHeaterStream*)))
    (LineOfTable 'DeadVolumeCm
                 (lambda (hx)
                   (/ hx^FluidVolume [cm3])))
    (LineOfTable 'HxArea
                 (lambda (hx)
                   hx^HeatExchangeArea))
    (terpri)
    (terpri)
    ))

(CalcInternalHeater)









