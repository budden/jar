; -*- Encoding: utf-8;  System :g1 -*-
;; Функции для определения потока тепла. Не зависят от конфигурации машины

(in-package :m)
(in-readtable :buddens-readtable-a)

(defun OutTStandard (ci)
  [ *Temp* ci ])

(defun NoHeatIn (ci)
  (declare (ignore ci))
  0.0)

(defun MakeSimpleHeatInFn (ExchangerAlpha)
  "Простой теплообменник с фиксированным коэф-том теплоотдачи"
  (lambda (ci)
    (perga
      (:lett Cell CoCell [ *Cells* ci ])
      (:lett TWall double-float Cell^TWall)
      (* (- TWall [ *Temp* ci ]) ExchangerAlpha) 
      )))

(defun HxHeatIn (ci)
  "Можно назначить её теплообменнику, чтобы она вычисляла мощность теплоотдачи. Функция от состояния решения. Не годится для регенератора, т.к. предполагается постоянной плотность"
  ; вообще, есть большие сомнения в адекватности квазистационарных приближений для Стирлинга, 
  ; с учётом малости пути, который проходит газ. А также именно приближение т-ры на выходе
  ; из теплообменника, к-рое для нас представляет интерес, нельзя смоделировать, если мы
  ; усредняем т-ру имеющегося газа с т-рой входящего газа. Наверное, если разбить теплообменник
  ; на несколько ячеек, проблема смягчится. Но тогда нужно вводить новые формулы для расчёта, т.к.
  ; получатся слишком короткие трубки. И полностью проблема не исчезнет, т.к. будет разное распределение
  ; т-р на входе. 
  (perga
    (:lett temp dbl [ *Temp* ci ])
    (:lett s WorkingFluidStreamType (MakeCellStream ci))
    (:lett c CoCell [ *Cells* ci ])
    (:lett hx hx:HxBase c^HeatExchanger)
    (:lett res dbl (* (HX-AND-STR:hxAlpha hx s) hx^HeatExchangeArea (- c^TWall temp)))
    ;(when (= i +h) (print res))
    res
    ))

(defun RegenOutT (ci right-p)
  "Функция состояния решения. Для регенератора - определяет т-ру выходящего потока с учётом одностороннего КПД регенератора"
  (perga
    (:lett Cell CoCell [ *Cells* ci ])
    (assert Cell^RegenCellP)
    (let OutT (if right-p Cell^RightOutT Cell^LeftOutT))
    (let InT (if right-p Cell^LeftOutT Cell^RightOutT))
    ; довольно вольно обращаемся с понятием эффективности
    ; считаем, что эффективность - это доля потерянного перепада
    ; между левой и правой т-рой металла, а не между
    ; т-рой входящего потока и т-рой металла на выходе
    ; Посколку т-ра входящего потока близка к т-ре металла на входе
    ; разница невелика
    (:lett epsilon dbl (CalcCellEpsilon ci))
    (linear-scalar-fun epsilon 0.0 InT 1.0 OutT)
    ))


(defun CellOutputT (ci right-p)
  "Функция состояния решения. Температура воздуха, выходящего из ячейки влево, если он выходит. Если он входит, то возврат этой ф-ии
 не имеет значения"
  (perga
    (:lett Cell CoCell [ *Cells* ci ])
    (let OutT (if right-p Cell^RightOutT Cell^LeftOutT))
    (let OutTFn Cell^OutTFn)
    (etypecase OutTFn
      (function (funcall OutTFn ci right-p))
      (null
       (etypecase OutT
         (null (OutTStandard ci))
         (dbl OutT)
         (function (funcall OutT ci))))
      (symbol (funcall OutTFn ci right-p)))))


(defun MakeCellStream (ci)
  "Создаёт поток в ячейке, соответствующий текущим значениям состояния задачи"
  (perga
    (:lett p dbl [ *P* ci ])
    ; (:lett tt dbl [ *Temp* i ])
    (:lett s WorkingFluidStreamType
     (MAKE-WorkingFluidStreamType
      :Pressure p
      :Temperature [ *Temp* ci ]
      ))
    (str:CalcStream s)
    ; средний массовый поток - считаем как средний между потоком на входе и выходе
    (:lett average-mass-flow
     dbl
     (abs (* 0.5 (+ [ *ymvr_inds* ci ] [ *ymvr_inds* (- ci 1) ]))))
    (setf s^VolumetricFlow (/ average-mass-flow s^rho))
    s
    ))


(defun HeatIn (ci)
  "Функция от состояния решения. Возвращает мгновенную мощность потока тепла в секцию."
  (perga
    ;(:lett thistemp double-float [ *Temp* i ])
    ;(:lett mvl double-float [ *mvl* i ])
    ;(:lett mvr double-float [ *ymvr_inds* i ])
    ;(budden-tools::ignored mvl mvr)
    (:lett Cell CoCell [ *Cells* ci ])
    ;(:lett TWall double-float Cell^TWall)
    (funcall Cell^HeatInFn ci))
  )



;; Теперь можно попрбовать приделать теплообменник в одном месте и посмотреть, как будет работать
