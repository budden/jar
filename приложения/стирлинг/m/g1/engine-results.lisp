; -*- coding: utf-8;  System :g1 -*-


(in-package :m)
(in-readtable :buddens-readtable-a)

(defun DisplacerGap ()
  (hx:CalcHeatExchanger
   (hx:MAKE-Annulus :Length *dStroke*
                    :OuterDiam (+ *dBore* (* 2 *dhotSideRadialClearance*))
                    :InnerDiam *dBore*)))

(defun ShuttleLossesWalker () 
	"Уолкер, стр. 63. 0.4 - безразм. коэфт, остальное - в СИ "
        (* 0.4
           (expt *dStroke* 2)
           *AverageStream*^LAMBDA
           *dBore*
           (- *th* *tk*)
           (/
            (* *dhotSideRadialClearance*
               *DisplacerShuttleLength*))))

(defun ShuttleLossesMartini ()
  (perga
    (let XB 0)
    (let SD (/ *dStroke* [cm]))
    (let KG (* *AverageStream*^LAMBDA [cm]))
    (let DC (/ *dBore* [cm]))
    (let G (/ *dhotSideRadialClearance* [cm]))
    (let LB (/ *DisplacerShuttleLength* [cm]))
    (*
     (/ (+ 1 XB)
        (+ 1 (expt XB 2)))
     pi 1/8
     (expt SD 2)
     KG
     (- *th* *tk*)
     DC
     (/
      (* G LB)))))

(defun AppendixPumpingLossBudden ()
  "Treat appendix gap as a regenerator. Calculate average stream at entry point to a gap and calculate regenerator efficiency for that stream"
  (perga
    (:lett DisplacerGapHx hx:HxBase
     (hx:CalcHeatExchanger
      (hx:MAKE-FlatChannel :Length *DisplacerShuttleLength*
                           :Height *dhotSideRadialClearance*
                           :Width (CircleLength *dBore*))))
    (let DisplacerGapVolume
      DisplacerGapHx^FluidVolume)
    (let TRegen (* 1/2 (+ *th* *tk*)))
    (let Mmin (* *StatMinimumPressure* DisplacerGapVolume (/ *Rmetric*) (/ TRegen))) 
    (let Mmax (* *StatMaximumPressure* DisplacerGapVolume (/ *Rmetric*) (/ TRegen)))
    (let TimeAvgMassFlowOfGapStream 
      (/       
       (- Mmax Mmin)
       (/ *period* 2)))
    (let PAvg (* 1/2 (+ *StatMaximumPressure* *StatMinimumPressure*)))
    (:lett TimeAvgGapStream WorkingFluidStreamType (MAKE-WorkingFluidStreamType :Pressure PAvg :Temperature TRegen))
    (str:CalcStream TimeAvgGapStream)
    (str:SetStreamMassFlow TimeAvgGapStream TimeAvgMassFlowOfGapStream)
    (let EfficiencyOfRegenerationInADisplacerGap
      (KAYS-LONDON-PERIODIC-FLOW:EpsilonPeriodicFlow_1_infinity DisplacerGapHx TimeAvgGapStream))
    ; точка, к-рая была в правом конце, пройдёт не весь цилиндр, а только его долю, равную (- 1 (/ *StatPressureRatio*))
    (unless (> EfficiencyOfRegenerationInADisplacerGap 0.7)
      (WARN-ONCE:warn-once
       "Low efficiency of regeneration in a displacer gap. AppendixPumpingLoss can be underestimated"
       ))
    (let MaximumTemperatureChange (* (- *th* *tk*) (- 1 (/ *StatPressureRatio*))))
    (let GasInAGapHeatCapacity 
      (* Mmax TimeAvgGapStream^Cp))
    ;(show-expr (HX-AND-STR:hxSkorostqVSechenii DisplacerGapHx TimeAvgGapStream))
    (let LossPower
      (* *freq* GasInAGapHeatCapacity MaximumTemperatureChange (- 1 EfficiencyOfRegenerationInADisplacerGap)))
    LossPower 
  ))


(defun check-epsilon ()
  (assert (= MJR_CMP:*mjr_cmp_eps* least-positive-double-float)))

(check-epsilon)

(defun CrankcasePumpingLossBuddenPower (#|CrankcaseVolume VolumeAmplitude|#)
  (* *freq* (- (IntegrateByCycle
                (lambda (tindex) [ *dWS* tindex +b ]))))
  #|"Оценка, исходя из наихудшего сценария - адиабатный процесс, затем изохорный, затем адиабатный, затем изохорный"
  (perga
    (:lett str WorkingFluidStreamType (MakeAvgStreamAtHx +k))
    (let p1 str^Pressure)
    (let t1 str^Temperature)
    (let v1 CrankcaseVolume)
    (let v2 (- CrankcaseVolume VolumeAmplitude))
    (let gamm str^GammaGas)
    (let cv str^Cv)
    (let mass (* str^rho v1))

    (show-expr v1)
    (show-expr v2)

    (let p2 (PAd t1 p1 v1 v2 gamm))

    (show-expr p1)
    (show-expr p2)
    (show-expr (VAd t1 p1 p2 v1 gamm))
    (let t2 (TAd t1 p1 p2 gamm))
    (let w12 (WAd t1 t2 mass cv))
    ; теперь изохорный процесс
    (let p3 (* p2 t1 (/ t2)))
    (let t3 t1)
    (let v3 v2)
    (show-expr p3)

    (let v4 v1)
    (let p4 (PAd t3 p3 v3 v4 gamm))
    (let t4 (TAd t3 p3 p4 gamm))
    (let w34 (WAd t3 t4 mass cv))
    (show-expr w12)
    (show-expr w34)
    (* *freq* (+ w12 w34)))|#)



(defun ShuttleLossesBudden ()
  (perga
    #|
    setAnnulus[displacerGap, dBore + 2dhotSideRadialClearance, dBore, dStroke];
    LengthNuFactorMM90[displacerGap] = 1.2 
    struType[DisplacerGapAverageStream] ^= WorkingFluidStreamType;

    In[74]:=
     (strVolumetricFlow[DisplacerGapAverageStream] ^= 
          hxFlowArea[
              displacerGap]*\((dStroke/
                    2*2*pi*freq (*макс.скорость поршня*) ))/((2)( (*т.к. газ между*) ));)
      (strTemperature[DisplacerGapAverageStream] ^= (trh + trk)/2;)
     (strPressure[DisplacerGapAverageStream] ^= pmean)
      (CalcStream[DisplacerGapAverageStream]);

    displacerGapRe := hxRey[displacerGap, DisplacerGapAverageStream]

    displacerGapHeatTransferCoeff := 
    hxAlpha[displacerGap, DisplacerGapAverageStream]; |#
  ; выдвинулся на полный stroke в одну сторону

    ; не весь, а лишь одно звено в цепи, ведь челночная передача тепла происходит
    ; в несколько этапов по цепочке. 
    (:lett dg hx:Annulus (DisplacerGap))
    (:lett TeploemkostqGazaVZazore dbl
     (* *AverageStream*^Cp
        *AverageStream*^RHO
        dg^FluidVolume
        0.5 ; половина воздуха прилипнет и её т-ра не поменяется
        ))
    (:lett PerepadTemperatur dbl
     (* (- *th* *tk*) *dStroke* *DisplacerShuttleLength*))
    (+
     (* TeploemkostqGazaVZazore *freq* PerepadTemperatur)
     (max ; передча тепла на стенки
      (ShuttleLossesWalker) (ShuttleLossesMartini)))
    ))


(defun RegenMatrixHeatConductionLossPower ()
  "Считаем сопротивление средним между сопротивлением сетки и газа, взвешенное по пористости. Берём одну из ячеек регенератора и считаем, что по всей длине регенератора сетка такая же. На самом деле надо считать по каждой ячейке регенератора"
  (perga
    (let result nil)
    (dolist (sym (list '+r1 '+r2))
      (let i (symbol-value sym))
      (:lett Cell CoCell [ *Cells* i ])
      (:lett hx hx:HxBase Cell^HeatExchanger)
      (:lett Lambda dbl (HX-AND-STR:ПродольнаяТеплопроводность hx *AverageStream*))
      (let LossPower (* Lambda (- Cell^LeftOutT Cell^RightOutT)))
      (push `(,sym ,LossPower) result)
      )
    (nreverse result)
    ))
  


(defun IntegrateByCycle (fn)
  "Интегрирует скалярную ф-ю за цикл (а можно бы было и векторную?). Функция должна принимать целое число - индекс в массиве *btS*. Последняя и первая точка учитываются с весом 0.5, т.к. они совпадают"
  (perga
    (:lett limit fixnum (- (length *btS*) 2))
    (:lett res dbl 0.0)
    (:lett i fixnum 0)
    (:lett fi dbl 0.0)
    (:lett bt dbl 0.0)
    (:lett btnext dbl (coerce [ *btS* 0 ] 'dbl))
    (:lett dt dbl 0.0)

    (flet set-fi (i) 
      (setf fi (coerce (funcall fn i) 'dbl)))

    (flet set-bt (i) 
      (setf bt btnext)
      (setf btnext (coerce [ *btS* (+ i 1) ] 'dbl))
      (setf dt (- btnext bt)))

    (set-fi 0)
    (set-bt 0)
    (loop
      (incf res (* dt 0.5 fi))
      (incf i)
      (set-fi i)
      (incf res (* dt 0.5 fi))
      (when (> i limit)
        (return))
      (set-bt i)
      )
    res))


(defmacro massFlowFun ()
  "См. MassFlowFunTimesHxFlowArea"
  )

(defun MassFlowFunTimesHxFlowArea (tindex i)
  "Массовый расход через ячейку i во время с индексом tindex"
  (* 0.5
     (+ (if (= i 0) 0 [ *mvrS* tindex (- i 1) ])
        [ *mvrS* tindex i ]))
  )

(defun Hxi (i)
  (perga
    (:lett c CoCell [ *Cells* i ])
    ; если hx нет, значит, неверно заданы ячейки или мы зря это вызываем
    (:lett hx hx:HxBase c^HeatExchanger)
    hx))


(defun MakeTempStreamAtTimeAndHx (tindex ci)
  "Создать временный поток, соответствующий движению во время с индексом tindex через ячейку i"
  (perga
    ;(:lett hx hx:HxBase (Hxi i))
    ; (:lett cell CoCell [ *Cells* i ])
    ; (:lett hx hx:HxBase cell^HeatExchanger)
    (:lett res WorkingFluidStreamType 
     (MAKE-WorkingFluidStreamType
      :Temperature [ *ts* tindex ci ]
      :Pressure [ *pS* tindex ci ]))
    (str:CalcStream res)
    (str:SetStreamMassFlow
     res
     (abs (MassFlowFunTimesHxFlowArea tindex ci)))))

(defun HeatExAvgRe (i) 
  "Среднее Re по теплообменнику за цикл. Довольно абсурдно"
  (perga
    (flet reynumB (tindex)
      (perga
        (let str (MakeTempStreamAtTimeAndHx tindex i))
        (:lett hx hx:HxBase (Hxi i))
        (hx-and-str:hxRey hx str)))
    (/ (IntegrateByCycle #'reynumB) *period*)
    ))


(defun HeatExAvgTemperature (ci) 
  "Средняя температура за цикл"
  (perga
    (flet SubIntFun (tindex)
      (perga
        (:lett str WorkingFluidStreamType
         (MakeTempStreamAtTimeAndHx tindex ci))
        str^Temperature
        ))
    (/ (IntegrateByCycle #'SubIntFun) *period*)
    ))

    

(defun MakeAvgStreamAtHx (ci)
  "Делает поток при среднем давлении, средней температуре теплообменника (может снижаться для нагревателя при подборе т-ры) и среднем расходе. Довольно безсмысленно во многих отношениях"
  (perga
    (:lett c CoCell [ *Cells* ci ])
    ; если hx нет, значит, неверно заданы ячейки
    (:lett hx hx:HxBase c^HeatExchanger)
    (:lett tt dbl (HeatExAvgTemperature ci))
    (:lett stream1 WorkingFluidStreamType (MAKE-WorkingFluidStreamType
                               :Pressure (pmean)
                               :Temperature tt))
    (str:CalcStream stream1)
    (:lett rey dbl (HeatExAvgRe ci))
    (:lett vel dbl (* rey stream1^NU (/ hx^HydrDiam)))
    (:lett vol dbl (* vel hx^FlowArea))
    (:lett result WorkingFluidStreamType
     (MAKE-WorkingFluidStreamType
      :Pressure (pmean)
      :Temperature tt
      :VolumetricFlow vol))
    (str:CalcStream result)
    result
    )
  )


(defun dispWallThickness ()
  (max *DispMinWallThickness*
       (WallThickness (- (Pmax) (Pmin)) *dBore* *HotCapCreep*)))

(defun regenWallThickness ()
  (max
   *RegenDomeMinWallThickness*
   (WallThickness (- (Pmax) ([Bar] 1)) *RegenDomeDout* *HotCapCreep*)))

(defun WallLeakageKoeff ()
  (perga
    (let regenWallThickness (regenWallThickness)) 
    (let dispWallThickness (dispWallThickness))
    (let cqwr
      (*
       (+
        (* (CircleLength *dBore*) dispWallThickness (/ *DisplacerShuttleLength*))
        (* (CircleLength *RegenDomeDout*) regenWallThickness (/ *RegenDomeLength*) *NoOfRegens*)
        )
       *kwr*
       ))
    (show-expr-t (/ regenWallThickness [mm]))
    (show-expr-t (/ dispWallThickness [mm]))
    cqwr
    ))


(defun pmean ()
  "Среднее давление в цикле. Сделать его переменной"
  (/ (IntegrateByCycle (lambda (i) [ *pS* i 0 ]))
     *period*)
  )

(defun expDivComp (fn)
  "fn - производная объема, вытесняемого каким-то поршнем. Считаем отношение работы расширения этим поршнем к работе сжатия"
  (perga
    (:lett pmean dbl (pmean))
    (:lett exp dbl
     (IntegrateByCycle
      (lambda (i)
        (max 0.0
             (* (funcall fn [ *btS* i ])
                (- [ *pS* i 0 ] pmean))))))
    (:lett comp dbl
     (IntegrateByCycle
      (lambda (i)
        (min 0.0
             (* (funcall fn [ *btS* i ])
                (- [ *pS* i 0 ] pmean))))))
    (values comp exp (- (/ comp exp)))
  ))

(defun ExpAndCompPList (fn)
  (mlvl-bind (comp exp div) (expDivComp fn)
    (declare (ignore div))
    `(:comp ,comp :exp ,exp)))


(defun Torque (time PCrankcase)
  (perga
    (let ti1 (time-to-ti time))
    (* (funcall 'dVolumeForMaxovik time)
       (- [ *pS* ti1 0 ] PCrankcase))))

(defun TorqueN (time N)
  (perga
    (let result 0.0)
    (dotimes (i N)
      (_f + result (Torque (+ time (* *period* i (/ N))) 0.0)))
    result))


(defun TorquePlotsVsNumberOfCylinders ()
  "Рисует графики изменения крутящего момента в зависимости от числа цилиндров, мощность нормализована"
  (plot-scalar-fn (list 
                   (lambda (time) (Torque time (pmean)))
                   (lambda (time) (/ (TorqueN time 2) 2))
                   (lambda (time) (/ (TorqueN time 3) 3))
                   (constantly 0))
                  0 *period* :title "TorquePlotsVsNumberOfCylinders"))


(defun time-to-ti (time)
  "По времени ti ближайшей точки"
  (perga
    (_f mod time *period*)
    (let timeX2 (* time 2))
    (let limit (- (length *btS*) 2))
    (do-for (i 1 limit)
      (let left [ *btS* (- i 1) ])
      (let this [ *btS* i ])
      (let next [ *btS* (+ i 1) ])
      (when (<= (+ left this) timeX2 (+ this next))
        (return-from time-to-ti i))
      )
    (cond
     ((< time [ *btS* 1 ]) 0)
     (t (+ limit 1)))))



(defun MakeLossPressureFn (CellIndex power-p)
  "Возвращает ф-ю, к-рая по индексу времени возвращает: power-p = t, то мощность, если
  nil, то давление"
  (perga
    (cond
     ((member CellIndex (list +e +b))
      (constantly 0))
     (t
      (:lett hx hx:HxBase
       (cond
        ((= CellIndex +c)
          ; в холодильник условно поместим потери в холодном трубопроводе 
         (hx:CalcHeatExchanger (hx:MAKE-Pipe :Length *CoolerDuctLength* :PipeDiameter *CoolerDuctDiameter*)))
        (t         
         (Hxi CellIndex))))
      (lambda (tindex)
        (perga
          (:lett str WorkingFluidStreamType
           (MakeTempStreamAtTimeAndHx tindex CellIndex))
          (let pressure (HX-AND-STR:hxPressureDrop hx str))
          (let power (* pressure str^VolumetricFlow))
          (if power-p
              power
            pressure)))))))


(defun FlowFrictionAverageLossPowerOrPressure (CellIndex power-p)
  (perga
    (:lett lp function (MakeLossPressureFn CellIndex power-p))
    (flet subInt (tindex)
      (funcall lp tindex))
    (/ (IntegrateByCycle #'subInt)
       *period*)))


(defun FlowFrictionTotalAveragePressure ()
  (SumOfSlotValueInVector
   *StatFlowFriction*
   'AveragePressure))

(defun StatFlowFriction ()
  "AKA workSim"
  (perga
    (setf *StatFlowFriction*
          (make-array `(,*n*)
                      :element-type
                      '(or StatFlowFrictionRec null)
                      :initial-element nil))
    (:lett TotalFrictionLossPower dbl 0.0)
    (dotimes (ci *n*)
      (:lett lpo number (FlowFrictionAverageLossPowerOrPressure ci t))
      (_f + TotalFrictionLossPower lpo)
      (:lett lpr number (FlowFrictionAverageLossPowerOrPressure ci nil))
      (setf [ *StatFlowFriction* ci ]
            (MAKE-StatFlowFrictionRec
             :Power
             lpo
             :AveragePressure
             lpr))
      )
     
    (values
     TotalFrictionLossPower
     *StatFlowFriction*)))

(defun maxovikMass ()
  (perga
    (let EnergyPerKg
      (*
       0.5
       (expt
        (* 
         *omega* 
         *genDiam*)
        2)))
    (* 
     (- 4) ; запас, чтоб не остановился
     (getf (ExpAndCompPList 'dVolumeForMaxovik) :comp)
     (/ EnergyPerKg))))


(defun WorkabilityRatioByMechLossGamma ()
  (perga
    (let ExpAndCompDispRod
      (ExpAndCompPList 'dVdispRod))
    (let ExpAndCompPowerCylinder
      (ExpAndCompPList 'dVC1))
    (let MechEff 0.75) ; закладываемся на такой КПД в каждой отдельной полумашине.
    (let TotalExpansion
      (+ (getf ExpAndCompDispRod :exp)
         (getf ExpAndCompPowerCylinder :exp)))
    (let TotalCompression
      (+ (getf ExpAndCompDispRod :comp)
         (getf ExpAndCompPowerCylinder :comp)))
    (let TotalWork (+ TotalExpansion TotalCompression))
    (let TotalWorkOr1 (if (= 0 TotalWork) 1.0 TotalWork))
    (/ (+
        (* TotalExpansion MechEff)
        (/ TotalCompression MechEff)
        )
       TotalWorkOr1
       )
    ))
      
(defun StatGamma ()
  "Выводит данные по гамме"
  (perga
    (:lett dispRodPower dbl
     (* *freq*
        (IntegrateByCycle
         (lambda (i)
           (* [ *pS* i +e ] (dVdispRod [ *btS* i ]))))))
    (show-expr-t dispRodPower)
    ;(:lett pmean dbl (pmean))
    (:lett workPower dbl
     (* *freq* 
        (IntegrateByCycle
         (lambda (i)
           (* [ *pS* i +c ] (dVC1 [ *btS* i ]))))))
    (show-expr-t workPower)
    (let Comp/exp-work-on-power-cylinder 
      (multiple-value-list (expDivComp 'dVC1)))
    (show-expr-t Comp/exp-work-on-power-cylinder)
    (let Comp/exp-work-on-disp-rod
      (multiple-value-list (expDivComp 'dVdispRod)))
    (show-expr-t Comp/exp-work-on-disp-rod)
    (let |Comp/exp-work|
      (multiple-value-list (expDivComp 'dVolumeForMaxovik)))
    (show-expr-t Comp/exp-work)
    (show-expr-t (WorkabilityRatioByMechLossGamma))

    (show-expr-t
     (maxovikMass)
     )
    (values)))


(defun BetaPureVC (ti)
  "Функция для проверки того, что рабочий поршень и вытеснитель не ударяются друг о друга"
  (perga
    (let lowArea (- (CircleArea *dBore*)
                    (CircleArea *dRodBore*)))
    (+
     (SinCylinder ti 0.0
                  (CylinderSweptVolume *wBore* *wStroke*) 0) ; VC1
     (SinCylinder ti 0.0
                  (* *dStroke* lowArea) (- (PistonPhaseShiftRadians) pi))
     (- (* (CircleArea *dBore*) *BetaCylinderShift*))
     )))

(defun StatBeta ()
  "Выводит данные по бета"
  (perga
    (dotimes (i (length *btS*))
      (assert (>= (BetaPureVC [ *btS* i ]) 0)))
    (:lett dispRodPower dbl
     (* *freq*
        (IntegrateByCycle
         (lambda (i)
           (* [ *pS* i +e ] (dVdispRod [ *btS* i ]))))))
    (show-expr-t dispRodPower)
    ;(:lett pmean dbl (pmean))
    (:lett workPower dbl
     (* *freq* 
        (IntegrateByCycle
         (lambda (i)
           (* [ *pS* i +c ] (dVC1 [ *btS* i ]))))))
    (show-expr-t workPower)
    (let Comp/exp-work-on-power-cylinder 
      (multiple-value-list (expDivComp 'dVC1)))
    (DONT-show-expr-t Comp/exp-work-on-power-cylinder)
    (let Comp/exp-work-on-disp-rod
      (multiple-value-list (expDivComp 'dVdispRod)))
    (DONT-show-expr-t Comp/exp-work-on-disp-rod)
    (let |Comp/exp-work|
      (multiple-value-list (expDivComp 'dVolumeForMaxovik)))
    (show-expr-t Comp/exp-work)
    (DONT-show-expr-t
     (maxovikMass)
     )
    (values)))


(defun StatAlpha ()
  (perga
    (format t "~%{StatAlpha ")
    (:lett ExpansionPistonPower dbl
     (* *freq*
        (IntegrateByCycle
         (lambda (i)
           (* [ *pS* i +e ] (dVE [ *btS* i ]))))))
    (show-expr-t ExpansionPistonPower)
    ;(:lett pmean dbl (pmean))
    (:lett CompressionPistonPower dbl
     (* *freq* 
        (IntegrateByCycle
         (lambda (i)
           (* [ *pS* i +c ] (dVC [ *btS* i ]))))))
    (show-expr-t CompressionPistonPower)
    (let Comp/exp-work-on-compression-piston 
      (multiple-value-list (expDivComp 'dVC)))
    (show-expr-t Comp/exp-work-on-compression-piston)
    (let Comp/exp-work-on-expansion-piston
      (multiple-value-list (expDivComp 'dVE)))
    (show-expr-t Comp/exp-work-on-expansion-piston)
    (let |Comp/exp-work|
      (multiple-value-list (expDivComp 'dVolumeForMaxovik)))
    (show-expr-t Comp/exp-work)
    (show-expr-t
     (maxovikMass)
     )
    (format t "StatAlpha}~%")
    (values)))


(defmacro lsx (expr)
  "Lisp show expression - return list of expression and its value. See also show-expr"
  (let ((e1 expr))
    (alexandria.0.dev:once-only (e1)
      ``(,',expr = ,,e1)
      )))

(defun-to-file StatHeatExchangers ()
               "Определяем, чтобы не было предупреждений. Будет переопределено в AdiabStat" (error "undefined yet"))

(defun-to-file StatInertia ()
               "Определяем, чтобы не было предупреждений. Будет переопределено в AdiabStat" (error "undefined yet"))

(defun-to-file StatShuttleLosses ()
               "Определяем, чтобы не было предупреждений. Будет переопределено в AdiabStat" (error "undefined yet"))


(defun-to-file VolumeForMaxovik (ti)
               "Определяем, чтобы не было предупреждений. Будет переопределено в AdiabStat" (declare (ignore ti)) (error "undefined yet"))

(defun-to-file dVolumeForMaxovik (ti)
               "Определяем, чтобы не было предупреждений. Будет переопределено в AdiabStat" (declare (ignore ti)) (error "undefined yet"))
               


(defun ExchCellVoidV (index)
  (^ (Hxi index) FluidVolume))


(defun CheckdVolumeForMaxovik (x dt)
  (values (/ (- (VolumeForMaxovik (+ x dt))
                (VolumeForMaxovik x)) dt)
          (dVolumeForMaxovik x)))


(defun CellAverageHeatExchangeEfficiencyWeightedByHeatFlow (ci)
  (/ (IntegrateByCycle
      (lambda (ti)
        (* [ *CellEpsilonS* ti ci ]
           (abs [ *dQS* ti ci ]))))
     (IntegrateByCycle
      (lambda (ti)
        (abs [ *dQS* ti ci ])))
     ))

(defun RegenCellEstimatedEnthalpyLossPower (ci)
  (/ (IntegrateByCycle
      (lambda (ti)
        (* (- 1 [ *CellEpsilonS* ti ci ])
           (max 0.0 [ *dQS* ti ci ]))))
     *period*
     ))

(defun StatRegen ()
  "Заполняет *StatRegen*"
  (perga
    (setf *StatRegen*
          (make-array `(,*n*)
                      :element-type
                      '(or StatRegenRec null)
                      :initial-element nil))
      


    (dolist (ci (list +r1 +r2))
      (let EnthalpyLossPower (RegenCellEstimatedEnthalpyLossPower ci))
      (setf
       [ *StatRegen* ci ]
       (MAKE-StatRegenRec
        ;:regenReAvg 
        ;:St St
        ;:Ntu Ntu
        :regEffect (CellAverageHeatExchangeEfficiencyWeightedByHeatFlow ci)
        :regEnthalpyLoss EnthalpyLossPower
        :regEntropyCreation (* EnthalpyLossPower (^ [ *Cells* ci ] TWall))
        ))
      )
    ;(let res 
    ;  (SumOfSlotValueInVector *StatRegen* 'regEnthalpyLoss :allow-missing-object t))
    (values))
  )


(defun StatPressure ()
  "Максимальное и минимальное давление"
  (perga 
    (multiple-value-setq (*StatMinimumPressure* *StatMaximumPressure*) (MinAndMaxPressure))
    (setf *StatAveragePressure* (* 1/2 (+ *StatMinimumPressure* *StatMaximumPressure*)))
    (setf *StatPressureRatio* (/ *StatMaximumPressure* *StatMinimumPressure*))
    (lsx `(,*StatAveragePressure* ,*StatPressureRatio* ,*StatMinimumPressure* ,*StatMaximumPressure*)))
  )


(defun SumOfSlotValueInVector (vector slot-name &key allow-missing-object)
  (perga
    (:lett result dbl 0.0)
    (dovector (index value vector)
      (cond
       ((and (null value)
             (not allow-missing-object))
        (error "m:SumOfSlotValueInVector : empty value at ~S" index))
       ((null value)
        ; do nothing
        )
       (t
        (_f + result (slot-value value slot-name)))))
    result))
    
    

(defun CheckGasFrictionLossByDiffur ()
      "Проверяет, что потеря из-за уравнения не слишком велика"
      (perga
        (let byDiffur (PressureLossByDiffur))
        (let byFriction (FlowFrictionTotalAveragePressure))
        (let ratio (/ byDiffur byFriction))
        (when (> ratio 0.7)
          ; этот коэфт 0.7 установлен эмпирически. Снизил
          ; отношение до 0.28 путём одновременного увеличения *Xi1* и
          ; уменьшения *dmvr-to-mvr-099-time*, мощность изменилась
          ; примерно на 1%, КПД - менее чем на 1%, при этом шаг
          ; диффура стал почти вдвое мельче
          (warn "1189. Воображаемые потери давления из-за формы уравнения составляют ~S от потерь давления трением. Это может привести к заниженным данным по мощности и КПД. См. CheckGasFrictionLossByDiffur~%" ratio))))


(defun PrintUpdateCrankcaseInitialPressureFactorMessage ()
  (format *trace-output* "~%Update *CrankcaseInitialPressureFactor* to ~A~%"
          (/ [ *pS* (- *times* 1) +b ] [ *pS* (- *times* 1) +c ])))



(defun DefineVolumeForMaxovik ()
  (eval 
   `(defun-with-symbolic-derivative VolumeForMaxovik (ti)
                                    (dVolumeForMaxovik
                                     :documentation
                                     "суммарный переменный объём, определён в AdiabStat")
                                    ,(ecase *EngineType*
                                       ((beta gamma)
                                        '(+ (VC1 ti) (VdispRod ti)))
                                       (alpha
                                        '(+ (VC ti) (VE ti)))))))
  

(defun ПерепадТемпературРёберНагревателя (ci)
  "Считаем 'потерю температуры' во внутренних рёбрах нагревателя"
  (perga
    (:lett hx hx:FlatChannelsMatrix (^ [ *Cells* ci ] HeatExchanger))
    (let ВнешнийДиаметр *HeaterOuterDiameter*)
    (let ДлинаРазмещенияРёбер (CircleLength ВнешнийДиаметр))
    (let ЧислоРёбер (/ ДлинаРазмещенияРёбер (+ hx^GapThickness hx^RebroThickness)))
    (show-expr ЧислоРёбер)
    (let ВысотаРебра ([mm] 17))
    (let ОбщееСечениеРёбер (* ЧислоРёбер hx^RebroThickness hx^Length))
    (let КоэффТеплопроводности 40)
    (let Теплопроводность (* ОбщееСечениеРёбер КоэффТеплопроводности
                             (/ (* 0.5 ; т.к. в среднем только полпути теплу
                                   ВысотаРебра))))
    (let TotalHeatIn 633)
    (let ПерепадТемператур (/ TotalHeatIn Теплопроводность))
    ПерепадТемператур))
      


(defun StatWorkingMassRelativeAmplitude ()
  "Половина разница между минимальной и максимальной массой рабочего тела в контуре за цикл, отнесённая к среднему значению массы, к-рое вычисляется как полусумма мин.и макс.значения"
  (perga
    (let MassesPerTime
      (make-array *times* :initial-element 0.0))
    
    ; заполняем вектор массами за все моменты
    (dotimes (ti *times*)
      (dotimes (ci *n*)
        (unless (= ci +b)
          (_f + [ MassesPerTime ti ] [ *mS* ti ci ]))))

    (mlvl-bind (min max)
        (min-and-max-of-sequence MassesPerTime))

    (/ (- max min) (+ max min))))



(defun AdiabStat ()
  (perga

    (StatPressure)

  ; вынуждены определить попозже, поскольку в момент компиляции этого файла ф-ии
  ; VC1 ещё нет почему-то
    (DefineVolumeForMaxovik)

    (show-expr *CircleNumber*)
    
    (StatRegen)

    (terpri)

    (let wallLeakage
      (* (WallLeakageKoeff) (- *th* *tk*)))

    (show-expr-t (RegenMatrixHeatConductionLossPower))
    (let MaxMatrixHeatConductionLossPower
      (apply 'max (mapcar 'second (RegenMatrixHeatConductionLossPower))))

    (let TotalHeatIn
      (+ 
       (* *freq* (+ [ *qS* *times* +e ]
                    [ *qS* *times* +h ]
                    (abs [ *qS* *times* +r1 ])
                    (abs [ *qS* *times* +r2 ])))
       ; *StatRegen*^regEnthalpyLoss ; потом убрать
       wallLeakage
       (if *SimpleGapRegenerator* 0 (ShuttleLossesBudden))
       (if *SimpleGapRegenerator* 0 (AppendixPumpingLossBudden))
       MaxMatrixHeatConductionLossPower))

    (terpri)

    (format t "~%{ AdiabStat ~%")

    ;(let CoolerHeatPower
    ;  (* *freq* [ *qS* *times* +k ]))
    ;(show-expr CoolerHeatPower)
    ;(let HeatersHeatPower
    ;  (* *freq* [ *qS* *times* +h ]))
    ;(show-expr HeatersHeatPower)
    
    (show-expr-t *StatRegen*)
    (terpri *trace-output*)
    (let GasFrictionLoss (StatFlowFriction))
    (show-expr-t GasFrictionLoss)

    (let StatFlowFriction1 (map 'list 'StatFlowFrictionRec-Power (nth-value 1 (StatFlowFriction))))
    (show-expr-t StatFlowFriction1)

    (show-expr-t (CrankcasePumpingLossBuddenPower))

    (show-expr-t wallLeakage)
    
    (with-output-to-string (*standard-output*)
      (eval 
       `(defun-to-file StatShuttleLosses ()
                       "См. AdiabStat"
                       (values
                        ',(lsx (ShuttleLossesBudden))
                        ',(lsx (ShuttleLossesWalker))
                        ',(lsx (ShuttleLossesMartini))
                       ))))
       
    (unless *SimpleGapRegenerator*
      (show-expr-t (ShuttleLossesBudden))
      (show-expr-t (AppendixPumpingLossBudden)))
    
    (show-expr-t (RegenMatrixHeatConductionLossPower))
    (show-expr-t (PistonRingFrictionPower))
    (show-expr-t (CrossheadFrictionPower))
    (show-expr-t (BearingFrictionPower))

    (DONT-show-expr-t TotalHeatIn)
    (DONT-show-expr-t (/ (heaterExternalArea) [cm2]))

    ;(let heatInDensity (/ TotalHeatIn
    ;                      (heaterExternalArea)))

    ;(show-expr-t ([cm2] heatInDensity))
      

    ;(let radiationalHeatIn800
    ;  (m0:RadiationalHeatExchangeK (+ 273 800) 0.8 *th* 0.8))
    ;(show-expr-t (/ radiationalHeatIn800 [cm2]))

    (with-output-to-string (*standard-output*)
      (eval 
       `(defun-to-file StatInertia ()
                       "См. AdiabStat"
                       (values
                        ',(lsx (maxPistonSpeed))
                        ',(lsx (hotCapMass))
                        ',(lsx (dispInertiaForce))
                        ',(lsx (dispPressureForce))
                        ',(lsx (wPressureForce))
                        ))))
    
    (show-expr-t (StatInertia))

    (let powerAdiab (* *freq* (CycleWork)))
    ;(show-expr-t powerAdiab)

    (setf *StatPowerSimple*
      (- powerAdiab
         GasFrictionLoss
         (PistonRingFrictionPower)
         (CrossheadFrictionPower)
         (BearingFrictionPower)))

    (terpri)
    
    (ecase *EngineType*
      (gamma (StatGamma))
      (beta (StatBeta))
      (alpha (StatAlpha))
      )

    (show-expr-t (/ (vSwept +e) [cm3]))
    (show-expr-t (/ (vSwept +c) [cm3]))
    (dotimes (i *n*)
      (format t "~&Вредное пр-во в полости ~A, см3 = ~A~%" i (/ (vClearance i) [cm3])))

    (show-expr-t (phaseAngleForMatlab))

    (with-output-to-string (*standard-output*)
      (eval 
       `(defun-to-file StatHeatExchangers ()
                       "См. AdiabStat"
                       (values
                        ',(lsx (list (/ (ExchCellVoidV +h) [cm3])
                                     (/ (ExchCellVoidV +r1) [cm3])
                                     (/ (ExchCellVoidV +r2) [cm3])
                                     (/ (ExchCellVoidV +k) [cm3])
                                     ))
                        ',(lsx (Hxi +h))
                        ',(lsx (Hxi +r1))
                        ',(lsx (Hxi +r2))
                        ',(lsx (Hxi +k))
                       ))))
       
    (DONT-show-expr-t
     (StatHeatExchangers)
     )

    (show-expr (/ *StatAveragePressure* [Bar])) 
    (show-expr-t (/ *StatMaximumPressure* *StatMinimumPressure*))

    (terpri *trace-output*)
    (show-expr *StatPowerSimple*)
    ; (show-expr-t (carnot *th* *tk*))

    (StatGrossEfficiency) 

    (show-expr-t TotalHeatIn)

    (setf *NetEfficiency*
      (/ *StatPowerSimple*
         TotalHeatIn))

    (show-expr *NetEfficiency*)
    (show-expr (^ [ *Cells* +r1 ] LeftOutT))
    (show-expr (^ [ *Cells* +r2 ] RightOutT))
    (let Carnot (carnot *th* *tk*))

    (show-expr-t (/ *NetEfficiency* Carnot))

    (CheckGasFrictionLossByDiffur)

    (PrintUpdateCrankcaseInitialPressureFactorMessage)
    (show-expr-t (StatWorkingMassRelativeAmplitude))

    (format t "~% AdiabStat }~%")
        
   ))


;;; Теплонагруженность

;  Для справки . Теплонагруженность 
;   у машины Dundee Foundry  -  3  Вт/см^2 .  
;            Ross35cc        -  5.9
;            Ross 65  CC     -  7.2 
;            Philips 102  C  -  4 - 11.  
;            GM GPU          - 40 



(defun heaterExternalArea ()
  "Более-менее от фонаря взяли из конструктивных соображений"
  (* (CircleLength *HeaterOuterDiameter*)
     (^ (Hxi +h) Length)))


;;; Инерция

(defun hotCapMass ()
  (* *hotCapDensity*
     (+
      (* (CircleLength *dBore*)
         (+ *DisplacerShuttleLength* *dStroke*))
      (* (CircleArea *dBore*)
         2.0))
     (dispWallThickness)
     4.0))
               

(defun maxPistonSpeed ()
  "Так себе способ, годится только для синусоиды"
  (* 0.5 (max *dStroke* *wStroke*) *omega*))


(defun maxDisplacerAcceleration ()
  "Годится только для синусоиды"
  (* *dStroke* 0.5 (expt *omega* 2)))

(defun dispInertiaForce ()
  (* (maxDisplacerAcceleration)
     (hotCapMass)))


;;; Поршневые кольца
(defun SpeedOfWorkingPiston (ti)
  (/ (dVC1 (the* dbl ti)) (CircleArea *wBore*))
  )

(defun SpeedOfDisplacer (ti)
  (/ (dVE (the* dbl ti)) (CircleArea *dBore*)))


(defun PistonRingFrictionWorkSubIntFn (SpeedFn RingDiameter &key ZeroPressure)
  "SpeedFn - функция, принимающая время. Вовзращает подинтегральную функцию, принимающую tindex и возвращающую мощность силы трения в этом кольце. Если ZeroPressure, то считаем перепад давлений равным нулю (для вытеснителя)"
  (lambda (tindex)
    (perga
      (let P1
        (cond
         (ZeroPressure 0.0)
         (t (abs (- [ *pS* tindex +c ] *StatAveragePressure*)))))
      (let P (+ (* 0.5 P1) *PistonRingInitialPressure*))
      (let speed (abs (funcall SpeedFn [ *btS* tindex ])))
      (let A
        (* (CircleLength RingDiameter)
           *PistonRingHeight*))
      (* P A speed *PistonRingFrictionCoefficient*))
    ))


(defun PistonRingFrictionPower ()
  "Мощность силы трения поршневых колец"
  (perga
    (let PistonRingFrictionWorkWorkingCylinder
      (IntegrateByCycle (PistonRingFrictionWorkSubIntFn 'SpeedOfWorkingPiston *wBore*)))
    (let PistonRingFrictionWorkDisplacer
      (IntegrateByCycle
       (PistonRingFrictionWorkSubIntFn 'SpeedOfWorkingPiston *dBore*
                                 :ZeroPressure (not (eq *EngineType* 'alpha)))))
    (let PistonRingFrictionWorkDisplacerRod
      (IntegrateByCycle (PistonRingFrictionWorkSubIntFn 'SpeedOfWorkingPiston *dRodBore*)))
    (*
     *freq*
     (+ PistonRingFrictionWorkWorkingCylinder
        PistonRingFrictionWorkDisplacer
        PistonRingFrictionWorkDisplacerRod
        ))))
       

;;;; Ползуны 


(defun WorkingPistonCrossheadFrictionWorkSubIntFn ()
  "Вовзращает подинтегральную функцию, принимающую tindex и возвращающую мощность силы трения в ползуна рабочего поршня."
  (lambda (tindex)
    (perga
      (let P1 (abs (- [ *pS* tindex +c ] *StatAveragePressure*)))
      (let ti [ *btS* tindex ])
      (let angle (WorkingPistonConrodAngle ti))
      (abs
       (* P1 (CircleArea *wBore*)
          (SpeedOfWorkingPiston ti)
          (sin angle)
          *CrossheadFrictionCoefficient*))
    )))


(defun DisplacerCrossheadFrictionWorkSubIntFn ()
  "То же, что и WorkingPistonCrossheadFrictionWorkSubIntFn, но для вытеснителя"
  (lambda (tindex)
    (perga
      (let P1 (abs (- [ *pS* tindex +c ] *StatAveragePressure*)))
      (let ti [ *btS* tindex ])
      (let angle (DisplacerConrodAngle ti))
      (abs
       (* P1 (CircleArea
              (ecase *EngineType*
                (gamma *dRodBore*)
                (beta *dRodBore*)
                (alpha *dBore*)))
          (SpeedOfDisplacer ti)
          (sin angle)
          *CrossheadFrictionCoefficient*))
    )))

;; Так можно посмотреть график:
;; (plot-adw *btS* (mjr_mat_cv2m (map 'vector (DisplacerCrossheadFrictionWorkSubIntFn) (integer-range 0 (- (length *btS*) 1)))))


(defun CrossheadFrictionPower ()
  "Мощность силы трения ползунов"
  (perga
    (let WorkingPistonWork
      (IntegrateByCycle (WorkingPistonCrossheadFrictionWorkSubIntFn)))
    (let DisplacerWork
      (IntegrateByCycle (DisplacerCrossheadFrictionWorkSubIntFn)))
    (*
     *freq*
     (+ WorkingPistonWork
        DisplacerWork))))


;;; 
(defun BearingFrictionPower ()
  "Мощность трения подшипников. Пренебрегаем трением в пальце шатуна, остальные 4 подшипника считаем одинаковыми. Здесь есть ещё и проверка подшипника на грузоподъёмность при заданном моторесурсе. См. также JOURNAL_BEARING::f454353"
  (perga
    (let Cs 1.5
    ; D:\energy\stirling\oil\качение\vybor_razmera_podshipnika.pdf, табл. 10 - при ударных нагрузках и обычных требованиях к уровню шума 
      )
    (let Cr *RollerBearingCr* ; динамич. нагрузка, http://www.tehburo.ru/index.php?id=255&Itemid=99999999&option=com_content&task=view 
      )
    (let Cor ; статич, там же
      *RollerBearingCor*)
    (let Нагрузка
      (*
       (CircleArea *wBore*)
       (- *StatMaximumPressure* *StatAveragePressure*)
       )
      )
    (let ПроходимПоЗапасуСтатическойНагрузки
      (/ Cor (* Cs Нагрузка)))
    (when (<= ПроходимПоЗапасуСтатическойНагрузки 1.0)
      (warn "В подшипниках не хватает запаса статической нагрузки: ~A" ПроходимПоЗапасуСтатическойНагрузки))
    (let Моторесурс 2000 #|часов|#
      )
    (let НужноеЧислоОборотов
      (* Моторесурс 3600 *freq*))
    (let L (* (expt (/ Cr Нагрузка) 3))
    ; долговечность, млн. оборотов
      )
    (let ПроходимПоЗапасуДинамическойНагрузки
    ; Орлов П.И.Основы конструирования т.2.1988
    ;  "D:\gw\1\Орлов П.И. Основы конструирования\Орлов П.И.Основы конструирования.т2.1988.djvu"
      (/ (* L 1e6) НужноеЧислоОборотов))
    (when (<= ПроходимПоЗапасуДинамическойНагрузки 1.0)
      (warn "В подшипниках не хватает запаса динамической нагрузки: ~A" ПроходимПоЗапасуДинамическойНагрузки))
    

    (let ВнутреннийДиаметр *RollerBearingInnerDiam*) ; диаметр отверстия, но мы перестрахуемся, т.к. он наверняка для больших подшипников

 
    (let МоментСопротивления1
      (* 0.5 Нагрузка *RollerBearingFnp* ВнутреннийДиаметр))

  ; Фролов К.В. Детали машин 1995
  ; расчёт составляющий момента трения от вязкости
    (let nu *LubricatingOilKinematicViscosity*) 
    (let numms (/ nu [mm] [mm]))
    (let n (* *freq* 60))
    (let Dpwmm *RollerBearingAvgDiam*) ; диаметр средней линии тел вращения подшипника в мм. 
    (let f0 1) ; шариковый радиальны, сферич, упорный
           ; масляный туман 0,7-1
           ; масланяа ванна или пластичная смазка 1,5-2
           ; масляная ванна (верт.вал) или циркуляционная смазка 3-4
    (let nuN (* numms n))
    (let МоментСопротивленияОтСмазки
      (cond
       ((<= nuN 2000)
        (* 1.55e-8 f0 (expt Dpwmm 3)))
       (t 
        (* 0.979e-10 f0 (expt nuN 2/3) (expt Dpwmm 3)))))
    ;(warn "МоментСопротивленияОтСмазки не проверен!")
    
    (let МощностьТрения
      (* (+ МоментСопротивления1 МоментСопротивленияОтСмазки)
         *freq* 2 pi))
    (let КоличествоПодшипников 4) 
    (* МощностьТрения КоличествоПодшипников)
    )
  )



(defun Pmax ()
  (nth-value 
   1 
   (FindExtremumInVector (extract-col-from-matrix *pS* +r1)
                         #'<=)))


(defun Pmin ()
  (nth-value
   1
   (FindExtremumInVector (extract-col-from-matrix *pS* +r1)
                         #'>=)))
       

(defun dispPressureForce ()
  (* 0.5 (- (Pmax) ([Bar] 1)) (CircleArea *dRodBore*)))


(defun wPressureForce ()
  (* (- (Pmax) (Pmin)) (CircleArea *wBore*)))


;;;; 


(defun phaseAngleForMatlab ()
  "Фазовый угол, если бы машина была альфой"
  (perga
    (let VCMaxI
      (FindExtremumInVector (extract-col-from-matrix *vS* +c)
                            #'<=))
    (let VEMaxI
      (FindExtremumInVector (extract-col-from-matrix *vS* +e)
                            #'<=))
    (float (* (- (mod VCMaxI *times*) (mod VEMaxI *times*)) 360 (/ *times*)))
    ))



(defun vClearance (cell-index)
  (perga
    (let Vs (extract-col-from-matrix *vS* cell-index))
    (reduce 'min Vs :initial-value [ Vs 0 ])))

(defun TotalDeadVolume ()
  "Используется только для вывода результатов расчёта"
  (iter
    (:for i :from 0 :to (- *n* 1))
    (:summing (vClearance i))))


(defun vSwept (cell-index)
  (perga
    (let Vs (extract-col-from-matrix *vS* cell-index))
    (let minn (vClearance cell-index))
    (let maxx (reduce 'max Vs :initial-value minn))
    (- maxx minn)))


(defun FindExtremumInVector (vector function)
  "Function can be max or min. Returns 
  (values position-of-extremum-value extremum-value)
   Use <= to find maximum,  >= to find minimum"
  (iter
    (:for i from 0)
    (:for x :in-vector vector)
    (:finding i :yielding-best-of x :by function :into z)
    (:finally (return (values z (elt vector z))))
    ))

    

(defun PressureLossByDiffur ()
  "Отношение максимального давления к минимальному, измеренных одновременно, кроме картера. Такой
перепад возникает из-за самого уравнения. Он должен в ~5 раз меньше перепада давления,
вызываемого трением, иначе будут неточные результаты"
  (* (pmean)
     (iter (:for i from 0 to *times*)
       (:for row = (subseq (extract-row-from-matrix *pS* i) 0 +b))
       (:for Max = (nth-value 1 (FindExtremumInVector row #'<=)))
       (:for Min = (nth-value 1 (FindExtremumInVector row #'>=)))
       (:maximizing (/ (- Max Min) Max)))))



(defun BealeNumber ()
  "Не выводится в/does not shown in AdiabStat"
  (/ *StatPowerSimple*
     (* *StatAveragePressure*
        (CylinderSweptVolume *wBore* *wStroke*)
        *freq*)
     ))