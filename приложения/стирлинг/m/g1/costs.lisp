; -*- Encoding: utf-8;  System :g1 -*-


(in-package :m)
(in-readtable :buddens-readtable-a)

(defun СтоимостьФольги ()
  (* 1/2
     (+ (^ (Hxi +r1) HeatExchangeArea) (^ (Hxi +r2) HeatExchangeArea))
     (^ (Hxi +r1) RebroThickness)
     7800
     3362))

(defun СтоимостьПрипоя ()
  (* (CircleLength *HeaterOuterDiameter*)
     (^ (Hxi +h) Length)
     ([mm] 1)
     9000 ; плотность пусть такая
     2825 ;  http://pripoev.ru/pripoy_pmc-36/
     ))

(defun СтоимостьБронзовойБолванки ()
  "Бронза плохо проводит тепло"
  (* (CircleArea *HeaterOuterDiameter*)
     (^ (Hxi +k) Length)
     7600 ; плотность в интернете нашёл
     512 ; http://mc.ru/page.asp/metalloprokat/krug_bronz
     ))

(defun СтоимостьЭрозионныхТеплообменников ()
  "Исходя из прикидок, 90тыр за кв.м"
  (perga
    (let ПлощадьГорячая
      (* 
       (^ (Hxi +h) HeatExchangeArea)
       2 ; внешний и внутренний
       ))
    (let ПлощадьХолодная
      (* 
       (^ (Hxi +k) HeatExchangeArea)
       2 ; внешний и внутренний
       ))
    (let ЦенаЗаМетр 90000)
    (* ЦенаЗаМетр (+ ПлощадьГорячая ПлощадьХолодная))
    ))
