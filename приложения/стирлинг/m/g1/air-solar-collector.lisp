; -*- Encoding: utf-8;  System :g1 -*-

(named-readtables:in-readtable :buddens-readtable-a)

(def-merge-packages::! :g1-external-heater
  (:always t)
  (:use :cl :physical-units :mjr_mat :mjr_vec :m0 :m
   :budden-adw-plotting)
  (:local-nicknames
   :str :fluid-stream :hx :heat-exchanger)
  (:import-from :perga-implementation
   perga-implementation:perga)
  (:import-from :budden-tools
   budden-tools:_f
   budden-tools:show-expr
   budden-tools:the*
   budden-tools:deftvar
   budden-tools:deftparameter
   BUDDEN-TOOLS:def-symbol-readmacro
   budden-tools:in-readtable
   budden-tools:str++
   budden-tools:|^|
   budden-tools:defun-to-file
   budden-tools:mandatory-slot
   budden-tools:byref
   budden-tools:with-byref-params
   BUDDEN-TOOLS:mlvl-list
   )
  (:custom-token-parsers budden-tools::convert-carat-to-^) 
  (:import-from :iterate-keywords
   iterate-keywords:iter)
  (:import-from :alexandria.0.dev
   ALEXANDRIA.0.DEV:copy-array
   alexandria.0.dev:eswitch)
  (:export
   "G1-EXTERNAL-HEATER::CalcExternalHeater")
  )

(in-package :g1-external-heater)


(deftparameter *ExternalStream* str:AirStream1
               :documentation "Поток воздуха, омывающего наружный нагреватель"
               :initial-value
               (str:CalcStream
                (str:MAKE-AirStream1
                 :Temperature ([Celsius] 0.0)
                 :Pressure [Bar] :VolumetricFlow 0.0750)))


; трубка 60х14, внутр. диам. 32
(defparameter *ДиаметрТрубки* ([mm] 33)) 
(defparameter *ВнутреннийДиаметрРёбер* ([mm] 38))
(defparameter *ДлинаТрубки* ([cm] 20))
(defparameter *ЧислоТрубок* 12)
(defparameter *ВнешТолщинаРебра* ([mm] 1))
(defparameter *ВнешШагРебра* ([mm] 3))
(defparameter *ВнешДлинаРебраПоТечению* ([mm] 36))
(defparameter *ЗазорВнутреннихПазов* ([mm] 1))
(defparameter *ВнешнДиаметрИсходнойТрубки* ([mm] 55)) ; делаем меньше, т.к. нужно сделать сектора

(defun ОбщаяДлинаТрубок () (* *ДлинаТрубки* *ЧислоТрубок*))

(show-expr (ОбщаяДлинаТрубок))


(deftparameter *ExternalHeater* hx:HxBase
               :initial-value
               (hx:CalcHeatExchanger
                (hx:MAKE-FlatChannel :Length 3.2 :Height ([cm] 1.5) :Width 5.0 :OneWallP t
                                     :XiEntryExit 1)
                ))
               


(deftparameter *ТрубаКТеплоаккумулятору* hx:HxBase
               :initial-value
               (hx:CalcHeatExchanger
                (hx:MAKE-Pipe
                 :Length ([mm] 200)
                 :PipeDiameter ([cm] 35)
                 :LengthNuFactorMM90 1)))


(deftparameter *КоллекторВнешнегоТеплообменника* hx:HxBase
               :initial-value
               (hx:CalcHeatExchanger
                (hx:MAKE-ParallelPipes
                 :NumPipes 12
                 :Length ([mm] 810)
                 :PipeDiameter ([mm] 150) ; на самом деле там квадрат 23х10
                 :LengthNuFactorMM90 1)))


(defun CalcExternalHeater ()
  (perga
    (WARN-ONCE:start-warnings-over)
    (format t "~%---------- Внешний нагреватель ------------~%")
    (let ПадениеДавленияВТрубеКТеплоаккумулятору
      (HX-AND-STR:hxPressureDrop *ТрубаКТеплоаккумулятору* *ExternalStream*))
    (show-expr ПадениеДавленияВТрубеКТеплоаккумулятору)
    (let ПадениеДавленияВКоллекторе
      (HX-AND-STR:hxPressureDrop *КоллекторВнешнегоТеплообменника* *ExternalStream*))
    (show-expr ПадениеДавленияВКоллекторе)
    (let ExtRe (HX-AND-STR:hxRey *ExternalHeater* *ExternalStream*))
    (show-expr ExtRe)
    (let ПадениеДавлениеВоВнешнемНагревателе
      (HX-AND-STR:hxPressureDrop *ExternalHeater* *ExternalStream*))
    (show-expr (HX-AND-STR:hxSkorostqVSechenii *ExternalHeater* *ExternalStream*))
    (show-expr ПадениеДавлениеВоВнешнемНагревателе)
    (let МощностьПрокачки
      (* (+ ПадениеДавлениеВоВнешнемНагревателе ПадениеДавленияВТрубеКТеплоаккумулятору ПадениеДавленияВКоллекторе)
         *ExternalStream*^VolumetricFlow))
    (show-expr МощностьПрокачки)
    (let Capacity
      (KAYS-LONDON-NOTATION:c *ExternalStream*))
    (let КПД
      (EPSILON-N_TU:CounterFlow
       (KAYS-LONDON-NOTATION:N_tu_zero *ExternalHeater* *ExternalStream*)
       0.0))
    (show-expr КПД)
    (let ТепловаяМощностьНаГрадус Capacity)
    (show-expr Capacity)
    (values)
    ))
               


(CalcExternalHeater)


;;; Оптимизируем внутренний и внешний нагреватель без прогона машины


;;; Потоки позаимствованы из расчитанной машины


(defparameter *AvgHeaterStream*
  ; (MakeAvgStreamAtHx +h)
  #S(WorkingFluidStreamType :VolumetricFlow 0.0377135439471171
                          :Pressure 84957.40356019524
                          :Temperature 603.5672324425382
                          :ThermalExpansionCoefficient 0.0016568162521897736
                          :nu 6.139344038035749E-5
                          :rho 0.49206297740758676
                          :lambda 0.04786460117899483
                          :Cp 1053.419118812933
                          :RGas 287.0
                          :GammaGas 1.4
                          :Cv 752.4422277235236))

(defparameter *MaxHeaterStream*
  ; (MakeTempStreamAtTimeAndHx 0 +h)
  #S(M::WorkingFluidStreamType :VolumetricFlow 0.057033033933883356
                               :Pressure 102439.59511791641
                               :Temperature 613.7530710249846
                               :ThermalExpansionCoefficient 0.001629319749602185
                               :nu 5.234641842845704E-5
                               :rho 0.5834710170051328
                               :lambda 0.04848593733252406
                               :Cp 1055.5581449152467
                               :RGas 287.0
                               :GammaGas 1.4
                               :Cv 753.9701035108906))


(defparameter *КоллекторВнутреннегоНагревателя*
  (hx:CalcHeatExchanger
   (hx:MAKE-ParallelPipes
       ; Интересует только сопротивление
    :Length (* 1/2 (- m::*RegenDomeDout* m::*dBore*)) ; от внешнего к внутреннему
    :XiEntryExit 2.0
    :PipeDiameter (* 0.5 *ДиаметрТрубки*)
    :NumPipes *ЧислоТрубок*
    :LengthNuFactorMM90 1.0
         ; львиная доля падения приходится на скорость
    )
        ;(hx:MAKE-FlatChannel ; переходный режим - не умеем.
        ; :Length (* 1/2 (- *RegenDomeDout* *dBore*)) ; от внешнего к внутреннему
        ;; :Height ([mm] 10) ; его подобрать 
        ; :Width (CircleLength *dBore*) ; на внутреннем
        ; :XiEntryExit 1.0
        ; :OneWallP t
        ; )
   ))


(defun ДлинаЗазораПоТечению ()
   (- (* (CircleLength (- *ДиаметрТрубки* *ЗазорВнутреннихПазов*)) 0.5) *ЗазорВнутреннихПазов*))

(defun ПлощадьСеченияТрубки ()
  (CircleArea *ДиаметрТрубки*))


(defparameter *ProbeHx*
  (hx:CalcHeatExchanger
   ; http://www.kkz.ru/orebrennie_trubi/401?type=object
   (hx:MAKE-FlatChannelsMatrix
    :Length (ДлинаЗазораПоТечению)
    :GapThickness *ЗазорВнутреннихПазов*
    :RebroThickness ([mm] 1)
    :FlowArea
    (*
     *ЗазорВнутреннихПазов* ; зазор
     (ОбщаяДлинаТрубок)
     2 ; вставка обходится с двух сторон
     )
    :XiEntryExit 1.0
    :OneWallP t
                               ; трубка 32x2 с н-образной вставкой - проход через вставку. Здесь нам важен теплообмен и чтобы давление было больше, чем на вход-выход.
    )))

(defparameter *ПодводВнутреннегоНагревателя*
  (hx:CalcHeatExchanger
   (hx:MAKE-ParallelPipes
       ; Интересует только сопротивление
    :Length *ДлинаТрубки*
                  ; в среднем воздух доходит только до
                  ; середины, но потом он попадает во вторую трубку, поэтому пусть так.                
    :XiEntryExit 2.0
    :PipeDiameter ; по площади прохода берём трубку с такой
                                        ; же площадью
         (* 2 (sqrt (/
                     (-
                      (* 0.5 (ПлощадьСеченияТрубки)) ; полкруга
                      (*
                       ([mm] 0.5) ;толщина материала половины Z
                       (+ *ДиаметрТрубки* (ДлинаЗазораПоТечению)))
                      (* *ЗазорВнутреннихПазов* ; зазор тоже не участвует
                         2
                         (ДлинаЗазораПоТечению)) 
                      ) 
                     pi)))
         :NumPipes *ЧислоТрубок*
         :LengthNuFactorMM90 1.0
                          ; львиная доля падения приходится на скорость
         )))
      
   #|(hx:MAKE-FlatChannelsMatrix :Length ([mm] 75)
                               :GapThickness ([mm] 41 1/5 0.99)
                               :RebroThickness ([mm] 1)
                               :FlowArea 0.02
                               :XiEntryExit 210
                               ) ; 0.25, 500, 1500 - нарочито плохой - получили 115Вт, 18.7% (37.6 от Карно) после 34 оборотов
   |#

   #|(hx:MAKE-FlatChannelsMatrix :Length ([mm] 40)
                               :GapThickness ([mm] 4)
                               :RebroThickness ([mm] 1)
                               :FlowArea 0.02
                               :XiEntryExit 85
                               ) ; 0.407, 204, 800 - чуть лучше, но тоже нарочито плохой - 
                                 ; получили 147вт, 22.67 (=0.456 от Карно) после 34 оборотов
   |#


   #|(hx:MAKE-ParallelPipes :Length ([mm] 14)
                          :XiEntryExit 1.0
                          :PipeDiameter ([mm] 1.7)
                          :NumPipes 2400) ; 0.54, 39, 76 . Сверлёная деталь из http://www.chipmaker.ru/topic/150921/page__view__findpost__p__2641149 
                                          ; площадь эрозируемой поверхности = 0.17м2 (должно получиться 9.37тыр за эрозию)
   |# 



   #|(hx:MAKE-ParallelPipes :Length ([mm] 2 45)
                          :XiEntryExit 2.0
                          :PipeDiameter ([mm] 4.5)
                          :LengthNuFactorMM90 1.0
                          :NumPipes 360) ; 0.57, 73, 515
                       |#
   #|(hx:MAKE-ParallelPipes :Length ([mm] 4 20)
                          :XiEntryExit 4.0
                          :PipeDiameter ([mm] 2.5)
                          :NumPipes (* 120 11)) ; 0.79, 109, 518 - подходит, но ещё теснее стало
    |#

   #|(hx:MAKE-FlatChannelsMatrix :Length (* 0.5 (CircleLength ([mm] 32)))
                               :GapThickness ([mm] 1)
                               :RebroThickness ([mm] 1)
                               :FlowArea (* [mm] 1 ; зазор 
                                            [mm] 8 ; высота - от R=12 до R=20
                                            25 ; штук в одном (удлинняем)
                                            12 ; штук по кругу поместится
                                            )
                               :XiEntryExit 1
                               ; 0.7, 602, 120, 0.24
                               ; за 34 оборота - 22.6%, 147 Вт. Не хватает 
                               ; учёта коллекторов нагревателя.
                               )|# 

   #|(hx:MAKE-FlatChannelsMatrix :Length ([mm] 80)
                               :GapThickness ([mm] 2.2)
                               :RebroThickness ([mm] 1.3)
                               :FlowArea (* [mm] 2.2 [mm] 20 180)
                               :XiEntryExit 1 
                               ) ; 0.75, 58, 633. Первая деталь с рёбрами из темы http://www.chipmaker.ru/topic/150921/. 32 тыр за эрозию за всё
                                 ; Площадь теплообмена 0.58 м2
   |#                          

   #|(hx:MAKE-ParallelPipes :Length ([cm] 12)
                          :XiEntryExit 1.0
                          :PipeDiameter ([mm] 4)
                          :LengthNuFactorMM90 1.0
                          :NumPipes 360) ; 0.6, 83, 542 - лучше, чем было. http://www.kkz.ru/orebrennie_trubi/401?type=object труба 6x1, наружный диам = 20.
    |#

   #| (hx:MAKE-ParallelPipes :Length ([cm] 20)
                          :XiEntryExit 2.0
                          :PipeDiameter ([mm] 8)
                          :LengthNuFactorMM90 1.0
                          :NumPipes 60
                          ; 0.53, 392, 402, 0.3 - оребрённые трубки 10мм с шагом 2мм
                          ) 
   |# 

   #|(hx:MAKE-ParallelPipes :Length ([cm] 20)
                          :XiEntryExit 2.0
                          :PipeDiameter ([mm] 11)
                          :LengthNuFactorMM90 1.0
                          :NumPipes 15
                          ; - оребрённые трубки 16x1.0 с шагом 2.8мм
                          )|#


; (* *ProbeHx*^NumPipes 2 (CircleArea ([mm] 20))) ; это круг диаметром 55см.

; 0.59, 121, 535 - это приемлемо

(defun CalcInternalHeater ()
  (perga
    ;(let *ProbeHx* (Hxi +h))
    (WARN-ONCE:start-warnings-over)
    (format t "~%-------- Внутренний нагреватель ----------------")
    (let fmt1 "~%~A~18t~A~28t~A~38t~A~48t~A")
    (let fmt2 "~%~A~18t~9,4G~28t~9,4G~38t~9,4G~48t~9,4G")
    (format t fmt1 "Показатель" "Поверхность" "Трубки" "Коллектор" "Итого")
    (let hxs (list *ProbeHx* *ПодводВнутреннегоНагревателя* *КоллекторВнутреннегоНагревателя*))
    (flet LineOfTable (Показатель fn)
      (let Results (mapcar fn hxs))
      (let Итого (apply #'+ Results))
      (apply 'format t fmt2 Показатель `(,@Results ,Итого))
      )
    (LineOfTable 'Re (lambda (hx) (hx-and-str:hxRey hx *MaxHeaterStream*)))
    (LineOfTable 'Effy (lambda (hx)
                       (EPSILON-N_TU:CounterFlow
                        (KAYS-LONDON-NOTATION:N_tu_zero hx *MaxHeaterStream*)
                        0.0)))
    (LineOfTable 'PressureDrop
                 (lambda (hx)
                   (HX-AND-STR:hxPressureDrop hx *MaxHeaterStream*)))
    (LineOfTable 'DeadVolumeCm
                 (lambda (hx)
                   (/ hx^FluidVolume [cm3])))
    (LineOfTable 'HxArea
                 (lambda (hx)
                   hx^HeatExchangeArea))
    (terpri)
    (terpri)
    ))





"
ВысотаРёберСДвухСторон = 0.01112388980384689

---------- Внешний нагреватель ------------
ПадениеДавленияВТрубеКТеплоаккумулятору = 1.3831450584330772
ПадениеДавленияВКоллекторе = 0.024642963260837044
ПадениеДавлениеВоВнешнемНагревателе = 4.276884269537708
МощностьПрокачки = 0.3410803374738973
КПД = 0.8219049763540208
ДельтаТ = 23.500611203720123

-------- Внутренний нагреватель ----------------
Показатель        Поверхность Трубки  Коллектор Итого
Re                290.5     5941.     3303.     9534.    
Warning: hxNuPipeOrAnnulus: 2300<=re<=5000
Warning: hxNuPipeOrAnnulus: Слишком короткая трубка при 2300<=re<=5000. Результат непредсказуемо неверен
Effy              .8763     .2550     4.4645E-2 1.176    
PressureDrop      132.3     283.1     22.65     438.1    
DeadVolumeCm      310.6     713.8     346.4     1371.    
HxArea            .6212     .1834     4.9480E-2 .8541"