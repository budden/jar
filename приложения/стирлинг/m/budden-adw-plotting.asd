; -*- Encoding: utf-8; -*-


(in-package :asdf)

(defsystem :budden-adw-plotting
  :description "Рисуем графики" 
  :serial t
  :depends-on (:iterate-keywords :budden-tools :perga :buddens-reader :sup-lm
               :adw-charting-vecto :m0)
  :components
  ((:file "plotting")
   ))
