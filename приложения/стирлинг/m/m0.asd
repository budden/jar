; -*- Encoding: utf-8; -*-


(in-package :asdf)

(defsystem :m0
  :description "Утилиты для работы с числами, матрицами, векторами" 
  :serial t
  :around-compile (lambda (thunk) (let ((cl:*read-default-float-format* 'cl:double-float)) (funcall thunk)))
  :depends-on (:iterate-keywords :budden-tools :perga
               :buddens-reader :sup-lm :alexandria
               )
  ;;; Когда при перекомпиляции cllib происходит ошибка,нужно сделать вручную require cllib ещё раз
  :components
  ((:file "physical-units.package")
   (:file "physical-units")
   (:file "m0.package")
   (:file "m0-types-vars-macros")
   (:file "m0-utils")
   (:file "m0-phys-mat")
   (:file "warn-once")
   (:file "buddens-symbolic-derivative")
   ))
