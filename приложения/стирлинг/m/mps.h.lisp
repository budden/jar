; -*- Encoding: utf-8; System :m -*-

(in-package :mps)
(in-readtable :buddens-readtable-a)

(deftype EngineTypes ()
  '(member alpha beta gamma))


(deftvar *Cells* vector :documentation "См. SetCells . Vector of CoCell structs, задаёт теплообменники")


