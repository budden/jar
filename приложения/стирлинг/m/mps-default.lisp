; -*- Encoding: utf-8; System :m -*-
;; m4
;; запуск/to run
;; (rc :filename "C:/stir/sw/m/es/m4/mps-m4.lisp" :no-of-circles 4)
(named-readtables:in-readtable :buddens-readtable-a)
(in-package :mps)

; Рабочее тело
(defstruct (WorkingFluidStreamType (:include str:HeliumStream)))

(defparameter *th* ([Celsius] 290))
(defparameter *tk* ([Celsius] 40))

(defparameter *InitialPressure* ([Bar] 8.4 1.2))
(defparameter *CrankcaseInitialPressureFactor* 0.81835)

(deftparameter *freq* dbl :initial-value (/ 50.0 3)
               :documentation "Частота вращения коленвала")


(deftparameter *times* fixnum :initial-value 240 :documentation "number of iterations. *times* * *tim* = total time")


(deftparameter *n* fixnum :documentation "number of cells" :initial-value 7)

(defparameter +e 0 "область расширения") 
(defparameter +h 1 "нагреватель")
(defparameter +r1 2 "регенератор 1")
(defparameter +r2 3 "регенератор 2")
(defparameter +k 4 "холодильник")
(defparameter +c 5 "область сжатия")
(defparameter +b 6 "пр-во картера")


;;; Нагреватель - подходит, например, для Ст3 при 350С. 
(defparameter *HotCapCreep* ([MPa] 100))

;;; Регенератор 
(defparameter *kwr* 25.0 "Коэфт теплоотдачи материала, на к-ром перепад т-р нагревателя")
; (defparameter *RegenDomeMinWallThickness* ([mm] 0.5) "Технологически заданная мин. толщина стенки корпуса регенератора")


(defparameter *RegenDomeMinWallThickness* ([mm] 0.7)
  "минимальная толщина стенки, задаваемая из технологических соображений. Потом где-то в другом месте считается истинная толщина по ползучести, далее берём максимум")
(defparameter *VysotaVnutrennegoRebra* ([mm] 2.5))
(defparameter *VysotaNaruzhnogoRebra* ([mm] 2.5))
(defparameter *TolshinaStenkiNagrevatelja* ([mm] 2))

(defparameter *DispMinWallThickness* ([mm] 0.7))
(defparameter *RegenDomeDIn* ([mm] 57))
; см. также *NoOfRegens* - оно вынесено выше, т.к. входит в определение вредного пр-ва
(defparameter *RegenDomeLength* ([mm] 70))

(deftparameter *DisplacerShuttleLength* dbl
               :documentation "Длина вытеснителя между изотермическими областями. Реальная длина вытеснителя больше этой величины на ход вытеснителя *dStroke* . См. также *dhotSideRadialClearance*"
               :initial-value *RegenDomeLength* ; ([mm] 35)
               )
; см. также *dBore*
(defparameter *HeaterOuterDiameter* ([mm] 100))


;;; Инерция

(defparameter *hotCapDensity* 7800.0)
(defparameter *genDiam* ([cm] 20.0))

;;; Контроль сходимости
(deftparameter *Xi1Factor* dbl
               :initial-value 0.7e-8
               :documentation "Эмпирический коэфт. Чем он больше, тем больше будет установившийся поток массы при данном перепаде давлений. См. SetEquationControls"
               )

(deftparameter *mvr-limit-factor* dbl
               :initial-value 3e-9
               :documentation "Эмпирический коэффициент. Чем он больше, тем больше возможное абсолютное значение *mvr*. Недопустим обрезание *mvr*, которое можно видеть на графике (do-pl :mvr). При этом также давления будут сильно отличаться друг от друга. См. SetEquationControls , *mvr-limit*")


(deftparameter *dmvr-limit-factor* dbl :initial-value 50.0 :documentation "эмпирический коэфт. Чем он больше, тем большие значения может принимать dmvr. Попытка урезать его обычно ни к чему хорошему не приводит. См. SetEquationControl, *dmvr-limit* ")


(deftparameter *dmvr-to-mvr-099-time-factor* dbl
               :initial-value 0.0035
               :documentation "Эмпирический фактор. За такую долю периода mvr должен приблизиться к dmvr на 0.99 их отличия")

(deftparameter *y-delta-abs-max-factor* dbl :initial-value 0.0025 :documentation "Макс. относит. приращение для решателя ODE. См. call-ode, y-delta-abs-fn")
(deftparameter *y-err-abs-max-factor* dbl :initial-value 2.5e-5 :documentation "Макс. отн. ошибка для решателя ODE. См. call-ode, y-delta-abs-fn")  



;; Геометрия машины
(deftparameter *EngineType* EngineTypes :initial-value 'gamma)
(defparameter *SimpleGapRegenerator* nil ; истина для машины с регенератором в виде щели зазора вытеснителя - пренебрегаем челночными потерями и накачкой зазора
  ) 

(defparameter *dBore* ([mm] 60.0))
(defparameter *dStroke* ([mm] 30.0))
(defparameter *dhotClearance* ([mm] 1))
(defparameter *dhotSideRadialClearance* ([mm] 1))
(defparameter *dcoldClearance* ([mm] 1))
(defparameter *dRodBore* ([mm] 30) "Диаметр штока. Для альфы - это шток вытеснителя.")

(defparameter *RegenDomeDout* (+ *dBore* (* 2 (+ *VysotaVnutrennegoRebra* *VysotaNaruzhnogoRebra* *TolshinaStenkiNagrevatelja*))))

;(defparameter *required-phase* ([gradusov] 125) "Какой хотим сдвиг между полостями. Имеет смысл только для беты. См. m:OptimumPhaseForBeta")

(defparameter *phase* 90.0 "Угол между поршнями (в градусах)")


(defparameter *wBore*
  ;*dRodBore*
  ;*dBore*
  (sqrt (* 4 (/ pi) (- (CircleArea *dBore*) (CircleArea *dRodBore*)))) ; вариант для гаммы двойного действия
  )
(defparameter *wStroke* *dStroke*)
(defparameter *wTopClearance* ([mm] 1))

(defparameter *BetaCylinderShift* ([mm] 2) "на сколько в бета машине вытеснитель вдвинут в цилиндр поршня")

(defparameter *HotCollectorVolume* ([cm3] 15.0)
  "Этот объём считается находящимся в области расширения, хотя он наполовину
находится в нагревателе"
  )

(defparameter *NoOfRegens* 1 "Канал от каждого регенератора с *CoolerDuctDiameter* и *CoolerDuctLength*")
(defparameter *CoolerDuctDiameter* ([cm] 1.0) "Неведомо что это, но вероятно это входит во вредное пр-во")
(defparameter *CoolerDuctLength* ([cm] 10))

(defparameter *ColdCollectorVolume* ([cm3] 11.0)) #|

 По Уолкеру должно быть 79 сс вредного пр-ва, а по Россу набирается только 
 (perga (let res 0.0)(do-for (i 1 4)
         (_f + res (^ (Hxi i) FluidVolume))) res)
 =3.165e-5

 Поэтому разницу делим поровну между областями сжатия и расширения, заодно
 будет лучше сходиться. 

 |#


; см. также *RegenDomeDout*

(defparameter *CrankcaseMaximumVolume* ([cm3] 1400))
(defparameter *PistonSealHoleDiameter* ([mm] 0.7))


(defparameter *LubricatingOilKinematicViscosity* 200e-6)

(defparameter *RollerBearingCr* 2500)
(defparameter *RollerBearingCor* 620)
(defparameter *RollerBearingInnerDiam* ([mm] 9))
(defparameter *RollerBearingAvgDiam* ([mm] (+ 19 6) 1/2))


(defparameter *RollerBearingFnp* 0.02 " Момент сопротивления можно приближённо определять, используя условное понятие о приведённом безразмерном коэффициентом трения fnp: M = 0,5P x fnp x d, где Р — нагрузка на подшипник; d — диаметр отверстия в подшипнике.

  Величина fnp = 0,0015—0,02 (меньшие значения принимают для шарикоподшипников, работающих при радиальных нагрузках и жидкой смазке).

  http://dic.academic.ru/dic.nsf/bse/121712/%D0%9F%D0%BE%D0%B4%D1%88%D0%B8%D0%BF%D0%BD%D0%B8%D0%BA

  ")

   

(defparameter *PistonRingHeight* ([mm] 2.5 2)) 
(defparameter *PistonRingFrictionCoefficient* 0.21) 
   ; без смазки чугун по чугуну 0.1-0.21 http://www.dpva.info/Guide/GuidePhysics/Frication/SlidingFriction/
   ; 0.165 максимум для бронза
   ; для текстолита - при плохой смазке 0.22, при хорошей якобы 0.02-0.06. 
(defparameter *PistonRingInitialPressure* ([Bar] 0.1))
(defparameter *CrossheadFrictionCoefficient* 0.12) ; коэфт трения ползуна (считаем граничную смазку)

#|
Определяем пористость: (^ (hx:CalcHeatExchanger (hx:MAKE-SetkaRodsKaysLondon :Length 1.0 :FrontalArea 1.0 :SetkaWireDiam 0.1 :SetkaCellSize 0.26)) Porosity)

 Сетка 016  - зазор 0,16, диам 0,1. шаг 0,26 - пористость 0.698
 Сетка 018  - зазор 0,18, диам 0,12, шаг 0,3 - пористость 0.686
 Сетка 02   - зазор 0,20, диам 0,12, шаг 0,32 - пористость 0.705
 Сетка 0212 - зазор 0,212, диам 0,12, шаг 0,332 - пористость 0.716 - правда, в 1.5 раз дороже
 Сетка 025 - зазор 0,25, диам 0,12, шаг 0.37 - пористость 0.745 
 Сетка 028 - зазор 0,28, диам 0,14, шаг 0.42 - пористость 0.738
 Сетка 0355 - зазор 0,355, диам 0,16, шаг 0.515 - пористость 0.646
 Сетка 04 -  зазор 0,4 диам 0,16, шаг 0.56 - пористость 0.775
 Сетка 05 -  зазор 0,5 диам 0,2, шаг 0,7 - пористость 0.776


 Сетка 341 - зазор 0,261, диам 0,1 шаг 0,361 - пористость 0.782 


 |#

(defun half-m4-regen (SetkaZazor SetkaWireDiam)
  (perga
    (let result
      (hx:CalcHeatExchanger
       (hx:MAKE-SetkaRodsKaysLondon
        :FrontalArea (- (CircleArea *RegenDomeDout*) (CircleArea *RegenDomeDIn*))
        :Length (* 0.5 *RegenDomeLength*)
        :SetkaWireDiam SetkaWireDiam
        :SetkaLambda 115.0
        :SetkaCellSize (+ SetkaWireDiam SetkaZazor)
        )))
    result
    ))

(defun SetCells ()
  "Инициализирует ячейки "
  (perga
    ;(let РазряжениеРегенератора 1.7)
    (setf *Cells*
          (vector
           (MAKE-CoCell :TWall *th* :HeatInFn ; (MakeSimpleHeatInFn 600)
                        'NoHeatIn
                        ) ; +e
           (MAKE-CoCell :TWall *th* :HeatInFn 'HxHeatIn ; +h
                        :HeatExchanger
                        (hx:CalcHeatExchanger
                         (hx:MAKE-FlatChannelsMatrix
                          :Length ([mm] 37.6)
                          :GapThickness ([mm] 0.6)
                          :RebroThickness ([mm] 0.3)
                          :FrontalArea (* (CircleLength *RegenDomeDout*) ([mm] 2.5))
                            ; :FlowArea (* [mm] 0.3 [mm] 2.5 180))
                          )))
           (MAKE-CoCell ; +r1
            :TWall (MeanEffectiveTemperature
                    *th*
                    (MeanEffectiveTemperature *th* *tk*)) 
            :HeatInFn 'HxHeatIn ; (MakeSimpleHeatInFn 30000.0)
            :OutTFn 'RegenOutT
            :LeftOutT *th* 
            :RightOutT (MeanEffectiveTemperature *th* *tk*)
            :HeatExchanger ; (half-m4-regen ([mm] 0.28) ([mm] 0.14))
            (half-m4-regen ([mm] 0.5) ([mm] 0.2))
            :RegenCellP t
            )
           (MAKE-CoCell ; +r2
            :TWall  (MeanEffectiveTemperature
                     (MeanEffectiveTemperature *th* *tk*)
                     *tk*)
            :HeatInFn 'HxHeatIn ; (MakeSimpleHeatInFn 30000.0)
            :OutTFn 'RegenOutT
            :LeftOutT (MeanEffectiveTemperature *th* *tk*) 
            :RightOutT *tk* 
            :HeatExchanger ; (half-m4-regen ([mm] 0.341) ([mm] 0.11)) сетка 341
            (half-m4-regen ([mm] 0.4) ([mm] 0.16))
            ; :HeatExchanger (half-m4-regen ([mm] 0.261) ([mm] 0.1))
            :RegenCellP t            
            )
           (MAKE-CoCell :TWall *tk* :HeatInFn 'HxHeatIn ; +k
                        :HeatExchanger
                        (hx:CalcHeatExchanger
                         (hx:MAKE-FlatChannelsMatrix
                          :Length ([mm] 50.0)
                          :GapThickness ([mm] 0.5)
                          :RebroThickness ([mm] 0.25)
                          :FrontalArea (* (CircleLength *RegenDomeDout*) ([mm] 2.5))
                            ; :FlowArea (* [mm] 0.3 [mm] 2.5 180))
                          ))
                        )
           (MAKE-CoCell :TWall *tk* :HeatInFn 'NoHeatIn ; (MakeSimpleHeatInFn 600)
                        )  ; +c
           (MAKE-CoCell :TWall *tk* :HeatInFn (MakeSimpleHeatInFn 400) ; +b
                        )
           ))))


(defun SetEngineVolumeLaws ()
  (funcall 'SetSinEngineVolumeLaws))
