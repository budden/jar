; -*- Encoding: utf-8; system  :m0 -*- 

(named-readtables:in-readtable :buddens-readtable-a)


(def-merge-packages::! :buddens-symbolic-derivative
                       (:always t)
                       (:use :cl)
                       (:import-from :budden-tools
                        BUDDEN-TOOLS:in-readtable)
                       (:import-from :perga-implementation
                        PERGA-IMPLEMENTATION:perga)
                       (:export "
  buddens-symbolic-derivative:defun-with-symbolic-derivative
  buddens-symbolic-derivative:assign-derivative
  buddens-symbolic-derivative:get-assigned-derivative
  "))

(in-package :buddens-symbolic-derivative)
                       

;;;;; budden vvvvvv
(defparameter *first-arg-derivatives* (make-hash-table :test 'eq)
  "Mapping of function object to its derivative function name. Derivative
 is supposed to be by first arg. All other args are assumed to be constants")


(defvar *x* (make-symbol "*X*")
  "Placeholder for differentiation variable, used somewhere")

(defun assign-derivative (function derivative-function)
  "Assigns a derivative function (by first arg) to function. Derivative is eiather a function name or a cons, in which case it is an expression of *x*. Pass nil to remove derivative"
  (check-type function (or symbol))
  (check-type derivative-function (or symbol cons))
  (cond
   (derivative-function
    (setf (gethash function *first-arg-derivatives*) derivative-function))
   (t
    (remhash function *first-arg-derivatives*)
    )))


(defmacro defun-with-symbolic-derivative (name lambda-list (derivative-name &key documentation) expr)
  "Define a function. Its derivative ON FIRST ARGUMENT ONLY is calculated and defined as a new function named 
  derivative-name. When using symbolic differentiation, derivative-name function is used if an expression 
  to differentiate contains 'name' function.   
  Beware that when you redefine 'name' function with simple defun, its derivative record is kept and it might
  give incorrect differentiation. To clear it, use either assign-derivative or just unintern symbol of function"
  (let*
      ((first-arg (first lambda-list))
       (d-code (sderiv expr first-arg)))
    (assign-derivative name derivative-name) ; it is made twice
    (identity ; print
     `(#-lispworks
       progn
       #+lispworks dspec:def
       #+lispworks (defun-with-symbolic-derivative ,name)
       #+lispworks (dspec:record-definition `(defun-with-symbolic-derivative ,',name) (dspec:location))       
       (budden-tools:defun-to-file ,name ,lambda-list
                                   ,@(when documentation `(,documentation))
                                   ,expr)
       (budden-tools:defun-to-file ,derivative-name ,lambda-list
                                   ,(format nil "Symbolic derivative of ~S" name)
                                   (declare (ignorable ,@lambda-list))
                                   ,d-code
                                   )
       (assign-derivative ',name ',derivative-name)
       (values ',name ',derivative-name)))))


#+lispworks (defun canonicalize-defun-with-symbolic-derivative-dspec (dspec)
  (and
   (consp dspec)
   (eq (first dspec) 'defun-with-symbolic-derivative)
   (typep (second dspec) 'symbol)
   `(defun-with-symbolic-derivative ,(second dspec))
   ))

#+lispworks (dspec:define-dspec-class defun-with-symbolic-derivative nil 
  "Toplevel progn locatable in a debugger session"
  :canonicalize #'canonicalize-defun-with-symbolic-derivative-dspec
  )

(assign-derivative 'sin 'cos)
(assign-derivative 'cos `(- (sin ,*x*)))

(defun get-assigned-derivative (function-name)
  (gethash function-name *first-arg-derivatives*))


;;;; budden ^^^^

(defun free-of (var expr)
  (etypecase expr
    (number t)
    (symbol
     (not (eql var expr)))
    (cons
     (and (free-of var (car expr))
          (free-of var (cdr expr))))))


(defvar *simplified-p*) ; t if we simplified something this time


(defun simplify-expt (expr)
  (assert (consp expr))
  (assert (eq (car expr) 'expt))
  (assert (= (length expr) 3))
  (let* ((f1 (simplify (second expr)))
         (f2 (simplify (third expr)))
         result)
    (setf
     result
     (cond
      ((and (eql f1 0) (eql f2 0))
       (error "(expt 0 0)"))
      ((and (eql f1 0) (numberp f2) (realp f2) (< f2 0))
       (error "(expt 0 ~S)" f2))
      ((and (eql f1 0) (numberp f2) (not (realp f2)))
       (error "(expt 0 ~S)" f2))
      ((eql f1 0)
       0) ; hopefully other cases were rejected already
      ((eql f2 1)
       f1)
      (t
       expr)))
    (unless (equalp result expr)
      (setf *simplified-p* t))
    result
    ))

(defun calc-expt-derivative (differentiate-fn expr)
  (assert (consp expr))
  (assert (eq (car expr) 'expt))
  (assert (= (length expr) 3))
  (let* ((f1 (simplify (second expr)))
         (f2 (simplify (third expr)))
         (df1 (funcall differentiate-fn f1))
         (df2 (funcall differentiate-fn f2))
         )
    (cond
     ((and (eql df1 0) (eql df2 0))
      0)
     ((eql df1 0)
      `(* ,expr (log ,f1) ,df2))
     ((eql df2 0)
      `(* ,f2 (expt ,f1 (- ,f2 1)) ,df1))
     (t ; general case 
      `(* (expt ,f1 ,f2)
          (+
           (/
            (* ,f2 ,df1)
            ,f1)
           (* 
            (log ,f1)
            ,df2)))))))


(defun calc-divide-derivative (differentiate-fn expr)
  (assert (consp expr))
  (assert (eq (car expr) '/))
  (let ((len (length expr))
        (a1 (second expr))
        (tail (cdr expr)))
    (assert (> len 1))
    (cond
     ((= len 1) (calc-divide-derivative differentiate-fn `(/ 1 ,a1)))
     (t   
      `(- (/ ,(funcall differentiate-fn a1)
             (* ,@(cdr tail)))
          ,@(mapcar (lambda (diff-term)
                      `(/ (* ,a1
                             ,(funcall differentiate-fn diff-term))
                          ,diff-term ,@(cdr tail)))
                    (cdr tail)))))))


(defun subst-var-in-expr (from to tree)
  (etypecase tree
    (null nil)
    (atom
     (if (eq tree from) to tree))
    (cons
     (cons (subst-var-in-expr from to (car tree))
           (subst-var-in-expr from to (cdr tree))))))


(defun deriv (exp var)
  (labels ((deriv-subexp (subexp)
             (deriv subexp var)))
    (cond ((numberp exp) 0)
          ((free-of var exp)
           0)
          ((variable-p exp)
           (if (same-variable-p exp var) 1 0))
          ((consp exp)
           (let* ((head (car exp))
                  (args (cdr exp)) 
                  (a1 (first args))
                  (kd (get-assigned-derivative head)))
             (etypecase kd
              (null ; no assigned derivative
               (ecase head
                 (+ `(+ ,@(mapcar #'deriv-subexp (cdr exp))))
                 (- `(- ,@(mapcar #'deriv-subexp (cdr exp))))
                 (* `(+ (* ,(multiplier exp)
                           ,(deriv-subexp (multiplicand exp)))
                        (* ,(deriv-subexp (multiplier exp))
                           ,(multiplicand exp))))
                 (expt (calc-expt-derivative #'deriv-subexp exp))
                 (/ (calc-divide-derivative #'deriv-subexp exp))
                 ))
              (symbol ; assigned function name
               (assert (free-of var (cdr args)))
               `(* ,(deriv-subexp a1) (,kd ,@args))
               )
              (cons ; assigned expr of *x*
               (assert (null (cdr args))) ; functions of one arg only for now
               `(* ,(deriv-subexp a1) ,(subst-var-in-expr *x* a1 kd))
               )
              )))
          (t (error "what's that?")))))


(defun nan-p (x)
  (declare (optimize (speed 3) (safety 0) (debug 0) (compilation-speed 0)))
  (let ((z (coerce x 'double-float)))
    (or (> z most-positive-double-float) ; +1D++0, + infinity
        (< z most-negative-double-float) ; -1D++0, - infinity
        (not (or (minusp x) ; "zero" 1D+-0
                 (zerop  x)
                 (plusp  x))))))


(defun simplify-one (expr)
  (etypecase expr
    (symbol expr)
    (number expr)
    (null expr)
    (cons
     (let* ((head (car expr))
            (ssubexprs (mapcar 'simplify-one (cdr expr)))
            (new-expr `(,head ,@ssubexprs)))
       (case head
         (* (simplify-binop new-expr))
         (+ (simplify-binop new-expr))
         (- (simplify-minus-or-divide new-expr))
         (/ (simplify-minus-or-divide new-expr))
         (expt (simplify-expt new-expr))
         (t new-expr))))
    ))
       

(defun simplify (expr)
  (loop 
   (let ((*simplified-p* nil))
     (setf expr (simplify-one expr))
     (unless *simplified-p*
       (return expr)))))

(defun sderiv (expr var)
  (simplify (deriv expr var)))


#|(defun simplify-mul (expr)
  (let ((head (car expr))
        (tail (cdr expr)))
    (assert (eq head '*))
    (let ((num 1) (non-nums nil) result)
      (dolist (se tail)
        (cond 
         ((numberp se)
          (setf num (* num se)))
         (t
          (push se non-nums))))
      (setf non-nums (nreverse non-nums))
      (setf result
            (cond
             ((nan-p num)
              (error "num is NAN while simplifying ~S" expr))
             ((zerop num)
              0)
             ((null non-nums)
              num)
             ((and (= num 1)
                   (= 1 (length non-nums)))
              (first non-nums))
             ((= num 1)
              `(* ,@non-nums))
             (t
              `(* ,num ,@non-nums))))
      (unless (equalp result expr)
        (setf *simplified-p* t))
      result)))|#


(defun simplify-minus-or-divide (expr)
  (let* ((head (car expr))
         (tail (cdr expr))
         (no-args (length tail))
         (a1 (simplify (first tail)))
         (more-args (mapcar 'simplify (cdr tail)))
         (a2 (first more-args))
         (neutral-num (ecase head (/ 1) (- 0)))
         result
         )
    (setf 
     result 
     (case no-args
       (0 (error "operation ~S is called without arguments" head))
       (1 (ecase head
            (- (typecase a1
                 (number
                  (- a1) ; hope that negation won't hurt precision
                  )
                 (t expr)))
            (/ (typecase a1
                 (rational
                  (/ a1)
                  )
                 (float
                  (/ a1))
                 (t
                  expr)))))
       (2
        (cond
         ((and (eq head '/) (eql a1 0))
          0)
         ((and (eq head '/) (equalp a1 a2))
          1)
         ((and (eq head '-) (equalp a1 a2))
          0)
         ((eql a2 neutral-num)
          a1)
         ((eql a1 neutral-num)
          `(,head ,a2))
         ((and (typep a1 'rational) (typep a2 'rational))
          (funcall head a1 a2))
         ((and (numberp a1) (numberp a2) (= a1 a2))
          neutral-num)
          ; we are careful simplifying floats as results
          ; are potentially inexact. So we don't simplify them at all
         (t
          expr)))
       (t ; more than two args
        (let ((anti-head
               (ecase head (/ '*) (- '+))))
          (simplify
           `(,head ,a1 (,anti-head ,@more-args)))))))
    (unless (equalp result expr)
      (setf *simplified-p* t))
    result))
          


(defun simplify-binop (expr)
  "Cover only trivial cases: multiplication by 1 or 0, addition of 0"
  (let ((head (car expr))
        (tail (cdr expr)))
    (assert (member head '(* +)))
    (let* ((neutral-num (ecase head (* 1) (+ 0)))
           (rational-part neutral-num)
           (essentials nil)
           result
           )
      (dolist (se tail)
        (cond
         ((and (eq head '*) (eql 0 se))
          (setf essentials nil
                rational-part 0)
          (return))         
         ((typep se 'rational)
          (setf rational-part (funcall head rational-part se))
          )
         (t
          (push se essentials))))
      (setf essentials (nreverse essentials))
      (setf result
            (cond
             ;((nan-p num)
             ; (error "num is NAN while simplifying ~S" expr))
             ((null essentials)
              rational-part)
             ((and (= 1 (length essentials))
                   (= neutral-num rational-part))
              (first essentials))
             ((= neutral-num rational-part)
              `(,head ,@essentials))
             (t
              `(,head ,rational-part ,@essentials))))
      (unless (equalp result expr)
        (setf *simplified-p* t))
      result)))
              



;; representing algebraic expressions

(defun variable-p (x) (symbolp x))

(defun same-variable-p (v1 v2)
  (and (variable-p v1) (variable-p v2) (eq v1 v2)))

;(defun make-sum (a1 a2) (list '+ a1 a2))

;(defun make-product (m1 m2) (list '* m1 m2))

(defun multiplier (p) (second p))

(defun multiplicand (p)
  (case (length p)
    (0 (error "wrong call to multiplicand"))
    (1 (error "wrong call to multiplicand"))
    (2 1)
    (3 (third p))
    (t `(* ,@(cddr p)))))
    



(def-trivial-test::! deriv.1
                     (sderiv '(expt x 2) 'x)
                     '(* 2 x))
                     
(def-trivial-test::! deriv.2
                     (sderiv '(/ 1 x) 'x)
                     '(- (/ (* x x))))

(def-trivial-test::! deriv.3
                     (sderiv '(sin (* x 2)) 'x)
                     '(* 2 (cos (* 2 x))))


(def-trivial-test::! deriv.4
                     (sderiv '(expt 4 (cos x)) 'x)
                     '(* (expt 4 (cos x)) (log 4) (- (sin x))))


(def-trivial-test::! deriv.5
                     (sderiv '(expt 4 (cos x)) 'x)
                     '(* (expt 4 (cos x)) (log 4) (- (sin x))))


(def-trivial-test::! deriv.6
                     (sderiv '(expt (cos x) 4) 'x)
                     '(* 4 (expt (cos x) 3) (- (sin x))))

(defun compare-from-1-to-10 (expr-of-x must-be &key (absolute-error 1e-10))
  (perga
   (let fn (eval `(lambda (x) ,expr-of-x)))
   (let must-be (eval `(lambda (x) ,must-be)))
   (do ((i 1 (+ i 1))) ((= i 10) t)
     (when (> (abs (- (funcall fn i)
                      (funcall must-be i)))
              absolute-error)
       (return nil)))))
   
   
(def-trivial-test::! deriv.7
                     (compare-from-1-to-10
                      (sderiv '(expt (cos x) (sin x)) 'x)
                      '(* (expt (cos x) (sin x))
                          (- (* (cos x) (log (cos x)))
                             (* (sin x) (tan x))))
                      :absolute-error 1e-6)
                     t)
                         
                         
(def-trivial-test::! deriv.8
                     (sderiv '(expt x x) 'x)
                     '(* (expt x x) (+ 1 (log x))))




(defun check-deriv (x fn dfn &key (delta 0.0000000001))
  (values (/ (- (funcall fn (+ x delta)) (funcall fn x)) delta) (funcall dfn x)))