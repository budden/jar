; -*- Encoding: utf-8; System :m -*-
;; m3
;; запуск/to run
;; (rc :filename "C:/stir/sw/m/es/m3/mps-m3.lisp" :no-of-circles 9)
(in-readtable :buddens-readtable-a)
(in-package :mps)

; Рабочее тело
(defstruct (WorkingFluidStreamType (:include str:HydrogenStream)))

(defparameter *th* ([Celsius] 280))
(defparameter *tk* ([Celsius] 60))
(defparameter *InitialPressure* ([Bar] 15 1.2))


(deftparameter *freq* dbl :initial-value 25.0
               :documentation "Частота вращения коленвала")


(deftparameter *times* fixnum :initial-value 240 :documentation "number of iterations. *times* * *tim* = total time")


(deftparameter *n* fixnum :documentation "number of cells" :initial-value 6)

(defparameter +e 0 "область расширения") 
(defparameter +h 1 "нагреватель")
(defparameter +r1 2 "регенератор 1")
(defparameter +r2 3 "регенератор 2")
(defparameter +k 4 "холодильник")
(defparameter +c 5 "область сжатия")


;;; Нагреватель - подходит, например, для Ст3 при 350С. 
(defparameter *HotCapCreep* ([MPa] 100))

;;; Регенератор 
(defparameter *kwr* 25.0 "Коэфт теплоотдачи материала, на к-ром перепад т-р нагревателя")
; (defparameter *RegenDomeMinWallThickness* ([mm] 0.5) "Технологически заданная мин. толщина стенки корпуса регенератора")


(defparameter *RegenDomeMinWallThickness* ([mm] 0.7)
  "минимальная толщина стенки, задаваемая из технологических соображений. Потом где-то в другом месте считается истинная толщина по ползучести, далее берём максимум")
(defparameter *RegenDomeDout* ([mm] 40))
(defparameter *DispMinWallThickness* ([mm] 1))

(defparameter *dhotSideRadialClearance* ([mm] 0.7))
(defparameter *dBore* ([cm] 3.2))
(defparameter *RegenDomeDIn* (+ *dBore* (* *dhotSideRadialClearance* 2) (* ([mm] 0.5) ; толщина стенки внутр. гильзы
                                                      2)))
; см. также *NoOfRegens* - оно вынесено выше, т.к. входит в определение вредного пр-ва
(defparameter *RegenDomeLength* ([mm] 70))

(deftparameter *DisplacerShuttleLength* dbl
               :documentation "Длина вытеснителя между изотермическими областями. Реальная длина вытеснителя больше этой величины на ход вытеснителя *dStroke* . См. также *dhotSideRadialClearance*"
               :initial-value *RegenDomeLength* ; ([mm] 35)
               )
; см. также *dBore*
(defparameter *HeaterOuterDiameter* ([mm] 45))


;;; Инерция

(defparameter *hotCapDensity* 7800.0)
(defparameter *genDiam* ([cm] 30.0))

;;; Контроль сходимости
(deftparameter *Xi1Factor* dbl
               :initial-value 0.7e-8
               :documentation "Эмпирический коэфт. Чем он больше, тем больше будет установившийся поток массы при данном перепаде давлений. См. SetEquationControls"
               )

(deftparameter *mvr-limit-factor* dbl
               :initial-value 3e-9
               :documentation "Эмпирический коэффициент. Чем он больше, тем больше возможное абсолютное значение *mvr*. Недопустим обрезание *mvr*, которое можно видеть на графике (do-pl :mvr). При этом также давления будут сильно отличаться друг от друга. См. SetEquationControls , *mvr-limit*")


(deftparameter *dmvr-limit-factor* dbl :initial-value 50.0 :documentation "эмпирический коэфт. Чем он больше, тем большие значения может принимать dmvr. Попытка урезать его обычно ни к чему хорошему не приводит. См. SetEquationControl, *dmvr-limit* ")


(deftparameter *dmvr-to-mvr-099-time-factor* dbl
               :initial-value 0.0035
               :documentation "Эмпирический фактор. За такую долю периода mvr должен приблизиться к dmvr на 0.99 их отличия")

(deftparameter *y-delta-abs-max-factor* dbl :initial-value 0.0025 :documentation "Макс. относит. приращение для решателя ODE. См. call-ode, y-delta-abs-fn")
(deftparameter *y-err-abs-max-factor* dbl :initial-value 2.5e-5 :documentation "Макс. отн. ошибка для решателя ODE. См. call-ode, y-delta-abs-fn")  



;; Геометрия машины
(deftparameter *EngineType* EngineTypes :initial-value 'gamma)

; см. *dBore* выше
(defparameter *dStroke* ([cm] 2.0))
(defparameter *dhotClearance* ([mm] 0.7))
; см. также *dhotSideRadialClearance*
(defparameter *dcoldClearance* ([mm] 0.7))
(defparameter *dRodBore* ([mm] 14) "Диаметр штока. Для альфы - это шток вытеснителя.")

(defparameter *phase* 90.0 "Угол между поршнями (в градусах)")

(defparameter *wBore* ([cm] 3.2))
(defparameter *CoolerInternalDiameter* ([mm] 40))
(defparameter *wStroke* *dStroke*)
(defparameter *wTopClearance* ([mm] 0.5))

;(defparameter *BetaCylinderShift* ([mm] 10) "на сколько в бета машине вытеснитель вдвинут в цилиндр поршня")

(defparameter *HotCollectorVolume* ([cm3] 7.0)
  "Этот объём считается находящимся в области расширения, хотя он наполовину
находится в нагревателе"
  )

(defparameter *NoOfRegens* 1 "Канал от каждого регенератора с *CoolerDuctDiameter* и *CoolerDuctLength*")
(defparameter *CoolerDuctDiameter* ([mm] 5) "Неведомо что это, но евроятно это входит во вредное пр-во")
(defparameter *CoolerDuctLength* ([cm] 10))

(defparameter *ColdCollectorVolume* ([cm3] (+ 6.5 17))) #|

 По Уолкеру должно быть 79 сс вредного пр-ва, а по Россу набирается только 
 (perga (let res 0.0)(do-for (i 1 4)
         (_f + res (^ (Hxi i) FluidVolume))) res)
 =3.165e-5

 Поэтому разницу делим поровну между областями сжатия и расширения, заодно
 будет лучше сходиться. 

 |#


; см. также *RegenDomeDout*

(defun half-m3-regen (SetkaWireDiam)
  (perga
    (let result
      (hx:CalcHeatExchanger
       (hx:MAKE-SetkaRodsKaysLondon-ByPorosity
        :FrontalArea (- (CircleArea *RegenDomeDout*)
                        (CircleArea *RegenDomeDIn*))
        :Length (* 0.5 *RegenDomeLength*)
        :SetkaWireDiam SetkaWireDiam
        :SetkaLambda 60.0 ; бронза, оценка сверху
        :Porosity 0.84
        )))
    result
    ))

#|(defun half-m3-regen (Distance)
  (perga
    (let result
      (hx:CalcHeatExchanger
       (hx:MAKE-FlatChannelsMatrix
        :FrontalArea (- (CircleArea *RegenDomeDout*) (CircleArea *RegenDomeDIn*))
        :Length (* 0.5 *RegenDomeLength*)
        :GapThickness Distance
        :RebroThickness ([mm] 0.02)
    ; :SetkaLambda 15.0
        )
       ))
    result
    ))|#

(defun SetCells ()
  "Инициализирует ячейки "
  (perga
    ;(let РазряжениеРегенератора 1.7)
    (setf *Cells*
          (vector
           (MAKE-CoCell :TWall *th* :HeatInFn ; (MakeSimpleHeatInFn 20)
                        'NoHeatIn
                        ) ; +e
           (MAKE-CoCell :TWall *th* :HeatInFn 'HxHeatIn ; +h
                        :HeatExchanger
                        (hx:CalcHeatExchanger
                         (hx:MAKE-FlatChannelsMatrix
                          :Length ([mm] 25)
                          :GapThickness ([mm] 0.3)
                          :RebroThickness ([mm] 0.1)
                          :FrontalArea (* (CircleLength *RegenDomeDout*) ([mm] 2.5))
                            ; :FlowArea (* [mm] 0.3 [mm] 2.5 180))
                          )))
           (MAKE-CoCell ; +r1
            :TWall (MeanEffectiveTemperature
                    *th*
                    (MeanEffectiveTemperature *th* *tk*)) 
            :HeatInFn 'HxHeatIn ; (MakeSimpleHeatInFn 30000.0)
            :OutTFn 'RegenOutT
            :LeftOutT *th* 
            :RightOutT (MeanEffectiveTemperature *th* *tk*)
            :HeatExchanger (half-m3-regen ([mm] 0.1))
            :RegenCellP t
            )
           (MAKE-CoCell ; +r2
            :TWall  (MeanEffectiveTemperature
                     (MeanEffectiveTemperature *th* *tk*)
                     *tk*)
            :HeatInFn 'HxHeatIn ; (MakeSimpleHeatInFn 30000.0)
            :OutTFn 'RegenOutT
            :LeftOutT (MeanEffectiveTemperature *th* *tk*) 
            :RightOutT *tk* 
            :HeatExchanger (half-m3-regen ([mm] 0.1))            
            :RegenCellP t            
            )
           (MAKE-CoCell :TWall *tk* :HeatInFn 'HxHeatIn ; +k
                        :HeatExchanger
                        (hx:CalcHeatExchanger
                         (hx:MAKE-FlatChannelsMatrix
                          :Length ([mm] 40.0)
                          :GapThickness ([mm] 0.3)
                          :RebroThickness ([mm] 0.15)
                          :FrontalArea (* (CircleLength *CoolerInternalDiameter*) ([mm] 2.5))
                            ; :FlowArea (* [mm] 0.3 [mm] 2.5 180))
                          ))
                        )
           (MAKE-CoCell :TWall *tk* :HeatInFn 'NoHeatIn ;(MakeSimpleHeatInFn 600)
                        )                 ; +c
           ))))


(defun SetEngineVolumeLaws ()
  (funcall 'SetSinEngineVolumeLaws))




(defun m3-regen-loss ()
  "Посчитал, что при условии неподвижности газа коэфт теплопроводности матрицы из проволок 1,1мм с шагом ячейки 5мм будет 3.3Вт/(м К). 
 Считаем, что при уменьшении коэфт теплопроводности не меняется, тогда общие потери теплопроводностью вернётся этой функцией. Коэфт посчситан для стали,
если возьмём вместо стали латунь, можно для простоты пересчитать по коэфтам теплопроводности самого материала"
  (perga
    (let КоэфтТеплопроводностиМатрицы 3.3)
    (let КоэфтТеплопроводностиСтали 40)
    (let КоэфтТеплопроводностиЛатуни 111)
    (* (- *th* *tk*) (^ (m::Hxi +r1) FrontalArea)
       (/ *RegenDomeLength*)
       КоэфтТеплопроводностиМатрицы
       (/ КоэфтТеплопроводностиЛатуни КоэфтТеплопроводностиСтали)
       )))