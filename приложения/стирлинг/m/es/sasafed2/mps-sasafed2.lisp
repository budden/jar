; -*- Encoding: utf-8; -*-
;; sasafed1970 - sheet regenerator engine

(in-readtable :buddens-readtable-a)
(in-package :mps)

; Рабочее тело
(defstruct (WorkingFluidStreamType (:include str:AirStream1)))

(defparameter *th* 900.0)
(defparameter *tk* 330.0)
(defparameter *InitialPressure* ([MPa] 0.6 (/ 0.6 0.456)))


(deftparameter *freq* dbl :initial-value 10.0
               :documentation "Частота вращения коленвала")


(deftparameter *times* fixnum :initial-value 240 :documentation "number of iterations. *times* * *tim* = total time")


(deftparameter *n* fixnum :documentation "number of cells" :initial-value 6)

(defparameter +e 0 "область расширения") 
(defparameter +h 1 "нагреватель")
(defparameter +r1 2 "регенератор 1")
(defparameter +r2 3 "регенератор 2")
(defparameter +k 4 "холодильник")
(defparameter +c 5 "область сжатия")


;;; Нагреватель - подходит, например, для Ст3 при 350С. 
(defparameter *HotCapCreep* ([MPa] 100))

;;; Регенератор 
(defparameter *kwr* 25.0 "Коэфт теплоотдачи материала, на к-ром перепад т-р нагревателя")
; (defparameter *RegenDomeMinWallThickness* ([mm] 0.5) "Технологически заданная мин. толщина стенки корпуса регенератора")


(defparameter *RegenDomeMinWallThickness* ([mm] 5)
  "минимальная толщина стенки, задаваемая из технологических соображений. Потом где-то в другом месте считается истинная толщина по ползучести, далее берём максимум")
(defparameter *RegenDomeDout* ([cm] 6))
(defparameter *DispMinWallThickness* ([mm] 1.5))
(defparameter *RegenDomeDIn*  0.0)
; см. также *NoOfRegens* - оно вынесено выше, т.к. входит в определение вредного пр-ва
(defparameter *RegenDomeLength* ([cm] 12))

(deftparameter *DisplacerShuttleLength* dbl
               :documentation "Длина вытеснителя между изотермическими областями. Реальная длина вытеснителя больше этой величины на ход вытеснителя *dStroke* . См. также *dhotSideRadialClearance*"
               :initial-value ([cm] 12)
               )
; см. также *dBore*
(defparameter *HeaterOuterDiameter* ([mm] 225))


;;; Инерция

(defparameter *hotCapDensity* 7800.0)
(defparameter *genDiam* ([cm] 30.0))

;;; Контроль сходимости
(deftparameter *Xi1Factor* dbl
               :initial-value 0.7e-7
               :documentation "Эмпирический коэфт. Чем он больше, тем больше будет установившийся поток массы при данном перепаде давлений. См. SetEquationControls"
               )

(deftparameter *mvr-limit-factor* dbl
               :initial-value 3e-9
               :documentation "Эмпирический коэффициент. Чем он больше, тем больше возможное абсолютное значение *mvr*. Недопустим обрезание *mvr*, которое можно видеть на графике (do-pl :mvr). При этом также давления будут сильно отличаться друг от друга. См. SetEquationControls , *mvr-limit*")


(deftparameter *dmvr-limit-factor* dbl :initial-value 50.0 :documentation "эмпирический коэфт. Чем он больше, тем большие значения может принимать dmvr. Попытка урезать его обычно ни к чему хорошему не приводит. См. SetEquationControl, *dmvr-limit* ")


(deftparameter *dmvr-to-mvr-099-time-factor* dbl
               :initial-value 0.0035
               :documentation "Эмпирический фактор. За такую долю периода mvr должен приблизиться к dmvr на 0.99 их отличия")

(deftparameter *y-delta-abs-max-factor* dbl :initial-value 0.0025 :documentation "Макс. относит. приращение для решателя ODE. См. call-ode, y-delta-abs-fn")
(deftparameter *y-err-abs-max-factor* dbl :initial-value 2.5e-5 :documentation "Макс. отн. ошибка для решателя ODE. См. call-ode, y-delta-abs-fn")  



;; Геометрия машины
(deftparameter *EngineType* EngineTypes :initial-value 'alpha)

(defparameter *dBore* ([cm] 8.3))
(defparameter *dStroke* ([cm] 6.4))
(defparameter *dhotClearance* ([cm] 0.15))
(defparameter *dhotSideRadialClearance* ([cm] 0.1))
(defparameter *dcoldClearance* ([mm] 1))
(defparameter *dRodBore* 0.0 "Диаметр штока. Для альфы - это шток вытеснителя.")

(defparameter *required-phase* ([gradusov] 120) "Какой хотим сдвиг между полостями")

(defparameter *phase* 90 "Угол между поршнями (в градусах)")


(defparameter *wBore* ([cm] 8.3))
(defparameter *wStroke* ([cm] 6.4))
(defparameter *wTopClearance* ([cm] 0.15))

(defparameter *BetaCylinderShift* ([mm] 10) "на сколько в бета машине вытеснитель вдвинут в цилиндр поршня")

(defparameter *HotCollectorVolume* ([cm3] 40)
  "Этот объём считается находящимся в области расширения, хотя он в реальности может находиться в нагревателе или регенераторе"
  )

(defparameter *NoOfRegens* 1 "Канал от каждого регенератора с *CoolerDuctDiameter* и *CoolerDuctLength*")
(defparameter *CoolerDuctDiameter* ([cm] 0.0001) "Неведомо что это, но евроятно это входит во вредное пр-во")
(defparameter *CoolerDuctLength* ([cm] 0.0001))

(defparameter *ColdCollectorVolume* ([cm3] 50))

; см. также *RegenDomeDout*



(defun sasafed-foil-half-regen ()
  (hx:CalcHeatExchanger
   (hx:MAKE-FlatChannelsMatrix
    :FrontalArea (* (CircleArea *RegenDomeDout*) *NoOfRegens*)
    :Length (* 0.5 *RegenDomeLength*)
    :RebroThickness ([cm] 0.05)
    :GapThickness ([cm] 0.05) ; porosity = 0.5
    ))
  )

(defun SetCells ()
  "Инициализирует ячейки "
  (perga
    ;(let РазряжениеРегенератора 1.7)
    (setf *Cells*
          (vector
           (MAKE-CoCell :TWall *th* :HeatInFn ; (MakeSimpleHeatInFn 260)
                        'NoHeatIn
                        ) ; +e
           (MAKE-CoCell :TWall *th* :HeatInFn 'HxHeatIn ; +h
                        :HeatExchanger
                        (hx:CalcHeatExchanger
                         (hx:MAKE-FlatChannels
                          :Length ([cm] 28)
                          :Width ([cm] 1.8)
                          :Height ([cm] 0.2)
                          :RebroThickness ([cm] 0.8)
                          :FrontalArea
                          (* ([cm] 13) ([cm] 1.8))
                         )))
           (MAKE-CoCell ; +r1
            :TWall (MeanEffectiveTemperature
                    *th*
                    (MeanEffectiveTemperature *th* *tk*)) 
            :HeatInFn 'HxHeatIn ; (MakeSimpleHeatInFn 30000.0)
            :OutTFn 'RegenOutT
            :LeftOutT *th* 
            :RightOutT (MeanEffectiveTemperature *th* *tk*)
            :HeatExchanger (sasafed-foil-half-regen)
            :RegenCellP t
            )
           (MAKE-CoCell ; +r2
            :TWall  (MeanEffectiveTemperature
                     (MeanEffectiveTemperature *th* *tk*)
                     *tk*)
            :HeatInFn 'HxHeatIn ; (MakeSimpleHeatInFn 30000.0)
            :OutTFn 'RegenOutT
            :LeftOutT (MeanEffectiveTemperature *th* *tk*) 
            :RightOutT *tk* 
            :HeatExchanger (sasafed-foil-half-regen)
            :RegenCellP t            
            )
           (MAKE-CoCell :TWall *tk* :HeatInFn 'HxHeatIn ; +k
                        :HeatExchanger
                        (perga
                          (hx:CalcHeatExchanger 
                           (hx:MAKE-FlatChannels
                            :Length ([cm] 8.5)
                            :Width ([cm] 1.05)
                            :Height ([cm] 0.06)
                            :RebroThickness ([cm] 0.095)
                            :FrontalArea
                            (* ([cm] 16.2) ([cm] 1.05))
                            )
                           )))
           (MAKE-CoCell :TWall *tk* :HeatInFn 'NoHeatIn ;(MakeSimpleHeatInFn 600)
                        )                 ; +c
           ))))



(defun SetEngineVolumeLaws ()
  (funcall 'SetSinEngineVolumeLaws))
