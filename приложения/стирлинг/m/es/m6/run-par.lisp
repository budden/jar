; -*- Encoding: utf-8; System :m -*-
;; Параметрически запускаем m6 
;; (rc :filename "C:/stir/sw/m/es/m6/mps-m6.lisp" :no-of-circles 7)
(in-readtable :buddens-readtable-a)
(in-package :m)

; Рабочее тело


;(defparameter *CurrentParameters*
;  (list
;;   '*dhotSideRadialClearance* ([mm] 0.5)
;   '*RegenDomeLength* ([mm] 300)
;   '*required-phase* ([gradusov] 120)
;  ))


(defun RunPar1 ()
  (dolist (*dhotSideRadialClearance* (list ([mm] 0.4)([mm] 0.5)([mm] 0.6)))
    (dolist (*RegenDomeLength* (list ([mm] 280)([mm] 300)([mm] 320)))
      (dolist (*required-phase* (list ([gradusov] 115)([gradusov] 120)([gradusov] 125)))
        (let ((*CurrentParameters*
               (list
                '*dhotSideRadialClearance* *dhotSideRadialClearance* 
                '*RegenDomeLength* *RegenDomeLength* 
                '*required-phase* *required-phase* 
                )))
          (rc :filename "C:/stir/sw/m/es/m6/mps-m6-par.lisp" :no-of-circles 7 :do-plots nil)
          (with-open-file (ou "C:/stir/sw/m/es/m6/run-par.report.txt" :direction :output :if-does-not-exist :create :if-exists :append)
            (show-expr *CurrentParameters* ou)
            (show-expr *NetEfficiency* ou)
            (show-expr *StatPowerSimple* ou)
            (terpri)
            (terpri)))))))
        
         

