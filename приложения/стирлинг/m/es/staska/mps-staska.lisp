; -*- Encoding: utf-8; System :m -*-
;; staska
;; запуск/to run
;; (rc :filename "C:/stir/sw/m/es/staska/mps-staska.lisp" :no-of-circles 4)
(in-readtable :buddens-readtable-a)
(in-package :mps)

; Рабочее тело
(defstruct (WorkingFluidStreamType (:include str:AirStream1)))


; Диаметр поршня 120мм, ход 120мм, диаметр вытеснителя - 120мм, ход 160мм, длина 950мм, из ней 550 мм нагревател,
; 400мм охладител. Диаметр внутри вытеснителя - 125мм или зазор по 2,5мм с каждей стороно.
; Давление нижние атмосферное. Обоороты - от 120 до 300 в минуту. Температура горячей части 500, холодной 80 градусов. Гамма.q


(defparameter *th* ([Celsius] 500))
(defparameter *tk* ([Celsius] 80))

(defparameter *InitialPressure* ([Bar] 1 (/ 0.817)))
(defparameter *CrankcaseInitialPressureFactor* 0.81835)

(deftparameter *freq* dbl :initial-value (/ 120 60.0)
               :documentation "Частота вращения коленвала")


(deftparameter *times* fixnum :initial-value 240 :documentation "number of iterations. *times* * *tim* = total time")


(deftparameter *n* fixnum :documentation "number of cells" :initial-value 7)

(defparameter +e 0 "область расширения") 
(defparameter +h 1 "нагреватель")
(defparameter +r1 2 "регенератор 1")
(defparameter +r2 3 "регенератор 2")
(defparameter +k 4 "холодильник")
(defparameter +c 5 "область сжатия")
(defparameter +b 6 "пр-во картера")


;;; Нагреватель - подходит, например, для Ст3 при 350С. 
(defparameter *HotCapCreep* ([MPa] 100))

;;; Регенератор 
(defparameter *kwr* 25.0 "Коэфт теплоотдачи материала, на к-ром перепад т-р нагревателя")
; (defparameter *RegenDomeMinWallThickness* ([mm] 0.5) "Технологически заданная мин. толщина стенки корпуса регенератора")


(defparameter *RegenDomeMinWallThickness* ([mm] 0.7)
  "минимальная толщина стенки, задаваемая из технологических соображений. Потом где-то в другом месте считается истинная толщина по ползучести, далее берём максимум")
(defparameter *TolshinaStenkiNagrevatelja* ([mm] 2))

(defparameter *DispMinWallThickness* ([mm] 0.7))
(defparameter *RegenDomeDIn* ([mm] 57))
; см. также *NoOfRegens* - оно вынесено выше, т.к. входит в определение вредного пр-ва
(defparameter *RegenDomeLength* ([mm] 300))

(deftparameter *DisplacerShuttleLength* dbl
               :documentation "Длина вытеснителя между изотермическими областями. Реальная длина вытеснителя больше этой величины на ход вытеснителя *dStroke* . См. также *dhotSideRadialClearance*"
               :initial-value *RegenDomeLength* ; ([mm] 35)
               )
; см. также *dBore*
(defparameter *HeaterOuterDiameter* ([mm] 130))


;;; Инерция

(defparameter *hotCapDensity* 7800.0)
(defparameter *genDiam* ([cm] 20.0))

;;; Контроль сходимости
(deftparameter *Xi1Factor* dbl
               :initial-value 0.7e-8
               :documentation "Эмпирический коэфт. Чем он больше, тем больше будет установившийся поток массы при данном перепаде давлений. См. SetEquationControls"
               )

(deftparameter *mvr-limit-factor* dbl
               :initial-value 3e-9
               :documentation "Эмпирический коэффициент. Чем он больше, тем больше возможное абсолютное значение *mvr*. Недопустим обрезание *mvr*, которое можно видеть на графике (do-pl :mvr). При этом также давления будут сильно отличаться друг от друга. См. SetEquationControls , *mvr-limit*")


(deftparameter *dmvr-limit-factor* dbl :initial-value 50.0 :documentation "эмпирический коэфт. Чем он больше, тем большие значения может принимать dmvr. Попытка урезать его обычно ни к чему хорошему не приводит. См. SetEquationControl, *dmvr-limit* ")


(deftparameter *dmvr-to-mvr-099-time-factor* dbl
               :initial-value 0.0035
               :documentation "Эмпирический фактор. За такую долю периода mvr должен приблизиться к dmvr на 0.99 их отличия")

(deftparameter *y-delta-abs-max-factor* dbl :initial-value 0.0025 :documentation "Макс. относит. приращение для решателя ODE. См. call-ode, y-delta-abs-fn")
(deftparameter *y-err-abs-max-factor* dbl :initial-value 2.5e-5 :documentation "Макс. отн. ошибка для решателя ODE. См. call-ode, y-delta-abs-fn")  



;; Геометрия машины
(deftparameter *EngineType* EngineTypes :initial-value 'gamma)
(defparameter *SimpleGapRegenerator* t) ; для машин, у к-рых нет регенератора, а просто зазор

(defparameter *dBore* ([mm] 120.0))
(defparameter *dStroke* ([mm] 160.0))
(defparameter *dhotClearance* ([mm] 1))
(defparameter *dhotSideRadialClearance* ([mm] 2.5))
(defparameter *dcoldClearance* ([mm] 1))
(defparameter *dRodBore* ([mm] 30) "Диаметр штока. Для альфы - это шток вытеснителя.")

(defparameter *RegenDomeDout* (+ *dBore* (* 2 (+ *dhotSideRadialClearance* *TolshinaStenkiNagrevatelja*))))

;(defparameter *required-phase* ([gradusov] 125) "Какой хотим сдвиг между полостями. Имеет смысл только для беты. См. m:OptimumPhaseForBeta")

(defparameter *phase* 90.0 "Угол между поршнями (в градусах)")


(defparameter *wBore* ([mm] 120))
(defparameter *wStroke* ([mm] 120))
(defparameter *wTopClearance* ([mm] 1))

;(defparameter *BetaCylinderShift* ([mm] 10) "на сколько в бета машине вытеснитель вдвинут в цилиндр поршня")

(defparameter *HotCollectorVolume* ([cm3] 15.0)
  "Этот объём считается находящимся в области расширения, хотя он наполовину
находится в нагревателе"
  )

(defparameter *NoOfRegens* 1 "Канал от каждого регенератора с *CoolerDuctDiameter* и *CoolerDuctLength*")
(defparameter *CoolerDuctDiameter* ([cm] 1.0) "Неведомо что это, но вероятно это входит во вредное пр-во")
(defparameter *CoolerDuctLength* ([cm] 10))

(defparameter *ColdCollectorVolume* ([cm3] 11.0)) 

; см. также *RegenDomeDout*

(defparameter *CrankcaseMaximumVolume* ([cm3] 9400))
(defparameter *PistonSealHoleDiameter* ([mm] 0.07))


(defparameter *LubricatingOilKinematicViscosity* 20e-6)
(defparameter *PistonRingHeight* ([mm] 0.00025 2)) 
(defparameter *PistonRingFrictionCoefficient* 0.21) 
   ; без смазки чугун по чугуну 0.1-0.21 http://www.dpva.info/Guide/GuidePhysics/Frication/SlidingFriction/
   ; 0.165 максимум для бронза
   ; для текстолита - при плохой смазке 0.22, при хорошей якобы 0.02-0.06. 
(defparameter *PistonRingInitialPressure* ([Bar] 0.1))
(defparameter *CrossheadFrictionCoefficient* 0.12) ; коэфт трения ползуна (считаем граничную смазку)

(defun half-regen ()
  (perga
    (let result
      (hx:CalcHeatExchanger
       (hx:MAKE-Annulus
        :Length (* 0.5 *RegenDomeLength*)
        :InnerDiam *dBore*
        :OuterDiam (+ *dBore* (* 2 *dhotSideRadialClearance*))
        )))
    result
    ))

(defun SetCells ()
  "Инициализирует ячейки "
  (perga
    ;(let РазряжениеРегенератора 1.7)
    (setf *Cells*
          (vector
           (MAKE-CoCell :TWall *th* :HeatInFn ; (MakeSimpleHeatInFn 600)
                        'NoHeatIn
                        ) ; +e
           (MAKE-CoCell :TWall *th* :HeatInFn 'HxHeatIn ; +h
                        :HeatExchanger
                        (hx:CalcHeatExchanger
                         (hx:MAKE-Annulus
                          :Length ([mm] 300)
                          :InnerDiam *dBore*
                          :OuterDiam (+ *dBore* (* 2 *dhotSideRadialClearance*)))
                          ))
           (MAKE-CoCell ; +r1
            :TWall (MeanEffectiveTemperature
                    *th*
                    (MeanEffectiveTemperature *th* *tk*)) 
            :HeatInFn 'HxHeatIn ; (MakeSimpleHeatInFn 30000.0)
            :OutTFn 'RegenOutT
            :LeftOutT *th* 
            :RightOutT (MeanEffectiveTemperature *th* *tk*)
            :HeatExchanger ; (half-m4-regen ([mm] 0.28) ([mm] 0.14))
            (half-regen)
            :RegenCellP t
            )
           (MAKE-CoCell ; +r2
            :TWall  (MeanEffectiveTemperature
                     (MeanEffectiveTemperature *th* *tk*)
                     *tk*)
            :HeatInFn 'HxHeatIn ; (MakeSimpleHeatInFn 30000.0)
            :OutTFn 'RegenOutT
            :LeftOutT (MeanEffectiveTemperature *th* *tk*) 
            :RightOutT *tk* 
            :HeatExchanger ; (half-m4-regen ([mm] 0.341) ([mm] 0.11)) сетка 341
            (half-regen)
            ; :HeatExchanger (half-m4-regen ([mm] 0.261) ([mm] 0.1))
            :RegenCellP t            
            )
           (MAKE-CoCell :TWall *tk* :HeatInFn 'HxHeatIn ; +k
                        :HeatExchanger
                        (hx:CalcHeatExchanger
                         (hx:MAKE-Annulus
                          :Length ([mm] 300)
                          :InnerDiam *dBore*
                          :OuterDiam (+ *dBore* (* 2 *dhotSideRadialClearance*)))
                          )
                        )
           (MAKE-CoCell :TWall *tk* :HeatInFn 'NoHeatIn ; (MakeSimpleHeatInFn 600)
                        )  ; +c
           (MAKE-CoCell :TWall *tk* :HeatInFn 'NoHeatIn ; +b
                        )
           ))))


(defun SetEngineVolumeLaws ()
  (funcall 'SetSinEngineVolumeLaws))
