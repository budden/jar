; -*- Encoding: utf-8; -*-

(in-package :physical-units)
(in-readtable :buddens-readtable-a)

(defconstant [cm3] 1e-6)
(defun [cm3] (&rest args) (apply '* [cm3] args))

(defconstant [gramm] 1e-3)
(defun [gramm] (&rest args) (apply '* [gramm] args))

(defconstant [cm2] 1e-4)
(defun [cm2] (&rest args) (apply '* [cm2] args))

(defconstant [cm] 1e-2)
(defun [cm] (&rest args) (apply '* [cm] args))

(defconstant [gradusov] (/ pi 180))
(defun [gradusov] (&rest args) (apply '* [gradusov] args))

(defconstant [mm] 1e-3)
(defun [mm] (&rest args) (apply '* [mm] args))

(defconstant [liter] 1e-3)
(defun [liter] (&rest args) (apply '* [liter] args))

(defconstant [MPa] 1e6)
(defun [MPa] (&rest args) (apply '* [MPa] args))

(defconstant [inch] (* 25.4 [mm]))
(defun [inch] (&rest args) (apply '* [inch] args))

(defconstant [foot] (* 30.4 [cm]))
(defun [foot] (&rest args) (apply '* [foot] args))

(defconstant [minute] 60)
(defun [minute] (&rest args) (apply '* [minute] args))

(defconstant [hour] 3600)
(defun [hour] (&rest args) (apply '* [hour] args))

(defconstant [PSI] (* 6.8946e-3 [MPa]))
(defun [PSI] (&rest args) (apply '* [PSI] args))

(defconstant [btu] 1055.05585262)
(defun [btu] (&rest args) (apply '* [btu] args))

(defconstant [kWh] 3.6e6)
(defun [kWh] (&rest args) (apply '* [kWh] args))

(defconstant [hp] 735.5) ; неоднозначно!
(defun [hp] (&rest args)
  (apply '* [hp] args))

(defconstant [ccal] 4200)
(defun [ccal] (&rest args) (apply '* [ccal] args))

(defconstant [lbs] 0.454)
(defun [lbs] (&rest args) (apply '* [lbs] args))

(defconstant [Bar] 1e5)
(defun [Bar] (&rest args) (apply '* [Bar] args))

(defconstant [atm] 1.01325e5)
(defun [atm] (&rest args) (apply '* [atm] args))

(defconstant [mm.hg] (* [atm] 1/760))
(defun [mm.hg] (&rest args) (apply '* [mm.hg] args))
 

(defconstant [kgs] 9.80665)
(defun [kgs] (&rest args) (apply '* [kgs] args))

(defun [Celsius] (tt)
  "Celsius to Kelvin"
  (+ 273.0 tt))

(defun [Fahrenheit] (tf)
  "Farenheit to Kelvin"
  ([Celsius] (* 5/9 (- tf 32))))

#| Для перевода температуры из шкалы Цельсия в шкалу Фаренгейта нужно умножить исходное число на 9/5 и прибавить 32. |#



#|
Для перевода температуры из шкалы Цельсия в шкалу Фаренгейта нужно умножить исходное число на 9/5 и прибавить 32. 
|#

;; Вязкость/Viscosity
(defconstant [cSt] 1.0e-6 "Сантистокс" #|m2/s|#)
(defun [cSt] (&rest args) (apply '* [cSt] args))

(defconstant [cP] 1.0e-3 "Сантипуаз" #| = 10−2 P = 10−3 Pa·s  |#)
(defun [cP] (&rest args) (apply '* [cSt] args))

