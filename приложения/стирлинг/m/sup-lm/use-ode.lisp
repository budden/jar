;; -*- Mode:Lisp; Syntax:ANSI-Common-LISP; Coding:utf-8; fill-column:132 -*-
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; @file      use-ode.lisp
;; @author    Mitch Richling <http://www.mitchr.me>
;; @Copyright Copyright 1997,1998,2004,2013 by Mitch Richling.  All rights reserved.
;; @Revision  $Revision: 1.45 $ 
;; @SCMdate   $Date: 2015/01/09 01:37:07 $
;; @brief     ODE (Ordinary Differential Equation) IVP (Initial Value Problem) solvers.@EOL
;; @Keywords  lisp interactive ODE ordinary differential equations IVP initial value problems root solutions
;; @Std       Common Lisp
;;
;;            
;;            

;; MJR TODO NOTE use-ode.lisp: Don't print too many warnings. Ex: WARNING(mjr_ode_slv-ivp-erk-interval): Violated Y-ERR-ABS-MAX or Y-DELTA-ABS-MAX constraint!
;; MJR TODO NOTE use-ode.lisp: * mjr_ode_slv-ivp-erk-interval
;; MJR TODO NOTE use-ode.lisp:   * Zero x-delta-min may lead to infinite loop
;; MJR TODO NOTE use-ode.lisp:   * Add exit for total path length beyond a threshold
;; MJR TODO NOTE use-ode.lisp:   * Add exit for length of y beyond a threshold
;; MJR TODO NOTE use-ode.lisp:   * Add min y-delta -- a "get progress" parameter
;; MJR TODO NOTE use-ode.lisp:   * Starting step size computation 
;; MJR TODO NOTE use-ode.lisp: * mjr_ode_erk-step-kernel
;; MJR TODO NOTE use-ode.lisp:   * Add option to provide traditional error computation
;; MJR TODO NOTE use-ode.lisp: * Add support EQ list with some elements returning vectors and others return numbers
;; MJR TODO NOTE use-ode.lisp: * Better handling when tolerances are not given: i.e. don't compute y2-delta, traverse lists unnecessarily
;; MJR TODO NOTE use-ode.lisp: * FASTER
;; MJR TODO NOTE use-ode.lisp: * Richardson Extrapolation for 'err' tolerances
;; MJR TODO NOTE use-ode.lisp: * RK methods
;; MJR TODO NOTE use-ode.lisp:   * Add high order Verner methods
;; MJR TODO NOTE use-ode.lisp:   * Add additional fehlberg methods
;; MJR TODO NOTE use-ode.lisp:   * Add optimized version of Dormand-Price method (double, formula based, FAST)
;; MJR TODO NOTE use-ode.lisp:   * Add embedded methods for all order 1, 2, 3, & 4 methods
;; MJR TODO NOTE use-ode.lisp:   * Add NAG methods
;; MJR TODO NOTE use-ode.lisp: * Do everything in float-double
;; MJR TODO NOTE use-ode.lisp: * Add better support for stiff problems

;;----------------------------------------------------------------------------------------------------------------------------------

(if (not (find-package :MJR_ODE))
    (defpackage :MJR_ODE (:USE :COMMON-LISP :MJR_NUMU :MJR_VVEC :MJR_CHK :MJR_VEC :MJR_UTIL)))

(in-package :MJR_ODE)

(export '(mjr_ode_help

          mjr_ode_slv-ivp-erk-mesh mjr_ode_slv-ivp-erk-interval

          mjr_ode_erk-step-kernel

          mjr_ode_erk-step-euler-1 mjr_ode_erk-step-heun-2 mjr_ode_erk-step-mid-point-2 mjr_ode_erk-step-runge-kutta-4
          mjr_ode_erk-step-kutta-three-eight-4

          mjr_ode_erk-step-heun-euler-1-2 mjr_ode_erk-step-zonneveld-4-3 mjr_ode_erk-step-merson-4-5
          mjr_ode_erk-step-fehlberg-4-5 mjr_ode_erk-step-fehlberg-7-8 mjr_ode_erk-step-bogackia-shampine-3-2
          mjr_ode_erk-step-cash-karp-5-4 mjr_ode_erk-step-dormand-prince-5-4 mjr_ode_erk-step-verner-6-5

          mjr_ode_erk-step-euler-1-direct mjr_ode_erk-step-heun-2-direct mjr_ode_erk-step-runge-kutta-4-direct
          ))

;;----------------------------------------------------------------------------------------------------------------------------------
(defun mjr_ode_help ()
"Help for MJR_ODE:
A system of ODEs is defined to be:

$$
\\frac{\\mathrm{d}\\vec{\\mathbf{y}}}{\\mathrm{d}x} =  \\mathbf{f}(x, \\mathbf{y})
=
\\left[\\begin{array}{c}
 \\frac{\\mathrm{d}y_1}{\\mathrm{d}x} \\\\
 \\vdots                            \\\\
 \\frac{\\mathrm{d}y_n}{\\mathrm{d}x} \\\\
\\end{array}\\right]
=
\\left[\\begin{array}{c}
 f_1(x, \\mathbf{y}) \\\\
 \\vdots             \\\\
 f_n(x, \\mathbf{y}) \\\\
\\end{array}\\right]
=
\\left[\\begin{array}{c}
 f_1(x, [y_1, \\cdots, y_n]^\\mathrm{T}) \\\\
 \\vdots                                \\\\
 f_n(x, [y_1, \\cdots, y_n]^\\mathrm{T}) \\\\
\\end{array}\\right]
$$

While such systems may be the natural result of the physical system under study, it is more common for them to be the result of
transforming a single $n^\\mathrm{th}$ order differential equation into a group of $n$ first order equations -- which facilitates
the solution of the original equation via numerical methods.  When multiple higher order ODEs are to be solved, the natural result
is several such systems to be simultaneously solved.  This package solves such groups of first order ODE systems via various
Runge-Kutta methods.

A Runge-Kutta method is defined by a set of coefficients that are most frequently organized into a ``Butcher tableau'':

$$\\begin{array}{c|cccc}
c_1              & a_{11}      & a_{12}      & \\dots  & a_{1s}      \\\\
c_2              & a_{21}      & a_{22}      & \\dots  & a_{2s}      \\\\
\\vdots          & \\vdots     & \\vdots     & \\ddots & \\vdots     \\\\
c_s              & a_{s1}      & a_{s2}      & \\dots  & a_{ss}      \\\\
\\hline
\\rule{0pt}{12pt} & \\check{b}_1 & \\check{b}_2 & \\dots  & \\check{b}_s \\\\
                 &   \\hat{b}_1 &   \\hat{b}_2 & \\dots  &   \\hat{b}_s \\\\
\\end{array}$$

Given initial conditions, $x_0$ and $\\mathbf{y_0}$, we may approximate the value of $\\mathbf{y}(x_0+\\Delta{x})$ by
$\\mathbf{y_0}+\\mathbf{\\Delta\\check{y}}$ with $\\mathbf{\\Delta\\check{y}}$ computed as below.  Note that the
$\\mathbf{\\hat{b}}$ vector, if it is defined, is used to construct an alternate solution for the purpose of estimating the error of
the approximation generated from $\\mathbf{\\check{b}}$.

$$\\mathbf{\\hat{k}}_i   = \\mathbf{f}\\left(x + c_i \\Delta{x},\\, \\mathbf{y} + \\Delta{x} \\sum_{j=1}^{s} a_{ij} \\mathbf{\\hat{k}}_j\\right)
     \\,\\,\\,\\,\\,\\,\\,\\,\\,\\,\\,\\,
  \\mathbf{\\check{k}}_i = \\mathbf{f}\\left(x + c_i \\Delta{x},\\, \\mathbf{y} + \\Delta{x} \\sum_{j=1}^{s} a_{ij} \\mathbf{\\check{k}}_j\\right)$$
$$\\mathbf{\\Delta\\check{y}} = \\Delta{x}\\sum_{i=1}^s \\check{b}_i \\mathbf{k}_i \\,\\,\\,\\,\\,\\,\\,\\,\\,\\,\\,\\,
  \\mathbf{\\Delta\\hat{y}}   = \\Delta{x}\\sum_{i=1}^s \\hat{b}_i   \\mathbf{k}_i$$

The primary 'Human Interface' functions are:

  * MJR_ODE_SLV-IVP-ERK-MESH     -- Solve ODE IVP via an explicit RK method at specified grid points
  * MJR_ODE_SLV-IVP-ERK-INTERVAL -- Solve ODE IVP via explicit RK method interval endpoint (with an adaptive mesh or single jump)

The primary emphisis of this code base is to eanble one to easily compare various RK methods and experiment with new ones.  The
functions above impliment the overall structure common to all RK algorithms, but leave the actual RK step to plugin functions.

Some vocabulary:
   * ERK      Explicit Runge-Kutta
               The matrix $[a_{i,}]$ is lower triangular.
   * EERK     Embedded Explicit Runge-Kutta
               The $\\mathbf{\\hat{b}}$ vector is defined.  
   * EERKLE   Embedded Explicit Runge-Kutta with Local Extrapolation
               The $\\mathbf{\\check{b}}$ method has is higher order than the $\\mathbf{\\hat{b}}$ method 
   * ODE      Ordinary Differential Equation
   * IVP      Initial Value Problem

The following functions each impliment a the single step RK method sutable for use with the MJR_ODE_SLV-IVP-ERK-MESH and
MJR_ODE_SLV-IVP-ERK-INTERVAL functions:

  * mjr_ode_erk-step-euler-1               --  order 1      ERK
  * mjr_ode_erk-step-euler-1-direct        --  order 1      ERK - as above, but a faster direct formula
  * mjr_ode_erk-step-heun-2                --  order 2      ERK
  * mjr_ode_erk-step-heun-2-direct         --  order 2      ERK - as above, but a faster direct formula
  * mjr_ode_erk-step-mid-point-2           --  order 2      ERK
  * mjr_ode_erk-step-runge-kutta-4         --  order 4      ERK
  * mjr_ode_erk-step-runge-kutta-4-direct  --  order 4      ERK - as above, but a faster direct formula
  * mjr_ode_erk-step-kutta-three-eight-4   --  order 4      ERK
  * mjr_ode_erk-step-heun-euler-1-2        --  order 1(2)   EERK
  * mjr_ode_erk-step-zonneveld-4-3         --  order 4(3)   EERK
  * mjr_ode_erk-step-merson-4-5            --  order 4('5') EERK
  * mjr_ode_erk-step-fehlberg-4-5          --  order 4(5)   EERK
  * mjr_ode_erk-step-fehlberg-7-8          --  order 7(8)   EERK
  * mjr_ode_erk-step-bogackia-shampine-3-2 --  order 3(2)   EERKLE
  * mjr_ode_erk-step-cash-karp-5-4         --  order 5(4)   EERKLE
  * mjr_ode_erk-step-dormand-prince-5-4    --  order 5(4)   EERKLE
  * mjr_ode_erk-step-verner-6-5            --  order 6(5)   EERKLE

See MJR_ODE_ERK-STEP-KERNEL and MJR_ODE_ERK-STEP-HEUN-EULER-1-2 for the interface guidelines for a RK step function, and how to
construct one from the Butcher Tableau.  See MJR_ODE_ERK-STEP-EULER-1-DIRECT for an example of directly implementing a non-embedded
RK step function."
  (documentation 'mjr_ode_help 'function))

;;----------------------------------------------------------------------------------------------------------------------------------
(defun mjr_ode_erk-step-kernel-sequential (eq x y xdelta
                                              a c b1 b2 p1 p2
                                              y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max)
  "Perform one step of an Explicit Runge-Kutta method (with or without local extrapolation).

Return is two values:

  * y1-delta -- the delta approximation computed from B1.
  * error    -- approximation of the error via  B2 or NIL if B2 was not defined

Arguments:
  A, C, B1, & B2 specify the Butcher Tableau (Butcher 1964b) for the Runge-Kutta method
     A - The matrix
     C - The left vector
     B1 - First bottom vector (Used to compute Y-NEXT approximation)
     B2 - Second bottom vector (Used to compute ERR)."
  (declare (ignore p2))
  (let* ((nstage (array-dimension a 0))
         (k-lst  (mapcar (lambda (cur-eq cur-y)
                           (let ((k (make-array nstage)))
                             (loop for i from 0 upto (1- nstage)
                                   finally (return k)
                                   do (setf (aref k i)
                                            (funcall cur-eq (+ x (* (aref c i) xdelta))
                                                     (mjr_vec_+ cur-y
                                                                (mjr_vec_* xdelta
                                                                           (loop with vs = 0
                                                                                 for j from 0 upto (1- i)
                                                                                 do (setf vs (mjr_vec_+ vs (mjr_vec_* (aref a i j)
                                                                                                                      (aref k j))))
                                                                                 finally (return vs)))))))))
                         eq
                         y))
         (y1-delta (mapcar (lambda (k)
                             (mjr_vec_* xdelta (reduce #'mjr_vec_+ (map 'vector #'mjr_vec_* b1 k))))
                           k-lst))
         (y2-delta (if b2
                       (mapcar (lambda (k)
                                 (mjr_vec_* xdelta (reduce #'mjr_vec_+ (map 'vector #'mjr_vec_* b2 k))))
                               k-lst)))
         (err      nil)
         (y-rat    nil))
    (if (or y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max) ;; Compute err and y-rat
        (loop for cur-y1-delta        in y1-delta
              for cur-y2-delta        in y2-delta
              for cur-y               in y
              for cur-y-err-abs-max   in y-err-abs-max
              for cur-y-err-rel-max   in y-err-rel-max
              for cur-y-delta-abs-max in y-delta-abs-max
              for cur-y-delta-rel-max in y-delta-rel-max
              do (loop for i-cur-y1-delta        across cur-y1-delta
                       for i-cur-y2-delta        across cur-y2-delta
                       for i-cur-y               across cur-y
                       for i-cur-y-err-abs-max   across cur-y-err-abs-max
                       for i-cur-y-err-rel-max   across cur-y-err-rel-max
                       for i-cur-y-delta-abs-max across cur-y-delta-abs-max
                       for i-cur-y-delta-rel-max across cur-y-delta-rel-max
                       for y-mag = (+ (abs i-cur-y) (mjr_numu_max-nil i-cur-y1-delta i-cur-y2-delta))
                       for i-s   = (mjr_numu_max-nil i-cur-y-err-abs-max (if i-cur-y-err-rel-max (* i-cur-y-err-rel-max y-mag)))
                       when (mjr_chk_!=0 i-cur-y1-delta)
                       do (progn (if i-cur-y-delta-abs-max
                                     (setf y-rat (mjr_numu_min-nil y-rat (abs (/ i-cur-y-delta-abs-max i-cur-y1-delta)))))
                                 (if i-cur-y-delta-rel-max
                                     (setf y-rat (mjr_numu_min-nil y-rat (abs (/ (* y-mag i-cur-y-delta-rel-max) i-cur-y1-delta))))))
                       ;; We don't check for /0 here as i-s can be very small....
                       when i-s
                       do (setf err (mjr_numu_max-nil err (abs (/ (- i-cur-y1-delta i-cur-y2-delta) i-s)))))))
    (values y1-delta
            (mjr_numu_min-nil (if err
                                  (if (mjr_chk_!=0 err)
                                      ;; We use p1+1 instead of 1+max(p1,p2) as is normally used.  The normal algorithm might
                                      ;; use b1 or b2 to compute y-delta, and we always use b1 here -- so we can just use p1+1.
                                      (expt (/ err) (/ (+ 1 p1)))
                                      1))
                              y-rat))))

;;; budden vvvv
(defparameter *ode-solver-subprocesses-limit* 7)
(defvar *ode-solver-subprocesses-running* 0)

(defparameter *ode-solver-lock* (bt:make-lock "ODE Solver lock"))

(defmacro with-ode-solver-lock (&body body)
  `(bt:with-lock-held (*ode-solver-lock*)
     ,@body))

(defparameter *call-ode-solver-function-in-parallel-when-possible-sleep-time* 0.001)

(defun call-ode-solver-function-in-parallel-when-possible (fn)
  "Waits when there are not too many processes and runs a function"
  (loop 
   (with-ode-solver-lock
     (when (> *ode-solver-subprocesses-limit* *ode-solver-subprocesses-running*)
       (incf *ode-solver-subprocesses-running*)
       (bt:make-thread
        (lambda ()
          (unwind-protect
              (funcall fn)
            (with-ode-solver-lock
             (incf *ode-solver-subprocesses-running* -1))))
        :name "ode solver worker"
        )
       (return-from call-ode-solver-function-in-parallel-when-possible (values))))
   ;(sleep *call-ode-solver-function-in-parallel-when-possible-sleep-time*)
   ))


(defun wait-for-all-ode-solver-subprocesses-to-end ()
  (loop
   (with-ode-solver-lock
     (when (= *ode-solver-subprocesses-running* 0)
       (return-from wait-for-all-ode-solver-subprocesses-to-end (values))))
   ;(sleep *call-ode-solver-function-in-parallel-when-possible-sleep-time*)
   ))


(defun mjr_ode_erk-step-kernel-parallel (eq x y xdelta
                                a c b1 b2 p1 p2
                                y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max)
  "Perform one step of an Explicit Runge-Kutta method (with or without local extrapolation).
Arguments:
  A, C, B1, & B2 specify the Butcher Tableau (Butcher 1964b) for the Runge-Kutta method
     A - The matrix
     C - The left vector
     B1 - First bottom vector (Used to compute Y-NEXT approximation)
     B2 - Second bottom vector (Used to compute ERR)."
  (declare (ignore p2))
  (assert (= 0 *ode-solver-subprocesses-running*))
  (let* ((nstage (array-dimension a 0))
         (k-lst  (mapcar (lambda (cur-eq cur-y)
                           (let ((k (make-array nstage)))
                             (dotimes (i nstage)
                               (let ((i i))
                                 (call-ode-solver-function-in-parallel-when-possible
                                  (lambda ()
                                    (let ((res (funcall
                                                cur-eq (+ x (* (aref c i) xdelta))
                                                (mjr_vec_+ cur-y
                                                           (mjr_vec_* xdelta
                                                                      (let ((vs 0))
                                                                        (dotimes (j i)
                                                                          (setf vs (mjr_vec_+ vs (mjr_vec_* (aref a i j)
                                                                                                            (aref k j)))))
                                                                        vs))))))
                                      (with-ode-solver-lock
                                        (setf (aref k i) res)))))))
                             (wait-for-all-ode-solver-subprocesses-to-end)
                             k))
                         eq
                         y))
         (y1-delta (mapcar (lambda (k)
                             (mjr_vec_* xdelta (reduce #'mjr_vec_+ (map 'vector #'mjr_vec_* b1 k))))
                           k-lst))
         (y2-delta (if b2
                       (mapcar (lambda (k)
                                 (mjr_vec_* xdelta (reduce #'mjr_vec_+ (map 'vector #'mjr_vec_* b2 k))))
                               k-lst)))
         (err      nil)
         (y-rat    nil))
    (if (or y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max) ;; Compute err and y-rat
        (loop for cur-y1-delta        in y1-delta
              for cur-y2-delta        in y2-delta
              for cur-y               in y
              for cur-y-err-abs-max   in y-err-abs-max
              for cur-y-err-rel-max   in y-err-rel-max
              for cur-y-delta-abs-max in y-delta-abs-max
              for cur-y-delta-rel-max in y-delta-rel-max
              do (loop for i-cur-y1-delta        across cur-y1-delta
                       for i-cur-y2-delta        across cur-y2-delta
                       for i-cur-y               across cur-y
                       for i-cur-y-err-abs-max   across cur-y-err-abs-max
                       for i-cur-y-err-rel-max   across cur-y-err-rel-max
                       for i-cur-y-delta-abs-max across cur-y-delta-abs-max
                       for i-cur-y-delta-rel-max across cur-y-delta-rel-max
                       for y-mag = (+ (abs i-cur-y) (mjr_numu_max-nil i-cur-y1-delta i-cur-y2-delta))
                       for i-s   = (mjr_numu_max-nil i-cur-y-err-abs-max (if i-cur-y-err-rel-max (* i-cur-y-err-rel-max y-mag)))
                       when (mjr_chk_!=0 i-cur-y1-delta)
                       do (progn (if i-cur-y-delta-abs-max
                                     (setf y-rat (mjr_numu_min-nil y-rat (abs (/ i-cur-y-delta-abs-max i-cur-y1-delta)))))
                                 (if i-cur-y-delta-rel-max
                                     (setf y-rat (mjr_numu_min-nil y-rat (abs (/ (* y-mag i-cur-y-delta-rel-max) i-cur-y1-delta))))))
                       ;; We don't check for /0 here as i-s can be very small....
                       when i-s
                       do (setf err (mjr_numu_max-nil err (abs (/ (- i-cur-y1-delta i-cur-y2-delta) i-s)))))))
    (values y1-delta
            (mjr_numu_min-nil (if err
                                  (if (mjr_chk_!=0 err)
                                      ;; We use p1+1 instead of 1+max(p1,p2) as is normally used.  The normal algorithm might
                                      ;; use b1 or b2 to compute y-delta, and we always use b1 here -- so we can just use p1+1.
                                      (expt (/ err) (/ (+ 1 p1)))
                                      1))
                              y-rat))))


(defun mjr_ode_erk-step-kernel (eq x y xdelta
                                a c b1 b2 p1 p2
                                y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max)
  "Perform one step of an Explicit Runge-Kutta method (with or without local extrapolation).
Arguments:
  A, C, B1, & B2 specify the Butcher Tableau (Butcher 1964b) for the Runge-Kutta method
     A - The matrix
     C - The left vector
     B1 - First bottom vector (Used to compute Y-NEXT approximation)
     B2 - Second bottom vector (Used to compute ERR)."
  (mjr_ode_erk-step-kernel-sequential eq x y xdelta
                                a c b1 b2 p1 p2
                                y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max)
  )
;; budden ^^^^            
;;----------------------------------------------------------------------------------------------------------------------------------
(defun mjr_ode_erk-step-euler-1 (eq x y xdelta y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max)
  "Order 1.
References:
  Euler (1768); Institutionum Calculi Integralis. Volumen Primum, Opera Omnia, Vol XI. p424
  Hairer, Norsett & Wanner (2009). Solving Ordinary Differential Equations. I: Nonstiff Problems. p35"
  (mjr_ode_erk-step-kernel eq x y xdelta
                           #2a((0))
                           #(0)
                           #(1)
                           nil
                           1 nil
                           y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max))

;;----------------------------------------------------------------------------------------------------------------------------------
(defun mjr_ode_erk-step-heun-2 (eq x y xdelta y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max)
  "Order 2.
References:
  Butcher (2008); Numerical Methods for Ordinary Differential Equations; p98
Corresponds to the trapezoidal rule."
  (mjr_ode_erk-step-kernel eq x y xdelta
                           #2a((0 0)
                               (1 0))
                           #(0 1)
                           #(1/2 1/2)
                           nil
                           2 nil
                           y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max))

;;----------------------------------------------------------------------------------------------------------------------------------
(defun mjr_ode_erk-step-mid-point-2 (eq x y xdelta y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max)
  "Order 2.
References:
  Butcher (2008); Numerical Methods for Ordinary Differential Equations; p98
Corresponds to the mid-point rule."
  (mjr_ode_erk-step-kernel eq x y xdelta
                           #2a((0   0)
                               (1/2 0))
                           #(0 1/2)
                           #(0 1)
                           nil
                           2 nil
                           y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max))

;;----------------------------------------------------------------------------------------------------------------------------------
(defun mjr_ode_erk-step-runge-kutta-4 (eq x y xdelta y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max)
  "Order 4.
References:
  Kutta (1901); Beitrag zur n\"herungsweisen Integration totaler Differentialgleichungen; Z. Math. Phys. 46; p435-453. 
  Hairer, Norsett & Wanner (2009). Solving Ordinary Differential Equations. I: Nonstiff Problems. p138
In the literature, this method is frequently called 'RK4'.  It is considered by many to be 'the' Runge-Kutta method."
  (mjr_ode_erk-step-kernel eq x y xdelta
                           #2a((0   0   0 0)
                               (1/2 0   0 0)
                               (0   1/2 0 0)
                               (0   0   1 0))
                           #(0   1/2 1/2 1)
                           #(1/6 1/3 1/3 1/6)
                           nil
                           4 nil
                           y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max))

;;----------------------------------------------------------------------------------------------------------------------------------
(defun mjr_ode_erk-step-kutta-three-eight-4 (eq x y xdelta y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max)
  "Order 4.  More precise than RK4.  Sometimes called the '3/8-rule'.
References:
  Kutta (1901), Beitrag zur n\"herungsweisen Integration totaler Differentialgleichungen; Z. Math. Phys. 46; p435-453. 
  Hairer, Norsett & Wanner (2009). Solving Ordinary Differential Equations. I: Nonstiff Problems. p138"
  (mjr_ode_erk-step-kernel eq x y xdelta
                           #2a(( 0   0 0 0)
                               ( 1/3 0 0 0)
                               (-1/3 1 0 0)
                               ( 0  -1 1 0))
                           #(0   1/3 2/3 1)
                           #(1/8 3/8 3/8 1/8)
                           nil
                           4 nil
                           y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max))


;;----------------------------------------------------------------------------------------------------------------------------------
(defun mjr_ode_erk-step-heun-euler-1-2 (eq x y xdelta y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max)
  (mjr_ode_erk-step-kernel eq x y xdelta
                           #2a((0 0)
                               (1 0))
                           #(0 1)
                           #(1 0)
                           #(1/2 1/2)
                           1 2
                           y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max))

;;----------------------------------------------------------------------------------------------------------------------------------
(defun mjr_ode_erk-step-verner-6-5 (eq x y xdelta y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max)
  "Order 6(5).
References:
  J.H. Verner (1978); Explicit Runge-Kutta methods with estimates of the local truncation error; SIAM J. Numer. Anal. 15; pp. 772
  Hairer, Norsett & Wanner (2009). Solving Ordinary Differential Equations. I: Nonstiff Problems. p181"
  (mjr_ode_erk-step-kernel eq x y xdelta
                           #2a((0             0           0             0        0           0 0          0)
                               ( 1/6          0           0             0        0           0 0          0)
                               ( 4/75         16/75       0             0        0           0 0          0)
                               ( 5/6         -8/3         5/2           0        0           0 0          0)
                               (-165/64       55/6       -425/64        85/96    0           0 0          0)
                               ( 12/5        -8           4015/612     -11/36    88/255      0 0          0)
                               (-8263/15000   124/75     -643/680      -81/250   2484/10625  0 0          0)
                               ( 3501/1720   -300/43      297275/52632 -319/2322 24068/84065 0 3850/26703 0))
                           #(0 1/6 4/15 2/3 5/6 1 1/15 1)
                           #(13/160 0 2375/5984 5/16  12/85    3/44 0         0)
                           #(3/40   0 875/2244  23/72 264/1955 0    125/11592 43/616)
                           6 5
                           y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max))

;;----------------------------------------------------------------------------------------------------------------------------------
(defun mjr_ode_erk-step-merson-4-5 (eq x y xdelta y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max)
  "Order 4('5').

The '5' is fake.  This method is really 4th order.

This method is frequently called 'Kutta-Merson' or 'Runge-Kutta-Merson' (as in the NAG library).

References:
  R.H. Merson (1957); An operational method for the study of integration processes; Proc. Symp. Data Processing; pp 110-125
  Butcher (2008); Numerical Methods for Ordinary Differential Equations; p201
  Hairer, Norsett & Wanner (2009). Solving Ordinary Differential Equations. I: Nonstiff Problems. p167"
  (mjr_ode_erk-step-kernel eq x y xdelta
                           #2a(( 0   0    0   0)
                               ( 1/3 0    0   0)
                               ( 1/6 1/6  0   0)
                               ( 1/8 3/8  0   0)
                               ( 1/2 0   -3/2 2))
                           #(0    1/3  1/3 1/2 1)
                           #(1/2  0   -3/2 2   0)
                           #(1/6  0    0   2/3 1/6)
                           4 4
                           y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max))


;;----------------------------------------------------------------------------------------------------------------------------------
(defun mjr_ode_erk-step-zonneveld-4-3 (eq x y xdelta y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max)
  "Order 4(3).

References:
  Hairer, Norsett & Wanner (2009). Solving Ordinary Differential Equations. I: Nonstiff Problems. p167
  Zonneveld (1963); Automatic Integration of Ordinary Differential Equations; Report R743, Mathematisch Centrum"
  (mjr_ode_erk-step-kernel eq x y xdelta
                           #2a(( 0    0    0      0)
                               ( 1/2  0    0      0)
                               ( 0    1/2  0      0)
                               ( 0    0    1      0)
                               ( 5/32 7/32 13/32 -1/32))
                           #(0     1/2  1/2 1     3/4)
                           #( 1/6  1/3  1/3 1/6   0)
                           #(-1/2  7/3  7/3 13/6 -16/3)
                           4 4
                           y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max))

;;----------------------------------------------------------------------------------------------------------------------------------
(defun mjr_ode_erk-step-fehlberg-4-5 (eq x y xdelta y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max)
  "Order 4(5).
This method is not designed for local extrapolation, and performs quite poorly when it is used with local extrapolation.
References:
  Erwin Fehlberg (1969); Klassische Runge-Kutta-Formeln f\"nfter and siebenter Ordnung mit Schrittweiten-Kontrolle; Computing (Arch. Elektron. Rechnen) 4
  Erwin Fehlberg (1969); Low-order Classical Runge-Kutta Formulas with Stepsize Control; NASA Technical Report R-315
  Hairer, Norsett & Wanner (2009). Solving Ordinary Differential Equations. I: Nonstiff Problems. p177"
  (mjr_ode_erk-step-kernel eq x y xdelta
                           #2a((0         0          0          0         0      0)
                               (1/4       0          0          0         0      0)
                               (3/32      9/32       0          0         0      0)
                               (1932/2197 -7200/2197 7296/2197  0         0      0)
                               (439/216   -8         3680/513   -845/4104 0      0)
                               (-8/27     2          -3544/2565 1859/4104 -11/40 0))
                           #(0 1/4 3/8 12/13 1 1/2)
                           #(25/216 0 1408/2565  2197/4104    -1/5  0)
                           #(16/135 0 6656/12825 28561/56430  -9/50 2/55)
                           4 5
                           y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max))

;;----------------------------------------------------------------------------------------------------------------------------------
(defun mjr_ode_erk-step-fehlberg-7-8 (eq x y xdelta y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max)
  "Order 7(8).
This method is not designed for local extrapolation, and performs quite poorly when it is used with local extrapolation.
References:
  Erwin Fehlberg (1972); Classical eight- and lower-order Runge-Kutta-Nystroem formulas with stepsize control for special second-order differential equations; NASA Technical Report M-533
  Butcher (2008); Numerical Methods for Ordinary Differential Equations; p209
  Hairer, Norsett & Wanner (2009). Solving Ordinary Differential Equations. I: Nonstiff Problems. p180"
  (mjr_ode_erk-step-kernel eq x y xdelta
                           #2a(( 0         0    0      0         0          0        0         0      0       0      0 0  0)
                               ( 2/27      0    0      0         0          0        0         0      0       0      0 0  0)
                               ( 1/36      1/12 0      0         0          0        0         0      0       0      0 0  0)
                               ( 1/24      0    1/8    0         0          0        0         0      0       0      0 0  0)
                               ( 5/12      0   -25/16  25/16     0          0        0         0      0       0      0 0  0)
                               ( 1/20      0    0      1/4       1/5        0        0         0      0       0      0 0  0)
                               (-25/108    0    0      125/108  -65/27      125/54   0         0      0       0      0 0  0)
                               ( 31/300    0    0      0         61/225    -2/9      13/900    0      0       0      0 0  0)
                               ( 2         0    0     -53/6      704/45    -107/9    67/90     3      0       0      0 0  0)
                               (-91/108    0    0      23/108   -976/135    311/54  -19/60     17/6  -1/12    0      0 0  0)
                               ( 2383/4100 0    0     -341/164   4496/1025 -301/82   2133/4100 45/82  45/164  18/41  0 0  0)
                               ( 3/205     0    0      0         0         -6/41    -3/205    -3/41   3/41    6/41   0 0  0)
                               (-1777/4100 0    0     -341/164   4496/1025 -289/82   2193/4100 51/82  33/164  12/41  0 1  0))
                           #(0 2/27 1/9 1/6 5/12 1/2 5/6 1/6 2/3 1/3 1 0 1)
                           #(41/840 0 0 0 0 34/105 9/35 9/35 9/280 9/280 41/840 0      0)
                           #(0      0 0 0 0 34/105 9/35 9/35 9/280 9/280 0      41/840 41/840)
                           7 8
                           y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max))

;;----------------------------------------------------------------------------------------------------------------------------------
(defun mjr_ode_erk-step-bogackia-shampine-3-2 (eq x y xdelta y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max)
  "Order 3(2).
References:
  Bogacki & Shampine (1989); A 3(2) pair of Runge-Kutta formulas; Applied Mathematics Letters."
  (mjr_ode_erk-step-kernel eq x y xdelta
                          #2a((0   0   0   0)
                              (1/2 0   0   0)
                              (0   3/4 0   0)
                              (2/9 1/3 4/9 0))
                          #(0 1/2 3/4 1)
                          #(2/9  1/3 4/9 0)
                          #(7/24 1/4 1/3 1/8)
                          3 2
                           y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max))

;;----------------------------------------------------------------------------------------------------------------------------------
(defun mjr_ode_erk-step-cash-karp-5-4 (eq x y xdelta y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max)
  "Order 5(4).
References:
  Cash & Karp(1990);  A variable order Runge-Kutta method for initial value problems with rapidly varying right-hand sides; ACM Transactions on Mathematical Software 16"
  (mjr_ode_erk-step-kernel eq x y xdelta
                          #2a((0          0       0         0            0        0)
                              (1/5        0       0         0            0        0)
                              (3/40       9/40    0         0            0        0)
                              (3/10       -9/10   6/5       0            0        0)
                              (-11/54     5/2     -70/27    35/27        0        0)
                              (1631/55296 175/512 575/13824 44275/110592 253/4096 0))
                          #(0 1/5 3/10 3/5 1 7/8)
                          #(37/378     0 250/621     125/594     0         512/1771)
                          #(2825/27648 0 18575/48384 13525/55296 277/14336 1/4)
                          5 4
                          y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max))

;;----------------------------------------------------------------------------------------------------------------------------------
(defun mjr_ode_erk-step-dormand-prince-5-4 (eq x y xdelta y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max)
  "Order 5(4).
References:
  Dormand & Prince (1980); A family of embedded Runge-Kutta formulae; J. Comput. Appl. Math. 6, no. 1"
  (mjr_ode_erk-step-kernel eq x y xdelta
                           #2a((0          0           0          0        0           0     0)
                               (1/5        0           0          0        0           0     0)
                               (3/40       9/40        0          0        0           0     0)
                               (44/45      -56/15      32/9       0        0           0     0)
                              (19372/6561 -25360/2187 64448/6561 -212/729 0           0     0)
                               (9017/3168  -355/33     46732/5247 49/176   -5103/18656 0     0)
                               (35/384     0           500/1113   125/192  -2187/6784  11/84 0))
                           #(0 1/5 3/10 4/5 8/9 1 1)
                           #(35/384     0  500/1113   125/192 -2187/6784    11/84    0)
                           #(5179/57600 0  7571/16695 393/640 -92097/339200 187/2100 1/40)
                           5 4
                           y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max))

;;----------------------------------------------------------------------------------------------------------------------------------
(defun mjr_ode_erk-step-euler-1-direct (eq x y xdelta y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max)
  "Compute YDELTA directly using Euler's method via formulas in the most simplistic way possible.  Used for testing.

$$\Delta\mathbf{\vec{y}}=\Delta{x}\cdot\vec{\mathbf{f}}(x, \vec{\mathbf{y}})$$"
  (declare (ignore y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max))
;;  (if (or y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max)
;;      (error "mjr_ode_erk-step-euler-1-direct: Method doesn't support 'err' or 'y-delta' tolerances."))
  (values (mapcar (lambda (cur-eq cur-y) (mjr_vec_* xdelta (funcall cur-eq x cur-y)))
                  eq
                  y)
          nil))

;;----------------------------------------------------------------------------------------------------------------------------------
(defun mjr_ode_erk-step-heun-2-direct (eq x y xdelta y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max)
  "Compute YDELTA directly using Heun's method via formulas in the most simplistic way possible.  Used for testing.

$$\Delta\mathbf{\vec{y}}=
\frac{\Delta{x}}{2}[\vec{\mathbf{f}}(x, \vec{\mathbf{y}}) +
\vec{\mathbf{f}}(x+\Delta{x}, \vec{\mathbf{y}} + 
\Delta{x}\cdot\vec{\mathbf{f}}(x, \vec{\mathbf{y}}))]$$"
  (declare (ignore y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max))
;;  (if (or y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max)
;;      (error "mjr_ode_erk-step-euler-1-direct: Method doesn't support 'err' or 'y-delta' tolerances."))
  (values (mapcar (lambda (cur-eq cur-y)
                    (let* ((f1 (funcall cur-eq x            cur-y))
                           (f2 (funcall cur-eq (+ x xdelta) (mjr_vec_+ cur-y (mjr_vec_* xdelta f1)))))
                      (mjr_vec_* (/ xdelta 2) (mjr_vec_+ f1 f2))))
                  eq
                  y)
          nil))

;;----------------------------------------------------------------------------------------------------------------------------------
(defun mjr_ode_erk-step-runge-kutta-4-direct (eq x y xdelta y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max)
  "Compute YDELTA directly using RK4 via formulas in the most simplistic way possible.  Used for testing.

$$ \vec{\mathbf{k}}_i = \vec{\mathbf{f}}(x+a_i\Delta{x}, \vec{\mathbf{y}}+\sum_{j=1}^{i-1}b_{i,j}\vec{\mathbf{k}}_j) $$
$$ \Delta\mathbf{\vec{y}}=\sum_{i=1}^{n}c_i^*\vec{\mathbf{k}}_j $$

$$\begin{array}{rcl}
\vec{\mathbf{k}}_1 & = & \vec{\mathbf{f}}(x,                      \vec{\mathbf{y}})                                             \\
\vec{\mathbf{k}}_2 & = & \vec{\mathbf{f}}(x+\frac{1}{2}\Delta{x}, \vec{\mathbf{y}}+\frac{1}{2}\Delta{x}\vec{\mathbf{k}}_1) \\
\vec{\mathbf{k}}_3 & = & \vec{\mathbf{f}}(x+\frac{1}{2}\Delta{x}, \vec{\mathbf{y}}+\frac{1}{2}\Delta{x}\vec{\mathbf{k}}_2) \\
\vec{\mathbf{k}}_4 & = & \vec{\mathbf{f}}(x+\Delta{x},            \vec{\mathbf{y}}+\Delta{x}\vec{\mathbf{k}}_3)            \\
\end{array}$$
$$\Delta\mathbf{\vec{y}}=\frac{\vec{\mathbf{k}}_1+2\vec{\mathbf{k}}_2+2\vec{\mathbf{k}}_3+\vec{\mathbf{k}}_4}{6}$$"
  (declare (ignore y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max))
;;  (if (or y-err-abs-max y-err-rel-max y-delta-abs-max y-delta-rel-max)
;;      (error "mjr_ode_erk-step-euler-1-direct: Method doesn't support 'err' or 'y-delta' tolerances."))
  (values (mapcar (lambda (cur-eq cur-y)
                    (let* ((xdelta2 (/ xdelta 2))
                           (k1 (funcall cur-eq x             cur-y))
                           (k2 (funcall cur-eq (+ x xdelta2) (mjr_vec_+ cur-y (mjr_vec_* xdelta2 k1))))
                           (k3 (funcall cur-eq (+ x xdelta2) (mjr_vec_+ cur-y (mjr_vec_* xdelta2 k2))))
                           (k4 (funcall cur-eq (+ x xdelta)  (mjr_vec_+ cur-y (mjr_vec_* xdelta  k3)))))
                       (mjr_vec_* xdelta (mjr_vec_+ (mjr_vec_/ k1 6) (mjr_vec_/ k2 3) (mjr_vec_/ k3 3) (mjr_vec_/ k4 6)))))
                  eq
                  y)
          nil))

;;----------------------------------------------------------------------------------------------------------------------------------
(defun mjr_ode_slv-ivp-erk-interval (eq ivy x-min x-max &key
                                         max-itr x-delta-min x-delta-max x-delta-init x-delta-fac-max x-delta-fac-min
                                         y-delta-abs-max y-delta-rel-max y-err-abs-max y-err-rel-max x-delta-fac-fuz
                                         suppress-warnings x-delta-min-rej-err show-progress algorithm return-all-steps
                                         out-y-canonical)
  "Solve one or more systems ODEs in EQ at point X-MAX given initial values X-MIN & IVY using an adaptive Runge-Kutta method.

   Arguments:

    Equations, initial values, and solution interval:
       *  eq ................... Lisp functions implementing $f_{i,j}$
                                 Three possible forms:

                                   * List of functions (each representing a system of ODEs) taking a real and a vector and
                                     returning a vector.
                                     IVY must be a list of vectors
                                   * Single function taking a real and a vector and returning a vector
                                     IVY must be a single vector
                                   * Single function taking two reals and returning a real
                                     IVY must be a single number
                                 DEFAULT: NONE!!
       *  ivy .................. Initial value for y (on the first mesh point)
                                 Three possible forms (form for EQ must be compatible with form of IVY):
                                   * List of vectors
                                     (list (vector y_{1,1}(x_0) ... y_{1,n_1}(x_0) )  ;; n_1 initial values for system 1
                                           (vector y_{2,1}(x_0) ... y_{2,n_2}(x_0) )  ;; n_2 initial values for system 2
                                                                ...
                                           (vector y_{m,1}(x_0) ... y_{m,n_m}(x_0) )) ;; n_m initial values for system m
                                   * Single vector
                                     (vector y_{1,1}(x_0) ... y_{1,n_1}(x_0))
                                   * Single number
                                     y_{1,1}(x_0)
                                 DEFAULT: NONE!!
       *  x-min ................ Initial x value (start of solution interval)
                                 DEFAULT: 0
       *  x-max ................ Final x value (end of solution interval)
                                 DEFAULT: 1
    Step size control:
       *  max-itr .............. Maximum number of iterations
                                 DEFAULT: nil
       *  x-delta-min .......... Minimum value x-delta may have
                                 DEFAULT: (max 1d-15 (/ (- x-max x-min) 100))
       *  x-delta-max .......... Maximum value x-delta may have
                                 DEFAULT: (- x-max x-min)
       *  x-delta-init ......... Initial value for x-delta
                                 DEFAULT: (- x-max x-min)
       *  x-delta-fac-max ...... Maximum factor to expand x-delta
                                 DEFAULT: 2
       *  x-delta-fac-min ...... Minimum factor to expand x-delta
                                 DEFAULT: 1/2
       *  y-delta-abs-max ...... Maximum y-delta to accept
                                 May be a list of vectors, a vector, or a number
       *  y-delta-rel-max ...... Maximum relative y-delta to accept
                                 May be a list of vectors, a vector, or a number
       *  y-err-abs-max ........ Maximum y-err to accept
                                 May be a list of vectors, a vector, or a number
       *  y-err-rel-max ........ Maximum relative y-err to accept
                                 May be a list of vectors, a vector, or a number
       *  x-delta-fac-fuz ...... If x-delta-fac is within x-delta-fac-fuz of 1, then (setq x-delta-fac 1)
                                 DEFAULT: 1/10
       *  x-delta-min-rej-err .. If rejecting a step at x-delta-min, then error out
                                 DEFAULT: nil
    Output Control:
       *  return-all-steps ..... Return data for all steps
                                 DEFAULT:  nil
       *  out-y-canonical ...... Returned y is a list of vectors
                                 If nil, then returned y takes the form of IVY
                                 Only used if RETURN-ALL-STEPS is NIL
                                 DEFAULT: nil
    Miscellaneous:
       *  suppress-warnings .... Don't print warnings
                                  * Rejecting a step at x-delta-min
                                 DEFAULT: nil
       *  show-progress ........ Print progress at each step
                                 DEFAULT: nil
       *  algorithm ............ Select the algorithm to use. This is a function that performs a single forward step.
                                 Possible values for adaptive algorithms:
                                     * mjr_ode_erk-step-heun-euler-1-2        --  order 1(2)   EERK
                                     * mjr_ode_erk-step-zonneveld-4-3         --  order 4(3)   EERK
                                     * mjr_ode_erk-step-merson-4-5            --  order 4('5') EERK
                                     * mjr_ode_erk-step-fehlberg-4-5          --  order 4(5)   EERK
                                     * mjr_ode_erk-step-fehlberg-7-8          --  order 7(8)   EERK
                                     * mjr_ode_erk-step-bogackia-shampine-3-2 --  order 3(2)   EERKLE
                                     * mjr_ode_erk-step-cash-karp-5-4         --  order 5(4)   EERKLE
                                     * mjr_ode_erk-step-dormand-prince-5-4    --  order 5(4)   EERKLE
                                     * mjr_ode_erk-step-verner-6-5            --  order 6(5)   EERKLE
                                 When using an embedded algorithm, error is best controlled via the Y-ERR-REL-MAX or Y-ERR-ABS-MAX
                                 parameters.  The x and y-delta parameters can be used to set bounds, and fine tune step sizes, but
                                 they are generally not required -- except as a safety factor for very large steps.

                                 While it is possible to use an RK method without an embedded error estimate, both performance and
                                 error control can be quite poor.  The following should only be used by experts:
                                     * mjr_ode_erk-step-euler-1               --  order 1      ERK
                                     * mjr_ode_erk-step-euler-1-direct        --  order 1      ERK - as above, but a faster direct formula
                                     * mjr_ode_erk-step-heun-2                --  order 2      ERK
                                     * mjr_ode_erk-step-heun-2-direct         --  order 2      ERK - as above, but a faster direct formula
                                     * mjr_ode_erk-step-mid-point-2           --  order 2      ERK
                                     * mjr_ode_erk-step-runge-kutta-4         --  order 4      ERK
                                     * mjr_ode_erk-step-runge-kutta-4-direct  --  order 4      ERK - as above, but a faster direct formula
                                     * mjr_ode_erk-step-kutta-three-eight-4   --  order 4      ERK
                                 When using a non-embedded algorithm, error can't be controlled via the Y-ERR-REL-MAX or
                                 Y-ERR-ABS-MAX as the method has no good error estimation built in.  When using such RK methods, the
                                 x and y-delta parameters are the most important -- i.e. you must take direct control over the
                                 points used in the solution.  When using a non-embedded method, one should consider using
                                 mjr_ode_slv-ivp-erk-mesh instead of mjr_ode_slv-ivp-erk-interval as the mesh solver allows more
                                 direct control over the solution points.

                                 DEFAULT: #'mjr_ode_erk-step-cash-karp-5-4"
  (flet ((nil-min   (x y)     (or (and x y (min x y)) x y))
         (nil-max   (x y)     (or (and x y (max x y)) x y))
         (fix-bnd   (m v)     (cond ((null m)    (mapcar (lambda (cur-v) (make-array (length cur-v) :initial-element nil)) v))
                                    ((listp m)   m)
                                    ((vectorp m) (list m))
                                    ((numberp m) (mapcar (lambda (cur-v) (make-array (length cur-v) :initial-element m)) v))
                                    ('t          (error "mjr_ode_slv-ivp-erk-interval: Bad type for y error/delta constraint: ~a" m)))))
    (let* ((y-are-num       (numberp ivy))
           (y-are-vec       (vectorp ivy))
           (y-are-lst       (and ivy (listp ivy)))
           (ivy             (cond (y-are-lst ivy)
                                  (y-are-vec (list ivy))
                                  (y-are-num (list (make-array 1 :initial-element ivy)))
                                  ('t        (error "mjr_ode_slv-ivp-erk-interval: Bad type for ivy (must be number, vector, or list)"))))
           (eq              (cond (y-are-lst eq)
                                  (y-are-vec (list eq))
                                  (y-are-num (list (lambda (x y) (make-array 1 :initial-element (funcall eq x (aref y 0))))))))
           (x-min           (or x-min 0))
           (x-max           (or x-max 1))
           (x-range         (- x-max x-min))
           (x-delta-fac-fuz (or x-delta-fac-fuz 1/10))
           (x-delta-fac-max (or x-delta-fac-max 2))
           (x-delta-fac-min (or x-delta-fac-min 1/2))
           (x-delta-max     (or x-delta-max x-range))
           (x-delta-min     (or x-delta-min (max 1d-15 (min x-delta-max (/ x-range 1000)))))
           (algorithm       (or algorithm #'mjr_ode::mjr_ode_erk-step-cash-karp-5-4))
           (y-err-abs-max   (fix-bnd y-err-abs-max ivy))
           (y-err-rel-max   (fix-bnd y-err-rel-max ivy))
           (y-delta-abs-max (fix-bnd y-delta-abs-max ivy))
           (y-delta-rel-max (fix-bnd y-delta-rel-max ivy))
           (x-delta         (or x-delta-init (nil-max x-delta-min (nil-min x-delta-max x-range))))
           (y               (copy-tree ivy))
           (x               x-min)
           (step-data       (loop for itr from 0
                                  initially (if show-progress (format 't "INFO(mjr_ode_slv-ivp-erk-interval): ~5@a ~20@a ~20@a ~%" "itr" "x" "x-delta"))
                                  when (not (zerop itr))
                                  do (loop for (y-delta x-delta-raw-fac) = (multiple-value-list (funcall algorithm eq x y x-delta
                                                                                                         y-err-abs-max y-err-rel-max
                                                                                                         y-delta-abs-max y-delta-rel-max))
                                           for x-delta-fac = (min x-delta-fac-max
                                                                  (max x-delta-fac-min
                                                                       (if (and x-delta-raw-fac (> (abs (- 1 x-delta-raw-fac)) x-delta-fac-fuz))
                                                                           x-delta-raw-fac
                                                                           1)))
                                           for y-bad? = (< x-delta-fac 1)
                                           for x-at-min = (and x-delta-min (<= x-delta x-delta-min))
                                           when (or x-at-min (not y-bad?))
                                           return (progn (if (and y-bad? x-at-min (not suppress-warnings))
                                                             (warn "mjr_ode_slv-ivp-erk-interval: Violated Y-ERR-ABS-MAX or Y-DELTA-ABS-MAX constraint!"))
                                                         (if (and y-bad? x-at-min x-delta-min-rej-err)
                                                             (error "mjr_ode_slv-ivp-erk-interval: Violated Y-ERR-ABS-MAX or Y-DELTA-ABS-MAX constraint!"))
                                                         (setf y      (mapcar (lambda (cur-y cur-y-delta) (mjr_vec_+ cur-y cur-y-delta)) y y-delta)
                                                               x      (+ x x-delta)
                                                               x-delta (min (- x-max x) (nil-min x-delta-max (nil-max x-delta-min (* x-delta x-delta-fac))))))
                                           when show-progress
                                           do (format 't "INFO(mjr_ode_slv-ivp-erk-interval): ~5d ~20f ~20f ~%" itr x x-delta)
                                           do (setf x-delta (nil-min x-delta-max (nil-max x-delta-min (* x-delta x-delta-fac))))
                                           when (and max-itr (> itr max-itr))
                                           return (error "mjr_ode_slv-ivp-erk-interval: Violated MAX-ITR constraint!"))
                                  when return-all-steps
                                  collect (apply #'concatenate 'list (list x) y)
                                  when show-progress
                                  do (format 't "INFO(mjr_ode_slv-ivp-erk-interval): ~5d ~20f ~20f ***~%" itr x x-delta)
                                  until (> x-delta-min (- x-max x)))))
      (if return-all-steps
          (make-array (list (length step-data) (length (first step-data))) :initial-contents step-data)
          (if out-y-canonical
              y
              (cond (y-are-lst y)
                    (y-are-vec (first y))
                    (y-are-num (aref (first y) 0))))))))

;;----------------------------------------------------------------------------------------------------------------------------------
(defun mjr_ode_slv-ivp-erk-mesh (eq ivy mesh &rest rest &key show-progress &allow-other-keys)
  "Approximate solutions to one or more systems of ODEs initial value problems across a mesh of points.
The return value will be a 2D array with a row for each mesh point, and a column for x and all the requested variables.

   Arguments:
    Specifying the mesh (See: MJR_VVEC_KW-NORMALIZE):
       *  POINTS, START, END, STEP, LEN 
    Equations and initial values (See: MJR_ODE_SLV-IVP-ERK-INTERVAL):
       *  EQ & IVY
    Miscellaneous:
       *  SHOW-PROGRESS .. Print progress at each step
                           DEFAULT: nil
    Arguments for MJR_ODE_SLV-IVP-ERK-INTERVAL
       The rest of the keyword arguments are for MJR_ODE_SLV-IVP-ERK-INTERVAL, but some will be suppressed:
         *  :RETURN-ALL-STEPS
         *  :OUT-Y-CANONICAL"
  (let* ((points (mjr_vvec_gen-0sim 'vector mesh)) ;; TODO: Instead of materializing the vector, we could get a forward iterator...
         (len    (length points))
         (kwa    (mjr_util_strip-kwarg rest :strip-list (list :return-all-steps :out-y-canonical)))
         (sol    (loop for i from 0 to (1- len)
                       for xi   = nil then xi+1
                       for xi+1 = (aref points i)
                       for y    = ivy then (apply #'mjr_ode_slv-ivp-erk-interval eq y xi xi+1 :out-y-canonical nil :return-all-steps nil kwa)
                       collect (cond ((numberp y) (list xi+1 y))
                                     ((vectorp y) (concatenate 'list (list xi+1) y))
                                     ('t          (apply #'concatenate 'list (list xi+1) (mapcar (lambda (cy) (if (numberp cy) (list cy) cy)) y))))
                       when show-progress
                       do (format 't "INFO(mjr_ode_slv-ivp-erk-mesh):     ~5d ~20f~%" i xi+1))))
    (make-array (list (length sol) (length (first sol))) :initial-contents sol)))



