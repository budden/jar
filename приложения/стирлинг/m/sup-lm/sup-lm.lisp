;; -*- Mode:Lisp; Syntax:ANSI-Common-LISP; Coding:us-ascii-unix; fill-column:132 -*-
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
; @file      sup-lm.lisp
; @author    Mitch Richling<http://www.mitchr.me>
; @Copyright Copyright 1998,2002,2004,2007,2010,2011 by Mitch Richling.  All rights reserved.
; @Revision  $Revision: 1.68 $
; @SCMdate   $Date: 2015/01/08 20:59:20 $
; @brief     Load the regression tests.@EOL
; @Keywords  lisp load mjr library
; @Std       Common Lisp
; @Tested    2015-01-08: SBCL, clisp, ECL, GCL (BROKEN -- Problem with PATHNAME-MATCH-P)
;
;            COMPILE THE CODE.  
;
;                WARNING: This is a bit of a hack right now and kind of clunky, but it works... ;)
;
;                You don't need to compile the code, but on some lisps this will make things one or two orders of of magnitude
;                faster (10-100x).  In order to compile things, set *LM-TUNE-COMPILE-ON-LOAD* to 'T, and then load this file.  Note
;                that the value of *LM-TUNE-LOAD-EXT* must be set to "*.lisp" as well.  This is the default, but you may have have
;                changed it.  For example, the following has worked on all the LISPs I have tested:
;
;                   (load "sup-lm.lisp")
;                   (setq *lm-tune-compile-on-load* 't)
;                   (setq *lm-tune-load-ext* ".lisp")
;                   (load "sup-lm.lisp")
;
;                Once everything is compiled, you need to change this file so that the compiled files will get loaded.  To do this,
;                change the value of *LM-TUNE-LOAD-EXT* to "".  For the LISPs I have tried, this will cause the implementation to 1)
;                load compiled file if it exists or 2) load the compiled file if it is newer and the source otherwise.  This is all
;                implementation defined behavior, so YMMV.  On most lisps (not ECL), you can alternately set *LM-TUNE-LOAD-EXT* to
;                the extension used for compiled files by your LISP to force it to always load the compiled file.  Note that you
;                probably make sure to set *LM-TUNE-COMPILE-ON-LOAD* to NIL, unless you want to recompile every time.  
;
;                You can use sup-cleanCompile.sh to clean up compiled files for several platforms.
;
;                Thanks to ecovillage AT yandex.ru for pointing out the importance of compilation on many LISP platforms.
;
;            PACKAGE AND SYMBOL NAMING CONVENTIONS
; 
;                The package names all match the following Perl regular expression:
; 
;                    /MJR_([a-z]+)/
; 
;                Where $1 is the "package identifier" and "MJR" is the prefix I use on all of my packages.  
; 
;                All functions and macro names match a similar Perl regular expression:
; 
;                    /MJR_([a-z]+)_([^_]+)
;
;                All parameter names match a similar Perl regular expression, but they have ear-muffs:
; 
;                    /\*MJR_([a-z]+)_([^_]+\*$)
; 
;                In both symbol regular expressions, $1 is the "package identifier" -- i.e. all external names begin with the
;                package name followed by an underscore.  The $2 could be thought of as the "unqualified symbol name" within the
;                package.  Note that external symbols have PRECISELY two underscores when we follow the rules above.  In the code,
;                package symbols are generally referred to in lowercase.
;
;                Using the above conventions, a typical function might be named:  
; 
;                    mjr_package:mjr_package_function-name
; 
;                This redundancy is designed to provide namespace isolation at the language level and at the interactive REPL level
;                even if all the packages are "USED".  This second form of namespace separation makes auto-completion work nicely in
;                clisp, SLIME, and rlwrap.  The biggest disadvantage is that it lengthens function names in source code files.
;
;                Unit test packages have names that look like
;
;                    /MJR_([a-z]+)-TESTS$/
                                        
;                The test will be for a package named MJR_$1 (i.e. with package identifier of $1).
;
;                Unit test files contain tests, data, and functions.  These symbols have names that match the ASSOCIATED package
;                they test.  Ex: :MJR_FOO-TESTS might contain a function named MJR_FOO_BAR-NAIVE.  Note that this function looks
;                like it might belong to the package :MJR_FOO!
;
;                An alternate implementation for a function found in a unit test package have names that end with "-naive[0-9]".
; 
;                Each normal package (i.e. not a unit test package), begins with the same three forms as outlined below:
;
;                    (if (not (find-package :MJR_PKG))
;                        (defpackage :MJR_PKG (:USE :COMMON-LISP :MJR_PKG1 :MJR_PKG2 ... )))
;                    (in-package :MJR_PKG)
;                    (export '(thing1 thing2 ... ))
;
;                In the above, :MJR_PKG is the name of the package -- i.e. the package identifier is "PKG".  The package symbol and
;                dependency symbols should be UPPERCASE.  This structure allows the following code to dynamically discover all
;                package dependencies, and then load the packages in an order consistent with those package dependencies.
;
;           FILE NAME CONVENTIONS
;
;                The package identifier relates to the source code files holding the code and unit tests.  Let $1 be as in the
;                first regexp above, then:
;
;                  -  lib-<PACKAGE_ID>.lisp .............. non-interactive library for PACKAGE_ID
;                  -  use-<PACKAGE_ID>.lisp .............. interactive library for PACKAGE_ID
;                  -  tst-<PACKAGE_ID>.lisp .............. lisp-unit tests for PACKAGE_ID
;                  - utst-<PACKAGE_ID>.lisp .............. u tests for PACKAGE_ID
;                  -  tst-<PACKAGE_ID>-REG-<TAG>.* ....... Regression golden test file for PACKAGE_ID
;                  -  tst-<PACKAGE_ID>-OUT-<TAG>.* ....... Test output file for PACKAGE_ID
;                  -  dev-<PACKAGE_ID>.lisp .............. Development library for PACKAGE_ID
;
;                Other files, not related to packages, include:
;
;                  - sup-<TOOL_ID>.* ..................... support code
;                  - exp-<EXAMP_ID>.lisp ................. example Lisp code
;                  - exp-<EXAMP_ID>-IN-<TAG1>.<EXT1> ..... input file
;                  - exp-<EXAMP_ID>-OUT-<TAG2>.<EXT2> .... output file
;                  - exp-<EXAMP_ID>-AUX-<TAG3>.<EXT3> .... Auxiliary file (ex: a povray file that will render an output file)
;                  - exp-<EXAMP_ID>-ART-<TAG4>.<EXT4> .... An artifact generated from an additional process (ex: image generated by povray)
;

(defvar
    *lm-tune-prt-skip* 
  nil
  "Set to non-NIL to print 'skip' messages")

(defvar
    *lm-tune-prt-load* 
  't
  "Set to non-NIL to print 'load' messages")

(defvar
    *lm-tune-compile-on-load*
  nil
  "Set to non-NIL to compile everything on load")

(defvar
    *lm-tune-load-ext*
  ".lisp"
  "Set the file extension for loaded files.
         * '.lisp' -- Force a source load.  
         * Whatever you lisp uses for compiled files to force compiled file load.  
         * Set it to the empty string to let many LISPs decide automatically to load compiled or source file")

(let* ((lib-directory (make-pathname :name nil :type nil :defaults *load-truename*))
       (dummy (format t "lib-directory = ~S" (setf cl-user::*lib-directory* lib-directory)))
       (load-s-time (float (/ (get-internal-real-time) internal-time-units-per-second)))
       (pkg-loaded  (list :COMMON-LISP))                                                           ;; The list of loaded packages
       (source-file-pattern (concatenate 'string (namestring lib-directory) "*.lisp"))
       (pkg2bfile   (print (loop for pkg-file in (loop
                                                for dir in (directory source-file-pattern)
                                                       ;; package name -> base (no extention file name)
                                                for dirns = (file-namestring dir)
                                                do (format t "dir = ~S" dir)
                                                when (or (pathname-match-p dirns "use-*.lisp")
                                                         (pathname-match-p dirns "lib-*.lisp"))
                                                collect (pathname-name dir))
                          collect (cons (concatenate 'string
                                                     "MJR_"
                                                     (string-upcase (subseq pkg-file 4)))
                                        pkg-file))))
       (pkg2file    (loop for (pkg . bfile) in pkg2bfile                                           ;; package name -> package file assoc list
                          collect (cons pkg (concatenate 'string bfile ".lisp"))))
       (pkg2lfile   (loop for (pkg . bfile) in pkg2bfile                                           ;; package name -> loadable package file name assoc list
                          collect (cons pkg (concatenate 'string bfile *lm-tune-load-ext*))))
       (pkg-list    (loop for thing in pkg2file
                          collect (car thing)))
       (pkg-deps    (loop for pkg-name in pkg-list                                                 ;; Each package name and the package dependencies
                           for fil-name = (cdr (assoc pkg-name pkg2file :test #'string-equal))
                           collect (with-open-file (in-file-stream (merge-pathnames lib-directory fil-name) :direction :input)
                                     (let* ((header-object (read in-file-stream))
                                            (pkg-name      (if (listp header-object)  ;; Package name
                                                               (if (equal 'if (car header-object))
                                                                   (if (listp (third header-object))
                                                                       (if (equal 'defpackage (car (third header-object)))
                                                                           (second (third header-object)))))))
                                            (use-list      (if (listp header-object)  ;; Package dep list
                                                               (if (equal 'if (car header-object))
                                                                   (if (listp (third header-object))
                                                                       (if (equal 'defpackage (car (third header-object)))
                                                                           (if (listp (third (third header-object)))
                                                                               (cdr (third (third header-object))))))))))
                                       (if (and pkg-name use-list)
                                           (cons pkg-name use-list)))))))
       ;;Repeatedly loop through the list of packages loading the ones we can.  Keep going till we load nothing new.
       (loop with num-loaded-this-pass = 0
             for i from 1 upto 10
             do (if *lm-tune-prt-skip* (format 't "LOAD LOOP(~1d) ====================~%" i))
             do (setf num-loaded-this-pass 0)

             (loop for cur-pkg-name in pkg-list
                   for cur-pkg-deps = (cdr (assoc cur-pkg-name pkg-deps :test #'string-equal))

                   ;;do (format 't "CURPK: ~a~%" cur-pkg-name)
                   ;;do (format 't "CURDP: ~a~%" (assoc cur-pkg-name pkg-deps :test #'string-equal))
                   do (if (member cur-pkg-name pkg-loaded :test #'string-equal)
                          (if *lm-tune-prt-skip* (format 't "LOADED(~d):  ~15a :: ~a~%" i cur-pkg-name cur-pkg-deps))
                          (if (every (lambda (x) (member x pkg-loaded :test #'string-equal)) cur-pkg-deps)
                              (let ((start-time (float (/ (get-internal-real-time) internal-time-units-per-second))))
                                (if *lm-tune-prt-load* (format 't "LOADING(~d): ~15a" i cur-pkg-name))
                                (load (merge-pathnames lib-directory (cdr (assoc cur-pkg-name pkg2lfile :test #'string-equal))))
                                (if *lm-tune-compile-on-load*
                                    (compile-file (merge-pathnames lib-directory (cdr (assoc cur-pkg-name pkg2file :test #'string-equal)))))
                                (if *lm-tune-prt-load* (format 't " ~7,2fs :: ~a~%" (- (/ (get-internal-real-time) internal-time-units-per-second) start-time) cur-pkg-deps))
                                (setq pkg-loaded (append pkg-loaded (list cur-pkg-name)))
                                (incf num-loaded-this-pass))
                              (if *lm-tune-prt-skip* (format 't "SKIP(~d):    ~15a :: ~a~%" i cur-pkg-name cur-pkg-deps)))))
             when (= 0 num-loaded-this-pass)
             do (progn (format 't "Loaded ~d of ~d packages in ~,2fs~%" (1- (length pkg-loaded)) (length pkg-list) (- (/ (get-internal-real-time) internal-time-units-per-second) load-s-time))
                       (if (not (= (1- (length pkg-loaded)) (length pkg-list)))
                           (loop for cur-pkg-name in pkg-list
                                 finally (format 't "~%")
                                 initially (format 't "Not Loaded: ")
                                 when (not (member cur-pkg-name pkg-loaded :test #'string-equal))
                                 do (format 't "~a " cur-pkg-name)))
                       (return nil)))
       ;; Go through the list and "use" each package we loaded.
       (loop for pkg in pkg-loaded
             when (equalp 0 (search "use-" (cdr (assoc pkg pkg2file :test #'string-equal))))
             do (use-package (if (symbolp pkg) ;; Convert to symbol to work around bug in ECL
                                 pkg
                                 (intern pkg :keyword)))))
