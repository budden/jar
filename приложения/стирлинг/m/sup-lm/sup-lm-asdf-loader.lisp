
(eval-when (:compile-toplevel :load-toplevel :execute)
  (defpackage :use-all-sup-lm (:use :cl)))

(in-package :use-all-sup-lm)

(setq *read-default-float-format* 'double-float)

; predefine variables so that avoid third loading
(defvar *lm-tune-compile-on-load*)
(defvar *lm-tune-load-ext*)

; load to create packages, and compile
(setf *lm-tune-compile-on-load* t)
(setf *lm-tune-load-ext* ".lisp")
(load (make-pathname :name "sup-lm" :defaults *load-truename*))

; load compiled files
#+lispworks (setf *lm-tune-load-ext* ".ofasl")
#+sbcl (setf *lm-tune-load-ext* ".fasl")
(setf *lm-tune-compile-on-load* nil)
(load (make-pathname :name "sup-lm" :defaults *load-truename*))

(setf MJR_CMP:*mjr_cmp_eps* least-positive-double-float)
