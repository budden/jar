;; -*- Mode:Lisp; Syntax:ANSI-Common-LISP; Coding:us-ascii-unix; fill-column:132 -*-
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; @file      exp-VTKcolorSpace.lisp
;; @author    Mitch Richling <http://www.mitchr.me>
;; @Copyright Copyright 2010,2012 by Mitch Richling.  All rights reserved.
;; @Revision  $Revision: 1.13 $ 
;; @SCMdate   $Date: 2015/01/09 19:35:30 $
;; @brief     Draw the RGB Cube and HSL sphere.@EOL
;; @Keywords  rgb cube hsl sphere
;; @Std       Common Lisp
;;
;;            
;;            

;;----------------------------------------------------------------------------------------------------------------------------------
;; RGB Cube
(mjr_vtk_grid-from-func "exp-VTKcolorSpace-OUT-RGBcube.vtk"
                       :c-func #'mjr_colorizer_i3-rgb-cube
                       :xdat '(:start 0 :end 1 :len 2)
                       :ydat '(:start 0 :end 1 :len 2)
                       :zdat '(:start 0 :end 1 :len 2)
                       :arg-mode :arg-number)

;;----------------------------------------------------------------------------------------------------------------------------------
;; HSL sphere
(let ((d 0.01))
  (mjr_vtk_polydata-from-func-r12-r123 "exp-VTKcolorSpace-OUT-HSLsphere.vtk"
                             (lambda (u v) (vector (* (sin v) (cos u))
                                                   (* (sin v) (sin u))
                                                   (cos v)))
                             :c-func (mjr_util_func-domain-rect-to-unit #'mjr_colorizer_i2-hsl-cone (vector 0 d) (vector (* pi 2) (- pi d)) :arg-number)
                             :udat (list :start 0 :end  (* pi 2) :len 50)
                             :vdat (list :start d :end (- pi d) :len 50)))
