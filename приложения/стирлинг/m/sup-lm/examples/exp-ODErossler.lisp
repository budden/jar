;; -*- Mode:Lisp; Syntax:ANSI-Common-LISP; Coding:us-ascii-unix; fill-column:132 -*-
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; @file      exp-ODErossler.lisp
;; @author    Mitch Richling <http://www.mitchr.me>
;; @Copyright Copyright 1997,1998,2004,2012 by Mitch Richling.  All rights reserved.
;; @Revision  $Revision: 1.14 $ 
;; @SCMdate   $Date: 2015/01/09 19:30:51 $
;; @brief     Compute the Rossler strange attracter.@EOL
;; @Keywords  Rossler strange attracter
;; @Std       Common Lisp
;;
;;            The equations:
;;
;;              $$\begin{array}{l}
;;              \frac{\mathrm{d}x}{\mathrm{d}t} = -y-z    \\
;;              \frac{\mathrm{d}y}{\mathrm{d}t} = x+ay    \\
;;              \frac{\mathrm{d}z}{\mathrm{d}t} = b+z(x-c)\\
;;              \end{array}$$
;;              $$a=1/5, b=1/5, c=57/10$$
;;              $$x(0)=1/10, y(0)=0, y(0)=0$$
;;            

;;----------------------------------------------------------------------------------------------------------------------------------
(time (let* ((ar (mjr_ode_slv-ivp-erk-mesh (list (lambda (tim p)
                                                   (declare (ignore tim))
                                                   (let ((a   0.2d0)
                                                         (b   0.2d0)
                                                         (c   5.7d0)
                                                         (x (aref p 0))
                                                         (y (aref p 1))
                                                         (z (aref p 2)))
                                                     (vector (- (+ y z))             ;; x
                                                             (+ x (* a y))           ;; y
                                                             (+ b (* z (- x c))))))) ;; z
                                           (list #(1/10 0 0))
                                           :algorithm #'mjr_ode_erk-step-heun-euler-1-2
                                           :y-delta-abs-max 1
                                           :start 0 :end 250 :len 10000)))
        (mjr_plot_data :dat ar :datcols '(1 2 3))
        (mjr_vtk_from-gndata "exp-ODErossler-OUT.vtk"
                            :xdat (mjr_arr_get-col ar 1)
                            :ydat (mjr_arr_get-col ar 2)
                            :zdat (mjr_arr_get-col ar 3)
                            :scalar-array (mjr_arr_get-col ar 0)
                            :poly 't)))
