;;; -*- Encoding: utf-8;  Mode: Lisp  -*-
(in-package #:asdf)

(defsystem :sup-lm
  :depends-on (:trivial-garbage :cl-fad)
  :description "It is a fake system. To load sup-lm, load (don't compile!) sup-lm-asdf-loader.lisp in this directory"
  ;:serial t
  ;:components
  ;((:file "sup-lm-asdf-loader")
   ;)
  )
