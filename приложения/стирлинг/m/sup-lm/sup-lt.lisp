;; -*- Mode:Lisp; Syntax:ANSI-Common-LISP; Coding:us-ascii-unix; fill-column:132 -*-
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;; @file      sup-lt.lisp
;; @author    Mitch Richling <http://www.mitchr.me>
;; @Copyright Copyright 1998,2002,2004,2007,2010 by Mitch Richling.  All rights reserved.
;; @Revision  $Revision: 1.18 $ 
;; @SCMdate   $Date: 2015/01/09 22:24:52 $
;; @brief     Load and run unit tests.@EOL
;; @Keywords  lisp unit test lisp-unit library
;; @Std       Common Lisp
;;
;;            Notes here
;;            

;;----------------------------------------------------------------------------------------------------------------------------------
; Load lisp-unit if it has not been loaded already
(let ((go-go-go nil))
  (if (not (find-package :LISP-UNIT))
      (progn (format 't "LISP-UNIT not loaded.~%")
             (format 't "USE: (load \"../olispy/lisp-unit.lisp\")~%"))
      (loop for test-file in (concatenate 'list (directory "tst-*.lisp"))
            do (format 't "~%~%<return> to run tests in: ~a" test-file)
            while (or go-go-go (string-equal (read-line) ""))
            do (handler-case
                   (load test-file)
                 (error (e) (print e)))
            do (format 't "~%DONE with package: ~a" test-file))))



