; -*- Encoding: utf-8; -*-


(in-package :asdf)

(defsystem :m
  :description "m = Моделирование Стирлинга. m = Stirling Modeling" 
  :serial t
  :around-compile (lambda (thunk) (let ((cl:*read-default-float-format* 'cl:double-float)) (funcall thunk)))
  :depends-on (:iterate-keywords :budden-tools :perga
               :buddens-reader :sup-lm
               :budden-adw-plotting
               :heat-exchanger-fluid-stream
               :alexandria
               :piston-drive-kinematics
               :m0)
  ;;; Когда при перекомпиляции cllib происходит ошибка,нужно сделать вручную require cllib ещё раз
  :components
  ((:file "mps.package" :description "mps = Параметры для m. Пакет, который экспортирует параметры машины. 
  mps = m parameters. package which exports engine parameters")
   (:file "mps.h" :description "Переменные и типы для mps. vars, types for mps")
   (:file "mps-utils" :description "Утилиты для параметров машины. tools for mps")
   (:file "mps-default" :description "Конфигурация машина по умолчанию. Default engine configuration")
   (:file "m.package")
   (:file "adiabatny-process" :description "Адиабатный процесс идеального газа. Adiabate process of ideal gas")
   ;(:file "m-types-vars-macros")
   ;(:file "compl-globals")
   ;(:file "m-utils")
   ))
