; -*- Encoding: utf-8; System :substances -*-

(in-package :substance-constants)
(named-readtables:in-readtable :buddens-readtable-a)


(cl:defvar Air (substances::MAKE-SubstanceStruct :Name 'Air))
(cl:defvar Water (substances::MAKE-SubstanceStruct :Name 'Water))
