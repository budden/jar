; -*- Encoding: utf-8; System :substances -*-

(in-package :substances)
(named-readtables:in-readtable :buddens-readtable-a)


(defun substances-constants-package ()
  (find-package :substance-constants))

(defparameter *debug-break-123* t "kill me")

(defmethod print-object ((o SubstanceStruct) out-stream)
  "Печатаем SubstanceStruct как (Substance <имя>), чтобы оно считалось 
 обратно в себя же"
  (perga
    (cond
     (*print-readably*
      (call-next-method o out-stream))
     (t
      (let printed-name
        (let ((*package* (substances-constants-package)))
          ;(with-output-to-string (s) (print o^Name s))
          (prin1-to-string o^Name)
          ))
      (format out-stream "(~S ~A)" '|Substance| printed-name)))))

