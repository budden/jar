; -*- Encoding: utf-8; System :substances -*-

(named-readtables:in-readtable :buddens-readtable-a)

(def-merge-packages::! :substance-constants
                       (:always t)
                       (:use :substances)
                       (:import-from :cl cl:defconstant)
                       (:import-from :named-readtables named-readtables:in-readtable)
                       (:export
                        "substance-constants:Air
                         substance-constants:Water
                        "
                       ))

