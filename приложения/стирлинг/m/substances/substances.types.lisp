; -*- Encoding: utf-8; System :substances -*-

(in-package :substances)
(named-readtables:in-readtable :buddens-readtable-a)

(defstruct SubstanceStruct
  "Химическое вещество или смесь"
  Name
  MolarMass
  )

(defun Substance-inner-readmacro-reader (stream)
  (perga
   (let potential-symbol  
     (let ((sbcl-reader-budden-tools-lispworks:*return-package-and-symbol-name-from-read* t)
           (*package* (substances-constants-package)))
       (read stream)))
   (typecase potential-symbol
     (sbcl-reader-budden-tools-lispworks:potential-symbol
      (let s (find-symbol potential-symbol^CASIFIED-NAME potential-symbol^PACKAGE))
      (etypecase s
        (null
         (simple-reader-error stream "potential symbol ~S not found" potential-symbol))
        (symbol
         (cond
          ((not (boundp s))
           (simple-reader-error stream "symbol ~S is unbound" s))
          ((typep (symbol-value s) '|SubstanceStruct|)
           (symbol-value s))
          (t
           (simple-reader-error stream "symbol-value of ~S must be a SubstanceStruct" s)
           )))))
     (t
      (simple-reader-error stream "symbol expected")))))


(defun Substance-readmacro-reader (stream symbol)
  (declare (ignore symbol))
  (budden-tools:it-is-a-car-symbol-readmacro
   (Substance-inner-readmacro-reader stream)))


(def-symbol-readmacro |Substance| #'Substance-readmacro-reader
                      :documentation "(Substance Air) would read as value of substances:Air")
