; -*- Encoding: utf-8; -*-


(in-package :asdf)

(defsystem :substances
  :description "Химические вещества/рабочие тела" 
  :serial t
  :depends-on (:iterate-keywords :budden-tools :perga :buddens-reader :alexandria)
  :components
  ((:file "substances.package")
   (:file "substance-constants.package")
   (:file "substances.types")
   (:file "substance-constants")
   (:file "substances-functions")
   ))
