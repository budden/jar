; -*- Encoding: utf-8; System :substances -*-

(named-readtables:in-readtable :buddens-readtable-a)

(def-merge-packages::! :substances
                       (:use :cl :budden-tools)
                       (:import-from :alexandria alexandria:simple-reader-error)
                       (:import-from :perga-implementation perga-implementation:perga)
                       ; (:local-nicknames :c :substance-constants)
                       (:custom-token-parsers budden-tools::convert-carat-to-^) 
                       (:export "substances:SubstanceStruct substances:Substance")
                       )

