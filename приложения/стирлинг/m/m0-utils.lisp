; -*- Encoding: utf-8; system  :m0 -*- 


(in-package :m0)
(in-readtable :buddens-readtable-a)

(defun add-vector-to-row-of-matrix (matrix row vector)
  "Добавляет вектор поэлементно к строке матрицы, меняя матрицу"
  (perga
    (:lett lenrow fixnum (array-dimension matrix 1))
    (:lett lenvec fixnum (length vector))
    (assert (= lenvec lenrow))
    (dotimes (i lenrow)
      (incf (aref matrix row i) (aref vector i)))))


(defun extract-row-from-matrix (matrix row)
  "Matrix is in sence of MJR_MAT:mjr_mat_help. row is zero-based. Returns vector"
  (perga
    (:lett len fixnum (array-dimension matrix 1))
    (:lett result array (make-array len))
    (dotimes (i len)
      (setf (aref result i) (aref matrix row i)))
    result))


(defun extract-col-from-matrix (matrix col)
  "Matrix is in sence of MJR_MAT:mjr_mat_help. row is zero-based. Returns vector"
  (perga
    (:lett len fixnum (array-dimension matrix 0))
    (:lett result array (make-array len))
    (dotimes (i len)
      (setf (aref result i) (aref matrix i col)))
    result))


(defun extract-row-from-matrix-as-matrix (matrix row)
  "Matrix is in sence of MJR_MAT:mjr_mat_help. row is zero-based. Returns 1xN matrix"
  (perga
    (:lett len fixnum (array-dimension matrix 1))
    (:lett result array (make-array (list 1 len)))
    (dotimes (i len)
      (setf (aref result 0 i) (aref matrix row i)))
    result))


(defun extract-rows-from-matrix (matrix indices)
  "Extract given rows from a matrix. Indices is a vector or an 1-column matrix"
  (perga
    (:lett ind vector (mjr_mat_m2cv indices))
    (:lett len fixnum (length ind))
    (:lett rows-bound fixnum (- (mjr_mat_rows matrix) 1))
    (:lett cols fixnum (mjr_mat_cols matrix))
    (:lett result array (mjr_mat_make-zero len cols))
    (dotimes (i len)
      (let idx (aref ind i))
      (assert (<= 0 idx rows-bound))
      (dotimes (j cols)
        (setf (aref result i j) (aref matrix idx j)))
      )
    result))

(defun extract-cols-from-matrix (matrix indices)
  "Extract given rows from a matrix. Indices is a vector or an 1-column matrix"
  (perga
    (:lett ind mjr_vec (mjr_mat_m2cv indices))
    (:lett len fixnum (length ind))
    (:lett rows fixnum (mjr_mat_rows matrix))
    (:lett cols-bound fixnum (- (mjr_mat_cols matrix) 1))
    (:lett result mjr_mat (mjr_mat_make-zero rows len))
    (dovector (i idx ind)
      (assert (<= 0 idx cols-bound))
      (dotimes (j rows)
        (setf (aref result j i) (aref matrix j idx)))
      )
    result))

#| (extract-cols-from-matrix (mjr_mat_make-diag '(1 2 3)) #(0 2)) => #2A((1 0) (0 0) (0 3)) |#


(defun extract-elements-from-vector (vector indices)
  "There is a vector and indices (another vector of integers). Extract data from vector with that indices and make fresh vector from them"
  (perga
    (:lett vec mjr_vec (mjr_mat_m2cv vector))
    (:lett ind mjr_vec (mjr_mat_m2cv indices))
    (:lett vec_bound fixnum (- (length vec) 1))
    (:lett ind_len fixnum (length ind))
    (:lett result mjr_vec (make-array (list ind_len)))
    (dotimes (i ind_len)
      (let idx (aref ind i))
      (assert (<= 0 idx vec_bound))
      (setf (aref result i) (aref vec idx))
      )
    result)
  )

; (proclaim '(ftype (function ((or mjr_mat mjr_vec) (or mjr_mat mjr_vec)) mjr_vec) extract-elements-from-vector))
; seem to be ignored... 


(defun extract-col-from-matrix-as-matrix (matrix col)
  "Matrix is in sence of MJR_MAT:mjr_mat_help. row is zero-based. Returns Nx1 matrix"
  (perga
    (:lett len fixnum (array-dimension matrix 0))
    (:lett result array (make-array (list len 1)))
    (dotimes (i len)
      (setf (aref result i 0) (aref matrix i col)))
    result))


(defun integer-range (from to)
  "Returns mjr_vect of from,from+1,...,to"
  (perga
    (let as-list
      (iterate-keywords:iter
        (:for i :from from :to to)
        (:collect i)))
    (coerce as-list 'vector)))


(defun integer-range-3 (from step to)
  "Similar to a:b:c in matlab"
  (perga
    (let as-list
      (iter (:for i :from from :to to :by step)
        (:collect i)))
    (coerce as-list 'vector)))
  


(defun sum-of-matrix-elements (m)
  (perga
    (:lett result array 
     (mjr_mat_*  (mjr_mat_make-const 1 (mjr_mat_rows m) 1.0)
                 m
                 (mjr_vec_make-const (mjr_mat_cols m) 1.0) ; это колонка
                 ))
    (assert (= 1 (mjr_mat_rows result) (mjr_mat_cols result)))
    (aref result 0 0)
    )
  )

(defun sum-of-matrix-columns (m)
  "Возвращает вектор сумм по каждой колонке"
  (mjr_mat_m2cv
   (mjr_mat_transpose
    (mjr_mat_*
     (mjr_mat_make-const 1 (mjr_mat_rows m) 1.0)
     m))))


(defun map-matrix (fn m)
  "Создаёт новую матрицу, применяя функцию поэлементно"
  (mjr_arr:mjr_arr_unary-map2
   (mjr_mat_cv2m (the* mjr_mat m))
   fn))

(defun sum-of-vector-elements (v)
  (let ((res 0)) (dovector (ii x v) (incf res x)) res))


(defun min-and-max-of-sequence (x)
  (values (apply 'min (coerce x 'list))(apply 'max (coerce x 'list))))


#|(defun remove-some-rows-from-matrix (a no-of-rows-to-keep)
  "На входе матрица. Вырезаем ряды чтобы осталось по примерно no-of-rows-to-keep равномерно расположенных рядов. Может вернуть a, если точек и так мало"
  (let* (dimensions no-of-x-s second-dim step new-dimensions result ii)
    (setf dimensions (array-dimensions a))
    (assert (= (length dimensions) 2))
    (setf no-of-x-s (first dimensions))
    (setf second-dim (second dimensions))
    (when (<= no-of-x-s no-of-rows-to-keep)
      (return-from remove-some-rows-from-matrix a))
    (setf step (floor no-of-x-s no-of-rows-to-keep))
    (setf new-dimensions (list no-of-rows-to-keep second-dim))
    (setf result (make-array new-dimensions))
    (dotimes (i no-of-rows-to-keep)
      (setf ii (* step i))
      (dotimes (j second-dim)
        (setf (aref result i j) (aref a ii j))))
    result))


 (defun remove-some-points-from-vector (a no-of-points-to-keep)
  "На входе вектор. Вырезаем точки, чтобы осталось примерно no-of-points-to-keep точек, расположенных равномерно. Может вернуть a, если точек и так мало"
  (let* (dimensions no-of-x-s step new-dimensions result ii)
    (setf dimensions (array-dimensions a))
    (assert (= (length dimensions) 1))
    (setf no-of-x-s (first dimensions))
    (when (<= no-of-x-s no-of-points-to-keep)
      (return-from remove-some-points-from-vector a))
    (setf step (floor no-of-x-s no-of-points-to-keep))
    (setf new-dimensions (list no-of-points-to-keep))
    (setf result (make-array new-dimensions))
    (dotimes (i no-of-points-to-keep)
      (setf ii (* step i))
      (setf (aref result i) (aref a ii)))
    result))|#


(defun matrix-to-double-list (matrix)
  "returns list of matrix rows. Each row is a list of values"
  (perga
    (:lett m mjr_mat matrix)
    (:lett rows fixnum (mjr_mat_rows m))
    (:lett cols fixnum (mjr_mat_cols m))
    (:lett result list nil)
    (dotimes (i rows)
      (:lett row list nil)
      (dotimes (j cols)
        (push [ m i j ] row))
      (push (nreverse row) result)
      )
    (nreverse result)
    ))
    


(defun show-mat (matrix)
  (declare (ignore matrix))
  (error "show-mat not implemented, as we have no ltk on board")
;;  (ltk::start-wish-new-window)
;;  (ltk::init-show-tbl)
;;  (ltk::show-tbl (matrix-to-double-list matrix)
;;                 (mapcar 'budden-tools:str++ (concatenate 'list (integer-range 0 (- (mjr_mat_cols matrix) 1)))))
;;  (ltk::drop-wishes)
;;  (array-dimensions matrix)
  )


(defun nan-p (x)
  (declare (optimize (speed 3) (safety 0) (debug 0) (compilation-speed 0)))
  (let ((z (coerce x 'double-float)))
    (or (> z most-positive-double-float) ; +1D++0, + infinity
        (< z most-negative-double-float) ; -1D++0, - infinity
        (not (or (minusp x) ; "zero" 1D+-0
                 (zerop  x)
                 (plusp  x))))))

(defun vector-check-nan (v)
  (dovector (i val v)
    (assert (not (nan-p val)) () "NAN at ~A in ~S" i v))
  v)

(defun matrix-check-nan (m)
  (dotimes (i (mjr_mat_rows m))
    (dotimes (j (mjr_mat_cols m))
      (assert (not (nan-p (aref m i j))) () "NAN at (~A,~A) in matrix" i j)))
  m)

(defun number-check-nan (x)
  (assert (not (nan-p x)))
  x)


(defun make-zero-vector (n)
  (make-array (list n) :element-type 'double-float :initial-element 0.0))


(defun linear-scalar-fun (x x1 y1 x2 y2) (+ (* (- x x1) (/ (- y2 y1) (- x2 x1))) y1))

(defun half-divide (x field len) "Ищет в массиве field длиной len индекс значения, равного X или меньшего"
  (perga
    (let focus (ceiling (/ len 2)))
    (let trg (if (>= x [ field focus 0 ]) focus 0))
    (loop
     (when (<= focus 2)
       (return 
        (if (>= x [ field (1+ trg) 0 ]) (1+ trg) trg)))
     (setf focus (ceiling (/ focus 2)))
     (let right-bound (min (+ trg focus) (- len 1)))
     (setf trg
           (+ trg (if (> x [ field right-bound 0 ]) focus 0))))))
     

(defun piecewize-linear-scalar-fun (x value-table) 
  "Кусочно-линейная, с линейной экстраполяцией, таблица - матрица 2 колонки N строк. Строка - (x y)"
  (perga
    (:lett vt mjr_mat value-table)
    (:lett rows fixnum (mjr_mat_rows vt))
    (cond
     ((<= x [ vt 1 0 ])
      (linear-scalar-fun x [ vt 0 0 ] [ vt 0 1 ] [ vt 1 0 ] [ vt 1 1 ]))
     ((>= x [ vt (- rows 2) 0 ])
      (:lett i1 fixnum (- rows 2))
      (:lett i2 fixnum (- rows 1))
      (linear-scalar-fun x [ vt i1 0 ] [ vt i1 1 ] [ vt i2 0 ] [ vt i2 1 ])
      )
     (t
      (:lett i fixnum (half-divide x vt rows))
      (:lett xprev number [ vt i 0 ])
      (:lett xnext number [ vt (1+ i) 0 ]) 
      (return-from piecewize-linear-scalar-fun
        (linear-scalar-fun x xprev [ vt i 1 ] xnext [ vt (1+ i) 1 ])))
     )))

(defun piecewize-linear-scalar-fun-nargs (value-table x &rest rest-args) 
  "Кусочно-линейная, с линейной экстраполяцией, таблица - матрица 2 колонки N строк. Строка - (x1 fpart), где fpart(x2,...,xn)=f(x1,x2,...xn), либо fpart = f(x1)"
  (perga function 
    (:lett vt (array * 2) value-table)
    (:lett rows fixnum (array-dimension vt 0))
      ;(:lett x number (car fn-args))
    (flet vor (rownum)
      "Value of row"
      (cond
       ((null rest-args)
        [ vt rownum 1 ])
       (t
        (apply [ vt rownum 1 ] rest-args)))
      )
    (cond
     ((<= x [ vt 1 0 ])
      (linear-scalar-fun x [ vt 0 0 ] (vor 0) [ vt 1 0 ] (vor 1)))
     ((>= x [ vt (- rows 2) 0 ])
      (:lett i1 fixnum (- rows 2))
      (:lett i2 fixnum (- rows 1))
      (linear-scalar-fun x [ vt i1 0 ] (vor i1) [ vt i2 0 ] (vor i2))
      )
     (t
      (:lett i fixnum (half-divide x vt rows))
      (:lett xprev number [ vt i 0 ])
      (:lett xnext number [ vt (1+ i) 0 ]) 
      (return-from function
        (linear-scalar-fun x xprev (vor i) xnext (vor (1+ i)))))
     )))


#|(defun piecewize-linear-scalar-fun (x value-table) 
  "Кусочно-линейная, с линейной экстраполяцией, таблица - матрица 2 колонки N строк. Строка - (x y). Очень медленная, надо ускорить"
  (perga
    (:lett vt mjr_mat value-table)
    (:lett rows fixnum (mjr_mat_rows vt))
    (cond
     ((<= x [ vt 1 0 ])
      (linear-scalar-fun x [ vt 0 0 ] [ vt 0 1 ] [ vt 1 0 ] [ vt 1 1 ]))
     ((>= x [ vt (- rows 2) 0 ])
      (:lett i1 fixnum (- rows 2))
      (:lett i2 fixnum (- rows 1))
      (linear-scalar-fun x [ vt i1 0 ] [ vt i1 1 ] [ vt i2 0 ] [ vt i2 1 ])
      )
     (t
      (:lett xprev number [ vt 1 0 ])
      (:lett xnext number 0) 
      (do-for (i 2 (- rows 2))
        (setf xnext [ vt i 0 ])
        (when (<= xprev x xnext)
          (return-from piecewize-linear-scalar-fun
            (linear-scalar-fun x xprev [ vt (- i 1) 1 ] xnext [ vt i 1 ])))
        (setf xprev xnext))
      (error "piecewize-linear-scalar-fun: unknown x = ~S" x)
      ))))|#

(defun make-piecewize-linear-scalar-fun (value-table &key allow-extrapolation (pseudo-name "Untitled piecewize linear scalar fun"))
  "Возвращает функцию одного аргумента, представляющую из себя кусочно - линейную скалярную ф-ю, по данным. Создаёт ссылку на данные - их нельзя разрушать. Данные должны быть упорядочены по x и их должно быть не менее 2 штук. Если allow-extrapolation, то за границами использует экстраполяцию из крайнего отрезка"
  (apply 'make-piecewize-linear-fun-inner value-table (budden-tools:dispatch-keyargs-simple allow-extrapolation pseudo-name))
  )

(defun make-piecewize-linear-fun-inner (value-table &key (no-of-args 1) allow-extrapolation pseudo-name)
  "Value-table может содержать значения (тогда no-of-args=1) или функции (тогда no-of-args>1). Called from make-piecewize-linear-scalar-fun"
  (perga
    (:lett vt mjr_mat value-table)
    (:lett rows fixnum (mjr_mat_rows value-table))
    (assert (> rows 1))
    (assert (= (mjr_mat_cols value-table) 2))
    (let current-x nil)
    (let min-x nil)
    (dotimes (i rows)
     (:lett new-x number [ vt i 0 ])
     (cond 
       ((null current-x) (setf min-x new-x))
       (t 
        (assert (> new-x current-x) () "Value table must be ordered at point ~A" new-x)))
     (setf current-x new-x)
     )
    (let max-x current-x)
    (flet check-range (x)
      (typecase allow-extrapolation
        (null 
         (assert (<= min-x x max-x) () "Extrapolation is forgiven for ~A" pseudo-name))
        ((vector * 2)
         (let ((new-min-x [ allow-extrapolation 0 ])
               (new-max-x [ allow-extrapolation 1 ]))
           (assert (<= new-min-x x new-max-x) () "Value out of allowed extrapolation range for ~A" pseudo-name)
           ))))      
    (ecase no-of-args
      (1
       (lambda (x) 
         (check-range x)
         (piecewize-linear-scalar-fun x vt)))
      (2
       (lambda (x y)
         (check-range x)
         (piecewize-linear-scalar-fun-nargs vt x y)
         ))
      (3
       (lambda (x &rest more)
         (check-range x)
         (apply 'piecewize-linear-scalar-fun-nargs vt x more)
         ))
      )))


(def-trivial-test::! make-piecewize-linear-scalar-fun.1
                     (perga
                       (let fn 
                         (make-piecewize-linear-scalar-fun #2A((1 1) (2 2) (3 3)) :allow-extrapolation t))
                       (and
                        (= (funcall fn 1.5) 1.5)
                        (= (funcall fn 0) 0)
                        (= (funcall fn 4) 4))
                       )
                     t)


(def-trivial-test::! make-piecewize-linear-fun-inner.1
                     (perga
                       (let fn (make-piecewize-linear-fun-inner (make-array '(3 2) :initial-contents `((-1 1-) (0 identity) (1 1+))) :no-of-args 2))
                       (list (funcall fn 0 5)
                             (funcall fn 1 5)
                             (funcall fn -1 5)
                             (funcall fn 0.5 0)
                             (funcall fn 0.5 0.5)
                             ))
                     '(5 6 4 0.5 1))
                             

(defun make-2d-interpolation-by-table (table function-name)
  "Для каждой строчки таблицы создаём интерполирующую ф-ю. По второму измерению создаём временную интерполирующую ф-ю и вызываем её. Пример:

  (defparameter *So0003*
  '((\"\"
          30    40   50   60   70   80  100 150  200)
    (0.6  0.65 0.80  1.0  1.4  2.0  3.0 4.0 5.0  6.0)
    (0.8  0.95 1.2   1.5  1.9  2.7  4.0 5.0 6.0  8.0)
    (1.0  1.2  1.5   1.9  2.4  3.3  4.5 6.0 7.0  9.0)
    (1.2  1.4  1.7   2.2  2.6  3.7  5.0 6.5 8.0 10.0)))

  (defparameter *So0003fn* (make-2d-interpolation-by-table *So0003* \"\"))

  (funcall *So0003fn* 1.1 175)
  = 8.5

  "
  (perga-implementation:perga
    (let RowsPlus1 (length table))
    (let ColsPlus1 (length (first table)))
    (let RowTitles (mapcar 'first (cdr table)))
    (let ColTitles (cdr (first table)))
    (let vt (make-array (list (- RowsPlus1 1) 2)))
    (let i 0)
    (dolist (row (cdr table))
      (unless (= (length row) ColsPlus1)
        (error
         "Должно быть одинаковое количество столбцов во всех строках"))
      (let current-x (elt RowTitles i))
      (setf (aref vt i 0) current-x)
      (let vti1 
        (make-array (list (- ColsPlus1 1) 2)))
      (let j 0)
      (dolist (this-value (cdr row))
        (setf (aref vti1 j 0) (elt ColTitles j))
        (setf (aref vti1 j 1) this-value)
        (incf j))
      (setf (aref vt i 1)
            (make-piecewize-linear-scalar-fun
             vti1 :pseudo-name (str++ function-name "(" current-x ")")
             ))
      (incf i))
    (make-piecewize-linear-fun-inner
     vt :no-of-args 2
     :pseudo-name function-name)))





(defun quadr (x &key xi yi xi-1 yi-1 xi+1 yi+1) "Парабола по трём точкам"
  (let* ((a2 (- (/ (- yi+1 yi-1) (* (- xi+1 xi-1) (- xi+1 xi)))
		(/ (- yi yi-1) (* (- xi xi-1) (- xi+1 xi)))))
	 (a1 (- (/ (- yi yi-1) (- xi xi-1)) (* a2 (+ xi xi-1))))
	 (a0 (- yi-1 (* a1 xi-1) (* a2 xi-1 xi-1))))
    (+ a0 (* a1 x) (* a2 x x))))


(defun piecewize-quadratic-scalar-fun (x value-table) 
  "Кусочно-квадратичная, наружу экстраполируются крайние параболы, таблица - матрица 2 колонки N строк. Строка - (x y)"
  (perga
    (:lett vt mjr_mat value-table)
    (:lett rows fixnum (mjr_mat_rows vt))
    (cond
     ((< rows 3)
      (piecewize-linear-scalar-fun x vt))
     #|((<= x [ vt 1 0 ])
      (linear-scalar-fun x [ vt 0 0 ] [ vt 0 1 ] [ vt 1 0 ] [ vt 1 1 ]))
     ((>= x [ vt (- rows 2) 0 ])
      (:lett i1 fixnum (- rows 2))
      (:lett i2 fixnum (- rows 1))
      (linear-scalar-fun x [ vt i1 0 ] [ vt i1 1 ] [ vt i2 0 ] [ vt i2 1 ])
      )|#
     (t
      (:lett i1 fixnum (half-divide x vt rows))
      (:lett i2 fixnum
       (cond
        ((< i1 1) 1)
        ((> i1 (- rows 2)) (- rows 2))
        (t i1)
        ))
      (print `(,x -> ,i2))
      (return-from piecewize-quadratic-scalar-fun
        (quadr x :xi-1 [ vt (1- i2) 0 ] :xi [ vt i2 0 ] :xi+1 [ vt (1+ i2) 0 ]
               :yi-1 [ vt (1- i2) 1 ] :yi [ vt i2 1 ] :yi+1 [ vt (1+ i2) 1 ])))
     )))


#|

 (plot-scalar-fn (lambda (x) (M0::piecewize-quadratic-scalar-fun x #2A((-1 0) (0 0) (1 1) (2 4)))) -2.0 2.0 :x-data-interval (lambda (i &rest ignore) (elt #(-2 -1 0 1 2) i)))

|#


(defun DONT-show-expr-t (expr)
  (declare (ignore expr)))

(defmacro show-expr-t (expr)
  `(show-expr ,expr *standard-output*))

(defmacro check-slots-are-dbl (instance slots)
  "Здесь могут быть тормоза, поскольку слоты выявляются в runtime"
  (budden-tools:with-gensyms (x)
    `(perga
       (:@ with-slots ,slots ,instance)
       (dolist (,x (list ,@slots))
         (assert
             (typep ,x 'dbl) ()
           "m0::check-slots-are-dbl : в ~S должны быть заданы поля ~A типа dbl"
           ,instance
           ;(cons-to-source:pds `("," ,',@slots)) - неверно!
           (cons-to-source:pds `("," ,@',slots))
           )))))


(defmacro check-slots-are-null (instance slots)
  (budden-tools:with-gensyms (x)
    `(perga
       (:@ with-slots ,slots ,instance)
       (dolist (,x (list ,@slots))
         (assert
             (null ,x) ()
           "m0::check-slots-are-null : в ~S поля ~A не используюстя и должны быть nil"
           ,instance
           (cons-to-source:pds `("," ,@',slots)))))))



(defparameter *package-documentation* 
  "

   идиомы для замены: 
  (a:b) - (integer-range a b)
  sum(a) - sum-of-matrix-elements , sum-of-vector-elements

  операции над векторами 
  v1.*v2 - mjr_vec_*
  v1+v2  - mjr_vec_+

  обращение к элементам
  m(i,j) = [ m i j ]
  v(i) = [ v i ]
  m(индексы,:) = extract-rows-from-matrix
  m(:,индексы) = extract-cols-from-matrix, индексы нужно сделать вектором
  
  figure,plot - см plot-adw

  a:b:c = integer-range-3 a b c

  вывод матрицы в грид - show-mat

  столбец из построчных сумм (результат - матрица) = (mjr_mat_* m (mjr_vec_make-const (mjr_mat_cols m) 1.0))

  строка из постолбцовых сумм (результат - матрица) = (mjr_mat_* (mjr_mat_transpose (mjr_vec_make-const (mjr_mat_rows aaa) 1.0)) aaa)


  ")


