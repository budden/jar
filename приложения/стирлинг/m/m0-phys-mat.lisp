; -*- Encoding: utf-8; -*-

(in-package :m0)
(in-readtable :buddens-readtable-a)
(eval-when (:compile-toplevel)
  (assert (eq *READ-DEFAULT-FLOAT-FORMAT* 'double-float)))

(defun CircleArea (d)
  (* pi d d 1/4))

(defun CircleLength (d)
  (* pi d))

(defun SphereSurfaceAreaByRadius (r)
  (* 4 pi (expt r 2)))

(defun SphereVolumeByRadius (r)
  (* 4/3 pi (expt r 3)))


(defvar Vshara 'SphereVolumeByRadius "Для перехода от Mathematica")
(defvar Sshara 'SphereSurfaceAreaByRadius "Для перехода от Mathematica")


(defun carnot (th tk)
  (float (/ (- th tk) th)))

(defun carnotc (thc tkc)
  (carnot ([Celsius] thc) ([Celsius] tkc)))


(defun RadiationalHeatExchangeK (T1 e1 T2 e2)
  (/
   (* 5.67e-8
      (- (expt T1 4)
         (expt T2 4)))
   (+ (/ e1) (/ e2) -1)
   ))



(def-trivial-test::! RadiationalHeatExchangeK.1
                     (RadiationalHeatExchangeK 800 0.9 400 1)
                     19595.519999999997)


(def-trivial-test::! RadiationalHeatExchangeK.2
                     (RadiationalHeatExchangeK 300 0.8 400 0.7)
                     -591.127659574468)

(def-trivial-test::! RadiationalHeatExchangeK.3
                     (RadiationalHeatExchangeK 300 0.8 300 0.7)
                     0)



(defun WallThickness (pressure diam creep)
  (/ (* pressure diam)
     (* 2 (* 0.5 creep))))


(defconstant +GravityG+ 9.80665 "Ускорение свободного падения, g")


(defun approx-equal (x y relative-delta)
  "FIXME - перенести в def-trivial-test"
  (cond
   ((and (= x 0) (= y 0))
    t)
   ((= x 0)
    (approx-equal y x relative-delta))
   (t
    (<= (- 1 relative-delta)
        (/ x y)
        (+ 1 relative-delta)))))