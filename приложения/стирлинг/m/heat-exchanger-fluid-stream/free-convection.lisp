; -*- Encoding: utf-8;  System :heat-exchanger-fluid-stream -*-

; свободная конвекция

(in-package :free-convection)
(in-readtable :buddens-readtable-a)

(defmethod CalcHeatExchanger ((hx OutsideOfVerticalPipeInUnlimitedSpace))
  (perga
   (:lett hx OutsideOfVerticalPipeInUnlimitedSpace hx)
   (:lett h dbl hx^Length)
   (assert (null hx^LengthNuFactorMM90) () "LengthNuFactorMM90 не должен быть задан для ~S" hx)
   (setf hx^HydrDiam hx^Length)
   (setf hx^HeatExchangeArea (* h (CircleLength hx^PipeDiameter)))
   (setf hx^FluidVolume nil)
   (setf hx^FlowArea nil)
   hx))

(defmethod CalcHeatExchanger ((hx OutsideOfHorizontalPipeInUnlimitedSpace))
  (perga
   (:lett hx OutsideOfHorizontalPipeInUnlimitedSpace hx)
   (:lett D dbl hx^D)
   (assert (null hx^Length) () "Длина не должна быть задана для ~S, она условно считается равной 1м" hx)
   (:lett LL dbl 1.0)
   (setf hx^HydrDiam D)
   (setf hx^HeatExchangeArea (* LL (CircleLength D)))
   (setf hx^FluidVolume nil)
   (setf hx^FlowArea nil)
   hx))


(defmethod CalcHeatExchanger ((hx VerticalPlateInUnlimitedSpace))
  (perga
    (:lett hx VerticalPlateInUnlimitedSpace hx)
    (check-slots-are-dbl hx (H L))
    (check-slots-are-null hx (Length))
    (:@ with-slots (H L) hx)
    (setf hx^HydrDiam H)
    (setf hx^HeatExchangeArea (* 2 H L))
    (setf hx^FluidVolume nil)
    (setf hx^FlowArea nil)
    hx))


(defmethod CalcHeatExchanger ((hx ПрослойкаМеждуПараллельнымиГоризонтальнымиПоверхностями))
  (perga
    (:lett hx ПрослойкаМеждуПараллельнымиГоризонтальнымиПоверхностями hx)
    (check-slots-are-dbl hx (Delta PlateArea))
    (check-slots-are-null hx (Length HeatExchangeArea))
    (:@ with-slots (Delta PlateArea) hx)
    (setf hx^HydrDiam Delta)
    (setf hx^FluidVolume (* PlateArea Delta)) ; в общем-то не нужен.
    (setf hx^HeatExchangeArea PlateArea)
    (setf hx^FlowArea nil)
    hx))

#|(defmethod CalcHeatExchanger ((hx ВертикальныеРёбраНаГоризонтальномОсновании))
  (perga
    (:lett hx ВертикальныеРёбраНаГоризонтальномОсновании hx)
    (assert (null hx^Length) () "Длина не задаётся для данного теплообменника")
    (:@ with-slots (S Delta H L N) hx)
    (dolist (x (list S Delta H L N))
      (assert
          (numberp x) () "В ~S Должны быть заданы S,Delta,H,L,N" hx))
    (setf hx^HydrDiam L)
    (setf hx^HeatExchangeArea
          (+ (* 2 L S N) ; основание
             (* 2 N H 2 L) ; рёбра
             (* Delta (+ N 1) 2 L); лезвия рёбер
             (* Delta (+ N 1) 2 H) ; бока рёбер
             ))
           ))|#


(defmethod CalcHeatExchanger ((hx ПараллельныеКруглыеДискиСОдинаковойТемпературой))
  (perga
    (:lett hx ПараллельныеКруглыеДискиСОдинаковойТемпературой hx)
    (assert (null hx^Length) () "Длина не задаётся для данного теплообменника")
    (:@ with-slots (S D) hx)
    (dolist (x (list S D))
      (assert
          (numberp x) () "В ~S Должны быть заданы S,D" hx))
    (setf hx^HydrDiam S)
    (setf hx^HeatExchangeArea (* 2 (CircleArea D)))
    (setf hx^FluidVolume nil)
    (setf hx^FlowArea nil)
    hx
    ))

(defmethod CalcHeatExchanger ((hx ПараллельныеПластиныСОдинаковойТемпературой))
  (perga
    (:lett hx ПараллельныеПластиныСОдинаковойТемпературой hx)
    (assert (null hx^Length) () "Длина не задаётся для данного теплообменника")
    (:@ with-slots (S L) hx)
    (dolist (x (list S L))
      (assert
          (numberp x) () "В ~S Должны быть заданы S,L" hx))
    (setf hx^HydrDiam S)
    (setf hx^HeatExchangeArea (* 2
                                 1.0 ; условная длина по горизонтали равна 1м
                                 L))
    (setf hx^FluidVolume nil)
    (setf hx^FlowArea nil)
    hx
    ))
  

#| не будем доделывать, т.к неподходящий диапазон 
   размеров рёбер. Cтр 238. 
 (defun NuКонвекции (hx)
  (perga
    (:lett hx ВертикальныеРёбраНаГоризонтальномОсновании hx)
    (let H hx^H)
    (let L hx^L)
    (let S hx^S)
    (let N hx^N)
    (*
     5.22d-3
     (expt (/ H L) 0.656)
     (expt (/ S L) 0.412)
     (expt (* S N
   |#


(defun Gr (hx str DeltaT)
  "Gr, Число Грасгофа"
  (perga
    (:lett hx HxBase hx)
    (:lett str FluidStreamWithLiquidProperties str)
    (* 9.81 str^ThermalExpansionCoefficient DeltaT (expt hx^HydrDiam 3) (/ (expt str^nu 2)))
    ))


(defun Ra (hx str DeltaT)
  "Ra, Число Релея"
  (* (Gr hx str DeltaT)
     (strPr str)))


(defmethod Nu ((hx ПараллельныеКруглыеДискиСОдинаковойТемпературой) str DeltaT)
  (perga
    (:lett hx ПараллельныеКруглыеДискиСОдинаковойТемпературой hx)
    (:@ with-slots (S D) hx)
    (:lett F1 dbl (coerce
                   (* S (/ D) (Ra hx str DeltaT))
                   'double-float))
    (show-expr F1)
    (cond
     ((<= 1 F1 1e4)
      (* (/ 1 6 pi) F1 (expt (- 1 (exp (/ -25.3 F1))) 3/4)))
     (t
      (error "Не знаю, как посчитать такую конвекцию")))))


(defmethod Nu ((hx ПараллельныеПластиныСОдинаковойТемпературой) str DeltaT)
  "См. примечание к ПараллельныеПластиныСОдинаковойТемпературой. Также мы никак не проявляем то, что 
  'оптимальное расстояние между пластинами, при котором передаваемый поток максимален, определяется из соотношения
   s/L * Ra = 46'. 
   Не совсем ясно, о какой максимальности идёт речь - для данного количества металла?"
  (perga
    (:lett hx ПараллельныеПластиныСОдинаковойТемпературой hx)
    (:@ with-slots (S L) hx)
    (:lett F1 dbl (coerce
                   (* S (/ L) (Ra hx str DeltaT))
                   'double-float))
    (show-expr F1)
    (cond
     ((<= 2e-1 F1 1e5)
      (* 1/24 F1 (expt (- 1 (exp (/ -35 F1))) 3/4)))
     (t
      (error "Не знаю, как посчитать такую конвекцию")))))



(defmethod Nu ((hx ПрослойкаМеждуПараллельнымиГоризонтальнымиПоверхностями) str DeltaT)
  "Поскольку всё должно считаться через Nu, придётся вернуть Nu. Но оно вроде совпадает с epsilon_k"
  (perga
    (:lett hx ПрослойкаМеждуПараллельнымиГоризонтальнымиПоверхностями hx)
    (:lett Ra dbl (coerce (Ra hx str DeltaT) 'dbl))
    (:lett epsilon_k dbl
     (cond
      ((<= Ra 1e3) 1.0)
      ((<= Ra 1e6)
       (* 0.105 (expt Ra 0.3)))
      ((<= Ra 1e10)
       (* 0.4 (expt Ra 0.2)))
      (t
       (error "Не знаю, как посчитать такую конвекцию"))))
    #|(:lett lambda-ek dbl (* str^lambda epsilon_k))

    ; решаем простую задачу теплопередачи через прослойку
    (:lett alpha dbl (/ lambda-ek hx^HydrDiam))
    (* alpha hx^HydrDiam
       Nu = alpha * l / lambda |#
    epsilon_k ; всё сократилось, Nu = epsilon_k
    ))
       

(defun ConvectionNuVerticalXInUnlimitedSpace (ra)
  "Считает Nu по Ra для потоков газа"
  (cond
   ((<= 1e3 ra 1e9) ; ламинарный режим
    (* 0.76 (expt ra 0.25)
       #| (expt (/ Prж Prс) 0.25) |#; не учитываем изменение Pr, поэтому ограничиваемся воздушными потоками
       ))
   ((>= ra 1e9) ; турбулентный режим
    (* 0.15 (expt ra 0.33)
       #| (expt (/ Prж Prс) 0.25) |#; не учитываем изменение Pr, поэтому ограничиваемся воздушными потоками
       ))
   (t (error "Слишком малое ra - не могу посчитать Nu для такой конвекции"))
   )) 


(defgeneric Nu (hx str DeltaT))

(defmethod Nu ((hx OutsideOfVerticalPipeInUnlimitedSpace) (str AirStream1) DeltaT)
  "[МихеевМихеева], стр. 96, ф-лы 3-42,3-43]"
  (perga
    (let ra (Ra hx str DeltaT))
    (ConvectionNuVerticalXInUnlimitedSpace ra)
    ))

(defmethod Nu ((hx VerticalPlateInUnlimitedSpace) (str AirStream1) DeltaT)
  "См. примечание к VerticalPlateInUnlimitedSpace"
  (perga
    (let ra (Ra hx str DeltaT))
    (ConvectionNuVerticalXInUnlimitedSpace ra)
    ))
   
(defmethod Nu ((hx OutsideOfHorizontalPipeInUnlimitedSpace) (str AirStream1) DeltaT)
  "[МихеевМихеева], [АвчуховПаюсте] стр. 58"
  (perga
    (let ra (Ra hx str DeltaT))
    (cond
     ((<= 1e-3 ra 1e3) ; ламинарный режим 1, [АвчуховПаюсте] стр. 58
      (* 1.18 (expt ra 0.125))
      )
     ((<= 1e3 ra 1e8) ; ламинарный режим
      (* 0.5 (expt ra 0.25)
         #| (expt (/ Prж Prс) 0.25) |#; не учитываем изменение Pr, поэтому ограничиваемся воздушными потоками
         ))
     ((<= 1e8 ra 1e13) ; турбулентный режим
      (* 0.135 (expt ra 0.33)
         #| (expt (/ Prж Prс) 0.25) |#; не учитываем изменение Pr, поэтому ограничиваемся воздушными потоками
         ))
     (t (error "Не могу посчитать Nu для такой конвекции"))
     )))
  


(defun Alpha (hx str DeltaT)
  (/ (* (Nu hx str DeltaT) str^lambda) hx^HydrDiam))


(defun FreeConvectionHeatExchangePower (hx str DeltaT)
  (* DeltaT (Alpha hx str DeltaT)
     hx^HeatExchangeArea))



(defun TrivialLinearBuoyancyPressure (str DeltaT DeltaH)
  "Вычисляет давление архимедовой силы, пренебрегая изменение плотности газа с высотой и т.п."
  (perga
    (:lett str FluidStreamWithLiquidProperties str)
    (* +GravityG+ DeltaH str^rho str^ThermalExpansionCoefficient DeltaT)))


(def-trivial-test::! Buoyancy.1
                     (perga
                       (:lett str AirStream1 (MAKE-AirStream1 :Pressure [Bar] :Temperature 300.0))
                       (CalcStream str)
                       (let res (TrivialLinearBuoyancyPressure str 6 1.0))
                       (<= 0.228 res 0.229)
                       )
                     t)


