; -*- Encoding: utf-8;  System :heat-exchanger-fluid-stream -*-

; регенератор

(named-readtables:in-readtable :buddens-readtable-a)

(def-merge-packages::! :kays-london-periodic-flow
                       (:documentation "Анализ регенератора, согласно [KaysLondon], стр 30. Здесь str - это по определению будет горячий поток")
                       (:always t)
                       (:use :cl :budden-tools
                        :m0
                        :heat-exchanger-fluid-stream-literature
                        :fluid-stream
                        :heat-exchanger
                        :hx-and-str
                        :physical-units
                        )
                       (:local-nicknames :hx :heat-exchanger :str :fluid-stream
                        :kl :kays-london-notation)
                       (:custom-token-parsers budden-tools::convert-carat-to-^) 
                       (:import-from :perga-implementation
                        perga-implementation:perga)
                       (:export
 "
 KAYS-LONDON-PERIODIC-FLOW::EpsilonPeriodicFlow_1_infinity
 "))

(in-package :kays-london-periodic-flow)


;;; 

(defparameter *FnKaysLondonTbl2-10-infinity*
  (make-piecewize-linear-scalar-fun
   #2A((0.0 0.0)
       (0.5 0.333)
       (1.0 0.500)
       (1.5 0.600)
       (2.0 0.667)
       (2.5 0.714)
       (3.0 0.750)
       (3.5 0.778)
       (4.0 0.800)
       (4.5 0.818)
       (5.0 0.833)
       (5.5 0.846)
       (6.0 0.857)
       (6.5 0.867)
       (7.0 0.875)
       (7.5 0.882)
       (8.0 0.889)
       (8.5 0.895)
       (9.0 0.900)
       (9.5 0.905)
       (10.0 0.909)
       (20.0 0.952)
       (50.0 0.980)
       (90.0 0.989)
       (100.0 0.990)
       (500.0 0.998)
       (1e300 1.0) ; added by budden to allow for some out-of-range cases
       )
   :pseudo-name "*FnKaysLondonTbl2-10-infinity*")
  "См. также другие графики Epsilon-ntu в файле N_tu-epsilon.lisp")

(defun EpsilonPeriodicFlow_1_infinity (hx str)
  "Cmin/Cmax=1, Cr/Cmin=infinity. [KaysLondon], Table 2-10, p 57"
  (perga
    (let Ntuo (kl:N_tu_o hx str))
    (funcall *FnKaysLondonTbl2-10-infinity* Ntuo)))








