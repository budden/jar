; -*- Encoding: utf-8; -*-

(in-package :asdf)

(defsystem :heat-exchanger-fluid-stream
  :description "Теплообменники и потоки рабочих тел" 
  :serial t
  :around-compile (lambda (thunk) (let ((cl:*read-default-float-format* 'cl:double-float)) (funcall thunk)))
  :depends-on (:iterate-keywords :budden-tools :perga :buddens-reader :sup-lm
               :budden-adw-plotting :substances)
  :components
  ((:file "literature" :description "Библиография. Ссылки [Литература] - это имена переменных, на них можно делать Find Source")
   (:file "heat-exchanger.package")
   (:file "fluid-stream.package")
   (:file "hx-and-str.package")
   (:file "free-convection.package")
   (:file "diffusion-drying.package")
   (:file "kays-london-notation.package" :description "Обозначения из heat-exchanger-fluid-stream-literature:[KaysLondon]")
   (:file "heat-exchanger-types")
   (:file "fluid-stream-types")
   (:file "free-convection-types")
   (:file "heat-exchanger")
   (:file "fluid-stream")
   (:file "air-stream-1")
   (:file "hx-and-str" :description "flow through heat exchanger. Nu, alpha, friction factor, pressure drop")
   (:file "kays-london-notation" :description "Некоторые обозначения из heat-exchanger-fluid-stream-literature:[KaysLondon] как функции потоков")
   (:file "free-convection")
   (:file "diffusion-drying")
   (:file "kays-london-periodic-flow")
   (:file "epsilon-n_tu")
   ))
