; -*- Encoding: utf-8;  System :heat-exchanger-fluid-stream -*-

(in-package :budden-tools)
(in-readtable :buddens-readtable-a)

(def-merge-packages::! :diffusion-drying
                       (:use :cl :budden-tools
                        :m0
                        :heat-exchanger-fluid-stream-literature
                        :fluid-stream
                        :heat-exchanger
                        :hx-and-str
                        :physical-units
                        :substances
                        ; :substance-constants
                        :warn-once)
                       (:local-nicknames :hx :heat-exchanger :str :fluid-stream :snc :substance-constants)
                       (:custom-token-parsers budden-tools::convert-carat-to-^) 
                       (:import-from :perga-implementation
                        perga-implementation:perga)
                       (:export
 "diffusion-drying:D"))



