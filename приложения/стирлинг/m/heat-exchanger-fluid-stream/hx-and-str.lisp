; -*- Encoding: utf-8;  System :heat-exchanger-fluid-stream -*-

; теплообмен при протекании потока через теплообменник

(in-package :hx-and-str)
(in-readtable :buddens-readtable-a)


(defun hxSkorostqVSechenii (hx str)
  "Скорость в узком сечении теплообменника"
  (perga
    (:lett hx HxBase hx)
    (:lett str FluidStreamWithLiquidProperties str)
    (/ str^VolumetricFlow
       hx^FlowArea)))


(defun hxRey (hx str)
  "Число Рейнольдса, Re"
  (perga
    (:lett hx HxBase hx)
    (:lett str FluidStreamWithLiquidProperties str)
    (* hx^HydrDiam (hxSkorostqVSechenii hx str)
       (/ str^nu))))


(defun hxPe (hx str)
  "Число Пекле, Pe"
  (* (hxRey hx str) (strPr str)))


(defun GenericPressureDrop (rho v DarciFactor length diam)
  "Формула падения давления при течении пока что по каналу"
  (* DarciFactor rho (expt v 2) 0.5 length (/ diam)))


(defgeneric DarciFactor (hx str)
  (:documentation "Коэффициент трения Дарси"))


(defun FanningFactor (hx str)
  "Коэффициент трения Фаннинга"
  (* 1/4 (DarciFactor hx str)))

(defgeneric hxPressureDrop (hx str)
  (:documentation "Падение давления при прохождении потока")
  )

(defun hxEntryExitPressureDrop (hx str)
  (perga
    (:lett hx HxBase hx)
    (:lett str FluidStreamWithLiquidProperties str)
    (* hx^XiEntryExit str^rho (expt (hxSkorostqVSechenii hx str) 2) 0.5)))
    


;; внутренние функции из старой версии dnslib - не рекомендуются к непосредственному использованию в коде 

(defun PipeResistanceMode (Rey)
  "Режим сопротивления для трубы для определения сопротивления (внутренняя, без потоков"
  (assert (>= Rey 0))
  (cond
   ((< Rey 3000.0) 'Lam)
   ((< Rey 1e5) 'Turb1)
   (t 'Turb2)))
    

(defun Xi (PipeKind FlowMode Rey) ; FIXME rename
  "множитель Дарси для трубы (без потоков) 
   64/Rey - закон Пуазейла, ламинарный режим, Rey<3000, A=64 для круглой трубы, 96 для кольца. 
   Turb1 - 0.3164/Rey^0.25, Rey=3000..1e5 - формула Блаузиуса
   Turb2 - 0.0032+0.22/Re^0.237, Re=1e5..1e8 - ф-ла Никурадзе.
   PipeKind может быть 'Pipe или 'Channel. 
   FlowMode возвращается ф-ей PipeResistanceMode"
  (cond
   ((= Rey 0.0) 0.0)
   (t
    (ecase PipeKind
      (Pipe
       (ecase FlowMode
         (Lam (/ 64.0 Rey))
         (Turb1 (/ 0.3164 (expt Rey 0.25)))
         (Turb2 (+ 0.0032 (/ 0.221 (expt Rey 0.237))))
         ))
      (Channel
       (ecase FlowMode
         (Lam (/ 96.0 Rey))
         )
       )))))


(defun PipeP (Xi rho w l d)
  "Падение давления в трубе - внутренняя версия, не пользоваться"
  (/ (* Xi rho (expt w 2) l)
     (* 2 d)))


(defun PipeW (diam vol)
  (/ vol
     (CircleArea diam)))


(defun PipeWRey (diam vol nu)
  (* (PipeW diam vol) diam (/ nu))
  )


(defun PipePvol (len diam vol nu rho)
  "Старая версия падения в трубе по объёму, без учёта потерь на входе"
  (perga
   (let Rey (PipeWRey diam vol nu))
   (let W (PipeW diam vol))
   (PipeP (Xi 'Pipe
              (PipeResistanceMode Rey)
              Rey
              )
          rho
          W
          len
          diam)))


#| 
(defstruct FluidStreamWithLiquidProperties VolumetricFlow Pressure Temperature ThermalExpansionCoefficient nu rho lambda Cp)

 (defstruct Pipe Length FluidVolume FlowArea HeatExchangeArea HydrDiam PipeDiameter) 
|#


(defmethod hxPressureDrop ((hx Pipe) (str FluidStreamWithLiquidProperties))
  (+ (hxEntryExitPressureDrop hx str)
     (PipePvol hx^Length
               hx^PipeDiameter
               str^VolumetricFlow
               str^nu
               str^rho)))



(defmethod hxPressureDrop ((hx Annulus) (str FluidStreamWithLiquidProperties))
  (perga
    (:lett hx HxBase hx)
    (:lett str FluidStreamWithLiquidProperties str)
    (:lett v dbl (/ str^VolumetricFlow hx^FlowArea))
    (+
     (hxEntryExitPressureDrop hx str)
     (GenericPressureDrop str^rho v (DarciFactor hx str) hx^Length hx^HydrDiam))))

(defmethod hxPressureDrop ((hx КоридорныйПучокТруб) (str FluidStreamWithLiquidProperties))
  (perga
    (:lett hx КоридорныйПучокТруб hx)
    (let xi
     (*
      (+ 6 (* 9 hx^ЧислоРядов))
      (expt (/ hx^x1 hx^PipeDiameter) -0.23)
      (expt (hxRey hx str) -0.26)         
      ))
    (GenericPressureDrop str^rho
                         (hxSkorostqVSechenii hx str)
                         xi 1.0 1.0)
    ))

(defmethod hxPressureDrop ((hx ParallelPipes) (str FluidStreamWithLiquidProperties))
  (+ 
   (hxEntryExitPressureDrop hx str)
   (PipePvol hx^Length
             hx^PipeDiameter
             (/ str^VolumetricFlow hx^NumPipes)
             str^nu
             str^rho)))

(defmethod hxPressureDrop ((hx FlatChannel) (str FluidStreamWithLiquidProperties))
  (perga
    (:lett hx HxBase hx)
    (:lett str FluidStreamWithLiquidProperties str)
    (:lett v dbl (/ str^VolumetricFlow hx^FlowArea))
    (+
     (hxEntryExitPressureDrop hx str)
     (GenericPressureDrop str^rho v (DarciFactor hx str) hx^Length hx^HydrDiam))))

(defmethod hxPressureDrop ((hx FlatChannelsMatrix) (str FluidStreamWithLiquidProperties))
  (perga
    (:lett hx HxBase hx)
    (:lett str FluidStreamWithLiquidProperties str)
    (:lett v dbl (/ str^VolumetricFlow hx^FlowArea))
    (+
     (hxEntryExitPressureDrop hx str) 
     (GenericPressureDrop str^rho v (DarciFactor hx str) hx^Length hx^HydrDiam))))


(defparameter *FnKutateladzeTable17-1Прямоугольник*
  (make-piecewize-linear-scalar-fun
   #2A(
       (0.0 96.0)
       (0.1 85.0)
       (0.2 76.0)
       (0.25 73.0)
       (0.333 69.0)
       (0.5 62.0)
       )
   :pseudo-name "*FnKutateladzeTable17-1Прямоугольник*"))


(defparameter *FnKaysLondonFig7-4f-40-alpha-8*
  (make-piecewize-linear-scalar-fun
   (map-matrix
    #'log
    #2A(
       (0.2e3 0.105)
       (0.75e3 0.03)
       (1.7e3 0.015)
       (2.0e3 0.013)
       (3.0e3 0.0105)
       (4.0e3 0.0095)
       (5.0e3 0.009)
       (9.0e3 0.008)
       (30.0e3 0.0063)))
   :pseudo-name "*FnKaysLondonFig7-4f-40-alpha-8*"
   :allow-extrapolation (vector (log 0.001) (log 1000.0e3)))
  "[KaysLondon], p.146, f*(Tw/Tm)^-m для (L/4rh=40), alpha = 8")


(defparameter *FnKaysLondonFig7-4StPr-40-alpha-8*
  (make-piecewize-linear-scalar-fun
   (map-matrix
    #'log
    #2A(
        (0.2e3 0.034)
        (1.0e3 0.0075)
        (1.5e3 0.00533)
        (2.0e3 0.0042)
        (2.5e3 0.0037)
        (3.0e3 0.0036)
        (4.0e3 0.00355)
        (6.0e3 0.0035)
        (9.0e3 0.00333)
        (30.0e3 0.00255)))
   :pseudo-name "*FnKaysLondonFig7-4StPr-40-alpha-8*"
   :allow-extrapolation (vector (log 0.001) (log 1000.0e3)))
  "[KaysLondon], p.146, StPr^2/3 для (L/4rh=40), alpha = 8. 
   Данные для случая постоянной температуры. 
   Для случая постоянного теплового потока будут занижнены до 1.1 рзаа")


(defparameter *FnKaysLondonFig7-2f-40-alpha-1*
  (make-piecewize-linear-scalar-fun
   (map-matrix
    #'log
    #2A(
       (0.2e3 0.073)
       (0.5e3 0.03)
       (1.5e3 0.0117)
       (2.0e3 0.009)
       (2.5e3 0.008)
       (3.0e3 0.0077)
       (3.8e3 0.008)
       (5.0e3 0.0084)
       (6.0e3 0.0085)
       (7.0e3 0.0084)
       (8.0e3 0.0083)
       (10.0e3 0.0079)
       (20.0e3 0.0069)
       (30.0e3 0.0065)       
       ))
   :pseudo-name "*FnKaysLondonFig7-2f-40-alpha-1*"
   :allow-extrapolation (vector (log 0.001) (log 1000.0e3))
   )
  "[KaysLondon], p.144, fig 7-2, f*(Tw/Tm)^-m для (L/4rh=40, для отношения = 100 результат будет завышен до 8/7 раз), alpha = 1")


(defparameter *FnKaysLondonFig7-2StPr-40-alpha-1*
  (make-piecewize-linear-scalar-fun
   (map-matrix
    #'log
    #2A(
        (0.2e3 0.0185)
        (0.7e3 0.007)
        (1.5e3 0.0041)
        (2.0e3 0.00345)
        (3.0e3 0.003)
        (4.0e3 0.00325)
        (6.0e3 0.00345)
        (7.0e3 0.00345)
        (10.0e3 0.0032)
        (30.0e3 0.00255)
        ))
   :pseudo-name "*FnKaysLondonFig7-4StPr-40-alpha-8*"
   :allow-extrapolation (vector (log 0.001) (log 1000.0e3)))
  "[KaysLondon], p.146, StPr^2/3 для (L/4rh=40), alpha = 1. 
   Данные для случая постоянной температуры. 
   Для случая постоянного теплового потока будут занижнены до 1.1 раз. 
   Для случая относительной длины 100 будут завышены до 1.25 раз.  ")


(defparameter *FnKaysLondonFig7-3f-40-alpha-3*
  (make-piecewize-linear-scalar-fun
   (map-matrix
    #'log
    #2A(
        (0.2e3 0.088)
        (0.3e3 0.059)
        (1.5e3 0.0135)
        (2.0e3 0.011)
        (2.3e3 0.01)
        (3.0e3 0.009)
        (3.8e3 0.009)
        (5.0e3 0.0088)
        (6.0e3 0.00855)
        (9.0e3 0.008)
        (30.0e3 0.0063)
        ))
   :pseudo-name "*FnKaysLondonFig7-3f-40-alpha-3*"
   :allow-extrapolation (vector (log 0.001) (log 1000.0e3))
   )
  "[KaysLondon], p.145, f*(Tw/Tm)^-m для (L/4rh=40, для отношения = 100 результат будет завышен в <=1.125 раз), alpha = 3, Re>=2000")

(defparameter *FnKaysLondonFig7-3StPr-40-alpha-3*
  (make-piecewize-linear-scalar-fun
   (map-matrix
    #'log
    #2A(
        (0.2e3 0.0255)
        (1.0e3 0.006)
        (1.75e3 0.004)
        (2.0e3 0.0036)
        (2.5e3 0.0034)
        (3.0e3 0.0033)
        (4.0e3 0.00335)
        (6.0e3 0.00345)
        (7.0e3 0.00345)
        (10.0e3 0.00325)
        (30.0e3 0.00255)
        ))
   :pseudo-name "*FnKaysLondonFig7-3StPr-40-alpha-3*"
   :allow-extrapolation (vector (log 0.001) (log 1000.0e3)))
  "[KaysLondon], p.145, StPr^2/3 для (L/4rh=40), alpha = 3. 
   Данные для случая постоянной температуры. 
   Для случая постоянного теплового потока будут занижены до 1.08 раз. 
   Для случая относительной длины 100 будут завышены до 1.23 раз.  ")



(defun DarciFactorChannelOrChannelsMatrix (re height-divided-by-width length-divided-by-hydr-diam)
  "В точке re = 2000 данная функция претерпевает разрыв"
  (declare (ignore length-divided-by-hydr-diam))
  (perga
    (cond
     ;((<= re 2000) ; [Кутателадзе], стр. 281, ф-ла 17-5. Подразумевается безконечный по длине канал
     ; (warn-once "При расчёте сопротивления каналов в ламинарном режиме канал считается безконечно длинным, сопротивление может быть заниженным")
     ; (:lett A dbl
     ;  (funcall *FnKutateladzeTable17-1Прямоугольник*
     ;           height-divided-by-width))
     ; (/ A re)
     ; )
     (t ; [KaysLondon], Fig 7-2,7-3,7-4, pp. 144-146 ; Канал длиной 40 гидр.диаметров
      (warn-once "При расчёте сопротивления канала(-ов) в не-ламинарном режиме полагаем отношение длины к гидр. диаметру = 40")
      (let logre (log re))
      (assert (<= 0 height-divided-by-width 1))
      (let alpha
        (cond
         ((= 0 height-divided-by-width) 1e5)
         (t (/ height-divided-by-width))))
      (let logfn
        (cond
         ((> alpha 8.0)
          (warn-once "При расчёте сопротивления канала относительно глубиной более 8 берём данные для глубины 8")
          (funcall *FnKaysLondonFig7-4f-40-alpha-8* logre))
         ((> alpha 3.0)
          (linear-scalar-fun alpha 3.0 (funcall *FnKaysLondonFig7-3f-40-alpha-3* logre) 8.0 (funcall *FnKaysLondonFig7-4f-40-alpha-8* logre)))
         ((>= alpha 1.0)
          (linear-scalar-fun alpha 1.0 (funcall *FnKaysLondonFig7-2f-40-alpha-1* logre) 3.0 (funcall *FnKaysLondonFig7-3f-40-alpha-3* logre)))
         (t
          (error "13213123765"))))
      (* 4 (exp logfn))
      ))))
  

(defmethod DarciFactor ((hx FlatChannel) (str FluidStreamWithLiquidProperties))
  "Источники написаны в случаях"
  (perga
    (:lett hx FlatChannel hx)
    (:lett str FluidStreamWithLiquidProperties str)
    (:lett re dbl (hxRey hx str))
    (DarciFactorChannelOrChannelsMatrix re (/ hx^Height hx^Width) (/ hx^Length hx^HydrDiam))
    ))


(defmethod DarciFactor ((hx FlatChannels) (str FluidStreamWithLiquidProperties))
  "См. для FlatChannel"
  (perga
    (:lett hx FlatChannel hx)
    (:lett str FluidStreamWithLiquidProperties str)
    (:lett re dbl (hxRey hx str))
    (DarciFactorChannelOrChannelsMatrix re (/ hx^Height hx^Width) (/ hx^Length hx^HydrDiam))
    ))


(defmethod DarciFactor ((hx FlatChannelsMatrix) (str FluidStreamWithLiquidProperties))
  "Источники написаны в случаях"
  (perga
    (:lett hx FlatChannelsMatrix hx)
    (:lett str FluidStreamWithLiquidProperties str)
    (:lett re dbl (hxRey hx str))
    (DarciFactorChannelOrChannelsMatrix re 0.0 (/ hx^Length hx^HydrDiam)) ; считаем щели очень длинными
    ))


(defmethod DarciFactor ((hx Annulus) (str FluidStreamWithLiquidProperties))
  "Источники написаны в случаях"
  (perga
    (:lett hx Annulus hx)
    (:lett str FluidStreamWithLiquidProperties str)
    (:lett re dbl (hxRey hx str))
    (cond
     ((= re 0) 0.0)
     ((< re 2300) ; [Кутателадзе], стр. 282
      (warn-once "DarciFactor (Annulus) в ламинарном режиме - нужно проверять длину кольца, но в источнике границы не указаны")
      (/ 96 re))
     ((< re 1e5) ; [Кутателадзе], стр. 281, ф-ла 17-6 . Написано, что можно перенести на Annulus
      (/ 0.316 (expt re 0.25)))
     (t ; [Кутателадзе], стр. 281, ф-ла 17-7. На стр. 281 написано, что можно перенести с трубы
      (/ (expt (- (* 1.82 (/ (log re) (log 10))) 1.64) 2)))
     )))
       

(defmethod DarciFactor ((hx SetkaRodsKaysLondon)(str FluidStreamWithLiquidProperties))
  "Быстрогрязно из программы Уриели. На самом деле зависит от пористости сетки, см. [KaysLondon], рис 7-9. Но зависимость настолько странная, что не поднимается рука её забивать"
  (perga
   (:lett re dbl (hxRey hx str))
   (* 4 (/ (+ 54 (* 1.43 (expt re 0.78))) re))
   ))


(defmethod DarciFactor ((hx SphericalBedKaysLondon)(str FluidStreamWithLiquidProperties))
  "[KaysLondon], Рис. 7-10, стр. 150"
  (* 4 0.23 (expt (hxRey hx str) -0.3))
  )


(defun hxPressureDropPorousBody (hx str)
  "Формула 2-26а,p36 Kays,London"
  (perga
    (:lett str FluidStreamWithLiquidProperties str)
    (:lett hx HxBase hx)
    (:lett f dbl (FanningFactor hx str))
    (:lett W dbl (* str^VolumetricFlow str^rho))
    (:lett Ac dbl hx^FlowArea)
    (:lett vm dbl (/ str^rho)) ; specific volume
    (:lett v1 dbl 1.0)
    (:lett p1 dbl str^Pressure)
    (:lett G dbl (/ W Ac)) ; массовая скорость
    (* p1 (/ (* (expt G 2) v1) (* 2 p1))
       f
       (/ hx^HeatExchangeArea Ac)
       (/ vm v1))))
    

(defmethod hxPressureDrop ((hx SetkaRodsKaysLondon)(str FluidStreamWithLiquidProperties))
  ; пренебрегаем входным/выходным сопротивлением
  (hxPressureDropPorousBody hx str)
  )


(defmethod hxPressureDrop ((hx SphericalBedKaysLondon)(str FluidStreamWithLiquidProperties))
  ; пренебрегаем входным/выходным сопротивлением
  (hxPressureDropPorousBody hx str)
  )

   
(defgeneric hxNu (hx str)
  (:documentation "Число Нуссельта, Nu"))


(defun hxAlpha (hx str)
  "Средний коэфт теплоотдачи"
  (perga
   (:lett hx HxBase hx)
   (:lett str FluidStreamWithLiquidProperties str)
   (cond
    ((= 0 str^VolumetricFlow) 0.0)
    (t (/ (* (hxNu hx str) str^lambda) hx^HydrDiam))
    )))

#|
 нюанс, связанный со средним коэфтом vs средним перепадом температур не раскрыт пока
 (defun HeatExchangePower (hx str DeltaT)
  "Тепловая мощность при проходе через теплообменник hx потока str"
  (* DeltaT (hxAlpha hx str)
     hx^HeatExchangeArea))

|#

(defun hxSt (hx str)
  "Число Пекле, St = Nu/Pe"
  (/ (hxNu hx str) (hxPe hx str)))


(defgeneric ПродольнаяТеплопроводность (hx str)
  (:documentation "Продольная теплопроводность - для регенераторов"))

(defmethod ПродольнаяТеплопроводность ((hx SetkaRodsKaysLondon) (str FluidStreamWithLiquidProperties))
  (perga
    (:lett hx SetkaRodsKaysLondon hx)
    (:lett str FluidStreamWithLiquidProperties str)
    (:lett GasResistance dbl (/ str^LAMBDA))
    (:lett SetkaResistance dbl (/ hx^SetkaLambda))
    (:lett p dbl hx^Porosity)
    (:lett TotalResistance dbl
     (+ (* p GasResistance) (* (- 1 p) SetkaResistance)))
    (:lett TotalConductivity dbl
     (/ TotalResistance))
    (* TotalConductivity
       hx^FrontalArea
       (/ hx^Length))))


(defmethod ПродольнаяТеплопроводность ((hx FlatChannelsMatrix) (str FluidStreamWithLiquidProperties))
  (perga
    (:lett hx FlatChannelsMatrix hx)
    (:lett str FluidStreamWithLiquidProperties str)
    (:lett GasArea dbl hx^FlowArea)
    (:lett MetalArea dbl (* (- 1 hx^Porosity) hx^FrontalArea))
    (* 
     (+ 
      (* GasArea str^LAMBDA)
      (* MetalArea hx^RebroLambda)
      )
     (/ hx^Length)
    )))


(defmethod ПродольнаяТеплопроводность ((hx Annulus) (str FluidStreamWithLiquidProperties))
  (perga
    (:lett hx Annulus hx)
    (:lett str FluidStreamWithLiquidProperties str)
    (:lett GasArea dbl hx^FlowArea)
    (* GasArea str^LAMBDA
       (/ hx^Length)
    )))

(defparameter *FnKaysLondonRis78Fn0832*
  (make-piecewize-linear-scalar-fun
   (map-matrix
    #'log
    #2A((30.0 0.3)
        (70 0.2)
        (400 0.08)
        (1000.0 0.052)
        (9000.0 0.02)
        (6e4 0.01)
        (1e5 0.095)))
   :allow-extrapolation (vector -100.0 (log 1e5))
   :pseudo-name "*FnKaysLondonRis78Fn0832*"))

(defparameter *FnKaysLondonRis78Fn0725*
  (make-piecewize-linear-scalar-fun
   (map-matrix
    #'log
    #2A((9.0 0.3)
        (105 0.1)
        (350 0.06)
        (900 0.038)
        (4300 0.02)
        (6.0e4 0.008)))
   :allow-extrapolation (vector -100.0 (log 6.0e4))
   :pseudo-name "*FnKaysLondonRis78Fn0832*"))

(defparameter *FnKaysLondonRis78Fn0766*
  (make-piecewize-linear-scalar-fun
   (map-matrix
    #'log
    #2A((13.0 0.3)
        (700 0.05)
        (6.0e4 0.0085)
        ))
   :allow-extrapolation (vector -100.0 (log 6.0e4))
   :pseudo-name "*FnKaysLondonRis78Fn0832*"))


(defun FnKaysLondonRis78 (re poro)
  "Данные с рис 7.8 из Kays, London, стр. 148. Сделано только 
  для трёх пористостей - 0.832 и 0.766, 0.725"
  (perga
   (:lett logre dbl (log re))
   (:lett logresult dbl 
    (ecase poro
      (0.725
       (funcall *FnKaysLondonRis78Fn0725* logre))
      (0.766
       (funcall *FnKaysLondonRis78Fn0766* logre))
      (0.832
       (funcall *FnKaysLondonRis78Fn0832* logre))))
   (exp logresult)))

(defparameter *FnKaysLondonRis7-10f*
  (make-piecewize-linear-scalar-fun
   (map-matrix
    #'log
    #2A((10.0 5.1)
        (20.0 3.1)
        (100.0 1.2)
        (200.0 0.8)
        (300.0 0.7)
        (500.0 0.6)
        (6000.0 0.4)
        (5.0e4 0.295)
        ))
   :pseudo-name "*FnKaysLondonRis7-10f*"))

(defun FnKaysLondonRis7-10f (re)
  "f с рисунка 7-10 Kays, London - засыпка сфер с пористостью 0.37 - 0.39"
  (perga
    (:lett logre dbl (log re))
    (:lett logresult dbl 
     (funcall *FnKaysLondonRis7-10f* logre))
   (exp logresult)))
    

#|
 (budden-adw-plotting::plot-log-xy-scalar-fn (lambda (x) (hx-and-str::FnKaysLondonRis78 x 0.832)) 30 1000)
|#  

(defmethod hxNu ((hx SetkaRodsKaysLondon)(str FluidStreamWithLiquidProperties))
  (perga
   (:lett re dbl (hxRey hx str))
   (:lett poro dbl hx^Porosity)
   (:lett StPr23 dbl
    (cond
     ((<= 0.71 poro 0.746)
      (FnKaysLondonRis78 re 0.725))
     ((<= 0.746 poro 0.80)
      (FnKaysLondonRis78 re 0.766))
     ((<= 0.80 poro 0.84)
      (FnKaysLondonRis78 re 0.832))
     (t
      (error "Неверная пористость ~S" poro))))
   (* (/ StPr23 (expt (strPr str) 2/3))
      (hxPe hx str))))


(defmethod hxNu ((hx SphericalBedKaysLondon)(str FluidStreamWithLiquidProperties))
  (perga
   (:lett re dbl (hxRey hx str))
   (assert (= hx^Porosity 0.38))
   (:lett StPr23 dbl (* 0.23 (expt re -0.3))) ; [KaysLondon], p. 150, fig 7-10
   (* (/ StPr23 (expt (strPr str) 2/3))
      (hxPe hx str))))


#|(defstruct SetkaRodsKaysLondon Length FluidVolume FlowArea HeatExchangeArea HydrDiam FrontalArea SetkaWireDiam SetkaCellSize Porosity SetkaKaysLondonHydrRadius NumberOfLayers AreaPerVolume)

 (defstruct FluidStreamWithLiquidProperties VolumetricFlow Pressure Temperature ThermalExpansionCoefficient nu rho lambda Cp)|#

(defun check-lnfmm90 (hx)
  (perga
    (etypecase hx
      (Pipe
       (let result hx^LengthNuFactorMM90)
       (assert (numberp result) () "Для вычисления теплообмена трубы ~S при данных условиях должен быть задан LengthNuFactorMM90" hx)
       (coerce result 'double-float)
       )
      (FlatChannel
       (warn-once "check-lnfmm90 вызвана для FlatChannel. Возвращаем 1")
       1.0 ; это неправильно
       )
      (FlatChannelsMatrix
       (warn-once "check-lnfmm90 вызвана для FlatChannelsMatrix. Возвращаем 1")
       1.0 ; это неправильно
       )
      (Annulus
       (warn-once "check-lnfmm90 вызвана для Annulus. Возвращаем 1")
       1.0 ; это неправильно
       )
      )))  
   

(defun hxNuPipeOrAnnulusLaminar (re dd ll pr)
  (* 1.4 (expt (* re dd (/ ll)) 0.4) (expt pr 0.33)))

(defun hxNuPipeOrAnnulusTurb1 (re pr lnfmm90)
  (* 0.021 (expt re 0.8) (expt pr 0.43) lnfmm90))

(defun hxNuPipeOrAnnulus (hx str)
  "Вариант 5e6<=re<=2e8 и 0.6<=pr<=100 - Кутателадзе, стр. 100, ф-ла 7-18
 0<=re<=2300 - ф-ла 3-33 из [МихеевМихеева] . Предполагаем, что Pr мало меняется, но это для жидкостей может быть неправдой.
 Переходный режим Справочник по теплопередаче, [Кутателадзе] , стр.105. 
 Имеются разрывы на границах переходного режима"
  (perga
   (:lett hx HxBase hx)
   (:lett str FluidStreamWithLiquidProperties str)
   (:lett re dbl (hxRey hx str))
   (:lett pr dbl (strPr str))
   (:lett dd dbl hx^HydrDiam)
   (:lett ll dbl hx^Length)
   (cond
    ((< re 10)
     (warn-once "hxNuPipeOrAnnulus : re<10")
     (hxNuPipeOrAnnulusLaminar 10 dd ll pr)
     )
    ((<= 5000.0 re 5e6)
     (hxNuPipeOrAnnulusTurb1 re pr (check-lnfmm90 hx)))     
    ((and
      (<= 5e6 re 2e8)
      (<= 0.6 pr 100.0))
     (:lett lnfmm90 dbl (check-lnfmm90 hx))
     (* 0.023 (expt re 0.8) (expt pr 0.49) lnfmm90))
    ((<= 10.0 re 2300.0)
     (unless (<= 0.0 (/ dd ll) 0.1)
       (warn-once "Отношение диаметра к длине трубы не должно быть больше 0.1 для расчёта Nu в ламинарном режиме. Считаем для отношения 10, получим заниженный Nu. См. также 22_Высокотемпературные теплоносители и их применеие"))
     (hxNuPipeOrAnnulusLaminar re dd ll pr))
    ((and
      (<= 2300 re 5000)      
      (> pr 0.5)
      (> (/ ll dd) 30.0)
      (> (* re pr dd (/ ll)) 12.0))
     (warn-once "hxNuPipeOrAnnulus: 2300<=re<=5000")
     (* 21.0
        (expt (* pr dd (/ ll)) 0.33)
        (expt (/ re 2300.0) (log (/ ll dd) 10.0))
        ))
    ((and
      (<= 2300 re 5000)
      (> pr 0.5)
      (> (/ ll dd) 30.0)
      (<= (* re pr dd (/ ll)) 12.0))
     (warn-once "hxNuPipeOrAnnulus: 2300<=re<=5000")
     (* 3.66 (expt (/ re 2300.0) (+ 2.3 (log pr) 10.0)))
     )
    ((<= 2300 re 5000)
     (warn-once "hxNuPipeOrAnnulus: 2300<=re<=5000")
     (unless (> (/ ll dd) 10.0)
       (warn-once "hxNuPipeOrAnnulus: Слишком короткая трубка при 2300<=re<=5000. Результат непредсказуемо неверен"))
     (:lett value2300 dbl (hxNuPipeOrAnnulusLaminar 2300 dd ll pr))
     (:lett value5000 dbl (hxNuPipeOrAnnulusTurb1 5000 pr (check-lnfmm90 hx)))
     (exp (linear-scalar-fun re 2300.0 (log value2300) 5000.0 (log value5000)))
     )
    (t
     (error "Не знаю, как посчитать Nu для трубы ~S и потока ~S" hx str))
    )))


(defun hxNuChannelsReToStPr (re height-divided-by-width)
  (perga
    (when (< re 1)
      (setf re 1))
    (let logre (log re))
    (let alpha
      (cond
       ((= 0 height-divided-by-width) 1e5)
       (t (/ height-divided-by-width))))
    (let logfn
      (cond
       ((> alpha 8.0)
        (warn-once "При расчёте сопротивления канала относительной глубиной более 8 берём данные для глубины 8")
        (funcall *FnKaysLondonFig7-4StPr-40-alpha-8* logre))
       ((> alpha 3.0)
        (linear-scalar-fun alpha 3.0 (funcall *FnKaysLondonFig7-3StPr-40-alpha-3* logre) 8.0 (funcall *FnKaysLondonFig7-4StPr-40-alpha-8* logre)))
       ((>= alpha 1.0)
        (linear-scalar-fun alpha 1.0 (funcall *FnKaysLondonFig7-2StPr-40-alpha-1* logre) 3.0 (funcall *FnKaysLondonFig7-3StPr-40-alpha-3* logre)))
       (t
        (error "13213123765"))))
    (exp logfn)))

(defun hxNuChannelsInner (hx str Gap LongerSide OneWallP)
  (perga
    (:lett hx HxBase hx)
    (:lett str FluidStreamWithLiquidProperties str)
    (:lett re dbl (hxRey hx str))
    (:lett pr dbl (strPr str))
    ;(:lett dd dbl hx^HydrDiam)
    ;(:lett ll dbl hx^Length)
    (warn-once "hxNuChannelsInner: формула верна только при условии, что длина/гидр.диаметр = 40")
    (let StPr (hxNuChannelsReToStPr re (/ Gap LongerSide)))
    (let St (/ StPr (expt pr 2/3)))
    (let Nu (* St (hxPe hx str)))
    (when OneWallP
      (when (> re 2000)
        (warn-once "hxNuChannelsInner: OneWallP = t and re>2000: в турбулентном режиме применение коэффициента не обосновано"))
      (_f * Nu (/ 4.86 7.54) ; [KaysLondon], стр 120, вариант Nu_T
          ))
    Nu
    ))


(defmethod hxNu ((hx Pipe)(str FluidStreamWithLiquidProperties))
  (hxNuPipeOrAnnulus hx str))

(defmethod hxNu ((hx КоридорныйПучокТруб)(str FluidStreamWithLiquidProperties))
  (perga
   (:lett hx КоридорныйПучокТруб hx)
   (:lett str FluidStreamWithLiquidProperties str)
   (:lett re dbl (hxRey hx str))
   (:lett pr dbl (strPr str))
   (let nu3
     (cond
      ((< re 1e3)
       (* 0.56 (expt re 0.5) (expt pr 0.36)))
      (t
       (* 0.22 (expt re 0.65) (expt pr 0.36)))))
   (let nu2 (* nu3 0.9))
   (let nu1 (* nu3 0.6))
   (let rows hx^ЧислоРядов)
   (case rows
     (1 nu1)
     (2 (* 0.5 (+ nu1 nu2)))
     (t (/ (+ nu1 nu2 (* (- rows 2) nu3)) rows))))
   )

(defmethod hxNu ((hx Annulus)(str FluidStreamWithLiquidProperties))
  (warn-once "Вызывает сомнение, что формула Nu для трубы годится для annulus")
  (hxNuPipeOrAnnulus hx str))

(defmethod hxNu ((hx ParallelPipes)(str FluidStreamWithLiquidProperties))
  (hxNuPipeOrAnnulus hx str))


(defun hxNuChannelInner1 (Pe2dL)
  (* 1.85 (expt Pe2dL 1/3)))

(defun hxNuChannels (hx str Gap LongerSide OneWallP)
  "Для FlatChannel и FlatChannelsMatrix . Gap - зазор между соседними рёбрами. 0<=re<=2300 - [Кутателадзе], стр. 97, 7-12, 7-13"
  (perga
   (:lett hx HxBase hx)
   (:lett str FluidStreamWithLiquidProperties str)
   ;(:lett pr dbl (strPr str))
   ;(:lett dd dbl hx^HydrDiam)
   (:lett ll dbl hx^Length)
   (:lett re dbl (hxRey hx str))
   (cond
    ((and (<= re 2000) (< (/ ll Gap) 20))
     ; формулы приведены для безконечной ширины
     ; будем считать от 1/20, хотя это произвольное допущение
     ; характер отличия при конечном отношении можно видеть
     ; в [KaysLondon], стр. 120,121,136. Разница между 6 и infinity 
     ; составляет до 1.4 раз.
     (assert (>= (/ hx^Length Gap) 5)) ; совпадение с точностью до 1.2 было при  
         ; отношении, равном 3 и Re = 55 (формула занижает). 
         ; Чем короче ребро, тем оно эффективнее. Поэтому наше ограничение в 5 является заниженным. 
         ; Определённая информация о коротких рёбрах находится в [KaysLondon], стр. 136,
         ; 139 (для труб)
         ; по коротким рёбрам для случая регенератора интерес предтсавляет рис. 9-16 (стр 185)
         ; а также, возможно, стр. 255 (но там неясно, изотермическая стенка ли)
         ; соотношение между регенератором и изотермой - см. стр. 144 (рис 7-2).
     (:lett Pe2dL dbl (* (hxPe hx str) 2 Gap (/ hx^Length)))
     (:lett res dbl
      (cond
       ((<= Pe2dL 70.0)
        (hxNuChannelInner1 70.0))
       (t
        (hxNuChannelInner1 Pe2dL))
       ))
     (when OneWallP
       (_f * res (/ 4.86 7.54) ; [KaysLondon], стр 120, вариант Nu_T
           ))
     res
     )
    (t
     (when (< (/ ll Gap) 20)
       (warn-once "Не ламинарное течение в коротком канале - канал считается безконечным, Nu имеет разрыв при Re=2000"))
     ;(warn-once "Не ламинарное течение в канале - применяем общую формулу для трубы")
     ;(hxNuPipeOrAnnulus hx str)
     (hxNuChannelsInner hx str Gap LongerSide OneWallP)
     ))))
    


(defmethod hxNu ((hx FlatChannel)(str FluidStreamWithLiquidProperties))
  (perga
    (:lett hx FlatChannel hx)
    (:lett Gap dbl hx^Height)
    ;(assert (>= (/ hx^Width Gap) 20.0))
    (hxNuChannels hx str Gap hx^Width hx^OneWallP)
    ))

(defmethod hxNu ((hx FlatChannels)(str FluidStreamWithLiquidProperties))
  (perga
    (:lett hx FlatChannels hx)
    (:lett Gap dbl hx^Height)
    ;(assert (>= (/ hx^Width Gap) 20.0))
    (hxNuChannels hx str Gap hx^Width hx^OneWallP)
    ))


(defmethod hxNu ((hx FlatChannelsMatrix)(str FluidStreamWithLiquidProperties))
  (perga
    (:lett hx FlatChannelsMatrix hx)
    (hxNuChannels hx str hx^GapThickness (* hx^GapThickness 1e5) hx^OneWallP)
    ))
     

#|(defun count-re-vs-cache-references (n)
  "Пример, который показывает, что выгоднее кешировать re, чем вычислять его. Разница получилась вдвое.
  Имеется в виду, что мы заводим хеш-таблицу, где каждому hx сопоставляем хеш-таблицу, в к-рой ключами являются 
  str, с которыми он посчитан. Таким образом, вычисление re заменяется на поиск в двух хеш-таблицах. Можно завести
  класс 'hx-and-str,к-рый означает, что через этот hx течёт этот str, и все вычисления кешировать в нём. Это будет ещё
  во много раз быстрее. Однако, когда у нас просто функции, это надёжнее. Поэтому пока пусть всё будет как есть"
  (defparameter *my-hxs* (make-hash-table :test 'eq :weak-kind :key))
  (defparameter *keep-hxs*
    (iterk:iter
      (:for i from 1 to 1000)
      (:for p = (MAKE-Pipe :Length 10.0 :PipeDiameter 0.01))
      (CalcHeatExchanger p)
      (:collect p)
      (setf (gethash (MAKE-Pipe) *my-hxs*) t)))
  (perga
    (:lett str AirStream1 (MAKE-AirStream1 :Pressure [Bar] :Temperature 273.0 :VolumetricFlow ([liter] 3)))
    (CalcStream str)
    (let result nil)
    (time (dotimes (i n) (push (hxRey (first *keep-hxs*) str) result)))
    (setf result (last result))
    (time (dotimes (i n)
            (push (gethash (first *keep-hxs*) *my-hxs*) result)
            (push (gethash (first *keep-hxs*) *my-hxs*) result)))
    (setf result (last result))
    (time (dotimes (i n)
            (push str^VolumetricFlow result))
    )))|#


(defun СреднелогарифмическийТемпературныйНапор (Th Tk)
  "[МихеевМихеева], стр. 84"
  (/ (- Th Tk) (log (/ Th Tk))))


(defun MeanEffectiveTemperature (Th Tk)
  (/ (- Th Tk) (log (/ Th Tk)))) 


(defmethod hxPressureDrop ((hx SequentialHeatExchangers) (str FluidStreamWithLiquidProperties))
  (perga
    (:lett hx SequentialHeatExchangers hx)
    (:lett result dbl 0.0)
    (dolist (m hx^Members)
      (incf result (hxPressureDrop m str)))
    result))


(defmethod hxNu ((hx SequentialHeatExchangers) (str FluidStreamWithLiquidProperties))
  "Рассчитываем средний общий коэф-т теплопроводности по всем теплообменникам. Условный гидравл. диам = 1м."
  (perga
    (:lett hx SequentialHeatExchangers hx)
    (:lett str FluidStreamWithLiquidProperties str)
    (:lett totalAUav dbl 0.0) ; [KaysLondon], p.16, A*Uav
    (dolist (m hx^Members)
      (:lett m HxBase m)
      (_f + totalAUav (* (hxAlpha m str) m^HeatExchangeArea))
      )
    (* totalAUav hx^HydrDiam (/ str^lambda) (/ hx^HeatExchangeArea))))



