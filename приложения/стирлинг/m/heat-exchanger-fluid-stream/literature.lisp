; -*- Encoding: utf-8;  System :heat-exchanger-fluid-stream -*-

(named-readtables:in-readtable :buddens-readtable-a)

(def-merge-packages::!
 :heat-exchanger-fluid-stream-literature
 (:always t)
 (:use :cl)
 (:import-from :named-readtables named-readtables:in-readtable)
 (:export
  "
  heat-exchanger-fluid-stream-literature::[АвчуховПаюсте]
  heat-exchanger-fluid-stream-literature::[МихеевМихеева]
  heat-exchanger-fluid-stream-literature::[РойзенДулькин]
  heat-exchanger-fluid-stream-literature::[KaysLondon]
  heat-exchanger-fluid-stream-literature::[Кутателадзе]
  heat-exchanger-fluid-stream-literature::[Уокер]
  "))

(in-package :heat-exchanger-fluid-stream-literature)

(defparameter [АвчуховПаюсте]
  "Авчухов, Паюсте, Задачник по процессам тепломассообмена, М., Энергоатомиздат, 1986"
   )
   ; C:\Documents and Settings\denis.97898F5EBA14422\Мои документы\Downloads\holod\Термодинамика и тепломассообмен\Задачник по процессам тепломассообмена (Авчухов В.В., Паюсте Б.Я.).djv 


(defparameter [МихеевМихеева]
  "Михеев, Михеева, Основы теплопередачи, 2-е издание"
   )


(defparameter [РойзенДулькин]
  "Ройзен,Дулькин. Тепловой расчёт оребрённых поверхностей"
  )


(defparameter [KaysLondon]
  "Kays, London. Compact Heat Exchangers. Third Edition")


(defparameter [Кутателадзе]
  "Кутателадзе, Боришанский. Справочник по теплопередаче. Государственное энергетическое издательство, 1958"
  ; (CAPI-WIN32-LIB:shell-open nil "C:/Documents and Settings/denis.97898F5EBA14422/Мои документы/Downloads/holod/Термодинамика и тепломассообмен/Справочник по теплопередаче (С. С. Кутателадзе).djvu")
  )

(defparameter [Уокер]
  "Уокер, Двигатели Стирлинга. м.Машиностроение, 1985")