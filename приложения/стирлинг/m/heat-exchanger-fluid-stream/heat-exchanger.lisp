; -*- Encoding: utf-8;  System :m -*-

(in-package :heat-exchanger)
(in-readtable :buddens-readtable-a)

(defgeneric CalcHeatExchanger (hx)
  (:documentation "Вычисляет и мемоизирует вспомогательные величины, возвращает HeatExchanger"))


(defun WettedPerimeter (hx)
  (perga
    (assert (typep hx
                   '(or Pipe ParallelPipes Channel FlatChannelsMatrix))
        () "Может не подходить для пористых насадок")
    (:lett hx HxBase hx)
    (/ hx^HeatExchangeArea hx^Length)))

(defmethod CalcHeatExchanger ((hx SetkaRodsKaysLondon))
  (perga
   (:lett hx SetkaRodsKaysLondon hx)
   (assert (and hx^Length hx^FrontalArea hx^SetkaWireDiam hx^SetkaCellSize))
   (:lett d dbl hx^SetkaWireDiam)
   (:lett Xt dbl (/ hx^SetkaCellSize d))
   ; (:lett fc dbl (* d (- Xt 1)))
   (:lett L dbl 1.0) ;условная длина будет 1 м
   (setf hx^Porosity
         (- 1 (/ (CircleArea d) (* (expt d 2) Xt))))
   ;(let sigma (/ (expt (- Xt 1) 2) (expt Xt 2)))
   (let AA (* (/ L d)
              (* (CircleLength d) L)
              (/ L (* d Xt))))
   (let alpha-alpha (/ AA (expt L 3)))
   (setf hx^SetkaKaysLondonHydrRadius (/ hx^Porosity alpha-alpha))
   (setf hx^AreaPerVolume alpha-alpha)
         
   (setf hx^FluidVolume
         (* hx^FrontalArea hx^Length hx^Porosity))

   (setf hx^FlowArea
         (* hx^FrontalArea hx^Porosity))

   
   (setf hx^HeatExchangeArea
         (* hx^AreaPerVolume hx^FrontalArea hx^Length))

   (setf hx^HydrDiam
         (* 4 hx^SetkaKaysLondonHydrRadius))
   
   hx))


(defun MAKE-SetkaRodsKaysLondon-ByPorosity (&key Length FrontalArea SetkaWireDiam Porosity SetkaLambda)
  (perga
    (:lett res SetkaRodsKaysLondon (apply 'MAKE-SetkaRodsKaysLondon (dispatch-keyargs-simple Length FrontalArea SetkaWireDiam SetkaLambda)))
    ; находим размер ячейки, чтобы получить такую пористость, взято из CalcHeatExchanger
    (:lett d dbl res^SetkaWireDiam)
    (:lett Xt dbl (/ (/ (CircleArea d) (- 1 Porosity)) (expt d 2)))
    (setf res^SetkaCellSize (* d Xt))
    (setf res^Porosity nil)
    res))

(defmethod CalcHeatExchanger ((hx SphericalBedKaysLondon))
  (perga
   (:lett hx SphericalBedKaysLondon hx)
   (assert (and hx^Length hx^FrontalArea hx^SphereDiam))
   (assert (eql hx^Porosity 0.38))
   (:lett r dbl (/ hx^SphereDiam 2))
   (:lett L dbl 1.0) ;условная длина будет 1 м
   (:lett NoOfSpheres dbl ; примерное число сфер в объёме L^3
    (*
     (- 1.0 hx^Porosity)
     (/ (expt L 3)
        (SphereVolumeByRadius r))))
   (:lett AA dbl (* NoOfSpheres (SphereSurfaceAreaByRadius r))) ; площадь теплообмена в L^3
   (let alpha-alpha (/ AA (expt L 3)))
   (setf hx^SphericalBedKaysLondonHydrRadius (/ hx^Porosity alpha-alpha))
   (setf hx^AreaPerVolume alpha-alpha)
         
   (setf hx^FluidVolume
         (* hx^FrontalArea hx^Length hx^Porosity))

   (setf hx^FlowArea
         (* hx^FrontalArea hx^Porosity))

   (setf hx^HeatExchangeArea
         (* hx^AreaPerVolume hx^FrontalArea hx^Length))

   (setf hx^HydrDiam
         (* 4 hx^SphericalBedKaysLondonHydrRadius))
   
   hx))

(defmethod CalcHeatExchanger ((hx Pipe))
  (perga
   (:lett hx Pipe hx)
   (:lett d dbl hx^PipeDiameter)
   (setf hx^HydrDiam d)
   (:lett l dbl hx^Length)
   (let a (CircleArea d))
   (setf hx^FlowArea a)
   (setf hx^FluidVolume (* a l))
   (setf hx^HeatExchangeArea (* l (CircleLength d)))
   hx))


(defmethod CalcHeatExchanger ((hx КоридорныйПучокТруб))
  (perga
   (:lett hx КоридорныйПучокТруб hx)
   (:lett d dbl hx^PipeDiameter)
   (setf hx^HydrDiam d)
   (setf hx^FlowArea (* hx^ЧислоТрубВРяду hx^X1 hx^ДлинаТрубы))
   (WARN-ONCE:warn-once "CalcHeatExchanger ((hx КоридорныйПучокТруб)): считаем объём нулевым")
   (setf hx^FluidVolume 0.0)
   (setf hx^HeatExchangeArea (* hx^ЧислоТрубВРяду hx^ЧислоРядов hx^ДлинаТрубы (CircleLength d)))
   hx))

(defmethod CalcHeatExchanger ((hx ParallelPipes))
  (perga
   (:lett hx ParallelPipes hx)
   (:lett d dbl hx^PipeDiameter)
   (setf hx^HydrDiam d)
   (:lett l dbl hx^Length)
   (:lett n integer hx^NumPipes)
   (let a (* n (CircleArea d)))
   (setf hx^FlowArea a)
   (setf hx^FluidVolume (* a l))
   (setf hx^HeatExchangeArea (* n l (CircleLength d)))
   hx
    ))


(defun GenericHydrDiam (FlowArea HeatExchangeArea Length)
  (/
   (* 4 FlowArea)
   (/ HeatExchangeArea Length)))

(defmethod CalcHeatExchanger ((hx Annulus))
  (perga
   (:lett hx Annulus hx)
   (:lett l dbl hx^Length)
   (:lett od dbl hx^OuterDiam)
   (:lett id dbl hx^InnerDiam)
   (let a (- (CircleArea od) (CircleArea id)))
   (setf hx^FlowArea a)
   (let hxa (* l (+ (CircleLength od) (CircleLength id))))
   (setf hx^HeatExchangeArea hxa)
   (setf hx^HydrDiam (GenericHydrDiam a hxa l))
   (setf hx^FluidVolume (* a l))
   hx
   ))


(defmethod CalcHeatExchanger ((hx FlatChannel))
  (perga
   (:lett hx FlatChannel hx)
   (check-slots-are-dbl hx (Length Height Width))
   (assert (>= hx^Width hx^Height))
   (:lett a dbl (* hx^Height hx^Width))
   (setf hx^FlowArea a)
   (setf hx^HeatExchangeArea (* 2.0 (+ hx^Height hx^Width) hx^Length))
   (setf hx^HydrDiam (GenericHydrDiam a hx^HeatExchangeArea hx^Length))
   (setf hx^FluidVolume (* a hx^Length))
   hx
   ))



(defmethod CalcHeatExchanger ((hx FlatChannels))
  (perga
   (:lett hx FlatChannels hx)
   (check-slots-are-dbl hx (Length Height Width))
   (assert (>= hx^Width hx^Height))

   (flet specified-number (x)
     (and (numberp x) (/= 0 x)))

   (let count-fields-specified
     (count-if
      #'specified-number
      (list hx^NumChannels hx^FlowArea hx^FrontalArea)))
   (assert (= 1 count-fields-specified) ()
     "One and only one of fields NumChannels, FlowArea, FrontalArea must be given for FlatChannels")

   (flet SetByNumChannels (nc)
     (setf hx^NumChannels nc)
     (let h hx^Height)
     (let w hx^Width)
     (let l hx^Length)
     (:lett a dbl (* h w))
     (setf hx^FlowArea
           (* nc a))
     (setf hx^FrontalArea
           (* nc (+ h hx^RebroThickness) w))
     (setf hx^HeatExchangeArea (* nc 2.0 (+ h w) l))
     (setf hx^HydrDiam (GenericHydrDiam hx^FlowArea hx^HeatExchangeArea l))
     (setf hx^FluidVolume (* nc a l))
     )
   
     
   (cond
    ((specified-number hx^NumChannels)     
     (SetByNumChannels hx^NumChannels))
    ((specified-number hx^FlowArea)
     (SetByNumChannels (/ hx^FlowArea hx^Height hx^Width)))
    ((specified-number hx^FrontalArea)
     (SetByNumChannels (/ hx^FrontalArea (+ hx^Height hx^RebroThickness) hx^Width)))
    (t (error "Coding-error"))
    )                  
   hx
   ))



(defmethod CalcHeatExchanger ((hx FlatChannelsMatrix))
  (perga
   (:lett hx FlatChannelsMatrix hx)
   (check-slots-are-dbl hx (GapThickness RebroThickness RebroLambda))
   (:lett Shag dbl (+ hx^GapThickness hx^RebroThickness))
   (setf hx^Porosity (/ hx^GapThickness Shag))
   (assert (alexandria.0.dev:xor (typep hx^FrontalArea 'non-zero-dbl)
                (typep hx^FlowArea 'non-zero-dbl)) ()
     "Должна быть задана FrontalArea либо FlowArea")
   (cond
    ((typep hx^FrontalArea 'non-zero-dbl)
     (:lett a dbl (* hx^FrontalArea hx^Porosity))
     (setf hx^FlowArea a))
    ((typep hx^FlowArea 'non-zero-dbl)
     (setf hx^FrontalArea (/ hx^FlowArea hx^Porosity)))
    (t
     (error "Неизвестным образом задана геометрия")))
   (:lett WettedPerimiter dbl
    (* hx^FrontalArea
       2
       (/ Shag)))
   (setf hx^HeatExchangeArea (* WettedPerimiter hx^Length))
   (setf hx^HydrDiam (GenericHydrDiam hx^FlowArea hx^HeatExchangeArea hx^Length))
   (setf hx^FluidVolume (* hx^FlowArea hx^Length))
   hx
   ))



(defmethod CalcHeatExchanger ((hx SequentialHeatExchangers))
  (perga
    (:lett hx SequentialHeatExchangers hx)
    (setf hx^FluidVolume 0.0)
    (setf hx^Length 0.0)
    (setf hx^HeatExchangeArea 0.0)
    (check-type hx^HydrDiam non-zero-dbl) ; можно задать любой гидравл.радиус, какой нам удобно, главное, чтобы он был не 0.
    ;(setf hx^FlowArea (CircleArea hx^HydrDiam)) ; кое-где она нужна, пусть будет вот такой
    (dolist (m hx^Members)
      (:lett m HxBase m)
      (CalcHeatExchanger m)
      (_f + hx^FluidVolume m^FluidVolume)
      (_f + hx^Length (the* non-zero-dbl m^Length))
           ; если здесь возникла ошибка, 
           ; значит мы зря пытались определить 
           ; длину для составного теплообменника.

      (_f + hx^HeatExchangeArea m^HeatExchangeArea)
      )
    hx
    ))
    
      