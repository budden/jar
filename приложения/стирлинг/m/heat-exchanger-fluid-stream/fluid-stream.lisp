; -*- Encoding: utf-8;  System :heat-exchanger-fluid-stream -*-

(in-package :fluid-stream)
(in-readtable :buddens-readtable-a)

(defgeneric CalcStream (str) (:documentation "Вычисляет второстепенные величины, возвращает поток"))

#| (defmethod CalcStream ((str AirStream1))
  (perga
   ; удостоверимся, что исходные данные определены
   (:lett vf dbl str^VolumetricFlow)
   (:lett p dbl str^Pressure)
   (:lett tt dbl str^Temperature)
   (setf str^RGas 287.0)
   (setf str^GammaGas 1.4)
   (break "не всё додедало")
   )
  ) |#


(defun strPr (str)
  (perga
    (:lett str FluidStreamWithLiquidProperties str)
    (* str^nu str^rho str^Cp (/ str^lambda))))

(defun strMu (str)
  (perga
    (:lett str FluidStreamWithLiquidProperties str)
    (* str^nu str^rho)))


(defun strMassFlow (str)
  (perga
    (:lett str FluidStreamWithLiquidProperties str)
    (* str^rho str^VolumetricFlow)))



; (defstruct WaterAt80CStream VolumetricFlow Pressure Temperature ThermalExpansionCoefficient nu rho lambda C
(defmethod CalcStream ((str WaterAt80CStream))
  (perga
   (:lett str WaterAt80CStream str)
   (setf str^nu 0.365e-6)
   (setf str^rho 971.8)
   (setf str^lambda 0.669)
   (setf str^Cp 4195.0)
   (setf str^ThermalExpansionCoefficient 6.32e-4)
   str))


(defmethod CalcStream ((str WaterAt20CStream))
  "[МихеевМихеева], таблицы в конце"
  (perga
   (:lett str WaterAt20CStream str)
   (setf str^nu 1.006e-6)
   (setf str^rho 998.2)
   (setf str^lambda 0.597)
   (setf str^Cp 4183.0)
   (setf str^ThermalExpansionCoefficient 1.82e-4)
   str))
   

  
  

(defgeneric FluidStream-SubstanceStruct (str)
  (:documentation "Возвращает вещество для потока")
  )


(defmethod FluidStream-SubstanceStruct ((str AirStream1))
  snc:Air)