; -*- Encoding: utf-8;  System :heat-exchanger-fluid-stream -*-

; регенератор

(named-readtables:in-readtable :buddens-readtable-a)

(def-merge-packages::! :epsilon-n_tu
                       (:documentation "Определение epsilon по n_tu для разных случаев, [KaysLondon]. См. также  KAYS-LONDON-PERIODIC-FLOW::EpsilonPeriodicFlow_1_infinity")
                       (:always t)
                       (:use :cl :budden-tools
                        :m0
                        :heat-exchanger-fluid-stream-literature
                        :fluid-stream
                        :heat-exchanger
                        :hx-and-str
                        :physical-units
                        )
                       (:local-nicknames :hx :heat-exchanger :str :fluid-stream
                        :kl :kays-london-notation)
                       (:custom-token-parsers budden-tools::convert-carat-to-^) 
                       (:import-from :perga-implementation
                        perga-implementation:perga)
                       (:export
 "
 epsilon-n_tu::CounterFlow
 "))

(in-package :epsilon-n_tu)


;;; 

(defparameter *FnKaysLondonTbl2-2-Zero*
  (make-piecewize-linear-scalar-fun
   #2A((0.0 0.0)
       (0.25 0.221)
       (0.50 0.393)
       (0.75 0.528)
       (1.00 0.632)
       (1.25 0.713)
       (1.50 0.777)
       (1.75 0.826)
       (2.00 0.865)
       (2.50 0.918)
       (3.00 0.950)
       (3.50 0.970)
       (4.00 0.982)
       (4.50 0.989)
       (5.00 0.993)
       (5.50 0.996)
       (1e300 1.0) ; added by budden to allow for some out-of-range cases
       )
   :pseudo-name "epsilon-n_tu::*FnKaysLondonTbl2-2-Zero*")
  )

(defun CounterFlow (N_tu CapacityRatio)
  "[KaysLondon], Table 2-2, p 50"
  (perga
    (ecase (coerce CapacityRatio 'double-float)
      (0.0
       (funcall *FnKaysLondonTbl2-2-Zero* N_tu)))))








