; -*- Encoding: utf-8;  System :heat-exchanger-fluid-stream -*-

(in-package :fluid-stream)
(in-readtable :buddens-readtable-a)

(defstruct BaseFluidStream
  "Свойства жидкости, к-рая считается несжимаемой"
  VolumetricFlow 
  Pressure
  Temperature
  ThermalExpansionCoefficient
  ; ??? ; вычисляется
  )

(defstruct (FluidStreamWithLiquidProperties (:include BaseFluidStream))
  "Свойства рабочего тела, общие для газа и жидкости"
  nu  ; кинематическая вязкость, м2/с. Динамическая вязкость - strMu
  rho 
  lambda 
  Cp
  )

(defstruct (FluidStreamWithGasProperties (:include FluidStreamWithLiquidProperties))
  "Свойства газообразного рабочего тела"
  RGas
  GammaGas
  Cv)
                    
(defstruct (AirStream1 (:include FluidStreamWithGasProperties))
  "Воздух при -40..+1200С. См. также SetStreamMassFlow"
  )

(defstruct (WaterAt80CStream (:include FluidStreamWithLiquidProperties))
  )

(defstruct (WaterAt20CStream (:include FluidStreamWithLiquidProperties))
  ; прекратить это безобразие и сделать нормальный с диапазоном!
  ) 


(defstruct (HydrogenStream (:include FluidStreamWithGasProperties))
  "Водород. Данные из программы Уриели")

(defstruct (HeliumStream (:include FluidStreamWithGasProperties))
  "Гелий. Данные из программы Уриели")

