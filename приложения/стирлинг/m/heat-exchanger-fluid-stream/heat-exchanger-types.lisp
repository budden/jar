; -*- Encoding: utf-8;  System :heat-exchanger-fluid-stream -*-

(in-package :heat-exchanger)
(in-readtable :buddens-readtable-a)

(defstruct HxBase
  "Базовый класс для теплообменников. Как правило, в теплообменнике нужно заполнить несколько полей, а затем вызвать для него CalcHeatExchanger"
  Length      ; длина, м. Задаётся
  FluidVolume ; объём жидкости внутри теплообменника, м3. Вычисляется в CalcHeatExchanger 
  FlowArea    ; условная площадь поперечного сечения для протекания жидкости, м2, вычисляется в CalcHeatExchanger 
  HeatExchangeArea ; полная площадь теплообмена, м2. Как правило, вычисляется в CalcHeatExchanger, но иногда задаётся
  HydrDiam    ; гидравлический диаметр, м, вычисляется в CalcHeatExchanger 
  (XiEntryExit 1.0) ; коэфт сопротивления (советский) при входе/выходе суммарный
  )

(m0:def-simple-struct-load-form HxBase)

(defstruct (Pipe (:include HxBase))
  "Труба. LengthNuFactorMM90 должен быть установлен для трубы при 1e4<=re<=5e6, 
  см. [МихеевМихеева] стр.90. Среднее значение по больнице = 1.2"
  PipeDiameter
  LengthNuFactorMM90 
  )

(m0:def-simple-struct-load-form Pipe)


(defstruct (КоридорныйПучокТруб (:include HxBase))
  "[МихеевМихеева] стр. 103-107,272"
  PipeDiameter
  ДлинаТрубы 
  ЧислоРядов ; Length не задаётся для этого теплообменника
  ЧислоТрубВРяду
  X1 ; расстояние по ширине пучка 
  X2 ; расстояние по глубине пучка - не участвует в расчёте теплоотдачи!
  )
  
(m0:def-simple-struct-load-form КоридорныйПучокТруб)


(defstruct (ParallelPipes (:include Pipe))
  "Несколько параллельных трубок"
  NumPipes)

(m0:def-simple-struct-load-form ParallelPipes)


(defstruct (Channel (:include HxBase))
  ChannelCrossSectionLongSide
  ChannelCrossSectionShortSide)

(m0:def-simple-struct-load-form Channel)


(defstruct (SetkaRodsKaysLondon (:include HxBase)) "Сетка согласно KaysLondon. Насколько я понял, она везде приближена прямыми пересекующимися стержнями. См. также MAKE-SetkaRodsKaysLondon-ByPorosity"
  FrontalArea SetkaWireDiam SetkaCellSize 
  Porosity ; вычисляемое
  SetkaKaysLondonHydrRadius ; вычисляемое
  NumberOfLayers ; вычисляемое
  AreaPerVolume ; вычисляемое
  SetkaLambda ; теплопроводность материала сетки
  )

(m0:def-simple-struct-load-form SetkaRodsKaysLondon)


(defstruct (SphericalBedKaysLondon (:include HxBase)) "Засыпка сфер согласно KaysLondon с пористостью 0.37-0.39 (считаем 0.38)"
  FrontalArea
  SphereDiam 
  (Porosity 0.38) ; фиксированное, не трогать
  SphericalBedKaysLondonHydrRadius ; вычисляемое
  AreaPerVolume ; вычисляемое
  )

(m0:def-simple-struct-load-form SphericalBedKaysLondon)

(defstruct (Foil (:include HxBase))
  FrontalArea FoilThickness UnrolledLengthOfFoil)

(m0:def-simple-struct-load-form Foil)

(defstruct (Annulus (:include HxBase)) "Подразумевается нагрев с обоих сторон" OuterDiam InnerDiam)

(m0:def-simple-struct-load-form Annulus)

(defstruct (FlatChannel (:include HxBase)) "Width>=Heigth . См. также FlatChannelsMatrix, FlatChannels"
  Height
  Width
  OneWallP ; t, если только одна стенка работает
  )

(m0:def-simple-struct-load-form FlatChannel)

(defstruct (FlatChannelsMatrix (:include HxBase)) "Для матрицы регенератора. См. также FlatChannel . Неявно подразумевается, что длина зазора намного больше его тлщины, но в геометрии задаётся только FrontalArea, поэтому нет возможности это проверить"
  FrontalArea ; общая площадь. При создании нужно задать ненулевым либо её, либо FlowArea
  GapThickness ; толщина зазора. Во FlatChannel она же называется Height
  RebroThickness ; толщина ребра.
  (RebroLambda 15.0) ; теплопроводность материала ребра (используется только для вычисления продольной теплопроводности)
  Porosity ; вычисляемое
  OneWallP ; t, если только одна стенка работает
  )

(m0:def-simple-struct-load-form FlatChannelsMatrix)

(defstruct (FlatChannels (:include FlatChannel)) "Несколько каналов, width>height."
  RebroThickness ; а толщина канала - это Height
  NumChannels ; либо можно задать FrontalArea
  FrontalArea
  )

(m0:def-simple-struct-load-form FlatChannels)


(defstruct (SequentialHeatExchangers (:include HxBase)) "Несколько теплообменников, соединённых последовательно, с противоположной стороны - безконечный поток"
  Members ; список теплообменников
  ; также нужно задать ненулевой гидравлический радиус 
  ;; при расчёте КПД мы вычисляем средний коэфт теплоотдачи и слепляем тем самым все теплообменники в один. Было бы более правильным 
  ;; посчитать отдельный КПД для каждого отдельного теплообменника, потом посчитать коэфт потерь и затем опять вернуться к КПД.
  ;; но для этого требуется сделать epsilon родовой функций, что требует значительного рефакторинга: у нас пока нет функции epsilon, принимающей
  ;; теплообменник и поток - эта функция принимает NTu. Тестирование показывает, что вроде бы погрешность вычисления epsilon невелика.
  )

(m0:def-simple-struct-load-form SequentialHeatExchangers)
