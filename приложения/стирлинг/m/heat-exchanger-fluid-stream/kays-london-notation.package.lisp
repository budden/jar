; -*- Encoding: utf-8;  System :heat-exchanger-fluid-stream -*-

(named-readtables:in-readtable :buddens-readtable-a)

(def-merge-packages::! :kays-london-notation
                       (:documentation "Обозначения из [KaysLondon] . Рекомендованный никнейм - :kl")
                       (:use :cl :budden-tools :m0
                        :heat-exchanger-fluid-stream-literature
                        :fluid-stream
                        :heat-exchanger
                        :hx-and-str
                        )
                       (:local-nicknames
                        :snc :substance-constants)
                       (:custom-token-parsers budden-tools::convert-carat-to-^) 
                       (:import-from :iterate-keywords
                        iterate-keywords:iter)
                       (:import-from :perga-implementation
                        perga-implementation:perga)
                       (:export
   "kays-london-notation::C ; Flow stream capacity rate, (W Cp)
    kays-london-notation::W ; mass flow rate
    kays-london-notation::N_tu_o ; modified number of transfer units (for periodic-flow heat exchangers), p.31 
    kays-london-notation::N_tu_zero ; Ntu for heat exchanger with ideal another side
    kays-london-notation::h ; unit conductance for thermal-convection heat transfer
   "
   ))
