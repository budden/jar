; -*- Encoding: utf-8;  -*-

; тесты теплообмена при протекании потока через теплообменник
; не включены (пока?) ни в какую систему, но см. систему
; :heat-exchanger-fluid-stream

(in-package :hx-and-str)
(in-readtable :buddens-readtable-a)

(deftparameter *StreamVyt* AirStream1
               :initial-value
               (MAKE-AirStream1
                :VolumetricFlow (* 10 [liter] 1/3)
                :Pressure (* 1 [Bar])
                :Temperature 295.0))

(CalcAirStream1WithoutVolumetricFlow *StreamVyt*)

(def-trivial-test::! StreamVyt.1
  (<= (abs (* 1/16 (- (* *StreamVyt*^NU 1e6) 16.00))) 0.06)
  t)

(def-trivial-test::! StreamVyt.2
  (<= (/ (- (abs *StreamVyt*^RHO) 1.165) 1.165) 0.06)
  t)
      
(def-trivial-test::! StreamVyt.3
  (<= (/ (abs (- (* *StreamVyt*^LAMBDA 1e2) 2.67)) 2.67)
      0.06)
  t)


(deftparameter *MySetka* SetkaRodsKaysLondon
               :initial-value
               (MAKE-SetkaRodsKaysLondon
                :SetkaWireDiam (* 0.21 [mm])
                :Length (* 2 (* 0.21 [mm]))
                :SetkaCellSize (* 1 [mm])
                :FrontalArea (CircleArea (* 186 [mm]))
                ;:Porosity 0.835066
                ))

(CalcHeatExchanger *MySetka*)
             

; тест сомнителен, т.к. не срабатывает в Mathematica
(def-trivial-test::! SetkaRey.1
                     (<= 10 (hxRey *MySetka* *StreamVyt*) 12)
                     t)


(def-trivial-test::! SetkaRey.2
  (perga
   (:lett hx SetkaRodsKaysLondon *MySetka*)
   (:lett str AirStream1 *StreamVyt*)
   (let re1 (hxRey hx str))
   (let innerVelocity 
    (/ str^VolumetricFlow
       (* hx^FrontalArea hx^Porosity)))
   (let re2
    (/ (* innerVelocity hx^HydrDiam)
       (fluid-stream::nu-air (* 1 [Bar])
                             str^Temperature)))
   (<= (abs (/ (- re1 re2) re1)) 0.01))
  t)


(deftparameter *PMM32* Pipe
               :initial-value
               (MAKE-Pipe :Length 0.5 :PipeDiameter (* 3 [mm])))

(CalcHeatExchanger *PMM32*)


; пока нет WaterAt80CStream
;(deftparameter *SMM32* WaterAt80CStream
;  ; strVolumetricFlow[SMM32] ^= 0.24*hxFlowArea[PMM32];
;  )

; \!\(assertXInRangeQ[hxAlpha[PMM32, SMM32]\/928, 0.85, 1.2]\)

#|
Погрешность возникает из-за неучёта т-ра и неучёта отилчия
Prж от Prс

|#


;; Проверка примера 3-3 из [МихеевМихеева]

(deftparameter *PMM33* Pipe
               :initial-value
               (MAKE-Pipe :Length 2.1 :PipeDiameter (* 60 [mm])))

(CalcHeatExchanger *PMM33*)

(deftparameter *SMM33* AirStream1
               :initial-value
               (MAKE-AirStream1
                :VolumetricFlow (* 5 *PMM33*^FlowArea)
                :Pressure (* 1 [Bar])
                :Temperature (+ 273 100.0)
                ))

(CalcAirStream1WithoutVolumetricFlow *SMM33*)

(setf *PMM33*^LengthNuFactorMM90 1.04)

(hxRey *PMM33* *SMM33*)
(hxNu *PMM33* *SMM33*)

(def-trivial-test::! MM33
                     (<= 0.95 (/ (hxAlpha *PMM33* *SMM33*) 18.18) 1.06)
                     t)


;; Проверка примера 3-4 из [МихеевМихеева]
; нужно иметь в виду, что эти ф-лы - для средней т-ры теплоносителя.
; Но она нам неизвестна, если дан теплообменник и параметры на входе

(deftparameter *PMM34* Pipe
               :initial-value
               (MAKE-Pipe :Length 3.0
                          :PipeDiameter (* 50 [mm])
                          :LengthNuFactorMM90 1.0))

(CalcHeatExchanger *PMM34*)

(deftparameter *SMM34* WaterAt80CStream
               :initial-value
               (MAKE-WaterAt80CStream :VolumetricFlow
                                      (* 0.8 *PMM34*^FlowArea)
                                      :Temperature
                                      (+ 273.0 50.0)))


(CalcStream *SMM34*)

(def-trivial-test::! MM34
                     (<= 0.85 (/ (hxAlpha *PMM34* *SMM34*) 3920) 1.15)
                     t)


(hxSt *PMM34* *SMM34*)

;; 10 параллельных трубок из того же примера

(deftparameter *PMM34x10* ParallelPipes
               :initial-value
               (MAKE-ParallelPipes
                :Length 3.0
                :PipeDiameter (* 50 [mm])
                :NumPipes 10
                :LengthNuFactorMM90 1.0))

(CalcHeatExchanger *PMM34x10*)


(deftparameter *SMM34x10* WaterAt80CStream
               :initial-value
               (MAKE-WaterAt80CStream :VolumetricFlow
                                      (* 0.8 *PMM34x10*^FlowArea)
                                      :Temperature
                                      (+ 273.0 50.0)))


(CalcStream *SMM34x10*)

(def-trivial-test::! MM34x10
                     (<= 0.999
                         (/ (hxAlpha *PMM34x10* *SMM34x10*)
                            (hxAlpha *PMM34* *SMM34*))
                         1.001)
                     t)


;; Начертим Nu для трубы (а с чем сравнивать?)

(defun NuSt (x)
  (perga
   (:lett hx Pipe (MAKE-Pipe
                   :PipeDiameter (* 10 [mm])
                   :Length (* 1000 [mm])
                   :LengthNuFactorMM90 1.0
                   ))
   (CalcHeatExchanger hx)
   (:lett str AirStream1
    (MAKE-AirStream1
     :Pressure (* 1 [Bar])
     :Temperature 400.0
     :VolumetricFlow x
     ))
   (CalcAirStream1WithoutVolumetricFlow str)
   (values
    (hxRey hx str)
    (hxNu hx str)
    (hxSt hx str))))

(defun PlotNuStPipe ()
  (perga
    (:lett range mjr_vec (integer-range-3 1e-5 1e-5 3e-3))
    (BUDDEN-ADW-PLOTTING:plot-adw
     (map 'vector (lambda (x) (nth-value 0 (NuSt x))) range)
     (MJR_MAT:mjr_mat_cv2m
      (map 'vector (lambda (x) (nth-value 2 (NuSt x))) range))
     :x-data-interval 2500.0
     :title "NuSt для трубы"
     )))

(PlotNuStPipe)


; и для канала начертим
(defun NuStAir300Channel (flow height width length)
  (perga
   (:lett hx FlatChannel (MAKE-FlatChannel :Height height :Width width :Length length))
   (CalcHeatExchanger hx)
   (:lett str AirStream1
    (MAKE-AirStream1
     :Pressure (* 1 [Bar])
     :Temperature 300.0
     :VolumetricFlow flow
     ))
   (CalcAirStream1WithoutVolumetricFlow str)
   ;(show-expr `(,(hxRey hx str) ,(* (hxSt hx str) (expt (strPr str) 2/3))))
   (values
    (hxRey hx str)
    (hxNu hx str)
    (hxSt hx str))))



(defun PlotNuStChannel (h w l)
  (perga
    (:lett range mjr_vec (integer-range-3 6e-4 2e-5 1e-2))
    (BUDDEN-ADW-PLOTTING:plot-adw
     (map 'vector (lambda (x) (nth-value 0 (NuStAir300Channel x h w l))) range)
     (MJR_MAT:mjr_mat_cv2m
      (map 'vector (lambda (x) (nth-value 2 (NuStAir300Channel x h w l))) range))
     ; :x-data-interval 2500.0
     :title "NuSt для канала"
     )))

(PlotNuStChannel ([mm] 10) ([mm] 80) ([mm] 1000))


(deftparameter *hxSKL83* SetkaRodsKaysLondon
               :initial-value
               (MAKE-SetkaRodsKaysLondon
                :SetkaWireDiam (* 0.216 [mm])
                :SetkaCellSize (* 1 [mm])
                :Length (* 50 [mm])
                :FrontalArea (CircleArea (* 50 [mm]))))

(CalcHeatExchanger *hxSKL83*)

(def-trivial-test::! hxSkl83.1a
                     (<= 0.829 *hxSKL83*^Porosity 0.831)
                     t)

(deftparameter *stSKL83* AirStream1
               :initial-value
               (MAKE-AirStream1
                :VolumetricFlow (* 1.637 [liter])
                :Pressure (* 1 [Bar])
                :Temperature 295.0))

(CalcAirStream1WithoutVolumetricFlow *stSKL83*)

(def-trivial-test::! stSKL83.2
                     (<= 69 (hxRey *hxSKL83* *stSKL83*) 71)
                     t)



; ну и диапазончик! FIXME Проверить 
(def-trivial-test::! stSKL83.3
                     (<= 0.01
                         (* (hxSt *hxSKL83* *stSKL83*)
                            (expt (strPr *stSKL83*) 2/3))
                         0.4)
                     t)

(def-trivial-test::! stSKL8.3
                     (<= 1.33
                         (FanningFactor *hxSKL83* *stSKL83*)
                         1.342)
                     t)


;;; То же, другая длина

(deftparameter *hxSKL8310* SetkaRodsKaysLondon
               :initial-value
               (MAKE-SetkaRodsKaysLondon
                :SetkaWireDiam (* 0.216 [mm])
                :SetkaCellSize (* 1 [mm])
                :Length (* 10 [mm])
                :FrontalArea (CircleArea (* 50 [mm]))))

(CalcHeatExchanger *hxSKL8310*)

(def-trivial-test::! hxSkl83.1b
                     (<= 0.829 *hxSKL8310*^Porosity 0.831)
                     t)


(deftparameter *hxSKL725* SetkaRodsKaysLondon
               :initial-value
               (MAKE-SetkaRodsKaysLondon
                :SetkaWireDiam (* 0.0032 [mm])
                :SetkaCellSize (* (+ 0.0064 0.0032) [mm])
                :Length (* 10 [mm])
                :FrontalArea (CircleArea (* 50 [mm]))))

(CalcHeatExchanger *hxSKL725*)

(def-trivial-test::! hxSkl725.1
                     (<= 0.710 *hxSKL725*^Porosity 0.740)
                     t)


(deftparameter *stSKL725* AirStream1
               :initial-value
               (MAKE-AirStream1
                :VolumetricFlow (* 235 [liter])
                :Pressure (* 1 [Bar])
                :Temperature 295.0))

(CalcAirStream1WithoutVolumetricFlow *stSKL725*)

(def-trivial-test::! stSKL725.1
                     (<= 0 (hxRey *hxSKL83* *stSKL83*) 10100)
                     t)

(def-trivial-test::! stSKL725.2
                     (<= 0
                         (* (hxSt *hxSKL725* *stSKL725*)
                            (expt (strPr *stSKL725*) 2/3))
                         300)
                     t)


(defun ReyAndStOf725KLMeshStack (x)
  (perga
   (:lett st AirStream1
    (MAKE-AirStream1
     :VolumetricFlow x
     :Pressure (* 1 [Bar])
     :Temperature 295.0))
    (CalcAirStream1WithoutVolumetricFlow st)
    (values
     (hxRey *hxSKL725* st)
     (* (hxSt *hxSKL725* st)
        (expt (strPr st) 2/3)))))


(defun plot-kl-mesh-and-fn-kays-london-ris-78 (range)
  (perga
   (:lett vKL mjr_vec
    (map
     'vector
     (lambda (x)
       (nth-value 1 (ReyAndStOf725KLMeshStack x))) range))
   (:lett vRis78-725 mjr_vec
    (map 
     'vector
     (lambda (x)
       (FnKaysLondonRis78 (* 500 x) 0.725))
     range))
   (:lett vRis78-766 mjr_vec
    (map 
     'vector
     (lambda (x)
       (FnKaysLondonRis78 (* 500 x) 0.766))
     range))
   (:lett vRis78-832 mjr_vec
    (map 
     'vector
     (lambda (x)
       (FnKaysLondonRis78 (* 500 x) 0.832))
     range))
   (let mm ; transposed as we can't add columns yet
     (MJR_MAT:mjr_mat_make-zero 4 (length range)))
   (add-vector-to-row-of-matrix mm 0 vKL)
   (add-vector-to-row-of-matrix mm 1 vRis78-725)
   (add-vector-to-row-of-matrix mm 2 vRis78-766)
   (add-vector-to-row-of-matrix mm 3 vRis78-832)
   (_f MJR_MAT:mjr_mat_transpose mm)
   (BUDDEN-ADW-PLOTTING:plot-adw
    (map
     'vector
     (lambda (x)
       (nth-value 0 (ReyAndStOf725KLMeshStack x))) range)
    mm  
    :title "Re vs St 0=KLMesh, 1=FnKaysLondonRis78-725, 2=766, 3=832"
    :x-data-interval (* 10 [liter])
    ; не работает. Почему? (BUDDEN-ADW-PLOTTING::make-next-point-by-sequence-fn range)
    )))

(plot-kl-mesh-and-fn-kays-london-ris-78
 (integer-range-3 (* 30 [liter]) (* 10 [liter]) (* 100 [liter])))

(plot-kl-mesh-and-fn-kays-london-ris-78
 (integer-range-3 (* 100 [liter]) (* 100 [liter]) (* 4000 [liter])))



(defun plot-channel-fanning-factor-at-range (range)
  "Рисуем канал в ламинарном и переходном режиме"
  (perga
    (:lett hx FlatChannel
     (CalcHeatExchanger
      (MAKE-FlatChannel
       :Length ([mm] 35.0)
       :Width ([mm] 4)
       :Height ([mm] 0.5)
       )))
    (flet f (v)
      (:lett str AirStream1
       (CalcStream
        (MAKE-AirStream1
         :Temperature ([Celsius] 0)
         :Pressure [Bar]
         :VolumetricFlow v)))
      (values
       (hxRey hx str)
       (FanningFactor hx str)))
    #|(let N (length range))
    (let mm 
      (MJR_MAT:mjr_mat_make-zero 2))
    (dotimes (i N)
      (:@ mlvl-bind (re fa) (f (elt range i)))
      (setf [ mm i 0 ] re)
      (setf [ mm i 1 ] fa))
   (_f MJR_MAT:mjr_mat_transpose mm)|#
   (BUDDEN-ADW-PLOTTING:plot-adw
    (map
     'vector
     (lambda (x)
       (nth-value 0 (f x))) range)
    (MJR_MAT:mjr_mat_cv2m
     (map
      'vector
      (lambda (x)
        (nth-value 1 (f x))) range))
    :title "Kays-London Fig 7-4"
    ; :x-data-interval (* 10 [liter])
    )))


(plot-channel-fanning-factor-at-range (integer-range-3 1e-5 2e-6 2e-4))


(defun plot-annulus-fanning-factor-at-range (range)
  "Рисуем канал в ламинарном и переходном режиме"
  (perga
    ;(let range )
    (:lett hx Annulus
     (CalcHeatExchanger
      (MAKE-Annulus
       :Length ([mm] 35.0)
       :OuterDiam ([mm] 4)
       :InnerDiam ([mm] 3.5)
       )))
    (flet f (v)
      (:lett str AirStream1
       (CalcStream
        (MAKE-AirStream1
         :Temperature ([Celsius] 0)
         :Pressure [Bar]
         :VolumetricFlow v)))
      (values
       (hxRey hx str)
       (FanningFactor hx str)))
    #|(let N (length range))
    (let mm 
      (MJR_MAT:mjr_mat_make-zero 2))
    (dotimes (i N)
      (:@ mlvl-bind (re fa) (f (elt range i)))
      (setf [ mm i 0 ] re)
      (setf [ mm i 1 ] fa))
   (_f MJR_MAT:mjr_mat_transpose mm)|#
   (BUDDEN-ADW-PLOTTING:plot-adw
    (map
     'vector
     (lambda (x)
       (nth-value 0 (f x))) range)
    (MJR_MAT:mjr_mat_cv2m
     (map
      'vector
      (lambda (x)
        (nth-value 1 (f x))) range))
    :title "Kays-London Fig 7-4"
    ; :x-data-interval (* 10 [liter])
    )))


; Проверяем соответствие между FlatChannel и FlatChannels
(defparameter *hxCVCs1*
  (CalcHeatExchanger
   (MAKE-FlatChannel :Length 1.0
                     :Height ([mm] 1)
                     :Width ([mm] 500))))

(defparameter *hxCVCs2*
  (CalcHeatExchanger
   (MAKE-FlatChannels :Length 1.0
                      :Height ([mm] 1)
                      :Width ([mm] 250)
                      :NumChannels 2
                      :RebroThickness ([mm] 1))))

(defparameter *strCVCs1*
  (CalcStream
   (MAKE-AirStream1
    :Pressure [Bar]
    :Temperature 273.0
    :VolumetricFlow ([liter] 20))))

(def-trivial-test::! ChannelVsChannels.1
                     (approx-equal
                      *hxCVCs1*^FluidVolume
                      *hxCVCs2*^FluidVolume
                      0.001)
                     t)

(def-trivial-test::! ChannelVsChannels.2
                     (approx-equal
                      (hxNu *hxCVCs1* *strCVCs1*)
                      (hxNu *hxCVCs2* *strCVCs1*)
                      1/250)
                     t)

(def-trivial-test::! ChannelVsChannels.3
                     (approx-equal
                      (hxPressureDrop *hxCVCs1* *strCVCs1*)
                      (hxPressureDrop *hxCVCs2* *strCVCs1*)
                      1/250)
                     t)

(plot-annulus-fanning-factor-at-range (integer-range-3 1e-4 1e-5 1e-3))
(plot-annulus-fanning-factor-at-range (integer-range-3 1e-3 1e-4 1e-1))

;; [МихеевМихеева], стр. 96, Пример 3-6
(defparameter *hxMM36*
  (free-convection:MAKE-OutsideOfVerticalPipeInUnlimitedSpace :Length 4.0 :PipeDiameter ([mm] 100))
  )

(CalcHeatExchanger *hxMM36*)

(defparameter *strMM36*
  (MAKE-AirStream1 :Temperature (+ 273.0 30) :Pressure ([atm] 1)))

(CalcAirStream1WithoutVolumetricFlow *strMM36*)

(def-trivial-test::! MM36 ; ошибка из-за неточных вычислений. Плюс в учебнике ошибка в промежуточных 
                          ; результатах
 (<= 0.9 (/ (free-convection:FreeConvectionHeatExchangePower *hxMM36* *strMM36* (- 170.0 30)) 1620) 1.1)
 t)

(defparameter *hxMM36A*
  (FREE-CONVECTION:MAKE-VerticalPlateInUnlimitedSpace
   :h 4.0
   :l (* 0.5 (CircleLength ([mm] 100)))))

(CalcHeatExchanger *hxMM36A*)

(def-trivial-test::! VerticalPlateInUnlimitedSpace.1
                     ; нет численного примера в [МихеевМихеева], поэтому берём такую же пластину, как была у нас труба
                     (free-convection:FreeConvectionHeatExchangePower *hxMM36A* *strMM36* (- 170.0 30))
                     (free-convection:FreeConvectionHeatExchangePower *hxMM36* *strMM36* (- 170.0 30)))


;; [МихеевМихеева], стр. 100, пример 3-7
(defparameter *hxMM37*
  (FREE-CONVECTION:MAKE-ПрослойкаМеждуПараллельнымиГоризонтальнымиПоверхностями :Delta ([mm] 25) :PlateArea 1.0))

(CalcHeatExchanger *hxMM37*)

(defparameter *strMM37*
  (MAKE-AirStream1
   :Temperature ([Celsius] 100)
   :Pressure [atm]
   ))

(CalcStream *strMM37*)

(def-trivial-test::! ПрослойкаМеждуПараллельнымиГоризонтальнымиПоверхностямию.1
                     (perga
                       (let DeltaT 100.0)
                       (let result 
                         (FREE-CONVECTION:FreeConvectionHeatExchangePower
                          *hxMM37* *strMM37* DeltaT))
                       (<= 350 result 352))
                     t)

(defparameter *AvcPaju1111*
  (MAKE-AirStream1 ; this is a fake air stream. It is an Air/Water mix indeed!
   :Temperature (+ 30 273.0)
   :Pressure ([mm.hg] 765)
   :RHO 1.102
   ))

#| (def-trivial-test::! Diffusion.11.11
                     ; [АвчуховПаюсте], стр. 88, 11.11
                     (* (/ 0.038 ([mm] 10))
                        (^ HX-AND-STR::*AvcPaju1111* rho)
                        (diffusion-drying:D
                         hx-and-str::*AvcPaju1111*
                         substance-constants:Water))
                     
                     1.08e-8 ; разошлось на 4 порядка
                     ) |# 



; (def-trivial-test::! Diffusion1


(defun approx-equal-fn (relative-error)
  "Возвращает функцию двух аргументов. Аргументы должны быть числами. Возвращаемая ф-я от аргументов вернёт истину, если
 отношение её аргументов находится в диапазоне 1 - relative-error .. 1 + relative-error "
  (lambda (x y)
    (< (- 1 relative-error) (/ x y) (+ 1 relative-error))))


(defun approx-equal-sequences-fn (relative-errors)
  "Relative-errors может числом или последовательностью (sequence) чисел. Возращает функцию от двух аргументов. Эта ф-я принимает 
две последовательности. Она вернёт истину, если одноимённые элементы списков близки друг к другу в смысле approx-equal-fn. Если relative-errors - число, то каждая пара сравниваемых элементов должны отклоняться не более, чем на relative-errors. Если relative-errors - последовательность, то пара должна отклоняться не более, чем на соответствующий элемент из relative-errors"
  (lambda (x y)
    (perga 
      (:@ block all)
      (assert (= (length x) (length y)))
      (etypecase relative-errors
        (number)
        (sequence (assert (= (length x) (length relative-errors)))))
      (dotimes (i (length x))
        (let xx (elt x i))
        (let yy (elt y i))
        (let err
          (etypecase relative-errors
            (number relative-errors)
            (sequence (elt relative-errors i))))
        (let err-fn (approx-equal-fn err))
        (unless (funcall err-fn xx yy)
          (return-from all nil)))
      t)))

; [Уокер], стр. 132
(def-trivial-test::! HydrogenStream.250
                     (perga
                       (:lett x HydrogenStream (MAKE-HydrogenStream :Pressure [Bar] :Temperature 250.0))
                       (CalcStream x)
                       (let mu (* x^nu x^rho))
                       (list x^Cp x^rho mu x^lambda))
                     '(14.0e3 0.098 7.92e-6 156e-3)
                     :test (approx-equal-sequences-fn 0.04))

                  

(def-trivial-test::! HydrogenStream.500
                     (perga
                       (:lett x HydrogenStream (MAKE-HydrogenStream :Pressure [Bar] :Temperature 500.0))
                       (CalcStream x)
                       (let mu (* x^nu x^rho))
                       (list x^Cp x^rho mu x^lambda))
                     '(14.51e3 0.0491 12.64e-6 271.8e-3)
                     :test (approx-equal-sequences-fn 0.05))


(def-trivial-test::! HydrogenStream.1000
                     (perga
                       (:lett x HydrogenStream (MAKE-HydrogenStream :Pressure [Bar] :Temperature 1000.0))
                       (CalcStream x)
                       (let mu (* x^nu x^rho))
                       (list x^Cp x^rho mu x^lambda)
                       )
                     '(14.98e3 0.0264 20.13e-6 452e-3)
                     :test (approx-equal-sequences-fn 0.14))



; [Уокер], стр. 132
(def-trivial-test::! HeliumStream.250
                     (perga
                       (:lett x HeliumStream (MAKE-HeliumStream :Pressure [Bar] :Temperature 250.0))
                       (CalcStream x)
                       (let mu (* x^nu x^rho))
                       (list x^Cp x^rho mu x^lambda))
                     '(5.19e3 0.195 18.4e-6 134.0e-3)
                     :test (approx-equal-sequences-fn 0.04))

(def-trivial-test::! HeliumStream.500
                     (perga
                       (:lett x HeliumStream (MAKE-HeliumStream :Pressure [Bar] :Temperature 500.0))
                       (CalcStream x)
                       (let mu (* x^nu x^rho))
                       (list x^Cp x^rho mu x^lambda))
                     '(5.19e3 0.097 29.3e-6 202.6e-3)
                     :test (approx-equal-sequences-fn 0.03))


(def-trivial-test::! HeliumStream.1000
                     (perga
                       (:lett x HeliumStream (MAKE-HeliumStream :Pressure [Bar] :Temperature 1000.0))
                       (CalcStream x)
                       (let mu (* x^nu x^rho))
                       (list x^Cp x^rho mu))
                     '(5.19e3 0.048 46.7e-6)
                     :test (approx-equal-sequences-fn 0.09))                    
                       

                     

#| Попытка сверки с provoloki8mm

 (perga
   (let hx (hx:MAKE-FlatChannel :Length ([mm] 8) :Height ([mm] 2.6) :Width ([mm] 100)))
   (hx:CalcHeatExchanger hx)
   (let str (str:MAKE-AirStream1 :VolumetricFlow (* hx^FlowArea 0.15 3 (/ 2.6)) :Temperature 300.0 :Pressure [Bar]))
   (str:CalcStream str)
   (show-expr (* [mm] 0.1 (str:strMassFlow str) (/ hx^Width)))
   (show-expr (hx-and-str:hxRey hx str))
   (show-expr (HX-AND-STR:hxAlpha hx str))
   (show-expr (HX-AND-STR:hxPressureDrop hx str))
   (show-expr (HX-AND-STR:hxSkorostqVSechenii hx str))
   (show-expr (KAYS-LONDON-NOTATION:N_tu_zero hx str))
   (values))
 =>

(* [mm] 0.1 (FLUID-STREAM:strMassFlow str) (/ (^ hx "Width"))) = 5.243682310469314E-8 - совпало
(HX-AND-STR:hxRey hx str) = 55.59422949630108 - ламинарное
(HX-AND-STR:hxAlpha hx str) = 39.68341096153846 - см. ниже, более-менее
(HX-AND-STR:hxPressureDrop hx str) = 0.04615435821063621 - более-менее
(HX-AND-STR:hxSkorostqVSechenii hx str) = 0.17307692307692307 - не с чем сверять
(KAYS-LONDON-NOTATION:N_tu_zero hx str) = 1.236157773565621

 В ANSYS, D:\energy\meteo\shel-ansys\provoloki8mm\provoloki8mm0dot15ms
 дают следующее:
 толщина слоя 0.1мм

 IntakePressure 	5.044e-02 [Pa] - при втрое более мелкой сетке - 0.054Па
 ExhaustTemperature 	2.925e+02 [K]
 мощность теплоотдачи 	-2.585e+02 [W m^-2] (перепад 10С)
 скорость перед входом в решётку 0.15м/с
 массовый поток         5.2939e-08

 длина ребра 8мм, толщина 0.4мм, шаг ребра 3.0мм, т.е. расстояние - 2.6. 

 попробуем совместить коэфт теплоотдачи:

 (HX-AND-STR:СреднелогарифмическийТемпературныйНапор 10 2.5) -> 5.41 
 * 39.68 ; вычисленная по ф-ле alpha
   5.41  ; средний напор
 -> 214.7 Как ни странно, не очень далеко от 258

 Теперь подход через NTU. [KaysLondon], 
 (N_TU-EPSILON:CounterFlow 1.236 0) -> 0.708 
 (* 10 (- 1 0.708)) -> 2.92. Как ни странно, тоже похоже на 2.925. 

 Давление. Берём с рисунка распределение скоростей и пытаемся
 посчитать по определению вязкости [МихеевМихеева], стр. 31

 (* (/ 4e-2 ; перепад скорости
       0.0001 ; расстояние
       )
    18.6e-6 ; вязкость воздуха при 30С из таблицы
    ([mm] 8) ; длина
    (/ (*
        0.5  ; две стороны
        ([mm] 3) ; фронтальное сечение зазора (при длине 1 м)
        ))) => 0.039 - по порядку величины похоже на ANSYS
 

 [KaysLondon] стр. 121, hbc/ 8/2
 1/alpha = 0. 
 (f Re) = 24
 Re = 56
 f = 24/56 = 0.42
 darci factor = 1.71 (а в нашем расчёте бело 1.67 - очень близко)

 Итак, совпадение считаем приемлемым 

|#



;; Проверка примера 3-10 из [МихеевМихеева], но угол атаки 90

(deftparameter *PMM3-10* КоридорныйПучокТруб
               :initial-value
               (MAKE-КоридорныйПучокТруб
                :ЧислоРядов 8
                :PipeDiameter ([mm] 40)
                :x1 (* [mm] 40 1.8)
                :x2 (* [mm] 40 2.3)
                :ДлинаТрубы 1.0
                :ЧислоТрубВРяду 1.0))

(CalcHeatExchanger *PMM3-10*)

(deftparameter *SMM3-10* AirStream1
               :initial-value
               (MAKE-AirStream1
                :VolumetricFlow (* 10.0 *PMM3-10*^FlowArea)
                :Pressure (* 1 [Bar])
                :Temperature ([Celsius] 300.0)
                ))

(CalcAirStream1WithoutVolumetricFlow *SMM3-10*)

(hxRey *PMM3-10* *SMM3-10*)
(hxNu *PMM3-10* *SMM3-10*)

(def-trivial-test::! MM3-10
                     (<= 0.9 (/ (hxAlpha *PMM3-10* *SMM3-10*) 79) 1.1)
                     t)

; тесты теплообмена при протекании потока через теплообменник
; не включены (пока?) ни в какую систему, но см. систему
; :heat-exchanger-fluid-stream

(in-package :hx-and-str)
(in-readtable :buddens-readtable-a)


;;; Последовательное соединение из 1-го теплообменника
(defun MakeSeqPipe1 (length)
  "Создаёт новый экземпляр трубы"
  (MAKE-Pipe
   :Length length
   :PipeDiameter ([mm] 4)
   :LengthNuFactorMM90 1.0
   ))


(deftparameter *SeqPipe1* Pipe :initial-value (MakeSeqPipe1 50.0))

(deftparameter *Seq1* SequentialHeatExchangers
  :initial-value
  (MAKE-SequentialHeatExchangers
   :HydrDiam ([mm] 40) ; его можно задавать произовльно
   :Members (list *SeqPipe1*)))

(CalcHeatExchanger *Seq1*)

(deftparameter *StrSeq1* AirStream1
               :initial-value
               (MAKE-AirStream1
                :VolumetricFlow 0.0001
                :Pressure [Bar]
                :Temperature ([Celsius] 0.0)))




(CalcStream *StrSeq1*)

(def-trivial-test::! Seq.1.1
                     (EPSILON-N_TU:CounterFlow (KAYS-LONDON-NOTATION:N_tu_zero *Seq1* *StrSeq1*) 0.0)
                     (EPSILON-N_TU:CounterFlow (KAYS-LONDON-NOTATION:N_tu_zero *SeqPipe1* *StrSeq1*) 0.0))


(def-trivial-test::! Seq.1.2
                     (hxPressureDrop *Seq1* *StrSeq1*)
                     (hxPressureDrop *SeqPipe1* *StrSeq1*)
                     )


(deftparameter *Seq2* SequentialHeatExchangers
  :initial-value
  (MAKE-SequentialHeatExchangers
   :HydrDiam ([mm] 40) ; его можно задавать произовльно
   :Members (list
             (MakeSeqPipe1 50.0)
             (MakeSeqPipe1 0.1))))

(CalcHeatExchanger *Seq2*)

(def-trivial-test::! Seq.2.2
                     (+ (hxPressureDrop (elt *Seq2*^Members 0) *StrSeq1*)
                        (hxPressureDrop (elt *Seq2*^Members 1) *StrSeq1*))
                     (hxPressureDrop *Seq2* *StrSeq1*)
                     )


(def-trivial-test::! Seq.2.1
                     (<=
                      0.95
                      (/ 
                       (- 1
                          (*
                           (- 1 (EPSILON-N_TU:CounterFlow (KAYS-LONDON-NOTATION:N_tu_zero (elt *Seq2*^Members 0) *StrSeq1*) 0.0))
                           (- 1 (EPSILON-N_TU:CounterFlow (KAYS-LONDON-NOTATION:N_tu_zero (elt *Seq2*^Members 1) *StrSeq1*) 0.0))
                           ))
                       (EPSILON-N_TU:CounterFlow (KAYS-LONDON-NOTATION:N_tu_zero *Seq2* *StrSeq1*) 0.0)
                       )
                      1.05)
                     t)
                     





;; --------------  [KaysLondon] - p.320 Example 4 ---------------------------
(deftparameter *hxSKLEx4* SetkaRodsKaysLondon
               :initial-value
               (CalcHeatExchanger
                (MAKE-SetkaRodsKaysLondon-ByPorosity :SetkaWireDiam ([inch] 0.0135)
                                                     :Length ([mm] 21)
                                                     :Porosity 0.725
                                                     :FrontalArea 0.989)))

(def-trivial-test::! hxSKLEx4.1a
                     (approx-equal *hxSKLEx4*^Porosity 0.725 0.02)
                     t)

(def-trivial-test::! hxSKLEx4.1b
                     (approx-equal *hxSKLEx4*^AreaPerVolume 3215 0.03)                     
                     t)

(def-trivial-test::! hxSKLEx4.1c
                     (approx-equal *hxSKLEx4*^HeatExchangeArea 67.8 0.03)
                     t)

(def-trivial-test::! hxSKLEx4.1d
                     (approx-equal *hxSKLEx4*^HeatExchangeArea 67.8 0.03)
                     t)
                      
(def-trivial-test::! hxSKLEx4.1e
                     (approx-equal (/ *hxSKLEx4*^HydrDiam 4) 2.26e-4 0.03)
                     t)

(def-trivial-test::! hxSKLEx4.1f
                     (approx-equal *hxSKLEx4*^FlowArea 0.716 0.03)
                     t)

(deftparameter *stSKLEx4* AirStream1
               :initial-value
               (SetStreamMassFlow
                (CalcAirStream1WithoutVolumetricFlow
                 (MAKE-AirStream1
                  :Pressure 3.25e5
                  :Temperature ([Celsius] 383)))
                 9.5))

(def-trivial-test::! stSKLEx4.2a
                     (approx-equal (hxRey *hxSKLEx4* *stSKLEx4*) 376 0.03)
                     t)

(def-trivial-test::! stSKLEx4.2b
                     (approx-equal (hxSt *hxSKLEx4* *stSKLEx4*) 0.0744 0.03)
                     t)

(def-trivial-test::! stSKLEx4.2c
                     (approx-equal (hxAlpha *hxSKLEx4* *stSKLEx4*) 1050 0.03)
                     t)

(def-trivial-test::! stSKLEx4.2d
                     (approx-equal
                      (KAYS-LONDON-NOTATION:N_tu_zero *hxSKLEx4* *stSKLEx4*)
                      7.04
                      0.03)
                     t)

;;; чтобы проверить эффективность регенератора, не хватает забитых табличных данных


(def-trivial-test::! stSKLEx4.3a
                     (approx-equal
                      (FanningFactor *hxSKLEx4* *stSKLEx4*)
                      0.46
                      0.2 ; причина расхождения - в том, что в [KaysLondon] считается по графику,
                          ; а у нас - не слишком точная формула из Уриели
                      )
                     t)

(def-trivial-test::! stSKLEx4.3b
                     (approx-equal
                      (/ (hxPressureDrop *hxSKLEx4* *stSKLEx4*) *stSKLEx4*^Pressure)
                      0.691e-2
                      0.2 ; причина расхождения - см. stSKLEx4.3a 
                      )
                     t)



; http://www.engineeringtoolbox.com/pressure-drop-compressed-air-pipes-d_852.html
(defparameter *hxEngineeringtoolboxPipe1*
  (CalcHeatExchanger
   (MAKE-Pipe :Length 100.0
              :PipeDiameter ([mm] 20)
              :LengthNuFactorMM90 1.0
              :XiEntryExit 0.0
              )))

(defparameter *strEngineeringtoolboxPipe1*
  (CalcStream
   (MAKE-AirStream1
    :Pressure ([Bar] 0.98 7)
    :VolumetricFlow (* (/ 1 60.0) (/ (/ 0.98) 7)) ; 1m/s atmospheric
    :Temperature ([Celsius] 20))))


;(defparameter *strEngineeringtoolboxPipe1*
;  (CalcStream
;   (MAKE-AirStream1
;    :Pressure ([Bar] 1)
;    :VolumetricFlow (/ 1 60.0)
 ;   :Temperature 300.0)))


(hxRey *hxEngineeringtoolboxPipe1* *strEngineeringtoolboxPipe1*)

; у нас получилось существенно ниже!
(def-trivial-test::! EngineeringToolbox.1
 (approx-equal
  (hxPressureDrop *hxEngineeringtoolboxPipe1* *strEngineeringtoolboxPipe1*)
  ([Bar] 0.34)
  0.5
  )
 t)

(hxRey *hxEngineeringtoolboxPipe1* *strEngineeringtoolboxPipe1*)


; http://bastion7.ru/faq3.html
#| Попытка применитьформулу к потоку *strEngineeringtoolboxPipe1* через *hxEngineeringtoolboxPipe1*
 совсем не удаётся - разница в несколько раз |#

(defparameter *Bastion7PressureDrop*
  ([Bar] 450 
     (/
      (* 
       (expt (* 1000 *strEngineeringtoolboxPipe1*^VolumetricFlow (/ *strEngineeringtoolboxPipe1*^Pressure [Bar])) 1.185)
       *hxEngineeringtoolboxPipe1*^Length)
      (*
       (expt (/ *hxEngineeringtoolboxPipe1*^PipeDiameter [mm]) 5)
       (/ *strEngineeringtoolboxPipe1*^Pressure [Bar])))))

(def-trivial-test::! EngineeringToolbox.2
 (approx-equal
  (hxPressureDrop *hxEngineeringtoolboxPipe1* *strEngineeringtoolboxPipe1*)
  *Bastion7PressureDrop*
  4 ; разница в 4 раза! 
  )
 t)



#| По трубопроводу, длина которого 23 м и внутренний диаметр 80 мм, протекает поток 140 л/с. Трубопровод имеет 8 колен с диаметрами закруглений, равными внутреннему диаметру. Чему равно падение давления в трубопроводе, если абсолютное давление равно 8 бар (абс.)?
Вначале нужно определить эквивалентную длину 8 колен. Из таблицы 3:36 следует, что эквивалентная длина каждого из колен составляет 1,3 м. Отсюда общая длина трубопровода равна: 8 х 1,3 + 23 = 33,4 м. Для определения падения давления используется следующая формула: ([Bar] 0.0054)
 |#
(defparameter *Bastion7Pipe*
  (CalcHeatExchanger
   (MAKE-Pipe :Length 33.4
              :XiEntryExit 0.0
              :PipeDiameter ([mm] 80))))


(defparameter *Bastion7Stream*
  (CalcStream
   (MAKE-AirStream1
    :VolumetricFlow (/ 0.14 8)
    :Pressure ([Bar] 8)
    :Temperature ([Celsius] 20))))

(def-trivial-test::! Bastion7.1
                     (approx-equal 
                      (hxPressureDrop *Bastion7Pipe* *Bastion7Stream*)
                      ([Bar] 0.0054) 
                      0.3) ; наша формула занижает
                     t)

(hxRey *Bastion7Pipe* *Bastion7Stream*)                  
                   


; http://rosinmn.ru/napor.html
(defparameter *hxRosinMNPipe1*
  (CalcHeatExchanger
   (MAKE-Pipe :Length 50.0
              :PipeDiameter ([mm] 80)
              :LengthNuFactorMM90 1.0
              )))

(defparameter *strRosinMNPipe1*
  (CalcStream
   (MAKE-AirStream1
    :Pressure ([MPa] 1)
    :VolumetricFlow 0.001
    :Temperature ([Celsius] 20))))


(def-trivial-test::! RosinMN.1a
 (approx-equal
  (hxPressureDrop *hxRosinMNPipe1* *strRosinMNPipe1*)
  4.47
  0.1
  )
 t)

(defparameter *strRosinMNPipe2*
  (CalcStream
   (MAKE-AirStream1
    :Pressure ([MPa] 1)
    :VolumetricFlow 0.01
    :Temperature ([Celsius] 20))))

(def-trivial-test::! RosinMN.1b
  (approx-equal
   (hxRey *hxRosinMNPipe1* *strRosinMNPipe1*)
   10500
   0.03)
  t)


(def-trivial-test::! RosinMN.2
 (approx-equal
  (hxPressureDrop *hxRosinMNPipe1* *strRosinMNPipe2*)
  259
  0.1
  )
 t)

(defparameter *strRosinMNPipe3*
  (CalcStream
   (MAKE-AirStream1
    :Pressure ([MPa] 0.1)
    :VolumetricFlow 0.0001
    :Temperature ([Celsius] 20))))    

(def-trivial-test::! RosinMN.3
 (approx-equal
  (hxPressureDrop *hxRosinMNPipe1* *strRosinMNPipe3*)
  0.0886
  0.02
  )
 t)              
