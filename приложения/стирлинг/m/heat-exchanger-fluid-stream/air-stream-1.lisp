; -*- Encoding: utf-8;  System :heat-exchanger-fluid-stream -*-


(in-package :fluid-stream)
(in-readtable :buddens-readtable-a)

(defparameter *lambda-air-fn-unscaled*
  (make-piecewize-linear-scalar-fun
   ; FIXME по-хорошему, для создания матриц нужен ридмакрос.
   #2A((-50 2.04) (30 2.67) (300 4.60) (400 5.21)
       (500 5.74) (600 6.22) (700 6.71) (800 7.18) 
       (900 7.63) (1200 9.15))
   :allow-extrapolation #(-90 1200)
   :pseudo-name "*lambda-air-fn-unscaled*"))


(defun lambda-air (tk) 
  "Теплопроводность воздуха от температуры. [МихеевМихеева], таблица в конце"
  (* (funcall *lambda-air-fn-unscaled* (- tk 273.0)) 1.0e-2))

(defun mu-air (tk)
  (* 17.08e-6 (/ (+ 273 112) (+ tk 112)) (expt (/ tk 273) 3/2)))

(defun rho-air (p tk)
  (/ (* 0.02905 p) (* 8.31 tk)))

(defun nu-air (p tk)
  (/ (mu-air tk) (rho-air p tk)))
  

(def-trivial-test::! mu-air.1
                  (and 
                   (= (mu-air 300) 1.8386085658860437E-5)
                   (= (mu-air 800) 3.616968684813432E-5))
                  t)


(def-trivial-test::! lambda-air.1
                     (lambda-air 573)
                     0.046)
                     


(defparameter *Cp-air-fn-unscaled*
  (make-piecewize-linear-scalar-fun
   #2A((-40 1.013) (-15 1.009) (0 1.005) (30 1.005)
       (90 1.009) (140 1.013) (180 1.022) (250 1.038)
       (300 1.047) (400 1.068) (500 1.093) (700 1.135)
       (900 1.172) (1200 1.210))
   :allow-extrapolation #(-90 1200) ; не считается иначе стирлинг. 
   :pseudo-name "*Cp-air-fn-unscaled*"))

(defun CpAir (tk)
  "Изобарная теплоёмкость воздуха от температуры. [МихеевМихеева], таблица в конце"
  (* (funcall *Cp-air-fn-unscaled* (- tk 273.0)) 1000.0))

;(defstruct AirStream1 VolumetricFlow Pressure Temperature ThermalExpansionCoefficient nu rho lambda Cp RGas GammaGas)
(defun CalcAirStream1WithoutVolumetricFlow (x)
  (perga
    (:lett x AirStream1 x)
    (:lett p dbl x^Pressure)
    (:lett tk dbl x^Temperature)
    (unless (<= (+ -40 273.) tk (+ 1000. 273.))
      (warn-once:warn-once "Экстраполяция свойств воздуха может быть неоправданной"))
    (setf x^RGas 287.0)
    (setf x^GammaGas 1.4)
    (setf x^nu (nu-air p tk))
    (setf x^rho (rho-air p tk)) 
    (setf x^lambda (lambda-air tk))
    (setf x^Cp (CpAir tk))
    (setf x^Cv (/ x^Cp x^GammaGas))
    (setf x^ThermalExpansionCoefficient (/ tk))
    x
    ))


(defun mu_by_t_suth_hydrogen (tk)
  (perga
    (:lett t0 dbl ([Celsius] 0))
    (:lett t_suth dbl 84.4)
    (:lett mu0 dbl 8.35e-6)
    (* mu0 (+ t0 t_suth) (/ (+ tk t_suth))
       (expt (/ tk t0) 1.5))))


(defun mu_by_t_suth_helium (tk)
  (perga
    (:lett t0 dbl ([Celsius] 0))
    (:lett t_suth dbl 80.0)
    (:lett mu0 dbl 18.85e-6)
    (* mu0 (+ t0 t_suth) (/ (+ tk t_suth))
       (expt (/ tk t0) 1.5))))


(defmethod CalcStream ((str HydrogenStream))
  (perga
    (:lett x HydrogenStream str)
    (:lett p dbl x^Pressure)
    (:lett tk dbl x^Temperature)
    (assert (<= ([Celsius] -40) tk ([Celsius] 1000)))
    (setf x^RGas 4157.2)
    (setf x^GammaGas 1.4)
    (setf x^Cv (/ x^RGas (- x^GammaGas 1)))
    (setf x^Cp (* x^GammaGas x^Cv))
    (:lett rho dbl (/ p x^RGas tk))
    (setf x^rho rho)
    (:lett mu dbl (mu_by_t_suth_hydrogen tk))
    (setf x^nu (/ mu rho))
    (setf x^ThermalExpansionCoefficient (/ tk))
    (:lett prandtl dbl 0.71)
    (setf x^lambda (* x^Cp mu (/ prandtl)))
    x
    )
  )

(defmethod CalcStream ((str HeliumStream))
  (perga
    (:lett x HeliumStream str)
    (:lett p dbl x^Pressure)
    (:lett tk dbl x^Temperature)
    (assert (<= ([Celsius] -40) tk ([Celsius] 1000)))
    (setf x^RGas 2078.6)
    (setf x^GammaGas 1.67)
    (setf x^Cv (/ x^RGas (- x^GammaGas 1)))
    (setf x^Cp (* x^GammaGas x^Cv))
    (:lett rho dbl (/ p x^RGas tk))
    (setf x^rho rho)
    (:lett mu dbl (mu_by_t_suth_helium tk))
    (setf x^nu (/ mu rho))
    (setf x^ThermalExpansionCoefficient (/ tk))
    (:lett prandtl dbl 0.71)
    (setf x^lambda (* x^Cp mu (/ prandtl)))
    x
    )
  )

    

(defmethod CalcStream ((str AirStream1))
  (perga
    (CalcAirStream1WithoutVolumetricFlow str)
    )
  )

(defun MakeAirStream1ByMassFlow (tk p massFlow)
  (perga
    (warn "MakeAirStream1ByMassFlow is deprecated. Use SetStreamMassFlow instead")
    (:lett res AirStream1 (MAKE-AirStream1 :Temperature tk :Pressure p))
    (CalcAirStream1WithoutVolumetricFlow res)
    (setf res^VolumetricFlow (/ massFlow res^rho))
    res))


(defun SetStreamMassFlow (str massFlow)
  (perga
    (:lett str FluidStreamWithGasProperties str)
    (setf str^VolumetricFlow (/ massFlow str^rho))
    str))





