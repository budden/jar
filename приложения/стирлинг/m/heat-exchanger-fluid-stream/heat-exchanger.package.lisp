; -*- Encoding: utf-8;  System :heat-exchanger-fluid-stream -*-

(in-package :budden-tools)
(in-readtable :buddens-readtable-a)

(def-merge-packages::! :heat-exchanger
                       (:use :cl :budden-tools :m0 :budden-adw-plotting
                        :heat-exchanger-fluid-stream-literature)
                       (:custom-token-parsers budden-tools::convert-carat-to-^) 
                       (:import-from :iterate-keywords
                        iterate-keywords:iter)
                       (:import-from :perga-implementation
                        perga-implementation:perga)
                       (:export
                        "
  HEAT-EXCHANGER::Pipe
  HEAT-EXCHANGER::MAKE-Pipe
  HEAT-EXCHANGER::ParallelPipes
  HEAT-EXCHANGER::MAKE-ParallelPipes
  HEAT-EXCHANGER::Annulus
  HEAT-EXCHANGER::MAKE-Annulus

  HEAT-EXCHANGER::КоридорныйПучокТруб
  HEAT-EXCHANGER::MAKE-КоридорныйПучокТруб

  HEAT-EXCHANGER::FlatChannel
  HEAT-EXCHANGER::MAKE-FlatChannel

  HEAT-EXCHANGER::FlatChannels
  HEAT-EXCHANGER::MAKE-FlatChannels

  HEAT-EXCHANGER::FlatChannelsMatrix
  HEAT-EXCHANGER::MAKE-FlatChannelsMatrix

  HEAT-EXCHANGER::SetkaRodsKaysLondon
  HEAT-EXCHANGER::MAKE-SetkaRodsKaysLondon
  HEAT-EXCHANGER::MAKE-SetkaRodsKaysLondon-ByPorosity
  HEAT-EXCHANGER::SphericalBedKaysLondon
  HEAT-EXCHANGER::MAKE-SphericalBedKaysLondon
  HEAT-EXCHANGER::CalcHeatExchanger

  HEAT-EXCHANGER::SequentialHeatExchangers
  HEAT-EXCHANGER::MAKE-SequentialHeatExchangers

  heat-exchanger::HxBase
  heat-exchanger::HeatExchangeArea  ; export some slot names
  heat-exchanger::Length  
  heat-exchanger::WettedPerimeter         
  "                       ))

