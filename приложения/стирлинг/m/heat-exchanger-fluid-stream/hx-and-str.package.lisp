; -*- Encoding: utf-8;  System :heat-exchanger-fluid-stream -*-

(named-readtables:in-readtable :buddens-readtable-a)

(def-merge-packages::! :hx-and-str
                       (:use :cl :budden-tools
                        :m0
                        :heat-exchanger-fluid-stream-literature
                        :fluid-stream
                        :heat-exchanger
                        :physical-units
                        :warn-once)
                       (:local-nicknames :hx :heat-exchanger :str :fluid-stream)
                       (:custom-token-parsers budden-tools::convert-carat-to-^) 
                       (:import-from :iterate-keywords
                        iterate-keywords:iter)
                       (:import-from :named-readtables
                        named-readtables:in-readtable
                        )
                       (:import-from :perga-implementation
                        perga-implementation:perga)
                       (:export
 "
 HX-AND-STR::hxSkorostqVSechenii
 HX-AND-STR::hxRey
 HX-AND-STR::hxPe
 HX-AND-STR::GenericPressureDrop
 HX-AND-STR::DarciFactor
 HX-AND-STR::FanningFactor
 HX-AND-STR::hxPressureDrop
 HX-AND-STR::hxNu
 HX-AND-STR::hxAlpha
 HX-AND-STR::hxSt
 hx-and-str::СреднелогарифмическийТемпературныйНапор ;
 hx-and-str::ПродольнаяТеплопроводность
 hx-and-str::MeanEffectiveTemperature 
 "))


