; -*- Encoding: utf-8;  System :heat-exchanger-fluid-stream -*-

; Некоторые обозначения из [KaysLondon] как функции потоков

(in-package :kays-london-notation)
(in-readtable :buddens-readtable-a)


(defun W (str)
  "Mass flow rate"
  (strMassFlow str))

(defun C (str)
  "Flow stream capacity rate, (W Cp)"
  (perga
    (:lett str FluidStreamWithLiquidProperties str)
    (* (W str) str^Cp)))


(defun N_tu_o (hx str)
  "modified number of transfer units (for periodic-flow heat exchangers), p.31"
  (perga
    (:lett hx HxBase hx)
    (:lett str FluidStreamWithLiquidProperties str)
    (let Cmin (C str))
    (let A hx^HeatExchangeArea)
    (let h (hxAlpha hx str))
    ;(show-expr `(,Cmin ,A ,h))
    (cond
     ((zerop Cmin) 10000.0)
     (t (* 0.5 h A (/ Cmin)))
    )))


(defun N_tu_zero (hx str)
  "Number of transfer units, p.16, 2-7. Предполагаем, что один поток безконечно мощный (например, изотермический нагреватель Стирлинга)"
  (perga
    (:lett hx HxBase hx)
    (:lett str FluidStreamWithLiquidProperties str)
    (let Cmin (C str))
    (let A hx^HeatExchangeArea)
    (let h (hxAlpha hx str))
    ;(show-expr `(,Cmin ,A ,h))
    (cond
     ((zerop Cmin) 10000.0)
     (t (* h A (/ Cmin)))
    )))