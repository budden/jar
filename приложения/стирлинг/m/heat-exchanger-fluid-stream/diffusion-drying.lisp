; -*- Encoding: utf-8;  System :heat-exchanger-fluid-stream -*-

; свободная конвекция

(in-package :diffusion-drying)
(in-readtable :buddens-readtable-a)

(defgeneric D0 (where substance &key aggregate-state)
  (:documentation "D0 для ф-лы (11.9) в [АвчуховПаюсте], стр. 83 where - вещество, в котором происходит диффузия. substance - что диффундирует, aggregate-state - агрегатное состояние смеси, например, :gas или :liquid")
  )

(defmethod D0 ((s1 (eql snc:Air)) (s2 (eql snc:Water)) &key (aggregate-state :gas))
  "[АвчуховПаюсте], стр. 83"
  (ecase aggregate-state
    (:gas
     0.216e-4)))

(defgeneric Diffusion-n (substance1 substance2 &key aggregate-state)
  (:documentation "показатель n для ф-лы коэфта диффузии [АвчуховПаюсте] (11.9), стр. 83.
  Вещества должны быть отсортированы по алфавиту или нужно определить два метода"))

(defmethod Diffusion-n ((s1 (eql snc:Air)) (s2 (eql snc:Water)) &key aggregate-state)
  "[АвчуховПаюсте], стр. 83"
  (ecase aggregate-state
    (:gas
     0.8)))

(defun D (str substance &key (aggregate-state :gas))
  "Коэффициент диффузии вещества в потоке"
  (perga
    (:lett str AirStream1 str)
    (:lett where SubstanceStruct (FluidStream-SubstanceStruct str))
    (:lett substance SubstanceStruct substance)
    (:lett n number (Diffusion-n where substance :aggregate-state aggregate-state))
    (* (apply #'D0 snc:Air substance (dispatch-keyargs-simple aggregate-state))
       (/ ([atm] 1) str^Pressure)
       (expt (/ str^Temperature 273) (+ 1 n))
       )))
    




