; -*- Encoding: utf-8;  System :heat-exchanger-fluid-stream -*-

(in-package :budden-tools)
(in-readtable :buddens-readtable-a)

(def-merge-packages::! :fluid-stream
                       (:use :cl :budden-tools :m0
                        :heat-exchanger-fluid-stream-literature
                        :physical-units
                        )
                       (:local-nicknames
                        :snc :substance-constants)
                       (:custom-token-parsers budden-tools::convert-carat-to-^) 
                       (:import-from :iterate-keywords
                        iterate-keywords:iter)
                       (:import-from :perga-implementation
                        perga-implementation:perga)
                       (:export
   "fluid-stream::AirStream1
    fluid-stream::MAKE-AirStream1

    FLUID-STREAM::HydrogenStream
    FLUID-STREAM::MAKE-HydrogenStream
    
    FLUID-STREAM::HeliumStream
    FLUID-STREAM::MAKE-HeliumStream

    fluid-stream::MakeAirStream1ByMassFlow
    fluid-stream::SetStreamMassFlow
     fluid-stream::CalcStream
     fluid-stream::FluidStreamWithLiquidProperties
     FLUID-STREAM::CalcAirStream1WithoutVolumetricFlow
     fluid-stream::WaterAt80CStream
     fluid-stream::MAKE-WaterAt80CStream
     fluid-stream::strPr  
     fluid-stream::strMu
     fluid-stream::FluidStream-SubstanceStruct
     FLUID-STREAM::strMassFlow
   "
   ))
