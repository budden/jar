; -*- Encoding: utf-8;  System :heat-exchanger-fluid-stream -*-

(in-package :budden-tools)
(in-readtable :buddens-readtable-a)

(def-merge-packages::! :free-convection
                       (:use :cl :budden-tools
                        :m0
                        :heat-exchanger-fluid-stream-literature
                        :fluid-stream
                        :heat-exchanger
                        :hx-and-str
                        :physical-units
                        :warn-once)
                       (:local-nicknames :hx :heat-exchanger :str :fluid-stream)
                       (:custom-token-parsers budden-tools::convert-carat-to-^) 
                       (:import-from :perga-implementation
                        perga-implementation:perga)
                       ;(:shadow #:HeatExchangePower)
                       (:forbid #:HeatExchangePower)
                       (:export
 "
 free-convection::OutsideOfVerticalPipeInUnlimitedSpace
 free-convection::MAKE-OutsideOfVerticalPipeInUnlimitedSpace

 free-convection::OutsideOfHorizontalPipeInUnlimitedSpace
 free-convection::MAKE-OutsideOfHorizontalPipeInUnlimitedSpace

 free-convection::ПараллельныеКруглыеДискиСОдинаковойТемпературой
 free-convection::MAKE-ПараллельныеКруглыеДискиСОдинаковойТемпературой

 free-convection::VerticalPlateInUnlimitedSpace
 free-convection::MAKE-VerticalPlateInUnlimitedSpace

 free-convection::ПараллельныеПластиныСОдинаковойТемпературой
 free-convection::MAKE-ПараллельныеПластиныСОдинаковойТемпературой

 free-convection::Ra
 free-convection::Nu
 FREE-CONVECTION::Alpha
 FREE-CONVECTION::FreeConvectionHeatExchangePower
 free-convection::TrivialLinearBuoyancyPressure
 free-convection::ПрослойкаМеждуПараллельнымиГоризонтальнымиПоверхностями
 free-convection::MAKE-ПрослойкаМеждуПараллельнымиГоризонтальнымиПоверхностями
 "))



