; -*- Encoding: utf-8; System :m0 -*-

(named-readtables:in-readtable :buddens-readtable-a)

(def-merge-packages::! :m0
  (:use :cl :physical-units :mjr_mat :mjr_vec)
  (:import-from :perga-implementation
   perga-implementation:perga
   PERGA-IMPLEMENTATION:def-perga-clause)
  (:import-from :budden-tools
   budden-tools:_f
   budden-tools:show-expr
   budden-tools:the*
   budden-tools:deftvar
   budden-tools:deftparameter
   BUDDEN-TOOLS:def-symbol-readmacro
   budden-tools:in-readtable
   budden-tools:str++
   budden-tools:|^|
   )
  (:custom-token-parsers budden-tools::convert-carat-to-^) 
  (:import-from :iterate-keywords
   iterate-keywords:iter)
  (:export ; m0-types-vars-macros.lisp
 "
 m0::mjr_vec
 m0::mjr_mat
 m0::dbl
 m0::isempty
 m0::do-for
 m0::dovector
 m0::mjr_vec
 m0::mjr_mat
 m0::dbl
 M0::non-zero-dbl
 M0::[
 m0::mjr_mat
 m0::mjr_vec
 M0::CoCell
 M0::CoCell-Isothermal
 M0::CoCell-TWall
 M0::COPY-CoCell
 M0::CoCell-P
 M0::MAKE-CoCell
 M0::[
 M0::def-simple-struct-load-form
 "
 )
  (:export ; m0-utils.lisp
 "
 m0::add-vector-to-row-of-matrix
 m0::extract-row-from-matrix
 m0::extract-col-from-matrix
 m0::extract-row-from-matrix-as-matrix
 m0::extract-rows-from-matrix
 m0::extract-cols-from-matrix
 m0::extract-elements-from-vector
 m0::extract-col-from-matrix-as-matrix
 m0::integer-range
 m0::integer-range-3
 m0::map-matrix
 m0::sum-of-matrix-elements
 m0::sum-of-vector-elements
 m0::matrix-to-double-list
 m0::min-and-max-of-sequence
 m0::show-mat
 m0::nan-p
 m0::vector-check-nan
 m0::matrix-check-nan
 m0::number-check-nan
 m0::make-zero-vector
 m0::linear-scalar-fun
 m0::make-piecewize-linear-scalar-fun
 m0::make-2d-interpolation-by-table
 m0::piecewize-linear-scalar-fun-nargs
 m0::*package-documentation*
 M0::check-slots-are-dbl 
 M0::check-slots-are-null
 M0::DONT-show-expr-t
 M0::show-expr-t
 ")
 (:export ; m0-phys-mat.lisp
 "
 m0::CircleArea
 m0::CircleLength
 m0::SphereSurfaceAreaByRadius
 m0::SphereVolumeByRadius
 m0::carnot
 m0::carnotc
 m0::RadiationalHeatExchangeK
 m0::WallThickness
 m0::+GravityG+
 m0::approx-equal
 ")
  )



