; -*- Encoding: utf-8; System :m0 -*-

(in-package :m0)
(in-readtable :buddens-readtable-a)

(eval-when (:compile-toplevel)
  (assert (eq *read-default-float-format* 'double-float)))

(defmacro isempty (var)
  `(or (not (boundp ',var))
       (null ,var)))


(defmacro do-for ((var from to) &body body)
  "Simplest form of for loop"
  `(do ((,var ,from (+ ,var 1))) ((> ,var ,to) (values))
     ,@body
     ))

(def-perga-clause do-for 'perga-implementation::when-perga-transformer)

(defmacro dovector ((index-var value-var vector) &body body)
  ""
  `(dotimes (,index-var (length ,vector))
     (let ((,value-var (aref ,vector ,index-var))) 
       ,@body)
     ))


(def-perga-clause dovector 'perga-implementation::when-perga-transformer)


(deftype mjr_vec (&key type rank) `(vector ,type ,rank))
(deftype mjr_mat (&key type rows cols) `(array ,type (,rows ,cols)))

(defun nonzero (x)
  (not (zerop x))
  )

(deftype dbl () 'double-float)
(deftype non-zero-dbl () '(and dbl (satisfies nonzero)))


(def-symbol-readmacro |[| #'(lambda (stream symbol) (declare (ignore symbol)) (bracket-reader stream)))


(defun bracket-reader (stream)
  #+SBCL (read-char stream)
  `([implementation] ,@(read-delimited-list #\] stream t)))

(defmacro [implementation] (&rest args)
  `(aref ,@args))


(defun symbol-function-for-mjr (type-or-class field-name)
  (perga
    (let name (str++ (string type-or-class) "_" (string-upcase field-name)))
    (let package (symbol-package type-or-class))
    (let result (find-symbol name package))
    (assert result () "symbol ~A not found in package ~A" name package)
    result))
  

(defmethod budden-tools::function-symbol-for-^ ((type-or-class (eql 'mjr_mat)) suffix field-name)
  (declare (ignore suffix))
  (symbol-function-for-mjr type-or-class field-name)
  )

(defmethod budden-tools::function-symbol-for-^ ((type-or-class (eql 'mjr_vec)) suffix field-name)
  (declare (ignore suffix))
  (symbol-function-for-mjr type-or-class field-name)
  )


(defstruct CoCell
  TWall
  HeatInFn ;  Функция состояния решения. Функция от номера ячейки, возвращает мгновенную ощность теплового потока по состоянию решения. 
  OutTFn ; функция от двух аргументов: номера ячейки и признака, что поток выходит вправо. Функция состояния решения. Ф-я определяет т-ру выходящего потока. Может быть nil, в этом случае см. LeftOutT, RightOutT
  LeftOutT  ; nil, число или ф-я от номера ячейки, определяет т-ру воздуха, выходящего из ячейки влево
            ; если nil, то выходит то же, что и внутри
  RightOutT ; то же, вправо
  HeatExchanger ; может использоваться HeatInFn
  RegenCellP ; регенератор машины Стирлинга
  )


(defmacro def-simple-struct-load-form (struct)
  "делает возможным дамп структур. Вероятно, при циклических структурах что-нибудь сломается, поэтому рекомендуется использовать только для структур, содержащих числа"
  `(defmethod make-load-form ((self ,struct) &optional environment)
    (make-load-form-saving-slots self :environment environment)))
