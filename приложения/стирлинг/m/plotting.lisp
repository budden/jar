; -*- Encoding: utf-8; system :budden-adw-plotting -*- 

(def-merge-packages::! :budden-adw-plotting
                       (:always t)
                       (:use :cl :budden-tools :iterate-keywords :mjr_vec :mjr_mat :m0)
                       (:local-nicknames :ac :adw-charting)
                       (:import-from :perga-implementation
                        perga-implementation:perga)
                       (:custom-token-parsers
                        budden-tools::convert-carat-to-^)
                       (:export "
                       budden-adw-plotting:plot-adw
                       budden-adw-plotting:plot-scalar-fn
                       budden-adw-plotting:plot-log-xy-scalar-fn
                       budden-adw-plotting:delete-all-plots
                       "))
                    
                  
(in-package :budden-adw-plotting)
(named-readtables:in-readtable :buddens-readtable-a)


(defun fn1 (x y) (declare (ignore x)) (vector (svref y 1) (- (svref y 0))))

#|(defparameter *sol* 
  (mjr_ode:mjr_ode_slv-ivp-erk-interval
   #'fn1 #(1.0 0.0) 0. (* 2 pi)
   :x-delta-init 1e-3
   :x-delta-min 1e-4
   :x-delta-max 1e-1
   :algorithm #'mjr_ode:mjr_ode_erk-step-runge-kutta-4
   :y-err-rel-max 1e-4
   :return-all-steps t
   ))|#



#| (defun parametric-plot-delphi (a title)
  "Массив N x 3"
  (let (dims firstdim seconddim str)
    (setf dims (array-dimensions a))
    (assert (= (length dims) 2))
    (setf firstdim (first dims))
    (setf seconddim (second dims))
    (assert (= seconddim 3))
    (setf str 
          (with-output-to-string (s)
            (dotimes (i firstdim)
              (format s "~A~%~A~%" (aref a i 1) (aref a i 2)))))
    (setf str (substitute #\E #\F str :test 'char-equal))
    (setf str (substitute #\E #\D str :test 'char-equal))
    (setf title (substitute #\  #\newline title))    
    (with-open-file (ou "C:/lisp/temp/parametric-plot.txt" :direction :output :if-does-not-exist :create :if-exists :supersede)
      (format ou "~A~%" title)
      (princ str ou))
    (system:call-system "c:\\utils\\TChartPlot.exe c:\\lisp\\temp\\parametric-plot.txt" :wait nil))) |#

#| (defun parametric-plot-adw-1 (a title)
  "Устарело"
  (perga
    (let temp-file-name "c:/lisp/temp/parametric-plot-adw.png")
    (let dims (array-dimensions a))
    (assert (= (length dims) 2))
    (let firstdim (first dims))
    (let seconddim (second dims))
    (assert (= seconddim 3))
    (adw-charting:with-line-chart (500 500 :background '(1 1 1))
      (adw-charting:add-series (the* string title)
                               (iter (:for i from 0 to (- firstdim 1))
                                     (:collect (list (aref a i 1) (aref a i 2))))
                               )
      (adw-charting:set-axis :y nil :label-formatter "~,2F")
      (adw-charting:set-axis :x nil
                :draw-gridlines-p nil
                )
      (adw-charting:save-file temp-file-name))
    (capi-win32-lib:shell-open nil temp-file-name))) |#

      
; example: (parametric-plot-adw *sol* "asdf")

(defvar *plot-file-counter* 0 "Счётчик для анонимных рисунков (в норме имя файла включает title)")
(defparameter *plot-dir* (merge-pathnames "plots/" (ql:where-is-system :m)))

(defun delete-all-plots ()
  (perga
    (let files (directory (str++ *plot-dir* "*.png")))
    (mapcar 'delete-file files)
    (length files)))

(defun quick-and-dirty-convert-string-to-valid-file-name (s)
  (remove-if
   (lambda (c) (position c "/\\<>?*\"~"))
   s))

(defun fix-plot-title-and-return-file-name (title)
  (with-byref-params (title)
    (unless title
      (setf title (str++ "untitled" (incf *plot-file-counter*))))
    (str++ *plot-dir* "plot-"
           (quick-and-dirty-convert-string-to-valid-file-name title)
           ".png")))
    
    
(defparameter *default-image-height* 400)  
(defparameter *default-image-width* 500)

(defun show-picture (file-name)
  #+lispworks (capi-win32-lib:shell-open nil file-name)
  #+sbcl (sb-ext:run-program
          "c:/clcon/bin/util/CallBatFromGuiDetached.exe"
          (list "cmd" "/c" "start" (sb-ext:native-namestring file-name))
          :wait nil
          )
  )

(defun plot-adw (xs ys &key
                    title
                    column-titles
                    (image-height *default-image-height*)
                    (image-width *default-image-width*)
                    x-data-interval ; метки по x, см. adw-charting:axis
                    y-data-interval ; метки по y, см. adw-charting:axis
                    )
  "В матрице по рядам (по первой оси) идёт время. Каждая колонка - значение ряда данных, соответствующего времени с таким же индексом из xs. Вызывается из plot-scalar-fun"
  (perga
    (:lett xs mjr_vec xs)
    (:lett ys mjr_mat ys)
    (let temp-file-name (fix-plot-title-and-return-file-name (byref title)))
    (let rows (mjr_mat_rows ys))
    (let cols (mjr_mat_cols ys))
    (assert (= rows (length xs)))
    (when column-titles
      (assert (= (length column-titles) cols) () "Количество заголовков колонок должно соответствовать количеству колонок/Number of column titles must match number of data columns"))
    (adw-charting:with-line-chart (image-width image-height :background '(1 1 1))
      (dotimes (j cols)
        (adw-charting:add-series (str++
                                  (if column-titles (elt column-titles j) j))
                                  ; (if (= j 1) (the* string title) (budden-tools:str++ j))
                                 (iter (:for i from 0 to (- rows 1))
                                   (:collect (list [ xs i ] [ ys i j ])))
                                 ))
      (adw-charting:set-axis :y nil :label-formatter "~3,2G"
                             :data-interval y-data-interval
                             :draw-gridlines-p t)
      (adw-charting:set-axis :x title :label-formatter "~3,2G"
                             :data-interval x-data-interval
                             :draw-gridlines-p t
                             )
      (adw-charting:save-file temp-file-name))
    (show-picture temp-file-name)))


(defun plot-scalar-fn (fn-or-list from to &key
                          (points 100)
                          (title "" title-supplied-p)
                          (image-height 0 image-height-supplied-p)
                          (image-width 0 image-width-supplied-p)
                          (x-data-interval nil x-data-interval-supplied-p)
                          (y-data-interval nil y-data-interval-supplied-p)
                          )
  "fn-or-list - ф-я 1 аргумента, возвращающая скаляр или список из нескольких таких функций"
  (perga
    (assert (> to from))
    (assert (> points 1))
    (:lett step number (/ (- to from) (- points 1)))
    (:lett xpoints array (integer-range-3 from step to))
    (:lett fns (cons (or symbol function)) (1-to-list fn-or-list))
    (:lett data mjr_mat (mjr_mat_make-zero (length xpoints) (length fns)))
    (dotimes (i (length xpoints))
      (dotimes (j (length fns))
        (setf [ data i j ]
              (funcall (elt fns j) [ xpoints i ]))))
    (assert (typep [ data 0 0 ] 'number))
    (apply #'plot-adw xpoints data
           (append
            (when (> (length fns) 1)
              `(:column-titles ,fns))
            (dispatch-keyargs-full title image-height image-width x-data-interval y-data-interval)))
    )
  )


(defparameter *logariphmic-scale-0dot01-100000*
  #(0.01 0.02 0.03 0.04 0.06 0.08 0.1 0.2 0.3 0.4 0.6 0.8 1 2 3 4 6 8 10 20 30 40 60 80 100 200 300 400 600 800 1000
         2000 3000 4000 6000 8000 10000 20000 40000 60000 80000 100000))

(defparameter *log-logariphmic-scale-0dot01-100000*
  (map 'vector 'log *logariphmic-scale-0dot01-100000*))

(defun format-logariphmic-label-0dot01-1000 (logx)
  (perga
   (:lett label-values sequence *logariphmic-scale-0dot01-100000*)
   (let i (position logx label-values :key 'log :test '=))
   (cond
    (i (format nil "~F" (elt label-values i)))
    (t (format nil "~F" (exp logx))))))
  

(defun plot-log-xy-scalar-fn
       (fn from to &key
           title
           (points 100)
           (image-height 400)
           (image-width 400)
           )
  "fn - ф-я 1 аргумента, возвращающая скаляр"
  (perga
    (assert (> to from))
    (assert (> points 1))
    (let temp-file-name (fix-plot-title-and-return-file-name (byref title)))
    (:lett logfrom dbl (log (coerce from 'double-float)))
    (:lett logto dbl (log (coerce to 'double-float)))
    (:lett step number (/ (- logto logfrom) (- points 1)))
    (:lett logxs mjr_vec (integer-range-3 logfrom step logto))
    (:lett logys-vector array (map 'vector (lambda (logx) (log (funcall fn (exp logx))))
                                  logxs))
    (assert (typep [ logys-vector 0 ] 'number))
    (:lett logys mjr_mat (MJR_MAT:mjr_mat_cv2m logys-vector))
    (let rows (mjr_mat_rows logys))
    (let cols (mjr_mat_cols logys))
    (let data-interval (make-next-point-by-sequence-fn *log-logariphmic-scale-0dot01-100000*))
    (let label-formatter-fn #'format-logariphmic-label-0dot01-1000)
    (assert (= rows (length logxs)))
    (adw-charting:with-line-chart (image-width image-height :background '(1 1 1))
      (dotimes (j cols)
        (adw-charting:add-series (str++ j) ; (if (= j 1) (the* string title) (budden-tools:str++ j))
                                 (iter (:for i from 0 to (- rows 1))
                                   (:collect (list [ logxs i ] [ logys i j ])))
                                 ))
      (adw-charting:set-axis :y nil :label-formatter label-formatter-fn
                             :data-interval data-interval
                             :draw-gridlines-p t)
      (adw-charting:set-axis :x title :label-formatter label-formatter-fn
                             :data-interval data-interval
                             :draw-gridlines-p t
                             )
      (adw-charting:save-file temp-file-name))
      (show-picture temp-file-name)
    )
  )

;;; port of 

(defun next-point-by-interval (i prev-x min-x max-x)
  (declare (ignore prev-x min-x max-x))
  (cond ((< i 4)
         (elt '(-10000 0.1 20.1 25) i))
        (t nil)))
        
(defun make-next-point-by-sequence-fn (seq)
  (let ((len (length seq)))
  (lambda (i prev-x min-x max-x)
    (declare (ignore prev-x min-x max-x))
    (cond
     ((< i len)
      (elt seq i))
     (t
      nil)))))


(defun make-next-point-by-floored-interval-fn (interval)
  (lambda (i prev-x min-x max-x)
    (declare (ignore max-x prev-x))
    (let ((initial (* interval (floor min-x interval))))
      (+ initial (* i interval)))))
        

(defun myplot ()
  (perga
   (let temp-file-name (str++ *plot-dir* "myplot" ".png"))
   (adw-charting:with-line-chart (400 400 :background '(0.5 0.5 0.5))
     (setf (ac::draw-legend-p ac::*current-chart*) nil)
     
     (adw-charting:add-series "asdf"
                              '((0.5 0.5) (20 20) (30.5 30)))
     (adw-charting:set-axis :x nil :draw-gridlines-p t
                            ; :data-interval (make-next-point-by-floored-interval-fn 2)
                            )
     (adw-charting:set-axis :y nil :draw-gridlines-p t
                            ; :data-interval (make-next-point-by-floored-interval-fn 2)
                            )
     (adw-charting:save-file temp-file-name)
     (show-picture temp-file-name)
      )))


(defun logariphmic-plot (fn )
  "test/example"
  (declare (ignorable fn))
  (perga
   (let temp-file-name (str++ *plot-dir* "myplot" ".png"))
   (adw-charting:with-line-chart (400 400 :background '(0.5 0.5 0.5))
     (setf (ac::draw-legend-p ac::*current-chart*) nil)
     
     (adw-charting:add-series "asdf"
                              '((0.5 0.5) (20 20) (30.5 30)))
     (adw-charting:set-axis :x nil :draw-gridlines-p t
                            ; :data-interval (make-next-point-by-floored-interval-fn 2)
                            )
     (adw-charting:set-axis :y nil :draw-gridlines-p t
                            ; :data-interval (make-next-point-by-floored-interval-fn 2)
                            )
     (adw-charting:save-file temp-file-name)
     (show-picture temp-file-name)
      )))
