;; -*- coding: utf-8; -*- 
;; Скрипт загрузки приложения "стирлинг"
;; (C) Денис Будяк 2015-17

(in-package :cl-user)

(eval-when (:compile-toplevel)
  (error "Не компилируй этот файл, загружай его с помощью load"))

#-SBCL (cerror "Всё же попробуем" "Приложение 'стирлинг' работает намного быстрее под SBCL (яр-sbcl.cmd)")

; Устанавливаем, что все литералы числа с плавающей точкой по умолчанию будут double
; Благодаря усилиям asdf, придётся делать это в каждом файле. 
(setf *READ-DEFAULT-FLOAT-FORMAT* 'DOUBLE-FLOAT)

; Это не настоящая система asd, загружает зависимости для sup-lm
(ql:quickload :sup-lm)
; Здесь мы действительно загружаем sup-lm
(load (merge-pathnames "sup-lm-asdf-loader.lisp" (ql:where-is-system :sup-lm)))


;(declaim (optimize speed))
;(proclaim '(optimize speed))
; g1 - приложение для расчёта двигателя стирлинга
(asdf:load-system :g1)

;; без этого, интерпретация бдудет слишком медленной и выполнение программы может
;; замедлиться в ~100 раз
#+SBCL (setf sb-ext:*evaluator-mode* :interpret)

;; Настраиваем текущий пакет и таблицу чтения
#+clcon 
(clco::EVAL-IN-TCL
 "::tkcon::EvalKnownCommand $::tkcon::PRIV(console) {(progn (in-package :m) (named-readtables:in-readtable :buddens-readtable-a))}")

